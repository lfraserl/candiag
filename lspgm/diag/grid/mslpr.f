      PROGRAM MSLPR 
C     PROGRAM MSLPR (GPTEMP,       GPPHI,       GPMSL,       OUTPUT,    )       D2
C    1         TAPE1=GPTEMP, TAPE2=GPPHI, TAPE3=GPMSL, TAPE6=OUTPUT)
C     --------------------------------------------------------------            D2
C                                                                               D2
C     MAR 04/10 - S.KHARIN,F.MAJAESS (CHANGE "RGAS" FROM 287. TO 287.04)        D2
C     JUL 03/03 - A.IRWIN,F.MAJAESS (REVISED FOR F90 AND LINUX PLATFORMS)       
C     JAN 14/93 - E. CHAN  (DECODE LEVELS IN 8-WORD LABEL)                      
C     JAN 29/92 - E. CHAN  (CONVERT HOLLERITH LITERALS TO ASCII)                
C     MAY 13/83 - R.LAPRISE.                                                    
C     NOV 27/80 - J.D.HENDERSON.
C                                                                               D2
CMSLPR   - COMPUTES MEAN-SEA-LEVEL-PRESSURE FROM T,PHI PRES. FILES      2  1    D1
C                                                                               D3
CAUTHOR  - J.D.HENDERSON                                                        D3
C                                                                               D3
CPURPOSE - COMPUTES A GRID FILE OF MEAN-SEA-LEVEL PRESSURE FROM THE             D3
C          LOWEST AVAILABLE ATMOSPHERIC LEVEL OF TEMPERATURE AND PHI.           D3
C          THE LAPSE RATE FROM THE LOWEST TEMPERATURE LEVEL TO                  D3
C          1000 MB IS .6 TIMES THE DRY ADIABATIC LAPSE RATE.                    D3
C          THE LAYER BETWEEN 1000. MB AND MSL IS ISOTHERMAL.                    D3
C                                                                               D3
CINPUT FILES...                                                                 D3
C                                                                               D3
C      GPTEMP = PRESSURE MULTI-LEVEL GRID SETS OF TEMPERATURE.                  D3
C      GPPHI  = PRESSURE MULTI-LEVEL GRID SETS OF GEOPOTENTIAL.                 D3
C                                                                               D3
COUTPUT FILE...                                                                 D3
C                                                                               D3
C      GPMSL  = GRIDS OF MEAN-SEA-LEVEL PRESSURE (MB)                           D3
C-----------------------------------------------------------------------------
C 
      use diag_sizes, only : SIZES_LONP1xLAT,
     &                       SIZES_LONP1xLATxNWORDIO,
     &                       SIZES_PLEV

      IMPLICIT REAL (A-H,O-Z),
     +INTEGER (I-N)
      COMMON/BLANCK/T(SIZES_LONP1xLAT),GZ(SIZES_LONP1xLAT),
     & PMSL(SIZES_LONP1xLAT)
C 
      LOGICAL OK
      INTEGER LEV(SIZES_PLEV) 
      COMMON/ICOM/ IBUF(8),IDAT(SIZES_LONP1xLATxNWORDIO) 
      DATA MAXX/SIZES_LONP1xLATxNWORDIO/, MAXL/SIZES_PLEV/
C---------------------------------------------------------------------
      NFF=4 
      CALL JCLPNT(NFF,1,2,3,6)
      REWIND 1
      REWIND 2
      REWIND 3
      RGOCP=2.E0/7.E0 
      RGAS=287.04 
C 
C     * EXAMINE FILE 1 TO DETERMINE THE NUMBER OF LEVELS IN EACH SET. 
C     * STOP IF THE FILE IS EMPTY.
C 
      CALL FILEV(LEV,NLEV,IBUF,1) 
      IF(NLEV.EQ.0)THEN 
        WRITE(6,6010) 
        CALL                                       XIT('MSLPR',-1)
      ENDIF 
      IF(NLEV.GT.MAXL) CALL                        XIT('MSLPR',-2)
      REWIND 1
      LOWEST=LEV(NLEV)
      CALL LVDCODE(PRESL,LOWEST,1)
      NPTS=IBUF(5)*IBUF(6)
C 
C     * GET THE LOWEST TEMP AND PHI IN THE NEXT SET.
C 
      NRECS=0 
  150 CALL GETFLD2(1, T,NC4TO8("GRID"), -1,NC4TO8("TEMP"),LOWEST,
     +                                              IBUF,MAXX,OK)
      IF(.NOT.OK)THEN 
        WRITE(6,6030) NRECS 
        CALL                                       XIT('MSLPR',0) 
      ENDIF 
      IF(NRECS.EQ.0) WRITE(6,6025) IBUF 
      NST=IBUF(2) 
      CALL GETFLD2(2,GZ,NC4TO8("GRID"),NST,NC4TO8(" PHI"),LOWEST,
     +                                              IBUF,MAXX,OK)
      IF(.NOT.OK)THEN 
        WRITE(6,6030) NRECS 
        CALL                                       XIT('MSLPR',-3)
      ENDIF 
      IF(NRECS.EQ.0) WRITE(6,6025) IBUF 
C 
C     * COMPUTE THE MEAN SEA LEVEL PRESSURE IN ARRAY T. 
C     * THIS CALCULATION MAKES THE ASSUMPTIONS... 
C     *    A) .6*(DRY LAPSE RATE) FROM LOWEST TEMP TO 1000 MB.
C     *    B) MSL TEMPERATURE EQUALS 1000 MB TEMP.
C 
      DO 210 I=1,NPTS 
      T1000=T(I)*(1000.E0/PRESL)**(0.6E0*RGOCP)
      GZ1000=GZ(I)-0.5E0*RGAS*(T1000+T(I))*LOG(1000.E0/PRESL) 
      PMSL(I)=1000*EXP(GZ1000/(RGAS*T1000)) 
  210 CONTINUE
C 
C     * SAVE THE PRESSURE ON FILE 3 
C 
      IBUF(3)=NC4TO8("PMSL")
      IBUF(4)=1 
      CALL PUTFLD2(3,PMSL,IBUF,MAXX) 
      IF(NRECS.EQ.0) WRITE(6,6025) IBUF 
      NRECS=NRECS+1 
      GO TO 150 
C---------------------------------------------------------------------
 6010 FORMAT('0.. MSLPR INPUT FILE IS EMPTY')
 6025 FORMAT(' ',A4,I10,2X,A4,I10,4I6)
 6030 FORMAT('0',I5,' FIELDS SAVED BY MSLPR')
      END
