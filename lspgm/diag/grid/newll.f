      PROGRAM NEWLL
C     PROGRAM NEWLL (FCSTLL,        INITLL,        GC,           GT,            D2
C    1               SNO,           WF,            WL,           SIC,           D2
C    2                                             INPUT,        OUTPUT,)       D2
C    3        TAPE11=FCSTLL, TAPE12=INITLL, TAPE13=GC,    TAPE14=GT,
C    4        TAPE15=SNO,    TAPE16=WF,     TAPE17=WL,    TAPE18=SIC,
C    5                                      TAPE5 =INPUT, TAPE6=OUTPUT)
C     -----------------------------------------------------------------         D2
C                                                                               D2
C     JUL 03/03 - A.IRWIN,F.MAJAESS (REVISED FOR F90 AND LINUX PLATFORMS)       D2
C     NOV 06/95 - F.MAJAESS (REVISE DEFAULT PACKING DENSITY VALUE)              
C     MAR 09/94 - D. RAMSDEN (MAKE CHECKING CONSISTENT WITH INITGG8)            
C     MAR 10/94 - D. RAMSDEN  (REPLACE CALLS TO CVMGT WITH CONDITIONAL BLOCKS)
C     JAN 29/92 - E. CHAN  (CONVERT HOLLERITH LITERALS TO ASCII)
C     AUG 19/88 - M.LAZARE (ADJUST FOR INTERPOLATED INSTEAD OF EXTRACTED SIC)
C     OCT 28/86 - M.LAZARE (IDAY CAN NOW BE ANY JULIAN DAY OF THE YEAR)
C     JUL 11/85 - M.LAZARE.
C     MAY 15/83 - R.LAPRISE.
C     APR 10/81 - J.D.HENDERSON
C                                                                               D2
CNEWLL   - PREPARES LAT-LONG PHYSICS GRIDS                              2  6 C  D1
C                                                                               D3
CAUTHOR  - J.D.HENDERSON                                                        D3
C                                                                               D3
CPURPOSE - PREPARES LAT-LONG PHYSICS GRIDS FOR INCLUSION IN THE INITIAL         D3
C          ANNUAL*PHYSICS*LL FILE.                                              D3
C          NOTE - ALL LAT-LONG GRIDS MUST BE THE SAME SIZE.                     D3
C                 ALSO, IT IS ASSUMED THAT THE INTERPOLATED FIELDS (FROM MODEL  D3
C                 DATA) AND THE INITLL FIELDS HAVE THE SAME DIMENSIONS.         D3
C                 ***** BE VERY CAREFUL ABOUT THIS!!!! *****                    D3
C                                                                               D3
C                 ALSO NOTE THAT INTERACTIVE ICE-OCEAN OPTIONS USED IN MODEL    D3
C                 WHICH RESULTED IN FCSTLL:                                     D3
C                    A.) IOCEAN=0 AND ICEMOD=0:                                 D3
C                        ---------------------                                  D3
C                        GC,GT,SIC OBTAINED FROM INITLL                         D3
C                    B.) IOCEAN=1 AND ICEMOD=0:                                 D3
C                        ---------------------                                  D3
C                        GC,SIC OBTAINED FROM INITLL AND                        D3
C                        GT     OBTAINED FROM FCSTLL                            D3
C                    C.) IOCEAN=1 AND ICEMOD=1:                                 D3
C                        ---------------------                                  D3
C                        GC,GT,SIC OBTAINED FROM FCSTLL                         D3
C                                                                               D3
CINPUT FILES...                                                                 D3
C                                                                               D3
C     FCSTLL = LAT-LONG GRIDS INTERPOLATED FROM FCST GG FILE                    D3
C     INITLL = INITIAL LAT-LONG GRID PHYSICS FILE                               D3
C                                                                               D3
COUTPUT FILES...                                                                D3
C                                                                               D3
C      GC    = GROUND COVER FIELD (LAND = -1., SEA = 0., ICE = +1)              D3
C      GT    = GROUND TEMPERATURE (DEGREES KELVIN).                             D3
C      SNO   = SNOW AMOUNT (KG/M**2).                                           D3
C      WF    = FROZEN GROUND WATER FRACTION (0. OVER SEA, 1. OVER ICE)          D3
C      WL    = LIQUID GROUND WATER FRACTION (1. OVER SEA, 0. OVER ICE)          D3
C      SIC   = SEA ICE AMOUNT (KG/M**2).                                        D3
C
CINPUT PARAMETERS...
C                                                                               D5
C      IDAY   = DAY OF THE YEAR OF FIRST TIMESTEP                               D5
C      GMT    = G.M.T. TIME (HRS)                                               D5
C      IOCEAN = SWITCH CONTROLLING INTERACTIVE OCEAN                            D5
C             = 1, IF INTERACTIVE OCEAN IS INCLUDED IN THE MODEL                D5
C             = 0, OTHERWISE                                                    D5
C      ICEMOD = SWITCH CONTROLLING INTERACTIVE ICE                              D5
C             = 1, IF INTERACTIVE ICE   IS INCLUDED IN THE MODEL                D5
C             = 0, OTHERWISE                                                    D5
C               NOTE - ICEMOD MUST BE ZERO IF IOCEAN IS EQUAL TO ZERO           D5
C                                                                               D5
CEXAMPLE OF INPUT CARD...                                                       D5
C                                                                               D5
C* NEWLL    18212.00    1    0                                                  D5
C----------------------------------------------------------------------------
C
C     * GTX AND GLL ARE WORK ARRAYS. GTX IS ONLY USED FOR LINEAR INTERPOLATION
C     * OF GROUND TEMPERATURE/SEA ICE AMOUNT IN THE NON-INTERACTIVE OCEAN/ICE
C     * CASE. GL IS GLACIER FIELD.
C
      use diag_sizes, only : SIZES_BLONP1xBLAT,
     &                       SIZES_BLONP1xBLATxNWORDIO

      IMPLICIT REAL (A-H,O-Z),
     +INTEGER (I-N)
      COMMON/BLANCK/GC(SIZES_BLONP1xBLAT),GTSIC(SIZES_BLONP1xBLAT),
     1              GLL(SIZES_BLONP1xBLAT),GTX(SIZES_BLONP1xBLAT),
     2              GL(SIZES_BLONP1xBLAT)
C
      LOGICAL OK
      INTEGER KBUF(8),NFDM(12),NMDM(12)
      COMMON/ICOM/IBUF(8),IDAT(SIZES_BLONP1xBLATxNWORDIO)

      DATA NFDM/  1, 32, 60, 91,121,152,182,213,244,274,305,335/
      DATA NMDM/ 16, 46, 75,106,136,167,197,228,259,289,320,350/
      DATA MAXX/SIZES_BLONP1xBLATxNWORDIO/
C--------------------------------------------------------------------
      NFF=10
      CALL JCLPNT(NFF,11,12,13,14,15,16,17,18,5,6)
      DO 100 N=11,18
  100 REWIND N
      GTFSW=271.20E0
      TFREZ=273.16E0

C     * READ THE DAY OF THE YEAR AND THE GMT, ALONG WITH THE ICE-OCEAN MODEL
C     * OPTIONS CHOSEN. CALCULATE THE FRACTION OF THE CURRENT DAY (IDAY) BASED
C     * ON GMT AND CALL IT PARTDAY.

      READ(5,5010,END=930) IDAY,GMT,IOCEAN,ICEMOD                               D4
      IF(IOCEAN.EQ.0.AND.ICEMOD.NE.0)THEN
        WRITE(6,6005)
        CALL                                       XIT('NEWLL',-1)
      ENDIF
      WRITE(6,6010) IDAY,GMT,IOCEAN,ICEMOD
      IF(IDAY.GT.365) CALL                         XIT('NEWLL',-2)

      PARTDAY=GMT/24.E0

C     * FIND THE BORDERING BEGINNING-MONTH AND MID-MONTH JULIAN DAYS FLANKING
C     * IDAY.

      L=0
      DO 105 N=1,12
        IF(IDAY.GE.NFDM(N)) L=N
  105 CONTINUE
      IF(L.EQ.0) CALL                              XIT('NEWLL',-3)
      IFDM=NFDM(L)
      IF(L.LT.12) THEN
         IFDP=NFDM(L+1)
      ELSE
         IFDP=NFDM(1)+365
      ENDIF

      M=0
      DO 110 N=1,12
        IF(IDAY.GE.NMDM(N)) M=N
  110 CONTINUE
      IF(M.EQ.0) M=12
       IMDM=NMDM(M)
      NDAYM=NFDM(M)
      IF(M.LT.12) THEN
         IMDP=NMDM(M+1)
        NDAYP=NFDM(M+1)
      ELSE
         IMDP=NMDM(1)+365
        NDAYP=NFDM(1)
      ENDIF

C     * GET THE GROUND COVER FIELD INTO GC.
C     * LAND = -1., SEA = 0., ICE = +1.

      CALL GETFLD2(12,GC,NC4TO8("GRID"),IFDM,NC4TO8("  GC"),1,
     +                                           IBUF,MAXX,OK)
      IF(.NOT.OK) CALL                             XIT('NEWLL',-4)
      WRITE(6,6025) IBUF

C     * CORRECT GC TO NEAREST VALUE OF -1,0,+1.

      NWDS=IBUF(5)*IBUF(6)
      CALL GCROUND(GC,1,NWDS)

C     * IF INTERACTIVE ICE-OCEAN MODEL IS INCLUDED, IT IS ASSUMED THAT THE
C     * EXTRACTED GC FIELD (FROM MODEL DATA) AND THE INITLL GC FIELD HAVE
C     * THE SAME DIMENSIONS.

      IF(ICEMOD.NE.0) THEN
        CALL GETFLD2(11,GLL,NC4TO8("GRID"),-1,NC4TO8("  GC"),1,
     +                                            IBUF,MAXX,OK)
        IF(.NOT.OK) CALL                           XIT('NEWLL',-5)
        WRITE(6,6025) IBUF

C       * CORRECT GC TO NEAREST VALUE OF -1,0,+1.

        NWDS=IBUF(5)*IBUF(6)
        CALL GCROUND(GLL,1,NWDS)

C       * ENSURE CONSISTENCY BETWEEN LAND COVER OF EXTRACTED MODEL DATA AND
C       * THAT OF STANDARD GEOGRAPHY DEFINED BY INITLL.

        DO 115 I=1,NWDS
          IF(GC(I).EQ.-1.E0)GLL(I)=-1.E0
          IF(GC(I).EQ.0.E0.AND.GLL(I).EQ.-1.E0)GLL(I)=0.E0
          IF(GC(I).EQ.1.E0.AND.GLL(I).EQ.-1.E0)GLL(I)=1.E0
          GC(I)=GLL(I)
  115   CONTINUE
      ENDIF
      IBUF(2)=IDAY
      IBUF(8)=1
      DO 120 I=1,8
  120 KBUF(I)=IBUF(I)
      CALL PUTFLD2(13,GC,IBUF,MAXX)
      WRITE(6,6025) IBUF
C--------------------------------------------------------------------
C     * GROUND TEMPERATURE (DEGREES KELVIN).

C     * INTERPOLATE TO CURRENT IDAY FOR NON-INTERACTIVE CASE, USING LLPHYS DATA.

      IF(IOCEAN.EQ.0) THEN
        CALL GETFLD2(-12,GLL,NC4TO8("GRID"),NDAYM,NC4TO8("  GT"),1,
     +                                                IBUF,MAXX,OK)
        IF(.NOT.OK) CALL                           XIT('NEWLL',-6)
        WRITE(6,6025) IBUF
        CALL GETFLD2(-12,GTX,NC4TO8("GRID"),NDAYP,NC4TO8("  GT"),1,
     +                                                IBUF,MAXX,OK)
        IF(.NOT.OK) CALL                           XIT('NEWLL',-7)
        WRITE(6,6025) IBUF
        IF(IDAY.LT.NMDM(1)) THEN
          RDAY=FLOAT(IDAY)+365.E0+PARTDAY
        ELSE
          RDAY=FLOAT(IDAY)+PARTDAY
        ENDIF
        WRITE(6,6030) RDAY,IMDM,IMDP
        DO 125 I=1,NWDS
          CALL LININT(FLOAT(IMDM),GLL(I),FLOAT(IMDP),GTX(I),
     1                RDAY,GTSIC(I))
  125   CONTINUE
      ENDIF

C     * GET THE NEW GROUND TEMPERATURE INTO GLL (DEG K).
C     * KEEP THE NEW VALUES OVER LAND AND ICE, ALSO OVER WATER IF THE
C     * INTERACTIVE OCEAN MODEL IS INCLUDED.

      CALL GETFLD2(11,GLL,NC4TO8("GRID"),-1,NC4TO8("  GT"),1,
     +                                          IBUF,MAXX,OK)
      IF(.NOT.OK) CALL                             XIT('NEWLL',-8)
      WRITE(6,6025) IBUF
      IBUF(2)=IDAY
      CALL CMPLBL(0,IBUF,0,KBUF,OK)
      IF(.NOT.OK)THEN
        WRITE(6,6025) IBUF
        CALL                                       XIT('NEWLL',-9)
      ENDIF
      IF(IOCEAN.EQ.0) THEN
        DO 130 I=1,NWDS
          IF(GC(I).EQ.0.E0)GLL(I)=GTSIC(I)
  130   CONTINUE
      ENDIF

C     * ENSURE CONSISTENCY BETWEEN GC AND GT.

      DO 150 I=1,NWDS
        IF(GC(I).EQ.0.E0.AND.GLL(I).LT.GTFSW)GLL(I)=GTFSW
C
C       SECOND TEST REMOVED 94.03.09 BY D. RAMSDEN
C
C        IF(GC(I).EQ.1..AND.GLL(I).GT.TFREZ)GLL(I)=TFREZ
  150 CONTINUE

      NPACK=MIN(2,IBUF(8))
      IBUF(8)=NPACK
      CALL PUTFLD2(14,GLL,IBUF,MAXX)
      WRITE(6,6025) IBUF
C--------------------------------------------------------------------
C     * SNOW AMOUNT (KG/M**2).

      REWIND 11
      CALL GETFLD2(11,GLL,NC4TO8("GRID"),-1,NC4TO8(" SNO"),1,
     +                                          IBUF,MAXX,OK)
      IF(.NOT.OK) CALL                             XIT('NEWLL',-10)
      WRITE(6,6025) IBUF

C     * MUST BE POSITIVE AND ZERO OVER WATER.

      DO 210 I=1,NWDS
        IF(GLL(I).LT.0.E0)GLL(I)=0.E0
        IF(GC(I).EQ.0.E0)GLL(I)=0.E0
  210 CONTINUE
      IBUF(2)=IDAY
      NPACK=MIN(2,IBUF(8))
      IBUF(8)= NPACK
      CALL PUTFLD2(15,GLL,IBUF,MAXX)
      WRITE(6,6025) IBUF
C
C      D. RAMSDEN 94.02.09 GET THE GLACIER FIELD TO MAKE CHECKING OF WF
C      AND WL CONSISTENT WITH INITGG8
C
      CALL GETFLD2(-12,GL,NC4TO8("GRID"),1,NC4TO8("PCNT"),1,
     +                                         IBUF,MAXX,OK)
      IF(.NOT.OK) CALL                             XIT('NEWLL',-18)
      WRITE(6,6025) IBUF
C
      DO 350 I=1,NWDS
        IF(GL(I).GT.0.5E0)    THEN
          GL(I)=1.E0
        ELSE
          GL(I)=0.E0
        ENDIF
  350 CONTINUE
C
C--------------------------------------------------------------------
C     * FROZEN GROUND WATER FRACTION (BETWEEN 0. AND 1.)

      REWIND 11
      CALL GETFLD2(11,GLL,NC4TO8("GRID"),-1,NC4TO8("  WF"),1,
     +                                          IBUF,MAXX,OK)
      IF(.NOT.OK) CALL                             XIT('NEWLL',-11)
      WRITE(6,6025) IBUF

C     * REPLACE OVER LAND, 0. OVER SEA, 1. OVER ICE.

      DO 310 I=1,NWDS
        IF(GLL(I).LT.0.E0)GLL(I)=0.E0
        IF(GLL(I).GT.1.E0)GLL(I)=1.E0
        IF(GC(I).EQ.0.E0) GLL(I)=0.E0
        IF(GC(I).EQ.1.E0) GLL(I)=1.E0
C
C       INCLUDE GLACIER CHECK: D. RAMSDEN 94.03.09
C
        IF(GL(I).EQ.1.0E0)GLL(I) =1.0E0
  310 CONTINUE
      IBUF(2)=IDAY
      NPACK=MIN(4,IBUF(8))
      IBUF(8)= NPACK
      CALL PUTFLD2(16,GLL,IBUF,MAXX)
      WRITE(6,6025) IBUF
C--------------------------------------------------------------------
C     * LIQUID GROUND WATER FRACTION (BETWEEN 0. AND 1.)

      REWIND 11
      CALL GETFLD2(11,GLL,NC4TO8("GRID"),-1,NC4TO8("  WL"),1,
     +                                          IBUF,MAXX,OK)
      IF(.NOT.OK) CALL                             XIT('NEWLL',-12)
      WRITE(6,6025) IBUF

C     * REPLACE OVER LAND, 1. OVER SEA, 0. OVER ICE.

      DO 410 I=1,NWDS
        IF(GLL(I).LT.0.E0)GLL(I)=0.E0
        IF(GLL(I).GT.1.E0)GLL(I)=1.E0
        IF(GC(I).EQ.0.E0) GLL(I)=1.E0
        IF(GC(I).EQ.1.E0) GLL(I)=0.E0
C
C       INCLUDE GLACIER CHECK: D. RAMSDEN 94.03.09
C
        IF(GL(I).EQ.1.0E0)GLL(I) =1.0E0
  410 CONTINUE
      IBUF(2)=IDAY
      NPACK=MIN(4,IBUF(8))
      IBUF(8)= NPACK
      CALL PUTFLD2(17,GLL,IBUF,MAXX)
      WRITE(6,6025) IBUF
C--------------------------------------------------------------------
C     * SEA ICE AMOUNT (KG/M**2).

      NPACK=2
      IF(ICEMOD.EQ.0) THEN

C       * INTERPOLATE TO CURRENT IDAY FOR NON-INTERACTIVE CASE, USING LLPHYS
C       * DATA.

        CALL GETFLD2(-12,GTSIC,NC4TO8("GRID"),IFDM,NC4TO8(" SIC"),1,
     +                                                 IBUF,MAXX,OK)
        IF(.NOT.OK) CALL                           XIT('NEWLL',-13)
        WRITE(6,6025) IBUF
        NPACK=MIN(NPACK,IBUF(8))
        CALL GETFLD2(-12,GTX,NC4TO8("GRID"),IFDP,NC4TO8(" SIC"),1,
     +                                               IBUF,MAXX,OK)
        IF(.NOT.OK) CALL                           XIT('NEWLL',-14)
        WRITE(6,6025) IBUF
        NPACK=MIN(NPACK,IBUF(8))
        FDAY=FLOAT(IDAY)+PARTDAY
        WRITE(6,6040) FDAY,IFDM,IFDP
        DO 510 I=1,NWDS
          CALL LININT(FLOAT(IFDM),GTSIC(I),FLOAT(IFDP),GTX(I),
     1                FDAY,GLL(I))
  510   CONTINUE

C       * MUST BE POSITIVE, ALSO NON-ZERO ONLY OVER GC=1.

        DO 550 I=1,NWDS
          IF(GLL(I).LT.0.E0)GLL(I)=0.0E0
          IF(GC(I).NE.1.E0)GLL(I)=0.0E0
  550   CONTINUE

        IBUF(2)=IDAY

      ELSE

C       * GET THE NEW SEA ICE AMOUNT INTO GLL (KG/M**2)
C       * KEEP THE NEW VALUES OVER LAND AND WATER, ALSO OVER ICE IF THE
C       * INTERACTIVE ICE MODEL IS INCLUDED.

        REWIND 11
        CALL GETFLD2(11,GLL,NC4TO8("GRID"),-1,NC4TO8(" SIC"),1,
     +                                            IBUF,MAXX,OK)
        IF(.NOT.OK) CALL                           XIT('NEWLL',-15)
        WRITE(6,6025) IBUF
        IBUF(2)=IDAY
        CALL CMPLBL(0,IBUF,0,KBUF,OK)
        IF(.NOT.OK)THEN
          WRITE(6,6025) IBUF
          CALL                                     XIT('NEWLL',-16)
        ENDIF
        NPACK=MIN(NPACK,IBUF(8))

C       * MUST BE POSITIVE, ALSO NON-ZERO ONLY OVER GC=1.

        DO 600 I=1,NWDS
          IF(GLL(I).LT.0.E0)GLL(I)=0.0E0
          IF(GC(I).NE.1.E0)GLL(I)=0.0E0
  600   CONTINUE

C       * PROBLEMS MAY HAVE OCCURRED IN INTERPOLATION OF SEA ICE VALUES NEAR
C       * LAND-ICE BOUNDARIES, SUCH THAT VALUES OF SIC=0 ARE PRESENT OVER
C       * ICE. THIS WILL OCCUR IF THE INTERPOLATED POINT ON THE LAT-LONG FILE
C       * IS CLOSEST TO A LAND BOUNDARY ON THE GAUSSIAN GRID, SO THAT ITS
C       * INTERPOLATED SEA ICE VALUE IS ZERO, WHILE THE GROUND COVER GETS RESET
C       * TO ICE SINCE THRE IS SEA ICE COVER AT THIS LOCATION. SINCE WE
C       * DESIRE THE LAND COVER GEOGRAPHY NOT CHANGE, WE MUST ALSO RESET
C       * SIC TO REPRESENTATIVE VALUES. CONSEQUENTLY, A SEARCH IS CARRIED OUT
C       * IN THE DIRECTIONS W,E,S,N, FOR AN ADJACENT LAT-LONG GRID POINT HAVING
C       * SEA ICE COVER, AND THIS VALUE IS USED. IF NONE IS FOUND, A VALUE OF
C       * 100 KG/M2 IS USED.

        NX=IBUF(5)
        NY=IBUF(6)
        DO 610 I=1,NWDS
          IF(GC(I).EQ.1.E0.AND.GLL(I).EQ.0.E0) THEN
            OLDGLLI=GLL(I)
            NCE=MOD(I,NX)
            IF (I.NE.1) THEN
              NCW=MOD((I-1),NX)
            ELSE
              NCW=0
            ENDIF
            NR=INT((FLOAT(I))/(FLOAT(NX)))+1
            IF (NCE.NE.0) THEN
              IEP=I+1
            ELSE
              NR=NR-1
              IEP=I-NX+2
            ENDIF
            IF (NCW.NE.0) THEN
              IWP=I-1
            ELSE
              IWP=I+NX-2
            ENDIF
            IF (NR.NE.1) THEN
              ISP=I-NX
            ELSE
              ISP=-100
            ENDIF
            IF (NR.NE.NY) THEN
              INP=I+NX
            ELSE
              INP=-100
            ENDIF
            IF(GC(IWP).EQ.1.E0.AND.GLL(IWP).NE.0.E0) THEN
              GLL(I)=GLL(IWP)
            ELSE IF(GC(IEP).EQ.1.E0.AND.GLL(IEP).NE.0.E0) THEN
              GLL(I)=GLL(IEP)
            ELSE IF(ISP.GT.0.AND.GC(ISP).EQ.1.E0.AND.
     &              GLL(ISP).NE.0.E0) THEN
              GLL(I)=GLL(ISP)
            ELSE IF(INP.GT.0.AND.GC(INP).EQ.1.E0.AND.
     &              GLL(INP).NE.0.E0) THEN
              GLL(I)=GLL(INP)
            ELSE
              GLL(I)=100.E0
            ENDIF
            WRITE(6,6050)I,OLDGLLI,GLL(IEP),GLL(IWP),GLL(ISP),
     1                                      GLL(INP),GLL(I)
          ENDIF
  610   CONTINUE

      ENDIF

      IBUF(8)= NPACK
      CALL PUTFLD2(18,GLL,IBUF,MAXX)
      WRITE(6,6025) IBUF

C     * NORMAL TERMINATION.

      CALL                                         XIT('NEWLL',0)

C     * E.O.F. ON INPUT.

  930 CALL                                         XIT('NEWLL',-17)
C-------------------------------------------------------------------
 5010 FORMAT(10X,I5,F5.2,2I5)                                                   D4
 6005 FORMAT('0NO INTERACTIVE ICE MODEL WITHOUT INTERACTIVE OCEAN')
 6010 FORMAT('0 IDAY =',I5,5X,' GMT =',F5.2,5X,' IOCEAN =',I5,5X,
     1       ' ICEMOD =',I5)
 6025 FORMAT(' ',A4,I10,2X,A4,I10,4I6)
 6030 FORMAT(' INTERPOLATING FOR RDAY= ',F6.2,' BETWEEN ',I3,' AND ',
     1           I3,')')
 6040 FORMAT(' INTERPOLATING FOR FDAY= ',F6.2,' BETWEEN ',I3,' AND ',
     1           I3,')')
 6050 FORMAT(' ADJUST SIC: I,OLD,EAST,WEST,SOUTH,NORTH,NEW=',I6,6F9.2)
      END
