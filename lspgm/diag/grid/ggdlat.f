      PROGRAM GGDLAT
C     PROGRAM GGDLAT (GGIN,       GGOUT,       OUTPUT,                  )       D2
C    1          TAPE1=GGIN, TAPE2=GGOUT, TAPE6=OUTPUT)
C     ------------------------------------------------                          D2
C                                                                               D2
C     JUL 03/03 - A.IRWIN,F.MAJAESS (REVISED FOR F90 AND LINUX PLATFORMS)       D2
C     JUL 13/92 - E. CHAN  (DIMENSION SELECTED VARIABLES AS REAL*8)             
C     JAN 29/92 - E. CHAN  (CONVERT HOLLERITH LITERALS TO ASCII)                
C     MAY 20/83 - R.LAPRISE.                                                   
C     NOV 07/80 - J.D.HENDERSON 
C                                                                               D2
CGGDLAT  - LATITUDE DERIVATIVE OF A GRID SET                            1  1   GD1
C                                                                               D3
CAUTHOR  - J.D.HENDERSON                                                        D3
C                                                                               D3
CPURPOSE - COMPUTES  THE LATITUDE DERIVATIVES OF GLOBAL GAUSSIAN GRIDS          D3
C          IN FILE GGIN USING FINITE DIFFERENCES AND STORES THE RESULT          D3
C          ON FILE GGOUT.                                                       D3
C                                                                               D3
CINPUT FILE...                                                                  D3
C                                                                               D3
C      GGIN  = GLOBAL GAUSSIAN GRIDS                                            D3
C                                                                               D3
COUTPUT FILE...                                                                 D3
C                                                                               D3
C      GGOUT = LATITUDE DERIVATIVES.                                            D3
C------------------------------------------------------------------------ 
C 
      use diag_sizes, only : SIZES_LAT,
     &                       SIZES_LONP1xLAT,
     &                       SIZES_LONP1xLATxNWORDIO

      IMPLICIT REAL (A-H,O-Z),
     +INTEGER (I-N)
      COMMON/BLANCK/D(SIZES_LONP1xLAT),DLON(SIZES_LONP1xLAT)
C 
      LOGICAL OK
      REAL*8 SL(SIZES_LAT),CL(SIZES_LAT),WL(SIZES_LAT),
     & WOSSL(SIZES_LAT),RAD(SIZES_LAT)
      COMMON/ICOM/ IBUF(8),IDAT(SIZES_LONP1xLATxNWORDIO) 
      DATA MAXX/SIZES_LONP1xLATxNWORDIO/
C---------------------------------------------------------------------
      NFF=3 
      CALL JCLPNT(NFF,1,2,6)
      REWIND 1
      REWIND 2
C 
C     * READ THE NEXT GRID FROM FILE GGIN.
C 
      NR=0
  140 CALL GETFLD2(1,D,NC4TO8("GRID"),-1,-1,-1,IBUF,MAXX,OK)
      IF(.NOT.OK)THEN 
        IF(NR.EQ.0)THEN 
          CALL                                     XIT('GGDLAT',-1) 
        ELSE
          WRITE(6,6010) NR
          CALL                                     XIT('GGDLAT',0)
        ENDIF 
      ENDIF 
      IF(NR.EQ.0) WRITE(6,6025) IBUF
C - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - --
C     * FIRST RECORD ONLY...
C     * GAUSSG COMPUTES GAUSSIAN LATITUDES, COSINES, ETC.(HEMISPHERIC). 
C     * TRIGL CONVERTS THESE VECTORS TO GLOBAL (S TO N).
C 
      IF(NR.GT.0) GO TO 190 
      NLON=IBUF(5)
      NLAT=IBUF(6)
      ILATH=NLAT/2
      CALL GAUSSG(ILATH,SL,WL,CL,RAD,WOSSL) 
      CALL  TRIGL(ILATH,SL,WL,CL,RAD,WOSSL) 
C - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - --
C     * COMPUTE THE  LATITUDE DERIVATIVES.
C     * SAVE ON FILE GGOUT. 
C 
  190 IF(IBUF(5).NE.NLON) CALL                     XIT('GGDLAT',-2) 
      IF(IBUF(6).NE.NLAT) CALL                     XIT('GGDLAT',-3) 
      CALL GGDY2(DLON,D,NLON,NLAT,RAD)
C 
      CALL PUTFLD2(2,DLON,IBUF,MAXX) 
      IF(NR.EQ.0) WRITE(6,6025) IBUF
      NR=NR+1 
      GO TO 140 
C---------------------------------------------------------------------
 6010 FORMAT(' ',I6,' RECORDS READ')
 6025 FORMAT(' ',A4,I10,2X,A4,I10,4I6)
      END
