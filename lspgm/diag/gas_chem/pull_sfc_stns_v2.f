      PROGRAM PULL_SFC_STATION
C     PROGRAM PULL_SFC_STATION (A,B,C,INPUT,OUTPUT)
C
C     ---------------------------------------------------------------------
C     * Read in a list of stations locations (latitude/longitude/model 
C     * level) and interpolate the model fields to that location.
C     * Instantaneous values at the station locations are output to a text 
C     * file. This version has the capability to extract station values from
C     * two separate fields:
C     *     -- a 2-D surface field saved at high frequency (perhaps hourly)
C     *     -- the standard 3-D model fields
C     * The need to extract from a separate surface field is controlled by
C     * a flag (ISFLAG) passed on the input card
C     *   ISFLAG=1; only extract data from the surface field
C     *   ISFLAG=2; only extract data from the 3-D fields
C     *   ISFLAG=3; extract data from both the 2-D and 3-D fields depending
C                     on in which level each station sites
C     ---------------------------------------------------------------------
C
C AUTHOR - D. Plummer
C
C INPUT FILES
C
C  Stations - ASCII list of station lat/lon/levels to be extracted
C  2D-FIELD - 2-D surface fields of quantity
C  3D-FIELD - full 3-D output field
C
C OUTPUT FILE
C
C  DATA - ASCII file of instantaneous model values at specified station locations
C
C INPUT CARD NEEDED (UNIT 5)
C
C*PULLSTN  1     720.0
C  INPUTS: 
COUTPUT FILE...                                                        
C                                                                      
C-------------------------------------------------------------------------- 
C
      use diag_sizes, only : SIZES_LONP1xLAT,
     &                       SIZES_LONP1xLATxNWORDIO,
     &                       SIZES_MAXLEV,
     &                       SIZES_LAT,
     &                       SIZES_LON,
     &                       SIZES_MAXLEVxLONP1xLAT
C
      IMPLICIT REAL (A-H,O-Z),
     +INTEGER (I-N)
C
C  -----DOUBLE PRECISION ARRAYS PASSED TO GAUSSG
      REAL*8, DIMENSION(SIZES_LAT)  :: RADL, WL, SL, CL, WOSSL
C
      REAL, DIMENSION(SIZES_LAT) :: GRLAT
      REAL, DIMENSION(SIZES_LON) :: GRLON
C
      LOGICAL OK
      INTEGER LEV(SIZES_MAXLEV)
C
      COMMON/BLANCK/GG(SIZES_MAXLEVxLONP1xLAT)
      COMMON/ICOM/IBUF(8),IDAT1(SIZES_LONP1xLATxNWORDIO)
C      
      DATA MAXX/SIZES_LONP1xLATxNWORDIO/
      DATA MAXLEV/SIZES_MAXLEV/
C
C ----- PARAMETERS READ FROM INPUT CARD
      INTEGER ISFLAG
      REAL DELT
C
C ----- ARRAYS TO HOLD STATION DATA
      PARAMETER (NTMAX=2000, NSTMAX= 200)
      CHARACTER*40 XPNAME(NSTMAX)
      INTEGER IPLEV(NSTMAX)
      REAL XPLAT(NSTMAX), XPLON(NSTMAX)
      REAL XPDATA(NTMAX,NSTMAX)
      REAL XPTIME1(NTMAX),XPTIME2(NTMAX)
C
      INTEGER IPT(NSTMAX),JPT(NSTMAX)
      REAL XPXFR(NSTMAX), XPYFR(NSTMAX)
C
C---------------------------------------------------------------------
      NF=6
      CALL JCLPNT(NF,1,2,-11,-12,5,6)
C
C READ INPUT CARD(1)
      READ(5,5010,END=900)ISFLAG,DELT
      WRITE(6,6010)ISFLAG,DELT
C
      IF(ISFLAG.NE.1 .AND. ISFLAG.NE.2 .AND.
     1                    ISFLAG.NE.3) CALL        XIT('PULLST',-1)
C
C --- READ IN THE LIST OF STATIONS TO PROCESS
C  ---- REMOVE ANY NEGATIVE LONGITUDES (WEST) TO MAKE THE
C       LONGITUDES RUN FROM 0 TO 360
      READ(11,*) NSTATS
      DO N=1,NSTATS
        READ(11,'(A)') XPNAME(N)
        READ(11,*)XPLAT(N),XPLON(N),IPLEV(N)
        IF(XPLON(N).LT.0.0) XPLON(N) = XPLON(N)+360.0
      ENDDO
C
C ----- IF ONLY PULLING STATIONS FROM THE SURFACE FIELD, CHECK TO ENSURE
C       ALL STATIONS ARE SPECIFIED AS BEING IN THE LOWEST MODEL LAYER
      IF(ISFLAG.EQ.1) THEN
        DO N=1,NSTATS
          IF(IPLEV(N).GT.1) CALL                  XIT('PULLST',-2)
        ENDDO
      ENDIF
C
C     * READ AND EXTRACT DATA FROM THE SEPARATE SURFACE FIELDS 
C     * IF REQUESTED
C
      NR1=0
C
      IF (ISFLAG.EQ.1 .OR. ISFLAG.EQ.3) THEN
C
C     * READ FIRST RECORD TO GET GRID INFORMATION
C
        CALL FILEV (LEV,ILEV,IBUF,1)
        IF (ILEV.GT.1) CALL                       XIT('PULLST',-3)
        WRITE(6,6007) ILEV,(LEV(L),L=1,ILEV)
C
        ILON  = IBUF(5)
        ILAT  = IBUF(6)
        ILATH = ILAT / 2
        CALL GAUSSG (ILATH,SL,WL,CL,RADL,WOSSL)
        CALL TRIGL (ILATH,SL,WL,CL,RADL,WOSSL)
C
        PI=ACOS(-1.0)
        FAC=180.0/PI
C
        DO I = 1,ILAT
           GRLAT(I)=REAL(RADL(I)*FAC)
        ENDDO
C
        XD=360.0/FLOAT(ILON-1)
        DO I=1,ILON
          GRLON(I) = FLOAT(I-1)*XD
        ENDDO
C
        WRITE(6,6020)ILON,ILAT
        write(6,*)' GAUSSIAN LATITUDES '
        write(6,'(6F13.8)')(GRLAT(I),I=1,ILAT)
        write(6,*)' GAUSSIAN LONGITUDES '
        write(6,'(6F13.8)')(GRLON(I),I=1,ILON)
C
C --- FIND THE LOCATION OF THE STATIONS IN THE GCM GRID
        DO N=1,NSTATS
          IPT(N)=0
          DO I=1,ILON
            IF(GRLON(I).LT.XPLON(N)) IPT(N)=I
          ENDDO
C
          JPT(N)=0
          DO J=1,ILAT
            IF(GRLAT(J).LT.XPLAT(N)) JPT(N)=J
          ENDDO
C
C --- FOR STATIONS BETWEEN THE POLE AND THE FIRST GAUSSIAN LATITUDE 
C     TAKE THE GRID VALUE CLOSEST TO THE STATION
          IF(JPT(N).EQ.0) THEN
            XPXFR(N)=0.0
            JPT(N)=1
          ELSEIF(JPT(N).EQ.ILAT) THEN
            XPXFR(N)=1.0
            JPT(N)=ILAT-1
          ELSE
            XPXFR(N) = (XPLON(N)-GRLON(IPT(N)))/
     1                 (GRLON(IPT(N)+1)-GRLON(IPT(N)))
          ENDIF
          XPYFR(N) = ABS( (XPLAT(N)-GRLAT(JPT(N)))/
     1                  (GRLAT(JPT(N)+1)-GRLAT(JPT(N))) )
C
          WRITE(6,'(A20,2F8.2,I5)')xpname(n),xplat(n),
     1            xplon(n),iplev(n)
          WRITE(6,'(2I5,2F8.2)')ipt(n),jpt(n),xpxfr(n),xpyfr(n)
C
        ENDDO
C
 100    CALL GETSET2(1,GG,LEV,NSL,IBUF,MAXX,OK)
        IF(NR1.EQ.0) WRITE(6,6035) IBUF
C
        IF(.NOT.OK) GOTO 991
C
        XPTIME1(NR1+1) = FLOAT(IBUF(2))*DELT/(365.0*86400.0)
        DO N=1,NSTATS
          IF(IPLEV(N).EQ.1) THEN
            XP1 = GG((JPT(N)-1)*ILON +IPT(N))
            XP2 = GG((JPT(N)-1)*ILON +IPT(N)+1)
            XP3 = GG((JPT(N)  )*ILON +IPT(N))
            XP4 = GG((JPT(N)  )*ILON +IPT(N)+1)
            PRX1 = (1.0 - XPXFR(N))*XP1 + XPXFR(N)*XP2
            PRX2 = (1.0 - XPXFR(N))*XP3 + XPXFR(N)*XP4
            XPDATA(NR1+1,N) = (1.0 - XPYFR(N))*PRX1 + XPYFR(N)*PRX2
          ENDIF
        ENDDO
C
        NR1=NR1+1
C
        GOTO 100
C 
 991    CONTINUE
C
      ENDIF
C
C     * READ AND EXTRACT DATA FROM THE FULL 3-D MODEL FIELDS
C
      NR2=0
C
      IF (ISFLAG.EQ.2 .OR. ISFLAG.EQ.3) THEN
C
C     * READ FIRST RECORD TO GET GRID INFORMATION
C
        CALL FILEV (LEV,ILEV,IBUF,2)
        IF(ILEV.LT.1 .OR. ILEV.GT.MAXLEV)  CALL      XIT('PULLST',-4)
        WRITE(6,6007) ILEV,(LEV(L),L=1,ILEV)
C
        ILON  = IBUF(5)
        ILAT  = IBUF(6)
        ILATH = ILAT / 2
        CALL GAUSSG (ILATH,SL,WL,CL,RADL,WOSSL)
        CALL TRIGL (ILATH,SL,WL,CL,RADL,WOSSL)
C
        PI=ACOS(-1.0)
        FAC=180.0/PI
C
        DO I = 1,ILAT
           GRLAT(I)=REAL(RADL(I)*FAC)
        ENDDO
C
        XD=360.0/FLOAT(ILON-1)
        DO I=1,ILON
          GRLON(I) = FLOAT(I-1)*XD
        ENDDO
C
        WRITE(6,6020)ILON,ILAT
        write(6,*)' GAUSSIAN LATITUDES '
        write(6,'(6F13.8)')(GRLAT(I),I=1,ILAT)
        write(6,*)' GAUSSIAN LONGITUDES '
        write(6,'(6F13.8)')(GRLON(I),I=1,ILON)
C
C --- FIND THE LOCATION OF THE STATIONS IN THE GCM GRID
        DO N=1,NSTATS
          IPT(N)=0
          DO I=1,ILON
            IF(GRLON(I).LT.XPLON(N)) IPT(N)=I
          ENDDO
C
          JPT(N)=0
          DO J=1,ILAT
            IF(GRLAT(J).LT.XPLAT(N)) JPT(N)=J
          ENDDO
C
C --- FOR STATIONS BETWEEN THE POLE AND THE FIRST GAUSSIAN LATITUDE 
C     TAKE THE GRID VALUE CLOSEST TO THE STATION
          IF(JPT(N).EQ.0) THEN
            XPXFR(N)=0.0
            JPT(N)=1
          ELSEIF(JPT(N).EQ.ILAT) THEN
            XPXFR(N)=1.0
            JPT(N)=ILAT-1
          ELSE
            XPXFR(N) = (XPLON(N)-GRLON(IPT(N)))/
     1                 (GRLON(IPT(N)+1)-GRLON(IPT(N)))
          ENDIF
          XPYFR(N) = ABS( (XPLAT(N)-GRLAT(JPT(N)))/
     1                  (GRLAT(JPT(N)+1)-GRLAT(JPT(N))) )
C
          WRITE(6,'(A20,2F8.2,I5)')xpname(n),xplat(n),
     1            xplon(n),iplev(n)
          WRITE(6,'(2I5,2F8.2)')ipt(n),jpt(n),xpxfr(n),xpyfr(n)
C
        ENDDO
C
 200    CALL GETSET2(2,GG,LEV,NSL,IBUF,MAXX,OK)
        IF(NR2.EQ.0) WRITE(6,6035) IBUF
C
        IF(.NOT.OK) GOTO 992
C
        XPTIME2(NR2+1) = FLOAT(IBUF(2))*DELT/(365.0*86400.0)
C
C     * IF DATA IS EXTRACTED FROM BOTH SURFACE AND 3-D FIELDS WE ONLY
C     * EXTRACT DATA FROM THE 3-D FIELDS FOR STATIONS WITH IPLEV>1
C
        IF(ISFLAG.EQ.3) THEN
          DO N=1,NSTATS
            IF(IPLEV(N).GT.1) THEN
              I1 = (ILEV-IPLEV(N))*ILON*ILAT
              XP1 = GG(I1 + (JPT(N)-1)*ILON +IPT(N))
              XP2 = GG(I1 + (JPT(N)-1)*ILON +IPT(N)+1)
              XP3 = GG(I1 + (JPT(N)  )*ILON +IPT(N))
              XP4 = GG(I1 + (JPT(N)  )*ILON +IPT(N)+1)
              PRX1 = (1.0 - XPXFR(N))*XP1 + XPXFR(N)*XP2
              PRX2 = (1.0 - XPXFR(N))*XP3 + XPXFR(N)*XP4
              XPDATA(NR2+1,N) = (1.0 - XPYFR(N))*PRX1 + XPYFR(N)*PRX2
            ENDIF
          ENDDO
        ELSE
          DO N=1,NSTATS
            I1 = (ILEV-IPLEV(N))*ILON*ILAT
            XP1 = GG(I1 + (JPT(N)-1)*ILON +IPT(N))
            XP2 = GG(I1 + (JPT(N)-1)*ILON +IPT(N)+1)
            XP3 = GG(I1 + (JPT(N)  )*ILON +IPT(N))
            XP4 = GG(I1 + (JPT(N)  )*ILON +IPT(N)+1)
            PRX1 = (1.0 - XPXFR(N))*XP1 + XPXFR(N)*XP2
            PRX2 = (1.0 - XPXFR(N))*XP3 + XPXFR(N)*XP4
            XPDATA(NR2+1,N) = (1.0 - XPYFR(N))*PRX1 + XPYFR(N)*PRX2
          ENDDO
        ENDIF
C
        NR2=NR2+1
C
        GOTO 200
C 
 992    CONTINUE
C
      ENDIF
C
      WRITE(6,6030) NR1
      WRITE(6,6031) NR2
C
      IF(NR1.GT.NTMAX) CALL                    XIT('PULLST',-5)
      IF(NR2.GT.NTMAX) CALL                    XIT('PULLST',-6)
C
      IF (ISFLAG.EQ.1 .AND. NR1.EQ.0) CALL         XIT('PULLST',-7)
      IF (ISFLAG.EQ.2 .AND. NR2.EQ.0) CALL         XIT('PULLST',-8)
      IF (ISFLAG.EQ.3 .AND.
     1     (NR1.EQ.0 .OR. NR2.EQ.0)) CALL          XIT('PULLST',-9)
C
      WRITE(12,'(A20,2I6)')' NUMBER OF TIMES ',NR1,NR2
C
      IF(ISFLAG.EQ.1 .OR. ISFLAG.EQ.3) THEN
        DO N=1,NSTATS
            IF(IPLEV(N).EQ.1) THEN
              WRITE(12,'(A35,4I4)')'STATION: '//XPNAME(N),
     1                  IPT(N),JPT(N),IPLEV(N),NR1
              DO I=1,NR1
                WRITE(12,'(F11.6,1PE11.4)')XPTIME1(I),XPDATA(I,N)
              ENDDO
            ELSE
              WRITE(12,'(A35,4I4)')'STATION: '//XPNAME(N),
     1                  IPT(N),JPT(N),IPLEV(N),NR2
              DO I=1,NR2
                WRITE(12,'(F11.6,1PE11.4)')XPTIME2(I),XPDATA(I,N)
              ENDDO
            ENDIF
        ENDDO
      ELSE
        DO N=1,NSTATS
          WRITE(12,'(A35,4I4)')'STATION: '//XPNAME(N),
     1                  IPT(N),JPT(N),IPLEV(N),NR2
          DO I=1,NR2
            WRITE(12,'(F11.6,1PE11.4)')XPTIME2(I),XPDATA(I,N)
          ENDDO
        ENDDO
      ENDIF
C
      CALL                                    XIT('PULLST', 0)
C
 900  CALL                                    XIT('PULLST',-10)
C
C---------------------------------------------------------------------
 5010 FORMAT(10X,I1,E10.0)
 6007 FORMAT(' ILEV,ETA =',I5,2X,20I5/(18X,20I5))
 6010 FORMAT(' MODEL FIELD SOURCES ',I2/,
     1        ' CALCULATE DATE USING DELT = ',F8.2)
 6020 FORMAT(' FIELD SIZE= ', 2I6)
 6030 FORMAT('0 PULLST PROCESSED ',I5,' SURFACE FIELDS') 
 6031 FORMAT('0 PULLST PROCESSED ',I5,' 3-D FIELDS') 
 6035 FORMAT(' ',A4,I10,2X,A4,I10,4I6)
      END 
