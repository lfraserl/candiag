      PROGRAM SORTTOC
C     PROGRAM SORTTOC (XIN,       XOUT,       OUTPUT,                 )
C    1          TAPE1=XIN, TAPE2=XOUT, TAPE6=OUTPUT)

C     AUTHOR  - Y.JIAO
C---------------------------------------------------------------------------
C

      use diag_sizes, only : SIZES_BLAT,
     &                       SIZES_BLONP1xBLAT

      IMPLICIT REAL (A-H,O-Z),
     +INTEGER (I-N)

      DATA LON, LAT, MAXLL /3, SIZES_BLAT,
     &  SIZES_BLONP1xBLAT/
      COMMON/BLANCK/ZTOC(3,SIZES_BLAT)
C
      INTEGER IP1(SIZES_BLAT)
      REAL ETA(SIZES_BLAT)
      COMMON/ICOM/IBUF(8)
C---------------------------------------------------------------------
      NFF=3
      CALL JCLPNT(NFF,1,2,6)
C
C     * READ ZTOC FROM FILE XIN.
      CALL GETFLD2(1,ZTOC,-1,0,0,0,IBUF,MAXLL,OK)
      NK=IBUF(6)

      OPEN(16,FILE='ztoc.txt',FORM='FORMATTED')
      K1=NK+1
      K2=NK*2-3

      DO K=K1,K2
        READ(16,*)IP1(K),ETA(K),ZTOC(2,K),ZTOC(3,K)
      ENDDO

C     CODE ETA INTO CCCMA STYLE, AND SAVE IN IP1
      CALL LVCODE(  IP1(  K1:K2),ETA(K1:K2),NK-3)

C     DECODE IP1 INTO ZTOC AND OUTPUT
      IBUF(6)=K2
      CALL LVDCODE(ZTOC(1,K1:K2),IP1(K1:K2),NK-3)
      CALL PUTFLD2(2,ZTOC,IBUF,MAXLL)
      END

