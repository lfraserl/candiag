      PROGRAM SORTMASK
C     PROGRAM SORTMASK (XIN,       XOUT,       OUTPUT,                  )       C2
C    1          TAPE1=XIN, TAPE2=XOUT, TAPE6=OUTPUT)
C     ----------------------------------------------                            C2
C                                                                               C2
CSORTMASK  - REMOVE SMALL LAKE/ISLAND <= 0.05
C                                                                               C3
CINPUT FILE...                                                                  C3
COUTPUT FILE...                                                                 C3
C                                                                               C3
C---------------------------------------------------------------------------

      use diag_sizes, only : SIZES_BLONP1xBLATxNWORDIO,
     &                       SIZES_BLONP1,
     &                       SIZES_BLAT

      IMPLICIT REAL (A-H,O-Z),
     +INTEGER (I-N)

      DATA MAXLON, MAXLAT, MAXLL /SIZES_BLONP1, SIZES_BLAT,
     &  SIZES_BLONP1xBLATxNWORDIO/
      COMMON/BLANCK/F(SIZES_BLONP1xBLATxNWORDIO)
C
      LOGICAL OK,SPEC
      COMMON/ICOM/ IBUF(8),idat(SIZES_BLONP1xBLATxNWORDIO)
      DATA MAXX/SIZES_BLONP1xBLATxNWORDIO/
C---------------------------------------------------------------------
      NFF=3
      CALL JCLPNT(NFF,1,3,6)
      REWIND 1
C
C     * READ THE NEXT GRID FROM FILE XIN.
C
      NR=0
C     READ GEM MASK
  150 CALL GETFLD2(1,F,-1,0,0,0,IBUF,MAXX,OK)
      IF(.NOT.OK)THEN
        IF(NR.EQ.0)THEN
          CALL                                     XIT('SORTMASK',-1)
        ELSE
          WRITE(6,6025) IBUF
          CALL                                     XIT('SORTMASK',0)
        ENDIF
      ENDIF
      IF(NR.EQ.0) WRITE(6,6025) IBUF
C
C     * DETERMINE SIZE OF THE FIELD.
C
      KIND=IBUF(1)
      SPEC=(KIND.EQ.NC4TO8("SPEC") .OR. KIND.EQ.NC4TO8("FOUR"))
      IF(SPEC) CALL                                XIT('SORTMASK',-2)
C
C     * PERFORM THE ALGEBRAIC OPERATION.
C     * ElIMINATE SMALL LAKE/ISLAND
C
      NWDS=IBUF(5)*IBUF(6)
      DO 210 I=1,NWDS
        IF ( F(I).GE.0.95) THEN
          F(I)=1.0
        ELSE IF ( F(I).LE.0.05) THEN
          F(I)=0.0
        ENDIF
  210 CONTINUE

C     * SAVE ON FILE XOUT.
C
      IBUF(3)=NC4TO8("MASK")
      IBUF(8)=1
      CALL PUTFLD2(3,F,IBUF,MAXX)
      NR=NR+1
      GO TO 150
 903  CALL                                       XIT('SORTMASK',-4)
C---------------------------------------------------------------------
 6025 FORMAT(' ',A4,I10,2X,A4,I10,4I6)
 6050 FORMAT("  LAND-SEA MASK ON POINT",I5,I5," REVERSED TO",F8.3)
      END
