      PROGRAM GSPAPLT
C     PROGRAM GSPAPLT(GSTEMP,   GSPHI,   GSRGASM,  PRES_T,                      J2
C    1                GPTEMP,     GPPHI,   INPUT,  OUTPUT,  )                   J2
C    2         TAPE11=GSTEMP,TAPE12=GSPHI,TAPE13=GSRGASM,TAPE14=PRES_T          J2
C    3         TAPE15=GPTEMP,TAPE16=GPPHI, TAPE5=INPUT,  TAPE6 =OUTPUT)         J2
C     -----------------------------------------------------------------         J2
C                                                                               J2
C     NOV 07/17 - MODIFIED GSAPLT TO USE PRESSURE READ FROM FILE
C     JUL 03/03 - A.IRWIN,F.MAJAESS (REVISED FOR F90 AND LINUX PLATFORMS)       J2
C     OCT  5/94 - J. KOSHYK (INTRODUCE COLUMN ARRAYS TEMPSC, PHISC INTO CALL    
C                 TO EAPLT).                                                    
C     SEP 23/94 - J. KOSHYK (CORRECT TEMP/PHI REFERENCES IN PUTSET2 CALLS)      
C     JUL 26/94 - J. KOSHYK                                                     
C                                                                               J2
CGSAPLT  - INTERPOLATES TEMP AND PHI FROM ETA (SIGMA/HYBRID) LEVELS             J1
C          TO PRESSURE LEVELS.                                          4  2 C  J1
C                                                                               J3
CAUTHOR  - J. KOSHYK                                                            J3
C                                                                               J3
CPURPOSE - INTERPOLATES TEMP AND PHI FROM ETA (SIGMA/HYBRID) LEVELS             J3
C          TO PRESSURE LEVELS.  EXTRAPOLATION UP AND DOWN IS BY LAPSE RATES,    J3
C          DF/D(LN THETA) SPECIFIED BY THE USER.                                J3
C                                                                               J3
CINPUT FILES...                                                                 J3
C                                                                               J3
C      GSTEMP  = SERIES OF GRIDS OF TEMPERATURE ON ETA LEVELS.                  J3
C      GSPHI   = SERIES OF GRIDS OF PHI ON ETA LEVELS.                          J3
C      GSRGASM = SERIES OF GRIDS OF MOIST GAS CONSTANT ON ETA LEVELS.           J3
C      GSLNSP  = SERIES OF GRIDS OF LN(SF PRES).                                J3
C                                                                               J3
COUTPUT FILES...                                                                J3
C                                                                               J3
C      GPTEMP  = PRESSURE LEVEL TEMPERATURES.                                   J3
C      GPPHI   = PRESSURE LEVEL GEOPOTENTIALS.                                  J3
C
CINPUT PARAMETERS...                                                            J5
C                                                                               J5
C      NPL    = NUMBER OF REQUESTED PRESSURE LEVELS, (MAX $L$).                 J5
C      RLUP   = LAPSE RATE, -DT/DZ USED TO EXTRAPOLATE UPWARDS (DEG/M).         J5
C      RLDN   = LAPSE RATE USED TO EXTRAPOLATE DOWNWARDS.                       J5
C      ICOORD = 4H SIG/4H ETA/4HET10/4HET15 FOR INPUT VERTICAL COORDINATE.      J5
C      PTOIT  = PRESSURE (PA) AT THE LID OF MODEL.                              J5
C      PR     = PRESSURE LEVELS (MB) (MONOTONE INCREASING I.E. TOP OF           J5
C               ATMOSPHERE TO BOTTOM.                                           J5
C                                                                               J5
CEXAMPLE OF INPUT CARDS...                                                      J5
C                                                                               J5
C*GSPAPLT.     5        0.        0.  SIG        0.                             J5
C*100  300  500  850 1000                                                       J5
C---------------------------------------------------------------------------------
C
      use diag_sizes, only : SIZES_LONP1xLAT,
     &                       SIZES_LONP1xLATxNWORDIO,
     &                       SIZES_MAXLEV,
     &                       SIZES_MAXLEVP1xLONP1xLAT,
     &                       SIZES_PTMIN

      IMPLICIT REAL (A-H,O-Z),
     +INTEGER (I-N)
      LOGICAL OK

      INTEGER LEV(SIZES_MAXLEV),LEVP(SIZES_MAXLEV),KBUF(8)

      REAL, ALLOCATABLE, DIMENSION(:) :: PR,
     &                                   TEMP, PHI, RGASM,
     &                                   PREST

C     * WORKSPACE ARRAYS

      REAL, ALLOCATABLE, DIMENSION(:) :: TEMPSC,PHISC,TPRESS,
     &                                   TRMEAN,TGAMMA,TCHI
      
      COMMON/ICOM/IBUF(8),IDAT(SIZES_LONP1xLATxNWORDIO)

      INTEGER :: NSIZ
      DATA MAXX/SIZES_LONP1xLATxNWORDIO/, 
     & MAXL/SIZES_MAXLEVP1xLONP1xLAT/, 
     & MAXLEV/SIZES_MAXLEV/
C---------------------------------------------------------------------------------
      NFIL=8
      CALL JCLPNT(NFIL,11,12,13,14,15,16,5,6)
      DO 110 N=11,16
  110 REWIND N

C     * READ THE CONTROL CARDS.

      READ(5,5010,END=908) NPL,RLUP,RLDN,ICOORD,PTOIT                           J4
      IF(ICOORD.EQ.NC4TO8("    ")) ICOORD=NC4TO8(" SIG")
      IF(ICOORD.EQ.NC4TO8(" SIG")) THEN
        PTOIT=MAX(PTOIT,0.00E0)
      ELSE
        PTOIT=MAX(PTOIT,SIZES_PTMIN)
      ENDIF
      IF(NPL.GT.MAXLEV) CALL                       XIT('GSPAPLT',-1)
      READ(5,5020,END=909) (LEVP(I),I=1,NPL)                                    J4

C     * DECODE LEVELS.

      ALLOCATE(PR(NPL))
      CALL LVDCODE(PR,LEVP,NPL)

      WRITE(6,6010) RLUP,RLDN,ICOORD,PTOIT
      CALL WRITLEV(PR,NPL,' PR ')

      DO 114 L=2,NPL
  114 IF(PR(L).LE.PR(L-1)) CALL                    XIT('GSPAPLT',-2)
        
      DO 115 L=1,NPL
  115 PR(L)=100.E0*PR(L)

C     * GET ETA VALUES FROM THE GSTEMP FILE.

      CALL FILEV (LEV,NSL,IBUF,11)
      IF(NSL.LT.1 .OR. NSL.GT.MAXLEV) CALL         XIT('GSPAPLT',-3)
      NWDS = IBUF(5)*IBUF(6)
      IF((MAX(NSL,NPL)+1)*NWDS.GT.MAXL) CALL       XIT('GSPAPLT',-4)
      DO 116 I=1,8
  116 KBUF(I)=IBUF(I)
      
      NSIZ=(MAX(NSL,NPL)+1)*NWDS
      ALLOCATE(TEMP(NSIZ), PHI(NSIZ), RGASM(NSIZ),
     &         PREST(NSIZ))
     
      NSIZ=MAX(NSL,NPL)
      ALLOCATE(TEMPSC(NSIZ),PHISC(NSIZ),TPRESS(NSIZ),
     &         TRMEAN(NSIZ+1),TGAMMA(NSIZ+1),TCHI(NSIZ+1))
C---------------------------------------------------------------------------------
C     * GET MULTI-LEVEL TEMPERATURE FIELD FROM FILE GSTEMP.

      NSETS=0
  150 CALL GETSET2 (11,TEMP,LEV,NSL,IBUF,MAXX,OK)
      IF(NSETS.EQ.0) WRITE(6,6035) IBUF
      IF(.NOT.OK)THEN
        WRITE(6,6030) NSETS
        IF(NSETS.EQ.0)THEN
          CALL                                     XIT('GSPAPLT',-5)
        ELSE
          CALL                                     XIT('GSPAPLT', 0)
        ENDIF
      ENDIF
      CALL CMPLBL (0,IBUF,0,KBUF,OK)
      IF(.NOT.OK) CALL                             XIT('GSPAPLT',-6)
      NPACK=IBUF(8)

C     * GET MULTI-LEVEL GEOPOTENTIAL FIELD FROM FILE GSPHI.

      CALL GETSET2 (12,PHI,LEV,NSL,IBUF,MAXX,OK)
      IF(NSETS.EQ.0) WRITE(6,6035) IBUF
      IF(.NOT.OK) CALL                             XIT('GSPAPLT',-7)
      CALL CMPLBL (0,IBUF,0,KBUF,OK)
      IF(.NOT.OK) CALL                             XIT('GSPAPLT',-8)

C     * GET MULTI-LEVEL MOIST GAS CONSTANT FIELD FROM FILE GSRGASM.

      CALL GETSET2 (13,RGASM,LEV,NSL,IBUF,MAXX,OK)
      IF(NSETS.EQ.0) WRITE(6,6035) IBUF
      IF(.NOT.OK) CALL                             XIT('GSPAPLT',-9)
      CALL CMPLBL (0,IBUF,0,KBUF,OK)
      IF(.NOT.OK) CALL                             XIT('GSPAPLT',-10)

C     * GET MULTI-LEVEL ATMSOPHERIC PRESSURE (HPA)

      NST= IBUF(2)
      CALL GETSET2 (14,PREST,LEV,NSL,IBUF,MAXX,OK)
      IF(NSETS.EQ.0) WRITE(6,6035) IBUF
      IF(.NOT.OK) CALL                             XIT('GSPAPLT',-11)
      CALL CMPLBL (0,IBUF,0,KBUF,OK)
      IF(.NOT.OK) CALL                             XIT('GSPAPLT',-12)
      IF(IBUF(2).NE.NST) CALL                      XIT('GSPAPLT',-13)

C     * INTERPOLATE IN-PLACE FROM ETA TO PRESSURE.

      CALL EPAPLT (TEMP,PHI,RGASM,PREST,NWDS,NSL,NSL+1,PR,
     1             NPL,RLUP,RLDN,TEMP,PHI,
     2             TEMPSC,PHISC,TPRESS,TRMEAN,TGAMMA,TCHI)

C     * WRITE THE PRESSURE LEVEL GRIDS ONTO FILES 15 AND 16.

      IBUF(8)=NPACK

      IBUF(3)=NC4TO8("TEMP")
      CALL PUTSET2 (15,TEMP,LEVP,NPL,IBUF,MAXX)
      IF(NSETS.EQ.0) WRITE(6,6035) IBUF

      IBUF(3)=NC4TO8(" PHI")
      CALL PUTSET2 (16, PHI,LEVP,NPL,IBUF,MAXX)
      IF(NSETS.EQ.0) WRITE(6,6035) IBUF

      NSETS=NSETS+1
      GO TO 150

C     * E.O.F. ON INPUT.

  908 CALL                                         XIT('GSPAPLT',-14)
  909 CALL                                         XIT('GSPAPLT',-15)
C---------------------------------------------------------------------------------
 5010 FORMAT(10X,I5,2E10.0,1X,A4,E10.0)                                         J4
 5020 FORMAT(16I5)                                                              J4
 6010 FORMAT(' RLUP,RLDN = ',2F6.2,', ICOORD=',1X,A4,
     1       ', P.LID (PA)=',E10.3)
 6030 FORMAT('0 GSPAPLT INTERPOLATED',I5,' SETS OF ',A4)
 6035 FORMAT(' ',A4,I10,2X,A4,I10,4I6)
      END
      SUBROUTINE EPAPLT(TEMPS,PHIS,RGASMS,PREST,LA,NSL,NSL1,PR,
     1                  NPL,RLUP,RLDN,TEMPPR,PHIPR,
     2                  TEMPC,PHIC,PRESS,RMEAN,GAMMA,CHI)

C     * NOV 07/17 - D.PLUMMER - ORIGINAL EAPLT MODIFIED TO USE PRESSURE
C                                PROVIDED AS AN INPUT FIELD
C     * OCT  5/94 - J.KOSHYK (INTRODUCE COLUMN ARRAYS TEMPC/PHIC TO CORRECT
C                   CALCULATIONS OF TEMPPR/PHIPPR).
C     * JUL 27/94 - J.KOSHYK

C     * INTERPOLATES MULTI-LEVEL SETS OF TEMP AND PHI FROM ETA
C     * LEVELS TO PRESSURE LEVELS.
C
C     * ALL GRIDS HAVE THE SAME HORIZONTAL SIZE (LA POINTS).
C     * TEMPS    = INPUT TEMPERATURE GRIDS ON ETA LEVELS.
C     * PHIS     = INPUT GEOPOTENTIAL HEIGHT GRIDS ON ETA LEVELS.
C     * RGASMS   = INPUT MOIST GAS CONSTANT GRIDS ON ETA LEVELS.
C     * PREST    = INPUT PRESSURE (HPA) GRIDS ON ETA LEVELS.
C     * NSL      = NUMBER OF ETA LEVELS.
C     * PR(NPL)  = VALUES OF INPUT PRESSURE LEVELS (MB);
C     *            (MUST BE MONOTONIC AND INCREASING).
C     * RLUP     = -DT/DZ USED TO EXTRAPOLATE ABOVE TOP ETA.
C     * RLDN     = -DT/DZ USED TO EXTRAPOLATE BELOW BOTTOM ETA.
C     * A, B     = PARAMETERS OF ETA VERTICAL DISCRETIZATION.
C     * TEMPPR   = OUTPUT GRIDS OF TEMPERATURE ON PRESSURE LEVELS.
C     * PHIPR    = OUTPUT GRIDS OF GEOPOTENTIAL HEIGHT ON PRESSURE LEVELS.
C
      IMPLICIT REAL (A-H,O-Z),
     +INTEGER (I-N)
      REAL TEMPS (LA,NSL),PHIS (LA,NSL),RGASMS (LA,NSL),PREST(LA,NSL)
      REAL PR       (NPL)
      REAL TEMPPR(LA,NPL),PHIPR(LA,NPL)

C     * WORK SPACE.

      REAL TEMPC(NSL),PHIC(NSL)
      REAL PRESS(NPL)
      REAL RMEAN(NSL1),GAMMA(NSL1),CHI(NSL1)

      PARAMETER(G=9.81E0)

C---------------------------------------------------------------------------------
C     * LOOP OVER ALL HORIZONTAL POINTS.

      DO 500 I=1,LA

C     * COMPUTE PRESSURE (PA) ON ETA LEVELS.

      DO 150 L=1,NSL
  150 PRESS(L) = 100.0*PREST(I,L)

C

      DO 160 L=1,NSL
        TEMPC(L) = TEMPS(I,L)
        PHIC (L) = PHIS (I,L)
  160 CONTINUE

C     * COMPUTE THE LAPSE RATE, GAMMA = -DT/D PHI OVER ALL INPUT INTERVALS,
C     * AND CALCULATE RELATED QUANTITIES FOR LATER USE.

      DO 180 L=1,NSL-1
        RMEAN(L+1) =  (RGASMS(I,L+1)+RGASMS(I,L))/2.E0
        GAMMA(L+1) = -(TEMPC(L+1)-TEMPC(L))/
     1                (PHIC (L+1)-PHIC (L))
        CHI  (L+1) =  RMEAN(L+1)*GAMMA(L+1)
  180 CONTINUE

C     * ASSIGN VALUES OF QUANTITIES ABOVE HIGHEST AND BELOW LOWEST
C     * ETA LEVEL.

      GAMMA(1)     = RLUP/G
      GAMMA(NSL1)  = RLDN/G

C     * ASSUME VALUE OF RGASM ABOVE HIGHEST LEVEL = VALUE WITHIN HIGHEST LAYER,
C     *    AND VALUE OF RGASM BELOW LOWEST  LEVEL = VALUE WITHIN LOWEST  LAYER.

      RMEAN(1)     = RMEAN(2)
      RMEAN(NSL1)  = RMEAN(NSL)
      CHI  (1)     = RMEAN(1)*GAMMA(1)
      CHI  (NSL1)  = RMEAN(NSL1)*GAMMA(NSL1)

C     * LOOP OVER PRESSURE LEVELS TO BE INTERPOLATED.

      K=1
      DO 350 N=1,NPL

C     * FIND WHICH SIGMA INTERVAL WE ARE IN.

      DO 310 L=K,NSL
      INTVL=L
  310 IF(PR(N).LT.PRESS(L)) GO TO 320
      INTVL=NSL+1
  320 K=INTVL-1
      IF(K.EQ.0) K=1

C     * NOW INTERPOLATE AT THIS POINT.

      TEMPPR(I,N) = TEMPC(K)*(PR(N)/PRESS(K))**CHI(INTVL)

      IF (GAMMA(INTVL) .NE. 0) THEN
        PHIPR (I,N) = PHIC(K)-(TEMPPR(I,N)-TEMPC(K))/GAMMA(INTVL)
      ELSE

C       * COMPUTE PHI FROM HYDROSTATIC EQUATION, ASSUMING ISOTHERMAL LAYER.

        PHIPR (I,N) = PHIC(K)-RMEAN(INTVL)*TEMPPR(I,N)
     1                          *LOG(PR(N)/PRESS(K))
      ENDIF

  350 CONTINUE
  500 CONTINUE

      RETURN
      END
