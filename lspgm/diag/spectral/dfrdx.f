      PROGRAM DFRDX 
C     PROGRAM DFRDX (FC,       DFDX,       INPUT,       OUTPUT,         )       E2
C    1         TAPE1=FC, TAPE2=DFDX, TAPE5=INPUT, TAPE6=OUTPUT) 
C     ---------------------------------------------------------                 E2
C                                                                               E2
C     JUL 03/03 - A.IRWIN,F.MAJAESS (REVISED FOR F90 AND LINUX PLATFORMS)       E2
C     JAN 29/92 - E. CHAN  (CONVERT HOLLERITH LITERALS TO ASCII)                
C     MAY 06/83 - R.LAPRISE.                                                   
C     DEC  4/80 - J.D.HENDERSON 
C     S. LAMBERT  MAR 1980  CCRN
C                                                                               E2
CDFRDX   - ZONAL DERIVATIVE OF FOURIER FILE                             1  1    E1
C                                                                               E3
CAUTHOR  - S.LAMBERT                                                            E3
C                                                                               E3
CPURPOSE - COMPUTES ZONAL DERIVATIVES OF A FILE OF FOURIER COEFF.               E3
C                                                                               E3
CINPUT FILE...                                                                  E3
C                                                                               E3
C      FC   = INPUT FILE OF FOURIER COEFFICIENTS                                E3
C                                                                               E3
COUTPUT FILE...                                                                 E3
C                                                                               E3
C      DFDX = OUTPUT FILE OF ZONAL DERIVATIVES OF FC.                           E3
C-------------------------------------------------------------------------
C 
      use diag_sizes, only : SIZES_LAT,
     &                       SIZES_LMTP1,
     &                       SIZES_NWORDIO

      IMPLICIT REAL (A-H,O-Z),
     +INTEGER (I-N)

      integer, parameter :: 
     & MAXX = 2*(SIZES_LMTP1+1)*SIZES_LAT*SIZES_NWORDIO 

      COMPLEX F 
      COMMON/BLANCK/F((SIZES_LMTP1+1)*SIZES_LAT) 
C 
      COMPLEX A 
      LOGICAL OK
      COMMON/ICOM/IBUF(8),IDAT(MAXX) 
C-----------------------------------------------------------------------
      NFF=3 
      CALL JCLPNT(NFF,1,2,6)
      REWIND 1
      REWIND 2
C 
      CALL GETFLD2(1,F,NC4TO8("FOUR"),-1,-1,-1,IBUF,MAXX,OK)
      IF(.NOT.OK) CALL                             XIT('DFRDX',-1)
      REWIND 1
      LM=IBUF(5)
      NLAT=IBUF(6)
      NRECS=0 
  100 CALL GETFLD2(1,F,NC4TO8("FOUR"),-1,-1,-1,IBUF,MAXX,OK)
      IF(.NOT.OK)THEN 
        WRITE(6,6020) NRECS 
        CALL                                       XIT('DFRDX',0) 
      ENDIF 
      IF(NRECS.EQ.0) WRITE(6,6030) IBUF 
C 
C     * COMPUTATION OF ZONAL DERIVATIVE 
C 
      DO 200 I=1,NLAT 
      DO 200 MM=1,LM
      M=MM-1
      A=F(LM*(I-1)+MM)
200   F(LM*(I-1)+MM)=CMPLX(-FLOAT(M)* IMAG(A),FLOAT(M)*REAL(A)) 
      IBUF(3)=NC4TO8("DFDX")
      CALL PUTFLD2(2,F,IBUF,MAXX)
      IF(NRECS.EQ.0) WRITE(6,6030) IBUF 
      NRECS=NRECS+1 
      GO TO 100 
C-----------------------------------------------------------------------
 6020 FORMAT(' ',I6,' RECORDS READ')
6030  FORMAT(1X,A4,I10,2X,A4,I10,4I6)
      END
