      PROGRAM ZXDLATF 
C     PROGRAM ZXDLATF (ZXIN,       ZXOUT,       INPUT,       OUTPUT,    )       F2
C    1           TAPE1=ZXIN, TAPE2=ZXOUT, TAPE5=INPUT, TAPE6=OUTPUT)
C     --------------------------------------------------------------            F2
C                                                                               F2
C     JUL 03/03 - A.IRWIN,F.MAJAESS (REVISED FOR F90 AND LINUX PLATFORMS)       F2
C     JUL 13/92 - E. CHAN  (DIMENSION SELECTED VARIABLES AS REAL*8)             
C     JAN 29/92 - E. CHAN  (CONVERT HOLLERITH LITERALS TO ASCII)                
C     MAY 14/83 - R.LAPRISE.                                                    
C     NOV 07/80 - J.D.HENDERSON 
C                                                                               F2
CZXDLATF - CROSS-SECTION LATITUDE DERIVATIVE - FINITE DIFFERENCE        1  1   GF1
C                                                                               F3
CAUTHOR  - J.D.HENDERSON                                                        F3
C                                                                               F3
CPURPOSE - READS CROSS-SECTIONS FROM FILE ZXIN, COMPUTES THE LATITUDE           F3
C          DERIVATIVE OF THE CROSS SECTIONS USING FINITE DIFFERENCES            F3
C          AND PUTS THE RESULT ON FILE ZXOUT.                                   F3
C          NOTE - MAX LATITUDES IS $BJ$.                                        F3
C                                                                               F3
CINPUT FILE...                                                                  F3
C                                                                               F3
C      ZXIN  = CROSS-SECTIONS                                                   F3
C                                                                               F3
COUTPUT FILE...                                                                 F3
C                                                                               F3
C      ZXOUT = LAT DERIVATIVES                                                  F3
C---------------------------------------------------------------------------- 
C 
      use diag_sizes, only : SIZES_BLAT,
     &                       SIZES_BLATxNWORDIO

      IMPLICIT REAL (A-H,O-Z),
     +INTEGER (I-N)

      integer, parameter :: MAXX = SIZES_BLATxNWORDIO

      COMMON/BLANCK/D(SIZES_BLAT),DLAT(SIZES_BLAT)
C 
      LOGICAL OK
      REAL*8 SL(SIZES_BLAT),CL(SIZES_BLAT),WL(SIZES_BLAT),
     & WOSSL(SIZES_BLAT), RAD(SIZES_BLAT)  
      COMMON/ICOM/IBUF(8),IDAT(MAXX)
C---------------------------------------------------------------------
      NFF=3 
      CALL JCLPNT(NFF,1,2,6)
      REWIND 1
      REWIND 2
C 
      NR=0
  110 CALL GETFLD2(1,D,NC4TO8("ZONL"),-1,-1,-1,IBUF,MAXX,OK)
      IF(.NOT.OK)THEN 
        IF(NR.EQ.0) CALL                           XIT('ZXDLATF',-1)
        WRITE(6,6010) NR
        CALL                                       XIT('ZXDLATF',0) 
      ENDIF 
      IF(NR.EQ.0) WRITE(6,6025) IBUF
C - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - 
C     * FIRST TIME ONLY...
C     * GAUSSG COMPUTES THE VALUE OF THE GAUSSIAN LATITUDES AND THEIR 
C     * SINES AND COSINES. TRIGL MAKES THEM GLOBAL (S TO N).
C 
      IF(NR.GT.0) GO TO 190 
      NLAT=IBUF(5)
      NLATM=NLAT-1
      ILATH=NLAT/2
      CALL GAUSSG(ILATH,SL,WL,CL,RAD,WOSSL) 
      CALL  TRIGL(ILATH,SL,WL,CL,RAD,WOSSL) 
C - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - 
C     * COMPUTE THE LATITUDE DERIVATIVE.
C     * ENDS USE ONE-SIDED DIFFERENCE. OTHER POINTS USE CENTERED DIFF.
C 
  190 IF(IBUF(5).NE.NLAT) CALL                     XIT('ZXDLATF',-2)
      DLAT(1)=(D(2)-D(1))/(RAD(2)-RAD(1)) 
      DO 310 J=2,NLATM
  310 DLAT(J)=(D(J+1)-D(J-1))/(RAD(J+1)-RAD(J-1)) 
      DLAT(NLAT)=(D(NLAT)-D(NLATM))/(RAD(NLAT)-RAD(NLATM))
C 
C     * PUT THE RESULT ON FILE ZXOUT. 
C 
      CALL PUTFLD2(2,DLAT,IBUF,MAXX) 
      IF(NR.EQ.0) WRITE(6,6025) IBUF
      NR=NR+1 
      GO TO 110 
C---------------------------------------------------------------------
 6010 FORMAT(' ',I6,' RECORDS READ')
 6025 FORMAT(' ',A4,I10,1X,A4,I10,4I6)
      END
