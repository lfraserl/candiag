      PROGRAM ZXPRINT 
C     PROGRAM ZXPRINT (INZX,       INPUT,       OUTPUT,                 )       A2
C    1           TAPE1=INZX, TAPE5=INPUT, TAPE6=OUTPUT) 
C     -------------------------------------------------                         A2
C                                                                               A2
C     JUL 03/03 - A.IRWIN,F.MAJAESS (REVISED FOR F90 AND LINUX PLATFORMS)       A2
C     JUL 13/92 - E. CHAN  (DIMENSION SELECTED VARIABLES AS REAL*8)             
C     JAN 29/92 - E. CHAN  (CONVERT HOLLERITH LITERALS TO ASCII)                
C     JUN 13/84 - M.LAZARE.                                                     
C                                                                               A2
CZXPRINT - PRINTS VALUES OF A CROSS SECTION FILE                        1  0 C  A1
C                                                                               A3
CAUTHOR  - M.LAZARE                                                             A3
C                                                                               A3
CPURPOSE - PRINT ON LINE PRINTER THE VALUES IN A CROSS-SECTION FILE.            A3
C          THESE CAN BE ZONAL OR MERIDIONAL AS DETERMINED FROM THE              A3
C          INPUT CARD(S).                                                       A3
C          NOTE - MAXIMUM LEVELS IS $L$ AND MAXIMUM LAT/LON IS $BI$.            A3
C                                                                               A3
CINPUT FILE...                                                                  A3
C                                                                               A3
C      INZX = FILE OF TYPE 4HZONL CONTAINING THE DATA                           A3
C 
CINPUT PARAMETERS...
C                                                                               A5
C      NAME  = NAME OF CROSS SECTION FOR WHICH VALUES ARE TO BE PRINTED         A5
C      ISW   = OMIT THE NAME CHECK IF ISW.GT.0. THIS IS INCLUDED FOR            A5
C              COMPATIBILITY WITH A PREVIOUS VERSION.                           A5
C      KZM   = 0 FOR ZONAL CROSS SECTION                                        A5
C              1 FOR MERIDIONAL CROSS-SECTION                                   A5
C      LABEL = 80 CHARACTER LABEL PRINTED IF WE ARE NOT IN SKIP MODE            A5
C                                                                               A5
CEXAMPLE OF INPUT CARD(S)...                                                    A5
C                                                                               A5
C*ZXPRINT  TEMP    0    0                                                       A5
C* TEMPERATURE ZONAL CROSS SECTION                                              A5
C------------------------------------------------------------------------------ 
C 
      use diag_sizes, only : SIZES_BLAT,
     &                       SIZES_BLONP1,
     &                       SIZES_BLONP1xNWORDIO,
     &                       SIZES_MAXLEV

      IMPLICIT REAL (A-H,O-Z),
     +INTEGER (I-N)

      LOGICAL OK
      INTEGER LEV(SIZES_MAXLEV),LABEL(10),IPR(SIZES_MAXLEV) 
      REAL*8 SL(SIZES_BLAT),CL(SIZES_BLAT),
     & WL(SIZES_BLAT),WOSSL(SIZES_BLAT),
     & RAD(SIZES_BLAT)
      REAL   ANG(SIZES_BLAT)
C 
      COMMON/ICOM/IBUF(8),IDAT(SIZES_BLONP1xNWORDIO) 
C 
      COMMON/BLANCK/GX(SIZES_BLONP1,SIZES_MAXLEV),
     & XS(SIZES_MAXLEV,SIZES_BLONP1) 
C 
      DATA MAXX/SIZES_BLONP1xNWORDIO/, 
     & MAXL/SIZES_MAXLEV/, 
     & MAXD/SIZES_BLONP1/ 
C---------------------------------------------------------------------
      NFF=3 
      CALL JCLPNT(NFF,1,5,6)
      REWIND 1
C 
C     * READ THE CONTROL CARDS. 
C     * KZM=(0,1) FOR (ZONAL,MERIDIONAL) CROSS-SECTION. 
C     * LABEL IS SIMPLY PRINTED AFTER THE CROSS-SECTION OUTPUT. 
C     * NO LABEL CARD IS READ IF WE ARE SKIPPING (NAME=4HSKIP). 
C 
  150 READ(5,5010,END=900) NAME,ISW,KZM                                         A4
      IF(NAME.NE.NC4TO8("SKIP")) READ(5,5012,END=902) LABEL                     A4
      IF(KZM.NE.0) KZM=1
C 
C     * GET THE NEXT REQUESTED CROSS-SECTION. 
C     * OMIT THE NAME CHECK IF ISW.GT.0. THIS IS INCLUDED 
C     * FOR COMPATIBILITY WITH A PREVIOUS VERSION.
C 
      NAM=NAME
      IF(NAME.EQ.NC4TO8("SKIP")) NAM=NC4TO8("NEXT")
      IF(ISW.GT.0) NAM=NC4TO8("NEXT")
      CALL FIND(1,NC4TO8("ZONL"),-1,NAM,-1,OK)
      IF(.NOT.OK) THEN
                  WRITE(6,6010) NAME
                  CALL                             XIT('ZXPRINT',-101)
      ENDIF 
      CALL GETZX2(1,GX,MAXD,LEV,NLEV,IBUF,MAXX,OK) 
      IF(NLEV.LT.1 .OR. NLEV.GT.MAXL) THEN
                  CALL                             XIT('ZXPRINT',-102)
      ENDIF 
      IF(NAME.EQ.NC4TO8("SKIP")) GO TO 150
C 
C     * PUT GX(NLAT,NLEV) INTO XS(NLEV,NLAT) WITH LEVELS REVERSED,
C     * AND ROWS REVERSED IF MERIDIONAL.
C     * NLAT IS (LATITUDE,LONGITUDE) AS KZM IS (0,1). 
C     * GX(1,1) CONTAINS (SOUTH,TOP) OR (WEST,TOP). 
C     * XS(1,1) CONTAINS (BOTTOM,SOUTH) OR (BOTTOM,EAST). 
C 
      NLAT=IBUF(5)
      DO 210 L=1,NLEV 
      K=NLEV+1-L
      IPR(K)=LEV(L) 
      DO 210 J=1,NLAT 
      JJ=J
      IF(KZM.EQ.1) JJ=NLAT+1-J
  210 XS(K,J)=GX(JJ,L)
C - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - 
C     * COMPUTE LATITUDES FOR ZONAL CROSS-SECTION (KZM=0).
C     * GAUSSG COMPUTES THE VALUE OF THE GAUSSIAN LATITUDES AND THEIR 
C     * SINES AND COSINES. TRIGL MAKES THEM GLOBAL (S TO N).
C     * NLAT MUST BE GLOBAL AND A MULTIPLE OF 2.
C 
      IF(KZM.NE.0) GO TO 322
      ILATH=NLAT/2
      CALL GAUSSG(ILATH,SL,WL,CL,RAD,WOSSL) 
      CALL  TRIGL(ILATH,SL,WL,CL,RAD,WOSSL) 
      DO 320 J=1,NLAT 
  320 ANG(J)= RAD(J)*180.E0/3.14159E0 
      GO TO 326 
C 
C     * COMPUTE LONGITUDES FOR MERIDIONAL CROSS-SECTION (KZM=1).
C 
  322 DO 324 I=1,NLAT 
      N=NLAT+1-I
  324 ANG(N)=FLOAT(I-1)*360.E0/FLOAT(NLAT-1)
C - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - 
C     * PRINT OUT THE CROSS-SECTION.
C     * N TO S IF KZM=0, E TO W IF KZM=1. 
C     * MAX LEVELS PRINTED PER PAGE IS 10.
C 
  326 LL=1
  328 LR=LL+9 
C 
      IF(LR.GT.NLEV) LR=NLEV
      IF(KZM.EQ.0) WRITE(6,6033) (IPR(L),L=LL,LR) 
      IF(KZM.EQ.1) WRITE(6,6034) (IPR(L),L=LL,LR) 
      DO 330 M=1,NLAT 
      J=NLAT+1-M
      K=M 
      IF(KZM.EQ.0) K=NLAT+1-M 
  330 WRITE(6,6035) K, ANG(J),(XS(L,J),L=LL,LR) 
      LL=LR+1 
      IF(LL.LE.NLEV) GO TO 328
      WRITE(6,6040) LABEL 
      WRITE(6,6025) IBUF
      GO TO 150 
C 
C     * E.O.F. ON INPUT.
C 
  900 CALL                                         XIT('ZXPRINT',0) 
  902 CALL                                         XIT('ZXPRINT',-103)
C---------------------------------------------------------------------
 5010 FORMAT(10X,1X,A4,2I5)                                                     A4
 5012 FORMAT(10A8)                                                              A4
 6010 FORMAT('0..ZXPRINT EOF LOOKING FOR',2X,A4)
 6025 FORMAT('0',A4,I10,1X,A4,I10,4I6)
 6033 FORMAT('1ROW   LAT',10I12)
 6034 FORMAT('1ROW   LON',10I12)
 6035 FORMAT(' ',I3,F6.1,1P10E12.4)
 6040 FORMAT('0 CROSS-SECTION...',10A8)
      END
