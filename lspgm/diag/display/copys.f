      PROGRAM COPYS 
C     PROGRAM COPYS (IN,       OUT,       OUTPUT,                       )       A2
C    1         TAPE1=IN, TAPE2=OUT, TAPE6=OUTPUT) 
C     -------------------------------------------                               A2
C                                                                               A2
C     JUL 03/03 - A.IRWIN,F.MAJAESS (REVISED FOR F90 AND LINUX PLATFORMS)       A2
C     JAN 26/93 - F. MAJAESS (ALLOW UP TO 124 CHARACTERS/LINE)                  
C     FEB 18/92 - E. CHAN   (FLAG UNITS 1 AND 2 FOR FORMATTED I/O IN CALL TO    
C                            JCLPNT)
C     JAN 29/92 - E. CHAN   (CONVERT HOLLERITH LITERALS TO ASCII)               
C     FEB 12/91 - F.MAJAESS (ALLOW 82 CHAR/LINE INSTEAD OF 80)                 
C     JUL 10/86 - J.HASLIP. (PAGINATION OMITTED)
C     DEC 06/83 - B.DUGAS.
C                                                                               A2
CCOPYS   - REFORMATS AN UPDATE SOURCE, PROGRAM OR PROCEDURE FILE        1  1    A1
C                                                                               A3
CAUTHOR  - B.DUGAS                                                              A3
C                                                                               A3
CPURPOSE - REFORMATS A CHARACTER FILE FOR PRINTING PURPOSE SUCH AS              A3
C          NUMBERING LINES AND WRITING PAGE SHIFT FOR THE PRINTER.              A3
C          A NEW PAGE IS STARTED EVERY 60 LINES.                                A3
C                                                                               A3
CINPUT FILE...                                                                  A3
C                                                                               A3
C      IN  = FILE TO BE REFORMATTED. IT CONTAINS EITHER UPDATE SOURCE DECKS     A3
C            PROGRAMS, SUBROUTINES OR PROCEDURES. THESE UNITS WILL START ON     A3
C            A NEW PAGE IN FILE OUT.                                            A3
C            ANY OTHER KIND OF INPUT IN FILE IN LEAD TO JUST NUMBERED LINES     A3
C            IN FILE OUT.                                                       A3
C                                                                               A3
COUTPUT FILE...                                                                 A3
C                                                                               A3
C      OUT = REFORMATTED FILE                                                   A3
C---------------------------------------------------------------------- 
C 
      IMPLICIT REAL (A-H,O-Z),
     +INTEGER (I-N)
      CHARACTER PROC*6,DEC*5,COM*8,SUB*14,PRO*14
      CHARACTER C1,C2*5,C3*3,C4*5,C5*110,CC1*8,CC2*14,CC3*6
      LOGICAL NEW 
C 
      DATA SUB,PRO/'      SUBROUTI','      PROGRAM '/ 
      DATA COM/'COMDECK '/
      DATA DEC/'DECK '/ 
      DATA PROC/'PROC. '/ 
      DATA LMAX/61/ 
C---------------------------------------------------------------------
      NFF=3 
      CALL JCLPNT(NFF,-1,-2,6)
      NC =0 
      NP =1 
      NU =1 
      NCU=1 
      WRITE (2,2002)
C 
C     * READ NEXT CARD, CHECK FIRST FEW LETTERS FOR DECK, PROC, 
C     * COMDECK, PROGRAM OR SUBROUTINE START. 
C     * COPY REFORMATED OUTPUT ON UNIT 2. 
C 
  100 READ (1,1000,END=999) C1,C2,C3,C4,C5
      CC1=C2//C3
      CC2=C1//C2//C3//C4
      CC3=C1//C2
      NEW = (C2.EQ.DEC.OR.CC1.EQ.COM.OR.CC2.EQ.PRO.OR.CC2.EQ.SUB) 
      NEW=(NEW.OR.CC3.EQ.PROC)
      IF (NEW.AND.NCU-2.GT.0) THEN
          NCU=1 
          NP =1 
          NU =NU+1
          WRITE (2,2001) NP,NCU,C1,C2,C3,C4,C5
      ELSE
          WRITE (2,2000) NCU,C1,C2,C3,C4,C5 
      ENDIF 
      NC =NC +1 
      NCU=NCU+1 
      GO TO 100 
C 
  999 IF (NC.EQ.0) CALL                            XIT('COPYS',-1)
      WRITE (6,6666) NC,NU
      CALL                                         XIT('COPYS',0) 
C---------------------------------------------------------------------
 1000 FORMAT(A1,A5,A3,A5,A110)
 2000 FORMAT(' ',I5,2X,A1,A5,A3,A5,A110)
 2001 FORMAT('1'/73X,'PAGE ',I2//I6,2X,A1,A5,A3,A5,A110)
 2002 FORMAT('1')
 6666 FORMAT('0 COPYS COPIED ',I5,' CARDS, IN ',I3,' UNIT(S).')
      END
