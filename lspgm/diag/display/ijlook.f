      PROGRAM IJLOOK
C     PROGRAM IJLOOK (GFILE,       INPUT,       OUTPUT,                 )       A2
C    1          TAPE1=GFILE, TAPE5=INPUT, TAPE6=OUTPUT) 
C     -------------------------------------------------                         A2
C                                                                               A2
C     JUL 03/03 - A.IRWIN,F.MAJAESS (REVISED FOR F90 AND LINUX PLATFORMS)       A2
C     JAN 14/93 - E. CHAN     (CONVERT CODED IBUF(4) TO ALTERNATE CODE          A2
C                              THAT IS MONOTONICALLY INCREASING)                A2 
C     JAN 29/92 - E. CHAN     (CONVERT HOLLERITH LITERALS TO ASCII)             A2
C     OCT 22/87 - M.LAZARE. - REVISE CALL TO GETFLD2 TO ALLOW FOR NON-GRIDS.    
C     FEB 14/84 - J-FRANCOIS FORTIN.
C                                                                               A2
CIJLOOK  - PRINTS TIME SERIES OF A WINDOW IN A GRID FILE                1  0 C  A1
C                                                                               A3
CAUTHOR  - R. LAPRISE                                                           A3
C                                                                               A3
CPURPOSE - PRINTS ALL THE INFORMATION CONTAINED IN GFILE PERTAINING TO          A3
C          VARIABLE NAME, IN THE WINDOW FROM I1 TO I2 AND FROM J1 TO J2         A3
C          BETWEEN TIMESTEPS NT1 AND NT2 BY INCREMENT OF INT, AT ALL            A3
C          LEVELS BETWEEN LV1 AND LV2.                                          A3
C                                                                               A3
CINPUT FILE...                                                                  A3
C                                                                               A3
C      GFILE = FILE CONTAINING A TIME SERIES OF GRIDS.                          A3
C 
CINPUT PARAMETERS...
C                                                                               A5
C      NT1,NT2     = FIRST AND LAST TIMESTEP NUMBERS TO BE PRINTED              A5
C      INT         = TIMESTEP INTERVAL TO BE PRINTED                            A5
C      LV1,LV2     = UPPER AND LOWER LEVELS TO BE PRINTED                       A5
C      NAME        = VARIABLE TO BE PRINTED (4H ALL,4H  -1 PRINTS ALL)          A5
C      I1,J1,I2,J2 = VALUES DEFINING WINDOW TO BE PRINTED                       A5
C      IRES        = OUTPUT RESOLUTION 'HI' FOR 5 NUMBER PER LINE AND           A5
C                    'LO' FOR 12 NUMBERS PER LINE.                              A5
C                                                                               A5
C             NOTE : -1 FOR LV, I OR J GIVE EVERYTHING.                         A5
C                                                                               A5
CEXAMPLE OF INPUT CARDS...                                                      A5
C                                                                               A5
C*  IJLOOK             24        48    1       100  850      VORT               A5
C* 24   30   24   32 HI                                                         A5
C------------------------------------------------------------------------ 
C 
      use diag_sizes, only : SIZES_LONP1xLAT,
     &                       SIZES_LONP1xLATxNWORDIO

      IMPLICIT REAL (A-H,O-Z),
     +INTEGER (I-N)
      COMMON/ICOM/ IBUF(8),IDAT(SIZES_LONP1xLATxNWORDIO) 
      COMMON// FIELD(SIZES_LONP1xLAT)
      LOGICAL OK, HIGH
      CHARACTER*2 IRES
C 
      DATA MAXX/SIZES_LONP1xLATxNWORDIO/
C ------------------------------------------------------------------- 
      NFF=3 
      CALL JCLPNT(NFF,1,5,6)
   50 REWIND 1
      READ (5,5010,END=900) NT1,NT2,INT,LV1,LV2,NAME                            A4
      READ (5,5020,END=904) I1,J1,I2,J2,IRES                                    A4
      IF (I1 .EQ. -1 .AND. J1 .EQ. -1) THEN 
         I1 = 1 
         J1 = 1 
      ENDIF 
      IF (I1 .LE. 0 .OR. J1 .LE. 0) CALL           XIT('IJLOOK',-1) 
      IF (IRES .EQ. '-1') IRES = 'LO' 
      IF (IRES .NE. 'LO' .AND. IRES.NE.'HI') CALL  XIT('IJLOOK',-2) 
      IF (INT .LE. 0) INT = 1 
      NT2 = MAX (NT2,NT1)
      HIGH = (IRES .EQ. 'HI') 
C 
  100 CALL GETFLD2 (1,FIELD,-1,-1,-1,-1,IBUF,MAXX,OK)
      IF (.NOT. OK) GO TO 50
      IF (I2 .EQ. -1 .AND. J2 .EQ. -1) THEN 
         I2 = IBUF(5) 
         J2 = IBUF(6) 
      ENDIF 
      WRITE(6,6020) I1,J1,I2,J2 
      IF (I2 .LE. 0 .OR.J2 .LE. 0) CALL            XIT('IJLOOK',-3) 
C 
      KOUNT = IBUF(2) 
      IF (NT1 .EQ. -1 .AND. NT2 .EQ. -1) GO TO 200
      IF (KOUNT .GT. NT2) GO TO 50
      IF (MOD (KOUNT-NT1,INT) .NE. 0) GO TO 100 
C 
  200 NOM = IBUF(3) 
      IF (NAME .EQ. NC4TO8("  -1")) GO TO 300
      IF (NAME .NE. NC4TO8(" ALL") .AND. NOM .NE. NAME) GO TO 100
C 
  300 LEVEL = IBUF(4) 
      IF (LV1 .EQ. -1 .AND. LV2 .EQ. -1) GO TO 400
C
C     * CONVERT CODED LEVEL INFORMATION INTO ALTERNATE CODE THAT IS 
C     * MONOTONICALLY INCREASING.
C
      CALL LVACODE(LV1,LV1,1)
      CALL LVACODE(LV2,LV2,1)
      CALL LVACODE(LEVEL,LEVEL,1)
C
      IF (LEVEL .LT. LV1 .OR. LEVEL .GT. LV2) GO TO 100 
C 
  400 ILON = IBUF(5)
      ILAT = IBUF(6)
      I1 = MIN (I1,IBUF(5))
      I2 = MIN (I2,IBUF(5))
      J1 = MIN (J1,IBUF(6))
      J2 = MIN (J2,IBUF(6))
C 
      WRITE(6,6000) 
      WRITE(6,6010) 
      WRITE(6,6030) IBUF,IRES 
      CALL PRINTA (FIELD,ILON,ILAT,I1,J1,I2,J2,HIGH)
      GO TO 100 
C 
C     * E-O-F ON INPUT. 
C 
  900 CALL                                         XIT('IJLOOK',0)
  904 CALL                                         XIT('IJLOOK',-4) 
C ------------------------------------------------------------------- 
 5010 FORMAT (15X,2I10,I5,5X,2I5,5X,1X,A4)                                      A4
 5020 FORMAT (4I5,1X,A2)                                                        A4
 6000 FORMAT (//,1X,'KIND',3X,' STEP',3X,' NAME',6X,' LEVEL',
     1        '  NX','   NY','  KHEM',' PACK',' RES')
 6010 FORMAT (1X,56('-'))
 6020 FORMAT (1X,' FROM (',I3,',',I3,')',' TO (',I3,
     1        ',',I3,')',/)
 6030 FORMAT (1X,A4,1X,I10,1X,A4,1X,I10,4I5,3X,A2,///)
      END
