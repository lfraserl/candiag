      PROGRAM KE
C     PROGRAM KE (UIN,       VIN,       KEOUT,       OUTPUT,            )       C2
C    1      TAPE1=UIN, TAPE2=VIN, TAPE3=KEOUT, TAPE6=OUTPUT)
C     ------------------------------------------------------                    C2
C                                                                               C2
C     JUL 03/03 - A.IRWIN,F.MAJAESS (REVISED FOR F90 AND LINUX PLATFORMS)       C2
C     JAN 29/92 - E. CHAN  (CONVERT HOLLERITH LITERALS TO ASCII)                
C     MAY 12/83 - R.LAPRISE.                                                    
C     JUL 08/80 - J.D.HENDERSON 
C                                                                               C2
CKE      - COMPUTES KINETIC ENERGY FROM WIND COMPONENTS                 2  1    C1
C                                                                               C3
CAUTHOR  - J.D.HENDERSON                                                        C3
C                                                                               C3
CPURPOSE - FILE COMPUTATION OF KINETIC ENERGY FROM THE TWO WIND COMPONENTS      C3
C          KEOUT = (UIN**2 + VIN**2)/2.                                         C3
C          NOTE - THE WIND COMPONENTS FILES UIN AND VIN MUST BE REAL.           C3
C                                                                               C3
CINPUT FILES...                                                                 C3
C                                                                               C3
C      UIN   = CONTAINS U WIND COMPONENT                                        C3
C      VIN   = CONTAINS V WIND COMPONENT                                        C3
C                                                                               C3
COUTPUT FILE...                                                                 C3
C                                                                               C3
C      KEOUT = (UIN**2 + VIN**2)/2.                                             C3
C---------------------------------------------------------------------------- 
C 
      use diag_sizes, only : SIZES_LONP1xLAT,
     &                       SIZES_LONP1xLATxNWORDIO

      IMPLICIT REAL (A-H,O-Z),
     +INTEGER (I-N)
      LOGICAL OK,SPEC 
      COMMON/BLANCK/U(SIZES_LONP1xLAT),V(SIZES_LONP1xLAT) 
C 
      COMMON/ICOM/IBUF(8),IDAT(SIZES_LONP1xLATxNWORDIO)
      COMMON/JCOM/JBUF(8),JDAT(SIZES_LONP1xLATxNWORDIO)
      DATA MAXX/SIZES_LONP1xLATxNWORDIO/
C---------------------------------------------------------------------
      NFF=4 
      CALL JCLPNT(NFF,1,2,3,6)
      REWIND 1
      REWIND 2
      REWIND 3
C 
C     * READ THE NEXT PAIR OF FIELDS. 
C 
      NR=0
  150 CALL GETFLD2(1,U,-1,0,0,0,IBUF,MAXX,OK)
      IF(.NOT.OK)THEN 
        IF(NR.EQ.0) CALL                           XIT('KE',-1) 
        WRITE(6,6025) IBUF
        WRITE(6,6010) NR
        CALL                                       XIT('KE',0)
      ENDIF 
C 
      CALL GETFLD2(2,V,-1,0,0,0,JBUF,MAXX,OK)
      IF(.NOT.OK) CALL                             XIT('KE',-2) 
C 
C     * MAKE SURE THAT THE FIELDS ARE THE SAME KIND AND SIZE. 
C 
      CALL CMPLBL(0,IBUF,0,JBUF,OK) 
      IF(.NOT.OK)THEN 
        WRITE(6,6025) IBUF,JBUF 
        CALL                                       XIT('KE',-3) 
      ENDIF 
C 
C     * DETERMINE SIZE OF THE FIELD.
C 
      KIND=IBUF(1)
      SPEC=(KIND.EQ.NC4TO8("SPEC") .OR. KIND.EQ.NC4TO8("FOUR"))
      NWDS=IBUF(5)*IBUF(6)
      IF(SPEC) NWDS=NWDS*2
C 
C     * COMPUTE KE. 
C 
      DO 210 I=1,NWDS 
  210 U(I)=0.5E0*(U(I)**2+V(I)**2) 
C 
C     * SAVE THE RESULT.
C 
      IBUF(3)=NC4TO8("  KE")
      CALL PUTFLD2(3,U,IBUF,MAXX)
      NR=NR+1 
      GO TO 150 
C---------------------------------------------------------------------
 6010 FORMAT('0   KE READ',I6,' RECORDS')
 6025 FORMAT(' ',A4,I10,2X,A4,I10,4I6)
      END
