      PROGRAM PSDIV 
C     PROGRAM PSDIV (IN1,       IN2,       OUT,       OUTPUT,           )       C2
C    1         TAPE1=IN1, TAPE2=IN2, TAPE3=OUT, TAPE6=OUTPUT) 
C     -------------------------------------------------------                   C2
C                                                                               C2
C     JUL 03/03 - A.IRWIN,F.MAJAESS (REVISED FOR F90 AND LINUX PLATFORMS)       C2
C     APR 23/93 - E. CHAN  (ADD ERROR EXIT IF FIRST FILE IS EMPTY)              
C     JAN 29/92 - E. CHAN  (CONVERT HOLLERITH LITERALS TO ASCII) 
C     MAY 13/83 - R.LAPRISE.                                                    
C                                                                               C2
CPSDIV   - DIVIDES A FILE OF REAL SETS BY A ONE-LEVEL REAL FILE         2  1    C1
C                                                                               C3
CAUTHOR  - J.D.HENDERSON                                                        C3
C                                                                               C3
CPURPOSE - DIVIDES A FILE OF REAL MULTI-LEVEL SETS (IN2) BY A FILE OF           C3
C          REAL ONE LEVEL FIELDS (IN1). EACH ONE LEVEL FIELD DIVIDES            C3
C          AN ENTIRE SET FROM THE MULTI-LEVEL FILE.                             C3
C          NOTE - BOTH INPUT FILES IN1 AND IN2 MUST BE REAL AND THERE           C3
C                 MUST BE ONE SET IN IN2 FOR EACH FIELD IN IN1.                 C3
C                                                                               C3
CINPUT FILES...                                                                 C3
C                                                                               C3
C      IN1 = FILE OF ONE LEVEL REAL FIELDS.                                     C3
C      IN2 = FILE OF MULTI-LEVEL REAL SETS.                                     C3
C                                                                               C3
COUTPUT FILE...                                                                 C3
C                                                                               C3
C      OUT = THE QUOTIENT                                                       C3
C-------------------------------------------------------------------------- 
C 
      use diag_sizes, only : SIZES_LONP1xLAT,
     &                       SIZES_LONP1xLATxNWORDIO,
     &                       SIZES_MAXLEV

      IMPLICIT REAL (A-H,O-Z),
     +INTEGER (I-N)

      COMMON/BLANCK/F(SIZES_LONP1xLAT),G(SIZES_LONP1xLAT) 
C 
      LOGICAL OK,SPEC 
      INTEGER LEV(SIZES_MAXLEV),JBUF(8)
      COMMON/ICOM/IBUF(8),IDAT(SIZES_LONP1xLATxNWORDIO)
      DATA MAXX/SIZES_LONP1xLATxNWORDIO/, MAXL/SIZES_MAXLEV/ 
C-----------------------------------------------------------------------
      NFF=4 
      CALL JCLPNT(NFF,1,2,3,6)
      REWIND 1
      REWIND 2
      REWIND 3
      NRECS=0 
C 
C     * GET THE NUMBER OF LEVELS IN THE MULTI-LEVEL FILE. 
C 
      CALL FILEV(LEV,NLEV,IBUF,2) 
      IF((NLEV.LT.1).OR.(NLEV.GT.MAXL)) CALL       XIT('PSDIV',-1)
      NPTS=IBUF(5)*IBUF(6)
      REWIND 2
C 
C     * GET THE NEXT FIELD FROM FILE 1. 
C 
  100 CALL GETFLD2(1,G,-1,-1,-1,-1,IBUF,MAXX,OK) 
      IF(.NOT.OK)THEN 
        IF (NRECS.EQ.0) CALL                       XIT('PSDIV',-2)
        WRITE(6,6010) NRECS,NLEV
        CALL                                       XIT('PSDIV',0) 
      ENDIF 
      IF(NRECS.EQ.0) WRITE(6,6025) IBUF 
C 
C     * STOP IF THIS FIELD IS COMPLEX.
C 
      KIND=IBUF(1)
      SPEC=(KIND.EQ.NC4TO8("SPEC").OR.KIND.EQ.NC4TO8("FOUR"))
      IF(SPEC)THEN
        WRITE(6,6025) IBUF
        CALL                                       XIT('PSDIV',-3)
      ENDIF 
      DO 110 I=1,8
  110 JBUF(I)=IBUF(I) 
C 
C     * READ NEXT SET ONE LEVEL AT A TIME. STOP IF WRONG KIND OR SIZE.
C 
      DO 300 I=1,NLEV 
      CALL GETFLD2(2,F,KIND,-1,-1,-1,IBUF,MAXX,OK) 
      IF(.NOT.OK) CALL                             XIT('PSDIV',-4)
      CALL CMPLBL(0,IBUF,0,JBUF,OK) 
      IF(.NOT.OK)THEN 
        WRITE(6,6025) IBUF
        CALL                                       XIT('PSDIV',-5)
      ENDIF 
      IF(NRECS.EQ.0) WRITE(6,6025) IBUF 
C 
C     * DIVIDE   THIS LEVEL OF THE SET BY THE ONE LEVEL FIELD.
C     * SAVE THE RESULT ON FILE 3.
C 
      DO 250 J=1,NPTS 
  250 F(J)=F(J)/G(J)
  300 CALL PUTFLD2(3,F,IBUF,MAXX)
C 
      NRECS=NRECS+1 
      GO TO 100 
C-----------------------------------------------------------------------
 6010 FORMAT('0   PSDIV DIVIDED',I6,' SETS TO',I3,' LEVELS')
 6025 FORMAT(' ',A4,I10,1X,A4,I10,4I6)
      END
