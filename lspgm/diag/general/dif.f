      PROGRAM DIF
C     PROGRAM DIF  (XIN,       XOUT,       INPUT,       OUTPUT,         )       C2
C    1        TAPE1=XIN, TAPE2=XOUT, TAPE5=INPUT, TAPE6=OUTPUT)
C     ---------------------------------------------------------                 C2
C                                                                               C2
C     JUL 03/03 - A.IRWIN,F.MAJAESS (REVISED FOR F90 AND LINUX PLATFORMS)       C2
C     APR 15/92 - E. CHAN  (MODIFY I/O FOR 2-RECORD FORMAT, AND                 C2
C                           CHANGE NAME TO DIF)                                 C2
C     JAN 29/92 - E. CHAN  (CONVERT HOLLERITH LITERALS TO ASCII)                
C     JAN 07/85 - B.DUGAS. (USE OF XXXXSCR FILE TO ACCUMULATE DATA)
C     MAY 06/83 - R.LAPRISE.
C                                                                               C2
CDIF     - DIFFERENCE OF TWO SETS IN THE SAME FILE (CAN DIV BY DT)      1  1 C  C1
C                                                                               C3
CAUTHOR  - J.D.HENDERSON                                                        C3
C                                                                               C3
CPURPOSE - SUBTRACTS TWO SETS IN A FILE XIN AS SELECTED ON A CARD AND           C3
C          OPTIONALLY DIVIDES BY THE TIME DIFFERENCE BETWEEN THEM.              C3
C                                                                               C3
CINPUT FILE...                                                                  C3
C                                                                               C3
C      XIN = FILE OF MULTI-LEVEL SETS (REAL OR COMPLEX)                         C3
C                                                                               C3
COUTPUT FILE...                                                                 C3
C                                                                               C3
C      XOUT = ONE SET WHICH IS THE DIFFERENCE BETWEEN THE TWO SPECIFIED         C3
C             INPUT SETS.                                                       C3
C
CINPUT PARAMETERS...
C                                                                               C5
C      NAME    = NAME OF THE FIELDS                                             C5
C      NT1,NT2 = FIRST AND SECOND TIMESTEP NUMBERS.                             C5
C                DATA AT TIME NT1 IS SUBTRACTED FROM TIME NT2.                  C5
C                NT2 MAY BE LESS THAN NT1 IF REQUIRED.                          C5
C                WHEN NT1 OR NT2 ARE NOT TIMESTEPS NUMBER OF                    C5
C                THE FILE,  THE NEAREST TIMESTEPS WITHIN THE PERIOD             C5
C                NT1 TO NT2 ARE USED.                                           C5
C                                                                               C5
C      DT      = NUMBER OF SECONDS BETWEEN SUCESSIVE TIMESTEPS IN THE FILE      C5
C                (IF DT=0. NO TIME DIFFERENCE DIVISION IS DONE)                 C5
C                                                                               C5
C      TCFLAG  = TIME CODE FLAG,  IF TIMESTEP IS OF THE FORM                    C5
C                YYMMDDHH,  TCFLAG=1,  OTHERWISE TCFLAG=0.                      C5
C                                                                               C5
CEXAMPLE OF INPUT CARD...                                                       C5
C                                                                               C5
C*DIF       PCP         0        72     1800.                                   C5
C----------------------------------------------------------------------
C
      use diag_sizes, only : SIZES_LONP1xLAT,
     &                       SIZES_LONP1xLATxNWORDIO,
     &                       SIZES_MAXLEV

      IMPLICIT REAL (A-H,O-Z),
     +INTEGER (I-N)


      COMMON /T1/ G1(SIZES_LONP1xLAT)
      COMMON /T2/ G2(SIZES_LONP1xLAT)
C
      LOGICAL OK,SPEC
      INTEGER LEV(SIZES_MAXLEV)
C
      COMMON/ICOM/IBUF(8),IDAT(SIZES_LONP1xLATxNWORDIO)
C
      DATA MAXX/SIZES_LONP1xLATxNWORDIO/, MAXL/SIZES_MAXLEV/ 
C--------------------------------------------------------------------
      NFF=4
      CALL JCLPNT(NFF,1,2,5,6)
      REWIND 1
      REWIND 2
C
C     * ASSIGN UNIT 3 TO XXXXSCR.
C
      OPEN(3,IOSTAT=IRTC,FILE='XXXXSCR',FORM='UNFORMATTED')
      IF (IRTC.NE.0) CALL                          XIT('   DIF',-1)
      REWIND 3
C
C     * FIND THE NUMBER OF LEVELS AND TYPE OF FILED.
C
      CALL FILEV(LEV,NLEV,IBUF,1)
      IF((NLEV.LT.1).OR.(NLEV.GT.MAXL)) CALL       XIT('   DIF',-2)
      REWIND 1
      KIND=IBUF(1)
      SPEC=(KIND.EQ.NC4TO8("FOUR").OR.KIND.EQ.NC4TO8("SPEC"))
      NWDS=IBUF(5)*IBUF(6)
      IF (SPEC) NWDS=NWDS*2
C
C     * READ A CARD IDENTIFYING THE SETS TO BE SUBTRACTED.
C     * DIFFERENCE IS TAKEN BETWEEN TIMESTEPS NT1 AND NT2.
C     * DT IS THE LENGTH OF A TIMESTEP IN SECONDS.
C     * IF TCFLAG IS NON-ZERO, NT1 AND NT2 CONTAIN TIME CODE,
C     * ASSUMED TO BE IN DESCENDING ORDER, SUCH AS YYMMDDHH.
C
      READ(5,5010,END=906) NAME,NT1,NT2,DT,TCFLAG                               C4
      WRITE(6,6005) NAME,NT1,NT2,DT,TCFLAG
C
C     * ENSURE SEQUENCE OF TIMESTEPS.
C
      SIGN=+1.E0
      IF(NT2.GE.NT1) GOTO 50
      NTHOLD=NT2
      NT2=NT1
      NT1=NTHOLD
      SIGN=-1.E0
C
C     * FIND THE FIRST SET AT OR AFTER NT1, SAVE IT IN A SCRATCH FILE.
C
   50 CALL RECGET(1,-1,-1, NAME,-1,IBUF,MAXX,OK)
      IF(.NOT.OK)THEN
        WRITE(6,6010)
        CALL                                       XIT('   DIF',-3)
      ENDIF
      NT=IBUF(2)
      IF(NT.LT.NT1) GO TO 50
      NT1=NT
      BACKSPACE 1
      BACKSPACE 1
C
      DO 60 L=1,NLEV
      CALL GETFLD2(1,G1,-1,-1,-1,LEV(L),IBUF,MAXX,OK)
      IF(.NOT.OK)THEN
        WRITE(6,6010)
        CALL                                       XIT('   DIF',-5-L)
      ENDIF
      WRITE(6,6025) IBUF
      IBUF(8)=1
      CALL PUTFLD2(3,G1,IBUF,MAXX)
   60 CONTINUE
      REWIND 3
C
C     * FIND THE LAST TIMESTEP AT OR BEFORE NT2, AND PUT IT INTO G2.
C
      NTPREV=NT1
   80 CALL RECGET(1,-1,-1, NAME,-1,IBUF,MAXX,OK)
      IF (.NOT.OK) THEN
        BACKSPACE 1
        NBACK=NLEV
        GO TO 85
      ENDIF
      NT=IBUF(2)
      IF(NT.LE.NT2)THEN
        NTPREV=NT
        GO TO 80
      ENDIF
      NBACK=NLEV+1
C
C     * WHEN ONE GETS HERE,  NT2 HAS JUST BEEN EXCEEDED, OR EOF REACHED.
C     * GO BACK TO PREVIOUS TIMESTEP NTPREV TO GET THE FIELD.
C
   85 NT2=NTPREV
      DO 90 N=1,NBACK
      BACKSPACE 1
   90 BACKSPACE 1
C
C     * DETERMINE THE TIME INTERVAL.
C     * IF DT=0. OR NT2-NT1=0 JUST COMPUTE AN ORDINARY DIFFERENCE.
C     * IF THE TIME CODE FLAG TCFLAG IS SET, INTERPRET NT1 AND NT2 IN
C     * THE FORM YYMMDDHH AND DIVIDE THE DIFFERENCE OF THE SETS BY
C     * THE TIME DIFFERENCE IN SECONDS.
C
      IDNT=NT2-NT1
      STEPS=FLOAT(IDNT)
      TIME=DT*STEPS
      CONST=1.E0
      IF(TIME.NE.0.E0) CONST=1.E0/TIME
C
      IF (TCFLAG .EQ. 0.E0) GO TO 95
      CALL TIMEDC(NSEC1,NT1)
      CALL TIMEDC(NSEC2,NT2)
      IDNT=NSEC2-NSEC1
      TIME=FLOAT(IDNT)
      CONST=1.E0
      IF(TIME.NE.0.E0.AND.DT.NE.0) CONST=1.E0/TIME
C
C     * DO CALCULATION ONE LEVEL AT A TIME.
C
   95 DO 220 L=1,NLEV
      CALL GETFLD2(3,G1,-1,-1,-1,LEV(L),IBUF,MAXX,OK)
      CALL GETFLD2(1,G2,-1,-1,-1,LEV(L),IBUF,MAXX,OK)
      IF(.NOT.OK)THEN
        WRITE(6,6010)
        CALL                                       XIT('   DIF',-4)
      ENDIF
      IF(L.EQ.NLEV) WRITE(6,6025) IBUF
      IBUF(2)=IDNT
C
C     * SUBTRACT THE FIRST TIMESTEP FROM THE SECOND.
C     * DIVIDE BY TIME DIFFERENCE AND PUT ON FILE 2.
C
      DO 210 I=1,NWDS
  210 G2(I)=SIGN*(G2(I)-G1(I))*CONST
C
      CALL PUTFLD2(2,G2,IBUF,MAXX)
  220 CONTINUE
C
      WRITE(6,6025) IBUF
      WRITE(6,6030) NLEV
C
      CALL                                         XIT('   DIF',0)
C
C     * E.O.F. ON INPUT.
C
  906 CALL                                         XIT('   DIF',-5)
C---------------------------------------------------------------------
 5010 FORMAT(10X,1X,A4,2I10,2F10.0)                                             C4
 6005 FORMAT('0  DIF  ON ',A4,2I10,2F10.0)
 6010 FORMAT('0  DIF  INPUT FIELD NOT FOUND')
 6025 FORMAT(' ',A4,I10,2X,A4,I10,4I6)
 6030 FORMAT('   LAST OF ',I5,' LEVELS')
      END
