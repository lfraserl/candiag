      PROGRAM LOGE
C     PROGRAM LOGE (XIN,       XOUT,       OUTPUT,                      )       C2
C    1        TAPE1=XIN, TAPE2=XOUT, TAPE6=OUTPUT)
C     --------------------------------------------                              C2
C                                                                               C2
C     JUL 03/03 - A.IRWIN,F.MAJAESS (REVISED FOR F90 AND LINUX PLATFORMS)       C2
C     JAN 29/92 - E. CHAN   (CONVERT HOLLERITH LITERALS TO ASCII)               
C     FEB 04/85 - F.ZWIERS. (DO LN FOR POSITIVE NUMBERS ONLY)                   
C     FEB 12/80 - J.D.HENDERSON 
C                                                                               C2
CLOGE    - NATURAL LOG OF A REAL FILE                                   1  1    C1
C                                                                               C3
CAUTHOR  - J.D.HENDERSON                                                        C3
C                                                                               C3
CPURPOSE - NATURAL LOGARITHM FILE COMPUTATION XOUT = LN(XIN).                   C3
C          NOTE - IF   XIN =< 0.   THEN   LN(XIN) = 0.0                         C3
C                 XIN FILE VALUES MUST NOT BE COMPLEX.                          C3
C                                                                               C3
CINPUT FILE...                                                                  C3
C                                                                               C3
C      XIN  = INPUT FILE OF REAL VALUES                                         C3
C                                                                               C3
COUTPUT FILE...                                                                 C3
C                                                                               C3
C      XOUT = LN(XIN)                                                           C3
C---------------------------------------------------------------------------- 
C 
      use diag_sizes, only : SIZES_BLONP1xBLAT,
     &                       SIZES_BLONP1xBLATxNWORDIO

      IMPLICIT REAL (A-H,O-Z),
     +INTEGER (I-N)
      COMMON/BLANCK/F(SIZES_BLONP1xBLAT)
C 
      LOGICAL OK,SPEC 
      COMMON/ICOM/IBUF(8),IDAT(SIZES_BLONP1xBLATxNWORDIO) 
      DATA MAXX/SIZES_BLONP1xBLATxNWORDIO/ 
C---------------------------------------------------------------------
      NFF=3 
      CALL JCLPNT(NFF,1,2,6)
      REWIND 1
      REWIND 2
C 
C     * READ THE NEXT GRID FROM FILE XIN. 
C 
      NR=0
  150 CALL GETFLD2(1,F,-1,0,0,0,IBUF,MAXX,OK)
      IF(.NOT.OK)THEN 
        IF(NR.EQ.0)THEN 
          CALL                                     XIT(' LOGE ',-1) 
        ELSE
          WRITE(6,6025) IBUF
          WRITE(6,6010) NR
          CALL                                     XIT(' LOGE ',0)
        ENDIF 
      ENDIF 
      IF(NR.EQ.0) WRITE(6,6025) IBUF
C 
C     * DETERMINE SIZE OF THE FIELD.
C 
      KIND=IBUF(1)
      SPEC=(KIND.EQ.NC4TO8("SPEC") .OR. KIND.EQ.NC4TO8("FOUR"))
      IF(SPEC) CALL                                XIT('LOGE',-2) 
C 
C     * PERFORM THE ALGEBRAIC OPERATION.
C 
      NWDS=IBUF(5)*IBUF(6)
      DO 205 I=1,NWDS 
         F(I)=MERGE(1.0E0,F(I),-F(I).GE.0.D0) 
  205 CONTINUE
      DO 210 I=1,NWDS 
         F(I)=LOG(F(I))
  210 CONTINUE
C 
C     * SAVE ON FILE XOUT.
C 
      CALL PUTFLD2(2,F,IBUF,MAXX)
      NR=NR+1 
      GO TO 150 
C---------------------------------------------------------------------
 6010 FORMAT('0  LOGE  READ',I6,' RECORDS')
 6025 FORMAT(' ',A4,I10,2X,A4,I10,4I6)
      END
