      PROGRAM FPOW
C     PROGRAM FPOW (XIN,       XOUT,       INPUT,       OUTPUT,         )       C2
C    1        TAPE1=XIN, TAPE2=XOUT, TAPE5=INPUT, TAPE6=OUTPUT) 
C     ---------------------------------------------------------                 C2
C                                                                               C2
C     JUL 03/03 - A.IRWIN,F.MAJAESS (REVISED FOR F90 AND LINUX PLATFORMS)       C2
C     JAN 29/92 - E. CHAN  (CONVERT HOLLERITH LITERALS TO ASCII)                
C     MAY 11/83 - R.LAPRISE.                                                    
C     FEB 19/80 - J.D.HENDERSON 
C                                                                               C2
CFPOW    - RAISES A REAL FILE TO A GIVEN POWER                          1  1 C  C1
C                                                                               C3
CAUTHOR  - J.D.HENDERSON                                                        C3
C                                                                               C3
CPURPOSE - RAISES REAL VALUES IN FILE XIN TO A SPECIFIED EXPONENT (EX)          C3
C          WHICH IS READ FROM A CARD AND PUTS THE RESULTS IN FILE XOUT.         C3
C          NOTE - XIN MUST NOT BE COMPLEX.                                      C3
C                 ZERO RAISED TO ANY POWER IS LEFT AS ZERO.                     C3
C                                                                               C3
CINPUT FILE...                                                                  C3
C                                                                               C3
C      XIN  = INPUT FILE                                                        C3
C                                                                               C3
COUTPUT FILE...                                                                 C3
C                                                                               C3
C      XOUT = XIN**EX                                                           C3
C 
CINPUT PARAMETERS...
C                                                                               C5
C      EX = EXPONENT                                                            C5
C                                                                               C5
CEXAMPLE OF INPUT CARD...                                                       C5
C                                                                               C5
C*    FPOW       2.5                                                            C5
C-----------------------------------------------------------------------------
C 
      use diag_sizes, only : SIZES_LONP1xLAT,
     &                       SIZES_LONP1xLATxNWORDIO

      IMPLICIT REAL (A-H,O-Z),
     +INTEGER (I-N)
      COMMON/BLANCK/F(SIZES_LONP1xLAT) 
C 
      LOGICAL OK,SPEC 
      COMMON/ICOM/IBUF(8),IDAT(SIZES_LONP1xLATxNWORDIO)
      DATA MAXX/SIZES_LONP1xLATxNWORDIO/
C---------------------------------------------------------------------
      NFF=4 
      CALL JCLPNT(NFF,1,2,5,6)
      REWIND 1
      REWIND 2
C 
C     * READ THE EXPONENT FROM A CARD.
C 
      READ(5,5010,END=903)  POW                                                 C4
      WRITE(6,6007) POW 
      APOW=ABS(POW) 
C 
C     * READ THE NEXT GRID FROM FILE XIN. 
C 
      NR=0
  150 CALL GETFLD2(1,F,-1,0,0,0,IBUF,MAXX,OK)
      IF(.NOT.OK)THEN 
        IF(NR.EQ.0)THEN 
          CALL                                     XIT('FPOW',-1) 
        ELSE
          WRITE(6,6025) IBUF
          WRITE(6,6010) NR
          CALL                                     XIT('FPOW',0)
        ENDIF 
      ENDIF 
      IF(NR.EQ.0) WRITE(6,6025) IBUF
C 
C     * DETERMINE SIZE OF THE FIELD.
C 
      KIND=IBUF(1)
      SPEC=(KIND.EQ.NC4TO8("SPEC") .OR. KIND.EQ.NC4TO8("FOUR"))
      IF(SPEC) CALL                                XIT('FPOW',-2) 
C 
C     * PERFORM THE ALGEBRAIC OPERATION.
C 
      NWDS=IBUF(5)*IBUF(6)
      DO 210 I=1,NWDS 
      IF(F(I).NE.0.E0) F(I)=F(I)**APOW
  210 CONTINUE
C 
C     * INVERT FOR NEGATIVE POWERS. 
C 
      IF(POW.GE.0.E0) GO TO 310 
      DO 250 I=1,NWDS 
      IF(F(I).NE.0.E0) F(I)=1.E0/F(I) 
  250 CONTINUE
C 
C     * SAVE ON FILE XOUT.
C 
  310 CALL PUTFLD2(2,F,IBUF,MAXX)
      NR=NR+1 
      GO TO 150 
C 
C     * E.O.F. ON INPUT.
C 
  903 CALL                                         XIT('FPOW',-3) 
C---------------------------------------------------------------------
 5010 FORMAT(10X, E10.0)                                                        C4
 6007 FORMAT('0 EXPONENT =',1PE12.4)
 6010 FORMAT('0  FPOW  READ',I6,' RECORDS')
 6025 FORMAT(' ',A4,I10,2X,A4,I10,4I6)
      END
