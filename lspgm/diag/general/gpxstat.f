      PROGRAM GPXSTAT
C     PROGRAM GPXSTAT (BETA,       U,       V,       W,                         C2
C    1                   X1,     ...,      GP,      XP,       OUTPUT)           C2
C    2          TAPE1 =BETA,TAPE2 =U,TAPE3 =V,TAPE4 =W,                         C2
C    3          TAPE11=  X1,...,    TAPE91=GP,TAPE92=XP,TAPE6=OUTPUT)           C2
C     ----------------------------------------------------------------          C2
C                                                                               C2
C     NOV 06/09 - S.KHARIN (FIX THE LOGIC BUG IN HANDLING LUVW PARAMETER).      C2
C     NOV 03/09 - S.KHARIN (ADD MORE OPTIONS TO "LUVW" INPUT PARAMETER)         C2
C     JUL 06/09 - F.MAJAESS (SWITCH "JCLPNT1" REFERENCES TO "JCLPNT")
C     MAR 26/09 - S.KHARIN (OPEN GP AND XP IN APPEND MODE)
C     FEB 09/09 - S.KHARIN (GET LEVEL NUMBER FROM THE FIRST TRACER X1).
C     NOV 17/08 - S.KHARIN
C                                                                               C2
CGPXSTAT - COMPUTES BASIC STATISTICS ON PRESSURE LEVELS.               84  2    C1
C                                                                               C3
CAUTHOR  - S.KHARIN                                                             C3
C                                                                               C3
CPURPOSE - COMPUTES TIME MEANS AND VARIANCES OF INPUT FILES ON PRESSURE LEVELS. C3
C                                                                               C3
CINPUT FILE...                                                                  C3
C                                                                               C3
C      BETA = TIME-AVERAGED BETA MASK FOR REPRESENTATIVE ZONAL AVERAGES         C3
C         U = U-VELOCTIY                                                        C3
C         V = V-VELOCITY                                                        C3
C        .W.= VERTICAL VELOCITY                                                 C3
C        X1 = FIRST TRACER,                                                     C3
C       ...   UP TO 80 TRACERS.                                                 C3
C                                                                               C3
COUTPUT FILE...                                                                 C3
C                                                                               C3
C        GP = TIME MEANS AND (CO)VARIANCES:                                     C3
C             MEAN U, V, W, DU/DT, DV/DT, DW/DT,                                C3
C             U"U", V"V", W"W", U"V", U"W", V"W".                               C3
C             AND FOR EACH TRACER X:                                            C3
C             MEAN X, DX/DT, X"X", X"U", X"V", X"W".                            C3
C        XP = REPESENTATIVE ZONAL AVERAGES:                                     C3
C             MEAN (U)R, (V)R, (W)R, (DU/DT)R, (DV/DT)R, (DW/DT)R,              C3
C             (U"U")R, (V"V")R, (W"W")R, (U"V")R, (U"W")R, (V"W")R,             C3
C             AND FOR EACH TRACER:                                              C3
C             (X)R,(DX/DT)R,(X"X")R,(X*X*)R,(X"V")R,(X*V*)R,(X"W")R,(X*W*)R.    C3
C                                                                               C3
C      NOTE1: COMPUTATION OF SOME STATISTICS IS CONTROLLED BY                   C3
C             INPUT CARD PARAMETERS                                             C3
C                                                                               C3
C      NOTE2: WHEN COMPUTING CROSS-COVARINCES X"U", X"V", ETC., TRACERS MUST BE C3
C             SAMPLED AS FREQUENTLY OR LESS FREQUENTLY AS WIND COMPONENTS.      C3
C             FOR EXAMPLE, ONCE-DAILY, 12-HOURLY, OR 6-HOURLY BOTH FOR WINDS    C3
C             AND TRACERS WOULD WORK. DAILY TRACERS AND 12-HOURLY OR 6-HOURLY   C3
C             WINDS WOULD ALSO WORK. HOWEVER, 12-HOURLY TRACERS AND DAILY WINDS C3
C             WOULDN'T WORK.                                                    C3
C                                                                               C3
C                                                                               C3
CINPUT PARAMETERS...
C                                                                               C5
C      LUVW = 1 COMPUTE STATISTICS WITH WIND COMPONENTS SUCH AS U"U", V"V",     C5
C               U"V", W"W", U"W",V"W" (THE LAST THREE ONLY WHEN LW=1), AND      C5
C               X"U", X"V", ETC.                                                C5
C           = 0 COMPUTE ONLY TRACER TRANSPORT STATISTICS X"U", X"V", ETC.       C5
C           =-1 DO NOT SAVE ANY STATISTICS WITH U,V AND W.                      C5
C       LW  = 1 COMPUTE STATISTICS WITH VERTICAL VELOCITY W SUCH AS W"W",       C5
C               U"W",V"W", AND X"W". OTHERWISE DO NOT COMPUTE.                  C5
C       LDT = 1 COMPUTE TIME DERIVATIVES, OTHERWISE DO NOT COMPUTE.             C5
C      LZON = 1 COMPUTE ZONAL AVERAGES. OTHERWISE DO NOT COMPUTE.               C5
C     LBETA = 1 USE BETA MASK FOR ZONAL AVERAGES. OTHERWISE COMPUTE SIMPLE      C5
C               ZONAL AVERAGES. BETA IS READ IN ONLY WHEN LZON=1 AND LBETA=1.   C5
C     LSTAT = 1 CALCULATE SECOND ORDER STATISTICS AND TIME DERIVATIVES.         C5
C               OTHERWISE SAVE ONLY TIME MEANS.                                 C5
C      DELT =   IF POSITIVE, MODEL TIME STEP IN SECONDS FOR COMPUTING           C5
C               TIME DERIVATIVE. OTHERWISE, TIME INCREMENTS ARE COMPUTED        C5
C               FROM IBUF(2) ASSUMING IT IS IN THE YYYYMMDDHH FORMAT.           C5
C                                                                               C5
CEXAMPLE OF INPUT CARD(S)...                                                    C5
C                                                                               C5
C*GPXSTAT     1    1    1    1    1    1     900.0                              C5
C (COMPUTE ALL STATISTICS)                                                      C5
C                                                                               C5
C*GPXSTAT     0    1    1    1    1    1     900.0                              C5
C (COMPUTE ONLY STATISTICS FOR TRACERS X1,... AND STATISTICS WITH WINDS)        C5
C                                                                               C5
C*GPXSTAT     1    1    1    1    1    0     900.0                              C5
C (COMPUTE ONLY TIME MEANS OF U,V,W,X1,... AND THEIR ZONAL AVERAGES)            C5
C-----------------------------------------------------------------------------
C
      use diag_sizes, only : SIZES_LONP1xLAT,
     &                       SIZES_LONP1xLATxNWORDIO,
     &                       SIZES_MAXLEV,
     &                       SIZES_MAXLEVxLONP1xLAT,
     &                       SIZES_ARGLEN

      IMPLICIT NONE


C     * ALLOCATABLE ARRAYS

C     * FOR 2-D VARIABLES (LON:LAT)

      REAL, ALLOCATABLE ::
     +     X(:),Y(:),Z(:)

C     * FOR 3-D VARIABLES (LON:LAT:LEV)

      REAL, ALLOCATABLE ::
     +     BETA(:),
     +     UAVG(:),UVAR(:),
     +     VAVG(:),VVAR(:),
     +     WAVG(:),WVAR(:),
     +     UPVP(:),UPWP(:),VPWP(:)

C     * FOR 2-D VARIABLES (LAT:LEV)

      REAL, ALLOCATABLE ::
     +     ZD(:),RZU(:),RZUS2(:),
     +     RZV(:),RZVS2(:),RZUVS(:),
     +     RZW(:),RZWS2(:),RZUWS(:),RZVWS(:),
     +     RZUU(:),RZVV(:),RZWW(:),
     +     RZUV(:),RZUW(:),RZVW(:)

      REAL ZDJ,ZUJ,ZVJ,ZWJ,
     +     ZUUJ,ZVVJ,ZUVJ,ZWWJ,ZUWJ,ZVWJ,
     +     ZDUJ,ZDVJ,ZDWJ,
     +     ZDUUJ,ZDVVJ,ZDUVJ,ZDWWJ,ZDUWJ,ZDVWJ

      INTEGER I,J,L,IJ,IL,NLG,NLGM,NLAT,NWDS,NLEV,NWDSLEV,NLATLEV,N,
     +     NSETS,NSETS0,NST,NS,LST,NT1,NT2,NSEC1,NSEC2,IDNT,K,ITIME,
     +     KIND,NC4TO8,NPACK,NAME,IOS

      INTEGER LUVW,LW,LDT,LZON,LBETA,LSTAT
      REAL DELT,DTI

      CHARACTER*80 LXAVG,LDXDT,LXPXP,LXPUP,LXPVP,LXPWP,
     +     LZXAVG,LZDXDT,LZXPXP,LZXSXS,LZXPUP,LZXSUS,
     +     LZXPVP,LZXSVS,LZXPWP,LZXSWS
      CHARACTER*4 NXAVG,NDXDT,NXPXP,NXPUP,NXPVP,NXPWP,
     +     NZXAVG,NZDXDT,NZXPXP,NZXSXS,NZXPUP,NZXSUS,
     +     NZXPVP,NZXSVS,NZXPWP,NZXSWS

      LOGICAL OK,SPEC,APPNDMD
      INTEGER LEV(SIZES_MAXLEV),JBUF(8)

      INTEGER IBL(28)
      CHARACTER SLABL*80
      EQUIVALENCE (IBL(9),SLABL)

      INTEGER MACHINE,INTSIZE
      COMMON /MACHTYP/ MACHINE,INTSIZE

      INTEGER NFF,NF,NFGP,NFXP,LFIL,NTRAC

      INTEGER       FD(100),IUA(100),IUST(100)
      CHARACTER(LEN=SIZES_ARGLEN) :: ARG(90),FILE
      CHARACTER*4 CNAM
C     * ARG - ARRAY WITH COMMAND LINE ARGUMENTS
      COMMON /JCLPNTA/ ARG
C     * IUA/IUST  - ARRAY KEEPING TRACK OF UNITS ASSIGNED/STATUS.
      COMMON /JCLPNTU/ IUA,IUST
C     * FD - ARRAY WITH FILE DESCRIPTORS.
      COMMON /JCLPNTC/ FD
C     * APPNDMD - LOGICAL SWITCH ALLOWING "APPEND" MODE FILE OPENING
C     *           =.TRUE. ; FILES OPENED IN "APPEND" MODE.
C     *           =.FALSE. (DEFAULT STANDARD "OPEN" BEHAVIOUR)
      COMMON /JCLPNTP/ APPNDMD

      INTEGER MAXX,MAXG,MAXL,MAXRSZ
      DATA MAXX,MAXG,MAXL,MAXRSZ 
     & /SIZES_LONP1xLATxNWORDIO,
     &  SIZES_MAXLEVxLONP1xLAT,
     &  SIZES_MAXLEV,
     &  SIZES_LONP1xLAT/

      INTEGER IBUF,IDAT
      COMMON/ICOM/IBUF(8),IDAT(SIZES_LONP1xLATxNWORDIO)
C---------------------------------------------------------------------

      NFF=88
      APPNDMD=.TRUE.
      CALL JCLPNT(NFF,1,2,3,4,
     +     11,12,13,14,15,16,17,18,19,20,
     +     21,22,23,24,25,26,27,28,29,30,
     +     31,32,33,34,35,36,37,38,39,40,
     +     41,42,43,44,45,46,47,48,49,50,
     +     51,52,53,54,55,56,57,58,59,60,
     +     61,62,63,64,65,66,67,68,69,70,
     +     71,72,73,74,75,76,77,78,79,80,
     +     81,82,83,84,85,86,87,88,89,90,91,92,5,6)
      NTRAC=NFF-8
      IF(NTRAC.LT.1)CALL                           XIT('GPXSTAT',-1)
      NFGP=NTRAC+11
      NFXP=NTRAC+12
      WRITE(6,'(A,I2)')' NTRAC=',NTRAC

      READ(5,5000,END=911) LUVW,LW,LDT,LZON,LBETA,LSTAT,DELT                    C3
      WRITE(6,6000) LUVW,LW,LDT,LZON,LBETA,LSTAT,DELT
C
C     * FIND THE NUMBER OF LEVELS FROM UNIT 11 (FIRST TRACER)
C
      CALL FILEV(LEV,NLEV,IBUF,11)
      CALL PRTLAB(IBUF)
      IF (NLEV.EQ.0) THEN
        WRITE(6,6010)
        CALL                                       XIT('GPXSTAT',-2)
      ENDIF
      IF (NLEV.GT.MAXL) CALL                       XIT('GPXSTAT',-3)
      NLG=IBUF(5)
      NLAT=IBUF(6)
      NLGM=NLG-1
      NWDS=NLG*NLAT
      NWDSLEV=NWDS*NLEV
      NLATLEV=NLAT*NLEV
      KIND=IBUF(1)
      IF (KIND.NE.NC4TO8('GRID')) CALL             XIT('GPXSTAT',-4)
      NAME=IBUF(3)
      WRITE(6,6020) NAME,NLEV,(LEV(L),L=1,NLEV)
      NPACK=MIN(2,IBUF(8))
      IF (NWDS.GT.MAXRSZ.OR.NWDS*NLEV.GT.MAXG)
     +     CALL                                    XIT('GPXSTAT',-5)
C
C     * ALLOCATE ARRAYS
C
      ALLOCATE(X(NWDS),Y(NWDS),Z(NWDS),
     +     BETA(NWDSLEV),
     +     UAVG(NWDSLEV),UVAR(NWDSLEV),
     +     VAVG(NWDSLEV),VVAR(NWDSLEV),
     +     WAVG(NWDSLEV),WVAR(NWDSLEV),
     +     UPVP(NWDSLEV),UPWP(NWDSLEV),VPWP(NWDSLEV),
     +     ZD(NLATLEV),RZU(NLATLEV),RZUS2(NLATLEV),
     +     RZV(NLATLEV),RZVS2(NLATLEV),RZUVS(NLATLEV),
     +     RZW(NLATLEV),RZWS2(NLATLEV),RZUWS(NLATLEV),RZVWS(NLATLEV),
     +     RZUU(NLATLEV),RZVV(NLATLEV),RZWW(NLATLEV),
     +     RZUV(NLATLEV),RZUW(NLATLEV),RZVW(NLATLEV))
C
C     * GET BETA
C     * (ONLY WHEN REPRESENTATIVE ZONAL AVERAGES ARE COMPUTED)
C
      IF(LZON.EQ.1)THEN
        IF(LBETA.EQ.1)THEN
          REWIND 1
          DO L=1,NLEV
            IJ=(L-1)*NWDS
            CALL GETFLD2(1,BETA(IJ+1),KIND,-1,-1,LEV(L),IBUF,MAXX,OK)
            IF(.NOT.OK) CALL                       XIT('GPXSTAT',-6)
          ENDDO
C
C         * COMPUTE ZONAL AVERAGES OF BETA
C
          DO L=1,NLEV
            IJ=(L-1)*NWDS
            IL=(L-1)*NLAT
            DO J=1,NLAT
              ZDJ=0.E0
              N=IJ+(J-1)*NLG
              DO I=1,NLGM
                ZDJ=ZDJ+BETA(N+I)
              ENDDO
              ZD(IL+J)=ZDJ
            ENDDO
          ENDDO
        ELSE
          DO I=1,NLEV*NLAT
            ZD(I)=FLOAT(NLGM)
          ENDDO
        ENDIF                   ! LBETA
      ENDIF                     ! LZON
C
C     * SKIP U/V/W STATISTICS IF LUVW.EQ.-1 OR
C     * LUVW.EQ.0.AND.LSTAT.EQ.0
C
      IF(LUVW.EQ.-1.OR.(LUVW.NE.1.AND.LSTAT.NE.1))GOTO 100
C
CCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCC
C
C     * CALCULATE TIME MEAN OF U AND DU/DT
C
CCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCC
C
      KIND=NC4TO8('GRID')
      REWIND 2
      DO L=1,NLEV
        IJ=(L-1)*NWDS
        CALL GETFLD2(2,X,KIND,-1,-1,LEV(L),IBUF,MAXX,OK)
        IF(.NOT.OK) CALL                           XIT('GPXSTAT',-7)
        CALL PRTLAB(IBUF)
C
C       * STORE FIRST TIME STEP (USE UAVG FOR MEAN AND UPVP FOR DU/DT)
C
        DO I=1,NWDS
          UAVG(IJ+I)=X(I)
          UPVP(IJ+I)=X(I)
        ENDDO
      ENDDO
      NT1=IBUF(2)
C
C     * TIMESTEP AND LEVEL LOOPS.
C
      NSETS=1
 200  CONTINUE
      DO L=1,NLEV
        IJ=(L-1)*NWDS
        CALL GETFLD2(2,X,KIND,-1,-1,LEV(L),IBUF,MAXX,OK)
        IF(.NOT.OK)THEN
          IF(L.NE.1)CALL                           XIT('GPXSTAT',-8)
          GOTO 210
        ENDIF
C
C       * ACCUMULATE MEAN AND STORE DU/DT IN UVAR
C
        DO I=1,NWDS
          UAVG(IJ+I)=UAVG(IJ+I)+X(I)
          UVAR(IJ+I)=X(I)
        ENDDO
      ENDDO
      NSETS=NSETS+1
      GO TO 200
 210  CONTINUE
      NSETS0=NSETS
      WRITE(6,'(A,I10)')' NSETS=',NSETS
      NT2=IBUF(2)
C
C     * COMPLETE TIME MEAN CALCULATIONS
C
      DO I=1,NLEV*NWDS
        UAVG(I)=UAVG(I)/FLOAT(NSETS)
      ENDDO
C
C     * SAVE MEAN U (ONLY IF LUVW=1)
C
      IF(LUVW.EQ.1)THEN
      CALL SETLAB(IBL,NC4TO8('LABL'),0,NC4TO8('LABL'),0,10,1,0,1)
      SLABL='    U'
      CALL FBUFOUT(NFGP,IBL,10*MACHINE+8,K)
      CALL SETLAB(IBUF,KIND,NSETS,NC4TO8('   U'),-1,NLG,NLAT,-1,NPACK)
      DO L=1,NLEV
        IJ=(L-1)*NWDS
        DO I=1,NWDS
          X(I)=UAVG(IJ+I)
        ENDDO
        IBUF(4)=LEV(L)
        CALL PUTFLD2(NFGP,X,IBUF,MAXX)
      ENDDO
C
C     * COMPLETE DU/DT (IN UPVP) CALCULATIONS
C     * (ONLY WHEN REQUESTED)
C
      IF(LDT.EQ.1.AND.LSTAT.EQ.1)THEN
      IF(DELT.GT.0.E0)THEN
        IDNT=NT2-NT1
        IF(IDNT.NE.0)THEN
          DTI=1.E0/(DELT*FLOAT(IDNT))
        ELSE
          DTI=1.E0
        ENDIF
      ELSE
        CALL TIMEDC(NSEC1,NT1)
        CALL TIMEDC(NSEC2,NT2)
        IDNT=NSEC2-NSEC1
        IF(IDNT.NE.0)THEN
          DTI=1.E0/FLOAT(IDNT)
        ELSE
          DTI=1.E0
        ENDIF
      ENDIF                     ! DELT
      DO I=1,NLEV*NWDS
        UPVP(I)=(UVAR(I)-UPVP(I))*DTI
      ENDDO
C
C     * SAVE DU/DT (STORED IN UPVP)
C
      CALL SETLAB(IBL,NC4TO8('LABL'),0,NC4TO8('LABL'),0,10,1,0,1)
      SLABL='    DU/DT'
      CALL FBUFOUT(NFGP,IBL,10*MACHINE+8,K)
      CALL SETLAB(IBUF,KIND,IDNT,NC4TO8('  U.'),-1,NLG,NLAT,-1,NPACK)
      DO L=1,NLEV
        IJ=(L-1)*NWDS
        DO I=1,NWDS
          X(I)=UPVP(IJ+I)
        ENDDO
        IBUF(4)=LEV(L)
        CALL PUTFLD2(NFGP,X,IBUF,MAXX)
      ENDDO
      ENDIF                     ! LDT & LSTAT
C
C     * ZONAL AVERAGES OF UAVG
C
      IF(LZON.EQ.1)THEN
      KIND=NC4TO8('ZONL')
      DO L=1,NLEV
        IJ=(L-1)*NWDS
        IL=(L-1)*NLAT
        DO J=1,NLAT
          ZUJ =0.E0
          ZDUJ=0.E0
          N=IJ+(J-1)*NLG
          DO I=1,NLGM
            ZUJ =ZUJ +UAVG(N+I)
            ZDUJ=ZDUJ+UAVG(N+I)*BETA(N+I)
          ENDDO
          IF(ZD(IL+J).GT.0.E0)THEN
            RZU(IL+J)=ZDUJ/ZD(IL+J)
          ELSE
            RZU(IL+J)=ZUJ/FLOAT(NLGM)
          ENDIF
        ENDDO
      ENDDO
C
C     * SAVE ZONAL (U)R
C
      CALL SETLAB(IBL,NC4TO8('LABL'),0,NC4TO8('LABL'),0,10,1,0,1)
      SLABL='    (U)R'
      CALL FBUFOUT(NFXP,IBL,10*MACHINE+8,K)
      CALL SETLAB(IBUF,KIND,NSETS,NC4TO8('   U'),-1,NLAT,1,-1,NPACK)
      DO L=1,NLEV
        IL=(L-1)*NLAT
        DO I=1,NLAT
          X(I)=RZU(IL+I)
        ENDDO
        IBUF(4)=LEV(L)
        CALL PUTFLD2(NFXP,X,IBUF,MAXX)
      ENDDO
C
C     * ZONAL AVERAGES OF DU/DT (IN UPVP)
C
      IF(LDT.EQ.1.AND.LSTAT.EQ.1)THEN
      DO L=1,NLEV
        IJ=(L-1)*NWDS
        IL=(L-1)*NLAT
        DO J=1,NLAT
          ZUJ =0.E0
          ZDUJ=0.E0
          N=IJ+(J-1)*NLG
          DO I=1,NLGM
            ZUJ =ZUJ +UPVP(N+I)
            ZDUJ=ZDUJ+UPVP(N+I)*BETA(N+I)
          ENDDO
          IF(ZD(IL+J).GT.0.E0)THEN
            RZUU(IL+J)=ZDUJ/ZD(IL+J)
          ELSE
            RZUU(IL+J)=ZUJ/FLOAT(NLGM)
          ENDIF
        ENDDO
      ENDDO
C
C     * SAVE (DU/DT)R
C
      CALL SETLAB(IBL,NC4TO8('LABL'),0,NC4TO8('LABL'),0,10,1,0,1)
      SLABL='    (DU/DT)R'
      CALL FBUFOUT(NFXP,IBL,10*MACHINE+8,K)
      CALL SETLAB(IBUF,KIND,IDNT,NC4TO8('  U.'),-1,NLAT,1,-1,NPACK)
      DO L=1,NLEV
        IL=(L-1)*NLAT
        DO I=1,NLAT
          X(I)=RZUU(IL+I)
        ENDDO
        IBUF(4)=LEV(L)
        CALL PUTFLD2(NFXP,X,IBUF,MAXX)
      ENDDO
      ENDIF                     ! LDT & LSTAT
      ENDIF                     ! LZON
      ENDIF                     ! LUVW
C
CCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCC
C
C     * CALCULATE TIME MEAN OF V AND DV/DT
C
CCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCC
C
      KIND=NC4TO8('GRID')
      REWIND 3
      DO L=1,NLEV
        IJ=(L-1)*NWDS
        CALL GETFLD2(3,X,KIND,-1,-1,LEV(L),IBUF,MAXX,OK)
        IF(.NOT.OK) CALL                           XIT('GPXSTAT',-9)
        CALL PRTLAB(IBUF)
C
C       * STORE FIRST TIME STEP (USE VAVG FOR MEAN AND UPVP FOR DV/DT)
C
        DO I=1,NWDS
          VAVG(IJ+I)=X(I)
          UPVP(IJ+I)=X(I)
        ENDDO
      ENDDO
      NT1=IBUF(2)
C
C     * TIMESTEP AND LEVEL LOOPS.
C
      NSETS=1
 220  CONTINUE
      DO L=1,NLEV
        IJ=(L-1)*NWDS
        CALL GETFLD2(3,X,KIND,-1,-1,LEV(L),IBUF,MAXX,OK)
        IF(.NOT.OK)THEN
          IF(L.NE.1)CALL                           XIT('GPXSTAT',-10)
          GOTO 230
        ENDIF
C
C       * ACCUMULATE MEAN AND STORE DV/DT IN VVAR
C
        DO I=1,NWDS
          VAVG(IJ+I)=VAVG(IJ+I)+X(I)
          VVAR(IJ+I)=X(I)
        ENDDO
      ENDDO
      NSETS=NSETS+1
      GO TO 220
 230  CONTINUE
      WRITE(6,'(A,I10)')' NSETS=',NSETS
      NT2=IBUF(2)
      IF(NSETS.NE.NSETS0)CALL                      XIT('GPXSTAT',-11)
C
C     * COMPLETE TIME MEAN CALCULATIONS
C
      DO I=1,NLEV*NWDS
        VAVG(I)=VAVG(I)/FLOAT(NSETS)
      ENDDO
C
C     * ZONAL AVERAGES OF VAVG
C
      IF(LZON.EQ.1)THEN
      DO L=1,NLEV
        IJ=(L-1)*NWDS
        IL=(L-1)*NLAT
        DO J=1,NLAT
          ZVJ =0.E0
          ZDVJ=0.E0
          N=IJ+(J-1)*NLG
          DO I=1,NLGM
            ZVJ =ZVJ +VAVG(N+I)
            ZDVJ=ZDVJ+VAVG(N+I)*BETA(N+I)
          ENDDO
          IF(ZD(IL+J).GT.0.E0)THEN
            RZV(IL+J)=ZDVJ/ZD(IL+J)
          ELSE
            RZV(IL+J)=ZVJ/FLOAT(NLGM)
          ENDIF
        ENDDO
      ENDDO
      ENDIF
C
C     * SAVE MEAN V (ONLY IF LUVW=1)
C
      IF(LUVW.EQ.1)THEN
      CALL SETLAB(IBL,NC4TO8('LABL'),0,NC4TO8('LABL'),0,10,1,0,1)
      SLABL='    V'
      CALL FBUFOUT(NFGP,IBL,10*MACHINE+8,K)
      CALL SETLAB(IBUF,KIND,NSETS,NC4TO8('   V'),-1,NLG,NLAT,-1,NPACK)
      DO L=1,NLEV
        IJ=(L-1)*NWDS
        DO I=1,NWDS
          X(I)=VAVG(IJ+I)
        ENDDO
        IBUF(4)=LEV(L)
        CALL PUTFLD2(NFGP,X,IBUF,MAXX)
      ENDDO
C
C     * COMPLETE DV/DT (IN UPVP) CALCULATIONS
C     * (ONLY WHEN REQUESTED)
C
      IF(LDT.EQ.1.AND.LSTAT.EQ.1)THEN
      DO I=1,NLEV*NWDS
        UPVP(I)=(VVAR(I)-UPVP(I))*DTI
      ENDDO
C
C     * SAVE DV/DT (STORED IN UPVP)
C
      CALL SETLAB(IBL,NC4TO8('LABL'),0,NC4TO8('LABL'),0,10,1,0,1)
      SLABL='    DV/DT'
      CALL FBUFOUT(NFGP,IBL,10*MACHINE+8,K)
      CALL SETLAB(IBUF,KIND,IDNT,NC4TO8('  V.'),-1,NLG,NLAT,-1,NPACK)
      DO L=1,NLEV
        IJ=(L-1)*NWDS
        DO I=1,NWDS
          X(I)=UPVP(IJ+I)
        ENDDO
        IBUF(4)=LEV(L)
        CALL PUTFLD2(NFGP,X,IBUF,MAXX)
      ENDDO
      ENDIF                     ! LDT & LSTAT
C
C     * SAVE ZONAL (V)R
C
      IF(LZON.EQ.1)THEN
      KIND=NC4TO8('ZONL')
      CALL SETLAB(IBL,NC4TO8('LABL'),0,NC4TO8('LABL'),0,10,1,0,1)
      SLABL='    (V)R'
      CALL FBUFOUT(NFXP,IBL,10*MACHINE+8,K)
      CALL SETLAB(IBUF,KIND,NSETS,NC4TO8('   V'),-1,NLAT,1,-1,NPACK)
      DO L=1,NLEV
        IL=(L-1)*NLAT
        DO I=1,NLAT
          X(I)=RZV(IL+I)
        ENDDO
        IBUF(4)=LEV(L)
        CALL PUTFLD2(NFXP,X,IBUF,MAXX)
      ENDDO
C
C     * ZONAL AVERAGES OF DV/DT (IN UPVP)
C
      IF(LDT.EQ.1.AND.LSTAT.EQ.1) THEN
      DO L=1,NLEV
        IJ=(L-1)*NWDS
        IL=(L-1)*NLAT
        DO J=1,NLAT
          ZVJ =0.E0
          ZDVJ=0.E0
          N=IJ+(J-1)*NLG
          DO I=1,NLGM
            ZVJ =ZVJ +UPVP(N+I)
            ZDVJ=ZDVJ+UPVP(N+I)*BETA(N+I)
          ENDDO
          IF(ZD(IL+J).GT.0.E0)THEN
            RZVV(IL+J)=ZDVJ/ZD(IL+J)
          ELSE
            RZVV(IL+J)=ZVJ/FLOAT(NLGM)
          ENDIF
        ENDDO
      ENDDO
C
C     * SAVE (DV/DT)R
C
      CALL SETLAB(IBL,NC4TO8('LABL'),0,NC4TO8('LABL'),0,10,1,0,1)
      SLABL='    (DV/DT)R'
      CALL FBUFOUT(NFXP,IBL,10*MACHINE+8,K)
      CALL SETLAB(IBUF,KIND,IDNT,NC4TO8('  V.'),-1,NLAT,1,-1,NPACK)
      DO L=1,NLEV
        IL=(L-1)*NLAT
        DO I=1,NLAT
          X(I)=RZVV(IL+I)
        ENDDO
        IBUF(4)=LEV(L)
        CALL PUTFLD2(NFXP,X,IBUF,MAXX)
      ENDDO
      ENDIF                     ! LDT & LSTAT
      ENDIF                     ! LZON
      ENDIF                     ! LUVW

CCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCC
C
C     * CALCULATE TIME MEAN OF W AND DW/DT
C
CCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCC
C
      IF(LW.EQ.1)THEN
      KIND=NC4TO8('GRID')
      REWIND 4
      DO L=1,NLEV
        IJ=(L-1)*NWDS
        CALL GETFLD2(4,X,KIND,-1,-1,LEV(L),IBUF,MAXX,OK)
        IF(.NOT.OK) CALL                           XIT('GPXSTAT',-12)
        CALL PRTLAB(IBUF)
C
C       * STORE FIRST TIME STEP (USE WAVG FOR MEAN AND UPVP FOR DW/DT)
C
        DO I=1,NWDS
          WAVG(IJ+I)=X(I)
          UPVP(IJ+I)=X(I)
        ENDDO
      ENDDO
      NT1=IBUF(2)
C
C     * TIMESTEP AND LEVEL LOOPS.
C
      NSETS=1
 240  CONTINUE
      DO L=1,NLEV
        IJ=(L-1)*NWDS
        CALL GETFLD2(4,X,KIND,-1,-1,LEV(L),IBUF,MAXX,OK)
        IF(.NOT.OK)THEN
          IF(L.NE.1)CALL                           XIT('GPXSTAT',-13)
          GOTO 250
        ENDIF
C
C       * ACCUMULATE MEAN AND STORE W FOR DW/DT IN WVAR
C
        DO I=1,NWDS
          WAVG(IJ+I)=WAVG(IJ+I)+X(I)
          WVAR(IJ+I)=X(I)
        ENDDO
      ENDDO
      NSETS=NSETS+1
      GO TO 240
 250  CONTINUE
      WRITE(6,'(A,I10)')' NSETS=',NSETS
      NT2=IBUF(2)
      IF(NSETS.NE.NSETS0)CALL                      XIT('GPXSTAT',-14)
C
C     * COMPLETE TIME MEAN AND DW/DT (IN UPVP) CALCULATIONS
C
      DO I=1,NLEV*NWDS
        WAVG(I)=WAVG(I)/FLOAT(NSETS)
      ENDDO
C
C     * ZONAL AVERAGES FOR WAVG
C
      IF(LZON.EQ.1)THEN
      DO L=1,NLEV
        IJ=(L-1)*NWDS
        IL=(L-1)*NLAT
        DO J=1,NLAT
          ZWJ =0.E0
          ZDWJ=0.E0
          N=IJ+(J-1)*NLG
          DO I=1,NLGM
            ZWJ =ZWJ +WAVG(N+I)
            ZDWJ=ZDWJ+WAVG(N+I)*BETA(N+I)
          ENDDO
          IF(ZD(IL+J).GT.0.E0)THEN
            RZW(IL+J)=ZDWJ/ZD(IL+J)
          ELSE
            RZW(IL+J)=ZWJ/FLOAT(NLGM)
          ENDIF
        ENDDO
      ENDDO
      ENDIF
C
C     * SAVE MEAN W (ONLY IF LUVW=1)
C
      IF(LUVW.EQ.1)THEN
      CALL SETLAB(IBL,NC4TO8('LABL'),0,NC4TO8('LABL'),0,10,1,0,1)
      SLABL='    W'
      CALL FBUFOUT(NFGP,IBL,10*MACHINE+8,K)
      CALL SETLAB(IBUF,KIND,NSETS,NC4TO8('OMEG'),-1,NLG,NLAT,-1,NPACK)
      DO L=1,NLEV
        IJ=(L-1)*NWDS
        DO I=1,NWDS
          X(I)=WAVG(IJ+I)
        ENDDO
        IBUF(4)=LEV(L)
        CALL PUTFLD2(NFGP,X,IBUF,MAXX)
      ENDDO
C
C     * COMPLETE TIME MEAN AND DW/DT (IN UPVP) CALCULATIONS
C
      IF(LDT.EQ.1.AND.LSTAT.EQ.1)THEN
      DO I=1,NLEV*NWDS
        UPVP(I)=(WVAR(I)-UPVP(I))*DTI
      ENDDO
C
C     * SAVE DW/DT
C
      CALL SETLAB(IBL,NC4TO8('LABL'),0,NC4TO8('LABL'),0,10,1,0,1)
      SLABL='    DW/DT'
      CALL FBUFOUT(NFGP,IBL,10*MACHINE+8,K)
      CALL SETLAB(IBUF,KIND,IDNT,NC4TO8('  W.'),-1,NLG,NLAT,-1,NPACK)
      DO L=1,NLEV
        IJ=(L-1)*NWDS
        DO I=1,NWDS
          X(I)=UPVP(IJ+I)
        ENDDO
        IBUF(4)=LEV(L)
        CALL PUTFLD2(NFGP,X,IBUF,MAXX)
      ENDDO
      ENDIF                     ! LDT & LSTAT
C
C     * SAVE ZONAL (W)R
C
      IF(LZON.EQ.1)THEN
      KIND=NC4TO8('ZONL')
      CALL SETLAB(IBL,NC4TO8('LABL'),0,NC4TO8('LABL'),0,10,1,0,1)
      SLABL='    (W)R'
      CALL FBUFOUT(NFXP,IBL,10*MACHINE+8,K)
      CALL SETLAB(IBUF,KIND,NSETS,NC4TO8('   W'),-1,NLAT,1,-1,NPACK)
      DO L=1,NLEV
        IL=(L-1)*NLAT
        DO I=1,NLAT
          X(I)=RZW(IL+I)
        ENDDO
        IBUF(4)=LEV(L)
        CALL PUTFLD2(NFXP,X,IBUF,MAXX)
      ENDDO
C
C     * ZONAL AVERAGES OF DW/DT (IN UPVP)
C
      IF(LDT.EQ.1.AND.LSTAT.EQ.1) THEN
      DO L=1,NLEV
        IJ=(L-1)*NWDS
        IL=(L-1)*NLAT
        DO J=1,NLAT
          ZWJ =0.E0
          ZDWJ=0.E0
          N=IJ+(J-1)*NLG
          DO I=1,NLGM
            ZWJ =ZWJ +UPVP(N+I)
            ZDWJ=ZDWJ+UPVP(N+I)*BETA(N+I)
          ENDDO
          IF(ZD(IL+J).GT.0.E0)THEN
            RZWW(IL+J)=ZDWJ/ZD(IL+J)
          ELSE
            RZWW(IL+J)=ZWJ/FLOAT(NLGM)
          ENDIF
        ENDDO
      ENDDO
C
C     * SAVE (DW/DT)R
C
      CALL SETLAB(IBL,NC4TO8('LABL'),0,NC4TO8('LABL'),0,10,1,0,1)
      SLABL='    (DW/DT)R'
      CALL FBUFOUT(NFXP,IBL,10*MACHINE+8,K)
      CALL SETLAB(IBUF,KIND,IDNT,NC4TO8('  W.'),-1,NLAT,1,-1,NPACK)
      DO L=1,NLEV
        IL=(L-1)*NLAT
        DO I=1,NLAT
          X(I)=RZWW(IL+I)
        ENDDO
        IBUF(4)=LEV(L)
        CALL PUTFLD2(NFXP,X,IBUF,MAXX)
      ENDDO
      ENDIF                     ! LDT & LSTAT
      ENDIF                     ! LZON
      ENDIF                     ! LUVW
      ENDIF                     ! LW
C
CCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCC
C
C     * CALCULATE VARIANCE OF U,V,W, AND
C     * COVARIANCES UPVP,UPWP,VPWP
C
CCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCC
C
      IF(LUVW.EQ.1.AND.LSTAT.EQ.1)THEN
      DO I=1,NLEV*NWDS
        UVAR(I)=0.E0
        VVAR(I)=0.E0
        WVAR(I)=0.E0
        UPVP(I)=0.E0
        UPWP(I)=0.E0
        VPWP(I)=0.E0
      ENDDO
      KIND=NC4TO8('GRID')
      REWIND 2
      REWIND 3
      REWIND 4
      DO N=1,NSETS
        DO L=1,NLEV
          IJ=(L-1)*NWDS
          CALL GETFLD2(2,X,KIND,-1,-1,LEV(L),IBUF,MAXX,OK)
          IF(.NOT.OK) CALL                         XIT('GPXSTAT',-15)
          CALL GETFLD2(3,Y,KIND,-1,-1,LEV(L),IBUF,MAXX,OK)
          IF(.NOT.OK) CALL                         XIT('GPXSTAT',-16)
          DO I=1,NWDS
            UVAR(IJ+I)=UVAR(IJ+I)+(X(I)-UAVG(IJ+I))**2
            VVAR(IJ+I)=VVAR(IJ+I)+(Y(I)-VAVG(IJ+I))**2
            UPVP(IJ+I)=UPVP(IJ+I)+(X(I)-UAVG(IJ+I))*(Y(I)-VAVG(IJ+I))
          ENDDO
          IF(LW.EQ.1)THEN
            CALL GETFLD2(4,Z,KIND,-1,-1,LEV(L),IBUF,MAXX,OK)
            IF(.NOT.OK) CALL                       XIT('GPXSTAT',-17)
            DO I=1,NWDS
              WVAR(IJ+I)=WVAR(IJ+I)+(Z(I)-WAVG(IJ+I))**2
              UPWP(IJ+I)=UPWP(IJ+I)+(X(I)-UAVG(IJ+I))*(Z(I)-WAVG(IJ+I))
              VPWP(IJ+I)=VPWP(IJ+I)+(Y(I)-VAVG(IJ+I))*(Z(I)-WAVG(IJ+I))
            ENDDO
          ENDIF                 ! LW
        ENDDO
      ENDDO
C
C     * COMPLETE VARIANCE CALCULATIONS
C
      DO I=1,NLEV*NWDS
        UVAR(I)=UVAR(I)/FLOAT(NSETS)
        VVAR(I)=VVAR(I)/FLOAT(NSETS)
        UPVP(I)=UPVP(I)/FLOAT(NSETS)
      ENDDO
      IF(LW.EQ.1)THEN
      DO I=1,NLEV*NWDS
        WVAR(I)=WVAR(I)/FLOAT(NSETS)
        UPWP(I)=UPWP(I)/FLOAT(NSETS)
        VPWP(I)=VPWP(I)/FLOAT(NSETS)
      ENDDO
      ENDIF                     ! LW
C
C     * SAVE U"U"
C
      CALL SETLAB(IBL,NC4TO8('LABL'),0,NC4TO8('LABL'),0,10,1,0,1)
      SLABL='    U"U"'
      CALL FBUFOUT(NFGP,IBL,10*MACHINE+8,K)
      CALL SETLAB(IBUF,KIND,NSETS,NC4TO8('U"U"'),-1,NLG,NLAT,-1,NPACK)
      DO L=1,NLEV
        IJ=(L-1)*NWDS
        DO I=1,NWDS
          X(I)=UVAR(IJ+I)
        ENDDO
        IBUF(4)=LEV(L)
        CALL PUTFLD2(NFGP,X,IBUF,MAXX)
      ENDDO
C
C     * SAVE V"V"
C
      CALL SETLAB(IBL,NC4TO8('LABL'),0,NC4TO8('LABL'),0,10,1,0,1)
      SLABL='    V"V"'
      CALL FBUFOUT(NFGP,IBL,10*MACHINE+8,K)
      CALL SETLAB(IBUF,KIND,NSETS,NC4TO8('V"V"'),-1,NLG,NLAT,-1,NPACK)
      DO L=1,NLEV
        IJ=(L-1)*NWDS
        DO I=1,NWDS
          X(I)=VVAR(IJ+I)
        ENDDO
        IBUF(4)=LEV(L)
        CALL PUTFLD2(NFGP,X,IBUF,MAXX)
      ENDDO
C
C     * SAVE U"V"
C
      CALL SETLAB(IBL,NC4TO8('LABL'),0,NC4TO8('LABL'),0,10,1,0,1)
      SLABL='    U"V"'
      CALL FBUFOUT(NFGP,IBL,10*MACHINE+8,K)
      CALL SETLAB(IBUF,KIND,NSETS,NC4TO8('U"V"'),-1,NLG,NLAT,-1,NPACK)
      DO L=1,NLEV
        IJ=(L-1)*NWDS
        DO I=1,NWDS
          X(I)=UPVP(IJ+I)
        ENDDO
        IBUF(4)=LEV(L)
        CALL PUTFLD2(NFGP,X,IBUF,MAXX)
      ENDDO
C
C     * SAVE W"W"
C
      IF(LW.EQ.1)THEN
      CALL SETLAB(IBL,NC4TO8('LABL'),0,NC4TO8('LABL'),0,10,1,0,1)
      SLABL='    W"W"'
      CALL FBUFOUT(NFGP,IBL,10*MACHINE+8,K)
      CALL SETLAB(IBUF,KIND,NSETS,NC4TO8('W"W"'),-1,NLG,NLAT,-1,NPACK)
      DO L=1,NLEV
        IJ=(L-1)*NWDS
        DO I=1,NWDS
          X(I)=WVAR(IJ+I)
        ENDDO
        IBUF(4)=LEV(L)
        CALL PUTFLD2(NFGP,X,IBUF,MAXX)
      ENDDO
C
C     * SAVE U"W"
C
      CALL SETLAB(IBL,NC4TO8('LABL'),0,NC4TO8('LABL'),0,10,1,0,1)
      SLABL='    U"W"'
      CALL FBUFOUT(NFGP,IBL,10*MACHINE+8,K)
      CALL SETLAB(IBUF,KIND,NSETS,NC4TO8('U"W"'),-1,NLG,NLAT,-1,NPACK)
      DO L=1,NLEV
        IJ=(L-1)*NWDS
        DO I=1,NWDS
          X(I)=UPWP(IJ+I)
        ENDDO
        IBUF(4)=LEV(L)
        CALL PUTFLD2(NFGP,X,IBUF,MAXX)
      ENDDO
C
C     * SAVE V"W"
C
      CALL SETLAB(IBL,NC4TO8('LABL'),0,NC4TO8('LABL'),0,10,1,0,1)
      SLABL='    V"W"'
      CALL FBUFOUT(NFGP,IBL,10*MACHINE+8,K)
      CALL SETLAB(IBUF,KIND,NSETS,NC4TO8('V"W"'),-1,NLG,NLAT,-1,NPACK)
      DO L=1,NLEV
        IJ=(L-1)*NWDS
        DO I=1,NWDS
          X(I)=VPWP(IJ+I)
        ENDDO
        IBUF(4)=LEV(L)
        CALL PUTFLD2(NFGP,X,IBUF,MAXX)
      ENDDO
      ENDIF                     ! LW
C
C     * ZONAL AVERAGES FOR U"U", V"V",U"V"
C
      IF(LZON.EQ.1)THEN
      KIND=NC4TO8('ZONL')
      DO L=1,NLEV
        IJ=(L-1)*NWDS
        IL=(L-1)*NLAT
        DO J=1,NLAT
          ZUUJ =0.E0
          ZVVJ =0.E0
          ZUVJ =0.E0
          ZDUUJ=0.E0
          ZDVVJ=0.E0
          ZDUVJ=0.E0
          N=IJ+(J-1)*NLG
          DO I=1,NLGM
            ZUUJ =ZUUJ +UVAR(N+I)
            ZVVJ =ZVVJ +VVAR(N+I)
            ZUVJ =ZUVJ +UPVP(N+I)
            ZDUUJ=ZDUUJ+UVAR(N+I)*BETA(N+I)
            ZDVVJ=ZDVVJ+VVAR(N+I)*BETA(N+I)
            ZDUVJ=ZDUVJ+UPVP(N+I)*BETA(N+I)
          ENDDO
          IF(ZD(IL+J).GT.0.E0)THEN
            RZUU(IL+J)=ZDUUJ/ZD(IL+J)
            RZVV(IL+J)=ZDVVJ/ZD(IL+J)
            RZUV(IL+J)=ZDUVJ/ZD(IL+J)
          ELSE
            RZUU(IL+J)=ZUUJ/FLOAT(NLGM)
            RZVV(IL+J)=ZVVJ/FLOAT(NLGM)
            RZUV(IL+J)=ZUVJ/FLOAT(NLGM)
          ENDIF
        ENDDO
      ENDDO
C
C     * ZONAL STATIONARY EDDIES OF U AND V
C
      DO L=1,NLEV
        IJ=(L-1)*NWDS
        IL=(L-1)*NLAT
        DO J=1,NLAT
          ZUUJ=0.E0
          ZVVJ=0.E0
          ZUVJ=0.E0

          ZDUUJ=0.E0
          ZDVVJ=0.E0
          ZDUVJ=0.E0
          N=IJ+(J-1)*NLG
          DO I=1,NLGM
            ZUUJ =ZUUJ +(UAVG(N+I)-RZU(IL+J))**2
            ZVVJ =ZVVJ +(VAVG(N+I)-RZV(IL+J))**2
            ZUVJ =ZUVJ +(UAVG(N+I)-RZU(IL+J))*(VAVG(N+I)-RZV(IL+J))

            ZDUUJ=ZDUUJ+(UAVG(N+I)-RZU(IL+J))**2*BETA(N+I)
            ZDVVJ=ZDVVJ+(VAVG(N+I)-RZV(IL+J))**2*BETA(N+I)
            ZDUVJ=ZDUVJ+(UAVG(N+I)-RZU(IL+J))*(VAVG(N+I)-RZV(IL+J))*
     +           BETA(N+I)
          ENDDO
          IF(ZD(IL+J).GT.0.E0)THEN
            RZUS2(IL+J)=ZDUUJ/ZD(IL+J)
            RZVS2(IL+J)=ZDVVJ/ZD(IL+J)
            RZUVS(IL+J)=ZDUVJ/ZD(IL+J)
          ELSE
            RZUS2(IL+J)=ZUUJ/FLOAT(NLGM)
            RZVS2(IL+J)=ZVVJ/FLOAT(NLGM)
            RZUVS(IL+J)=ZUVJ/FLOAT(NLGM)
          ENDIF
        ENDDO
      ENDDO
C
C     * SAVE ZONAL (U"U")R
C
      CALL SETLAB(IBL,NC4TO8('LABL'),0,NC4TO8('LABL'),0,10,1,0,1)
      SLABL='    (U"U")R'
      CALL FBUFOUT(NFXP,IBL,10*MACHINE+8,K)
      CALL SETLAB(IBUF,KIND,NSETS,NC4TO8('U"U"'),-1,NLAT,1,-1,NPACK)
      DO L=1,NLEV
        IL=(L-1)*NLAT
        DO I=1,NLAT
          X(I)=RZUU(IL+I)
        ENDDO
        IBUF(4)=LEV(L)
        CALL PUTFLD2(NFXP,X,IBUF,MAXX)
      ENDDO
C
C     * SAVE ZONAL (U*U*)R
C
      CALL SETLAB(IBL,NC4TO8('LABL'),0,NC4TO8('LABL'),0,10,1,0,1)
      SLABL='    (U*U*)R'
      CALL FBUFOUT(NFXP,IBL,10*MACHINE+8,K)
      CALL SETLAB(IBUF,KIND,NSETS,NC4TO8('U*U*'),-1,NLAT,1,-1,NPACK)
      DO L=1,NLEV
        IL=(L-1)*NLAT
        DO I=1,NLAT
          X(I)=RZUS2(IL+I)
        ENDDO
        IBUF(4)=LEV(L)
        CALL PUTFLD2(NFXP,X,IBUF,MAXX)
      ENDDO
C
C     * SAVE ZONAL (V"V")R
C
      CALL SETLAB(IBL,NC4TO8('LABL'),0,NC4TO8('LABL'),0,10,1,0,1)
      SLABL='    (V"V")R'
      CALL FBUFOUT(NFXP,IBL,10*MACHINE+8,K)
      CALL SETLAB(IBUF,KIND,NSETS,NC4TO8('V"V"'),-1,NLAT,1,-1,NPACK)
      DO L=1,NLEV
        IL=(L-1)*NLAT
        DO I=1,NLAT
          X(I)=RZVV(IL+I)
        ENDDO
        IBUF(4)=LEV(L)
        CALL PUTFLD2(NFXP,X,IBUF,MAXX)
      ENDDO
C
C     * SAVE ZONAL (V*V*)R
C
      CALL SETLAB(IBL,NC4TO8('LABL'),0,NC4TO8('LABL'),0,10,1,0,1)
      SLABL='    (V*V*)R'
      CALL FBUFOUT(NFXP,IBL,10*MACHINE+8,K)
      CALL SETLAB(IBUF,KIND,NSETS,NC4TO8('V*V*'),-1,NLAT,1,-1,NPACK)
      DO L=1,NLEV
        IL=(L-1)*NLAT
        DO I=1,NLAT
          X(I)=RZVS2(IL+I)
        ENDDO
        IBUF(4)=LEV(L)
        CALL PUTFLD2(NFXP,X,IBUF,MAXX)
      ENDDO
C
C     * SAVE ZONAL (U"V")R
C
      CALL SETLAB(IBL,NC4TO8('LABL'),0,NC4TO8('LABL'),0,10,1,0,1)
      SLABL='    (U"V")R'
      CALL FBUFOUT(NFXP,IBL,10*MACHINE+8,K)
      CALL SETLAB(IBUF,KIND,NSETS,NC4TO8('U"V"'),-1,NLAT,1,-1,NPACK)
      DO L=1,NLEV
        IL=(L-1)*NLAT
        DO I=1,NLAT
          X(I)=RZUV(IL+I)
        ENDDO
        IBUF(4)=LEV(L)
        CALL PUTFLD2(NFXP,X,IBUF,MAXX)
      ENDDO
C
C     * SAVE ZONAL (U*V*)R
C
      CALL SETLAB(IBL,NC4TO8('LABL'),0,NC4TO8('LABL'),0,10,1,0,1)
      SLABL='    (U*V*)R'
      CALL FBUFOUT(NFXP,IBL,10*MACHINE+8,K)
      CALL SETLAB(IBUF,KIND,NSETS,NC4TO8('U*V*'),-1,NLAT,1,-1,NPACK)
      DO L=1,NLEV
        IL=(L-1)*NLAT
        DO I=1,NLAT
          X(I)=RZUVS(IL+I)
        ENDDO
        IBUF(4)=LEV(L)
        CALL PUTFLD2(NFXP,X,IBUF,MAXX)
      ENDDO
C
C     * ZONAL AVERAGES FOR W"W", U"W", V"W"
C
      IF(LW.EQ.1)THEN
      DO L=1,NLEV
        IJ=(L-1)*NWDS
        IL=(L-1)*NLAT
        DO J=1,NLAT
          ZWWJ =0.E0
          ZUWJ =0.E0
          ZVWJ =0.E0
          ZDWWJ=0.E0
          ZDUWJ=0.E0
          ZDVWJ=0.E0
          N=IJ+(J-1)*NLG
          DO I=1,NLGM
            ZWWJ =ZWWJ +WVAR(N+I)
            ZUWJ =ZUWJ +UPWP(N+I)
            ZVWJ =ZVWJ +VPWP(N+I)
            ZDWWJ=ZDWWJ+WVAR(N+I)*BETA(N+I)
            ZDUWJ=ZDUWJ+UPWP(N+I)*BETA(N+I)
            ZDVWJ=ZDVWJ+VPWP(N+I)*BETA(N+I)
          ENDDO
          IF(ZD(IL+J).GT.0.E0)THEN
            RZWW(IL+J)=ZDWWJ/ZD(IL+J)
            RZUW(IL+J)=ZDUWJ/ZD(IL+J)
            RZVW(IL+J)=ZDVWJ/ZD(IL+J)
          ELSE
            RZWW(IL+J)=ZWWJ/FLOAT(NLGM)
            RZUW(IL+J)=ZUWJ/FLOAT(NLGM)
            RZVW(IL+J)=ZVWJ/FLOAT(NLGM)
          ENDIF
        ENDDO
      ENDDO
C
C     * ZONAL STATIONARY EDDIES OF W*W*, U*W*, V*W*
C
      DO L=1,NLEV
        IJ=(L-1)*NWDS
        IL=(L-1)*NLAT
        DO J=1,NLAT
          ZWWJ=0.E0
          ZUWJ=0.E0
          ZVWJ=0.E0
          ZDWWJ=0.E0
          ZDUWJ=0.E0
          ZDVWJ=0.E0
          N=IJ+(J-1)*NLG
          DO I=1,NLGM
            ZWWJ=ZWWJ+(WAVG(N+I)-RZW(IL+J))**2
            ZUWJ=ZUWJ+(UAVG(N+I)-RZU(IL+J))*(WAVG(N+I)-RZW(IL+J))
            ZVWJ=ZVWJ+(VAVG(N+I)-RZV(IL+J))*(WAVG(N+I)-RZW(IL+J))
            ZDWWJ=ZDWWJ+(WAVG(N+I)-RZW(IL+J))**2*BETA(N+I)
            ZDUWJ=ZDUWJ+(UAVG(N+I)-RZU(IL+J))*(WAVG(N+I)-RZW(IL+J))*
     +           BETA(N+I)
            ZDVWJ=ZDVWJ+(VAVG(N+I)-RZV(IL+J))*(WAVG(N+I)-RZW(IL+J))*
     +           BETA(N+I)
          ENDDO
          IF(ZD(IL+J).GT.0.E0)THEN
            RZWS2(IL+J)=ZDWWJ/ZD(IL+J)
            RZUWS(IL+J)=ZDUWJ/ZD(IL+J)
            RZVWS(IL+J)=ZDVWJ/ZD(IL+J)
          ELSE
            RZWS2(IL+J)=ZWWJ/FLOAT(NLGM)
            RZUWS(IL+J)=ZUWJ/FLOAT(NLGM)
            RZVWS(IL+J)=ZVWJ/FLOAT(NLGM)
          ENDIF
        ENDDO
      ENDDO
C
C     * SAVE ZONAL (W"W")R
C
      CALL SETLAB(IBL,NC4TO8('LABL'),0,NC4TO8('LABL'),0,10,1,0,1)
      SLABL='    (W"W")R'
      CALL FBUFOUT(NFXP,IBL,10*MACHINE+8,K)
      CALL SETLAB(IBUF,KIND,NSETS,NC4TO8('W"W"'),-1,NLAT,1,-1,NPACK)
      DO L=1,NLEV
        IL=(L-1)*NLAT
        DO I=1,NLAT
          X(I)=RZWW(IL+I)
        ENDDO
        IBUF(4)=LEV(L)
        CALL PUTFLD2(NFXP,X,IBUF,MAXX)
      ENDDO
C
C     * SAVE ZONAL (W*W*)R
C
      CALL SETLAB(IBL,NC4TO8('LABL'),0,NC4TO8('LABL'),0,10,1,0,1)
      SLABL='    (W*W*)R'
      CALL FBUFOUT(NFXP,IBL,10*MACHINE+8,K)
      CALL SETLAB(IBUF,KIND,NSETS,NC4TO8('W*W*'),-1,NLAT,1,-1,NPACK)
      DO L=1,NLEV
        IL=(L-1)*NLAT
        DO I=1,NLAT
          X(I)=RZWS2(IL+I)
        ENDDO
        IBUF(4)=LEV(L)
        CALL PUTFLD2(NFXP,X,IBUF,MAXX)
      ENDDO
C
C     * SAVE ZONAL (U"W")R
C
      CALL SETLAB(IBL,NC4TO8('LABL'),0,NC4TO8('LABL'),0,10,1,0,1)
      SLABL='    (U"W")R'
      CALL FBUFOUT(NFXP,IBL,10*MACHINE+8,K)
      CALL SETLAB(IBUF,KIND,NSETS,NC4TO8('U"W"'),-1,NLAT,1,-1,NPACK)
      DO L=1,NLEV
        IL=(L-1)*NLAT
        DO I=1,NLAT
          X(I)=RZUW(IL+I)
        ENDDO
        IBUF(4)=LEV(L)
        CALL PUTFLD2(NFXP,X,IBUF,MAXX)
      ENDDO
C
C     * SAVE ZONAL (U*W*)R
C
      CALL SETLAB(IBL,NC4TO8('LABL'),0,NC4TO8('LABL'),0,10,1,0,1)
      SLABL='    (U*W*)R'
      CALL FBUFOUT(NFXP,IBL,10*MACHINE+8,K)
      CALL SETLAB(IBUF,KIND,NSETS,NC4TO8('U*W*'),-1,NLAT,1,-1,NPACK)
      DO L=1,NLEV
        IL=(L-1)*NLAT
        DO I=1,NLAT
          X(I)=RZUWS(IL+I)
        ENDDO
        IBUF(4)=LEV(L)
        CALL PUTFLD2(NFXP,X,IBUF,MAXX)
      ENDDO
C
C     * SAVE ZONAL (V"W")R
C
      CALL SETLAB(IBL,NC4TO8('LABL'),0,NC4TO8('LABL'),0,10,1,0,1)
      SLABL='    (V"W")R'
      CALL FBUFOUT(NFXP,IBL,10*MACHINE+8,K)
      CALL SETLAB(IBUF,KIND,NSETS,NC4TO8('V"W"'),-1,NLAT,1,-1,NPACK)
      DO L=1,NLEV
        IL=(L-1)*NLAT
        DO I=1,NLAT
          X(I)=RZVW(IL+I)
        ENDDO
        IBUF(4)=LEV(L)
        CALL PUTFLD2(NFXP,X,IBUF,MAXX)
      ENDDO
C
C     * SAVE ZONAL (V*W*)R
C
      CALL SETLAB(IBL,NC4TO8('LABL'),0,NC4TO8('LABL'),0,10,1,0,1)
      SLABL='    (V*W*)R'
      CALL FBUFOUT(NFXP,IBL,10*MACHINE+8,K)
      CALL SETLAB(IBUF,KIND,NSETS,NC4TO8('V*W*'),-1,NLAT,1,-1,NPACK)
      DO L=1,NLEV
        IL=(L-1)*NLAT
        DO I=1,NLAT
          X(I)=RZVWS(IL+I)
        ENDDO
        IBUF(4)=LEV(L)
        CALL PUTFLD2(NFXP,X,IBUF,MAXX)
      ENDDO
      ENDIF                     ! LW
      ENDIF                     ! LZON
      ENDIF                     ! LUVW & LSTAT
 100  CONTINUE
C
CCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCC
C
C     * DO CALCULATIONS FOR TRACERS X
C
CCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCC
C
      DO NF=11,10+NTRAC
      FILE=ARG(NF-6)
      LFIL=LEN_TRIM(FILE)
      WRITE(*,*)'FILE=',FILE(1:LFIL)
C
C     * CALCULATE TIME MEAN OF X
C
      KIND=NC4TO8('GRID')
      REWIND NF
      DO L=1,NLEV
        IJ=(L-1)*NWDS
        CALL GETFLD2(NF,X,KIND,-1,-1,LEV(L),IBUF,MAXX,OK)
        IF(.NOT.OK) CALL                           XIT('GPXSTAT',-18)
        CALL PRTLAB(IBUF)
C
C       * STORE FIRST TIME STEP FOR DUDT (USE UVAR FOR MEAN AND UPVP FOR DX/DT)
C
        DO I=1,NWDS
          UVAR(IJ+I)=X(I)
          UPVP(IJ+I)=X(I)
        ENDDO
      ENDDO
      NT1=IBUF(2)
      WRITE(CNAM,'(A4)')IBUF(3)
C
C     * FORM SUPERLABELS FOR BASIC MODEL VARIABLES Q, T, Z, VR
C
      IF(IBUF(3).EQ.NC4TO8('SHUM'))THEN
        LXAVG='Q'
        NXAVG='SHUM'
        LDXDT='DQ/DT'
        NDXDT='  Q.'
        LXPXP='Q"Q"'
        NXPXP='Q"Q"'
        LXPUP='Q"U"'
        NXPUP='Q"U"'
        LXPVP='Q"V"'
        NXPVP='Q"V"'
        LXPWP='Q"W"'
        NXPWP='Q"W"'
        LZXAVG='(Q)R'
        NZXAVG='   Q'
        LZDXDT='(DQ/DT)R'
        NZDXDT='  Q.'
        LZXPXP='(Q"Q")R'
        NZXPXP='Q"Q"'
        LZXSXS='(Q*Q*)R'
        NZXSXS='Q*Q*'
        LZXPVP='(Q"V")R'
        NZXPVP='Q"V"'
        LZXSVS='(Q*V*)R'
        NZXSVS='Q*V*'
        LZXPWP='(Q"W")R'
        NZXPWP='Q"W"'
        LZXSWS='(Q*W*)R'
        NZXSWS='Q*W*'
      ELSE IF(IBUF(3).EQ.NC4TO8('TEMP'))THEN
        LXAVG='T'
        NXAVG='TEMP'
        LDXDT='DT/DT'
        NDXDT='  T.'
        LXPXP='T"T"'
        NXPXP='T"T"'
        LXPUP='T"U"'
        NXPUP='T"U"'
        LXPVP='T"V"'
        NXPVP='T"V"'
        LXPWP='T"W"'
        NXPWP='T"W"'
        LZXAVG='(T)R'
        NZXAVG='   T'
        LZDXDT='(DT/DT)R'
        NZDXDT='  T.'
        LZXPXP='(T"T")R'
        NZXPXP='T"T"'
        LZXSXS='(T*T*)R'
        NZXSXS='T*T*'
        LZXPVP='(T"V")R'
        NZXPVP='T"V"'
        LZXSVS='(T*V*)R'
        NZXSVS='T*V*'
        LZXPWP='(T"W")R'
        NZXPWP='T"W"'
        LZXSWS='(T*W*)R'
        NZXSWS='T*W*'
      ELSE IF(IBUF(3).EQ.NC4TO8(' PHI'))THEN
        LXAVG='GZ'
        NXAVG=' PHI'
        LDXDT='DGZ/DT'
        NDXDT=' GZ.'
        LXPXP='GZ"GZ"'
        NXPXP='Z"Z"'
        LXPUP='GZ"U"'
        NXPUP='Z"U"'
        LXPVP='GZ"V"'
        NXPVP='Z"V"'
        LXPWP='GZ"W"'
        NXPWP='Z"W"'
        LZXAVG='(GZ)R'
        NZXAVG='  GZ'
        LZDXDT='(DGZ/DT)R'
        NZDXDT=' GZ.'
        LZXPXP='(GZ"GZ")R'
        NZXPXP='Z"Z"'
        LZXSXS='(GZ*GZ*)R'
        NZXSXS='Z*Z*'
        LZXPVP='(GZ"V")R'
        NZXPVP='Z"V"'
        LZXSVS='(GZ*V*)R'
        NZXSVS='Z*V*'
        LZXPWP='(GZ"W")R'
        NZXPWP='Z"W"'
        LZXSWS='(GZ*W*)R'
        NZXSWS='Z*W*'
      ELSE IF(IBUF(3).EQ.NC4TO8('VORT'))THEN
        LXAVG='VORT'
        NXAVG='VORT'
        LDXDT='DVORT/DT'
        NDXDT='  P.'
        LXPXP='VORT"VORT"'
        NXPXP='P"P"'
        LXPUP='VORT"U"'
        NXPUP='P"U"'
        LXPVP='VORT"V"'
        NXPVP='P"V"'
        LXPWP='VORT"W"'
        NXPWP='P"W"'
        LZXAVG='(VORT)R'
        NZXAVG='VORT'
        LZDXDT='(DVORT/DT)R'
        NZDXDT='  P.'
        LZXPXP='(VORT"VORT")R'
        NZXPXP='P"P"'
        LZXSXS='(VORT*VORT*)R'
        NZXSXS='P*P*'
        LZXPVP='(VORT"V")R'
        NZXPVP='P"V"'
        LZXSVS='(VORT*V*)R'
        NZXSVS='P*V*'
        LZXPWP='(VORT"W")R'
        NZXPWP='P"W"'
        LZXSWS='(VORT*W*)R'
        NZXSWS='P*W*'
      ELSE
C
C       * FORM SUPERLABELS FROM FILE NAMES
C
        LXAVG=FILE(1:LFIL)
        NXAVG=CNAM
        LDXDT='D'//FILE(1:LFIL)//'/DT'
        NDXDT='  X.'
        LXPXP=''//FILE(1:LFIL)//'"'//FILE(1:LFIL)//'"'
        NXPXP='X"X"'
        LXPUP=''//FILE(1:LFIL)//'"U"'
        NXPUP='X"U"'
        LXPVP=''//FILE(1:LFIL)//'"V"'
        NXPVP='X"V"'
        LXPWP=''//FILE(1:LFIL)//'"W"'
        NXPWP='X"W"'
        LZXAVG='('//FILE(1:LFIL)//')R'
        NZXAVG=CNAM
        LZDXDT='(D'//FILE(1:LFIL)//'/DT)R'
        NZDXDT='  X.'
        LZXPXP='('//FILE(1:LFIL)//'"'//FILE(1:LFIL)//'")R'
        NZXPXP='X"X"'
        LZXSXS='('//FILE(1:LFIL)//'*'//FILE(1:LFIL)//'*)R'
        NZXSXS='X*X*'
        LZXPVP='('//FILE(1:LFIL)//'"V")R'
        NZXPVP='X"V"'
        LZXSVS='('//FILE(1:LFIL)//'*V*)R'
        NZXSVS='X*V*'
        LZXPWP='('//FILE(1:LFIL)//'"W")R'
        NZXPWP='X"W"'
        LZXSWS='('//FILE(1:LFIL)//'*W*)R'
        NZXSWS='X*W*'
      ENDIF
C
C     * TIMESTEP AND LEVEL LOOPS.
C
      NSETS=1
 300  CONTINUE
      DO L=1,NLEV
        IJ=(L-1)*NWDS
        CALL GETFLD2(NF,X,KIND,-1,-1,LEV(L),IBUF,MAXX,OK)
        IF(.NOT.OK)THEN
          IF(L.NE.1)CALL                           XIT('GPXSTAT',-19)
          GOTO 310
        ENDIF
C
C       * ACCUMULATE MEAN (IN UVAR) AND STORE X FOR DX/DT (IN VVAR)
C
        DO I=1,NWDS
          UVAR(IJ+I)=UVAR(IJ+I)+X(I)
          VVAR(IJ+I)=X(I)
        ENDDO
      ENDDO
      NSETS=NSETS+1
      GO TO 300
 310  CONTINUE
      WRITE(6,'(A,I10)')' NSETS=',NSETS
      NT2=IBUF(2)
C
C     * COMPLETE X TIME MEAN (IN UVAR)
C
      DO I=1,NLEV*NWDS
        UVAR(I)=UVAR(I)/FLOAT(NSETS)
      ENDDO
C
C     * SAVE MEAN OF X (IN UVAR)
C
      CALL SETLAB(IBL,NC4TO8('LABL'),0,NC4TO8('LABL'),0,10,1,0,1)
      SLABL='    '//LXAVG
      CALL FBUFOUT(NFGP,IBL,10*MACHINE+8,K)
      CALL SETLAB(IBUF,KIND,NSETS,NC4TO8(NXAVG),-1,NLG,NLAT,-1,NPACK)
      DO L=1,NLEV
        IJ=(L-1)*NWDS
        DO I=1,NWDS
          X(I)=UVAR(IJ+I)
        ENDDO
        IBUF(4)=LEV(L)
        CALL PUTFLD2(NFGP,X,IBUF,MAXX)
      ENDDO
C
C     * COMPLETE DXDT (IN UPVP) CALCULATIONS
C
      IF(LDT.EQ.1.AND.LSTAT.EQ.1)THEN
      IF(DELT.GT.0.E0)THEN
        IDNT=NT2-NT1
        IF(IDNT.NE.0)THEN
          DTI=1.E0/(DELT*FLOAT(IDNT))
        ELSE
          DTI=1.E0
        ENDIF
      ELSE
        CALL TIMEDC(NSEC1,NT1)
        CALL TIMEDC(NSEC2,NT2)
        IDNT=NSEC2-NSEC1
        IF(IDNT.NE.0)THEN
          DTI=1.E0/FLOAT(IDNT)
        ELSE
          DTI=1.E0
        ENDIF
      ENDIF                     ! DELT
      DO I=1,NLEV*NWDS
        UPVP(I)=(VVAR(I)-UPVP(I))*DTI
      ENDDO
C
C     * SAVE DX/DT (STORED IN UPVP)
C
      CALL SETLAB(IBL,NC4TO8('LABL'),0,NC4TO8('LABL'),0,10,1,0,1)
      SLABL='    '//LDXDT
      CALL FBUFOUT(NFGP,IBL,10*MACHINE+8,K)
      CALL SETLAB(IBUF,KIND,IDNT,NC4TO8(NDXDT),-1,NLG,NLAT,-1,NPACK)
      DO L=1,NLEV
        IJ=(L-1)*NWDS
        DO I=1,NWDS
          X(I)=UPVP(IJ+I)
        ENDDO
        IBUF(4)=LEV(L)
        CALL PUTFLD2(NFGP,X,IBUF,MAXX)
      ENDDO
      ENDIF                     ! LDT & LSTAT
C
C     * ZONAL AVERAGES FOR X (IN UVAR)
C
      IF(LZON.EQ.1)THEN
      DO L=1,NLEV
        IJ=(L-1)*NWDS
        IL=(L-1)*NLAT
        DO J=1,NLAT
          ZUJ  =0.E0
          ZDUJ =0.E0
          N=IJ+(J-1)*NLG
          DO I=1,NLGM
            ZUJ  =ZUJ  +UVAR(N+I)
            ZDUJ =ZDUJ +UVAR(N+I)*BETA(N+I)
          ENDDO
          IF(ZD(IL+J).GT.0.E0)THEN
            RZU(IL+J) =ZDUJ/ZD(IL+J)
          ELSE
            RZU(IL+J) =ZUJ /FLOAT(NLGM)
          ENDIF
        ENDDO
      ENDDO
C
C     * SAVE (X)R
C
      KIND=NC4TO8('ZONL')
      CALL SETLAB(IBL,NC4TO8('LABL'),0,NC4TO8('LABL'),0,10,1,0,1)
      SLABL='    '//LZXAVG
      CALL FBUFOUT(NFXP,IBL,10*MACHINE+8,K)
      CALL SETLAB(IBUF,KIND,NSETS,NC4TO8(NZXAVG),-1,NLAT,1,-1,NPACK)
      DO L=1,NLEV
        IL=(L-1)*NLAT
        DO I=1,NLAT
          X(I)=RZU(IL+I)
        ENDDO
        IBUF(4)=LEV(L)
        CALL PUTFLD2(NFXP,X,IBUF,MAXX)
      ENDDO
C
C     * ZONAL AVERAGES FOR DX/DT (UPVP)
C
      IF(LDT.EQ.1.AND.LSTAT.EQ.1)THEN
      DO L=1,NLEV
        IJ=(L-1)*NWDS
        IL=(L-1)*NLAT
        DO J=1,NLAT
          ZUJ  =0.E0
          ZDUJ =0.E0
          N=IJ+(J-1)*NLG
          DO I=1,NLGM
            ZUJ =ZUJ +UPVP(N+I)
            ZDUJ=ZDUJ+UPVP(N+I)*BETA(N+I)
          ENDDO
          IF(ZD(IL+J).GT.0.E0)THEN
            RZUU(IL+J)=ZDUJ/ZD(IL+J)
          ELSE
            RZUU(IL+J)=ZUJ/FLOAT(NLGM)
          ENDIF
        ENDDO
      ENDDO
C
C     * SAVE (DX/DT)R
C
      CALL SETLAB(IBL,NC4TO8('LABL'),0,NC4TO8('LABL'),0,10,1,0,1)
      SLABL='    '//LZDXDT
      CALL FBUFOUT(NFXP,IBL,10*MACHINE+8,K)
      CALL SETLAB(IBUF,KIND,IDNT,NC4TO8(NZDXDT),-1,NLAT,1,-1,NPACK)
      DO L=1,NLEV
        IL=(L-1)*NLAT
        DO I=1,NLAT
          X(I)=RZUU(IL+I)
        ENDDO
        IBUF(4)=LEV(L)
        CALL PUTFLD2(NFXP,X,IBUF,MAXX)
      ENDDO
      ENDIF                     ! LDT & LSTAT
      ENDIF                     ! LZON
C
CCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCC
C
C     * CALCULATE VARIANCE OF X AND XPUP,XPVP,XPWP
C     * DO IT ONLY IF LSTAT.EQ.1
C
C     * XAVG IN UVAR
C     * XVAR IN VVAR
C     * XPUP IN UPWP
C     * XPVP IN VPWP
C     * XPWP IN WVAR
C
CCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCC
C
      IF(LSTAT.NE.1) GOTO 400

      DO I=1,NLEV*NWDS
        VVAR(I)=0.E0
        UPWP(I)=0.E0
        VPWP(I)=0.E0
        WVAR(I)=0.E0
      ENDDO
      KIND=NC4TO8('GRID')
      REWIND 2
      REWIND 3
      REWIND 4
      REWIND NF
      DO N=1,NSETS
        DO L=1,NLEV
          IJ=(L-1)*NWDS
C         * X"X"
          CALL GETFLD2(NF,X,KIND,-1,-1,LEV(L),IBUF,MAXX,OK)
          IF(.NOT.OK) CALL                         XIT('GPXSTAT',-20)
          ITIME=IBUF(2)
          DO I=1,NWDS
            VVAR(IJ+I)=VVAR(IJ+I)+(X(I)-UVAR(IJ+I))**2
          ENDDO
C         * X"U"
          IF(LUVW.NE.-1)THEN
          CALL GETFLD2(2,Y,KIND,ITIME,-1,LEV(L),IBUF,MAXX,OK)
          IF(.NOT.OK) CALL                         XIT('GPXSTAT',-21)
          DO I=1,NWDS
            UPWP(IJ+I)=UPWP(IJ+I)+(X(I)-UVAR(IJ+I))*(Y(I)-UAVG(IJ+I))
          ENDDO
C         * X"V"
          CALL GETFLD2(3,Y,KIND,ITIME,-1,LEV(L),IBUF,MAXX,OK)
          IF(.NOT.OK) CALL                         XIT('GPXSTAT',-22)
          DO I=1,NWDS
            VPWP(IJ+I)=VPWP(IJ+I)+(X(I)-UVAR(IJ+I))*(Y(I)-VAVG(IJ+I))
          ENDDO
C         * X"W"
          IF(LW.EQ.1)THEN
          CALL GETFLD2(4,Y,KIND,ITIME,-1,LEV(L),IBUF,MAXX,OK)
          IF(.NOT.OK) CALL                         XIT('GPXSTAT',-23)
          DO I=1,NWDS
            WVAR(IJ+I)=WVAR(IJ+I)+(X(I)-UVAR(IJ+I))*(Y(I)-WAVG(IJ+I))
          ENDDO
          ENDIF                 ! LW
          ENDIF                 ! LUVW
        ENDDO                   ! NLEV
      ENDDO
C
C     * COMPLETE VARIANCE CALCULATIONS
C
      DO I=1,NLEV*NWDS
        VVAR(I)=VVAR(I)/FLOAT(NSETS)
        UPWP(I)=UPWP(I)/FLOAT(NSETS)
        VPWP(I)=VPWP(I)/FLOAT(NSETS)
      ENDDO
      IF(LW.EQ.1)THEN
      DO I=1,NLEV*NWDS
        WVAR(I)=WVAR(I)/FLOAT(NSETS)
      ENDDO
      ENDIF                     ! LW
C
C     * SAVE X"X"
C
      CALL SETLAB(IBL,NC4TO8('LABL'),0,NC4TO8('LABL'),0,10,1,0,1)
      SLABL='    '//LXPXP
      CALL FBUFOUT(NFGP,IBL,10*MACHINE+8,K)
      CALL SETLAB(IBUF,KIND,NSETS,NC4TO8(NXPXP),-1,NLG,NLAT,-1,NPACK)
      DO L=1,NLEV
        IJ=(L-1)*NWDS
        DO I=1,NWDS
          X(I)=VVAR(IJ+I)
        ENDDO
        IBUF(4)=LEV(L)
        CALL PUTFLD2(NFGP,X,IBUF,MAXX)
      ENDDO
C
C     * SAVE X"U"
C
      IF(LUVW.NE.-1)THEN
      CALL SETLAB(IBL,NC4TO8('LABL'),0,NC4TO8('LABL'),0,10,1,0,1)
      SLABL='    '//LXPUP
      CALL FBUFOUT(NFGP,IBL,10*MACHINE+8,K)
      CALL SETLAB(IBUF,KIND,NSETS,NC4TO8(NXPUP),-1,NLG,NLAT,-1,NPACK)
      DO L=1,NLEV
        IJ=(L-1)*NWDS
        DO I=1,NWDS
          X(I)=UPWP(IJ+I)
        ENDDO
        IBUF(4)=LEV(L)
        CALL PUTFLD2(NFGP,X,IBUF,MAXX)
      ENDDO
C
C     * SAVE X"V"
C
      CALL SETLAB(IBL,NC4TO8('LABL'),0,NC4TO8('LABL'),0,10,1,0,1)
      SLABL='    '//LXPVP
      CALL FBUFOUT(NFGP,IBL,10*MACHINE+8,K)
      CALL SETLAB(IBUF,KIND,NSETS,NC4TO8(NXPVP),-1,NLG,NLAT,-1,NPACK)
      DO L=1,NLEV
        IJ=(L-1)*NWDS
        DO I=1,NWDS
          X(I)=VPWP(IJ+I)
        ENDDO
        IBUF(4)=LEV(L)
        CALL PUTFLD2(NFGP,X,IBUF,MAXX)
      ENDDO

      IF(LW.EQ.1)THEN
C
C     * SAVE X"W"
C
      CALL SETLAB(IBL,NC4TO8('LABL'),0,NC4TO8('LABL'),0,10,1,0,1)
      SLABL='    '//LXPWP
      CALL FBUFOUT(NFGP,IBL,10*MACHINE+8,K)
      CALL SETLAB(IBUF,KIND,NSETS,NC4TO8(NXPWP),-1,NLG,NLAT,-1,NPACK)
      DO L=1,NLEV
        IJ=(L-1)*NWDS
        DO I=1,NWDS
          X(I)=WVAR(IJ+I)
        ENDDO
        IBUF(4)=LEV(L)
        CALL PUTFLD2(NFGP,X,IBUF,MAXX)
      ENDDO
      ENDIF                     ! LW
      ENDIF                     ! LUVW
C
C     * ZONAL AVERAGES FOR X"X" AND X"V"
C
      IF(LZON.EQ.1)THEN
      KIND=NC4TO8('ZONL')
      DO L=1,NLEV
        IJ=(L-1)*NWDS
        IL=(L-1)*NLAT
        DO J=1,NLAT
          ZUUJ =0.E0
          ZUVJ =0.E0
          ZDUUJ=0.E0
          ZDUVJ=0.E0
          N=IJ+(J-1)*NLG
          DO I=1,NLGM
            ZUUJ =ZUUJ +VVAR(N+I)
            ZUVJ =ZUVJ +VPWP(N+I)
            ZDUUJ=ZDUUJ+VVAR(N+I)*BETA(N+I)
            ZDUVJ=ZDUVJ+VPWP(N+I)*BETA(N+I)
          ENDDO
          IF(ZD(IL+J).GT.0.E0)THEN
            RZUU(IL+J)=ZDUUJ/ZD(IL+J)
            RZUV(IL+J)=ZDUVJ/ZD(IL+J)
          ELSE
            RZUU(IL+J)=ZUUJ/FLOAT(NLGM)
            RZUV(IL+J)=ZUVJ/FLOAT(NLGM)
          ENDIF
        ENDDO
      ENDDO
C
C     * ZONAL STATIONARY EDDIES FOR X*X* AND X*V*
C
      DO L=1,NLEV
        IJ=(L-1)*NWDS
        IL=(L-1)*NLAT
        DO J=1,NLAT
          ZUUJ=0.E0
          ZUVJ=0.E0
          ZDUUJ=0.E0
          ZDUVJ=0.E0
          N=IJ+(J-1)*NLG
          DO I=1,NLGM
            ZUUJ =ZUUJ +(UVAR(N+I)-RZU(IL+J))**2
            ZDUUJ=ZDUUJ+(UVAR(N+I)-RZU(IL+J))**2*BETA(N+I)
            ZUVJ =ZUVJ +(UVAR(N+I)-RZU(IL+J))*(VAVG(N+I)-RZV(IL+J))
            ZDUVJ=ZDUVJ+(UVAR(N+I)-RZU(IL+J))*(VAVG(N+I)-RZV(IL+J))*
     +           BETA(N+I)
          ENDDO
          IF(ZD(IL+J).GT.0.E0)THEN
            RZUS2(IL+J)=ZDUUJ/ZD(IL+J)
            RZUVS(IL+J)=ZDUVJ/ZD(IL+J)
          ELSE
            RZUS2(IL+J)=ZUUJ/FLOAT(NLGM)
            RZUVS(IL+J)=ZUVJ/FLOAT(NLGM)
          ENDIF
        ENDDO
      ENDDO
C
C     * ZONAL AVERAGES FOR XW
C
      IF(LW.EQ.1)THEN
      DO L=1,NLEV
        IJ=(L-1)*NWDS
        IL=(L-1)*NLAT
        DO J=1,NLAT
          ZUWJ =0.E0
          ZDUWJ=0.E0
          N=IJ+(J-1)*NLG
          DO I=1,NLGM
            ZUWJ =ZUWJ +WVAR(N+I)
            ZDUWJ=ZDUWJ+WVAR(N+I)*BETA(N+I)
          ENDDO
          IF(ZD(IL+J).GT.0.E0)THEN
            RZUW(IL+J)=ZDUWJ/ZD(IL+J)
          ELSE
            RZUW(IL+J)=ZUWJ/FLOAT(NLGM)
          ENDIF
        ENDDO
      ENDDO
C
C     * ZONAL STATIONARY EDDIES FOR X*W*
C
      DO L=1,NLEV
        IJ=(L-1)*NWDS
        IL=(L-1)*NLAT
        DO J=1,NLAT
          ZUWJ=0.E0
          ZDUWJ=0.E0
          N=IJ+(J-1)*NLG
          DO I=1,NLGM
            ZUWJ=ZUWJ+(UVAR(N+I)-RZU(IL+J))*(WAVG(N+I)-RZW(IL+J))
            ZDUWJ=ZDUWJ+(UVAR(N+I)-RZU(IL+J))*(WAVG(N+I)-RZW(IL+J))*
     +           BETA(N+I)
          ENDDO
          IF(ZD(IL+J).GT.0.E0)THEN
            RZUWS(IL+J)=ZDUWJ/ZD(IL+J)
          ELSE
            RZUWS(IL+J)=ZUWJ/FLOAT(NLGM)
          ENDIF
        ENDDO
      ENDDO
      ENDIF                     ! LW
C
C     * SAVE ZONAL (X"X")R
C
      CALL SETLAB(IBL,NC4TO8('LABL'),0,NC4TO8('LABL'),0,10,1,0,1)
      SLABL='    '//LZXPXP
      CALL FBUFOUT(NFXP,IBL,10*MACHINE+8,K)
      CALL SETLAB(IBUF,KIND,NSETS,NC4TO8(NZXPXP),-1,NLAT,1,-1,NPACK)
      DO L=1,NLEV
        IL=(L-1)*NLAT
        DO I=1,NLAT
          X(I)=RZUU(IL+I)
        ENDDO
        IBUF(4)=LEV(L)
        CALL PUTFLD2(NFXP,X,IBUF,MAXX)
      ENDDO
C
C     * SAVE ZONAL (X*X*)R
C
      CALL SETLAB(IBL,NC4TO8('LABL'),0,NC4TO8('LABL'),0,10,1,0,1)
      SLABL='    '//LZXSXS
      CALL FBUFOUT(NFXP,IBL,10*MACHINE+8,K)
      CALL SETLAB(IBUF,KIND,NSETS,NC4TO8(NZXSXS),-1,NLAT,1,-1,NPACK)
      DO L=1,NLEV
        IL=(L-1)*NLAT
        DO I=1,NLAT
          X(I)=RZUS2(IL+I)
        ENDDO
        IBUF(4)=LEV(L)
        CALL PUTFLD2(NFXP,X,IBUF,MAXX)
      ENDDO
C
C     * SAVE ZONAL (X"V")R
C
      IF(LUVW.NE.-1)THEN
      CALL SETLAB(IBL,NC4TO8('LABL'),0,NC4TO8('LABL'),0,10,1,0,1)
      SLABL='    '//LZXPVP
      CALL FBUFOUT(NFXP,IBL,10*MACHINE+8,K)
      CALL SETLAB(IBUF,KIND,NSETS,NC4TO8(NZXPVP),-1,NLAT,1,-1,NPACK)
      DO L=1,NLEV
        IL=(L-1)*NLAT
        DO I=1,NLAT
          X(I)=RZUV(IL+I)
        ENDDO
        IBUF(4)=LEV(L)
        CALL PUTFLD2(NFXP,X,IBUF,MAXX)
      ENDDO
C
C     * SAVE ZONAL (X*V*)R
C
      CALL SETLAB(IBL,NC4TO8('LABL'),0,NC4TO8('LABL'),0,10,1,0,1)
      SLABL='    '//LZXSVS
      CALL FBUFOUT(NFXP,IBL,10*MACHINE+8,K)
      CALL SETLAB(IBUF,KIND,NSETS,NC4TO8(NZXSVS),-1,NLAT,1,-1,NPACK)
      DO L=1,NLEV
        IL=(L-1)*NLAT
        DO I=1,NLAT
          X(I)=RZUVS(IL+I)
        ENDDO
        IBUF(4)=LEV(L)
        CALL PUTFLD2(NFXP,X,IBUF,MAXX)
      ENDDO
C
C     * SAVE ZONAL (X"W")R
C
      IF(LW.EQ.1)THEN
      CALL SETLAB(IBL,NC4TO8('LABL'),0,NC4TO8('LABL'),0,10,1,0,1)
      SLABL='    '//LZXPWP
      CALL FBUFOUT(NFXP,IBL,10*MACHINE+8,K)
      CALL SETLAB(IBUF,KIND,NSETS,NC4TO8(NZXPWP),-1,NLAT,1,-1,NPACK)
      DO L=1,NLEV
        IL=(L-1)*NLAT
        DO I=1,NLAT
          X(I)=RZUW(IL+I)
        ENDDO
        IBUF(4)=LEV(L)
        CALL PUTFLD2(NFXP,X,IBUF,MAXX)
      ENDDO
C
C     * SAVE ZONAL (X*W*)R
C
      CALL SETLAB(IBL,NC4TO8('LABL'),0,NC4TO8('LABL'),0,10,1,0,1)
      SLABL='    '//LZXSWS
      CALL FBUFOUT(NFXP,IBL,10*MACHINE+8,K)
      CALL SETLAB(IBUF,KIND,NSETS,NC4TO8(NZXSWS),-1,NLAT,1,-1,NPACK)
      DO L=1,NLEV
        IL=(L-1)*NLAT
        DO I=1,NLAT
          X(I)=RZUWS(IL+I)
        ENDDO
        IBUF(4)=LEV(L)
        CALL PUTFLD2(NFXP,X,IBUF,MAXX)
      ENDDO
      ENDIF                     ! LW
      ENDIF                     ! LUVW
      ENDIF                     ! LZON
 400  CONTINUE
      ENDDO                     ! NF
C
C     * DEALLOCATE
C
      DEALLOCATE(X,Y,Z,
     +     BETA,
     +     UAVG,UVAR,
     +     VAVG,VVAR,
     +     WAVG,WVAR,
     +     UPVP,UPWP,VPWP,
     +     ZD,RZU,RZUS2,
     +     RZV,RZVS2,RZUVS,
     +     RZW,RZWS2,RZUWS,RZVWS,
     +     RZUU,RZVV,RZWW,
     +     RZUV,RZUW,RZVW)
C
C     * NORMAL EXIT.
C

      CALL                                         XIT('GPXSTAT',0)
  911 CALL                                         XIT('GPXSTAT',-24)

C---------------------------------------------------------------------
 5000 FORMAT(10X,6I5,E10.0)                                                     C4
 6000 FORMAT(' LUVW=',I5,' LW=',I5,' LDT=',I5,' LZON=',I5,' LBETA=',I5,
     1     ' LSTAT=',I5,' DELT=',E10.3)
 6010 FORMAT(' ..GPXSTAT INPUT FILE IS EMPTY')
 6020 FORMAT(' NAME =',A4/' NLEVS =',I5/
     1     ' LEVELS = ',15I6/100(10X,15I6/))
 6050 FORMAT(' ..GPXSTAT INPUT ERROR - NAME,L=',2X,A4,I5)
 6060 FORMAT(' GPXSTAT PROCESSED ',I10,' SETS OF ',A4)
      END
