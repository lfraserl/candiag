      PROGRAM CRITT 
C     PROGRAM CRITT (TC,       INPUT,       OUTPUT,                     )       H2
C    1         TAPE2=TC, TAPE5=INPUT, TAPE6=OUTPUT) 
C     ---------------------------------------------                             H2
C                                                                               H2
C     JUL 03/03 - A.IRWIN,F.MAJAESS (REVISED FOR F90 AND LINUX PLATFORMS)       H2
C     JAN 29/92 - E. CHAN  (CONVERT HOLLERITH LITERALS TO ASCII)                
C     APRIL 16/84 - M.LAZARE.                                                  
C     MAY 06/83 - R.LAPRISE.
C     CCRN NOVEMBER 16/82 - M LAZARE
C                                                                               H2
CCRITT   - COMPUTES THE CRITICAL VALUE FOR THE STUDENT-T TEST           0  1 C  H1
C                                                                               H3
CAUTHOR  - M.LAZARE                                                             H3
C                                                                               H3
CPURPOSE - COMPUTES THE CRITICAL VALUE OF THE STANDARD SIGNIFICANCE             H3
C          (STUDENT-T) TEST FOR 2 POOLED DATA SETS,  FOR A GIVEN                H3
C          LEVEL OF THE TEST , AND FOR CERTAIN SAMPLE SIZES OF                  H3
C          CONTROL AND EXPERIMENT.                                              H3
C                                                                               H3
COUTPUT FILE...                                                                 H3
C                                                                               H3
C      TC = CARD IMAGE FILE OF CRITICAL VALUE IN THE PROPER FORMAT              H3
C           TO BE READ BY THE PROGRAM XLIN.                                     H3
C 
CINPUT PARAMETERS...
C                                                                               H5
C       ALFA  = SIGNIFICANCE LEVEL (ALFA=.01, .05, .10 CORRESPOND               H5
C               RESPECTIVELY TO 99%, 95% AND 90% CONFIDENCE LEVELS)             H5
C       RM    = SAMPLE SIZE OF THE EXPERIMENT                                   H5
C       RN    = SAMPLE SIZE OF THE CONTROL RUN                                  H5
C               RM+RN-2 MUST BE .LE. 10.                                        H5
C                                                                               H5
CEXAMPLE OF INPUT CARD...                                                       H5
C                                                                               H5
C*CRITT         0.05        1.        5.                                        H5
C------------------------------------------------------------------------ 
C 
C     * DEFINE THE MATRIX OF POSSIBLE VALUES OF TCRITICAL- THE 3 ROWS OF
C     * "TCRMAT" MATRIX CORRESPOND TO ALFA=.01,.05, AND .10 (THE USUAL
C     * SIGNIFICANCE LEVELS), WHILE THE 30 COLUMNS CORRESPOND TO THE
C     * NUMBER OF ALLOWABLE DEGREES OF FREEDOM. 
C 
      IMPLICIT REAL (A-H,O-Z),
     +INTEGER (I-N)
      REAL TCRMAT(3,30) 
      REAL TCRIT,ALFA,RM,RN 
      INTEGER IDEGFR,IROW 
      DATA TCRMAT/63.657E0,12.706E0,6.314E0,9.925E0,4.303E0,2.920E0,
     1            5.841E0,3.182E0,2.353E0,4.604E0,2.776E0,2.132E0,
     1            4.032E0,2.571E0,2.015E0,3.707E0,2.447E0,1.943E0,
     1            3.499E0,2.365E0,1.895E0,3.355E0,2.306E0,1.860E0,
     1            3.250E0,2.262E0,1.833E0,3.169E0,2.228E0,1.812E0,
     1            3.106E0,2.201E0,1.796E0,3.055E0,2.179E0,1.782E0,
     1            3.012E0,2.160E0,1.771E0,2.977E0,2.145E0,1.761E0,
     1            2.947E0,2.131E0,1.753E0,2.921E0,2.120E0,1.746E0,
     1            2.898E0,2.110E0,1.740E0,2.878E0,2.101E0,1.734E0,
     1            2.861E0,2.093E0,1.729E0,2.845E0,2.086E0,1.725E0,
     1            2.831E0,2.080E0,1.721E0,2.819E0,2.074E0,1.717E0,
     1            2.807E0,2.069E0,1.714E0,2.797E0,2.064E0,1.711E0,
     1            2.787E0,2.060E0,1.708E0,2.779E0,2.056E0,1.706E0,
     1            2.771E0,2.052E0,1.703E0,2.763E0,2.048E0,1.701E0,
     1            2.756E0,2.045E0,1.699E0,2.576E0,1.960E0,1.645E0/
C-----------------------------------------------------------------------
      NFF=3 
      CALL JCLPNT(NFF,-2,5,6)
      REWIND 2
C 
C     * READ IN PARAMETERS
C 
      READ(5,5000,END=903) ALFA,RM,RN                                           H4
      WRITE(6,6000) ALFA,RM,RN
      IDEGFR=NINT(RM+RN-2.E0) 
C 
C     * EXIT WITH ERROR MESSAGE IF BAD INPUT PARAMETERS 
C 
      IF(ALFA.NE.0.01E0.AND.ALFA.NE.0.05E0.AND.ALFA.NE.0.10E0) 
     1   CALL                                      XIT('CRITT',-1)
      IF(IDEGFR.LT.1) CALL                         XIT('CRITT',-2)
      IF(IDEGFR.GE.30) IDEGFR=30
C 
C     * FIND OUT CRITICAL VALUE OF T FOR REQUIRED PARAMETERS
C     * CORRESPONDING TO THE ABOVE MATRIX 
C 
      IF(ALFA.EQ.0.01E0) IROW=1
      IF(ALFA.EQ.0.05E0) IROW=2
      IF(ALFA.EQ.0.10E0) IROW=3
      TCRIT=TCRMAT(IROW,IDEGFR) 
C 
C     * WRITE OUT RESULT AND EXIT NORMALLY
C 
      X=0.E0
      WRITE(2,2000) X,TCRIT 
      WRITE(6,6010) X,TCRIT 
      CALL                                         XIT('CRITT',0) 
C 
C     * E.O.F. ON INPUT.
C 
  903 CALL                                         XIT('CRITT',-3)
C-----------------------------------------------------------------------
 2000 FORMAT(' XLIN.    ',2F10.3)
 5000 FORMAT(10X,3E10.0)                                                        H4
 6000 FORMAT('0 ALFA,XYR,YYR=',3F10.3)
 6010 FORMAT('0   X,TCRIT=',2F10.3)
      END
