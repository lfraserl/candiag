      PROGRAM FINDER
C     PROGRAM FINDER (IN,       OUT,       INPUT,       OUTPUT,         )       B2
C    1          TAPE1=IN, TAPE2=OUT, TAPE5=INPUT, TAPE6=OUTPUT) 
C     ---------------------------------------------------------                 B2
C                                                                               B2
C     JUL 03/03 - A.IRWIN,F.MAJAESS (REVISED FOR F90 AND LINUX PLATFORMS)       B2
C     JAN 29/92 - E. CHAN  (CONVERT HOLLERITH LITERALS TO ASCII)                
C     MAY 20/83 - R.LAPRISE.                                                    
C     JUL 14/81 - J.D.HENDERSON 
C                                                                               B2
CFINDER  - FINDS AND COPIES A FIELD OR A SET FROM AN UN-ORDERED FILE    1  1 C  B1
C                                                                               B3
CAUTHOR  - J.D.HENDERSON                                                        B3
C                                                                               B3
CPURPOSE - FINDS AND COPIES ONE FIELD OR ONE MULTI-LEVEL SET FROM A             B3
C          FILE ONTO A SEPARATE FILE. THE FIRST FILE DOES NOT HAVE              B3
C          TO BE ORDERED BY TIMESTEP NUMBER. (SEE SELECT)                       B3
C                                                                               B3
CINPUT FILE...                                                                  B3
C                                                                               B3
C      IN  = FILE CONTAINING THE FIELD OR SET TO BE COPIED.                     B3
C                                                                               B3
COUTPUT FILE...                                                                 B3
C                                                                               B3
C      OUT = FILE CONTAINING THE REQUESTED FIELD OR SET.                        B3
C 
CINPUT PARAMETERS...
C                                                                               B5
C      NT   = TIMESTEP NUMBER OF REQUESTED DATA.                                B5
C             (-1 OMITS TIMESTEP CHECK)                                         B5
C      NAME = NAME OF REQUESTED DATA (LABEL WORD 3).                            B5
C             ('NEXT' USES THE NEXT FIELD ON THE FILE).                         B5
C      LVL  = LEVEL NUMBER OF REQUESTED DATA.                                   B5
C             (-1 OMITS LEVEL CHECK).                                           B5
C      NFL  = 1 TO RETRIEVE A FIELD, OTHERWISE A SET IS COPIED.                 B5
C                                                                               B5
CEXAMPLE OF INPUT CARD...                                                       B5
C                                                                               B5
C*  FINDER        36 TEMP   -1    0                                             B5
C                                                                               B5
CTHIS COPIES ONE MULTI-LEVEL SET OF TEMP AT TIMESTEP 36.                        B5
C---------------------------------------------------------------------------- 
C 
      use diag_sizes, only : SIZES_BLONP1xBLATxNWORDIO

      IMPLICIT REAL (A-H,O-Z),
     +INTEGER (I-N)
      LOGICAL OK
      COMMON/ICOM/IBUF(8),IDAT(SIZES_BLONP1xBLATxNWORDIO)
C 
      DATA MAXX/SIZES_BLONP1xBLATxNWORDIO/
C-------------------------------------------------------------------- 
      NFF=4 
      CALL JCLPNT(NFF,1,2,5,6)
      REWIND 1
      REWIND 2
C 
C     * READ FIELD ID FROM A CARD.
C     * NT,NAME,LVL = REQUESTED TIME,NAME AND LEVEL.
C     * NFL = 1 TO READ ONE FIELD ONLY. OTHERWISE A SET IS READ.
C 
      READ(5,5010,END=901) NT,NAME,LVL,NFL                                      B4
      WRITE(6,6005) NT,NAME,LVL,NFL 
C 
C     * TRANSFER THE FIRST FIELD. 
C 
      CALL RECGET(1,-1,NT,NAME,LVL,IBUF,MAXX,OK) 
      IF(.NOT.OK)THEN 
        WRITE(6,6010) 
        CALL                                       XIT('FINDER',-1) 
      ENDIF 
      CALL RECPUT(2,IBUF) 
      WRITE(6,6025) IBUF
      IF(NFL.EQ.1) CALL                            XIT('FINDER',1)
      NTR=IBUF(2) 
      NAM=IBUF(3) 
C 
C     * COPY THE REST OF THE SET IF REQUESTED (NFL=0).
C 
  150 CALL RECGET(1,-1,-1,NC4TO8("NEXT"),-1,IBUF,MAXX,OK)
      IF(.NOT.OK) CALL                             XIT('FINDER',2)
      IF(IBUF(2).NE.NTR) CALL                      XIT('FINDER',3)
      IF(IBUF(3).NE.NAM) CALL                      XIT('FINDER',4)
      CALL RECPUT(2,IBUF) 
      WRITE(6,6025) IBUF
      GO TO 150 
C 
C     * E.O.F.ON INPUT. 
C 
  901 CALL                                         XIT('FINDER',-2) 
C-------------------------------------------------------------------- 
 5010 FORMAT(10X,I10,1X,A4,2I5)                                                 B4
 6005 FORMAT('0LOOKING FOR ',I10,1X,A4,I6,I10)
 6010 FORMAT('0** NOT FOUND')
 6025 FORMAT(' ',A4,I10,1X,A4,I10,4I6)
      END
