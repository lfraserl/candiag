      PROGRAM XLFIND
C     PROGRAM XLFIND (XIN,        XOUT,       INPUT,       OUTPUT,      )       B2
C    1         TAPE11=XIN, TAPE12=XOUT, TAPE5=INPUT, TAPE6=OUTPUT)
C     ------------------------------------------------------------              B2
C                                                                               B2
C     JUL 03/03 - A.IRWIN,F.MAJAESS (REVISED FOR F90 AND LINUX PLATFORMS)       B2
C     JAN 29/92 - E. CHAN  (CONVERT HOLLERITH LITERALS TO ASCII)                
C     MAR 06/89 - F.MAJAESS (INCREASE FIELD DIMENSION TO 66000)                 
C     JAN 04/84 - R.LAPRISE.
C                                                                               B2
CXLFIND  - EXTRACT A HYPERLABELLED SET FROM A FILE.                     1  1 C  B1
C                                                                               B3
CAUTHOR  - R.LAPRISE                                                            B3
C                                                                               B3
CPURPOSE - SEARCH FILE XIN FOR HYPERLABEL RECORD MATCHING INPUT CARD            B3
C          HYPERLABEL.                                                          B3
C          THE HYPERLABELLED SET IS PUT ON FILE XOUT, WITH THE HYPERLABEL       B3
C          STRIPPED OFF.                                                        B3
C                                                                               B3
CINPUT FILE...                                                                  B3
C                                                                               B3
C      XIN   = FILE CONTAINING HYPERLABELLED SETS.                              B3
C                                                                               B3
COUTPUT FILE...                                                                 B3
C                                                                               B3
C      XOUT  = SET WHOSE HYPERLABEL MATCHES THE REQUESTED ONE.                  B3
C 
CINPUT PARAMETERS...
C                                                                               B5
C      LABEL = 80 CHARACTER HYPERLABEL WHICH IS REQUESTED.                      B5
C                                                                               B5
CEXAMPLE OF INPUT CARD...                                                       B5
C                                                                               B5
C*ARD007 M1A11SS                                                                B5
C-----------------------------------------------------------------------------
C 
      use diag_sizes, only : SIZES_BLONP1xBLATxNWORDIO

      IMPLICIT REAL (A-H,O-Z),
     +INTEGER (I-N)
      LOGICAL OK,LSAME
      INTEGER LABEL(10) 
      COMMON /ICOM/ IBUF(8),IDAT(SIZES_BLONP1xBLATxNWORDIO) 
C 
      DATA MAXX/SIZES_BLONP1xBLATxNWORDIO/ 
C------------------------------------------------------------------------ 
      NFF=4 
      CALL JCLPNT(NFF,11,12,5,6)
      REWIND 11 
      REWIND 12 
C 
C     * READ-IN HYPERLABEL CARD.
C 
      READ(5,5010,END=901) LABEL                                                B4
      WRITE(6,6010) LABEL 
C 
C     * SEARCH XIN FOR MATCHING HYPERLABEL RECORD.
C 
      NS=0
  100 CALL RECGET(11,NC4TO8("LABL"),-1,NC4TO8("FILE"),-1,IBUF,MAXX,OK)
      IF(.NOT.OK) CALL                             XIT('XLFIND',-1) 
      NS=NS+1 
      CALL COMPAR(LSAME,IDAT,LABEL,10)
      IF(.NOT.LSAME) GO TO 100
C 
C     * MATCHING HYPERLABEL FOUND.  COPY FOLLOWING SET TO XOUT
C     * TILL NEXT HYPERLABEL FOUND. 
C 
      NC=0
  200 CALL RECGET(11,-1,-1,-1,-1,IBUF,MAXX,OK) 
      IF(.NOT.OK .AND. NC.EQ.0) CALL               XIT('XLFIND',-2) 
      OK=OK .AND. .NOT.(IBUF(1).EQ.NC4TO8("LABL") .AND. 
     +                  IBUF(3).EQ.NC4TO8("FILE")      )
      IF(.NOT.OK)THEN 
        WRITE(6,6030)IBUF 
        WRITE(6,6020) NS,NC 
        CALL                                       XIT('XLFIND',0)
      ENDIF 
      CALL RECPUT(12,IBUF)
      NC=NC+1 
      IF(NC.EQ.1) WRITE(6,6030)IBUF 
      GO TO 200 
C 
C     * E.O.F. ON INPUT.
C 
  901 CALL                                         XIT('XLFIND',-3) 
C---------------------------------------------------------------------- 
 5010 FORMAT(10A8)                                                              B4
 6010 FORMAT('0 SEARCH FOR HYPERLABEL =',10A8)
 6020 FORMAT(' XLFIND SKIPPED',I5,' RECORDS AND COPIED',I5,
     1       ' RECORDS.')
 6030 FORMAT(1X,A4,I10,1X,A4,I10,4I6)
      END
