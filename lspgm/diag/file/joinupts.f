      PROGRAM JOINUPTS
C     PROGRAM JOINUPTS(JOIN,        IN11,...        IN98,        OUTPUT,)       B2
C    1          TAPE10=JOIN, TAPE21=IN11,...,TAPE50=IN98, TAPE6 =OUTPUT)
C     ------------------------------------------------------------              B2
C                                                                               B2
C     JUL 03/03 - A.IRWIN,F.MAJAESS (REVISED FOR F90 AND LINUX PLATFORMS)       B2
C     OCT 20/00 - S. KHARIN (INCREASE NUMBER OF INPUT FILES TO 88)              B2
C     JAN 14/93 - E. CHAN  (END READ USING FBUFFIN IF K.GE.0)
C     MAR 16/92 - E. CHAN  (INVOKE FBUFFIN/FBUFOUT TO DO THE I/O)
C     JAN 29/92 - E. CHAN  (CONVERT HOLLERITH LITERALS TO ASCII)
C     NOV 30/81 - J.D.HENDERSON.
C                                                                               B2
CJOINUPTS - JOINS UP TO 88 FILES WITHOUT CHECKING.                     88  1    B1
C                                                                               B3
CAUTHOR  - J.D.HENDERSON                                                        B3
C                                                                               B3
CPURPOSE - JOINS TOGETHER UP TO 88 FILES (WITHOUT SEQUENCE CHECKING).           B3
C          NOTE - MAXIMUM RECORD LENGTH IS $BIJP10$ WORDS.                      B3
C                                                                               B3
CINPUT FILE(S)...                                                               B3
C                                                                               B3
C      IN11,... ,IN70 = INPUT FILES TO BE JOINED                                B3
C                       (UP TO 88 CAN BE USED)                                  B3
C                                                                               B3
COUTPUT FILE...                                                                 B3
C                                                                               B3
C      JOIN = FILE CONTAINING ALL OF THE ABOVE FILES JOINED.                    B3
C-------------------------------------------------------------------------
C
      use diag_sizes, only : SIZES_BLONP1xBLATxNWORDIO

      IMPLICIT REAL (A-H,O-Z),
     +INTEGER (I-N)

      integer, parameter :: LMAX = 10+SIZES_BLONP1xBLATxNWORDIO

      COMMON/ICOM/IBUF(LMAX)
C
C--------------------------------------------------------------------
      NF=90
      CALL JCLPNT(NF,10,
     1     11,12,13,14,15,16,17,18,19,20,
     2     21,22,23,24,25,26,27,28,29,30,
     3     31,32,33,34,35,36,37,38,39,40,
     4     41,42,43,44,45,46,47,48,49,50,
     5     51,52,53,54,55,56,57,58,59,60,
     6     61,62,63,64,65,66,67,68,69,70,
     7     71,72,73,74,75,76,77,78,79,80,
     8     81,82,83,84,85,86,87,88,89,90,
     9     91,92,93,94,95,96,97,98,
     +     6)
      NF=NF-2
      REWIND 10
      NRECS=0
C
      LLAB=0
      DO 210 N=11,10+NF
        REWIND N
C
        NR=0
 150    CALL FBUFFIN(N,IBUF,LMAX,K,LEN)
        IF (K.GE.0) GO TO 190
        IF (IBUF(1).EQ.NC4TO8("LABL"))THEN
          IF(LLAB.EQ.1)GOTO 150
          LLAB=1
        ENDIF
        CALL FBUFOUT(10,IBUF,LEN,K)
        NR=NR+1
        GO TO 150
C
 190    WRITE(6,6010) NR
        NRECS=NRECS+NR
        CLOSE(N)
 210  CONTINUE
C
      WRITE(6,6020) NRECS
      IF(NRECS.EQ.0) CALL                          XIT('JOINUPTS',-1)
      CALL                                         XIT('JOINUPTS',0)
C--------------------------------------------------------------------
 6010 FORMAT(' ',I10,' RECORDS COPIED')
 6020 FORMAT(' ',I10,' TOTAL RECORDS IN NEW FILE')
      END
