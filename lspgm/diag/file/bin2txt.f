      PROGRAM BIN2TXT
C     PROGRAM BIN2TXT(BIN,        CHAR,       OUTPUT,                   )       B2
C    1         TAPE11=BIN, TAPE12=CHAR, TAPE6=OUTPUT)
C     -----------------------------------------------                           B2
C                                                                               B2
C     JUL 03/03 - A.IRWIN,F.MAJAESS (REVISED FOR F90 AND LINUX PLATFORMS)       B2
C     DEC 18/02 - F.MAJAESS (REVISE TO SUPPORT UP TO 128-CHARACTERS PER LINE)   B2
C     MAR 05/02 - F.MAJAESS                                                     
C                                                                               B2
CBIN2TXT - CONVERTS "CHAR" KIND CCRN BINARY RECORD(S) BACK TO TEXT      1  1    B1
C                                                                               B3
CAUTHOR  - F.MAJAESS                                                            B3
C                                                                               B3
CPURPOSE - CONVERTS "CHAR" KIND CCRN BINARY RECORD(S) TO TEXT.                  B3
C          NOTE: CONVERTED LINES ARE WRITTEN WITH 128 CHARACTERS PER LINE.      B3
C                THE FIELD NAME FOR THE BINARY RECORD IN THE 8-WORD LABEL IS    B3
C                WRITTEN AS THE FIRST LINE IN "CHAR" FILE (4 CHARACTERS LONG;   B3
C                COLUMNS 3-6) WITH A " +" IN THE FIRST 2 COLUMNS.               B3
C                THE MAXIMUM NUMBER OF LINES THAT CAN BE HANDLED PER CCRN       B3
C                BINARY RECORD IS EQUVALENT TO 1000 128-CHARACTERS LINES        B3
C                EXCLUDING THE FIELD NAME LINE.                                 B3
C                                                                               B3
CINPUT FILE...                                                                  B3
C                                                                               B3
C      BIN = BINARY STANDARD CCRN FILE CONTAINING "CHAR" KIND RECORD.           B3
C                                                                               B3
COUTPUT FILE...                                                                 B3
C                                                                               B3
C      CHAR = TEXT FILE OF DATA IN THE CONVERTED RECORD.                        B3
C--------------------------------------------------------------------------
C     
C     * NCBUF = MAXIMUM NUMBER OF 128-CHARACTER LINES PER SET.
C
      IMPLICIT REAL (A-H,O-Z),
     +INTEGER (I-N)
      PARAMETER (NCBUF=1000,NDAT=1000*16,NDATV=NDAT*2)
      CHARACTER*128 CBUF(NCBUF)
      CHARACTER*8  FLINE(16)
      LOGICAL OK
      INTEGER NAME
      EQUIVALENCE (CBUF,FLINE)
      COMMON/ICOM/IBUF(8),IDAT(NDATV)
      COMMON/MACHTYP/MACHINE,INTSIZE
C-----------------------------------------------------------------------
     
      NFF=3
      CALL JCLPNT(NFF,11,-12,6)
     
      REWIND 11
      REWIND 12
C
C     * SET "MAXX" VALUE BASED ON "NDAT" AND THE "INTSIZE" VALUE
C     * COMPUTED IN "MACHTYP" COMMON BLOCK VIA THE CALL TO "JCLPNT".
C
      MAXX=NDAT*INTSIZE
C
C     * READ IN "CHAR" KIND BINARY RECORD IN "CBUF".
C
      NSET=0
  10  CALL GETFLD2(11,CBUF,NC4TO8("CHAR"),-1,-1,-1,IBUF,MAXX,OK)
      IF(.NOT.OK)THEN
       IF (NSET.GT.0) THEN
        CALL                                       XIT('BIN2TXT',0)
       ELSE
        CALL                                       XIT('BIN2TXT',-1)
       ENDIF
      ENDIF
      CALL PRTLAB (IBUF) 
C
C     * EXTRACT AND WRITE OUT THE NAME OF THE FIELD.
C
      NAME=IBUF(3)
      WRITE(6,6010) NAME
      WRITE(12,1210) NAME
C
C     * EXTRACT THE NUMBER OF 8-BYTE ELEMENTS PER LINE, AND THE
C     * NUMBER OF LINES.
C
      NWDPLN=IBUF(5)
      NLINE=IBUF(6)
C     PRINT *,' NWDPLN=',NWDPLN,', NLINE=',NLINE
C
C     * WRITE OUT THE RECORD READ IN...
C
      IF ( NLINE.EQ.1) THEN

C       * PARTIAL 128 CHARACTERS LINE CASE.

        WRITE(6,1000) (FLINE(I),I=1,NWDPLN)
        WRITE(12,1000) (FLINE(I),I=1,NWDPLN)
        WRITE(6,6005) NWDPLN,NSET+1

      ELSE
       
C       * FULL 128 CHARACTERS PER LINE CASE.

        DO I=1,NLINE
         WRITE(6,1010) CBUF(I)
         WRITE(12,1010) CBUF(I)
        ENDDO
        WRITE(6,6000) NLINE,NSET+1

      ENDIF
C
      NSET=NSET+1
C 
C     * GO BACK AND POSSIBLY PROCESS ANOTHER "CHAR" BINARY RECORD.
C
      GO TO 10
C-----------------------------------------------------------------------
 1000 FORMAT(16A8)
 1010 FORMAT(A128)
 1210 FORMAT(' +',A4)
 6000 FORMAT(/,' BIN2TXT PROCESSED ',I5,' LINES FOR "CHAR" ',
     +                        'BINARY RECORD # ',I5,'.',/)
 6005 FORMAT(/,' BIN2TXT PROCESSED ',I5,' 8-BYTE WORDS FOR ',
     +                 '"CHAR" BINARY RECORD # ',I5,'.',/)
 6010 FORMAT(/,' +',A4)
      END
