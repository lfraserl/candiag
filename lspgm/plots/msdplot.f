      PROGRAM MSDPLOT
C     PROGRAM MSDPLOT (YAXIS,       XAXIS,       INPUT,       OUTPUT,   )       A2
C    1           TAPE1=YAXIS, TAPE2=XAXIS, TAPE5=INPUT, TAPE6=OUTPUT)
C     ---------------------------------------------------------------           A2
C
C     FEB 02/10 - F.MAJAESS (ADD "NCURRCOLRESET" TO "CCCAGCHCU" COMMON BLOCK)   A2
C     MAY 07/08 - F.MAJAESS (MAKE "ICURVCOL" DIMENSION INLINE WITH ITS          
C                            CORRESPONDING DECLARATION IN "AGCHCU_BD")          
C     SEP 18/06 - F.MAJAESS (MODIFIED FOR TOLERANCE IN SPVAL)                   
C     OCT 10/00 - M. BERKLEY (COPY FROM CRVPLOT)                                
C                                                                               A2
CMSDPLOT - PLOT CURVES IN THE X - Y PLANE                               2  1 C  A1
C                                                                               A3
CAUTHORS - F.ZWIERS, R.LAPRISE, M.BERKLEY                                       A3
C                                                                               A3
CPURPOSE - READ SETS OF POINTS FROM FILES, PLOT ON MSD CURVE, AND PLOT          A3
C          A CURVE THROUGH THE POINTS.                                          A3
C                                                                               A3
CINPUT FILES...                                                                 A3
C                                                                               A3
C      YAXIS = BINARY FILE FROM WHICH Y-VALUES ARE READ IN                      A3
C      XAXIS = BINARY FILE FROM WHICH x-VALUES ARE READ IN                      A3
C                                                                               A3
CCARDS READ...                                                                  A5
C                                                                               A5
C      UP TO FIVE INPUT CARDS ARE READ.                                         A5
C      EVERY SET OF INPUT CARDS DEFINES THE NUMBER OF CURVES ON THAT PLOT.      A5
C                                                                               A5
C      CARD 1:                                                                  A5
C      -------                                                                  A5
C                                                                               A5
C      READ (5,5010) NT,NAME,LVL,NPNTS,NC,ICURVE                                A5
C 5010 FORMAT(10X,I10,1X,A4,I5,I4,2I2)                                          A5
C                                                                               A5
C      NT     = TIMESTEP NUMBER                                                 A5
C      NAME   = NAME OF VARIABLE                                                A5
C      LVL    = LEVEL NUMBER                                                    A5
C      NPNTS  = MAX. NO. OF POINTS PER CURVE.                                   A5
C      NC     = MAX. NO. OF CURVES PER PLOT (NC <= 26)                          A5
C      ICURVE = 0 - DO NOT DRAW CURVE THROUGH POINTS.                           A5
C             = 1 - DRAW CURVE THROUGH POINTS       .                           A5
C             = 2 - DRAW CURVES IN COLOUR, USING SHADES ON CARD 3.              A5
C                                                                               A5
C      CARD 2:                                                                  A5
C      -------                                                                  A5
C      READ (5,5020) IAX,JAX,TITLE                                              A5
C 5020 FORMAT(2(20A1),40A1)                                                     A5
C                                                                               A5
C      IAX   = 20 CHAR. LABEL OF X-AXIS                                         A5
C      JAX   = 20 CHAR. LABEL OF Y-AXIS                                         A5
C      TITLE = 40 CHAR. LABEL OF PLOT                                           A5
C                                                                               A5
C      NOTE - ALL LEADING AND TRAILING SPACES TO THE LABELS ARE STRIPPED        A5
C             AND THEY ARE CENTERED AUTOMATICALLY.                              A5
C                                                                               A5
C      CARD 3                                                                   A5
C      ------                                                                   A5
C                                                                               A5
C      IF CARD 3 IS PROVIDED, THEN CURVES WILL BE COLOURED, WITHOUT             A5
C      DASH PATTERNS.  IF THERE ARE MORE CURVES THAN SHADES, THEN               A5
C      MSDPLOT WILL START OVER WITH SHADE 1, USING A DIFFERENT DASH             A5
C      PATTERN FOR THE SECOND SET OF CURVES, ETC.                               A5
C                                                                               A5
C      READ(5,5050) NPAT, (IPAT(I),I=1,7)                                       A5
C 5050 FORMAT(10X,1X,I4,7I5)                                                    A5
C                                                                               A5
CEXAMPLE OF INPUT CARDS...                                                      A5
C                                                                               A5
C* MSDPLOT        -1 NEXT    1 1                                                A5
C* DELTA^2/SIGMA^2                                                              A5
C*            7   52    0   48    0   48    0   52                              A5
C-----------------------------------------------------------------------------
C

C     * VARIABLES:
C     * EQUIVALENCE(AIAX, IAX) - LABEL FOR X-AXIS
C     * EQUIVALENCE(AJAX, JAX) - LABEL FOR Y-AXIS
C     * EQUIVALENCE(ATITLE, TITLE) - TITLE OF GRAPH
C     * PAT - ARRAY OF 26 DASHED-LINE PATTERNS
C     * NGWND - AN ARRAY THAT HOLDS THE NUMBER OF GRAPHS PER PAGE FOR EACH
C     *         VALUE OF NFRM.
C     * GWND - A 3D ARRAY CONTAINING THE GRAPH WINDOW LOCATIONS FOR EACH
C     *        VALUE OF NFRM.  E.G. FOR GWND(I,J,K), I=(1, 2, 3, 4) FOR THE
C     *        LEFT, RIGHT, BOTTOM & TOP CORNERS OF THE WINDOW RESPECTIVELY;
C     *        J=(1, 2, ... , NGWND(NFRM)) SPECIFIES WHICH WINDOW; AND
C     *        K=(1, 2, ... , MAXFRM) FOR WHATEVER NFRM IS.
C     * NPLOT - KEEPS TRACK OF HOW MANY PLOTS STILL HAVE TO BE DRAWN ON
C     *         A PAGE
C     * NPLT - THE NUMBER OF PLOTS THAT HAVE BEEN DRAWN
C     * X - HOLDS THE X VALUES TO BE PLOTTED (I.E. PASSED TO 'EZMXY')
C     * Y - HOLDS THE Y VALUES TO BE PLOTTED (I.E. PASSED TO 'EZMXY')
C     * R - HOLDS THE R VALUES TO BE PLOTTED (I.E. PASSED TO 'EZMXY')
C     * GX - IS A TEMPORARY ARRAY WHICH X VALUES ARE READ INTO
C     * GY - IS A TEMPORARY ARRAY WHICH Y VALUES ARE READ INTO
C     * GR - IS A TEMPORARY ARRAY WHICH R VALUES ARE READ INTO


C     Load diagnostic size values
      use diag_sizes, only : SIZES_LONP1xLAT,
     &                       SIZES_LONP1xLATxNWORDIO
      integer, parameter :: LONP1xLATx26 = 26*SIZES_LONP1xLAT

      INTEGER MAXWND, MAXFRM
      PARAMETER (MAXWND = 4, MAXFRM = 4)

      LOGICAL XAXS,OK
      REAL X(LONP1xLATx26), Y(LONP1xLATx26)
      REAL FXLFT, FXRIT, FYLO, FYHI, YLO, YHI
      REAL UCOORD(4), NMLBDT(11), TCKDAT(3), FSCALE
      INTEGER IBUF(8),PTYPE,CBASE,CWDS,LNT(26),NGWND(4)
      INTEGER NPLOT, NPLT, ILLL
      REAL GWND(4,MAXWND,MAXFRM), XINC

C     SHADES FOR COLOUR CURVES
      LOGICAL COLOURCURVES
      INTEGER NCURVCOL,MAXNCURVCOL,ICURVCOL(26)
      INTEGER IPATNE,NCURRCOLRESET
      COMMON /CCCAGCHCU/ COLOURCURVES,NCURVCOL,MAXNCURVCOL,ICURVCOL,
     1     PAT,IPATNE,NCURRCOLRESET

C     Array to pass user defined colours to dfclrs - unused in MSDPLOT
      REAL HSVV(3, 16)

      CHARACTER AIAX*23,AJAX*23,ATITLE*43,PAT(26)*16
      CHARACTER*1 IAX(23),JAX(23),TITLE(43),ICHR(26),IPLUS,PLTCHR
      CHARACTER STRING*24, TMP*8, PLTNAM*8
      EQUIVALENCE (IAX,AIAX),(JAX,AJAX),(TITLE,ATITLE)

      EQUIVALENCE (UCOORD(1),FXLFT),(UCOORD(2),FXRIT)
      EQUIVALENCE (UCOORD(3),FYLO),(UCOORD(4),FYHI)

C     logical unit numbers for saving card images, data, etc.
      INTEGER LUNCARD,UNIQNUM,LUNDATAFILES(2)
      PARAMETER (LUNOFFSET=0,
     1     LUNFIELD1=LUNOFFSET+1,
     2     LUNFIELD2=LUNOFFSET+2)

      COMMON /ICOM/ IBUFX(8),IDATX(SIZES_LONP1xLATxNWORDIO)
      COMMON /JCOM/ IBUFY(8),IDATY(SIZES_LONP1xLATxNWORDIO)

      COMMON/BLANCK/ GX(SIZES_LONP1xLAT),GY(SIZES_LONP1xLAT)

      COMMON /INTPR/ZZZZZZ(2),TENSN,ZZZZZY(7)
      COMMON /PUSER/ MODE

C
C     * COMMON BLOCK TO TELL AGCHNL WHICH PROGRAM IS CALLING IT AND
C     * WHAT TYPE OF X-AXIS (ZONAL/MERIDIONAL) IS BEING DRAWN.
C
      COMMON / AGNMLB / PLTNAM, LAT
C
C     * DECLARE NCAR PLUGINS EXTERNAL, SO CORRECT VERSION ON SGI
C
      EXTERNAL AGCHCU
      EXTERNAL AGCHNL
      EXTERNAL AGPWRT

C
C     * DECLARE BLOCK DATAS EXTERNAL, SO INITIALIZATION HAPPENS
C
      EXTERNAL AGCHCU_BD
      EXTERNAL FILLCBBD
      EXTERNAL FILLPAT_BD

      DATA MAXX/SIZES_LONP1xLATxNWORDIO/, MAXWDS/LONP1xLATx26/
      DATA ICHR/'A','B','C','D','E','F','G','H','I','J',
     1          'K','L','M','N','O','P','Q','R','S','T',
     2          'U','V','W','X','Y','Z'/
      DATA IPLUS/'+'/

      DATA NGWND / 1, 2, 2, 4 /

      DATA (GWND(I,1,1),I=1,4) / 0.0, 1.0, 0.0, 1.0 /

      DATA (GWND(I,2,2),I=1,4) / 0.0, 1.0, 0.5, 1.0 /
      DATA (GWND(I,1,2),I=1,4) / 0.0, 1.0, 0.0, 0.5 /

      DATA (GWND(I,2,3),I=1,4) / 0.0, 0.5, 0.0, 1.0 /
      DATA (GWND(I,1,3),I=1,4) / 0.5, 1.0, 0.0, 1.0 /

      DATA (GWND(I,4,4),I=1,4) / 0.0, 0.5, 0.5, 1.0 /
      DATA (GWND(I,3,4),I=1,4) / 0.5, 1.0, 0.5, 1.0 /
      DATA (GWND(I,2,4),I=1,4) / 0.0, 0.5, 0.0, 0.5 /
      DATA (GWND(I,1,4),I=1,4) / 0.5, 1.0, 0.0, 0.5 /

      DATA SPVAL/1.E+38/
C-----------------------------------------------------------------------

C     * OPEN ALL FILES
      NF=4
      CALL JCLPNT(NF,LUNFIELD1,LUNFIELD2,5,6)

C     * SETUP IN "SPVALT" THE TOLERANCE TO CHECK "SPVAL" AGAINST.

      SPVALT=1.E-6*SPVAL

C     * SET NCAR TENSION
C     TENSN=9.0
C
C
C     * SET THRESHOLD FOR THE MINIMUM NUMBER OF POINTS
C     * PASSED TO NCAR PLOTTING ROUTINES.
C
C      MINPTS=144
C      MINPTS=129
      MINPTS=1000


C     * START UP NCAR GRAPHICS PACKAGE
      CALL PSTART
      CALL GSFAIS (1)
      CALL BFCRDF(0)

      NF=NF-2

      XAXS=.FALSE.
      IF(NF.EQ.2) XAXS=.TRUE.

C     * TELL PWRITX TO USE THE DUPLEX CHARACTER FONT
      MODE = 1

      REWIND LUNFIELD1
      REWIND LUNFIELD2

C
C     * SET THE PLOT NAME FOR AGCHNL AND SET THE LINE THICKNESSES FOR
C     * AGCHCU.
C
      PLTNAM = 'MSDPLOT'
      CALL GSLWSC(2.0)
      CALL GSMKSC(2.0)

C     * READ CONTROL INFORMATION AND LABELS ON 2,3 OR 4 INPUT CARDS.

      NPLOT=0
      NPLT=0
      NFOLD=1

      CALL PCSETI('FN', 12)
      CALL PCSETI('OF', 1)
      CALL PCSETI('OL', 2)

C     * CHANGE THE LINE-END CHARACTER TO '!'.
      CALL AGSETC('LINE/END.', '!')

C     * SET THE LENGTH OF THE DASH-PATTERN STRINGS & THE SIZE OF THE
C     * CHARACTERS DRAWN IN THE DASHED-LINES.
      CALL AGSETF('DASH/LENGTH.', 16.0)
      CALL AGSETF('DASH/CHARACTER.', 0.02)

C     * MAKE THE NUMERIC LABELS BIGGER.
      CALL AGSETF('LEFT/WIDTH/MANTISSA.', 0.020)
      CALL AGSETF('LEFT/WIDTH/EXPONENT.', 0.013333)
      CALL AGSETF('BOTTOM/WIDTH/MANTISSA.', 0.020)
      CALL AGSETF('BOTTOM/WIDTH/EXPONENT.', 0.013333)

C     * MAKE THE TOP LABEL BIGGER.
      CALL AGSETC('LABEL/NAME.', 'T')
      CALL AGSETF('LINE/NUMBER.', 100.0)
      CALL AGSETF('LINE/CHARACTER.', 0.026666)

C     * MAKE THE LEFT LABEL BIGGER.
      CALL AGSETC('LABEL/NAME.', 'L')
      CALL AGSETF('LINE/NUMBER.', 100.0)
      CALL AGSETF('LINE/CHARACTER.', 0.020)

C     * MAKE THE BOTTOM LABEL BIGGER.
      CALL AGSETC('LABEL/NAME.', 'B')
      CALL AGSETF('LINE/NUMBER.', -100.0)
      CALL AGSETF('LINE/CHARACTER.', 0.020)

C     * SET UP THE SCALE LABEL IN THE TOP LEFT HAND CORNER FOR
C     * SCIENTIFIC NOTATION.
      CALL AGSETC('LABEL/NAME.', 'SCALE')
      CALL AGSETF('LABEL/BASEPOINT/X.', 0.0)
      CALL AGSETF('LABEL/BASEPOINT/Y.', 0.9)
      CALL AGSETF('LABEL/OFFSET/X.', -0.015)
      CALL AGSETF('LABEL/ANGLE.', 90.0)

C     * MAKE THE SCALING LABEL BIGGER.
      CALL AGSETF('LINE/NUMBER.', 100.0)
      CALL AGSETF('LINE/CHARACTER.', 0.020)

C
C     * SHUT OFF INFORMATION LABEL SCALING.  AUTOGRAPH WILL ONLY BE ABLE TO
C     * PREVENT INFORMATION LABELS FROM OVERLAPPING BY MOVING THEM.
C
      CALL AGSETF('LABEL/CONTROL.', 1.0)

C     * GET BASIC PLOT PARAMETERS INCLUDING THE RANGE
C     * AND SIMULATE A 'WHILE NOT(EOF)' LOOP.
C
C     * INITIALIZE COLOURCURVES TO FALSE
      COLOURCURVES=.FALSE.
C
C    * READ IN INPUT PARAMETERS... ** CARD 1 **

  110 READ (5,5010,END=901) NT,NAME,LVL,NPNTS,NC,ICURVE                         

      IF(ICURVE.EQ.2) COLOURCURVES=.TRUE.

C     * GET THE LABELS  ** CARD 2 **
      READ (5,5020,END=902) (IAX(I),I=1,20),(JAX(I),I=1,20),
     1     (TITLE(I),I=1,40)


C     * SET THE X & Y LABELS AND THE DASHED LINE PATTERNS
      CALL SHORTN(IAX, 23, .TRUE., .TRUE., ILEN)
      CALL SHORTN(JAX, 23, .TRUE., .TRUE., JLEN)
      IF(COLOURCURVES) THEN
         CALL ANOTAT(AIAX(4:ILEN)//'!', AJAX(4:JLEN)//'!',
     1        0,0,0,PAT)
      ELSE
         CALL ANOTAT(AIAX(4:ILEN)//'!', AJAX(4:JLEN)//'!',
     1        0,0,26,PAT)
      ENDIF


C     CLIP GRAPHICS
      CALL AGSETI('WINDOW.', 0)
C     MAKE NICE X/Y AXIS TICKS
      CALL AGSETF('X/NICE.', -1.0)
      CALL AGSETF('Y/NICE.', -1.0)

C     ** CARD 3 **
      IF(COLOURCURVES) THEN
         READ(5,5050) NCURVCOL,(ICURVCOL(I),I=1,MAXNCURVCOL)
         CALL DFCLRS(-NCURVCOL,HSVV,ICURVCOL)
      ENDIF

      WRITE(6,6010) NT,NAME,LVL,NPNTS,NC,ICURVE

C
C     * THIS SECTION READS IN THE X,Y DATA FOR THE PLOTS
C-----------------------------------------------------------------------

C     * REWIND INPUT FILES IF NECESSARY

      NCURV=0
      REWIND LUNFIELD1
      REWIND LUNFIELD2

C     * READ UP TO NC CURVES FROM FILE CURV BASED ON INPUT CARD INFO.

      CWDS = 0

C     * SIMULATE 'REPEAT-UNTIL' LOOP
 200  CONTINUE

      CALL GETFLD2(LUNFIELD1,GY,-1,NT,NAME,LVL,IBUFY,MAXX,OK)
      IF(.NOT.OK .AND. NCURV.EQ.0) THEN
         CALL                                      PXIT('MSDPLOT',-101)
      ENDIF
      IF(OK)THEN
         IF(IBUFY(1).EQ.NC4TO8('SPEC')
     1        .OR. IBUFY(1).EQ.NC4TO8('FOUR')
     2        .OR. IBUFY(1).EQ.NC4TO8('LABL')
     3        .OR. IBUFY(1).EQ.NC4TO8('CHAR')
     4        .OR. IBUFY(6).NE.1) THEN
            CALL                                   PXIT('MSDPLOT',-1)
         ENDIF
         IF(IBUFY(8).LE.0) WRITE(6,6090) IBUFY(8)
         NCURV=NCURV+1

C     * SET LENGTH OF CURVE BASED ON FIRST RECORD.

         IF((NCURV.EQ.1).AND.(MLTRX.EQ.0)) THEN
            DO 210 I=1,8
               IBUF(I)=IBUFY(I)
 210        CONTINUE
         ENDIF

         IF ((NCURV.GT.1).AND.(MLTRX.EQ.0).AND.
     1        (IBUFY(5).NE.IBUF(5))) CALL          PXIT('MSDPLOT',-2)

         NWDS=IBUFY(5)*IBUFY(6)
         LNT(NCURV)=MIN(NWDS,NPNTS)
         IF(LNT(NCURV).LT.2) CALL                  PXIT('MSDPLOT',-3)

         IF((CWDS+NPNTS) .GT. MAXWDS) GOTO 300

C
C     * ACCUMULATE CURVE VALUES IN MULTI-CURVE ARRAY.
C
         CBASE = CWDS
         CWDS = CWDS + NPNTS
         LMIN = MIN(NWDS, NPNTS)

C     * THE TASK OF THE FOLLOWING LOOP IS TO FIRST REVERSE
C     * THE ORDER OF THE ELEMENTS IF "YFLIP=.TRUE.", THEN
C     * TO SKIP FROM THE BEGINNING OR THE END (IF APPICABLE).
C     * NOTE: THE Y-ARRAY IS POSSIBLY REDUCED IN LENGTH
C     *       TO LMIN-NSKIP ELEMENTS IN LOOP 335 BELOW.

C     * REPLACE SPVAL BY 1.E36, THE MISSING VALUE IN NCAR

         DO L=1,NPNTS
            IF (ABS(GX(L)-SPVAL).LE.SPVALT) GX(L)=1.E36
         ENDDO

         DO 220 L = 1, LMIN
            Y(CBASE + L) = GY(L)
 220     CONTINUE
C

         DO 221 L = LMIN+1, NPNTS
            Y(CBASE + L) = 1.E36
 221     CONTINUE

      ELSE

         NC=NCURV
         GO TO 300

      ENDIF

C     * READ X-AXIS IF NECESSARY

      IF (XAXS.AND.((MLTRX.NE.0).OR.
     1     ((MLTRX.EQ.0).AND.(NCURV.EQ.1)))) THEN
         CALL GETFLD2(LUNFIELD2,
     1        GX,-1,NT,NAME,LVL,IBUFX,MAXX,OK)
         IF((IBUFX(1).NE.IBUFY(1) .AND. IBUFX(5).NE.IBUFY(5)
     1        .AND. IBUFX(6).NE.IBUFY(6)) .OR.  .NOT.OK)
     2        CALL                                 PXIT('MSDPLOT',-4)

         IF(IBUFX(8).LE.0) WRITE(6,6090) IBUFX(8)
        

C                  * REPLACE SPVAL BY 1.E36, THE MISSING VALUE IN NCAR

                   DO L=1,NPNTS
                      IF (ABS(GX(L)-SPVAL).LE.SPVALT) GX(L)=1.E36
                   ENDDO

C                  * ACCUMULATE X-VALUES

                   DO 230 L=1,NPNTS
                        IF (L .GT. NSKIP .AND. L .LE. NWDS) THEN
                             X(CBASE+L) = GX(L)
                        ELSE
                             X(CBASE + L) = 1.E36
                        ENDIF
  230              CONTINUE
              ENDIF

C             * GO BACK AND REPEAT LOOP UNTIL NCURV .LT. NC
              IF (NCURV.LT.NC) GO TO 200
  300      CONTINUE


C          * FIND THE MOST NUMBER OF POINTS THE CURVES WILL HAVE.
           LMAX = 0
           DO 310 I=1,NCURV
              IF(LNT(I).GT.LMAX) LMAX=LNT(I)
  310      CONTINUE
           WRITE(6,6998) NCURV,LMAX,IXAXS,XAXS


C          * CHECK IF THERE IS NO FILE FOR X DATA
           IF (.NOT. XAXS) THEN

C               * IF NO X DATA EXISTS AND FXLFT, FXRIT HAVEN'T BEEN SPECIFIED
C               * THEN THE X DATA WILL JUST BE THE POINT NUMBERS.
                IF (IXAXS .EQ. 0) THEN
                     XINC = 1.0
                     FXLFT = 1.0
                     FXRIT = FLOAT(LMAX)

C               * IF WE GET HERE, THEN FXLFT AND FXRIT MUST HAVE BEEN SPECIFIED
C               * BUT THERE IS NO X DATA.  THE X DATA IS LINEARLY INTERPOLATED
C               * BETWEEN FXLFT AND FXRIT.
                ELSE
                     XINC=(FXRIT-FXLFT)/FLOAT(LMAX-1)
                ENDIF
           ENDIF


C          * REPACK THE ARRAYS AND SET UP XAXIS IF NECESSARY

           DO 335 I=1,NCURV
              CBASE = (I-1)*NPNTS
              NBASE = (I-1)*LMAX

              DO 320 L=1,LMAX
                   Y(NBASE + L)=Y(CBASE + NSKIP + L)
  320         CONTINUE

              IF(XAXS)THEN
                 DO 325 L=1,LMAX
                    X(NBASE+L)=X(CBASE+L+NSKIP)
  325            CONTINUE
              ELSE
                 DO 330 L=1,LMAX
                    IF(L.LE.LNT(I))THEN
                       X(NBASE+L)=FXLFT+XINC*FLOAT(L-1)
                    ELSE
                       X(NBASE+L)=1.E36
                    ENDIF
  330            CONTINUE
              ENDIF
  335      CONTINUE

C-----------------------------------------------------------------------

C          * SET THE AXIS RANGES

           IF(IXAXS.EQ.0 .AND. .NOT.XAXS) FXRIT=FLOAT(LMAX)
           WRITE(6,6999) LMAX,FXLFT,FXRIT,FYLO,FYHI

           IF(IXAXS.NE.0) WRITE(6,6030) FXLFT,FXRIT


C-----------------------------------------------------------------------
C
C        *  DOUBLE RESOLUTION BY INTERPOLATION IF THE NUMBER OF
C        *  POINTS TO PLOT PER CURVE IS LESS THAN MINPTS.
C
         ISUBST=0
  350    IF ( LMAX.GE.MINPTS .OR. ISCAT.EQ.1 ) GO TO 390

         NLMAX=2*LMAX-1
         IF (NLMAX.GT.MAXWDS) THEN
           WRITE(6,6100)
           CALL                                    PXIT('MSDPLOT',-5) 
         ENDIF
         DO 385 I = NCURV,1,-1
          NBASE=(I-1)*LMAX
          NNBASE=(I-1)*NLMAX

          Y(NNBASE+NLMAX)=Y(NBASE+LMAX)
          X(NNBASE+NLMAX)=X(NBASE+LMAX)
C
          DO 380 J = LMAX-1, 1, -1
           IF (Y(NBASE+J).NE.1.E36.AND.Y(NBASE+J+1).NE.1.E36) THEN
             IF(LTYPE.NE.2.AND.LTYPE.NE.4)THEN
              Y(NNBASE+2*J)=0.5*(Y(NBASE+J)+Y(NBASE+J+1))
             ELSE
              IF(Y(NBASE+J).EQ.0.0.AND.Y(NBASE+J+1).EQ.0.0) THEN
               Y(NNBASE+2*J)=0.0
              ELSE
               IF(Y(NBASE+J).EQ.0.0) THEN
                Y(NNBASE+2*J)=1.E36
                ISUBST=1
               ELSE
                IF(Y(NBASE+J+1).EQ.0.0) THEN
                 Y(NNBASE+2*J)=1.E36
                 ISUBST=1
                ELSE
                 Y(NNBASE+2*J)=10.**(0.5*(LOG10(Y(NBASE+J))+
     1                                    LOG10(Y(NBASE+J+1))))
                ENDIF
               ENDIF
              ENDIF
             ENDIF
           ELSE
             Y(NNBASE+2*J)   = 1.E36
           ENDIF
           Y(NNBASE+2*J-1) = Y(NBASE+J)
C
           IF (X(NBASE+J).NE.1.E36.AND.X(NBASE+J+1).NE.1.E36) THEN
             IF(LTYPE.NE.3.AND.LTYPE.NE.4)THEN
              X(NNBASE+2*J)=0.5*(X(NBASE+J)+X(NBASE+J+1))
             ELSE
              IF(X(NBASE+J).EQ.0.0.AND.X(NBASE+J+1).EQ.0.0) THEN
               X(NNBASE+2*J)=0.0
              ELSE
               IF(X(NBASE+J).EQ.0.0) THEN
                X(NNBASE+2*J)=1.E36
                ISUBST=1
               ELSE
                IF(X(NBASE+J+1).EQ.0.0) THEN
                 X(NNBASE+2*J)=1.E36
                 ISUBST=1
                ELSE
                 X(NNBASE+2*J)=10.**(0.5*(LOG10(X(NBASE+J))+
     1                                    LOG10(X(NBASE+J+1))))
                ENDIF
               ENDIF
              ENDIF
             ENDIF
           ELSE
             X(NNBASE+2*J)   = 1.E36
           ENDIF
           X(NNBASE+2*J-1) = X(NBASE+J)

  380     CONTINUE
          LNT(I)=NLMAX
  385    CONTINUE
         LMAX=NLMAX
         GO TO 350

  390    CONTINUE
         IF ( ISUBST .NE. 0 ) WRITE(6,6070)


C-----------------------------------------------------------------------
C
C          * IF THIS PLOT IS GOING ON A NEW PAGE THEN SET THE NUMBER OF
C          * PLOTS LEFT TO DRAW ON THIS PAGE AND STORE THE OLD VALUE OF
C          * NFRM IN NFOLD.
C
           IF (NPLOT .EQ. 0) THEN
                IF (NFRM .EQ. 0) NFRM = 1
                NPLOT = NGWND(NFRM)
                NFOLD = NFRM

C
C          * IF NFRM DOES NOT AGREE WITH THE OLD VALUE OF NFRM (NFOLD)
C          * THEN START ON A NEW PAGE AND RESET THE VALUES FOR NPLOT
C          * AND NFOLD AS ABOVE.
C
           ELSEIF (NFRM .NE. NFOLD) THEN
                CALL FRAME
                CALL RESET

                IF (NFRM .EQ. 0) NFRM = 1
                NPLOT = NGWND(NFRM)
                NFOLD = NFRM
           ENDIF

C
C          * SET THE WINDOW IN WHICH THE GRAPH IS TO BE DRAWN
C
           CALL AGSETP('GRAPH WINDOW.', GWND(1, NPLOT, NFOLD), 4)

C
C          * SUPPRESS THE FRAME ADVANCE, SET THE X ARRAY DIMENSION TO
C          * ONE OR TWO DIMENSIONS, WHATEVER MLTRX SPECIFIES, AND SET
C          * THE LINEAR OR LOGARITHMIC NATURE OF THE PLOT.
C
           CALL DISPLA(2, MLTRX + 1, LTYPE)

           IF(PTYPE.NE.0) WRITE(6,6040) MGRX,MINRX,MGRY,MINRY

C          * IF A SCATTER PLOT IS TO BE DRAWN, SUPPRESS THE DRAWING
C          * OF CURVES.
           IF (ISCAT .EQ. 1) THEN
                CALL AGSETF('SET.', -1.0)
           ENDIF
C
C          * IF FXRIT AND FXLFT WERE SPECIFIED, THEN WE MUST MODIFY THE DEFAULTS.
C
           IF(IXAXS.NE.0) THEN
C
C            * TURN WINDOWING ON SO THAT ONLY THE PORTION OF THE
C            * CURVE IN THE X-AXIS RANGE IS DRAWN. MAJOR TICK MARKS
C            * DON'T HAVE TO BE AT THE ENDPOINTS OF THE X-AXIS.

C             CALL AGSETI('WINDOWING.', 1)
             CALL AGSETF('X/NICE.', 0.0)
           ENDIF

C
C          * DO THE SAME AS ABOVE FOR THE Y-AXIS IF FYLO AND FYHI HAVE BEEN
C          * SPECIFIED.
C
           IF (FYHI .GT. FYLO) THEN
                CALL AGSETI('WINDOWING.', 1)
                CALL AGSETF('Y/NICE.', 0.0)
           ENDIF

           IF (FXLFT .LE. FXRIT) THEN
                CALL AGSETF('X/MINIMUM.', FXLFT)
                CALL AGSETF('X/MAXIMUM.', FXRIT)
                CALL AGSETF('X/ORDER.', 0.0)
           ELSE
                CALL AGSETF('X/MINIMUM.', FXRIT)
                CALL AGSETF('X/MAXIMUM.', FXLFT)
                CALL AGSETF('X/ORDER.', 1.0)
           ENDIF
           CALL AGSETF('Y/MINIMUM.', FYLO)
           CALL AGSETF('Y/MAXIMUM.', FYHI)

C
C          * RUN AGSTUP TO MAKE AUTOGRAPH CALCULATE THE NUMERIC LABEL
C          * SETTINGS.  WE CAN CHECK THE 'SECONDARY' PARAMETERS TO MAKE
C          * SURE THE THE LABELS WILL NOT USE SCIENTIFIC NOTATION OR HAVE
C          * LABELS OF THE FORM 0.00001, 0.00002, 0.00003, ETC.  WHERE THERE
C          * ARE TOO MANY ZEROS.
C
           CALL AGSTUP(X,NCURV,LMAX,LMAX,1,Y,NCURV,LMAX,LMAX,1)

C
C          * GET THE NUMERIC LABEL INFORMATION ON THE Y AXIS.
C
           CALL AGGETP('SECONDARY/LEFT/NUMERIC.', NMLBDT, 11)
           CALL AGGETP('SECONDARY/LEFT/TICKS.', TCKDAT, 3)

           YLO = FYLO
           YHI = FYHI

C
C          * GET THE USER COORDINATES OF THE GRID WINDOW.
C
           CALL AGGETP('SECONDARY/USER.', UCOORD, 4)

C
C          * TAKE THE ABSOLUTE VALUES OF FYLO AND FYHI SET FSCALE EQUAL
C          * TO THE LOG OF WHICH EVER ONE IS HIGHER.
C
           IF (FYLO * FYHI .NE. 0) THEN
                FSCALE = MAX(ANINT(LOG10(ABS(FYLO))),
     1                         ANINT(LOG10(ABS(FYHI))))
           ELSE
                FSCALE = ANINT(LOG10(MAX(ABS(FYLO), ABS(FYHI))))
           ENDIF

C
C          * IF (THE NUMERIC LABEL USES SCIENTIFIC NOTATION) OR (THE
C          * NUMERIC LABEL *DOESN'T* USE SCIENTIFIC NOTATION *AND*
C          * THE LARGEST ABSOLUTE VALUE IS LESS THAN 0.01) THEN
C          * SCALE THE DATA ACCORDINGLY AND SHOW THE SCALING LABEL
C          * ALONG THE Y AXIS.
C
           IF ((NMLBDT(1) .EQ. 2.0 .AND. TCKDAT(1) .EQ. 1.0) .OR.
     1         (NMLBDT(1) .EQ. 3.0 .AND. ((FSCALE .LT. -2.0).OR.
     1                                    (FSCALE .GT.  4.0)))) THEN

C
C               * IF SCIENTIFIC NOTATION WAS USED, THE LOG OF THE
C               * SCALING FACTOR IS STORED IN NMLBDT(2) (WHICH IS ACTUALLY
C               * THE VALUE OF 'LEFT/EXPONENT.')
C
                IF (NMLBDT(1) .EQ. 2 .AND. TCKDAT(1) .EQ. 1) THEN
                     WRITE(TMP, 6000) INT(NMLBDT(2))
                     FSCALE = 10.0**(-1*NMLBDT(2))

C
C               * IF SCIENTIFIC NOTATION WASN'T USED, THEN WE'VE ALREADY
C               * CALCULATED THE LOG OF THE SCALING FACTOR IN FSCALE.
C
                ELSE
                     WRITE(TMP, 6000) INT(FSCALE)
                     FSCALE = 10.0**(-1*FSCALE)
                ENDIF

C
C               * HERE IS WHERE WE SCALE THE DATA.
C
                DO 400 I = 1, NCURV*LMAX
                     IF (Y(I) .NE. 1.E36) THEN
                          Y(I) = Y(I) * FSCALE
                     ENDIF
  400           CONTINUE

C
C               * CHANGE THE YHI AND YLO SETTINGS IF THEY WERE SPECIFIED
C               * BY THE USER
C
                IF (YLO .NE. 1.E36) THEN
                     CALL AGSETF('Y/MAXIMUM.', YHI*FSCALE)
                     CALL AGSETF('Y/MINIMUM.', YLO*FSCALE)
                ENDIF

C
C               * SET NUMERIC LABEL TYPE TO 'NO-EXPONENT' NOTATION.
C
                CALL AGSETF('LEFT/TYPE.', 3.)

C
C               * TURN ON THE SCALE LABEL IN THE TOP LEFT HAND CORNER FOR
C               * SCIENTIFIC NOTATION.
C
                CALL AGSETC('LABEL/NAME.', 'SCALE')
                CALL AGSETF('LABEL/SUPPRESSION.', 0.0)

C
C               * SET THE TEXT OF THE LABEL.
C
                CALL AGSETF('LINE/NUMBER.', 100.0)
                CALL SHORTN(TMP, 11, .TRUE., .TRUE., ILEN)
C                STRING='''L1''X10''H-7'''//TMP(4:ILEN)//'''H7''!'
                STRING='X10:S:'//TMP(4:ILEN)//':E:'
                CALL AGSETC('LINE/TEXT.', STRING)


                WRITE(6, *) 'SCALING LABEL SHOULD BE DRAWN.'
           ELSE

C
C               * MAKE SURE THE SCALE LABEL IS TURNED OFF.
C
                CALL AGSETC('LABEL/NAME.', 'SCALE')
                CALL AGSETF('LABEL/SUPPRESSION.', 1.0)

                WRITE (6, *) 'NO SCALING LABEL NEEDED.'
           ENDIF

           WRITE(6, 6997) NMLBDT
           WRITE(6, 6996) TCKDAT


C
C          * FIX UP THE TITLE AND DRAW THE GRAPH
C
           CALL SHORTN(TITLE, 43, .TRUE., .TRUE., ILEN)

           CALL EZMXY(X, Y, LMAX, NCURV, LMAX, ATITLE(4:ILEN)//'!')

C          * ACTIVATE "NCURRCOLRESET=1" LINE,IF NEED BE,TO 
C          * SETUP FOR RESETTING THE CURVE PATTERNS 

C          NCURRCOLRESET=1

C
C          * DRAW Y=0 LINE IF 0 IS IN THE RANGE
C
           IF(FYLO*FYHI .LT. 0.0) THEN
             CALL LINE (FXLFT, 0.0, FXRIT, 0.0)
           ENDIF

C
C          * IF THE GRAPH IS A SCATTER PLOT, PLOT THE POINTS.
C
           IF (ISCAT .EQ. 1) THEN
                DO 450 J = 1, NCURV
                     IF (NCURV .EQ. 1) THEN
                          PLTCHR = '+'
                     ELSE
                          PLTCHR = ICHR(J)
                     ENDIF

                     DO 440 I = NSKIP + 1, LNT(J)
                          CALL AGPWRT(X(I + (J - 1) * NPNTS),
     1                                Y(I + (J - 1) * NPNTS),
     1                                PLTCHR, 1, 12, 0, 0)
  440                CONTINUE
  450           CONTINUE
                CALL AGSETF('SET.', 1.0)
           ENDIF

           NPLOT = NPLOT - 1
           NPLT = NPLT + 1
           IF (NPLOT .EQ. 0) THEN
                CALL FRAME
                CALL RESET
           ENDIF

           WRITE(6,6060) NCURV,NPLT

C     * END 'WHILE NOT(EOF)' LOOP
      GO TO 110

C     * E.O.F. ON INPUT.

  901 IF (NPLT .EQ. 0) THEN
         CALL                                      PXIT('MSDPLOT',-6)
      ELSE
         IF (NPLOT .NE. 0) THEN
            CALL FRAME
         ENDIF
         CALL                                      PXIT('MSDPLOT',0)
      ENDIF
  902 IF (NPLT .EQ. 0) THEN
         CALL                                      PXIT('MSDPLOT',-7)
      ELSE
         IF (NPLOT .NE. 0) THEN
            CALL FRAME
         ENDIF
         CALL                                      PXIT('MSDPLOT',0)
      ENDIF
  903 IF (NPLT .EQ. 0) THEN
         CALL                                      PXIT('MSDPLOT',-8)
      ELSE
         IF (NPLOT .NE. 0) THEN
            CALL FRAME
         ENDIF
         CALL                                      PXIT('MSDPLOT',0)
      ENDIF
  904 IF (NPLT .EQ. 0) THEN
         CALL                                      PXIT('MSDPLOT',-9)
      ELSE
         IF (NPLOT .NE. 0) THEN
            CALL FRAME
         ENDIF
         CALL                                      PXIT('MSDPLOT',0)
      ENDIF

C-----------------------------------------------------------------------

 5010 FORMAT(10X,I10,1X,A4,I5,I4,2I2)                                           
 5020 FORMAT(2(20A1),40A1)                                                      
 5050 FORMAT(10X,1X,I4,7I5)                                                     

 6000 FORMAT(I8)
 6010 FORMAT('0MSDPLOT   ',I10,1X,A4,I5,I4,2I2)
 6030 FORMAT(31X,2G11.3)
 6040 FORMAT(11X,4I5)
 6060 FORMAT(I5,' CURVES ON PLOT NO',I5)
 6070 FORMAT(/,' *** WARNING: MISSING VALUE(S) SUBSTITUTED IN ',
     1         'INTERPOLATING LOGARITHMIC DATA ***',/)
 6090 FORMAT(/,' *** WARNING: PACKING DENSITY = ', I4,' ***',/)
 6100 FORMAT(/,' *** SORRY, THE ARRAY DIMENSIONS NOT LARGE',
     1         ' ENOUGH TO PERFORM LINEAR INTERPOLATION ***',/)
 6996      FORMAT(1X,'TCKDAT:',  3E11.4)
 6997      FORMAT(1X,'NMLBDT:', 11E11.4)
 6998      FORMAT(1X,'NCURV,LMAX,IXAXS,XAXS:',3I10,L10)
 6999      FORMAT(1X,'LMAX,FXLFT,FXRIT,FYLO,FYHI:',I10,4E12.4)

      END
