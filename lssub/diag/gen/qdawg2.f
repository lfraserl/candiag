      SUBROUTINE QDAWG2(U,V,Q,D,EPSI,LSR,LM)
C 
C     * JUL 14/92 - E. CHAN (ADD REAL*8 DECLARATIONS)
C     * AUG 15/79 - J.D.HENDERSON 
C     * CONVERTS GLOBAL SPECTRAL RELATIVE VORTICITY (Q) 
C     * AND DIVERGENCE (D) TO WIND COMPONENTS (U) AND (V).
C     * U,V HAVE THE SAME DIMENSIONS AS EPSI. 
C 
C     * LEVEL 2,U,V,Q,D,EPSI
      IMPLICIT REAL (A-H,O-Z),
     +INTEGER (I-N)
      COMPLEX U(2),V(2),Q(2),D(2) 
      REAL*8 EPSI(2)
C 
      INTEGER LSR(2,1)
      COMPLEX IZ
C-------------------------------------------------------------------- 
      IZ=(0.E0,1.E0)
C 
C     * FIRST LRW-2 COEFFICIENTS IN EACH WIND ROW.
C     * K INDEXES Q,D. IUV INDEXES U,V,EPSI.
C 
      DO 40 M=1,LM
      MS=M-1
      FMS=FLOAT(MS) 
C 
      KL=LSR(1,M) 
      KR=LSR(1,M+1)-2 
      IF(KR.LT.KL) GO TO 50 
      JL=LSR(2,M) 
      DO 20 K=KL,KR 
      NS=MS+(K-KL)
      IF(NS.EQ.0) GO TO 20
      FNS=FLOAT(NS) 
      CON=FMS/(FNS*(FNS+1.E0))
C 
      IUV=JL+(K-KL) 
      EPS=EPSI(IUV)/FNS 
      EPSR=EPSI(IUV+1)/(FNS+1.E0) 
      U(IUV)=-EPS*Q(K-1) + EPSR*Q(K+1) - IZ*D(K)*CON
      V(IUV)= EPS*D(K-1) - EPSR*D(K+1) - IZ*Q(K)*CON
   20 CONTINUE
C 
C     * LAST TWO COEFFICIENTS IN EACH ROW OF U,V. 
C 
      FNS=FNS+1.E0
      CON=FMS/(FNS*(FNS+1.E0))
      IUV=IUV+1 
      U(IUV)=-(EPSI(IUV)/FNS)*Q(KR) - IZ*D(KR+1)*CON
      V(IUV)= (EPSI(IUV)/FNS)*D(KR) - IZ*Q(KR+1)*CON
      IUV=IUV+1 
      FNS=FNS+1.E0
      U(IUV)=-(EPSI(IUV)/FNS)*Q(KR+1) 
      V(IUV)= (EPSI(IUV)/FNS)*D(KR+1) 
   40 CONTINUE
      GO TO 60
C 
C     * TRIANGULAR CASE - LAST ROW OF Q,D HAS ONLY ONE COEFF. 
C 
   50 IUV=LSR(2,LM) 
      K  =LSR(1,LM) 
      FNS=FLOAT(LM-1) 
      CON=FMS/(FNS*(FNS+1.E0))
      U(IUV)=-IZ*D(K)*CON 
      V(IUV)=-IZ*Q(K)*CON 
      IUV=IUV+1 
      FNS=FNS+1.E0
      U(IUV)=-(EPSI(IUV)/FNS)*Q(K)
      V(IUV)= (EPSI(IUV)/FNS)*D(K)
C 
C     * SET THE MEAN VALUES OF U AND V. 
C 
   60 U(1)= Q(2)*EPSI(2)
      V(1)=-D(2)*EPSI(2)
      RETURN
      END 
