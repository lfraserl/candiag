      SUBROUTINE UVWAVE(U,V,LON,N,NPOLE,A,B,IFLD)
C
C     * MAR 25/13 - S.KHARIN
C     * FIT A SIN/COS WAVE TO (U,V) DEFINED AT LONGITUDES LON AND
C     * RETURN ITS FOURIER COEFFICIENTS (A,B)
C
C     * IFLD=0 (ONE SCALAR FIELD U ONLY):
C
C     *  UFIT = A
C
C     * IFLD=1 (ONE U-VECTOR COMPONENT ONLY):
C
C     *  UFIT = A*COS(LON) + B*SIN(LON)
C
C     * IFLD=2 AND NPOLE=1 (U,V AT THE NORTH POLE):
C
C     *  UFIT = A*COS(LON) + B*SIN(LON)
C     *  VFIT = B*COS(LON) - A*SIN(LON)
C
C     * IFLD=2 AND NPOLE=-1 (U,V AT THE SOUTH POLE):
C
C     *  UFIT = A*COS(LON) + B*SIN(LON)
C     *  VFIT =-B*COS(LON) + A*SIN(LON)
C
      IMPLICIT NONE
      INTEGER I,N,NPOLE,IFLD
      REAL U(N),V(N),LON(N),A,B
      REAL COSD, SIND
      A=0.E0
      B=0.E0
      IF(IFLD.EQ.0)THEN
        DO I=1,N
          A=A+U(I)
        ENDDO
        A=A/FLOAT(N)
        RETURN
      ELSE IF(IFLD.EQ.1)THEN
        DO I=1,N
          A=A+U(I)*COSD(LON(I))
          B=B+U(I)*SIND(LON(I))
        ENDDO
        A=2.E0*A/FLOAT(N)
        B=2.E0*B/FLOAT(N)
        RETURN
      ELSE IF(IFLD.EQ.2)THEN
        IF(NPOLE.EQ.1)THEN
          DO I=1,N
            A=A+U(I)*COSD(LON(I))-V(I)*SIND(LON(I))
            B=B+U(I)*SIND(LON(I))+V(I)*COSD(LON(I))
          ENDDO
        ELSE IF(NPOLE.EQ.-1)THEN
          DO I=1,N
            A=A+U(I)*COSD(LON(I))+V(I)*SIND(LON(I))
            B=B+U(I)*SIND(LON(I))-V(I)*COSD(LON(I))
          ENDDO
        ELSE
C
C         * INVALID NPOLE
C
          CALL                                     XIT('UVWAVE',-1)
        ENDIF
        A=1.E0*A/FLOAT(N)
        B=1.E0*B/FLOAT(N)
        RETURN
      ELSE
C
C       * INVALID IFLD
C
        CALL                                       XIT('UVWAVE',-2)
      ENDIF
      END
