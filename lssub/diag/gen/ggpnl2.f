      SUBROUTINE GGPNL2(VAL,GG,ILG1,ILAT,DLAT,DLON,SLAT)
C
C     * SEP 18/06 - F.MAJAESS (MODIFIED FOR TOLERANCE IN SPVAL)
C     * NOV 02/04 - S.KHARIN (RETURN THE SPECIAL VALUE 1.0E+38 IF ANY
C     *                       OF SURROUNDING GRID POINTS IS SET TO IT)
C     * MAY  1/80 - J.D.HENDERSON
C     * LINEAR INTERPOLATION AT POINT (DLON,DLAT)
C     * IN GLOBAL GAUSSIAN GRID GG(ILG1,ILAT).
C
C     * LEVEL 2,GG
      IMPLICIT REAL (A-H,O-Z),
     +INTEGER (I-N)
      REAL GG(ILG1,ILAT)
      REAL SLAT(ILAT)
      DATA SPVAL /1.0E+38/
C-----------------------------------------------------------------------

C     * SETUP IN "SPVALT" THE TOLERANCE TO CHECK "SPVAL" AGAINST.

      SPVALT=1.E-6*SPVAL

      DI=360.E0/FLOAT(ILG1-1)
      FI=DLON/DI+1.E0
      I=INT(FI)
      X=FI-FLOAT(I)
C
      DO 110 K=1,ILAT
      J=K-1
  110 IF(DLAT.LT.SLAT(K)) GO TO 120
  120 IF(J.EQ.0) J=1
      IF(DLAT.GT.SLAT(ILAT)) J=ILAT-1
      Y=(DLAT-SLAT(J))/(SLAT(J+1)-SLAT(J))
C
C     * RETURN SPECIAL VALUE IF ANY OF SURROUNDING GRID POINTS CONTAINS
C     * THE SPECIAL VALUE (SIMPLE BUT WASTEFUL)
C
      IF ( ABS(GG(I  ,J  )-SPVAL).LE.SPVALT .OR.
     1     ABS(GG(I+1,J  )-SPVAL).LE.SPVALT .OR.
     2     ABS(GG(I,J+1  )-SPVAL).LE.SPVALT .OR.
     3     ABS(GG(I+1,J+1)-SPVAL).LE.SPVALT     ) THEN
        VAL=SPVAL
        RETURN
      ENDIF
C
C     * BI-LINEAR INTERPOLATION
C
      BOT=(1.E0-X)*GG(I,J)  +X*GG(I+1,J)
      TOP=(1.E0-X)*GG(I,J+1)+X*GG(I+1,J+1)
C
      VAL=(1.E0-Y)*BOT+Y*TOP
C
      RETURN
      END
