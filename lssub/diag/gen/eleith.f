      SUBROUTINE ELEITH(G,LSR,LM,LA,NSTAR,ETA3) 
C 
C     * AUGUST 21, 1979 - TED SHEPHERD. 
C     * JUNE 18, 1980 - MODIFIED BY TED SHEPHERD. 
C     * SUBROUTINE RETURNS L(K)=LEITH EDDY VISCOSITY COEFF. 
C 
      IMPLICIT REAL (A-H,O-Z),
     +INTEGER (I-N)
      INTEGER LSR(2,1)
C     * LEVEL 2,G 
      COMPLEX G(LA) 
C-----------------------------------------------------------------------
      DO 100 M=1,LM 
      KL=LSR(1,M) 
      KR=LSR(1,M+1)-1 
      DO 100 K=KL,KR
      NS=(M-1)+(K-KL) 
      X=FLOAT(NS)/FLOAT(NSTAR)
      IF(X.LE.0.15E0) F=0.E0
      IF(X.GT.0.15E0.AND.X.LE.0.33E0) F=-0.2E0*(X-0.15E0)
      IF(X.GT.0.33E0.AND.X.LE.0.65E0) F=(1.1E0*((X-0.38E0)**2))-0.04E0
      IF(X.GT.0.65E0) F=4.0E0*((X-0.55E0)**2)
      G(K)=F*ETA3 
  100 CONTINUE
      RETURN
      END 
