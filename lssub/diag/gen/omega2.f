      SUBROUTINE OMEGA2 (OMEGAG, CG,UG,VG,PSDLG,PSDPG,PRESSG, 
     1                   CGH,UGH,VGH,CIGH,UIGH,VIGH,SDH,WA,WB,
     2                   LON,ILG,ILEV,S,SH,DS,AVERT,BETA,SIAI)
C 
C     * JAN 28/91 - F.MAJAESS (PASS WORK ARRAYS AS PARAMETERS)
C     * JAN 26/83 - B.DUGAS.
C     * MAR 10/81 - J.D.HENDERSON 
C 
C     * THIS SUBROUTINE COMPUTES VERTICAL MOTION FOR ILG POINTS 
C     * ABOUT A LATITUDE CIRCLE THE SAME WAY AS VRTIGW IN THE GCM.
C     * THEN SCALE BY SF.PRES. TO GET UNITS OF (NEWTONS/M**2/SEC).
C 
C     * ALL FIELDS HAVE LON DIFFERENT LONGITUDES OF WHICH ONLY
C     * FIRST ILG ARE OF ANY USE. THE REST ARE UNDEFINED. 
C 
C     * OUTPUT IS IN OMEGAG ON EVEN LEVELS. 
C     * INPUT ARRAYS CG,UG,VG ARE ON ODD LEVELS.
C     * PSDLG,PSDPG CONTAIN HORIZONTAL DERIVATIVES OF LN(SP). 
C     * CGH,UGH,VGH,CIGH,UIGH,VIGH,SDH,WA AND WB ARE WORK ARRAYS. 
C 
C     * LEVEL 2,OMEGAG, CG,UG,VG,PSDLG,PSDPG,PRESSG 
      IMPLICIT REAL (A-H,O-Z),
     +INTEGER (I-N)
      REAL OMEGAG(LON,1)
      REAL CG(LON,1),UG(LON,1),VG(LON,1)
      REAL PSDLG(1),PSDPG(1),PRESSG(1)
C 
      REAL S(1),SH(1),DS(1) 
C 
      REAL CGH(ILEV),UGH(ILEV),VGH(ILEV)
      REAL CIGH(ILEV),UIGH(ILEV),VIGH(ILEV),SDH(ILEV) 
      REAL WA(ILEV),WB(ILEV)
C---------------------------------------------------------------------- 
C 
      ILEVM=ILEV-1
      RECCSQ=1.E0/(SIAI**2) 
C 
      DO 14 IH=1,ILEVM
      WA(IH)=(SH(IH)-S(IH))/(S(IH+1)-S(IH)) 
      WB(IH)=(S(IH+1)-SH(IH))/(S(IH+1)-S(IH)) 
   14 CONTINUE
C 
C     * LONGITUDE LOOP OVER ILG GRID POINTS.
C 
      DO 900 IK=1,ILG 
C 
C     * (DIV,U,V) BARSIGMA AT EVEN LEVELS.
C 
      CIGH(ILEV) = -CG(IK,ILEV)*DS(ILEV)
      UIGH(ILEV) = -UG(IK,ILEV)*DS(ILEV)
      VIGH(ILEV) = -VG(IK,ILEV)*DS(ILEV)
      DO 10 IHI=1,ILEVM 
      IH=ILEV-IHI 
      CIGH(IH)=CIGH(IH+1)-CG(IK,IH)*DS(IH)
      UIGH(IH)=UIGH(IH+1)-UG(IK,IH)*DS(IH)
      VIGH(IH)=VIGH(IH+1)-VG(IK,IH)*DS(IH)
   10 CONTINUE
C 
C     * LAT AND LONG DERIVATIVES OF LNSP.  ADVECTION OF LNSP. 
C 
      PSDPOC=PSDPG(IK)*RECCSQ 
      PSDLOC=PSDLG(IK)*RECCSQ 
      VMDPS=UIGH(1)*PSDLOC+VIGH(1)*PSDPOC 
      DPSDT=CIGH(1)+VMDPS 
C 
C     * SIGMADOT AT EVEN LEVELS.
C 
      DO 20 IH=1,ILEVM
      SIG=1.E0-SH(IH) 
      SDH(IH)=SIG*CIGH(1)-CIGH(IH+1) +
     1         (SIG*UIGH(1)-UIGH(IH+1))*PSDLOC +
     2         (SIG*VIGH(1)-VIGH(IH+1))*PSDPOC
   20 CONTINUE
      SDH(ILEV)=AVERT*SDH(ILEVM)
C 
C     * (DIV,U,V) AVERAGED TO EVEN LEVELS.
C 
      DO 35 IH=2,ILEV 
      UGH(IH-1)=UG(IK,IH-1)*WB(IH-1) + UG(IK,IH)*WA(IH-1) 
      VGH(IH-1)=VG(IK,IH-1)*WB(IH-1) + VG(IK,IH)*WA(IH-1) 
      CGH(IH-1)=CG(IK,IH-1)*WB(IH-1) + CG(IK,IH)*WA(IH-1) 
   35 CONTINUE
      UGH(ILEV) = BETA*UG(IK,ILEV)
      VGH(ILEV) = BETA*VG(IK,ILEV)
      CGH(ILEV) = BETA*CG(IK,ILEV)
C 
C     * VERTICAL MOTION AT EVEN LEVELS. 
C 
      DO 36 IH=1,ILEV 
      OMG = SH(IH)*(UGH(IH)*PSDLOC + VGH(IH)*PSDPOC)
      OMG           = OMG + SH(IH)*DPSDT + SDH(IH)
      OMEGAG(IK,IH) = OMG*PRESSG(IK)
   36 CONTINUE
C 
  900 CONTINUE
C 
      RETURN
      END 
