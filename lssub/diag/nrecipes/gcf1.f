      SUBROUTINE GCF1(GAMMCF,A,X,GLN)
C
C     * THIS FUNCTION IS TAKEN FROM NUMERICAL RECIPES.
C     * PAUSE STATEMENT IS REPLACED BY A CALL TO "XIT"
C     * SUBROUTINE.
C     * THE ORIGINAL NAME "GCF" IS CHANGED TO "GCF1".
C
C     JUL 25/96 - SLAVA KHARIN
C
C  (C) COPR. 1986-92 NUMERICAL RECIPES SOFTWARE #=!E=#,)]UBCJ.
C--------------------------------------------------------------------
C
      IMPLICIT REAL (A-H,O-Z),
     +INTEGER (I-N)
      INTEGER ITMAX
      REAL A,GAMMCF,GLN,X,EPS,FPMIN
      PARAMETER (ITMAX=10000,EPS=3.E-7,FPMIN=1.E-30)
CU    USES GAMMLN1
      INTEGER I
      REAL AN,B,C,D,DEL,H,GAMMLN1
C--------------------------------------------------------------------
C
      GLN=GAMMLN1(A)
      B=X+1.E0-A
      C=1.E0/FPMIN
      D=1.E0/B
      H=D
      DO 10 I=1,ITMAX
        AN=-I*(I-A)
        B=B+2.E0
        D=AN*D+B
        IF(ABS(D).LT.FPMIN)D=FPMIN
        C=B+AN/C
        IF(ABS(C).LT.FPMIN)C=FPMIN
        D=1.E0/D
        DEL=D*C
        H=H*DEL
        IF(ABS(DEL-1.E0).LT.EPS) GOTO 100
  10  CONTINUE
C
      WRITE (6,'(A)') 
     1     ' *** ERROR IN GCF1: A TOO LARGE, ITMAX TOO SMALL.'
      CALL                                         XIT('GCF1',-1)
C
 100  GAMMCF=EXP(-X+A*LOG(X)-GLN)*H
      RETURN
      END
