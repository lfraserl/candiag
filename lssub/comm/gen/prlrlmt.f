      SUBROUTINE PRLRLMT (LA,LR,LM,KTR,LRLMT) 
C
C     * FEB 11/94 - F.MAJAESS 
C     * THIS ROUTINE WRITES OUT THE SPECTRAL TRUNCATION
C     * PARAMETERS TO UNIT 6.
C 
C     * LA   = TOTAL LENGTH OF SPECTRAL ARRAY (COMPLEX WORDS).
C     * LR   = LENGTH OF FIRST SPECTRAL ROW.
C     * LM   = NUMBER OF SPECTRAL ROWS. 
C     * KTR  = TRUNCATION TYPE. 
C     * LRLMT FORMED FROM LR, LM AND KTR. 
C
C-------------------------------------------------------------------- 
      IMPLICIT REAL (A-H,O-Z),
     +INTEGER (I-N)
      INTEGER LA,LR,LM,KTR,LRLMT
C-------------------------------------------------------------------- 
C 
      WRITE(6,6000) LA,LR,LM,KTR,LRLMT
 6000 FORMAT('0LA,LR,LM,KTR,LRLMT=',4I6,I8)
C
      RETURN
      END 
