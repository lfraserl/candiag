      SUBROUTINE SIGLAB2 (LSG,LSH,SG,SH,NLEV)
C 
C     * JAN 15/93 - E.CHAN (CALL LVCODE DO GENERATE LEVEL LABELS).   
C     * DEC 03/87 - R.LAPRISE.
C
C     * COMPUTES INTEGER LABEL VALUES OF ETA/SIGMA LEVELS FOR 
C     * GENERAL STAGGERED OR NON-STAGGERED VERSIONS OF GCM.
C     * SG  = SIGMA VALUES AT MID POINT OF MOMENTUM LAYERS. 
C     * SH  = SIGMA VALUES AT MID POINT OF THERMODYNAMIC LAYERS.
C     * LSG = 1000*SG.
C     * LSH = 1000*SH.
C 
      IMPLICIT REAL (A-H,O-Z),
     +INTEGER (I-N)
      REAL     SG(NLEV), SH(NLEV)
      INTEGER LSG(NLEV),LSH(NLEV) 
C-----------------------------------------------------------------------
      CALL LVCODE(LSG,SG,NLEV)
      CALL LVCODE(LSH,SH,NLEV)
      RETURN
C-----------------------------------------------------------------------
      END
