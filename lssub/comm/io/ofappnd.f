      SUBROUTINE OFAPPND (UNIT,FILNAME,FORMAT,IOS)
C
C     * JUL 07/03 - A.IRWIN,F.MAJAESS (REVISED FOR F90 AND LINUX PLATFORMS)
C     * APR 26/95 - F.MAJAESS
C     * THIS ROUTINE IS USED TO OPEN A FILE IN APPEND MODE.
C     * THE MAIN REASON FOR ITS CREATION IS TO DEAL WITH
C     * SITE SPECIFIC HANDLING OF OPENING DATA FILE IN
C     * APPEND MODE (WHICH IS NOT STANDARD).
C
C     ******************************************************
C     * THIS VERSION IS TO BE USED ON SYSTEM THAT SUPPORTS *
C     * OPENING FILES IN APPEND MODE VIA "ACCESS='APPEND'".*
C     ******************************************************
C
C     * INPUT PARAMETERS:
C     *
C     * UNIT    = FORTRAN UNIT NUMBER.
C     * FILNAME = FILENAME TO WHICH "UNIT" IS TO BE CONNECTED.
C     * FORMAT  = FORMAT MODE "FORMATTED/UNFORMATTED" 
C     *
C     * OUTPUT PARAMETER:
C     *
C     * IOS     = UPON RETURN, IT CONTAINS THE STATUS SWITCH 
C     *           RESULTED FROM THE OPEN STATEMENT OPERATION
C--------------------------------------------------------------------
C
      IMPLICIT REAL (A-H,O-Z),
     +INTEGER (I-N)
      INTEGER UNIT,IOS
      CHARACTER*(*) FILNAME,FORMAT
C--------------------------------------------------------------------
C
      OPEN(UNIT,FILE=FILNAME,FORM=FORMAT,POSITION='APPEND',IOSTAT=IOS)
C
C--------------------------------------------------------------------
      RETURN
      END
