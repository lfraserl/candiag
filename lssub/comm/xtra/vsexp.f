      SUBROUTINE VSEXP(XOUT,XIN,LLEN)
C
C     * APR 23/2007 - M. LAZARE. NEW 32-bits ROUTINE TO EMULATE IBM
C     *                          MASS VECTOR LIBRARY "VSEXP" CALL
C     *                          ON OTHER PLATFORMS.
C
      INTEGER*4 LLEN
      REAL*4 XOUT(LLEN), XIN(LLEN)
C--------------------------------------------------------------------
      DO N=1,LLEN
        XOUT(N)=EXP(XIN(N))
      ENDDO
C
      RETURN
      END
