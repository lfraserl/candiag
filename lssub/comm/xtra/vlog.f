      SUBROUTINE VLOG(XOUT,XIN,LENI)
C
C     * FEB 24, 2004 - M. LAZARE. NEW ROUTINE TO EMULATE IBM
C     *                           MASS VECTOR LIBRARY "VLOG" CALL
C     *                           ON OTHER PLATFORMS.
C
      INTEGER LENI
      REAL*8 XOUT(LENI), XIN(LENI)
C--------------------------------------------------------------------
      DO N=1,LENI
        XOUT(N)=LOG(XIN(N))
      ENDDO
C
      RETURN
      END
