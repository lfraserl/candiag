      SUBROUTINE GZASFP2(SFPR,PHI,PHIS,PMSL,NPTS,NLEV,PR) 
C 
C     * NOV 25/80 - J.D.HENDERSON 
C     * COMPUTES SURFACE PRESSURE FROM PHI,PHIS AND MSL. PRES.
C 
C     * LEVEL 2,SFPR,PHI,PHIS,PMSL
      IMPLICIT REAL (A-H,O-Z),
     +INTEGER (I-N)
      REAL SFPR(1),PHIS(1),PMSL(1)
      REAL PHI(NPTS,NLEV) 
      REAL PR(NLEV) 
C-----------------------------------------------------------------------
      DO 400 I=1,NPTS 
      L=1 
  300 IF(PHIS(I).LT.PHI(I,L)) GO TO 350 
      SFPR(I)=PR(L-1)*((PR(L)/PR(L-1))**((PHIS(I)-PHI(I,L-1))/
     1                              (PHI(I,L)-PHI(I,L-1)))) 
      GO TO 400 
  350 L=L+1 
      IF(L.LE.NLEV) GO TO 300 
      IF(PHI(I,NLEV).EQ.0.E0) PHI(I,NLEV)=0.001E0
      SFPR(I)=PR(NLEV)*(PMSL(I)/PR(NLEV))** 
     1                       ((PHI(I,NLEV)-PHIS(I))/PHI(I,NLEV))
  400 CONTINUE
C 
      RETURN
      END 
