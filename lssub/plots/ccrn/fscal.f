      FUNCTION FSCAL(AMX,AMN) 
  
C     * FUNCTION BORROWED FROM J.WALMSLEY, JAN 15 1985. 
C    *******************************************************************
C    *        THIS FUNCTION CALCULATES THE APPROPRIATE SCALING FACTOR  *
C    *   FOR THE CONTOUR MAP.                                          *
C    *******************************************************************
  
      IMPLICIT REAL (A-H,O-Z),
     +INTEGER (I-N)
      FSCAL = 1.E0
      AAMX = ABS(AMX) 
      AAMN = ABS(AMN) 
      AAMXN = MAX(AAMX,AAMN)
      IF (AAMXN.EQ.0) GO TO 1 
      LR =  INT(LOG10(AAMXN)) 
      IF (AAMXN.LT.1) LR = LR - 1 
      FSCAL = 10.E0 ** (3-LR) 
      P = AAMXN*FSCAL 
      IQ = INT(P+0.5E0)
      IQ10 = IQ/10000 
      IF (IQ10.NE.0) FSCAL = FSCAL/10.E0
    1 CONTINUE
      RETURN
      END
