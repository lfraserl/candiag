      SUBROUTINE GNTRG(XCNTR, YCNTR, DELTA, XCOORD, YCOORD)
 
C     * AUG 23/07 - B.MIVILLE
C     * GENERATE A TRIANGULAR MARKER
C

      IMPLICIT REAL (A-H,O-Z),
     +INTEGER (I-N)
      REAL XCNTR, YCNTR, XCOORD(4), YCOORD(4)
C-----------------------------------------------------------
C
       XCOORD(1) = XCNTR
       YCOORD(1) = YCNTR + DELTA
       XCOORD(2) = XCNTR + DELTA
       YCOORD(2) = YCNTR - DELTA
       XCOORD(3) = XCNTR - DELTA
       YCOORD(3) = YCNTR - DELTA
       XCOORD(4) = XCNTR
       YCOORD(4) = YCNTR + DELTA
C
      RETURN
      END
