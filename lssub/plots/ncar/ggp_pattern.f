      SUBROUTINE GGP_PATTERN (XWRK,YWRK,NWRK,IAREA,IGRP,NGRPS)

C     * MAR 11/2004 - M.BERKLEY (MODIFIED for CCCMA)

C     Color and Non-color shading
      IMPLICIT NONE

      REAL XWRK(*), YWRK(*)
      INTEGER NWRK
      INTEGER IAREA(*), IGRP(*)
      INTEGER NGRPS

C For passing colour index array into colour FILL routines through
C Common Block.

      INTEGER NIPAT, NPAT, MAXPAT, MAXCLRS
      PARAMETER(NIPAT = 28)
      INTEGER IPAT(NIPAT)
      REAL ZLEV(NIPAT)
      COMMON /FILLCB/ IPAT, ZLEV, NPAT, MAXPAT, MAXCLRS

      INTEGER NIPATS
      PARAMETER(NIPATS = 28)
      REAL SABR(NIPATS)
      COMMON /RGB/ SABR

      INTEGER NDSTWK,NINDWK
      
      REAL DSTWK(16384)
      INTEGER INDWK(32768)

      INTEGER IDMAP, IDCONT, FILPA
      INTEGER I,J

C If the area is defined by 2 or fewer points, return to ARSCAM
      IF (NWRK .LE. 3) RETURN

      NDSTWK=4*NWRK
      NINDWK=6*NWRK

      IDMAP=-1
      IDCONT=-1
C!!!      PRINT*,'ggp_pattern: '
C!!!      PRINT*,NGRPS,(IGRP(J),J=1,NGRPS),(IAREA(J),J=1,NGRPS)
      DO 10, I=1,NGRPS
        IF (IGRP(I).EQ.1) IDMAP=IAREA(I)
        IF (IGRP(I).EQ.3) IDCONT=IAREA(I)
 10   CONTINUE 
C Check if the area is over the map

      IF ((IDMAP .GT. 0) .AND. (IDCONT .GT. 0)) THEN 
        IF (IDCONT .GT. NPAT) THEN
C!!!          PRINT*, "Errors in the number of colours"
          RETURN
        ENDIF
C        CALL GSFACI(IPAT(IDCONT) - 98)
C        CALL GFA(NWRK,XWRK,YWRK)

C!!!      PRINT*,'Drawing',IDMAP,IDCONT,IPAT(IDCONT),FILPA
        CALL DFNCLR(IPAT(IDCONT),FILPA)
        IF(SABR(IDCONT).NE.0.) THEN
           FILPA=419+IDCONT
        ENDIF
        CALL SFSGFA(XWRK,YWRK,NWRK,
     1           DSTWK,NDSTWK,
     2           INDWK,NINDWK,
     3           FILPA)
      ENDIF

      RETURN

      END
