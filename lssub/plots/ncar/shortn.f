      SUBROUTINE SHORTN(LABEL,LBEFOR,LEAD,TRAIL,LAFTER) 

C    * MAR 11/2004 - M.BERKLEY (MODIFIED for NCAR V3.2)
C    * APR 19/91 - F.MAJAESS (FORCED TO RETURN AT LEAST 4 IN "LAFTER")
C    * AUTHOR: HAROLD REYNOLDS, JUNE 13/89. 
C 
C    * INPUT PARAMETERS:  
C 
C    * LABEL   =   LABEL TO BE PRINTED, SENT AS A CHARACTER*1 ARRAY 
C    * LBEFOR  =   INITIAL LENGTH OF INPUT LABEL
C    * LEAD    =   FLAG INDICATING WHETHER OR NOT TO STRIP LEADING  BLANKS
C    * TRAIL   =   FLAG INDICATING WHETHER OR NOT TO STRIP TRAILING BLANKS
C 
C    * OUTPUT PARAMETERS: 
C 
C    * LAFTER  =   LENGTH OF LABEL MINUS LEADING AND TRAILING BLANKS
C 
C    * PURPOSE: 
C 
C     THIS SUBROUTINE IS USED IN CONJUNTION WITH NCAR SUBROUTINES WHICH 
C     PRINT LABELS, I.E. PCHIQU. IT TAKES THE LABEL AND REMOVES LEADING 
C     AND TRAILING BLANK CHARACTERS FROM IT. BECAUSE PCHIQU USES CONTROL
C     CODES PLACED BETWEEN APOSTROPHES, APOSTROPHES ARE SEARCHED FOR IN 
C     THE LABEL. IF NONE ARE FOUND, A DEFAULT CONTROL CHARACTER IS PLACED 
C     AT THE START OF THE LABEL.
C     NOTE THAT THE LENGTH OF THE LABEL SENT SHOULD BE THREE CHARACTERS 
C     GREATER THAN THE MAXIMUM LENGTH IN THE INPUT CARD SO THAT THERE WILL
C     BE ROOM FOR THE DEFAULT CONTROL CODE. 
C ------------------------------------------------------------------------
  
      LOGICAL LEAD,TRAIL
      INTEGER LBEFOR,LAFTER,APFLAG,START
      CHARACTER*1 LABEL(LBEFOR),TEMP(83)
C 
C  FIND THE FIRST NON-BLANK CHARACTER 
C 
      START=1 
      IF(LEAD) THEN 
        DO 5 I=1,LBEFOR 
          IF(LABEL(I) .NE. ' ')THEN 
            START=I 
            GOTO 10 
          ENDIF 
    5   CONTINUE
   10   CONTINUE
      ENDIF 
C 
C FIND THE LAST NON-BLANK CHARACTER. NOTE THAT LAST 3 ELEMENTS OF ARRAY 
C ARE NOT DEFINED, SO DO NOT CHECK THEM.
C 
      LAFTER=LBEFOR-3 
      IF(TRAIL) THEN
        DO 15 I=LBEFOR-3,1,-1 
          IF(LABEL(I) .NE. ' ')THEN 
            LAFTER=I
            GOTO 20 
          ENDIF 
   15   CONTINUE
   20   CONTINUE
      END IF
C 
C  SEE IF APOSTROPHES PRESENT 
C 
      APFLAG=1
      DO 30 I=1,LAFTER
        IF(LABEL(I) .EQ. "'") THEN
          APFLAG=0
          GOTO 40 
        END IF
   30 CONTINUE
   40 CONTINUE
C 
C  SHIFT STRING TO REMOVE LEADING BLANKS. IF APOSTROPHES ARE NOT PRESENT, 
C  THE SHIFT WILL LEAVE THE FIRST THREE SPACES BLANK FOR THE INSERTION OF 
C  THE DEFAULT CONTROL STRING.
C 
      DO 45 I=START,LAFTER
        TEMP(I-START+1+ 3*APFLAG)=LABEL(I)
   45 CONTINUE
      LAFTER=LAFTER-START+1 + 3*APFLAG
C 
C  IF NO APOSTROPHES, INSERT DEFAULT FONT SIZE CONTROL CHARACTER. 
C  IT IS ASSUMED THAT THE LENGTH OF LABEL WHEN INPUT IS ACTUALLY
C  LBEFOR-3 SO THAT NO TRUNCATION WILL OCCUR. 
C 
      IF(APFLAG .EQ. 1) THEN
        TEMP(1)="'" 
        TEMP(2)='K' 
        TEMP(3)="'" 
      END IF
      DO 50 I=1,LAFTER
        LABEL(I)=TEMP(I)
   50 CONTINUE
C 
C     * FORCE THE LENGTH OF THE RETURNED SUBSTRING TO BE AT LEAST 4.
C     * (MAINLY TO FIX A BLANK STRING CASE) 
      IF(LAFTER.LT.4)THEN 
       DO 150 I=LAFTER+1,4
         LABEL(I)=" " 
  150  CONTINUE 
       LAFTER=4 
      ENDIF 
C 
C     * MAKE SURE THE REST OF "LABEL" IS RESET TO BLANKS... 
C 
      IF(LAFTER.LT.LBEFOR)THEN
       DO 200 I=LAFTER+1,LBEFOR 
         LABEL(I)=" " 
  200  CONTINUE 
      ENDIF 
      RETURN
      END 
