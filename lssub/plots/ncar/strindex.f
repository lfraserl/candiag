      INTEGER FUNCTION STRINDEX(STR,CH,DIR)
      IMPLICIT NONE
      CHARACTER*(*) STR
      CHARACTER CH,DIR

      INTEGER I

      IF(DIR.EQ.'F') THEN
         DO I=1,LEN(STR)
            IF(STR(I:I).EQ.CH) THEN
               STRINDEX=I
               RETURN
            ENDIF
         ENDDO
      ELSE IF(DIR.EQ.'B') THEN
         DO I=LEN(STR),1,-1
            IF(STR(I:I).EQ.CH) THEN
               STRINDEX=I
               RETURN
            ENDIF
         ENDDO
      ELSE
         WRITE(6,*)'STRINDEX: Invalid search direction.'
         CALL ABORT
      ENDIF

      STRINDEX=-1
      RETURN

      END
