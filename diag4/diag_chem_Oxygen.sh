#!/bin/sh
#
# This script is only included in the merged_diag_script grouping when
# gas-phase chemistry is run in MAM mode. BW, June 2023
#
# Amendments to original: hard-code dy1, dy2, dy3
#                         hard-code deltmin and yearoff
#                         source the file of switches
#
#  DESCRIPTION (original name: diag_chem_Oxygen_962b.sh)
# 
#  Create monthly averages and monthly zonal averages for ozone (O3)
#  volume mixing ratios, number densities and vertical columns, and
#  for O and O(1D), and for the advected family OX (O3 + O + O(1D)).
#
#  For the ozone vertical columns the instantaneous (non-averaged)
#  fields are saved as well.  Both total columns and tropospheric
#  columns are saved.
#
#  Zonal averages include daytime and nighttime masked values for O3
#  mixing ratios and number densities, and for O and O(1D).
#
#  PARMSUB PARAMETERS
#
#    -- chem, coord, days, dnsave, flabel, lat, lon, memory1, model1,
#       npg, plid, plv, plv2, pmax, pmaxc, pmin, run, stime, t1, t2,
#       t3, topsig, trchem, ztsave.
#    -- p01-p100 (pressure level set)
#
#  PREREQUISITES
#
#    -- GS and SS history files
#    -- DAYMASK deck (GPDAY data file, unaveraged)
#    -- NUMBERDENSITY deck (GEND data file, unaveraged)
#
#  CHANGELOG
#
#    2023-01-09: Conversion to shell script and removal of treatment of
#                 chemical species on a reduced vertical domain by removing
#                 trchem flag and pmint and pmaxc variables (D. Plummer)
#    2014-06-10: Integration of tropospheric ozone column now also outputs
#                 pressure of the tropopause that was found
#    2014-06-06: Monthly-average output on model surfaces for CCMI and 
#                 harmonization of save options iesave, ipsave, gesave
#                 (D. Plummer)
#    2013-06-10: Add iesave option to output ie file for ccmi (Y. Jiao)
#    2011-11-29: Modified xsave labels to be friendlier when converted to
#                timeseries filenames by spltdiag (D. Plummer)
#    2009-09-08: Always calculate separate day and night averages for ozone 
#                   - moved day/night calculations out from control of 
#                     dnsave flag
#    2007-07-01: Added saving of instantaneous (un-averaged) O3 to ie
#                file (A.Jonsson)
#    2007-06-26: Changed super label for OX to show components
#                (A.Jonsson)
#    2008-04-22: Added trchem flag to account for tropospheric chemistry
#                (D. Plummer)
#    2007-12-17: Moved call to access gpday to within $dnsave if-statement
#                (A.Jonsson)
#    2007-11-23: Capitalized all super labels (affecting H20(g),
#                H2O(s), HNO3(g), HNO3(s) and labels containing Br,
#                Cl, x, y or z) (A.Jonsson)
#    2007-11-23: Changed super label unit from "[MOL/M3]" to "[CM-3]"
#                (A.Jonsson)
#    2007-11-18: Bug fix: Added $dnsave if-statement for daymask
#                select input card (A.Jonsson)
#    2007-11-15: Added subversion date ('Date') and revision ('Rev')
#                keyword substitutions to all decks (A.Jonsson)
#    2007-11-15: Added parmsub parameter $dnsave to control day/night
#                time zonal averaging (A.Jonsson)
#    2007-11-15: Changed output name for O3 number density from 'O3ND'
#                to 'O3' (A.Jonsson)
#    2007-11-13: Revised $ztsave if-statement to only encompass xp
#                file data; Moved unaveraged O3 column output from
#                'ccp' file to 'cc' file (A.Jonsson)
#    2007-11-13: Added 'cat Input_cards' at the end of most decks
#                (A.Jonsson)
#    2007-11-13: Replaced hard coded level specification for upper
#                limit of "tropospheric" domain by $pmint (A.Jonsson)
#    2007-11-12: Bug fix: Switched upper limit from $pmin to $p01 in
#                input card for daymask selection; Bugfix: Switched
#                from vsintx to vsinth in column ozone calculation
#                (using the Ox field below the chemistry sub-domain)
#                (A.Jonsson)
#    2007-11-07: Added Oxygen deck (A.Jonsson)
#    2007-11-??: Based on the original O3 and Ox decks (A.Jonsson)
# -----------------------------------------------------------------------------

# Source the list of on/off switches
source ${CANESM_SRC_ROOT}/CanDIAG/diag4/gaschem_diag_switches.sh

# Define variables in lieu of reading them off a jobfile
dy1="000000000"; dy2="999999999"; dy3="0192";
deltmin=7.500
yearoff=1950
#
#  ----  access model history files
#
.   spfiles.cdk
.   ggfiles.cdk
#
#  ----  access the background number density field calculated
#        previously by diag_chem_nd
    access nd_ge  ${flabel}_gend 
#
#  ----  get the surface pressure field
#
    access gslnsp ${flabel}_gslnsp na
    if [ ! -s gslnsp ] ; then
      if [ "$datatype" = "gridsig" ] ; then
        echo "SELECT    STEPS $t1 $t2 $t3 LEVS    1    1 NAME LNSP" |
           ccc select npakgg gslnsp
      else
        echo "SELECT    STEPS $t1 $t2 $t3 LEVS    1    1 NAME LNSP" |
           ccc select npaksp sslnsp
        echo "COFAGG    $lon$lat" | ccc cofagg sslnsp gslnsp
      fi
    fi
#
    expone gslnsp sfprx
    echo "NEWNAM       PS" | ccc newnam sfprx sfpr1
    rm sfprx
#
#  ----  select required fields from model history files
    echo "SELECT          $t1 $t2 $t3 LEVS-9001 1000 NAME   O3   OO   O1" |
       ccc select npakgg O3_ge O3P_ge O1D_ge
    echo "SELECT          $t1 $t2 0001         1    1      O3LL" |
       ccc select npakgg O3sfc
    echo "SELECT          $t1 $t2 $t3 LEVS-9001 1000 NAME   OX" |
       ccc select npaksp OX_se
    echo "COFAGG.   $lon$lat    0$npg" | ccc cofagg OX_se OX_ge
    rm OX_se
#   
#  ----  create number density fields from VMR
#
#     nd_tracer = vmr_tracer * nd_air
#     nd  - number density [molecules/m3]
#
    mlt O3_ge nd_ge O3nd_ge
    rm nd_*
#
#  ----  interpolate onto constant pressure surfaces
#
    echo "GSAPL.    $plv       0.0       0.0$coord$plid
$p01$p02$p03$p04$p05$p06$p07$p08$p09$p10$p11$p12$p13$p14$p15$p16
$p17$p18$p19$p20$p21$p22$p23$p24$p25$p26$p27$p28$p29$p30$p31$p32
$p33$p34$p35$p36$p37$p38$p39$p40$p41$p42$p43$p44$p45$p46$p47$p48
$p49$p50$p51$p52$p53$p54$p55$p56$p57$p58$p59$p60$p61$p62$p63$p64
$p65$p66$p67$p68$p69$p70$p71$p72$p73$p74$p75$p76$p77$p78$p79$p80
$p81$p82$p83$p84$p85$p86$p87$p88$p89$p90$p91$p92$p93$p94$p95$p96
$p97$p98$p99$p100" > .gsapl_input_card
#
    gsapl O3P_ge  gslnsp O3P_gp  input=.gsapl_input_card
    gsapl O1D_ge  gslnsp O1D_gp  input=.gsapl_input_card
    gsapl OX_ge   gslnsp OX_gp   input=.gsapl_input_card
    gsapl O3_ge   gslnsp O3_gp   input=.gsapl_input_card
    gsapl O3nd_ge gslnsp O3nd_gp input=.gsapl_input_card
    rm .gsapl_input_card
#
#  ----  create monthly and zonal averages
#
#  -- monthly averages
#
    timavg O3P_gp  O3P_tav
    timavg O1D_gp  O1D_tav
    timavg OX_gp   OX_tav
    timavg O3_gp   O3_tav
    timavg O3nd_gp O3nd_tav
    timavg O3sfc   O3sfc_tav
#
    if [ "$gesave" = on ] ; then
      timavg O3P_ge  O3P_etav
      timavg O1D_ge  O1D_etav
      timavg O3_ge   O3_etav
    fi
#
    if [ "$ztsave" = on ] ; then
#
#  -- monthly zonal averages
#
      zonavg O3P_tav  O3P_ztav
      zonavg O1D_tav  O1D_ztav
      zonavg OX_tav   OX_ztav
      zonavg O3_tav   O3_ztav
      zonavg O3nd_tav O3nd_ztav
#
# -- day/night zonal average of ozone is always done
#
      access day_gp ${flabel}_gpday 
      rzonavg3 O3_gp day_gp O3_z O3_d O3_n
#
      timavgmsk O3_d O3_dztav
      timavgmsk O3_n O3_nztav
      rm O3_z O3_d O3_n
#
    fi
#
#  ----  day/night zonal average of other species if dnsave=on
#
    if [ "$dnsave" = on ] ; then
      rzonavg3 O3P_gp  day_gp O3P_z  O3P_d  O3P_n
      rzonavg3 O1D_gp  day_gp O1D_z  O1D_d  O1D_n
      rzonavg3 O3nd_gp day_gp O3nd_z O3nd_d O3nd_n
#
#  -- monthly zonal/day/night averages 
#
      timavgmsk O3P_d O3P_dztav
      timavgmsk O3P_n O3P_nztav
#
      timavgmsk O1D_d O1D_dztav
      timavgmsk O1D_n O1D_nztav
#
      timavgmsk O3nd_d O3nd_dztav
      timavgmsk O3nd_n O3nd_nztav
#
      rm day* *_z *_d *_n
    fi
#
#  ----  compute ozone column
#
#   --- constant on vsinth parmsub line converts integral of
#       mixing ratio in pressure coordinates to Dobson units
    echo "VSINTH..   HALF 7885.9784    2 ET15$topsig$plid" | ccc vsinth O3_ge gslnsp xa
#
#     Multiply column integrated in sigma by surface pressure 
#     converted from mb to Pa -- end result is column in DU
    echo "XLIN.....       100." | ccc xlin sfpr1 ps
    mlt xa ps xb
    echo "NEWNAM.    O3CL" | ccc newnam xb xcol
#
    rm sfpr1 xa xb
#
#  -- create monthly and zonal averages
#
    timavg xcol xcol_tav
    zonavg xcol xcol_zav
    timavg xcol_zav xcol_ztav
#
    rm xcol_zav
#
#  ----  compute O3 column below the diagnosed 2K/km tropopause
#
    if [ "$gcmtsav" = on ] ; then
      echo "SELECT          $t1 $t2 $t3 LEVS-9001 1000 NAME TEMP" | ccc select npaksp sstemp
    else
      echo "SELECT          $t1 $t2 $t3 LEVS-9001 1000 NAME PHIS PHI" | ccc select npaksp ssphis ssphi
      ctemps ssphis ssphi sstemp
    fi
    echo "COFAGG.   $lon$lat    0$npg" | ccc cofagg sstemp gstemp
#
    echo "VSINTROP   HALF 7885.9784$lay$coord$topsig$plid" | ccc vsinth_trop O3_ge gslnsp gstemp xc tppres tptemp
    mlt  xc   ps  trcol
    timavg trcol trcol_tav
    timavg tppres tppres_tav
    timavg tptemp tptemp_tav
#
    rm npakgg npaksp ps xc
#
#  ----  save instantaneous fields
#
#     ie - instantaneous data on model levels
#     ip - instantaneous data on constant pressure surfaces
#     ix - instanteneous zonal average cross-sections
#     cc - instantaneous ozone column fields
#
    if [ "$iesave" = "on" ] ; then
      if [ "$ieslct" = "on" ] ; then
        echo "C*TSTEP   $deltmin     ${yearoff}010100          MODEL     YYYYMMDDHH" > ic.tstep_model
        selstep O3P_ge O3P_ie input=ic.tstep_model
        selstep O1D_ge O1D_ie input=ic.tstep_model
        selstep  O3_ge  O3_ie input=ic.tstep_model
#
        release oldie
        access oldie ${flabel}ie      na
        echo "XSAVE.        O_3P
NEWNAM.
XSAVE.        O_1D
NEWNAM.
XSAVE.        O3
NEWNAM." | ccc xsave oldie O3P_ie O1D_ie O3_ie newie
        save   newie ${flabel}ie
        delete oldie                  na
        rm *_ie
      fi
#
      if [ "$ieslct" = "off" ] ; then
        release oldie
        access oldie ${flabel}ie      na
        echo "XSAVE.        O_3P
NEWNAM.
XSAVE.        O_1D
NEWNAM.
XSAVE.        O3
NEWNAM." | ccc xsave oldie O3P_ge O1D_ge O3_ge newie
        save   newie ${flabel}ie
        delete oldie                  na
      fi
    fi
#
    if [ "$ipsave" = "on" ] ; then
      release oldip
      access oldip ${flabel}ip      na
      echo "XSAVE.        O_3P
NEWNAM.
XSAVE.        O_1D
NEWNAM.
XSAVE.        O3
NEWNAM.
XSAVE.        O3 NUMDEN - 1 OVER CM3
NEWNAM.      O3
XSAVE.        OX - O3 O3P O1D
NEWNAM." | ccc xsave oldip O3P_gp O1d_gp O3_gp O3nd_gp OX_gp newip
      save   newip ${flabel}ip
      delete oldip                  na
    fi
#
    if [ "$dailysv" = on ] ; then
      echo "SELECT.   STEPS $dy1 $dy2 $dy3 LEVS-9001 1000 NAME   O3" | ccc select O3_gp O3_dly_gp
      zonavg O3_dly_gp O3_ixp
#
      release oldix
      access oldix ${flabel}ix   na
      echo "XSAVE.        INSTANTANEOUS O3
NEWNAM." | ccc xsave oldix O3_ixp newix
      save newix ${flabel}ix
      delete oldix               na
      rm O3_ixp
#
      echo "SELECT.   STEPS $dy1 $dy2 $dy3 LEVS-9001 1000 NAME O3CL" | ccc select xcol xcol_dly
      release oldip
      access oldip ${flabel}ip   na
      echo "XSAVE.        DAILY INSTANTANEOUS O3 COLUMN - DU
NEWNAM." | ccc xsave oldip xcol_dly newip
      save newip ${flabel}ip
      delete oldip
      rm xcol_dly
    fi
#
      release oldcc
      access oldcc ${flabel}cc     na
      echo "XSAVE.        INSTANTANEOUS O3 COLUMN - DU
NEWNAM.
XSAVE.        INSTANTANEOUS TROPOSPHERIC O3 COLUMN - DU
NEWNAM.    O3CT
XSAVE.        INSTANTANEOUS LAPSE RATE TROPOPAUSE PRESSURE - PA
NEWNAM.    TPPR
XSAVE.        INSTANTANEOUS LAPSE RATE TROPOPAUSE TEMPERATURE - K
NEWNAM.    TPTA" | ccc xsave oldcc xcol trcol tppres tptemp newcc
      save  newcc ${flabel}cc
      delete oldcc                 na
      rm xcol trcol
#
      rm *_gp
      rm *_ge
#
# (H) SAVE AVERAGED FIELDS
#
#     xp - time and zonal averaged data on pressure levels
#     gp - time averaged data on pressure levels
#     ge - time averaged data on model eta levels
#
    if [ "$ztsave" = on ] ; then
      echo "XSAVE.        O_3P
NEWNAM.
XSAVE.        O_1D
NEWNAM.
XSAVE.        O3
NEWNAM.
XSAVE.        O3 NUMDEN - 1 OVER CM3
NEWNAM.      O3
XSAVE.        OX - O3 O3P O1D
NEWNAM.
XSAVE.        O3 DAY
NEWNAM.
XSAVE.        O3 NIGHT
NEWNAM.
XSAVE.        O3 COLUMN - DU
NEWNAM." | ccc xsave oldxp O3P_ztav O1D_ztav O3_ztav O3nd_ztav OX_ztav \
                     O3_dztav O3_nztav xcol_ztav newxp 
    fi    
#
    if [ "$dnsave" = on ] ; then
      echo "XSAVE.        O_3P DAY
NEWNAM.
XSAVE.        O_1D DAY
NEWNAM.
XSAVE.        O3 DAY NUMDEN - 1 OVER CM3
NEWNAM.      O3
XSAVE.        O_3P NIGHT
NEWNAM.
XSAVE.        O_1D NIGHT
NEWNAM.
XSAVE.        O3 NIGHT NUMDEN - 1 OVER CM3
NEWNAM.      O3" | ccc xsave newxp O3P_dztav O1D_dztav O3nd_dztav O3P_nztav \
                                   O1D_nztav O3nd_nztav newxp1
      mv newxp1 newxp
    fi
#
    release old_xp
    access old_xp ${flabel}xp     na
    xjoin  old_xp newxp new_xp
    save   new_xp ${flabel}xp
    delete old_xp                 na
    rm *_ztav *_dztav *_nztav
#
    release oldgp
    access oldgp ${flabel}gp      na
    echo "XSAVE.        O_3P
NEWNAM.
XSAVE.        O_1D
NEWNAM.
XSAVE.        O3
NEWNAM.
XSAVE.        O3 NUMDEN - 1 OVER CM3
NEWNAM.      O3
XSAVE.        OX - O3 O3P O1D
NEWNAM
XSAVE.        O3 IN BOTTOM MODEL LAYER
NEWNAM.
XSAVE.        O3 COLUMN - DU
NEWNAM.
XSAVE.        TROPOSPHERIC O3 COLUMN - DU
NEWNAM.    O3CT
XSAVE.        LAPSE RATE TROPOPAUSE PRESSURE - PA
NEWNAM.    TPPR
XSAVE.        LAPSE RATE TROPOPAUSE TEMPERATURE - K
NEWNAM.    TPTA" | ccc xsave oldgp O3P_tav O1D_tav O3_tav O3nd_tav OX_tav \
                       O3sfc_tav xcol_tav trcol_tav tppres_tav tptemp_tav newgp
    save newgp ${flabel}gp
    delete oldgp                na
    rm *_tav
#
    if [ "$gesave" = "on" ] ; then
      access oldge ${flabel}ge  na
      echo "XSAVE.        O_3P
NEWNAM.
XSAVE.        O_1D
NEWNAM.
XSAVE.        O3
NEWNAM." | ccc xsave oldge O3P_etav O1D_etav O3_etav newge
      save newge ${flabel}ge
      delete oldge              na
      rm *_etav
    fi

