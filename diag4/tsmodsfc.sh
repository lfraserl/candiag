#!/bin/sh
#                  tsmodsfc            dl. - Nov 21/00.
#   ---------------------------------- save time series of sfc fields:
#                                      pmsl, pcp, qfs, hfs, ufs, vfs, st, gt,
#                                      sno, wf, wl, and beg.
#   Replace -273. by -273.16

.     ggfiles.cdk
.     spfiles.cdk

#   ---------------------------------- pcp, qfs, hfs, ufs, and vfs.
echo "SELECT          $t1 $t2 $t3         1    1      PHIS LNSP" | ccc select npaksp ssphis sslnsp
      if [ "$gcmtsav" = on ] ; then
echo "SELECT          $t1 $t2 $t3 LEVS-9001 1000      TEMP" | ccc      select npaksp sstemp
      else
echo "SELECT          $t1 $t2 $t3 LEVS-9001 1000       PHI" | ccc      select npaksp ssphi
         ctemps ssphis ssphi sstemp
      fi
#   ..................................................................... sstemp
.     sstemp.cdk
echo "COFAGG    $lon$lat          PHIS" | ccc cofagg ssphis gsphis
echo "COFAGG    $lon$lat          LNSP" | ccc cofagg sslnsp gslnsp
echo "COFAGG    $lon$lat          TEMP" | ccc cofagg sstemp gstemp
      rm ssphis sstemp sslnsp
      if [ "$gcm2plus" = on ] ; then
echo "GSMSLPH        .0065    0$lay$coord$plid" | ccc gsmslph gstemp gslnsp gsphis gpmsl
      fi
      if [ "$gcm1" = on ] ; then
echo "GSMSLP         .0065    1" | ccc gsmslp  gstemp gslnsp gsphis gpmsl
      fi
      savets gpmsl ${tlabel}msl$tendlbl
      rm gpmsl gstemp gslnsp gsphis

#                                      precipitation.
echo "SELECT NPAKGG   $t1 $t2 $t3 LEVS    1    1 NAME  PCP" | ccc   select npakgg pcp
      savets pcp ${tlabel}pcp$tendlbl
      rm pcp

#                                      surface moisture flux.
echo "SELECT NPAKGG   $t1 $t2 $t3 LEVS    1    1 NAME  QFS" | ccc   select npakgg qfs
      savets qfs ${tlabel}qfs$tendlbl
      rm qfs

#                                      surface heat flux.
echo "SELECT NPAKGG   $t1 $t2 $t3 LEVS    1    1 NAME  HFS" | ccc   select npakgg hfs
      savets hfs ${tlabel}hfs$tendlbl
      rm hfs

#                                      stresses.
echo "SELECT NPAKGG   $g1 $g2 $g3 LEVS    1    1 NAME  UFS  VFS" | ccc   select npakgg taux tauy
      savets taux ${tlabel}ufs$tendlbl
      savets tauy ${tlabel}vfs$tendlbl
      rm taux tauy

#   ---------------------------------- st.
echo "SELECT NPAKGG   $t1 $t2 $t3 LEVS    1    1 NAME   ST" | ccc   select npakgg nst
echo "XLIN.          1.0E0 -2.7316E2" | ccc      xlin nst x
      savets x ${tlabel}st$tendlbl
      rm x nst

#   ---------------------------------- gt, sno, wf, and wl.
echo "SELECT NPAKGG   $t1 $t2 $t3 LEVS    1    1 NAME   GT  SNO   WF   WL" | ccc   select npakgg ngt nsnow nwf nwl
echo "XLIN.          1.0E0 -2.7316E2" | ccc      xlin ngt x
      rm ngt
      savets x ${tlabel}gt$tendlbl
      savets nsnow ${tlabel}sno$tendlbl
      savets nwf ${tlabel}wf$tendlbl
      savets nwl ${tlabel}wl$tendlbl
      rm x nsnow nwf nwl

#   ---------------------------------- beg.
echo "SELECT          $g1 $g2 $g3         0    1       BEG" | ccc   select npakgg beg
      savets beg ${tlabel}beg$tendlbl
