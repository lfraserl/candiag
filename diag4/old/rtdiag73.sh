#!/bin/sh
#                  rtdiag73            M.Lazare/S.Kharin/V.Arora - Dec 22/2015
#   ---------------------------------- Model run-time diagnostics
#
#   Runtime diagnostics is run once a year assuming that model
#   files (gs,cm,tm,gz,etc.) are available on disk for all 12 months.
#
#   Changes vs. v68:
#   - add tm (terrestrial carbon) file
#   - minor fixes here and there and a few new variables.
#   - remove diagnosed annual global CO2 emissions (can be done in plot_rtdiag)
#   - remove CO2 burden change (can be done in plot_rtdiag)
#   - simplify merging (now using a binary joinrtd)
#   Changes vs. v67:
#   - switches PhysA,PhysO,CarbA,CarbO,CarbL are determined automatically based on variable availability
#   - new parameter to control the number of rtd files kept on disk (keep_old_rtdiag_number). Default is keep_old_rtdiag_number=3.
#   - new variable leaf area index (CLAI)
#   - add monthly ST over land, ocean
#   - add monthly GT over global, land, ocean and global under sea ice
#   - add monthly PCP over global, land, ocean
#   - add monthly CO2 PPMV (when CO2 is a tracer)
#   - new STMX and STMN over globe, land, ocean
#   - compute ice cover separately for ocean and lakes
#   - add sea-ice area (SICN>0) as compared to sea-ice extent (SICN>15%)
#   - add global ocean nitrate
#   - fix a bug in global vertical integral for global ocean alkalinity (it was global mean instead of integral)
#   - change names for land areas and fractions
#   - add OBEI from cm files
#
#   Switches: (determined automatically)
#
#   PhysA=on/off            physical atmosphere variables
#   PhysO=on/off            physical ocean variables
#   CarbA=on/off            atmosphere carbon variables
#   CarbO=on/off            ocean carbon variables
#   CarbL=on/off            land carbon variables
#
#   keep_old_rtdiag=on/off  keep older rtd files for all years
#   keep_old_rtdiag_number  the number of rtdiag files for previous years that are kept on disk.
#                           Default is 3, i.e., the current year plus previous two years.
#                           If =1, only the latest rtdfile which includes the current year is kept on disk.
#   rtd2vic=on/off          transfer rtd file to Victoria in /plots/pubplots/CanESM_run_plots/rtdfiles/.

# ----------------------------------------------------------------------
#                                      runtime diagnostic deck version
    version="73"

# ----------------------------------------------------------------------
#                                      initialize list of variables to be
#                                      extracted from model files
    gsvars=""   # from gs files each month
    ssvars=""   # from ss files
    cmvars=""   # from cm files
    tmvars=""   # from tm files
    gzvars=""   # from gz files

    gzlevd=5  # ocean level increment to be extracted
    upper_ocean_depths="220 500 800 1000 2000 3000" # integrate to these depths

# ---------------------------------- Local function defs

# A simple error exit routine
bail(){
  echo "${jobname}: $1"
  echo "${jobname}: $1" >> haltit
  exit 1
}

# Extract variable definitions from a shell script
pardef(){
  # usage: pardef PARM_txt var1 [var2 ...]
  # Larry Solheim ...Jan,2004

  # Extract particular variable definitions from a shell script
  # and add them to the current environment.
  # This is typically used to extract parmsub variables from a PARM record.

  # The first arg is the name of a text file containing the shell script.
  # The second and subsequent args define a list of variable names to evaluate.
  [ -z "$1" ] && bail "pardef: Missing PARM_txt file name."
  [ -z "$2" ] && bail "pardef: Missing variable name."
  loc_PARM_txt=$1
  [ -z "$loc_PARM_txt" ] && bail "pardef: loc_PARM_txt is missing"
  shift
  loc_env_list="$*"
  [ -z "$loc_env_list" ] && bail "pardef: loc_env_list is missing"

  [ ! -s $loc_PARM_txt ] && bail "pardef: $loc_PARM_txt is missing or empty"

  rm -f parmsub1 parmsub.x

  # Remove any CPP_I definition that may appear in the parmsub section
  addr1='/^ *## *[Cc][Pp][Pp]_[Ii]_[Ss][Tt][Aa][Rr][Tt]/'
  addr2='/^ *## *[Cc][Pp][Pp]_[Ii]_[Ee][Nn][Dd]/'
  eval sed \'${addr1},${addr2}d\' $loc_PARM_txt > parmsub.x

  # Remove comments and blank lines
  # Remove any lines that source an external script via ". extscript"
  # Remove " +PARM" line inserted by bin2txt, if any
  sed 's/#.*$//; /^ *[+]/d; /^ *\. .*/d; /^ *$/d' parmsub.x > parmsub1

  # Prepend a shebang line
  echo '#!/bin/sh' | cat - parmsub1 > parmsub.x

  # Ensure the resulting script ends with a blank line
  echo 'echo " "' | cat parmsub.x - > parmsub1

  # Make sure parmsub1 and parmsub.x are equivalent and executable
  cat parmsub1 > parmsub.x
  chmod u+x parmsub.x parmsub1

  [ ! -s parmsub.x ] && bail "pardef: Unable to create parmsub.x"

  echo " "
  for loc_var in $loc_env_list; do

    # Determine parameter values from the parmsub record
    echo 'echo "$'${loc_var}'"' | cat parmsub.x - > parmsub1 # added by SK to fix xemacs fontification bug"
    loc_val=`/bin/sh ./parmsub1|tail -1`

    # Create/reassign a variable of the same name in the current env
    eval ${loc_var}=\"\$loc_val\"

    [ -n "$loc_val" ] && echo "Found in $loc_PARM_txt :: ${loc_var}=$loc_val"
  done
  rm -f parmsub1 parmsub.x
}

# ------------------------------------ access PARM section to obtain values of some parameters
    PhysA="on"  # gs file must exist
    release gs01
    access  gs01 ${uxxx}_${runid}_${year}_m01_gs na
    if [ ! -s gs01 ] ; then
      echo "Error: **** ${uxxx}_${runid}_${year}_m01_gs is not found. ****"
      exit 1
    fi

    echo "C*SELECT   STEP         0         0    1         0    0 NAME PARM" |\
     ccc select gs01 parm ||\
     bail "Unable to extract PARM record from ${uxxx}_${runid}_${year}_m01_gs"
    release gs01
    bin2txt parm parm.txt >/dev/null
    [ ! -s parm.txt ] && bail "parm.txt is missing or empty"

    # First determine the value of ntrac to be used below
    # to create a list of tracer variable names (itNN)
    # Also get the grid size (nlat, lonsl)
    pardef parm.txt ntrac nlat lonsl ilev levs delt isgg isbeg israd ksteps ntld ntlk ntwt
    [ -z "$ntrac" ] && ntrac=0
    [ -z "$nlat"  ] && bail "nlat is not defined in parm.txt"
    [ -z "$lonsl" ] && bail "lonsl is not defined in parm.txt"
    [ -z "$ilev" ] && bail "ilev is not defined in parm.txt"
    [ -z "$levs" ] && bail "levs is not defined in parm.txt"
    [ -z "$delt" ] && bail "delt is not defined in parm.txt"
    [ -z "$isgg" ] && bail "issg is not defined in parm.txt"
    [ -z "$isbeg" ] && bail "isbeg is not defined in parm.txt"
    [ -z "$israd" ] && bail "israd is not defined in parm.txt"
    [ -z "$ksteps" ] && bail "ksteps is not defined in parm.txt"
    [ -z "$ntld" ] && bail "ntld (number of land tiles) is not defined in parm.txt"
    [ -z "$ntlk" ] && bail "ntlk (number of lake tiles) is not defined in parm.txt"
    [ -z "$ntwt" ] && bail "ntwt (number of ocean tiles) is not defined in parm.txt"
    echo ntrac=$ntrac nlat=$nlat lonsl=$lonsl ilev=$ilev levs=$levs delt=$delt isgg=$isgg isbeg=$isbeg israd=$israd ksteps=$ksteps ntld=$ntld ntlk=$ntlk ntwt=$ntwt

    if [ $ntrac -gt 0 ]; then
      # Create a list of parmsub variable names whose value
      # will (should) be tracer names
      itlist=''
      nn=0
      while [ $nn -lt $ntrac ]; do
        nn=`echo $nn|awk '{printf "%2.2d",$1+1}' -`
        itlist="$itlist it$nn"
      done
      [ -z "$itlist" ] && bail "Error defining itNN variable names"

      # Add these tracer names to the current environment
      pardef parm.txt $itlist
    fi

    # assign tiles manually (for now)
    ilnd=1 # land
    iwat=2 # ocean sea water
    isic=3 # ocean sea ice

# ------------------------------------ Physical atmosphere variables
    if [ "$PhysA" = "on" ] ; then
      gsvars="$gsvars ST STMX STMN SQ SRH FN SNO SIC SICN GT FSR FSG FSGC FLG FLGC FDL OLR FSLO FSA FLA FSAM FLAM BEG BEGI BEGO BEGL FSGI FSGO HFLI BWGI BWGO OBEG OBEX HFS HFL BWG OBWG PCP PCPC PCPN QFS QFSL ROF RIVO FSO CLDT CLDO FSS FSSC FSD FSDC PWAT CO2 CH4 N2O WGL WGF WVL WVF WSNO ZPND TG TV RHON REF BCSN FMI RES FARE GTT SNOT AN TN TBEG TBEI TBES SBEG SBEI SBES TSNN TBWG TTG1 TWG1"
      if [ "$rtdssvars" = "off" ] ; then
        ssvars=""
      else
        ssvars="TEMP ES TMPN ESN TMPR ESR"
      fi
#                                      find AGCM tracers
      if [ -z "$ntrac" ] ; then
        echo "Error: **** PhysA: The number of tracer parameter ntrac is undefined. ****"
        exit 1
      fi

      PLAtracers="off"
      i2=1
      while [ $i2 -le $ntrac ] ; do
#                                      determine tracer name
        i2=`echo $i2 | awk '{printf "%02i", $1}'` # 2-digit tracer number
        eval x=\${it$i2}
        if [ -z "$x" ] ; then
          echo "Error: **** tracer name for it$i2 is undefined. ****"
          exit 1
        fi
        if [ "$x" != "CO2" ] ; then
#                                      skip CO2 tracer (which is processed in CarbA section)
          gsvars="$gsvars VI$i2"
        fi
#                                      check for A01 name: if found then assume PLA-scheme for tracers
        if [ "$x" = "A01" ] ; then
          PLAtracers="on"
        fi
        i2=`expr $i2 + 1`
      done
    fi # PhysA

# ------------------------------------ Physical ocean variables

    release gz01
    access  gz01 ${uxxx}_${runid}_${year}_m01_gz na
    if [ -s gz01 ] ; then
      PhysO="on"
    else
      PhysO="off"
    fi
    release gz01
    if [ "$PhysO" = "on" ] ; then
      gzvars="$gzvars TEMP SALT PSIB V VISO"
      cmvars="$cmvars OBET OBWA OBEI OBEX"
    fi

# ------------------------------------ Determine carbon switches (based on availibity of variables in cm files)

    release cm01
    access  cm01 ${uxxx}_${runid}_${year}_m01_cm na

#   check if CarbA and CarbO need to be set to on or off

    if [ -s cm01 ] ; then
      echo "C*SELECT   STEP         0 999999999    1     -900199999 NAME ECO2 FCOO" | ccc select cm01 eco2 fcoo || true
      # ECO2 must exist for CarbA=on
      if [ -s eco2 ] ; then
        CarbA="on"
        release eco2
      else
        CarbA="off"
      fi
      # FCOO must exist for CarbO=on
      if [ -s fcoo -a "$PhysO" = "on" ] ; then
        CarbO="on"
        release fcoo
      else
        CarbO="off"
      fi
    else
      # if cm file does not exist, there is no ocean and atmos carbon cycle
      CarbA="off"
      CarbO="off"
      cmvars=""
    fi

#   check if CarbL need to be set to on or off

    release tm01
    access  tm01 ${uxxx}_${runid}_${year}_m01_tm na

    if [ -s cm01 -o -s tm01 ] ; then
      echo "C*SELECT   STEP         0 999999999    1     -900199999 NAME CVEG" | ccc select cm01 cveg_cm || true

      echo "C*SELECT   STEP         0 999999999    1     -900199999 NAME CVEG" | ccc select tm01 cveg_tm || true
      # CVEG must exist for CarbL=on
      if [ -s cveg_cm -o -s cveg_tm ] ; then
        CarbL="on"
        release cveg_cm
        release cveg_tm
      else
        CarbL="off"
      fi
    else
      CarbL="off"
    fi

#   is CTEM output thrown to tm or cm file. If tm file exists make a note of that

    if [ -s tm01 ] ; then
      tm_file_exists="yes"
    else
      tm_file_exists="no"
    fi

    release cm01
    release tm01

    echo PhysA=$PhysA PhysO=$PhysO CarbA=$CarbA CarbO=$CarbO CarbL=$CarbL

# ------------------------------------ Atmospheric carbon variables
    if [ "$CarbA" = "on" ] ; then
      cmvars="$cmvars ECO2"
#                                      find CO2 tracer
      if [ -z "$ntrac" ] ; then
        echo "Error: **** CarbA: The number of tracer parameter ntrac is undefined. ****"
        exit 1
      fi
      iCO2=""
      i2=1
      while [ $i2 -le $ntrac ] ; do
#                                      determine tracer name
        i2=`echo $i2 | awk '{printf "%02i", $1}'` # 2-digit tracer number
        eval x=\${it$i2}
        if [ -z "$x" ] ; then
          echo "Error: **** tracer name for it$i2 is undefined. ****"
          exit 1
        fi
        if [ "$x" = "CO2" ] ; then
          iCO2="$i2"
          gsvars="$gsvars XF$i2 XL$i2 VI$i2"
        fi
        i2=`expr $i2 + 1`
      done
      if [ -z "$iCO2" ] ; then
        echo "**** CO2 tracer is not found. ****"
      fi
    fi

# ------------------------------------ Ocean carbon variables

    if [ "$CarbO" = "on" ] ; then
#                                      Ocean carbon variables
      cmvars="$cmvars FCOO SWMX OFSG PMSL"
      gzvars="$gzvars OPCO ODPC OVIC OFNP OFXO OFXI OFNF OFSO OFSI OPH ONIT ODIC OALK OPHY OZOO ODET ALK DIC BNI"
#                                      ODIC(new)=DIC(old), ONIT(new)=BNI(old), OALK(new)=ALK(old)
    fi
    if [ "$CarbL" = "on" ] ; then
#                                      Land carbon variables
      if [ "$tm_file_exists" = "yes" ] ; then
        # CTEM output is in tm file
        tmvars="$tmvars CVEG CDEB CHUM CFNP CFNE CFRV CFGP CFNB CFLV CFLD CFLH CFRH CFHT CFLF CFRD CFFD CFFV CBRN WFRA CH4H CH4N CW1D CW2D CLAI"
      else
        # CTEM output is in cm file
        cmvars="$cmvars CVEG CDEB CHUM CFNP CFNE CFRV CFGP CFNB CFLV CFLD CFLH CFRH CFHT CFLF CFRD CFFD CFFV CBRN WFRA CH4H CH4N CW1D CW2D CLAI"
      fi
    fi

    if [ -z "$gsvars" -a -z "$ssvars" -a -z "$gzvars" -a -z "$cmvars" -a -z "$tmvars" ] ; then
      echo "*** There is nothing to process ***"
      exit 0
    fi

    echo "gsvars=$gsvars"
    echo "ssvars=$ssvars"
    echo "cmvars=$cmvars"
    echo "tmvars=$tmvars"
    echo "gzvars=$gzvars"

# ----------------------------------------------------------------------

#                                      starting year of rtdiag.
    if [ -z "$year_rtdiag_start" ] ; then
#                                      try $year_rdiag_start if $year_rtdiag_start is undefined
      if [ -z "$year_rdiag_start" ] ; then
        echo "*** ERROR: Start year is undefined $year_rdiag_start ***"
        exit 1
      else
        year_rtdiag_start="$year_rdiag_start"
      fi
    fi
#                                      new rtd file
    year_rtdiag_start=`echo $year_rtdiag_start | awk '{printf "%04d", $1}'`;
    year=`echo $year | awk '{printf "%04d", $1}'`;
#                                      starting year of rtdiag.
    if [ -z "${uxxx_rtd}" ] ; then
      if [ -z "${uxxx}" ] ; then
        uxxx_rtd="sc"
      else
        uxxx_rtd=`echo "$uxxx" | sed -e 's/^m/s/'`
      fi
    fi
    rtdfile="${uxxx_rtd}_${runid}_${year_rtdiag_start}_${year}_rtd073"

#                                      rtd file for the previous years
    keep_old_rtdiag_number=${keep_old_rtdiag_number:=1}
    echo keep_old_rtdiag_number=$keep_old_rtdiag_number

    yearm1=`echo $year | awk '{printf "%04d", $1 - 1}'`;
    yearmo=`echo $year $keep_old_rtdiag_number | awk '{printf "%04d", $1 - $2}'`
    rtdfile1="${uxxx_rtd}_${runid}_${year_rtdiag_start}_${yearm1}_rtd073" # last year rtd file
    rtdfileo="${uxxx_rtd}_${runid}_${year_rtdiag_start}_${yearmo}_rtd073" # older rtd file to be deleted (depends on keep_old_rtdiag_number)
    echo yearm1=$yearm1 yearmo=$yearmo
    echo rtdfile1=$rtdfile1 rtdfileo=$rtdfileo

#                                      some common input cards
    echo "C*GGATIM      1    1    1    1" > ic.ggatim
    echo "C*WINDOW      1    1    1    1" > ic.window

# ------------------------------------ access last-year rtd file (except for the very first year)

    if [ $year -gt ${year_rtdiag_start} ] ; then
      access oldrtd $rtdfile1
    fi

# ------------------------------------ Save version and current year

    VERS=`echo $version | awk '{printf "%22.15e", $1}'`
    echo " TIME      1001 VERS         1         1         1    100101        -1
$VERS" > vers.txt
    YEAR=`echo $year | awk '{printf "%22.15e", $1}'`
    echo " TIME      1001 YEAR         1         1         1    100101        -1
$YEAR" > year.txt
    chabin vers.txt vers
    chabin year.txt year

    echo "C*XFIND       VERSION                                      (ANN ${year_rtdiag_start}-${yearm1})
C*XFIND       YEAR                                         (ANN ${year_rtdiag_start}-${yearm1})" >> ic.xfind
    echo "C*XSAVE       VERSION                                      (ANN ${year_rtdiag_start}-${year})
NEWNAM     VERS
C*XSAVE       YEAR                                         (ANN ${year_rtdiag_start}-${year})
NEWNAM     YEAR" >> ic.xsave
    xsave dummy vers year newrtd input=ic.xsave

# ------------------------------------ maximum number of variables that can be selected/xsaved

    nselmax=80
    nselmaxp1=`expr $nselmax + 1`
    nselmax2=`expr $nselmax + $nselmax`
    nselmax2p1=`expr $nselmax2 + 1`

    nvarmax=70
    nvarmaxp1=`expr $nvarmax + 1`
    nvarmax2=`expr $nvarmax + $nvarmax`
    nvarmax2p1=`expr $nvarmax2 + 1`

# ------------------------------------ month loop

    month_names="JAN FEB MAR APR MAY JUN JUL AUG SEP OCT NOV DEC"
    month_lengths="31 28 31 30 31 30 31 31 30 31 30 31"
    rtdlist_ann=""
    rm ic.xfind_ann
    rm ic.xsave_ann
    for m in 01 02 03 04 05 06 07 08 09 10 11 12 ; do
      days=`echo ${month_names}   | cut -f${m} -d' '`
      lmon=`echo ${month_lengths} | cut -f${m} -d' '`

      rtdlist=""
      rm ic.xfind
      rm ic.xsave
#                                      access model files and select variables
#                                      gs file
      if [ -n "$gsvars" ] ; then
        release gs${m}
        access  gs${m} ${uxxx}_${runid}_${year}_m${m}_gs
        if [ ${m} -eq 1 ] ; then
#                                      add additional time-invariant variables for m01
# FARE (3-level) 1=land, 2=ocean sea water, 3=ocean sea ice
# FARE (6-level) 1=land, 2=unresolved lakes, 3=resolved lakes water, 4=resolved lakes ice, 5=ocean sea water, 6=ocean sea ice
# FARE(ice) = SICN*(1-FLND)
# FARE(wat) = (1-SICN)*(1-FLND)
# FARE(ice)+FARE(wat)=1-FARE(lnd)
          gsvars1="$gsvars DPTH DZG FLND FLKR FLKU"
        else
          gsvars1="$gsvars"
        fi

        varlist=`echo $gsvars1 | sed -e 's/^ *//' -e 's/  */ /g' -e 's/ *$/ /'`
        varlist0="$varlist"
        while [ -n "$varlist0" ] ; do
          varlist1=`echo "$varlist0" | cut -f1-$nselmax -d' '`
          GSVARS=`fmtselname $varlist1`
          echo "C*SELECT   STEP         0 999999999    1     -900199999 NAME$GSVARS" | ccc select gs${m} $varlist1 || true
          varlist0=`echo "$varlist0" | cut -f$nselmaxp1- -d' '`
        done
#                                      rename variables
        for x in $gsvars1 ; do
          if [ -s $x ] ; then
            mv $x gs${x}${m}
          fi
        done

#       # eliminate first step from SICN and FARE to make it consistent with coupler RTDs
#       x="SICN"
#       if [ -s gs${x}${m} ] ; then
#         echo "C* RCOPY           2 999999999" | ccc rcopy gs${x}${m} gs${x}${m}2 ; mv gs${x}${m}2 gs${x}${m}
#       fi
#       x="FARE"
#       if [ -s gs${x}${m} ] ; then
#         echo "C* RCOPY           4 999999999" | ccc rcopy gs${x}${m} gs${x}${m}2 ; mv gs${x}${m}2 gs${x}${m}
#       fi
#       # eliminate last time step
#       nsteplast=`echo $lmon $delt $ksteps | awk '{printf "%10d",$1*86400/$2/$3-1}'`
#       echo nsteplast=$nsteplast
#       for x in PCP QFS BEGI BEGO FSGI FSGO HFLI BWGO BWGI ; do
#         if [ -s gs${x}${m} ] ; then
#           echo "C* RCOPY           1$nsteplast" | ccc rcopy gs${x}${m} gs${x}${m}2 ; mv gs${x}${m}2 gs${x}${m}
#         fi
#       done
#       nsteplast3=`echo $nsteplast | awk '{printf "%10d",$1*3}'`
#       echo nsteplast3=$nsteplast3
#       for x in SNOT GTT ; do
#         if [ -s gs${x}${m} ] ; then
#           echo "C* RCOPY           1$nsteplast3" | ccc rcopy gs${x}${m} gs${x}${m}2 ; mv gs${x}${m}2 gs${x}${m}
#         fi
#       done

#                                      select fractional mask etc. from gs file
        if [ ${m} -eq 1 ]; then
          # fractional masks
          cp gsFLND01 dland_frac # dry land fraction (no unresolved lakes)
          echo "C*XLIN           0.0      0.E0" | ccc xlin dland_frac ulake_frac # unresolved (parameterized) lakes fraction (assumed 0 for now)
          echo "C*XLIN           0.0      0.E0" | ccc xlin dland_frac rlake_frac # resolved lake fraction (assumed 0 for now)
          add dland_frac ulake_frac tland_frac # total land fraction (dry land + unresolved lakes)
          echo "C*FMASK           -1 NEXT   EQ        1." | ccc fmask dland_frac lland_frac # land only (no fractional dry land points)
          echo "C*XLIN          -1.0      1.E0" | ccc xlin tland_frac water_frac # water fraction (1-tland_frac = ocean + resolved lakes)
          sub water_frac rlake_frac ocean_frac # ocean fraction

          # glaciers/bedrock binary mask
          echo "C*FMASK           -1 NEXT   LT      -3.5                   1" | ccc fmask gsDPTH01 glaci_mask # glacier binary mask (DPTH=-4)
          echo "C*FMASK           -1 NEXT   LT      -2.5                   1" | ccc fmask gsDPTH01 glbed_mask # glacier & bedrock binary mask (DPTH=-3 + DPTH=-4)
          sub glbed_mask glaci_mask bedro_mask # bedrock binary mask (DPTH=-3)
          echo "C*FMASK           -1 NEXT   GT      -3.5                   1" | ccc fmask gsDPTH01 nogla_mask # non-glacier land binary mask
          echo "C*FMASK           -1 NEXT   GT      -2.5                   1" | ccc fmask gsDPTH01 noglb_mask # non-glacier/bedrock land binary mask
          # glaciers/bedrock fractional mask
          mlt dland_frac glaci_mask glaci_frac
          mlt dland_frac bedro_mask bedro_frac
          mlt dland_frac nogla_mask nogla_frac
          mlt dland_frac noglb_mask noglb_frac

          # globe mask (1 everywhere)
          echo "C*XLIN            0.        1." | ccc xlin dland_frac globe_frac

          # NH and SH masks
          nlath=`echo $nlat | awk '{printf "%5d",$1/2}'`
          nlatp=`echo $nlat | awk '{printf "%5d",$1/2+1}'`
          nlatnhe=`echo $nlat | awk '{printf "%5.0f",2*$1/3+1}'` # north of 30N
          nlatshe=`echo $nlat | awk '{printf "%5.0f",$1/3}'`     # south of 30S
          nlatnhem1=`echo $nlatnhe | awk '{printf "%5d",$1-1}'`  # south of 30N
          nlatshep1=`echo $nlatshe | awk '{printf "%5d",$1+1}'`  # north of 30S
          nlatnhp=`echo $nlat | awk '{printf "%5.0f",$1*5/6+2}'` # north of 60N
          nlatshp=`echo $nlat | awk '{printf "%5.0f",$1*1/6-1}'` # south of 60S
          nlatshp1=`echo $nlat | awk '{printf "%5.0f",$1*1/6}'`  # north of 60S
          echo nlath=$nlath nlatp=$nlatp nlatnhe=$nlatnhe nlatshe=$nlatshe nlatnhp=$nlatnhp nlatshp=$nlatshp
          echo "C*WINDOW      1 9999    1$nlath         1" | ccc window globe_frac globe_frac_sh
          echo "C*WINDOW      1 9999$nlatp 9999         1" | ccc window globe_frac globe_frac_nh
          echo "C*WINDOW      1 9999    1$nlath         1" | ccc window ocean_frac ocean_frac_sh
          echo "C*WINDOW      1 9999$nlatp 9999         1" | ccc window ocean_frac ocean_frac_nh
          echo "C*WINDOW      1 9999    1$nlath         1" | ccc window water_frac water_frac_sh
          echo "C*WINDOW      1 9999$nlatp 9999         1" | ccc window water_frac water_frac_nh
          echo "C*WINDOW      1 9999    1$nlath         1" | ccc window tland_frac tland_frac_sh
          echo "C*WINDOW      1 9999$nlatp 9999         1" | ccc window tland_frac tland_frac_nh
          echo "C*WINDOW      1 9999$nlatnhe 9999         1"        | ccc window globe_frac globe_frac_nhe # globe north of 30N
          echo "C*WINDOW      1 9999    1$nlatshe         1"        | ccc window globe_frac globe_frac_she # globe south of 30S
          echo "C*WINDOW      1 9999$nlatshep1$nlatnhem1         1" | ccc window globe_frac globe_frac_tro # globe tropics (30S-30N)
          echo "C*WINDOW      1 9999$nlatnhe 9999         1"        | ccc window tland_frac tland_frac_nhe # land north of 30N
          echo "C*WINDOW      1 9999    1$nlatshe         1"        | ccc window tland_frac tland_frac_she # land south of 30S
          echo "C*WINDOW      1 9999$nlatshep1$nlatnhem1         1" | ccc window tland_frac tland_frac_tro # land tropics (30S-30N)
          echo "C*WINDOW      1 9999$nlatnhe 9999         1"        | ccc window ocean_frac ocean_frac_nhe # ocean north of 30N
          echo "C*WINDOW      1 9999    1$nlatshe         1"        | ccc window ocean_frac ocean_frac_she # ocean south of 30S
          echo "C*WINDOW      1 9999$nlatshep1$nlatnhem1         1" | ccc window ocean_frac ocean_frac_tro # ocean tropics (30S-30N)
          echo "C*WINDOW      1 9999$nlatnhp 9999         1"        | ccc window globe_frac globe_frac_nhp # north of 60N
          echo "C*WINDOW      1 9999    1$nlatshp         1"        | ccc window globe_frac globe_frac_shp # south of 60S
          echo "C*WINDOW      1 9999$nlatshp1 9999         1"       | ccc window tland_frac noant_frac # land without Antarctica

          # fraction averages
          globavg globe_frac _ globe_favg
          globavg dland_frac _ dland_favg
          globavg tland_frac _ tland_favg
          globavg lland_frac _ lland_favg
          globavg ocean_frac _ ocean_favg
          globavg rlake_frac _ rlake_favg
          globavg ulake_frac _ ulake_favg
          globavg water_frac _ water_favg
          globavg nogla_frac _ nogla_favg
          globavg noglb_frac _ noglb_favg
          globavg glaci_frac _ glaci_favg
          globavg bedro_frac _ bedro_favg
          globavg globe_frac_nh _ globe_favg_nh
          globavg globe_frac_sh _ globe_favg_sh
          globavg ocean_frac_nh _ ocean_favg_nh
          globavg ocean_frac_sh _ ocean_favg_sh
          globavg water_frac_nh _ water_favg_nh
          globavg water_frac_sh _ water_favg_sh
          globavg tland_frac_nh _ tland_favg_nh
          globavg tland_frac_sh _ tland_favg_sh
          globavg tland_frac_nhe _ tland_favg_nhe
          globavg tland_frac_she _ tland_favg_she
          globavg tland_frac_tro _ tland_favg_tro
          globavg globe_frac_nhp _ globe_favg_nhp
          globavg globe_frac_shp _ globe_favg_shp

          # areas (m2)
          # earth_radius=6.371220E06 # AGCM
          globe_area_km2="510099699." # AGCM value
          echo "C*XLIN            0.${globe_area_km2}" | ccc xlin globe_favg globe_area_km2 # km2
          echo "C*XLIN          1.E6        0." | ccc xlin globe_area_km2 globe_area # m2
          mlt globe_area dland_favg dland_area
          mlt globe_area tland_favg tland_area
          mlt globe_area lland_favg lland_area
          mlt globe_area ocean_favg ocean_area
          mlt globe_area rlake_favg rlake_area
          mlt globe_area ulake_favg ulake_area
          mlt globe_area water_favg water_area
          mlt globe_area nogla_favg nogla_area
          mlt globe_area noglb_favg noglb_area
          mlt globe_area glaci_favg glaci_area
          mlt globe_area bedro_favg bedro_area
          mlt globe_area globe_favg_nh globe_area_nh
          mlt globe_area globe_favg_sh globe_area_sh
          mlt globe_area ocean_favg_nh ocean_area_nh
          mlt globe_area ocean_favg_sh ocean_area_sh
          mlt globe_area water_favg_nh water_area_nh
          mlt globe_area water_favg_sh water_area_sh
          mlt globe_area tland_favg_nh tland_area_nh
          mlt globe_area tland_favg_sh tland_area_sh
          mlt globe_area tland_favg_nhe tland_area_nhe
          mlt globe_area tland_favg_she tland_area_she
          mlt globe_area tland_favg_tro tland_area_tro
          mlt globe_area globe_favg_nhp globe_area_nhp
          mlt globe_area globe_favg_shp globe_area_shp
          # averaged soil level depth
          echo "C*BINS       -1" | ccc bins gsDZG01  gsDZGavg
        fi # m=1
      fi # gsvars

#                                      select area tiles (these are AGCM grid-cell fractions)
      x="FARE"
      echo "C*SELLEV      1$ilnd" | ccc sellev gs${x}${m} gs${x}l${m} # fraction of dry land
      echo "C*SELLEV      1$iwat" | ccc sellev gs${x}${m} gs${x}w${m} # fraction of ocean sea water
      echo "C*SELLEV      1$isic" | ccc sellev gs${x}${m} gs${x}i${m} # fraction of ocean sea ice
#                                      these are needed when computing ocean averages
      gdiv gs${x}w${m} ocean_frac gs${x}wo${m} #  sea water fraction (ocean portion fraction)
      gdiv gs${x}i${m} ocean_frac gs${x}io${m} #  sea ice fraction (ocean portion fraction)

      # fraction averages
      globavg gs${x}l${m} _ gs${x}l${m}_favg
      globavg gs${x}w${m} _ gs${x}w${m}_favg
      globavg gs${x}i${m} _ gs${x}i${m}_favg
      # areas (m2)
      mlt globe_area gs${x}l${m}_favg gs${x}l${m}_area
      mlt globe_area gs${x}w${m}_favg gs${x}w${m}_area
      mlt globe_area gs${x}i${m}_favg gs${x}i${m}_area

#                                      ss file
      if [ -n "$ssvars" ] ; then
        release ss${m}
        access  ss${m} ${uxxx}_${runid}_${year}_m${m}_ss
        SSVARS=`fmtselname $ssvars`
        echo "C*SELECT   STEP         0 999999999    1     -900199999 NAME$SSVARS" | ccc select ss${m} $ssvars || true
        for x in $ssvars ; do
          if [ -s $x ] ; then
            mv $x ss${x}${m}
          fi
        done
      fi # ssvars
#                                      cm file
      if [ -n "$cmvars" ] ; then
        release cm${m}
        access  cm${m} ${uxxx}_${runid}_${year}_m${m}_cm
        CMVARS=`fmtselname $cmvars`
        echo "C*SELECT   STEP         0 999999999    1     -900199999 NAME$CMVARS" | ccc select cm${m} $cmvars || true
        for x in $cmvars ; do
          if [ -s $x ] ; then
            mv $x cm${x}${m}
          fi
        done
      fi # cmvars
#                                      tm file
      if [ -n "$tmvars" ] ; then
        release tm${m}
        access  tm${m} ${uxxx}_${runid}_${year}_m${m}_tm
        TMVARS=`fmtselname $tmvars`
        echo "C*SELECT   STEP         0 999999999    1     -900199999 NAME$TMVARS" | ccc select tm${m} $tmvars || true
        for x in $tmvars ; do
          if [ -s $x ] ; then
            mv $x cm${x}${m} # not changing CTEM variable names to tm here so that we don't need to
                             # modify code further down
          fi
        done
      fi # tmvars
#                                      gz file
      if [ -n "$gzvars" ] ; then
        release gz${m}
        access  gz${m} ${uxxx}_${runid}_${year}_m${m}_gz
        GZVARS=`fmtselname $gzvars`
        echo "C*SELECT   STEP         0 999999999    1     -900199999 NAME$GZVARS" | ccc select gz${m} $gzvars || true
        for x in $gzvars ; do
          if [ -s $x ] ; then
            mv $x gz${x}${m}
          fi
        done
#                                      try to select left-shifted OPH, if OPH does not exist
        if [ "$CarbO" = "on" ] ; then
          if [ ! -s gzOPH${m} ] ; then
            echo "C*SELECT   STEP         0 999999999    1     -900199999 NAME OPH " | ccc select gz${m} gzOPH${m}
          fi
        fi

#                                      get ocean grid geometry
        if [ ${m} -eq 1 ] ; then
          grido gz${m} betao da dx dy dz
          echo "C*SELECT   STEP         0 999999999    1     -900199999 NAME  LAT  LON" | ccc select gz${m} lato lono
#                                      area weights of the upper layer
          echo "C*RCOPY            1         1" | ccc rcopy betao betao1
          mlt betao1 da bda1
#                                      tropics, northern and southern extratropics masks
          echo "C* FMSKPLT        -1 NEXT   GT      30.0    1        0.    1" | ccc fmskplt bda1 bdanhe1 lato
          echo "C* FMSKPLT        -1 NEXT   LT     -30.0    1        0.    1" | ccc fmskplt bda1 bdashe1 lato
          echo "C* FMSKPLT        -1 NEXT   LT      30.0    1        0.    1" | ccc fmskplt bda1 bda2    lato
          echo "C* FMSKPLT        -1 NEXT   GT     -30.0    1        0.    1" | ccc fmskplt bda2 bdatro1 lato
          release bda2
#                                      relabel level to 0
          echo "
                                  0" | ccc relabl bda1 bda0
#                                      relabel level to 1
          echo "
                                  1" | ccc relabl da da1
#                                      ocean depth
          vzint betao dz betaovi
#                                      global mean depth
          globavw betaovi da1 _ _ gbetaovi gbetaovol
#         ggstat gbetaovi
#         ggstat gbetaovol
#                                      global mean depth at various levels
          for depth in $upper_ocean_depths ; do
            depth10=${depth}0
            echo "C*  VZINTV         0${depth10}" | ccc vzintv betao dz betaovi${depth}
            globavw betaovi${depth} da1 _ _ gbetaovi${depth}
          done
#                                      extract data description records
          echo "C*XFIND.      DATA DESCRIPTION" | ccc xfind gz${m} gz_data_description
          gznlev=`ggstat gz_data_description | grep 'GRID         0     Z' | cut -c'42-46'`
          gznlat=`ggstat gz_data_description | grep 'GRID         0   LAT' | cut -c'50-54'`
#                                      define nino regions masks
          echo "C*SELECT.  STEP         0         0    1         0    0 NAME  LAT  LON" | ccc select gz_data_description lat lon
          zonavg lat zlat
          # NINO3:  5N-5S,  90W-150W
          # NINO4:  5N-5S, 150W-160E
          # NINO34: 5N-5S, 120W-170W
          # NINO12: 0-10S,  80W-90W

          echo "C*FMASK           -1 NEXT   GE       -5.                   1" | ccc fmask lat latm5
          echo "C*FMASK           -1 NEXT   LE        5.                   1" | ccc fmask lat latp5
          mlt latm5 latp5 latm5p5

          # nino3: 90�W-150�W and 5�S-5�N.
          echo "C*FMASK           -1 NEXT   GE      210.                   1" | ccc fmask lon lon210
          echo "C*FMASK           -1 NEXT   LE      270.                   1" | ccc fmask lon lon270
          mlt lon210 lon270 lon210to270
          mlt latm5p5 lon210to270 nino3
          mlt bda1 nino3 bda_nino3

          # nino4: 150�W-160�E and 5�S-5�N.
          echo "C*FMASK           -1 NEXT   GE      160.                   1" | ccc fmask lon lon160
          echo "C*FMASK           -1 NEXT   LE      210.                   1" | ccc fmask lon lon210
          mlt lon160 lon210 lon160to210
          mlt latm5p5 lon160to210 nino4
          mlt bda1 nino4 bda_nino4

          # nino3.4: 120�W-170�W and 5�S-5�N.
          echo "C*FMASK           -1 NEXT   GE      190.                   1" | ccc fmask lon lon190
          echo "C*FMASK           -1 NEXT   LE      240.                   1" | ccc fmask lon lon240
          mlt lon190 lon240 lon190to240
          mlt latm5p5 lon190to240 nino34
          mlt bda1 nino34 bda_nino34

          # nino1.2: 80�W-90�W and 10�S-0�N.
          echo "C*FMASK           -1 NEXT   GE      -10.                   1" | ccc fmask lat latm10
          echo "C*FMASK           -1 NEXT   LE        0.                   1" | ccc fmask lat latp10
          mlt latm10 latp10 latm10p10
          echo "C*FMASK           -1 NEXT   GE      270.                   1" | ccc fmask lon lon270
          echo "C*FMASK           -1 NEXT   LE      280.                   1" | ccc fmask lon lon280
          mlt lon270 lon280 lon270to280
          mlt latm10p10 lon270to280 nino12
          mlt bda1 nino12 bda_nino12

          # mask latitudes selecting nearest grid location to 26N and 48N
          if [ $gznlat -eq 192 ] ; then
            echo "C*FMASK           -1 NEXT   GE      25.5                   1" | ccc fmask zlat zlat25
            echo "C*FMASK           -1 NEXT   LE      26.5                   1" | ccc fmask zlat zlat26
            mlt zlat25 zlat26 zlat25to26

            echo "C*FMASK           -1 NEXT   GE      47.5                   1" | ccc fmask zlat zlat47
            echo "C*FMASK           -1 NEXT   LE      48.5                   1" | ccc fmask zlat zlat48
            mlt zlat47 zlat48 zlat47to48
          elif [ $gznlat -eq 96 ] ; then
            echo "C*FMASK           -1 NEXT   GE      25.0                   1" | ccc fmask zlat zlat25
            echo "C*FMASK           -1 NEXT   LE      26.8                   1" | ccc fmask zlat zlat26
            mlt zlat25 zlat26 zlat25to26

            echo "C*FMASK           -1 NEXT   GE      47.1                   1" | ccc fmask zlat zlat47
            echo "C*FMASK           -1 NEXT   LE      48.8                   1" | ccc fmask zlat zlat48
            mlt zlat47 zlat48 zlat47to48
          else
            echo "ERROR: invalid gznlat=$gznlat"
            exit 1
          fi
#                                      define Atlantic basin
          echo "                 ATL" | ccc masko gz_data_description atlreg
        fi # m=1
      fi # gzvars

# #################################### Physical Atmosphere section

      if [ "$PhysA" = "on" ] ; then

# ------------------------------------ monthly time stamps

        x="SECS"
        gsfile=`readlink gs${m}`
        datesec=`stat -c %Y $gsfile`
        echo " TIME      1001 SECS         0         1         1    100101        -1
$datesec.E00" > datesec.txt
        chabin datesec.txt gtgs${x}${m}
        rtdlist="$rtdlist gtgs${x}${m}"
        echo "C*XFIND       DATE IN SECONDS SINCE 1970                   (${days} ${year_rtdiag_start}-${yearm1})" >> ic.xfind
        echo "C*XSAVE       DATE IN SECONDS SINCE 1970                   (${days} ${year_rtdiag_start}-${year})
NEWNAM     SECS" >> ic.xsave

# ------------------------------------ monthly ST

        x="ST"
        timavg gs${x}${m} tgs${x}${m}
#                                      global
        globavg tgs${x}${m} _ gtgs${x}${m}g
#                                      dry land
        globavg tgs${x}${m} _ gtgs${x}${m}l dland_frac
#                                      ocean
        globavg tgs${x}${m} _ gtgs${x}${m}o ocean_frac
#                                      land excluding glaciers
        globavg tgs${x}${m} _ gtgs${x}${m}n nogla_frac
#                                      land excluding Antarctica
        globavg tgs${x}${m} _ gtgs${x}${m}c noant_frac
#                                      glaciers
        globavg tgs${x}${m} _ gtgs${x}${m}i glaci_frac
#                                      NH polar cap
        globavg tgs${x}${m} _ gtgs${x}${m}nhp globe_frac_nhp
#                                      SH polar cap
        globavg tgs${x}${m} _ gtgs${x}${m}shp globe_frac_shp

        rtdlist="$rtdlist gtgs${x}${m}g gtgs${x}${m}l gtgs${x}${m}o gtgs${x}${m}n gtgs${x}${m}c gtgs${x}${m}i gtgs${x}${m}nhp gtgs${x}${m}shp"
        echo "C*XFIND       SCREEN TEMPERATURE GLOBAL (K)                (${days} ${year_rtdiag_start}-${yearm1})
C*XFIND       SCREEN TEMPERATURE LAND (K)                  (${days} ${year_rtdiag_start}-${yearm1})
C*XFIND       SCREEN TEMPERATURE OCEAN (K)                 (${days} ${year_rtdiag_start}-${yearm1})
C*XFIND       SCREEN TEMPERATURE LAND EXCL.GLACIERS (K)    (${days} ${year_rtdiag_start}-${yearm1})
C*XFIND       SCREEN TEMPERATURE LAND EXCL.ANTARCTICA (K)  (${days} ${year_rtdiag_start}-${yearm1})
C*XFIND       SCREEN TEMPERATURE GLACIERS (K)              (${days} ${year_rtdiag_start}-${yearm1})
C*XFIND       SCREEN TEMPERATURE NORTH OF 60N (K)          (${days} ${year_rtdiag_start}-${yearm1})
C*XFIND       SCREEN TEMPERATURE SOUTH OF 60S (K)          (${days} ${year_rtdiag_start}-${yearm1})" >> ic.xfind
        echo "C*XSAVE       SCREEN TEMPERATURE GLOBAL (K)                (${days} ${year_rtdiag_start}-${year})
NEWNAM       ST
C*XSAVE       SCREEN TEMPERATURE LAND (K)                  (${days} ${year_rtdiag_start}-${year})
NEWNAM       ST
C*XSAVE       SCREEN TEMPERATURE OCEAN (K)                 (${days} ${year_rtdiag_start}-${year})
NEWNAM       ST
C*XSAVE       SCREEN TEMPERATURE LAND EXCL.GLACIERS (K)    (${days} ${year_rtdiag_start}-${year})
NEWNAM       ST
C*XSAVE       SCREEN TEMPERATURE LAND EXCL.ANTARCTICA (K)  (${days} ${year_rtdiag_start}-${year})
NEWNAM       ST
C*XSAVE       SCREEN TEMPERATURE GLACIERS (K)              (${days} ${year_rtdiag_start}-${year})
NEWNAM       ST
C*XSAVE       SCREEN TEMPERATURE NORTH OF 60N (K)          (${days} ${year_rtdiag_start}-${year})
NEWNAM       ST
C*XSAVE       SCREEN TEMPERATURE SOUTH OF 60S (K)          (${days} ${year_rtdiag_start}-${year})
NEWNAM       ST" >> ic.xsave

# ------------------------------------ monthly STMX

        x="STMX"
        timavg gs${x}${m} tgs${x}${m}
#                                      global
        globavg tgs${x}${m} _ gtgs${x}${m}g
#                                      land
        globavg tgs${x}${m} _ gtgs${x}${m}l tland_frac
#                                      ocean
        globavg tgs${x}${m} _ gtgs${x}${m}o ocean_frac

        rtdlist="$rtdlist gtgs${x}${m}g gtgs${x}${m}l gtgs${x}${m}o"
        echo "C*XFIND       SCREEN TEMPERATURE MAX GLOBAL (K)            (${days} ${year_rtdiag_start}-${yearm1})
C*XFIND       SCREEN TEMPERATURE MAX LAND (K)              (${days} ${year_rtdiag_start}-${yearm1})
C*XFIND       SCREEN TEMPERATURE MAX OCEAN (K)             (${days} ${year_rtdiag_start}-${yearm1})" >> ic.xfind
        echo "C*XSAVE       SCREEN TEMPERATURE MAX GLOBAL (K)            (${days} ${year_rtdiag_start}-${year})
NEWNAM     STMX
C*XSAVE       SCREEN TEMPERATURE MAX LAND (K)              (${days} ${year_rtdiag_start}-${year})
NEWNAM     STMX
C*XSAVE       SCREEN TEMPERATURE MAX OCEAN (K)             (${days} ${year_rtdiag_start}-${year})
NEWNAM     STMX" >> ic.xsave

# ------------------------------------ monthly STMX max

        x="STMX"
        timmax gs${x}${m} tgs${x}max${m}
#                                      global
        globavg tgs${x}max${m} _ gtgs${x}max${m}g
#                                      land
        globavg tgs${x}max${m} _ gtgs${x}max${m}l tland_frac
#                                      ocean
        globavg tgs${x}max${m} _ gtgs${x}max${m}o ocean_frac

        rtdlist="$rtdlist gtgs${x}max${m}g gtgs${x}max${m}l gtgs${x}max${m}o"
        echo "C*XFIND       MAX SCREEN TEMPERATURE MAX GLOBAL (K)        (${days} ${year_rtdiag_start}-${yearm1})
C*XFIND       MAX SCREEN TEMPERATURE MAX LAND (K)          (${days} ${year_rtdiag_start}-${yearm1})
C*XFIND       MAX SCREEN TEMPERATURE MAX OCEAN (K)         (${days} ${year_rtdiag_start}-${yearm1})" >> ic.xfind
        echo "C*XSAVE       MAX SCREEN TEMPERATURE MAX GLOBAL (K)        (${days} ${year_rtdiag_start}-${year})
NEWNAM     STMX
C*XSAVE       MAX SCREEN TEMPERATURE MAX LAND (K)          (${days} ${year_rtdiag_start}-${year})
NEWNAM     STMX
C*XSAVE       MAX SCREEN TEMPERATURE MAX OCEAN (K)         (${days} ${year_rtdiag_start}-${year})
NEWNAM     STMX" >> ic.xsave

# ------------------------------------ monthly STMN

        x="STMN"
        timavg gs${x}${m} tgs${x}${m}
#                                      global
        globavg tgs${x}${m} _ gtgs${x}${m}g
#                                      land
        globavg tgs${x}${m} _ gtgs${x}${m}l tland_frac
#                                      ocean
        globavg tgs${x}${m} _ gtgs${x}${m}o ocean_frac

        rtdlist="$rtdlist gtgs${x}${m}g gtgs${x}${m}l gtgs${x}${m}o"
        echo "C*XFIND       SCREEN TEMPERATURE MIN GLOBAL (K)            (${days} ${year_rtdiag_start}-${yearm1})
C*XFIND       SCREEN TEMPERATURE MIN LAND (K)              (${days} ${year_rtdiag_start}-${yearm1})
C*XFIND       SCREEN TEMPERATURE MIN OCEAN (K)             (${days} ${year_rtdiag_start}-${yearm1})" >> ic.xfind
        echo "C*XSAVE       SCREEN TEMPERATURE MIN GLOBAL (K)            (${days} ${year_rtdiag_start}-${year})
NEWNAM     STMN
C*XSAVE       SCREEN TEMPERATURE MIN LAND (K)              (${days} ${year_rtdiag_start}-${year})
NEWNAM     STMN
C*XSAVE       SCREEN TEMPERATURE MIN OCEAN (K)             (${days} ${year_rtdiag_start}-${year})
NEWNAM     STMN" >> ic.xsave

# ------------------------------------ monthly STMN min

        x="STMN"
        timmin gs${x}${m} tgs${x}min${m}
#                                      global
        globavg tgs${x}min${m} _ gtgs${x}min${m}g
#                                      land
        globavg tgs${x}min${m} _ gtgs${x}min${m}l tland_frac
#                                      ocean
        globavg tgs${x}min${m} _ gtgs${x}min${m}o ocean_frac

        rtdlist="$rtdlist gtgs${x}min${m}g gtgs${x}min${m}l gtgs${x}min${m}o"
        echo "C*XFIND       MIN SCREEN TEMPERATURE MIN GLOBAL (K)        (${days} ${year_rtdiag_start}-${yearm1})
C*XFIND       MIN SCREEN TEMPERATURE MIN LAND (K)          (${days} ${year_rtdiag_start}-${yearm1})
C*XFIND       MIN SCREEN TEMPERATURE MIN OCEAN (K)         (${days} ${year_rtdiag_start}-${yearm1})" >> ic.xfind
        echo "C*XSAVE       MIN SCREEN TEMPERATURE MIN GLOBAL (K)        (${days} ${year_rtdiag_start}-${year})
NEWNAM     STMN
C*XSAVE       MIN SCREEN TEMPERATURE MIN LAND (K)          (${days} ${year_rtdiag_start}-${year})
NEWNAM     STMN
C*XSAVE       MIN SCREEN TEMPERATURE MIN OCEAN (K)         (${days} ${year_rtdiag_start}-${year})
NEWNAM     STMN" >> ic.xsave

# ------------------------------------ monthly SQ

        x="SQ"
        if [ -s gs${x}${m} ] ; then
        timavg gs${x}${m} tgs${x}${m}
#                                      global
        globavg tgs${x}${m} _ gtgs${x}${m}g
#                                      land
        globavg tgs${x}${m} _ gtgs${x}${m}l tland_frac
#                                      ocean
        globavg tgs${x}${m} _ gtgs${x}${m}o ocean_frac

        rtdlist="$rtdlist gtgs${x}${m}g gtgs${x}${m}l gtgs${x}${m}o"
        echo "C*XFIND       SCREEN SPECIFIC HUMIDITY GLOBAL              (${days} ${year_rtdiag_start}-${yearm1})
C*XFIND       SCREEN SPECIFIC HUMIDITY LAND                (${days} ${year_rtdiag_start}-${yearm1})
C*XFIND       SCREEN SPECIFIC HUMIDITY OCEAN               (${days} ${year_rtdiag_start}-${yearm1})" >> ic.xfind
        echo "C*XSAVE       SCREEN SPECIFIC HUMIDITY GLOBAL              (${days} ${year_rtdiag_start}-${year})
NEWNAM       SQ
C*XSAVE       SCREEN SPECIFIC HUMIDITY LAND                (${days} ${year_rtdiag_start}-${year})
NEWNAM       SQ
C*XSAVE       SCREEN SPECIFIC HUMIDITY OCEAN               (${days} ${year_rtdiag_start}-${year})
NEWNAM       SQ" >> ic.xsave
        fi

# ------------------------------------ monthly SRH

        x="SRH"
        if [ -s gs${x}${m} ] ; then
        timavg gs${x}${m} tgs${x}${m}
#                                      global
        globavg tgs${x}${m} _ gtgs${x}${m}g
#                                      land
        globavg tgs${x}${m} _ gtgs${x}${m}l tland_frac
#                                      ocean
        globavg tgs${x}${m} _ gtgs${x}${m}o ocean_frac

        rtdlist="$rtdlist gtgs${x}${m}g gtgs${x}${m}l gtgs${x}${m}o"
        echo "C*XFIND       SCREEN RELATIVE HUMIDITY GLOBAL              (${days} ${year_rtdiag_start}-${yearm1})
C*XFIND       SCREEN RELATIVE HUMIDITY LAND                (${days} ${year_rtdiag_start}-${yearm1})
C*XFIND       SCREEN RELATIVE HUMIDITY OCEAN               (${days} ${year_rtdiag_start}-${yearm1})" >> ic.xfind
        echo "C*XSAVE       SCREEN RELATIVE HUMIDITY GLOBAL              (${days} ${year_rtdiag_start}-${year})
NEWNAM      SRH
C*XSAVE       SCREEN RELATIVE HUMIDITY LAND                (${days} ${year_rtdiag_start}-${year})
NEWNAM      SRH
C*XSAVE       SCREEN RELATIVE HUMIDITY OCEAN               (${days} ${year_rtdiag_start}-${year})
NEWNAM      SRH" >> ic.xsave
        fi

# ------------------------------------ monthly CLDT

        x="CLDT"
        if [ -s gs${x}${m} ] ; then
        timavg gs${x}${m} tgs${x}${m}
#                                      global
        globavg tgs${x}${m} _ gtgs${x}${m}g
#                                      land
        globavg tgs${x}${m} _ gtgs${x}${m}l tland_frac
#                                      ocean
        globavg tgs${x}${m} _ gtgs${x}${m}o ocean_frac

        rtdlist="$rtdlist gtgs${x}${m}g gtgs${x}${m}l gtgs${x}${m}o"
        echo "C*XFIND       GLOBAL TOTAL CLOUD FRACTION CLDT             (${days} ${year_rtdiag_start}-${yearm1})
C*XFIND       LAND TOTAL CLOUD FRACTION CLDT               (${days} ${year_rtdiag_start}-${yearm1})
C*XFIND       OCEAN TOTAL CLOUD FRACTION CLDT              (${days} ${year_rtdiag_start}-${yearm1})" >> ic.xfind
        echo "C*XSAVE       GLOBAL TOTAL CLOUD FRACTION CLDT             (${days} ${year_rtdiag_start}-${year})
NEWNAM     CLDT
C*XSAVE       LAND TOTAL CLOUD FRACTION CLDT               (${days} ${year_rtdiag_start}-${year})
NEWNAM     CLDT
C*XSAVE       OCEAN TOTAL CLOUD FRACTION CLDT              (${days} ${year_rtdiag_start}-${year})
NEWNAM     CLDT" >> ic.xsave
        fi

# ------------------------------------ monthly CLDO

        x="CLDO"
        if [ -s gs${x}${m} ] ; then
        timavg gs${x}${m} tgs${x}${m}
#                                      global
        globavg tgs${x}${m} _ gtgs${x}${m}g
#                                      land
        globavg tgs${x}${m} _ gtgs${x}${m}l tland_frac
#                                      ocean
        globavg tgs${x}${m} _ gtgs${x}${m}o ocean_frac

        rtdlist="$rtdlist gtgs${x}${m}g gtgs${x}${m}l gtgs${x}${m}o"
        echo "C*XFIND       GLOBAL TOTAL CLOUD FRACTION CLDO             (${days} ${year_rtdiag_start}-${yearm1})
C*XFIND       LAND TOTAL CLOUD FRACTION CLDO               (${days} ${year_rtdiag_start}-${yearm1})
C*XFIND       OCEAN TOTAL CLOUD FRACTION CLDO              (${days} ${year_rtdiag_start}-${yearm1})" >> ic.xfind
        echo "C*XSAVE       GLOBAL TOTAL CLOUD FRACTION CLDO             (${days} ${year_rtdiag_start}-${year})
NEWNAM     CLDO
C*XSAVE       LAND TOTAL CLOUD FRACTION CLDO               (${days} ${year_rtdiag_start}-${year})
NEWNAM     CLDO
C*XSAVE       OCEAN TOTAL CLOUD FRACTION CLDO              (${days} ${year_rtdiag_start}-${year})
NEWNAM     CLDO" >> ic.xsave
        fi

# ------------------------------------ monthly GT

        x="GT"
        timavg gs${x}${m} tgs${x}${m}
#                                      global
        globavg tgs${x}${m} _ gtgs${x}${m}g
#                                      dry land
        globavg tgs${x}${m} _ gtgs${x}${m}l dland_frac
#                                      resolved lakes
        globavg tgs${x}${m} _ gtgs${x}${m}r rlake_frac
#                                      glaciers
        globavg tgs${x}${m} _ gtgs${x}${m}i glaci_frac
#                                      ocean
        globavg tgs${x}${m} _ gtgs${x}${m}o ocean_frac

        rtdlist="$rtdlist gtgs${x}${m}g gtgs${x}${m}l gtgs${x}${m}r gtgs${x}${m}i gtgs${x}${m}o"
        echo "C*XFIND       SKIN TEMPERATURE GLOBAL (K)                  (${days} ${year_rtdiag_start}-${yearm1})
C*XFIND       SKIN TEMPERATURE LAND (K)                    (${days} ${year_rtdiag_start}-${yearm1})
C*XFIND       SKIN TEMPERATURE LAKES (K)                   (${days} ${year_rtdiag_start}-${yearm1})
C*XFIND       SKIN TEMPERATURE GLACIERS (K)                (${days} ${year_rtdiag_start}-${yearm1})
C*XFIND       SKIN TEMPERATURE OCEAN (K)                   (${days} ${year_rtdiag_start}-${yearm1})" >> ic.xfind
        echo "C*XSAVE       SKIN TEMPERATURE GLOBAL (K)                  (${days} ${year_rtdiag_start}-${year})
NEWNAM       GT
C*XSAVE       SKIN TEMPERATURE LAND (K)                    (${days} ${year_rtdiag_start}-${year})
NEWNAM       GT
C*XSAVE       SKIN TEMPERATURE LAKES (K)                   (${days} ${year_rtdiag_start}-${year})
NEWNAM       GT
C*XSAVE       SKIN TEMPERATURE GLACIERS (K)                (${days} ${year_rtdiag_start}-${year})
NEWNAM       GT
C*XSAVE       SKIN TEMPERATURE OCEAN (K)                   (${days} ${year_rtdiag_start}-${year})
NEWNAM       GT" >> ic.xsave

# ------------------------------------ monthly GTT (land/sea water/sea ice tiles)

        x="GTT"
        y="FARE"
        echo "C*SELLEV      1$ilnd" | ccc sellev gs${x}${m} gs${x}l${m} # GT over land tile
        echo "C*SELLEV      1$iwat" | ccc sellev gs${x}${m} gs${x}w${m} # GT over sea water
        echo "C*SELLEV      1$isic" | ccc sellev gs${x}${m} gs${x}i${m} # GT over sea ice
#                                      spread over grid cell
        mlt gs${x}l${m} gs${y}l${m} gs${x}lg${m}
        mlt gs${x}w${m} gs${y}w${m} gs${x}wg${m}
        mlt gs${x}i${m} gs${y}i${m} gs${x}ig${m}
#                                      add sea water and sea ice tiles = ocean tiles
        add gs${y}i${m} gs${y}w${m} gs${y}o${m}
        add gs${x}wg${m} gs${x}ig${m} gs${x}og${m}

#                                      global mean
        globavg gs${x}lg${m} _ ggs${x}lg${m}
        globavg gs${x}wg${m} _ ggs${x}wg${m}
        globavg gs${x}ig${m} _ ggs${x}ig${m}
        globavg gs${x}og${m} _ ggs${x}og${m}

        globavg gs${y}l${m} _ ggs${y}l${m}
        globavg gs${y}w${m} _ ggs${y}w${m}
        globavg gs${y}i${m} _ ggs${y}i${m}
        globavg gs${y}o${m} _ ggs${y}o${m}

        div ggs${x}lg${m} ggs${y}l${m} ggs${x}l${m}
        div ggs${x}wg${m} ggs${y}w${m} ggs${x}w${m}
        div ggs${x}ig${m} ggs${y}i${m} ggs${x}i${m}
        div ggs${x}og${m} ggs${y}o${m} ggs${x}o${m}
#                                      time average
        timavg ggs${x}l${m} tggs${x}l${m}
        timavg ggs${x}w${m} tggs${x}w${m}
        timavg ggs${x}i${m} tggs${x}i${m}
        timavg ggs${x}o${m} tggs${x}o${m}

        rtdlist="$rtdlist tggs${x}l${m} tggs${x}w${m} tggs${x}i${m} tggs${x}o${m}"
        echo "C*XFIND       SKIN TEMPERATURE LAND TILE (K)               (${days} ${year_rtdiag_start}-${yearm1})
C*XFIND       SKIN TEMPERATURE OPEN WATER TILE (K)         (${days} ${year_rtdiag_start}-${yearm1})
C*XFIND       SKIN TEMPERATURE SEA ICE TILE (K)            (${days} ${year_rtdiag_start}-${yearm1})
C*XFIND       SKIN TEMPERATURE OCEAN TILES (K)             (${days} ${year_rtdiag_start}-${yearm1})" >> ic.xfind
        echo "C*XSAVE       SKIN TEMPERATURE LAND TILE (K)               (${days} ${year_rtdiag_start}-${year})
NEWNAM      GTT
C*XSAVE       SKIN TEMPERATURE OPEN WATER TILE (K)         (${days} ${year_rtdiag_start}-${year})
NEWNAM      GTT
C*XSAVE       SKIN TEMPERATURE SEA ICE TILE (K)            (${days} ${year_rtdiag_start}-${year})
NEWNAM      GTT
C*XSAVE       SKIN TEMPERATURE OCEAN TILES (K)             (${days} ${year_rtdiag_start}-${year})
NEWNAM      GTT" >> ic.xsave

# ------------------------------------ monthly PCP

        x="PCP"
        timavg gs${x}${m} tgs${x}${m}
#                                      convert kg/m2/s or mm/s to mm/day by multiplying with 60x60x24=86400
        echo "C*XLIN        86400.       0.0" | ccc xlin tgs${x}${m} tgs${x}${m}.1 ; mv tgs${x}${m}.1 tgs${x}${m}
#                                      global
        globavg tgs${x}${m} _ gtgs${x}${m}g
#                                      dry land
        globavg tgs${x}${m} _ gtgs${x}${m}l dland_frac
#                                      land only
        globavg tgs${x}${m} _ gtgs${x}${m}ll lland_frac
#                                      ocean
        globavg tgs${x}${m} _ gtgs${x}${m}o ocean_frac
#                                      resolved lakes
        globavg tgs${x}${m} _ gtgs${x}${m}r rlake_frac
#                                      glaciers
        globavg tgs${x}${m} _ gtgs${x}${m}i glaci_frac
#                                      bedrock
        globavg tgs${x}${m} _ gtgs${x}${m}b bedro_frac
#                                      land excluding glaciers
        globavg tgs${x}${m} _ gtgs${x}${m}a nogla_frac
#                                      land excluding glaciers/bedrock
        globavg tgs${x}${m} _ gtgs${x}${m}d noglb_frac
#                                      land excluding Antarctica
        globavg tgs${x}${m} _ gtgs${x}${m}c noant_frac
#                                      NH extratropics
        globavg tgs${x}${m} _ gtgs${x}${m}n globe_frac_nhe
#                                      SH extratropics
        globavg tgs${x}${m} _ gtgs${x}${m}s globe_frac_she
#                                      Tropics
        globavg tgs${x}${m} _ gtgs${x}${m}t globe_frac_tro

        rtdlist="$rtdlist gtgs${x}${m}g gtgs${x}${m}l gtgs${x}${m}ll gtgs${x}${m}o gtgs${x}${m}r gtgs${x}${m}i gtgs${x}${m}b gtgs${x}${m}a gtgs${x}${m}d gtgs${x}${m}c gtgs${x}${m}n gtgs${x}${m}s gtgs${x}${m}t"
        echo "C*XFIND       PRECIPITATION GLOBAL (MM/DAY)                (${days} ${year_rtdiag_start}-${yearm1})
C*XFIND       PRECIPITATION LAND (MM/DAY)                  (${days} ${year_rtdiag_start}-${yearm1})
C*XFIND       PRECIPITATION LAND ONLY (MM/DAY)             (${days} ${year_rtdiag_start}-${yearm1})
C*XFIND       PRECIPITATION OCEAN (MM/DAY)                 (${days} ${year_rtdiag_start}-${yearm1})
C*XFIND       PRECIPITATION LAKES (MM/DAY)                 (${days} ${year_rtdiag_start}-${yearm1})
C*XFIND       PRECIPITATION GLACIERS (MM/DAY)              (${days} ${year_rtdiag_start}-${yearm1})
C*XFIND       PRECIPITATION BEDROCK (MM/DAY)               (${days} ${year_rtdiag_start}-${yearm1})
C*XFIND       PRECIPITATION LAND EXCL.GLAC.(MM/DAY)        (${days} ${year_rtdiag_start}-${yearm1})
C*XFIND       PRECIPITATION LAND EXCL.GLAC./BEDR.(MM/DAY)  (${days} ${year_rtdiag_start}-${yearm1})
C*XFIND       PRECIPITATION LAND EXCL.ANTARCTICA(MM/DAY)   (${days} ${year_rtdiag_start}-${yearm1})
C*XFIND       PRECIPITATION 30N-90N (MM/DAY)               (${days} ${year_rtdiag_start}-${yearm1})
C*XFIND       PRECIPITATION 30S-90S (MM/DAY)               (${days} ${year_rtdiag_start}-${yearm1})
C*XFIND       PRECIPITATION 30S-30N (MM/DAY)               (${days} ${year_rtdiag_start}-${yearm1})" >> ic.xfind
        echo "C*XSAVE       PRECIPITATION GLOBAL (MM/DAY)                (${days} ${year_rtdiag_start}-${year})
NEWNAM      PCP
C*XSAVE       PRECIPITATION LAND (MM/DAY)                  (${days} ${year_rtdiag_start}-${year})
NEWNAM      PCP
C*XSAVE       PRECIPITATION LAND ONLY (MM/DAY)             (${days} ${year_rtdiag_start}-${year})
NEWNAM      PCP
C*XSAVE       PRECIPITATION OCEAN (MM/DAY)                 (${days} ${year_rtdiag_start}-${year})
NEWNAM      PCP
C*XSAVE       PRECIPITATION LAKES (MM/DAY)                 (${days} ${year_rtdiag_start}-${year})
NEWNAM      PCP
C*XSAVE       PRECIPITATION GLACIERS (MM/DAY)              (${days} ${year_rtdiag_start}-${year})
NEWNAM      PCP
C*XSAVE       PRECIPITATION BEDROCK (MM/DAY)               (${days} ${year_rtdiag_start}-${year})
NEWNAM      PCP
C*XSAVE       PRECIPITATION LAND EXCL.GLAC.(MM/DAY)        (${days} ${year_rtdiag_start}-${year})
NEWNAM      PCP
C*XSAVE       PRECIPITATION LAND EXCL.GLAC./BEDR.(MM/DAY)  (${days} ${year_rtdiag_start}-${year})
NEWNAM      PCP
C*XSAVE       PRECIPITATION LAND EXCL.ANTARCTICA(MM/DAY)   (${days} ${year_rtdiag_start}-${year})
NEWNAM      PCP
C*XSAVE       PRECIPITATION 30N-90N (MM/DAY)               (${days} ${year_rtdiag_start}-${year})
NEWNAM      PCP
C*XSAVE       PRECIPITATION 30S-90S (MM/DAY)               (${days} ${year_rtdiag_start}-${year})
NEWNAM      PCP
C*XSAVE       PRECIPITATION 30S-30N (MM/DAY)               (${days} ${year_rtdiag_start}-${year})
NEWNAM      PCP" >> ic.xsave

# ------------------------------------ monthly PCP max

        x="PCP"
        timmax gs${x}${m} tgs${x}max${m}
#                                      convert kg/m2/s or mm/s to mm/day by multiplying with 60x60x24=86400
        echo "C*XLIN        86400.       0.0" | ccc xlin tgs${x}max${m} tgs${x}max${m}.1 ; mv tgs${x}max${m}.1 tgs${x}max${m}
#                                      global
        globavg tgs${x}max${m} _ gtgs${x}max${m}g
#                                      land
        globavg tgs${x}max${m} _ gtgs${x}max${m}l tland_frac
#                                      ocean
        globavg tgs${x}max${m} _ gtgs${x}max${m}o ocean_frac
#                                      NH extratropics
        globavg tgs${x}max${m} _ gtgs${x}max${m}n globe_frac_nhe
#                                      SH extratropics
        globavg tgs${x}max${m} _ gtgs${x}max${m}s globe_frac_she
#                                      Tropics
        globavg tgs${x}max${m} _ gtgs${x}max${m}t globe_frac_tro

        rtdlist="$rtdlist gtgs${x}max${m}g gtgs${x}max${m}l gtgs${x}max${m}o gtgs${x}max${m}n gtgs${x}max${m}s gtgs${x}max${m}t"
        echo "C*XFIND       MAX PRECIPITATION GLOBAL (MM/DAY)            (${days} ${year_rtdiag_start}-${yearm1})
C*XFIND       MAX PRECIPITATION LAND (MM/DAY)              (${days} ${year_rtdiag_start}-${yearm1})
C*XFIND       MAX PRECIPITATION OCEAN (MM/DAY)             (${days} ${year_rtdiag_start}-${yearm1})
C*XFIND       MAX PRECIPITATION 30N-90N (MM/DAY)           (${days} ${year_rtdiag_start}-${yearm1})
C*XFIND       MAX PRECIPITATION 30S-90S (MM/DAY)           (${days} ${year_rtdiag_start}-${yearm1})
C*XFIND       MAX PRECIPITATION 30S-30N (MM/DAY)           (${days} ${year_rtdiag_start}-${yearm1})" >> ic.xfind
        echo "C*XSAVE       MAX PRECIPITATION GLOBAL (MM/DAY)            (${days} ${year_rtdiag_start}-${year})
NEWNAM      PCP
C*XSAVE       MAX PRECIPITATION LAND (MM/DAY)              (${days} ${year_rtdiag_start}-${year})
NEWNAM      PCP
C*XSAVE       MAX PRECIPITATION OCEAN (MM/DAY)             (${days} ${year_rtdiag_start}-${year})
NEWNAM      PCP
C*XSAVE       MAX PRECIPITATION 30N-90N (MM/DAY)           (${days} ${year_rtdiag_start}-${year})
NEWNAM      PCP
C*XSAVE       MAX PRECIPITATION 30S-90S (MM/DAY)           (${days} ${year_rtdiag_start}-${year})
NEWNAM      PCP
C*XSAVE       MAX PRECIPITATION 30S-30N (MM/DAY)           (${days} ${year_rtdiag_start}-${year})
NEWNAM      PCP" >> ic.xsave

# ------------------------------------ monthly PCPC

        x="PCPC"
        timavg gs${x}${m} tgs${x}${m}
#                                      convert kg/m2/s or mm/s to mm/day by multiplying with 60x60x24=86400
        echo "C*XLIN        86400.       0.0" | ccc xlin tgs${x}${m} tgs${x}${m}.1 ; mv tgs${x}${m}.1 tgs${x}${m}
#                                      global
        globavg tgs${x}${m} _ gtgs${x}${m}g
#                                      land
        globavg tgs${x}${m} _ gtgs${x}${m}l tland_frac
#                                      ocean
        globavg tgs${x}${m} _ gtgs${x}${m}o ocean_frac
#                                      NH extratropics
        globavg tgs${x}${m} _ gtgs${x}${m}n globe_frac_nhe
#                                      SH extratropics
        globavg tgs${x}${m} _ gtgs${x}${m}s globe_frac_she
#                                      Tropics
        globavg tgs${x}${m} _ gtgs${x}${m}t globe_frac_tro

        rtdlist="$rtdlist gtgs${x}${m}g gtgs${x}${m}l gtgs${x}${m}o gtgs${x}${m}n gtgs${x}${m}s gtgs${x}${m}t"
        echo "C*XFIND       CONV. PRECIPITATION GLOBAL (MM/DAY)          (${days} ${year_rtdiag_start}-${yearm1})
C*XFIND       CONV. PRECIPITATION LAND (MM/DAY)            (${days} ${year_rtdiag_start}-${yearm1})
C*XFIND       CONV. PRECIPITATION OCEAN (MM/DAY)           (${days} ${year_rtdiag_start}-${yearm1})
C*XFIND       CONV. PRECIPITATION 30N-90N (MM/DAY)         (${days} ${year_rtdiag_start}-${yearm1})
C*XFIND       CONV. PRECIPITATION 30S-90S (MM/DAY)         (${days} ${year_rtdiag_start}-${yearm1})
C*XFIND       CONV. PRECIPITATION 30S-30N (MM/DAY)         (${days} ${year_rtdiag_start}-${yearm1})" >> ic.xfind
        echo "C*XSAVE       CONV. PRECIPITATION GLOBAL (MM/DAY)          (${days} ${year_rtdiag_start}-${year})
NEWNAM     PCPC
C*XSAVE       CONV. PRECIPITATION LAND (MM/DAY)            (${days} ${year_rtdiag_start}-${year})
NEWNAM     PCPC
C*XSAVE       CONV. PRECIPITATION OCEAN (MM/DAY)           (${days} ${year_rtdiag_start}-${year})
NEWNAM     PCPC
C*XSAVE       CONV. PRECIPITATION 30N-90N (MM/DAY)         (${days} ${year_rtdiag_start}-${year})
NEWNAM     PCPC
C*XSAVE       CONV. PRECIPITATION 30S-90S (MM/DAY)         (${days} ${year_rtdiag_start}-${year})
NEWNAM     PCPC
C*XSAVE       CONV. PRECIPITATION 30S-30N (MM/DAY)         (${days} ${year_rtdiag_start}-${year})
NEWNAM     PCPC" >> ic.xsave

# ------------------------------------ monthly snowfall PCPN

        x="PCPN"
        timavg gs${x}${m} tgs${x}${m}
#                                      convert kg/m2/s or mm/s to mm/day by multiplying with 60x60x24=86400
        echo "C*XLIN        86400.       0.0" | ccc xlin tgs${x}${m} tgs${x}${m}.1 ; mv tgs${x}${m}.1 tgs${x}${m}
#                                      global
        globavg tgs${x}${m} _ gtgs${x}${m}g
#                                      land
        globavg tgs${x}${m} _ gtgs${x}${m}l tland_frac
#                                      ocean
        globavg tgs${x}${m} _ gtgs${x}${m}o ocean_frac
#                                      resolved lakes
        globavg tgs${x}${m} _ gtgs${x}${m}r rlake_frac
#                                      glaciers
        globavg tgs${x}${m} _ gtgs${x}${m}i glaci_frac
#                                      bedrock
        globavg tgs${x}${m} _ gtgs${x}${m}b bedro_frac
#                                      land excluding glaciers
        globavg tgs${x}${m} _ gtgs${x}${m}a nogla_frac
#                                      NH extratropics
        globavg tgs${x}${m} _ gtgs${x}${m}n globe_frac_nhe
#                                      SH extratropics
        globavg tgs${x}${m} _ gtgs${x}${m}s globe_frac_she
#                                      Tropics
        globavg tgs${x}${m} _ gtgs${x}${m}t globe_frac_tro

        rtdlist="$rtdlist gtgs${x}${m}g gtgs${x}${m}l gtgs${x}${m}o gtgs${x}${m}r gtgs${x}${m}i gtgs${x}${m}b gtgs${x}${m}a gtgs${x}${m}n gtgs${x}${m}s gtgs${x}${m}t"
        echo "C*XFIND       SNOWFALL RATE GLOBAL (MM/DAY)                (${days} ${year_rtdiag_start}-${yearm1})
C*XFIND       SNOWFALL RATE LAND (MM/DAY)                  (${days} ${year_rtdiag_start}-${yearm1})
C*XFIND       SNOWFALL RATE OCEAN (MM/DAY)                 (${days} ${year_rtdiag_start}-${yearm1})
C*XFIND       SNOWFALL RATE LAKES (MM/DAY)                 (${days} ${year_rtdiag_start}-${yearm1})
C*XFIND       SNOWFALL RATE GLACIERS (MM/DAY)              (${days} ${year_rtdiag_start}-${yearm1})
C*XFIND       SNOWFALL RATE BEDROCK (MM/DAY)               (${days} ${year_rtdiag_start}-${yearm1})
C*XFIND       SNOWFALL RATE LAND EXCL.GLAC.(MM/DAY)        (${days} ${year_rtdiag_start}-${yearm1})
C*XFIND       SNOWFALL RATE 30N-90N (MM/DAY)               (${days} ${year_rtdiag_start}-${yearm1})
C*XFIND       SNOWFALL RATE 30S-90S (MM/DAY)               (${days} ${year_rtdiag_start}-${yearm1})
C*XFIND       SNOWFALL RATE 30S-30N (MM/DAY)               (${days} ${year_rtdiag_start}-${yearm1})" >> ic.xfind
        echo "C*XSAVE       SNOWFALL RATE GLOBAL (MM/DAY)                (${days} ${year_rtdiag_start}-${year})
NEWNAM     PCPN
C*XSAVE       SNOWFALL RATE LAND (MM/DAY)                  (${days} ${year_rtdiag_start}-${year})
NEWNAM     PCPN
C*XSAVE       SNOWFALL RATE OCEAN (MM/DAY)                 (${days} ${year_rtdiag_start}-${year})
NEWNAM     PCPN
C*XSAVE       SNOWFALL RATE LAKES (MM/DAY)                 (${days} ${year_rtdiag_start}-${year})
NEWNAM     PCPN
C*XSAVE       SNOWFALL RATE GLACIERS (MM/DAY)              (${days} ${year_rtdiag_start}-${year})
NEWNAM     PCPN
C*XSAVE       SNOWFALL RATE BEDROCK (MM/DAY)               (${days} ${year_rtdiag_start}-${year})
NEWNAM     PCPN
C*XSAVE       SNOWFALL RATE LAND EXCL.GLAC.(MM/DAY)        (${days} ${year_rtdiag_start}-${year})
NEWNAM     PCPN
C*XSAVE       SNOWFALL RATE 30N-90N (MM/DAY)               (${days} ${year_rtdiag_start}-${year})
NEWNAM     PCPN
C*XSAVE       SNOWFALL RATE 30S-90S (MM/DAY)               (${days} ${year_rtdiag_start}-${year})
NEWNAM     PCPN
C*XSAVE       SNOWFALL RATE 30S-30N (MM/DAY)               (${days} ${year_rtdiag_start}-${year})
NEWNAM     PCPN" >> ic.xsave

# ------------------------------------ monthly snowfall PCPN over tiles

        x="PCPN"
        y="FARE"
#                                      spread over grid cell
        mlt gs${x}${m} gs${y}l${m} gs${x}lg${m}
        mlt gs${x}${m} gs${y}w${m} gs${x}wg${m}
        mlt gs${x}${m} gs${y}i${m} gs${x}ig${m}
#                                      global mean
        globavg gs${x}lg${m} _ ggs${x}lg${m}
        globavg gs${x}wg${m} _ ggs${x}wg${m}
        globavg gs${x}ig${m} _ ggs${x}ig${m}
#                                      time average
        timavg ggs${x}lg${m} tggs${x}lg${m}
        timavg ggs${x}wg${m} tggs${x}wg${m}
        timavg ggs${x}ig${m} tggs${x}ig${m}
	add tggs${x}wg${m} tggs${x}ig${m} tggs${x}og${m}
	add tggs${x}lg${m} tggs${x}og${m} tggs${x}gg${m}

        rtdlist="$rtdlist tggs${x}lg${m} tggs${x}wg${m} tggs${x}ig${m} tggs${x}og${m} tggs${x}gg${m}"
        echo "C*XFIND       GLOBAL SNOWFALL RATE LAND (KG/M2/S)          (${days} ${year_rtdiag_start}-${yearm1})
C*XFIND       GLOBAL SNOWFALL RATE WATER (KG/M2/S)         (${days} ${year_rtdiag_start}-${yearm1})
C*XFIND       GLOBAL SNOWFALL RATE SEAICE (KG/M2/S)        (${days} ${year_rtdiag_start}-${yearm1})
C*XFIND       GLOBAL SNOWFALL RATE OCEAN (KG/M2/S)         (${days} ${year_rtdiag_start}-${yearm1})
C*XFIND       GLOBAL SNOWFALL RATE GLOBE (KG/M2/S)         (${days} ${year_rtdiag_start}-${yearm1})" >> ic.xfind
        echo "C*XSAVE       GLOBAL SNOWFALL RATE LAND (KG/M2/S)          (${days} ${year_rtdiag_start}-${year})
NEWNAM     PCPN
C*XSAVE       GLOBAL SNOWFALL RATE WATER (KG/M2/S)         (${days} ${year_rtdiag_start}-${year})
NEWNAM     PCPN
C*XSAVE       GLOBAL SNOWFALL RATE SEAICE (KG/M2/S)        (${days} ${year_rtdiag_start}-${year})
NEWNAM     PCPN
C*XSAVE       GLOBAL SNOWFALL RATE OCEAN (KG/M2/S)         (${days} ${year_rtdiag_start}-${year})
NEWNAM     PCPN
C*XSAVE       GLOBAL SNOWFALL RATE GLOBE (KG/M2/S)         (${days} ${year_rtdiag_start}-${year})
NEWNAM     PCPN" >> ic.xsave

# -----------------------------------  derive evaporation over land/ocean QFSL/QFSO if it does not exist

        if [ ! -s gsQFSL${m} -a -s gsPCP${m} -a -s gsQFS${m} -a -s gsBWGO${m} -a -s gsBWGI${m} ] ; then
          sub gsPCP${m}   gsBWGO${m}   gsQFSO${m}
          sub gsPCP${m}   gsBWGI${m}   gsQFSI${m}
          mlt gsQFSO${m}  gsFAREo${m}  gsQFSOg${m}
          mlt gsQFSI${m}  gsFAREi${m}  gsQFSIg${m}
          add gsQFSOg${m} gsQFSIg${m}  gsQFSOIg${m}
          sub gsQFS${m}   gsQFSOIg${m} gsQFSLg${m}
          gdiv gsQFSLg${m}  tland_frac  gsQFSL${m}
          gdiv gsQFSOIg${m} ocean_frac  gsQFSO${m}
        fi
        if [ ! -s gsQFSO${m} ] ; then
          gmlt gsQFSL${m}  tland_frac  gsQFSLg${m}
          sub  gsQFS${m}   gsQFSLg${m} gsQFSOg${m}
          gdiv gsQFSOg${m} ocean_frac  gsQFSO${m}
        fi

# ------------------------------------ monthly evaporation QFS

        x="QFS"
        timavg gs${x}${m}  tgs${x}${m}
        timavg gs${x}L${m} tgs${x}L${m}
        timavg gs${x}O${m} tgs${x}O${m}
#                                      convert kg/m2/s or mm/s to mm/day by multiplying with 60x60x24=86400
        echo "C*XLIN        86400.       0.0" | ccc xlin tgs${x}${m}  tgs${x}${m}.1  ; mv tgs${x}${m}.1  tgs${x}${m}
        echo "C*XLIN        86400.       0.0" | ccc xlin tgs${x}L${m} tgs${x}L${m}.1 ; mv tgs${x}L${m}.1 tgs${x}L${m}
        echo "C*XLIN        86400.       0.0" | ccc xlin tgs${x}O${m} tgs${x}O${m}.1 ; mv tgs${x}O${m}.1 tgs${x}O${m}
#                                      global
        globavg tgs${x}${m}  _ gtgs${x}${m}g
#                                      dry land
        globavg tgs${x}L${m} _ gtgs${x}${m}l dland_frac
#                                      land only
        globavg tgs${x}L${m} _ gtgs${x}${m}ll lland_frac
#                                      resolved lakes
        globavg tgs${x}L${m} _ gtgs${x}${m}r rlake_frac
#                                      glaciers
        globavg tgs${x}L${m} _ gtgs${x}${m}i glaci_frac
#                                      bedrock
        globavg tgs${x}L${m} _ gtgs${x}${m}b bedro_frac
#                                      land excluding glaciers
        globavg tgs${x}L${m} _ gtgs${x}${m}n nogla_frac
#                                      land excluding glaciers/bedrock
        globavg tgs${x}L${m} _ gtgs${x}${m}d noglb_frac
#                                      ocean
        globavg tgs${x}O${m} _ gtgs${x}${m}o ocean_frac

        rtdlist="$rtdlist gtgs${x}${m}g gtgs${x}${m}l gtgs${x}${m}ll gtgs${x}${m}r gtgs${x}${m}i gtgs${x}${m}b gtgs${x}${m}n gtgs${x}${m}d gtgs${x}${m}o"
        echo "C*XFIND       EVAPORATION GLOBAL (MM/DAY)                  (${days} ${year_rtdiag_start}-${yearm1})
C*XFIND       EVAPORATION LAND (MM/DAY)                    (${days} ${year_rtdiag_start}-${yearm1})
C*XFIND       EVAPORATION LAND ONLY (MM/DAY)               (${days} ${year_rtdiag_start}-${yearm1})
C*XFIND       EVAPORATION LAKES (MM/DAY)                   (${days} ${year_rtdiag_start}-${yearm1})
C*XFIND       EVAPORATION GLACIERS (MM/DAY)                (${days} ${year_rtdiag_start}-${yearm1})
C*XFIND       EVAPORATION BEDROCK (MM/DAY)                 (${days} ${year_rtdiag_start}-${yearm1})
C*XFIND       EVAPORATION LAND EXCL.GLAC.(MM/DAY)          (${days} ${year_rtdiag_start}-${yearm1})
C*XFIND       EVAPORATION LAND EXCL.GLAC./BEDR.(MM/DAY)    (${days} ${year_rtdiag_start}-${yearm1})
C*XFIND       EVAPORATION OCEAN (MM/DAY)                   (${days} ${year_rtdiag_start}-${yearm1})" >> ic.xfind
        echo "C*XSAVE       EVAPORATION GLOBAL (MM/DAY)                  (${days} ${year_rtdiag_start}-${year})
NEWNAM      QFS
C*XSAVE       EVAPORATION LAND (MM/DAY)                    (${days} ${year_rtdiag_start}-${year})
NEWNAM      QFS
C*XSAVE       EVAPORATION LAND ONLY (MM/DAY)               (${days} ${year_rtdiag_start}-${year})
NEWNAM      QFS
C*XSAVE       EVAPORATION LAKES (MM/DAY)                   (${days} ${year_rtdiag_start}-${year})
NEWNAM      QFS
C*XSAVE       EVAPORATION GLACIERS (MM/DAY)                (${days} ${year_rtdiag_start}-${year})
NEWNAM      QFS
C*XSAVE       EVAPORATION BEDROCK (MM/DAY)                 (${days} ${year_rtdiag_start}-${year})
NEWNAM      QFS
C*XSAVE       EVAPORATION LAND EXCL.GLAC.(MM/DAY)          (${days} ${year_rtdiag_start}-${year})
NEWNAM      QFS
C*XSAVE       EVAPORATION LAND EXCL.GLAC./BEDR.(MM/DAY)    (${days} ${year_rtdiag_start}-${year})
NEWNAM      QFS
C*XSAVE       EVAPORATION OCEAN (MM/DAY)                   (${days} ${year_rtdiag_start}-${year})
NEWNAM      QFS" >> ic.xsave

# ------------------------------------ monthly ROF

        x="ROF"
        timavg gs${x}${m} tgs${x}${m}
#                                      convert kg/m2/s or mm/s to mm/day by multiplying with 60x60x24=86400
        echo "C*XLIN        86400.       0.0" | ccc xlin tgs${x}${m} tgs${x}${m}.1 ; mv tgs${x}${m}.1 tgs${x}${m}
#                                      land
        globavg tgs${x}${m} _ gtgs${x}${m}l tland_frac
#                                      land only
        globavg tgs${x}${m} _ gtgs${x}${m}ll lland_frac
#                                      glaciers
        globavg tgs${x}${m} _ gtgs${x}${m}i glaci_frac
#                                      bedrock
        globavg tgs${x}${m} _ gtgs${x}${m}b bedro_frac
#                                      land excluding glaciers
        globavg tgs${x}${m} _ gtgs${x}${m}n nogla_frac
#                                      land excluding glaciers/bedrock
        globavg tgs${x}${m} _ gtgs${x}${m}d noglb_frac

        rtdlist="$rtdlist gtgs${x}${m}l gtgs${x}${m}ll gtgs${x}${m}i gtgs${x}${m}b gtgs${x}${m}n gtgs${x}${m}d"
        echo "C*XFIND       RUNOFF LAND (MM/DAY)                         (${days} ${year_rtdiag_start}-${yearm1})
C*XFIND       RUNOFF LAND ONLY (MM/DAY)                    (${days} ${year_rtdiag_start}-${yearm1})
C*XFIND       RUNOFF GLACIERS (MM/DAY)                     (${days} ${year_rtdiag_start}-${yearm1})
C*XFIND       RUNOFF BEDROCK (MM/DAY)                      (${days} ${year_rtdiag_start}-${yearm1})
C*XFIND       RUNOFF LAND EXCL.GLAC.(MM/DAY)               (${days} ${year_rtdiag_start}-${yearm1})
C*XFIND       RUNOFF LAND EXCL.GLAC./BEDR.(MM/DAY)         (${days} ${year_rtdiag_start}-${yearm1})" >> ic.xfind
        echo "C*XSAVE       RUNOFF LAND (MM/DAY)                         (${days} ${year_rtdiag_start}-${year})
NEWNAM      ROF
C*XSAVE       RUNOFF LAND ONLY (MM/DAY)                    (${days} ${year_rtdiag_start}-${year})
NEWNAM      ROF
C*XSAVE       RUNOFF GLACIERS(MM/DAY)                      (${days} ${year_rtdiag_start}-${year})
NEWNAM      ROF
C*XSAVE       RUNOFF BEDROCK(MM/DAY)                       (${days} ${year_rtdiag_start}-${year})
NEWNAM      ROF
C*XSAVE       RUNOFF LAND EXCL.GLAC.(MM/DAY)               (${days} ${year_rtdiag_start}-${year})
NEWNAM      ROF
C*XSAVE       RUNOFF LAND EXCL.GLAC./BEDR.(MM/DAY)         (${days} ${year_rtdiag_start}-${year})
NEWNAM      ROF" >> ic.xsave

# ------------------------------------ monthly RIVO

        x="RIVO"
        if [ -s gs${x}${m} ] ; then
        timavg gs${x}${m} tgs${x}${m}
#                                      ocean
        globavg tgs${x}${m} _ gtgs${x}${m}o ocean_frac
#                                      convert kg/m2/s or mm/s to mm/day by multiplying with 60x60x24=86400
        echo "C*XLIN        86400.       0.0" | ccc xlin gtgs${x}${m}o gtgs${x}${m}o.1 ; mv gtgs${x}${m}o.1 gtgs${x}${m}o

        rtdlist="$rtdlist gtgs${x}${m}o"
        echo "C*XFIND       RIVER DISCHARGE (MM/DAY)                     (${days} ${year_rtdiag_start}-${yearm1})" >> ic.xfind
        echo "C*XSAVE       RIVER DISCHARGE (MM/DAY)                     (${days} ${year_rtdiag_start}-${year})
NEWNAM     RIVO" >> ic.xsave
        fi

# ------------------------------------ monthly (P-E+R) ocean

        x1="PCP"
        x2="QFS"
        x3="RIVO"
        x="PMER"
        if [ -s gs${x1}${m} -a -s gs${x2}${m} -a -s gs${x3}${m} ] ; then
        sub gtgs${x1}${m}o gtgs${x2}${m}o gtgsPME${m}o  # P-E over ocean
        add gtgsPME${m}o   gtgs${x3}${m}o gtgs${x}${m}o # P-E+R over ocean

        rtdlist="$rtdlist gtgs${x}${m}o"
        echo "C*XFIND       OCEAN P-E PLUS RUNOFF (MM/DAY)               (${days} ${year_rtdiag_start}-${yearm1})" >> ic.xfind
        echo "C*XSAVE       OCEAN P-E PLUS RUNOFF (MM/DAY)               (${days} ${year_rtdiag_start}-${year})
NEWNAM     PMER" >> ic.xsave
        fi

# ------------------------------------ monthly ocean ice mass (SIC)

        x="SIC"
        timavg gs${x}${m} tgs${x}${m}
#                                      water
        globavg tgs${x}${m} _ gtavg water_frac
#                                      convert kg/m2 to kg
        gmlt gtavg water_area gtgs${x}${m}w
#                                      ocean
        globavg tgs${x}${m} _ gtavg ocean_frac
#                                      convert kg/m2 to kg
        gmlt gtavg ocean_area gtgs${x}${m}o
#                                      lakes
        globavg tgs${x}${m} _ gtavg rlake_frac
#                                      convert kg/m2 to kg
        gmlt gtavg rlake_area gtgs${x}${m}r

        rtdlist="$rtdlist gtgs${x}${m}w gtgs${x}${m}o gtgs${x}${m}r"
        echo "C*XFIND       TOTAL SEAICE MASS (KG)                       (${days} ${year_rtdiag_start}-${yearm1})
C*XFIND       OCEAN SEAICE MASS (KG)                       (${days} ${year_rtdiag_start}-${yearm1})
C*XFIND       LAKES SEAICE MASS (KG)                       (${days} ${year_rtdiag_start}-${yearm1})" >> ic.xfind
        echo "C*XSAVE       TOTAL SEAICE MASS (KG)                       (${days} ${year_rtdiag_start}-${year})
NEWNAM      SIC
C*XSAVE       OCEAN SEAICE MASS (KG)                       (${days} ${year_rtdiag_start}-${year})
NEWNAM      SIC
C*XSAVE       LAKES SEAICE MASS (KG)                       (${days} ${year_rtdiag_start}-${year})
NEWNAM      SIC" >> ic.xsave

# ------------------------------------ monthly SIC volume in NH and SH

        x="SIC"
#        timavg gs${x}${m} tgs${x}${m}
# #                                      convert to thickness (m) by dividing by DENI=913(kg/m3) (from spwcon*.f)
#         echo "C*XLIN          913.      0.E0         1" | ccc xlin tgs${x}${m} tgs${x}h
#                                      convert to thickness (m) by dividing by 900(kg/m3) (in NEMO)
        echo "C*XLIN          900.      0.E0         1" | ccc xlin tgs${x}${m} tgs${x}h
#                                      ice thickness over ocean + lakes
        globavg tgs${x}h _ gtgs${x}wn water_frac_nh
        globavg tgs${x}h _ gtgs${x}ws water_frac_sh
#                                      convert to volume (m3)
        gmlt gtgs${x}wn water_area_nh gtgs${x}w${m}nv
        gmlt gtgs${x}ws water_area_sh gtgs${x}w${m}sv
        rm gtgs${x}wn gtgs${x}ws
#                                      ice volume over ocean
        globavg tgs${x}h _ gtgs${x}on ocean_frac_nh
        globavg tgs${x}h _ gtgs${x}os ocean_frac_sh
#                                      convert to volume (m3)
        gmlt gtgs${x}on ocean_area_nh gtgs${x}o${m}nv
        gmlt gtgs${x}os ocean_area_sh gtgs${x}o${m}sv
        rm gtgs${x}on gtgs${x}os
#                                      ice volume over lakes (both hemispheres)
        globavg tgs${x}h   _ gtgs${x}r rlake_frac
#                                      convert to volume (m3)
        gmlt gtgs${x}r rlake_area gtgs${x}r${m}v
        rm gtgs${x}r

        rtdlist="$rtdlist gtgs${x}w${m}nv gtgs${x}w${m}sv gtgs${x}o${m}nv gtgs${x}o${m}sv gtgs${x}r${m}v"
        echo "C*XFIND       TOTAL SEAICE VOLUME NH (M3)                  (${days} ${year_rtdiag_start}-${yearm1})
C*XFIND       TOTAL SEAICE VOLUME SH (M3)                  (${days} ${year_rtdiag_start}-${yearm1})
C*XFIND       OCEAN SEAICE VOLUME NH (M3)                  (${days} ${year_rtdiag_start}-${yearm1})
C*XFIND       OCEAN SEAICE VOLUME SH (M3)                  (${days} ${year_rtdiag_start}-${yearm1})
C*XFIND       LAKES SEAICE VOLUME (M3)                     (${days} ${year_rtdiag_start}-${yearm1})" >> ic.xfind
        echo "C*XSAVE       TOTAL SEAICE VOLUME NH (M3)                  (${days} ${year_rtdiag_start}-${year})
NEWNAM      SIC
C*XSAVE       TOTAL SEAICE VOLUME SH (M3)                  (${days} ${year_rtdiag_start}-${year})
NEWNAM      SIC
C*XSAVE       OCEAN SEAICE VOLUME NH (M3)                  (${days} ${year_rtdiag_start}-${year})
NEWNAM      SIC
C*XSAVE       OCEAN SEAICE VOLUME SH (M3)                  (${days} ${year_rtdiag_start}-${year})
NEWNAM      SIC
C*XSAVE       LAKES SEAICE VOLUME (M3)                     (${days} ${year_rtdiag_start}-${year})
NEWNAM      SIC" >> ic.xsave

# ------------------------------------ monthly maximum hemispheric SIC thickness (derived from monthly mean SIC depth)

#                                      maximum ice thickness (ocean + lakes)
        gmlt tgs${x}h water_frac_nh tgs${x}wn
        gmlt tgs${x}h water_frac_sh tgs${x}ws
        rmax tgs${x}wn maxwnh
        rmax tgs${x}ws maxwsh
        window maxwnh gtgs${x}${m}wnx input=ic.window
        window maxwsh gtgs${x}${m}wsx input=ic.window
        rm maxwnh maxwsh
#                                      maximum ice thickness (ocean only)
        gmlt tgs${x}h ocean_frac_nh tgs${x}on
        gmlt tgs${x}h ocean_frac_sh tgs${x}os
        rmax tgs${x}on maxon
        rmax tgs${x}os maxos
        window maxon gtgs${x}${m}onx input=ic.window
        window maxos gtgs${x}${m}osx input=ic.window
        rm maxon maxos
#                                      maximum ice thickness (lakes only)
        gmlt tgs${x}h rlake_frac tgs${x}r
        rmax tgs${x}r maxk
        window maxk gtgs${x}${m}rx input=ic.window
        rm maxk

        rtdlist="$rtdlist gtgs${x}${m}wnx gtgs${x}${m}wsx gtgs${x}${m}onx gtgs${x}${m}osx gtgs${x}${m}rx"
        echo "C*XFIND       MAX TOTAL SEAICE THICKNESS NH (M)            (${days} ${year_rtdiag_start}-${yearm1})
C*XFIND       MAX TOTAL SEAICE THICKNESS SH (M)            (${days} ${year_rtdiag_start}-${yearm1})
C*XFIND       MAX OCEAN SEAICE THICKNESS NH (M)            (${days} ${year_rtdiag_start}-${yearm1})
C*XFIND       MAX OCEAN SEAICE THICKNESS SH (M)            (${days} ${year_rtdiag_start}-${yearm1})
C*XFIND       MAX LAKES SEAICE THICKNESS (M)               (${days} ${year_rtdiag_start}-${yearm1})" >> ic.xfind
        echo "C*XSAVE       MAX TOTAL SEAICE THICKNESS NH (M)            (${days} ${year_rtdiag_start}-${year})
NEWNAM      SIC
C*XSAVE       MAX TOTAL SEAICE THICKNESS SH (M)            (${days} ${year_rtdiag_start}-${year})
NEWNAM      SIC
C*XSAVE       MAX OCEAN SEAICE THICKNESS NH (M)            (${days} ${year_rtdiag_start}-${year})
NEWNAM      SIC
C*XSAVE       MAX OCEAN SEAICE THICKNESS SH (M)            (${days} ${year_rtdiag_start}-${year})
NEWNAM      SIC
C*XSAVE       MAX LAKES SEAICE THICKNESS (M)               (${days} ${year_rtdiag_start}-${year})
NEWNAM      SIC" >> ic.xsave

# ------------------------------------ sea ice area in NH and SH

        x="SICN"

        timavg gs${x}${m} tgs${x}${m}

        globavg tgs${x}${m} _ gtgs${x}anw water_frac_nh
        globavg tgs${x}${m} _ gtgs${x}asw water_frac_sh
        globavg tgs${x}${m} _ gtgs${x}ano ocean_frac_nh
        globavg tgs${x}${m} _ gtgs${x}aso ocean_frac_sh
        globavg tgs${x}${m} _ gtgs${x}ak  rlake_frac
#                                      convert to total (hemisphere) area
        gmlt gtgs${x}anw water_area_nh gtgs${x}a${m}naw
        gmlt gtgs${x}asw water_area_sh gtgs${x}a${m}saw
        gmlt gtgs${x}ano ocean_area_nh gtgs${x}a${m}nao
        gmlt gtgs${x}aso ocean_area_sh gtgs${x}a${m}sao
        gmlt gtgs${x}ak  rlake_area    gtgs${x}a${m}ra

        rtdlist="$rtdlist gtgs${x}a${m}naw gtgs${x}a${m}saw gtgs${x}a${m}nao gtgs${x}a${m}sao gtgs${x}a${m}ra"
        echo "C*XFIND       TOTAL SEAICE AREA NH SICN>0 (M2)             (${days} ${year_rtdiag_start}-${yearm1})
C*XFIND       TOTAL SEAICE AREA SH SICN>0 (M2)             (${days} ${year_rtdiag_start}-${yearm1})
C*XFIND       OCEAN SEAICE AREA NH SICN>0 (M2)             (${days} ${year_rtdiag_start}-${yearm1})
C*XFIND       OCEAN SEAICE AREA SH SICN>0 (M2)             (${days} ${year_rtdiag_start}-${yearm1})
C*XFIND       LAKES SEAICE AREA SICN>0 (M2)                (${days} ${year_rtdiag_start}-${yearm1})" >> ic.xfind
        echo "C*XSAVE       TOTAL SEAICE AREA NH SICN>0 (M2)             (${days} ${year_rtdiag_start}-${year})
NEWNAM     SICN
C*XSAVE       TOTAL SEAICE AREA SH SICN>0 (M2)             (${days} ${year_rtdiag_start}-${year})
NEWNAM     SICN
C*XSAVE       OCEAN SEAICE AREA NH SICN>0 (M2)             (${days} ${year_rtdiag_start}-${year})
NEWNAM     SICN
C*XSAVE       OCEAN SEAICE AREA SH SICN>0 (M2)             (${days} ${year_rtdiag_start}-${year})
NEWNAM     SICN
C*XSAVE       LAKES SEAICE AREA SICN>0 (M2)                (${days} ${year_rtdiag_start}-${year})
NEWNAM     SICN" >> ic.xsave

# ------------------------------------ sea ice extent in NH and SH derived from monthly means SICN>0.15

        x="SICN"
        echo "FMASK             -1 NEXT   GT      0.15                   1                   1" | ccc fmask tgs${x}${m} tgs${x}_ext

        globavg tgs${x}_ext _ gtgs${x}nw water_frac_nh
        globavg tgs${x}_ext _ gtgs${x}sw water_frac_sh
        globavg tgs${x}_ext _ gtgs${x}no ocean_frac_nh
        globavg tgs${x}_ext _ gtgs${x}so ocean_frac_sh
        globavg tgs${x}_ext _ gtgs${x}r  rlake_frac
#                                      convert to total (hemisphere) area
        gmlt gtgs${x}nw water_area_nh gtgs${x}${m}naw
        gmlt gtgs${x}sw water_area_sh gtgs${x}${m}saw
        gmlt gtgs${x}no ocean_area_nh gtgs${x}${m}nao
        gmlt gtgs${x}so ocean_area_sh gtgs${x}${m}sao
        gmlt gtgs${x}r  rlake_area    gtgs${x}${m}ra

        rtdlist="$rtdlist gtgs${x}${m}naw gtgs${x}${m}saw gtgs${x}${m}nao gtgs${x}${m}sao gtgs${x}${m}ra"
        echo "C*XFIND       TOTAL SEAICE COVER NH SICN>15% (M2)          (${days} ${year_rtdiag_start}-${yearm1})
C*XFIND       TOTAL SEAICE COVER SH SICN>15% (M2)          (${days} ${year_rtdiag_start}-${yearm1})
C*XFIND       OCEAN SEAICE COVER NH SICN>15% (M2)          (${days} ${year_rtdiag_start}-${yearm1})
C*XFIND       OCEAN SEAICE COVER SH SICN>15% (M2)          (${days} ${year_rtdiag_start}-${yearm1})
C*XFIND       LAKES SEAICE COVER SICN>15% (M2)             (${days} ${year_rtdiag_start}-${yearm1})" >> ic.xfind
        echo "C*XSAVE       TOTAL SEAICE COVER NH SICN>15% (M2)          (${days} ${year_rtdiag_start}-${year})
NEWNAM     SICN
C*XSAVE       TOTAL SEAICE COVER SH SICN>15% (M2)          (${days} ${year_rtdiag_start}-${year})
NEWNAM     SICN
C*XSAVE       OCEAN SEAICE COVER NH SICN>15% (M2)          (${days} ${year_rtdiag_start}-${year})
NEWNAM     SICN
C*XSAVE       OCEAN SEAICE COVER SH SICN>15% (M2)          (${days} ${year_rtdiag_start}-${year})
NEWNAM     SICN
C*XSAVE       LAKES SEAICE COVER SICN>15% (M2)             (${days} ${year_rtdiag_start}-${year})
NEWNAM     SICN" >> ic.xsave

# ------------------------------------ sea ice extent in NH and SH from SIC>100kg/m2 (from monthly means)

        x="SIC"
        echo "FMASK             -1 NEXT   GT      100.                   1                   1" | ccc fmask tgs${x}${m} tgs${x}_sicn

        globavg tgs${x}_sicn _ gtgs${x}nw water_frac_nh
        globavg tgs${x}_sicn _ gtgs${x}sw water_frac_sh
        globavg tgs${x}_sicn _ gtgs${x}no ocean_frac_nh
        globavg tgs${x}_sicn _ gtgs${x}so ocean_frac_sh
        globavg tgs${x}_sicn _ gtgs${x}r  rlake_frac
#                                      convert to total (hemisphere) area
        gmlt gtgs${x}nw water_area_nh gtgs${x}${m}naw
        gmlt gtgs${x}sw water_area_sh gtgs${x}${m}saw
        gmlt gtgs${x}no ocean_area_nh gtgs${x}${m}nao
        gmlt gtgs${x}so ocean_area_sh gtgs${x}${m}sao
        gmlt gtgs${x}r  rlake_area    gtgs${x}${m}ra

        rtdlist="$rtdlist gtgs${x}${m}naw gtgs${x}${m}saw gtgs${x}${m}nao gtgs${x}${m}sao gtgs${x}${m}ra"
        echo "C*XFIND       TOTAL SEAICE COVER NH SIC>100 (M2)           (${days} ${year_rtdiag_start}-${yearm1})
C*XFIND       TOTAL SEAICE COVER SH SIC>100 (M2)           (${days} ${year_rtdiag_start}-${yearm1})
C*XFIND       OCEAN SEAICE COVER NH SIC>100 (M2)           (${days} ${year_rtdiag_start}-${yearm1})
C*XFIND       OCEAN SEAICE COVER SH SIC>100 (M2)           (${days} ${year_rtdiag_start}-${yearm1})
C*XFIND       LAKES SEAICE COVER SIC>100 (M2)              (${days} ${year_rtdiag_start}-${yearm1})" >> ic.xfind
        echo "C*XSAVE       TOTAL SEAICE COVER NH SIC>100 (M2)           (${days} ${year_rtdiag_start}-${year})
NEWNAM      SIC
C*XSAVE       TOTAL SEAICE COVER SH SIC>100 (M2)           (${days} ${year_rtdiag_start}-${year})
NEWNAM      SIC
C*XSAVE       OCEAN SEAICE COVER NH SIC>100 (M2)           (${days} ${year_rtdiag_start}-${year})
NEWNAM      SIC
C*XSAVE       OCEAN SEAICE COVER SH SIC>100 (M2)           (${days} ${year_rtdiag_start}-${year})
NEWNAM      SIC
C*XSAVE       LAKES SEAICE COVER SIC>100 (M2)              (${days} ${year_rtdiag_start}-${year})
NEWNAM      SIC" >> ic.xsave

# ------------------------------------ monthly radiation fluxes
#                                      compute BALT=radiation budget at top of atmosphere
#                                      compute BALX=radiation budget at top of model
#                                      compute BALP=AR5 definition of radiation budget at top of atmosphere
        if [ -s gsFSA${m} -a -s gsFSG${m} -a -s gsFLA${m} -a -s gsFLG${m} ] ; then
          # CanAM4.0
          add gsFSA${m}  gsFSG${m}  gsFSAG${m}
          add gsFLA${m}  gsFLG${m}  gsFLAG${m}
          add gsFSAG${m} gsFLAG${m} gsBALT${m}
        elif [ -s gsFSO${m} -a -s gsFSLO${m} -a -s gsFSR${m} -a -s gsOLR${m} -a -s gsFSAM${m} -a -s gsFLAM${m} ] ; then
          # CanAM4.1+
          sub gsFSO${m}  gsFSLO${m} gsFSI${m}
          sub gsFSI${m}  gsFSR${m}  gsFSAG${m}
          sub gsFSLO${m} gsOLR${m}  gsFLAG${m}
          add gsFSAG${m} gsFLAG${m} gsBALT${m}
#
          add gsFSAM${m} gsFLAM${m} gsFAM${m}
          sub gsBALT${m} gsFAM${m}  gsBALX${m}
#
          sub gsBALT${m} gsFLAM${m} gsBALP${m}
        fi

# ------------------------------------ monthly global soil temperature (TG)

        x="TG"
        timavg gs${x}${m} tgs${x}${m}
#                                      land
        globavg tgs${x}${m} _ gtgs${x}${m}l dland_frac
#                                      land excluding glaciers
        globavg tgs${x}${m} _ gtgs${x}${m}n nogla_frac

        rtdlist="$rtdlist gtgs${x}${m}l gtgs${x}${m}n"
        echo "C*XFIND       SOIL TEMPERATURE LAND (K)                    (${days} ${year_rtdiag_start}-${yearm1})
C*XFIND       SOIL TEMPERATURE EXCL.GLACIERS (K)           (${days} ${year_rtdiag_start}-${yearm1})" >> ic.xfind
        echo "C*XSAVE       SOIL TEMPERATURE LAND (K)                    (${days} ${year_rtdiag_start}-${year})
NEWNAM       TG
C*XSAVE       SOIL TEMPERATURE EXCL.GLACIERS (K)           (${days} ${year_rtdiag_start}-${year})
NEWNAM       TG" >> ic.xsave

# ------------------------------------ monthly canopy temperature (TV)

        x="TV"
        if [ -s gs${x}${m} ] ; then
#                                      masked time mean (only where TV>0)
        y="${x}msk"
        echo "C*FMASK           -1 NEXT   GT        0.                   1" | ccc fmask gs${x}${m} gs${y}${m} # 1=TV>0, 0=otherwise
        timavg gs${x}${m} tgs${x}${m}
        timavg gs${y}${m} tgs${y}${m}

#                                      masked spatial mean
        globavg tgs${x}${m} _ gtgs${x}${m}
        globavg tgs${y}${m} _ gtgs${y}${m}
        div gtgs${x}${m} gtgs${y}${m} gtgs${x}${m}.1 ; mv gtgs${x}${m}.1 gtgs${x}${m}

        rtdlist="$rtdlist gtgs${x}${m}"
        echo "C*XFIND       CANOPY TEMPERATURE LAND (K)                  (${days} ${year_rtdiag_start}-${yearm1})" >> ic.xfind
        echo "C*XSAVE       CANOPY TEMPERATURE LAND (K)                  (${days} ${year_rtdiag_start}-${year})
NEWNAM       TV" >> ic.xsave
        fi

# ------------------------------------ monthly liquid soil moisture (WGL)

        x="WGL"
        timavg gs${x}${m} tgs${x}${m}
#                                      land
        globavg tgs${x}${m} _ gtgs${x}${m}l tland_frac
#                                      land only
        globavg tgs${x}${m} _ gtgs${x}${m}ll lland_frac
#                                      land excluding glaciers
        globavg tgs${x}${m} _ gtgs${x}${m}n nogla_frac
#                                      land excluding glaciers/bedrock
        globavg tgs${x}${m} _ gtgs${x}${m}d noglb_frac

        rtdlist="$rtdlist gtgs${x}${m}l gtgs${x}${m}ll gtgs${x}${m}n gtgs${x}${m}d"
        echo "C*XFIND       LIQUID SOIL MOISTURE LAND (KG/M2)            (${days} ${year_rtdiag_start}-${yearm1})
C*XFIND       LIQUID SOIL MOISTURE LAND ONLY (KG/M2)       (${days} ${year_rtdiag_start}-${yearm1})
C*XFIND       LIQUID SOIL MOISTURE EXCL.GLAC.(KG/M2)       (${days} ${year_rtdiag_start}-${yearm1})
C*XFIND       LIQUID SOIL MOISTURE EXCL.GLAC./BEDR.(KG/M2) (${days} ${year_rtdiag_start}-${yearm1})" >> ic.xfind
        echo "C*XSAVE       LIQUID SOIL MOISTURE LAND (KG/M2)            (${days} ${year_rtdiag_start}-${year})
NEWNAM      WGL
C*XSAVE       LIQUID SOIL MOISTURE LAND ONLY (KG/M2)       (${days} ${year_rtdiag_start}-${year})
NEWNAM      WGL
C*XSAVE       LIQUID SOIL MOISTURE EXCL.GLAC.(KG/M2)       (${days} ${year_rtdiag_start}-${year})
NEWNAM      WGL
C*XSAVE       LIQUID SOIL MOISTURE EXCL.GLAC./BEDR.(KG/M2) (${days} ${year_rtdiag_start}-${year})
NEWNAM      WGL" >> ic.xsave

# ------------------------------------ monthly frozen soil moisture (WGF)

        x="WGF"
        timavg gs${x}${m} tgs${x}${m}
#                                      land
        globavg tgs${x}${m} _ gtgs${x}${m}l tland_frac
#                                      land only
        globavg tgs${x}${m} _ gtgs${x}${m}ll lland_frac
#                                      land excluding glaciers
        globavg tgs${x}${m} _ gtgs${x}${m}n nogla_frac
#                                      land excluding glaciers/bedrock
        globavg tgs${x}${m} _ gtgs${x}${m}d noglb_frac

        rtdlist="$rtdlist gtgs${x}${m}l gtgs${x}${m}ll gtgs${x}${m}n gtgs${x}${m}d"
        echo "C*XFIND       FROZEN SOIL MOISTURE LAND (KG/M2)            (${days} ${year_rtdiag_start}-${yearm1})
C*XFIND       FROZEN SOIL MOISTURE LAND ONLY (KG/M2)       (${days} ${year_rtdiag_start}-${yearm1})
C*XFIND       FROZEN SOIL MOISTURE EXCL.GLAC.(KG/M2)       (${days} ${year_rtdiag_start}-${yearm1})
C*XFIND       FROZEN SOIL MOISTURE EXCL.GLAC./BEDR.(KG/M2) (${days} ${year_rtdiag_start}-${yearm1})" >> ic.xfind
        echo "C*XSAVE       FROZEN SOIL MOISTURE LAND (KG/M2)            (${days} ${year_rtdiag_start}-${year})
NEWNAM      WGF
C*XSAVE       FROZEN SOIL MOISTURE LAND ONLY (KG/M2)       (${days} ${year_rtdiag_start}-${year})
NEWNAM      WGF
C*XSAVE       FROZEN SOIL MOISTURE EXCL.GLAC.(KG/M2)       (${days} ${year_rtdiag_start}-${year})
NEWNAM      WGF
C*XSAVE       FROZEN SOIL MOISTURE EXCL.GLAC./BEDR.(KG/M2) (${days} ${year_rtdiag_start}-${year})
NEWNAM      WGF" >> ic.xsave

# ------------------------------------ monthly total (liquid + frozen) soil moisture (WGF+WGL)

        x="WGLF"
        add tgsWGL${m} tgsWGF${m} tgs${x}${m}
#                                      land
        globavg tgs${x}${m} _ gtgs${x}${m}l tland_frac
#                                      land only
        globavg tgs${x}${m} _ gtgs${x}${m}ll lland_frac
#                                      land excluding glaciers
        globavg tgs${x}${m} _ gtgs${x}${m}n nogla_frac
#                                      land excluding glaciers/bedrock
        globavg tgs${x}${m} _ gtgs${x}${m}d noglb_frac

        rtdlist="$rtdlist gtgs${x}${m}l gtgs${x}${m}ll gtgs${x}${m}n gtgs${x}${m}d"
        echo "C*XFIND       TOTAL SOIL MOISTURE LAND (KG/M2)             (${days} ${year_rtdiag_start}-${yearm1})
C*XFIND       TOTAL SOIL MOISTURE LAND ONLY (KG/M2)        (${days} ${year_rtdiag_start}-${yearm1})
C*XFIND       TOTAL SOIL MOISTURE EXCL.GLAC.(KG/M2)        (${days} ${year_rtdiag_start}-${yearm1})
C*XFIND       TOTAL SOIL MOISTURE EXCL.GLAC./BEDR.(KG/M2)  (${days} ${year_rtdiag_start}-${yearm1})" >> ic.xfind
        echo "C*XSAVE       TOTAL SOIL MOISTURE LAND (KG/M2)             (${days} ${year_rtdiag_start}-${year})
NEWNAM     WGLF
C*XSAVE       TOTAL SOIL MOISTURE LAND ONLY (KG/M2)        (${days} ${year_rtdiag_start}-${year})
NEWNAM     WGLF
C*XSAVE       TOTAL SOIL MOISTURE EXCL.GLAC.(KG/M2)        (${days} ${year_rtdiag_start}-${year})
NEWNAM     WGLF
C*XSAVE       TOTAL SOIL MOISTURE EXCL.GLAC./BEDR.(KG/M2)  (${days} ${year_rtdiag_start}-${year})
NEWNAM     WGLF" >> ic.xsave

# ------------------------------------ instantaneous first time step total (liquid + frozen) soil moisture (WGF+WGL)

        x="WGL"
        echo "C* RCOPY           1         3" | ccc rcopy gs${x}${m} tgs${x}i${m}
        x="WGF"
        echo "C* RCOPY           1         3" | ccc rcopy gs${x}${m} tgs${x}i${m}
        x="WGLFi"
        add tgsWGLi${m} tgsWGFi${m} tgs${x}${m}
#                                      land
        globavg tgs${x}${m} _ gtgs${x}${m}l tland_frac
#                                      land only
        globavg tgs${x}${m} _ gtgs${x}${m}ll lland_frac
#                                      land excluding glaciers
        globavg tgs${x}${m} _ gtgs${x}${m}n nogla_frac
#                                      land excluding glaciers/bedrock
        globavg tgs${x}${m} _ gtgs${x}${m}d noglb_frac

        rtdlist="$rtdlist gtgs${x}${m}l gtgs${x}${m}ll gtgs${x}${m}n gtgs${x}${m}d"
        echo "C*XFIND       TOTAL SOIL MOISTURE FIRST LAND (KG/M2)       (${days} ${year_rtdiag_start}-${yearm1})
C*XFIND       TOTAL SOIL MOISTURE FIRST LAND ONLY (KG/M2)  (${days} ${year_rtdiag_start}-${yearm1})
C*XFIND       TOTAL SOIL MOISTURE FIRST EXCL.GLAC.(KG/M2)  (${days} ${year_rtdiag_start}-${yearm1})
C*XFIND       TOTAL SOIL MOISTURE FIRST NO GL./BR.(KG/M2)  (${days} ${year_rtdiag_start}-${yearm1})" >> ic.xfind
        echo "C*XSAVE       TOTAL SOIL MOISTURE FIRST LAND (KG/M2)       (${days} ${year_rtdiag_start}-${year})
NEWNAM     WGLF
C*XSAVE       TOTAL SOIL MOISTURE FIRST LAND ONLY (KG/M2)  (${days} ${year_rtdiag_start}-${year})
NEWNAM     WGLF
C*XSAVE       TOTAL SOIL MOISTURE FIRST EXCL.GLAC.(KG/M2)  (${days} ${year_rtdiag_start}-${year})
NEWNAM     WGLF
C*XSAVE       TOTAL SOIL MOISTURE FIRST NO GL./BR.(KG/M2)  (${days} ${year_rtdiag_start}-${year})
NEWNAM     WGLF" >> ic.xsave

# ------------------------------------ monthly liquid veg. moisture (WVL)

        x="WVL"
        timavg gs${x}${m} tgs${x}${m}
#                                      land
        globavg tgs${x}${m} _ gtgs${x}${m}l tland_frac
#                                      land only
        globavg tgs${x}${m} _ gtgs${x}${m}ll lland_frac
#                                      land excluding glaciers
        globavg tgs${x}${m} _ gtgs${x}${m}n nogla_frac
#                                      land excluding glaciers/bedrock
        globavg tgs${x}${m} _ gtgs${x}${m}d noglb_frac

        rtdlist="$rtdlist gtgs${x}${m}l gtgs${x}${m}ll gtgs${x}${m}n gtgs${x}${m}d"
        echo "C*XFIND       LIQUID VEG. MOISTURE LAND (KG/M2)            (${days} ${year_rtdiag_start}-${yearm1})
C*XFIND       LIQUID VEG. MOISTURE LAND ONLY (KG/M2)       (${days} ${year_rtdiag_start}-${yearm1})
C*XFIND       LIQUID VEG. MOISTURE EXCL.GLAC.(KG/M2)       (${days} ${year_rtdiag_start}-${yearm1})
C*XFIND       LIQUID VEG. MOISTURE EXCL.GLAC./BEDR.(KG/M2) (${days} ${year_rtdiag_start}-${yearm1})" >> ic.xfind
        echo "C*XSAVE       LIQUID VEG. MOISTURE LAND (KG/M2)            (${days} ${year_rtdiag_start}-${year})
NEWNAM      WVL
C*XSAVE       LIQUID VEG. MOISTURE LAND ONLY (KG/M2)       (${days} ${year_rtdiag_start}-${year})
NEWNAM      WVL
C*XSAVE       LIQUID VEG. MOISTURE EXCL.GLAC.(KG/M2)       (${days} ${year_rtdiag_start}-${year})
NEWNAM      WVL
C*XSAVE       LIQUID VEG. MOISTURE EXCL.GLAC./BEDR.(KG/M2) (${days} ${year_rtdiag_start}-${year})
NEWNAM      WVL" >> ic.xsave

# ------------------------------------ monthly frozen veg. moisture (WVF)

        x="WVF"
        timavg gs${x}${m} tgs${x}${m}
#                                      land
        globavg tgs${x}${m} _ gtgs${x}${m}l tland_frac
#                                      land only
        globavg tgs${x}${m} _ gtgs${x}${m}ll lland_frac
#                                      land excluding glaciers
        globavg tgs${x}${m} _ gtgs${x}${m}n nogla_frac
#                                      land excluding glaciers/bedrock
        globavg tgs${x}${m} _ gtgs${x}${m}d noglb_frac

        rtdlist="$rtdlist gtgs${x}${m}l gtgs${x}${m}ll gtgs${x}${m}n gtgs${x}${m}d"
        echo "C*XFIND       FROZEN VEG. MOISTURE LAND (KG/M2)            (${days} ${year_rtdiag_start}-${yearm1})
C*XFIND       FROZEN VEG. MOISTURE LAND ONLY (KG/M2)       (${days} ${year_rtdiag_start}-${yearm1})
C*XFIND       FROZEN VEG. MOISTURE EXCL.GLAC.(KG/M2)       (${days} ${year_rtdiag_start}-${yearm1})
C*XFIND       FROZEN VEG. MOISTURE EXCL.GLAC./BEDR.(KG/M2) (${days} ${year_rtdiag_start}-${yearm1})" >> ic.xfind
        echo "C*XSAVE       FROZEN VEG. MOISTURE LAND (KG/M2)            (${days} ${year_rtdiag_start}-${year})
NEWNAM      WVF
C*XSAVE       FROZEN VEG. MOISTURE LAND ONLY (KG/M2)       (${days} ${year_rtdiag_start}-${year})
NEWNAM      WVF
C*XSAVE       FROZEN VEG. MOISTURE EXCL.GLAC.(KG/M2)       (${days} ${year_rtdiag_start}-${year})
NEWNAM      WVF
C*XSAVE       FROZEN VEG. MOISTURE EXCL.GLAC./BEDR.(KG/M2) (${days} ${year_rtdiag_start}-${year})
NEWNAM      WVF" >> ic.xsave

# ------------------------------------ monthly total (liquid + frozen) veg. moisture (WVF+WVL)

        x="WVLF"
        add tgsWVL${m} tgsWVF${m} tgs${x}${m}
#                                      land
        globavg tgs${x}${m} _ gtgs${x}${m}l tland_frac
#                                      land only
        globavg tgs${x}${m} _ gtgs${x}${m}ll lland_frac
#                                      land excluding glaciers
        globavg tgs${x}${m} _ gtgs${x}${m}n nogla_frac
#                                      land excluding glaciers/bedrock
        globavg tgs${x}${m} _ gtgs${x}${m}d noglb_frac

        rtdlist="$rtdlist gtgs${x}${m}l gtgs${x}${m}ll gtgs${x}${m}n gtgs${x}${m}d"
        echo "C*XFIND       TOTAL VEG. MOISTURE LAND (KG/M2)             (${days} ${year_rtdiag_start}-${yearm1})
C*XFIND       TOTAL VEG. MOISTURE LAND ONLY (KG/M2)        (${days} ${year_rtdiag_start}-${yearm1})
C*XFIND       TOTAL VEG. MOISTURE EXCL.GLAC.(KG/M2)        (${days} ${year_rtdiag_start}-${yearm1})
C*XFIND       TOTAL VEG. MOISTURE EXCL.GLAC./BEDR.(KG/M2)  (${days} ${year_rtdiag_start}-${yearm1})" >> ic.xfind
        echo "C*XSAVE       TOTAL VEG. MOISTURE LAND (KG/M2)             (${days} ${year_rtdiag_start}-${year})
NEWNAM     WVLF
C*XSAVE       TOTAL VEG. MOISTURE LAND ONLY (KG/M2)        (${days} ${year_rtdiag_start}-${year})
NEWNAM     WVLF
C*XSAVE       TOTAL VEG. MOISTURE EXCL.GLAC.(KG/M2)        (${days} ${year_rtdiag_start}-${year})
NEWNAM     WVLF
C*XSAVE       TOTAL VEG. MOISTURE EXCL.GLAC./BEDR.(KG/M2)  (${days} ${year_rtdiag_start}-${year})
NEWNAM     WVLF" >> ic.xsave

# ------------------------------------ instantaneous first time step total (liquid + frozen) veg. moisture (WVF+WVL)

        x="WVL"
        echo "C* RCOPY           1         1" | ccc rcopy gs${x}${m} tgs${x}i${m}
        x="WVF"
        echo "C* RCOPY           1         1" | ccc rcopy gs${x}${m} tgs${x}i${m}
        x="WVLFi"
        add tgsWVLi${m} tgsWVFi${m} tgs${x}${m}
#                                      land
        globavg tgs${x}${m} _ gtgs${x}${m}l tland_frac
#                                      land only
        globavg tgs${x}${m} _ gtgs${x}${m}ll lland_frac
#                                      land excluding glaciers
        globavg tgs${x}${m} _ gtgs${x}${m}n nogla_frac
#                                      land excluding glaciers/bedrock
        globavg tgs${x}${m} _ gtgs${x}${m}d noglb_frac

        rtdlist="$rtdlist gtgs${x}${m}l gtgs${x}${m}ll gtgs${x}${m}n gtgs${x}${m}d"
        echo "C*XFIND       TOTAL VEG. MOISTURE FIRST LAND (KG/M2)       (${days} ${year_rtdiag_start}-${yearm1})
C*XFIND       TOTAL VEG. MOISTURE FIRST LAND ONLY (KG/M2)  (${days} ${year_rtdiag_start}-${yearm1})
C*XFIND       TOTAL VEG. MOISTURE FIRST EXCL.GLAC.(KG/M2)  (${days} ${year_rtdiag_start}-${yearm1})
C*XFIND       TOTAL VEG. MOISTURE FIRST NO GL./BR.(KG/M2)  (${days} ${year_rtdiag_start}-${yearm1})" >> ic.xfind
        echo "C*XSAVE       TOTAL VEG. MOISTURE FIRST LAND (KG/M2)       (${days} ${year_rtdiag_start}-${year})
NEWNAM     WVLF
C*XSAVE       TOTAL VEG. MOISTURE FIRST LAND ONLY (KG/M2)  (${days} ${year_rtdiag_start}-${year})
NEWNAM     WVLF
C*XSAVE       TOTAL VEG. MOISTURE FIRST EXCL.GLAC.(KG/M2)  (${days} ${year_rtdiag_start}-${year})
NEWNAM     WVLF
C*XSAVE       TOTAL VEG. MOISTURE FIRST NO GL./BR.(KG/M2)  (${days} ${year_rtdiag_start}-${year})
NEWNAM     WVLF" >> ic.xsave

# ------------------------------------ monthly volumetric fraction of WGL

        x="VFSL"
#                                      convert kg/m2 to m by dividing by density (1000 kg/m3)
        echo "C*XLIN         1000.       0.0         1" | ccc xlin tgsWGL${m} tgsWGL${m}d
        div tgsWGL${m}d gsDZG01 tgs${x}${m}
#                                      land
        globavg tgs${x}${m} _ gtgs${x}${m}l tland_frac
#                                      land excluding glaciers
        globavg tgs${x}${m} _ gtgs${x}${m}n nogla_frac

        rtdlist="$rtdlist gtgs${x}${m}l gtgs${x}${m}n"
        echo "C*XFIND       VOLUM.LIQUID SOIL MOIST.FRAC.LAND            (${days} ${year_rtdiag_start}-${yearm1})
C*XFIND       VOLUM.LIQUID SOIL MOIST.FRAC.EXCL.GLAC.      (${days} ${year_rtdiag_start}-${yearm1})" >> ic.xfind
        echo "C*XSAVE       VOLUM LIQUID SOIL MOIST.FRAC.LAND            (${days} ${year_rtdiag_start}-${year})
NEWNAM     VFSL
C*XSAVE       VOLUM.LIQUID SOIL MOIST.FRAC.EXCL.GLAC.      (${days} ${year_rtdiag_start}-${year})
NEWNAM     VFSL" >> ic.xsave

# ------------------------------------ monthly volumetric fraction of WGF

        x="VFSF"
#                                      convert kg/m2 to m by dividing by density (917 kg/m3)
        echo "C*XLIN          917.       0.0         1" | ccc xlin tgsWGF${m} tgsWGF${m}d
        div tgsWGF${m}d gsDZG01 tgs${x}${m}
#                                      land
        globavg tgs${x}${m} _ gtgs${x}${m}l tland_frac
#                                      land excluding glaciers
        globavg tgs${x}${m} _ gtgs${x}${m}n nogla_frac

        rtdlist="$rtdlist gtgs${x}${m}l gtgs${x}${m}n"
        echo "C*XFIND       VOLUM.FROZEN SOIL MOIST.FRAC.LAND            (${days} ${year_rtdiag_start}-${yearm1})
C*XFIND       VOLUM.FROZEN SOIL MOIST.FRAC.EXCL.GLAC.      (${days} ${year_rtdiag_start}-${yearm1})" >> ic.xfind
        echo "C*XSAVE       VOLUM.FROZEN SOIL MOIST.FRAC.LAND            (${days} ${year_rtdiag_start}-${year})
NEWNAM     VFSF
C*XSAVE       VOLUM.FROZEN SOIL MOIST.FRAC.EXCL.GLAC.      (${days} ${year_rtdiag_start}-${year})
NEWNAM     VFSF" >> ic.xsave

# ------------------------------------ monthly volumetric fraction of total soil moisture (VFSM)

        x="VFSM"
#                                      total moisture depth
        add tgsWGL${m}d tgsWGF${m}d tgsWGLF${m}d
#                                      multi-level average
        echo "C*BINS       -1" | ccc bins tgsWGLF${m}d tgsWGLF${m}davg
#                                      divide by averaged soil depth
        gdiv tgsWGLF${m}davg gsDZGavg tgs${x}${m}
#                                      land
        globavg tgs${x}${m} _ gtgs${x}${m}l tland_frac
#                                      land excluding glaciers
        globavg tgs${x}${m} _ gtgs${x}${m}n nogla_frac

        rtdlist="$rtdlist gtgs${x}${m}l gtgs${x}${m}n"
        echo "C*XFIND       VOLUM.TOTAL SOIL MOIST.FRAC.LAND             (${days} ${year_rtdiag_start}-${yearm1})
C*XFIND       VOLUM.TOTAL SOIL MOIST.FRAC.EXCL.GLAC.       (${days} ${year_rtdiag_start}-${yearm1})" >> ic.xfind
        echo "C*XSAVE       VOLUM.TOTAL SOIL MOIST.FRAC.LAND             (${days} ${year_rtdiag_start}-${year})
NEWNAM     VFSM
C*XSAVE       VOLUM.TOTAL SOIL MOIST.FRAC.EXCL.GLAC.       (${days} ${year_rtdiag_start}-${year})
NEWNAM     VFSM" >> ic.xsave

# ------------------------------------ monthly water in snow WSNO

        x="WSNO"
        timavg gs${x}${m} tgs${x}${m}
#                                      land
        globavg tgs${x}${m} _ gtgs${x}${m}l tland_frac
#                                      land only
        globavg tgs${x}${m} _ gtgs${x}${m}ll lland_frac
#                                      land excluding glaciers
        globavg tgs${x}${m} _ gtgs${x}${m}n nogla_frac
#                                      land excluding glaciers/bedrock
        globavg tgs${x}${m} _ gtgs${x}${m}d noglb_frac

        rtdlist="$rtdlist gtgs${x}${m}l gtgs${x}${m}ll gtgs${x}${m}n gtgs${x}${m}d"
        echo "C*XFIND       LIQ. WATER IN SNOW LAND (KG/M2)              (${days} ${year_rtdiag_start}-${yearm1})
C*XFIND       LIQ. WATER IN SNOW LAND ONLY (KG/M2)         (${days} ${year_rtdiag_start}-${yearm1})
C*XFIND       LIQ. WATER IN SNOW EXCL.GLAC.(KG/M2)         (${days} ${year_rtdiag_start}-${yearm1})
C*XFIND       LIQ. WATER IN SNOW EXCL.GLAC./BEDR.(KG/M2)   (${days} ${year_rtdiag_start}-${yearm1})" >> ic.xfind
        echo "C*XSAVE       LIQ. WATER IN SNOW LAND (KG/M2)              (${days} ${year_rtdiag_start}-${year})
NEWNAM     WSNO
C*XSAVE       LIQ. WATER IN SNOW LAND ONLY (KG/M2)         (${days} ${year_rtdiag_start}-${year})
NEWNAM     WSNO
C*XSAVE       LIQ. WATER IN SNOW EXCL.GLAC.(KG/M2)         (${days} ${year_rtdiag_start}-${year})
NEWNAM     WSNO
C*XSAVE       LIQ. WATER IN SNOW EXCL.GLAC./BEDR.(KG/M2)   (${days} ${year_rtdiag_start}-${year})
NEWNAM     WSNO" >> ic.xsave

# ------------------------------------ instantaneous first time step liq. water in snow WSNO

        x="WSNO"
        echo "C* RCOPY           1         1" | ccc rcopy gs${x}${m} tgs${x}i${m}
        x="WSNOi"
#                                      land
        globavg tgs${x}${m} _ gtgs${x}${m}l tland_frac
#                                      land only
        globavg tgs${x}${m} _ gtgs${x}${m}ll lland_frac
#                                      land excluding glaciers
        globavg tgs${x}${m} _ gtgs${x}${m}n nogla_frac
#                                      land excluding glaciers/bedrock
        globavg tgs${x}${m} _ gtgs${x}${m}d noglb_frac

        rtdlist="$rtdlist gtgs${x}${m}l gtgs${x}${m}ll gtgs${x}${m}n gtgs${x}${m}d"
        echo "C*XFIND       LIQ. WATER IN SNOW FIRST LAND (KG/M2)        (${days} ${year_rtdiag_start}-${yearm1})
C*XFIND       LIQ. WATER IN SNOW FIRST LAND ONLY (KG/M2)   (${days} ${year_rtdiag_start}-${yearm1})
C*XFIND       LIQ. WATER IN SNOW FIRST EXCL.GLAC.(KG/M2)   (${days} ${year_rtdiag_start}-${yearm1})
C*XFIND       LIQ. WATER IN SNOW FIRST NO GL./BR.(KG/M2)   (${days} ${year_rtdiag_start}-${yearm1})" >> ic.xfind
        echo "C*XSAVE       LIQ. WATER IN SNOW FIRST LAND (KG/M2)        (${days} ${year_rtdiag_start}-${year})
NEWNAM     WSNO
C*XSAVE       LIQ. WATER IN SNOW FIRST LAND ONLY (KG/M2)   (${days} ${year_rtdiag_start}-${year})
NEWNAM     WSNO
C*XSAVE       LIQ. WATER IN SNOW FIRST EXCL.GLAC.(KG/M2)   (${days} ${year_rtdiag_start}-${year})
NEWNAM     WSNO
C*XSAVE       LIQ. WATER IN SNOW FIRST NO GL./BR.(KG/M2)   (${days} ${year_rtdiag_start}-${year})
NEWNAM     WSNO" >> ic.xsave

# ------------------------------------ monthly ponding depth ZPND

        x="ZPND"
        timavg gs${x}${m} tgs${x}${m}
#                                      land
        globavg tgs${x}${m} _ gtgs${x}${m}l tland_frac
#                                      land only
        globavg tgs${x}${m} _ gtgs${x}${m}ll lland_frac
#                                      land excluding glaciers
        globavg tgs${x}${m} _ gtgs${x}${m}n nogla_frac
#                                      land excluding glaciers/bedrock
        globavg tgs${x}${m} _ gtgs${x}${m}d noglb_frac

        rtdlist="$rtdlist gtgs${x}${m}l gtgs${x}${m}ll gtgs${x}${m}n gtgs${x}${m}d"
        echo "C*XFIND       PONDING DEPTH LAND (M)                       (${days} ${year_rtdiag_start}-${yearm1})
C*XFIND       PONDING DEPTH LAND ONLY (M)                  (${days} ${year_rtdiag_start}-${yearm1})
C*XFIND       PONDING DEPTH EXCL.GLAC.(M)                  (${days} ${year_rtdiag_start}-${yearm1})
C*XFIND       PONDING DEPTH EXCL.GLAC./BEDR.(M)            (${days} ${year_rtdiag_start}-${yearm1})" >> ic.xfind
        echo "C*XSAVE       PONDING DEPTH LAND (M)                       (${days} ${year_rtdiag_start}-${year})
NEWNAM     ZPND
C*XSAVE       PONDING DEPTH LAND ONLY (M)                  (${days} ${year_rtdiag_start}-${year})
NEWNAM     ZPND
C*XSAVE       PONDING DEPTH EXCL.GLAC.(M)                  (${days} ${year_rtdiag_start}-${year})
NEWNAM     ZPND
C*XSAVE       PONDING DEPTH EXCL.GLAC./BEDR.(M)            (${days} ${year_rtdiag_start}-${year})
NEWNAM     ZPND" >> ic.xsave

# ------------------------------------ instantaneous first time step ponding depth ZPND

        x="ZPND"
        echo "C* RCOPY           1         1" | ccc rcopy gs${x}${m} tgs${x}i${m}
        x="ZPNDi"
#                                      land
        globavg tgs${x}${m} _ gtgs${x}${m}l tland_frac
#                                      land only
        globavg tgs${x}${m} _ gtgs${x}${m}ll lland_frac
#                                      land excluding glaciers
        globavg tgs${x}${m} _ gtgs${x}${m}n nogla_frac
#                                      land excluding glaciers/bedrock
        globavg tgs${x}${m} _ gtgs${x}${m}d noglb_frac

        rtdlist="$rtdlist gtgs${x}${m}l gtgs${x}${m}ll gtgs${x}${m}n gtgs${x}${m}d"
        echo "C*XFIND       PONDING DEPTH FIRST LAND (M)                 (${days} ${year_rtdiag_start}-${yearm1})
C*XFIND       PONDING DEPTH FIRST LAND ONLY (M)            (${days} ${year_rtdiag_start}-${yearm1})
C*XFIND       PONDING DEPTH FIRST EXCL.GLAC.(M)            (${days} ${year_rtdiag_start}-${yearm1})
C*XFIND       PONDING DEPTH FIRST NO GL./BR.(M)            (${days} ${year_rtdiag_start}-${yearm1})" >> ic.xfind
        echo "C*XSAVE       PONDING DEPTH FIRST LAND (M)                 (${days} ${year_rtdiag_start}-${year})
NEWNAM     ZPND
C*XSAVE       PONDING DEPTH FIRST LAND ONLY (M)            (${days} ${year_rtdiag_start}-${year})
NEWNAM     ZPND
C*XSAVE       PONDING DEPTH FIRST EXCL.GLAC.(M)            (${days} ${year_rtdiag_start}-${year})
NEWNAM     ZPND
C*XSAVE       PONDING DEPTH FIRST NO GL./BR.(M)            (${days} ${year_rtdiag_start}-${year})
NEWNAM     ZPND" >> ic.xsave

# ------------------------------------ monthly snow mass SNO global

        x="SNO"
        timavg gs${x}${m} tgs${x}${m}
#                                      global
        globavg tgs${x}${m} _ gtavg
#                                      convert kg/m2 to kg
        gmlt gtavg globe_area gtgs${x}${m}g

        rtdlist="$rtdlist gtgs${x}${m}g"
        echo "C*XFIND       SNOW MASS GLOBAL (KG)                        (${days} ${year_rtdiag_start}-${yearm1})" >> ic.xfind
        echo "C*XSAVE       SNOW MASS GLOBAL (KG)                        (${days} ${year_rtdiag_start}-${year})
NEWNAM      SNO" >> ic.xsave

# ------------------------------------ monthly snow mass SNO over land

        x="SNOT"
        echo "C*SELLEV      1$ilnd" | ccc sellev gs${x}${m} gs${x}l${m} # snow over land
        timavg gs${x}l${m} tgs${x}l${m}

#                                      land
        globavg tgs${x}l${m} _ gtavg dland_frac
#                                      convert kg/m2 to kg
        gmlt gtavg dland_area gtgs${x}${m}l

#                                      land only
        globavg tgs${x}l${m} _ gtavg lland_frac
#                                      convert kg/m2 to kg
        gmlt gtavg lland_area gtgs${x}${m}ll

#                                      land excluding glaciers
        globavg tgs${x}l${m} _ gtavg nogla_frac
#                                      convert kg/m2 to kg
        gmlt gtavg nogla_area gtgs${x}${m}n

#                                      land excluding glaciers/bedrock
        globavg tgs${x}l${m} _ gtavg noglb_frac
#                                      convert kg/m2 to kg
        gmlt gtavg nogla_area gtgs${x}${m}d

#                                      glaciers
        globavg tgs${x}l${m} _ gtavg glaci_frac
#                                      convert kg/m2 to kg
        gmlt gtavg glaci_area gtgs${x}${m}i

#                                      bedrock
        globavg tgs${x}l${m} _ gtavg bedro_frac
#                                      convert kg/m2 to kg
        gmlt gtavg bedro_area gtgs${x}${m}b

        rtdlist="$rtdlist gtgs${x}${m}l gtgs${x}${m}ll gtgs${x}${m}n gtgs${x}${m}d gtgs${x}${m}i gtgs${x}${m}b"
        echo "C*XFIND       SNOW MASS LAND (KG)                          (${days} ${year_rtdiag_start}-${yearm1})
C*XFIND       SNOW MASS LAND ONLY (KG)                     (${days} ${year_rtdiag_start}-${yearm1})
C*XFIND       SNOW MASS LAND EXCL.GLACIERS (KG)            (${days} ${year_rtdiag_start}-${yearm1})
C*XFIND       SNOW MASS LAND EXCL.GLACIERS/BEDR. (KG)      (${days} ${year_rtdiag_start}-${yearm1})
C*XFIND       SNOW MASS GLACIERS (KG)                      (${days} ${year_rtdiag_start}-${yearm1})
C*XFIND       SNOW MASS BEDROCK (KG)                       (${days} ${year_rtdiag_start}-${yearm1})" >> ic.xfind
        echo "C*XSAVE       SNOW MASS LAND (KG)                          (${days} ${year_rtdiag_start}-${year})
NEWNAM     SNOT
C*XSAVE       SNOW MASS LAND ONLY (KG)                     (${days} ${year_rtdiag_start}-${year})
NEWNAM     SNOT
C*XSAVE       SNOW MASS LAND EXCL.GLACIERS (KG)            (${days} ${year_rtdiag_start}-${year})
NEWNAM     SNOT
C*XSAVE       SNOW MASS LAND EXCL.GLACIERS/BEDR. (KG)      (${days} ${year_rtdiag_start}-${year})
NEWNAM     SNOT
C*XSAVE       SNOW MASS GLACIERS (KG)                      (${days} ${year_rtdiag_start}-${year})
NEWNAM     SNOT
C*XSAVE       SNOW MASS BEDROCK (KG)                       (${days} ${year_rtdiag_start}-${year})
NEWNAM     SNOT" >> ic.xsave

# ------------------------------------ monthly snow mass SNO over sea ice

        x="SNOT"
        y="FARE"
        echo "C*SELLEV      1$isic" | ccc sellev gs${x}${m} gs${x}i${m} # snow over ocean sea ice
#                                      spread over grid cell
        mlt gs${x}i${m} gs${y}i${m} gs${x}ig${m}

        timavg gs${x}ig${m} tgs${x}ig${m}
#                                      global mean
        globavg tgs${x}ig${m} _ gtavg
#                                      convert kg/m2 to kg
        gmlt gtavg globe_area gtgs${x}${m}ig

        rtdlist="$rtdlist gtgs${x}${m}ig"
        echo "C*XFIND       SNOW MASS OCEAN (KG)                         (${days} ${year_rtdiag_start}-${yearm1})" >> ic.xfind
        echo "C*XSAVE       SNOW MASS OCEAN (KG)                         (${days} ${year_rtdiag_start}-${year})
NEWNAM     SNOT" >> ic.xsave

# ------------------------------------ instantaneous first time step snow mass SNO global

        x="SNO"
#                                      select first time step
        echo "C* RCOPY           1         1" | ccc rcopy gs${x}${m} tgs${x}1${m}
        x="SNO1"
#                                      global
        globavg tgs${x}${m} _ gtavg
#                                      convert kg/m2 to kg
        gmlt gtavg globe_area gtgs${x}${m}g

        rtdlist="$rtdlist gtgs${x}${m}g"
        echo "C*XFIND       SNOW MASS FIRST GLOBAL (KG)                  (${days} ${year_rtdiag_start}-${yearm1})" >> ic.xfind
        echo "C*XSAVE       SNOW MASS FIRST GLOBAL (KG)                  (${days} ${year_rtdiag_start}-${year})
NEWNAM      SNO" >> ic.xsave

# ------------------------------------ instantaneous first time step snow mass SNO over land

        x="SNOT"
        echo "C*SELLEV      1$ilnd" | ccc sellev gs${x}${m} gs${x}l${m} # snow over land
#                                      select first time step
        echo "C* RCOPY           1         1" | ccc rcopy gs${x}l${m} tgs${x}1l${m}
        x="SNOT1"

#                                      land
        globavg tgs${x}l${m} _ gtavg dland_frac
#                                      convert kg/m2 to kg
        gmlt gtavg dland_area gtgs${x}${m}l

#                                      land only
        globavg tgs${x}l${m} _ gtavg lland_frac
#                                      convert kg/m2 to kg
        gmlt gtavg lland_area gtgs${x}${m}ll

#                                      land excluding glaciers
        globavg tgs${x}l${m} _ gtavg nogla_frac
#                                      convert kg/m2 to kg
        gmlt gtavg nogla_area gtgs${x}${m}n

#                                      land excluding glaciers/bedrock
        globavg tgs${x}l${m} _ gtavg noglb_frac
#                                      convert kg/m2 to kg
        gmlt gtavg nogla_area gtgs${x}${m}d

#                                      glaciers
        globavg tgs${x}l${m} _ gtavg glaci_frac
#                                      convert kg/m2 to kg
        gmlt gtavg glaci_area gtgs${x}${m}i

#                                      bedrock
        globavg tgs${x}l${m} _ gtavg bedro_frac
#                                      convert kg/m2 to kg
        gmlt gtavg bedro_area gtgs${x}${m}b

        rtdlist="$rtdlist gtgs${x}${m}l gtgs${x}${m}ll gtgs${x}${m}n gtgs${x}${m}d gtgs${x}${m}i gtgs${x}${m}b"
        echo "C*XFIND       SNOW MASS FIRST LAND (KG)                    (${days} ${year_rtdiag_start}-${yearm1})
C*XFIND       SNOW MASS FIRST LAND ONLY (KG)               (${days} ${year_rtdiag_start}-${yearm1})
C*XFIND       SNOW MASS FIRST LAND EXCL.GLACIERS (KG)      (${days} ${year_rtdiag_start}-${yearm1})
C*XFIND       SNOW MASS FIRST LAND EXCL.GLACIERS/BEDR. (KG)(${days} ${year_rtdiag_start}-${yearm1})
C*XFIND       SNOW MASS FIRST GLACIERS (KG)                (${days} ${year_rtdiag_start}-${yearm1})
C*XFIND       SNOW MASS FIRST BEDROCK (KG)                 (${days} ${year_rtdiag_start}-${yearm1})" >> ic.xfind
        echo "C*XSAVE       SNOW MASS FIRST LAND (KG)                    (${days} ${year_rtdiag_start}-${year})
NEWNAM     SNOT
C*XSAVE       SNOW MASS FIRST LAND ONLY (KG)               (${days} ${year_rtdiag_start}-${year})
NEWNAM     SNOT
C*XSAVE       SNOW MASS FIRST LAND EXCL.GLACIERS (KG)      (${days} ${year_rtdiag_start}-${year})
NEWNAM     SNOT
C*XSAVE       SNOW MASS FIRST LAND EXCL.GLACIERS/BEDR. (KG)(${days} ${year_rtdiag_start}-${year})
NEWNAM     SNOT
C*XSAVE       SNOW MASS FIRST GLACIERS (KG)                (${days} ${year_rtdiag_start}-${year})
NEWNAM     SNOT
C*XSAVE       SNOW MASS FIRST BEDROCK (KG)                 (${days} ${year_rtdiag_start}-${year})
NEWNAM     SNOT" >> ic.xsave

# ------------------------------------ instantaneous first time step snow mass SNO over sea ice

        x="SNOT"
        y="FARE"
        echo "C*SELLEV      1$isic" | ccc sellev gs${x}${m} gs${x}i${m} # snow over ocean sea ice
#                                      select first time step
        echo "C* RCOPY           1         1" | ccc rcopy gs${x}i${m} tgs${x}1i${m}
        x="SNOT1"
#                                      spread over grid cell
        gmlt tgs${x}i${m} gs${y}i${m} tgs${x}ig${m}

#                                      global mean
        globavg tgs${x}ig${m} _ gtavg
#                                      convert kg/m2 to kg
        gmlt gtavg globe_area gtgs${x}${m}ig

        rtdlist="$rtdlist gtgs${x}${m}ig"
        echo "C*XFIND       SNOW MASS FIRST OCEAN (KG)                   (${days} ${year_rtdiag_start}-${yearm1})" >> ic.xfind
        echo "C*XSAVE       SNOW MASS FIRST OCEAN (KG)                   (${days} ${year_rtdiag_start}-${year})
NEWNAM     SNOT" >> ic.xsave

# ------------------------------------ monthly hemispheric snow cover derived from snow fraction FN

        x="FN"
        timavg gs${x}${m} tgs${x}
#                                      compute averages of NH and SH land
        globavg tgs${x} _ gtgs${x}gn globe_frac_nh
        globavg tgs${x} _ gtgs${x}gs globe_frac_sh
        globavg tgs${x} _ gtgs${x}ln tland_frac_nh
        globavg tgs${x} _ gtgs${x}ls tland_frac_sh
        globavg tgs${x} _ gtgs${x}on ocean_frac_nh
        globavg tgs${x} _ gtgs${x}os ocean_frac_sh
#                                      convert to area (m^2)
        gmlt gtgs${x}gn globe_area_nh gtgs${x}${m}gna
        gmlt gtgs${x}gs globe_area_sh gtgs${x}${m}gsa
        gmlt gtgs${x}ln tland_area_nh gtgs${x}${m}lna
        gmlt gtgs${x}ls tland_area_sh gtgs${x}${m}lsa
        gmlt gtgs${x}on ocean_area_nh gtgs${x}${m}ona
        gmlt gtgs${x}os ocean_area_sh gtgs${x}${m}osa

        rtdlist="$rtdlist gtgs${x}${m}gna gtgs${x}${m}gsa gtgs${x}${m}lna gtgs${x}${m}lsa gtgs${x}${m}ona gtgs${x}${m}osa"

        echo "C*XFIND       SNOW COVER NH (M2)                           (${days} ${year_rtdiag_start}-${yearm1})
C*XFIND       SNOW COVER SH (M2)                           (${days} ${year_rtdiag_start}-${yearm1})
C*XFIND       SNOW COVER NH LAND (M2)                      (${days} ${year_rtdiag_start}-${yearm1})
C*XFIND       SNOW COVER SH LAND (M2)                      (${days} ${year_rtdiag_start}-${yearm1})
C*XFIND       SNOW COVER NH OCEAN (M2)                     (${days} ${year_rtdiag_start}-${yearm1})
C*XFIND       SNOW COVER SH OCEAN (M2)                     (${days} ${year_rtdiag_start}-${yearm1})" >> ic.xfind
        echo "C*XSAVE       SNOW COVER NH (M2)                           (${days} ${year_rtdiag_start}-${year})
NEWNAM       FN
C*XSAVE       SNOW COVER SH (M2)                           (${days} ${year_rtdiag_start}-${year})
NEWNAM       FN
C*XSAVE       SNOW COVER NH LAND (M2)                      (${days} ${year_rtdiag_start}-${year})
NEWNAM       FN
C*XSAVE       SNOW COVER SH LAND (M2)                      (${days} ${year_rtdiag_start}-${year})
NEWNAM       FN
C*XSAVE       SNOW COVER NH OCEAN (M2)                     (${days} ${year_rtdiag_start}-${year})
NEWNAM       FN
C*XSAVE       SNOW COVER SH OCEAN (M2)                     (${days} ${year_rtdiag_start}-${year})
NEWNAM       FN" >> ic.xsave

# ------------------------------------ monthly snow density (RHON)

        x="RHON"
        if [ -s gs${x}${m} ] ; then
#                                      masked time mean (only for RHON>0)
        y="${x}msk"
        echo "C*FMASK           -1 NEXT   GT        0.                   1" | ccc fmask gs${x}${m} gs${y}${m} # 1=RHON>0, 0=otherwise
        timavg gs${x}${m} tgs${x}${m}
        timavg gs${y}${m} tgs${y}${m}

#                                      masked spatial mean
        globavg tgs${x}${m} _ gtgs${x}${m}
        globavg tgs${y}${m} _ gtgs${y}${m}
        div gtgs${x}${m} gtgs${y}${m} gtgs${x}${m}.1 ; mv gtgs${x}${m}.1 gtgs${x}${m}

        rtdlist="$rtdlist gtgs${x}${m}"
        echo "C*XFIND       SNOW DENSITY (KG/M3)                         (${days} ${year_rtdiag_start}-${yearm1})" >> ic.xfind
        echo "C*XSAVE       SNOW DENSITY (KG/M3)                         (${days} ${year_rtdiag_start}-${year})
NEWNAM     RHON" >> ic.xsave
        fi

# ------------------------------------ monthly effective snow grain radius (REF)

        x="REF"
        if [ -s gs${x}${m} ] ; then
#                                      masked time mean (only for REF>0)
        y="${x}msk"
        echo "C*FMASK           -1 NEXT   GT        0.                   1" | ccc fmask gs${x}${m} gs${y}${m} # 1=REF>0, 0=otherwise
        timavg gs${x}${m} tgs${x}${m}
        timavg gs${y}${m} tgs${y}${m}

#                                      masked spatial mean
        globavg tgs${x}${m} _ gtgs${x}${m}
        globavg tgs${y}${m} _ gtgs${y}${m}
        div gtgs${x}${m} gtgs${y}${m} gtgs${x}${m}.1 ; mv gtgs${x}${m}.1 gtgs${x}${m}

        rtdlist="$rtdlist gtgs${x}${m}"
        echo "C*XFIND       EFFECTIVE SNOW GRAIN RADIUS (M)              (${days} ${year_rtdiag_start}-${yearm1})" >> ic.xfind
        echo "C*XSAVE       EFFECTIVE SNOW GRAIN RADIUS (M)              (${days} ${year_rtdiag_start}-${year})
NEWNAM      REF" >> ic.xsave
        fi

# ------------------------------------ monthly snow albedo AN

        x="AN"
        y="TN" # snow temperature is used to define where snow exists
#                                     snow mask
        echo "C*FMASK           -1 NEXT   GT     1.e-4" | ccc fmask gs${y}${m} gs${y}g${m}
        gmlt gs${y}g${m} dland_frac gs${y}l${m}
        gmlt gs${y}g${m} ocean_frac gs${y}o${m}
#
        mlt gs${x}${m} gs${y}g${m} gs${x}g${m}
        mlt gs${x}${m} gs${y}l${m} gs${x}l${m}
        mlt gs${x}${m} gs${y}o${m} gs${x}o${m}

#                                      spatial mean
        globavg gs${x}g${m} _ ggs${x}g${m}
        globavg gs${x}l${m} _ ggs${x}l${m}
        globavg gs${x}o${m} _ ggs${x}o${m}

        globavg gs${y}g${m} _ ggs${y}g${m}
        globavg gs${y}l${m} _ ggs${y}l${m}
        globavg gs${y}o${m} _ ggs${y}o${m}

        div ggs${x}g${m} ggs${y}g${m} ggs${x}g${m}.1 ; mv ggs${x}g${m}.1 ggs${x}g${m}
        div ggs${x}l${m} ggs${y}l${m} ggs${x}l${m}.1 ; mv ggs${x}l${m}.1 ggs${x}l${m}
        div ggs${x}o${m} ggs${y}o${m} ggs${x}o${m}.1 ; mv ggs${x}o${m}.1 ggs${x}o${m}
#                                      time average
        timavg ggs${x}g${m} tggs${x}g${m}
        timavg ggs${x}l${m} tggs${x}l${m}
        timavg ggs${x}o${m} tggs${x}o${m}

        rtdlist="$rtdlist tggs${x}g${m} tggs${x}l${m} tggs${x}o${m}"
        echo "
C*XFIND       SNOW ALBEDO GLOBAL                           (${days} ${year_rtdiag_start}-${yearm1})
C*XFIND       SNOW ALBEDO LAND                             (${days} ${year_rtdiag_start}-${yearm1})
C*XFIND       SNOW ALBEDO OCEAN                            (${days} ${year_rtdiag_start}-${yearm1})" >> ic.xfind
        echo "C*XSAVE       SNOW ALBEDO GLOBAL                           (${days} ${year_rtdiag_start}-${year})
NEWNAM       AN
C*XSAVE       SNOW ALBEDO LAND                             (${days} ${year_rtdiag_start}-${year})
NEWNAM       AN
C*XSAVE       SNOW ALBEDO OCEAN                            (${days} ${year_rtdiag_start}-${year})
NEWNAM       AN" >> ic.xsave

# ------------------------------------ monthly global BALT (new definition)

        x="BALT"
        if [ -s gs${x}${m} ] ; then
        timavg gs${x}${m} tgs${x}${m}
#                                      global
        globavg tgs${x}${m} _ gtgs${x}${m}g

        rtdlist="$rtdlist gtgs${x}${m}g"
        echo "C*XFIND       NET TOA ENERGY GLOBAL BALT (W/M2)            (${days} ${year_rtdiag_start}-${yearm1})" >> ic.xfind
        echo "C*XSAVE       NET TOA ENERGY GLOBAL BALT (W/M2)            (${days} ${year_rtdiag_start}-${year})
NEWNAM     BALT" >> ic.xsave
        fi

# ------------------------------------ monthly global BALP (AR5 definition of BALT)

        x="BALP"
        if [ -s gs${x}${m} ] ; then
        timavg gs${x}${m} tgs${x}${m}
#                                      global
        globavg tgs${x}${m} _ gtgs${x}${m}g

        rtdlist="$rtdlist gtgs${x}${m}g"
        echo "C*XFIND       NET TOA ENERGY GLOBAL BALP (W/M2)            (${days} ${year_rtdiag_start}-${yearm1})" >> ic.xfind
        echo "C*XSAVE       NET TOA ENERGY GLOBAL BALP (W/M2)            (${days} ${year_rtdiag_start}-${year})
NEWNAM     BALP" >> ic.xsave
        fi

# ------------------------------------ monthly global BALX (radiation budget at top of model)

        x="BALX"
        if [ -s gs${x}${m} ] ; then
        timavg gs${x}${m} tgs${x}${m}
#                                      global
        globavg tgs${x}${m} _ gtgs${x}${m}g

        rtdlist="$rtdlist gtgs${x}${m}g"
        echo "C*XFIND       NET TOA ENERGY GLOBAL BALX (W/M2)            (${days} ${year_rtdiag_start}-${yearm1})" >> ic.xfind
        echo "C*XSAVE       NET TOA ENERGY GLOBAL BALX (W/M2)            (${days} ${year_rtdiag_start}-${year})
NEWNAM     BALX" >> ic.xsave
        fi

# ------------------------------------ monthly BEG

        x="BEG"
        timavg gs${x}${m} tgs${x}${m}
#                                      global
        globavg tgs${x}${m} _ gtgs${x}${m}g
#                                      ocean
        globavg tgs${x}${m} _ gtgs${x}${m}o ocean_frac
#                                      land
        globavg tgs${x}${m} _ gtgs${x}${m}l tland_frac
#                                      lakes
        globavg tgs${x}${m} _ gtgs${x}${m}r rlake_frac

        rtdlist="$rtdlist gtgs${x}${m}g gtgs${x}${m}o gtgs${x}${m}l gtgs${x}${m}r"
        echo "C*XFIND       NET SFC ENERGY GLOBAL BEG (W/M2)             (${days} ${year_rtdiag_start}-${yearm1})
C*XFIND       NET SFC ENERGY OCEAN BEG (W/M2)              (${days} ${year_rtdiag_start}-${yearm1})
C*XFIND       NET SFC ENERGY LAND BEG (W/M2)               (${days} ${year_rtdiag_start}-${yearm1})
C*XFIND       NET SFC ENERGY LAKES BEG (W/M2)              (${days} ${year_rtdiag_start}-${yearm1})" >> ic.xfind
        echo "C*XSAVE       NET SFC ENERGY GLOBAL BEG (W/M2)             (${days} ${year_rtdiag_start}-${year})
NEWNAM      BEG
C*XSAVE       NET SFC ENERGY OCEAN BEG (W/M2)              (${days} ${year_rtdiag_start}-${year})
NEWNAM      BEG
C*XSAVE       NET SFC ENERGY LAND BEG (W/M2)               (${days} ${year_rtdiag_start}-${year})
NEWNAM      BEG
C*XSAVE       NET SFC ENERGY LAKES BEG (W/M2)              (${days} ${year_rtdiag_start}-${year})
NEWNAM      BEG" >> ic.xsave

# ------------------------------------ monthly BEGI/BEGO

        x="BEGI" # heat flux over sea ice
        y="BEGO" # heat flux over sea water
        z="FARE" # fractional areas
        if [ -s gs${x}${m} -a -s gs${y}${m} -a -s gs${z}${m} ] ; then

        # spread fluxes over ocean portion of a grid cell
        mlt gs${x}${m} gs${z}io${m} gs${x}${m}i
        mlt gs${y}${m} gs${z}wo${m} gs${y}${m}w

        timavg gs${x}${m}i tgs${x}${m}i
        timavg gs${y}${m}w tgs${y}${m}w
#                                      ocean mean
        globavg tgs${x}${m}i _ gtgs${x}${m}io ocean_frac
        globavg tgs${y}${m}w _ gtgs${y}${m}wo ocean_frac

        rtdlist="$rtdlist gtgs${x}${m}io gtgs${y}${m}wo"
        echo "C*XFIND       NET SFC ENERGY SEAICE BEGI (W/M2)            (${days} ${year_rtdiag_start}-${yearm1})
C*XFIND       NET SFC ENERGY OPEN WATER BEGO (W/M2)        (${days} ${year_rtdiag_start}-${yearm1})" >> ic.xfind
        echo "C*XSAVE       NET SFC ENERGY SEAICE BEGI (W/M2)            (${days} ${year_rtdiag_start}-${year})
NEWNAM     BEGI
C*XSAVE       NET SFC ENERGY OPEN WATER BEGO (W/M2)        (${days} ${year_rtdiag_start}-${year})
NEWNAM     BEGO" >> ic.xsave
        fi

# ------------------------------------ monthly BEGL

        x="BEGL" # heat flux over land
        if [ -s gs${x}${m} ] ; then
        timavg gs${x}${m} tgs${x}${m}
#                                      land mean
        globavg tgs${x}${m} _ gtgs${x}${m}l tland_frac

        rtdlist="$rtdlist gtgs${x}${m}l"
        echo "C*XFIND       NET SFC ENERGY LAND BEGL (W/M2)              (${days} ${year_rtdiag_start}-${yearm1}" >> ic.xfind
        echo "C*XSAVE       NET SFC ENERGY LAND BEGL (W/M2)              (${days} ${year_rtdiag_start}-${year})
NEWNAM     BEGL" >> ic.xsave
        fi

# ------------------------------------ monthly FSGI/FSGO

        x="FSGI" # heat flux over sea ice
        y="FSGO" # heat flux over sea water
        z="FARE" # fractional areas
        if [ -s gs${x}${m} -a -s gs${y}${m} -a -s gs${z}${m} ] ; then

        # spread fluxes over ocean portion of a grid cell
        mlt gs${x}${m} gs${z}io${m} gs${x}${m}i
        mlt gs${y}${m} gs${z}wo${m} gs${y}${m}w

        timavg gs${x}${m}i tgs${x}${m}i
        timavg gs${y}${m}w tgs${y}${m}w
#                                      ocean mean
        globavg tgs${x}${m}i _ gtgs${x}${m}io ocean_frac
        globavg tgs${y}${m}w _ gtgs${y}${m}wo ocean_frac

        rtdlist="$rtdlist gtgs${x}${m}io gtgs${y}${m}wo"
        echo "C*XFIND       NET SFC SOLAR FLUX SEAICE FSGI (W/M2)        (${days} ${year_rtdiag_start}-${yearm1})
C*XFIND       NET SFC SOLAR FLUX OPEN WATER FSGO (W/M2)    (${days} ${year_rtdiag_start}-${yearm1})" >> ic.xfind
        echo "C*XSAVE       NET SFC SOLAR FLUX SEAICE FSGI (W/M2)        (${days} ${year_rtdiag_start}-${year})
NEWNAM     FSGI
C*XSAVE       NET SFC SOLAR FLUX OPEN WATER FSGO (W/M2)    (${days} ${year_rtdiag_start}-${year})
NEWNAM     FSGO" >> ic.xsave
        fi

# ------------------------------------ monthly PCP over sea water and sea ice (PCPO/PCPI)

        x="PCP" # precipitation flux over sea ice
        y="PCP" # precipitation flux over sea water
        z="FARE" # fractional areas
        if [ -s gs${x}${m} -a -s gs${y}${m} -a -s gs${z}${m} ] ; then

        # spread fluxes over ocean portion of a grid cell
        mlt gs${x}${m} gs${z}io${m} gs${x}${m}i
        mlt gs${y}${m} gs${z}wo${m} gs${y}${m}w

        timavg gs${x}${m}i tgs${x}${m}i
        timavg gs${y}${m}w tgs${y}${m}w
#                                      convert kg/m2/s or mm/s to mm/day by multiplying with 60x60x24=86400
        echo "C*XLIN        86400.       0.0" | ccc xlin tgs${x}${m}i tgs${x}${m}i.1 ; mv tgs${x}${m}i.1 tgs${x}${m}i
        echo "C*XLIN        86400.       0.0" | ccc xlin tgs${y}${m}w tgs${y}${m}w.1 ; mv tgs${y}${m}w.1 tgs${y}${m}w
#                                      ocean mean
        globavg tgs${x}${m}i _ gtgs${x}${m}io ocean_frac
        globavg tgs${y}${m}w _ gtgs${y}${m}wo ocean_frac

        rtdlist="$rtdlist gtgs${x}${m}io gtgs${y}${m}wo"
        echo "C*XFIND       PRECIPITATION OVER SEAICE (MM/DAY)           (${days} ${year_rtdiag_start}-${yearm1})
C*XFIND       PRECIPITATION OVER OPEN WATER (MM/DAY)       (${days} ${year_rtdiag_start}-${yearm1})" >> ic.xfind
        echo "C*XSAVE       PRECIPITATION OVER SEAICE (MM/DAY)           (${days} ${year_rtdiag_start}-${year})
NEWNAM      PCP
C*XSAVE       PRECIPITATION OVER OPEN WATER (MM/DAY)       (${days} ${year_rtdiag_start}-${year})
NEWNAM      PCP" >> ic.xsave
        fi

# ------------------------------------ monthly QFS over sea water and sea ice (QFSO/QFSI)

        x="QFSI" # precipitation flux over sea ice
        y="QFSO" # precipitation flux over sea water
        z="FARE" # fractional areas
        if [ -s gs${x}${m} -a -s gs${y}${m} -a -s gs${z}${m} ] ; then

        # spread fluxes over ocean portion of a grid cell
        mlt gs${x}${m} gs${z}io${m} gs${x}${m}i
        mlt gs${y}${m} gs${z}wo${m} gs${y}${m}w

        timavg gs${x}${m}i tgs${x}${m}i
        timavg gs${y}${m}w tgs${y}${m}w
#                                      convert kg/m2/s or mm/s to mm/day by multiplying with 60x60x24=86400
        echo "C*XLIN        86400.       0.0" | ccc xlin tgs${x}${m}i tgs${x}${m}i.1 ; mv tgs${x}${m}i.1 tgs${x}${m}i
        echo "C*XLIN        86400.       0.0" | ccc xlin tgs${y}${m}w tgs${y}${m}w.1 ; mv tgs${y}${m}w.1 tgs${y}${m}w
#                                      ocean mean
        globavg tgs${x}${m}i _ gtgs${x}${m}io ocean_frac
        globavg tgs${y}${m}w _ gtgs${y}${m}wo ocean_frac

        rtdlist="$rtdlist gtgs${x}${m}io gtgs${y}${m}wo"
        echo "C*XFIND       EVAPORATION OVER SEAICE (MM/DAY)             (${days} ${year_rtdiag_start}-${yearm1})
C*XFIND       EVAPORATION OVER OPEN WATER (MM/DAY)         (${days} ${year_rtdiag_start}-${yearm1})" >> ic.xfind
        echo "C*XSAVE       EVAPORATION OVER SEAICE (MM/DAY)             (${days} ${year_rtdiag_start}-${year})
NEWNAM      QFS
C*XSAVE       EVAPORATION OVER OPEN WATER (MM/DAY)         (${days} ${year_rtdiag_start}-${year})
NEWNAM      QFS" >> ic.xsave
        fi

# ------------------------------------ monthly HFLI

        x="HFLI" # heat flux over sea ice
        z="FARE" # fractional areas
        if [ -s gs${x}${m} -a -s gs${z}${m} ] ; then

        # spread fluxes over ocean portion of a grid cell
        mlt gs${x}${m} gs${z}io${m} gs${x}${m}i

        timavg gs${x}${m}i tgs${x}${m}i
#                                      ocean mean
        globavg tgs${x}${m}i _ gtgs${x}${m}io ocean_frac

        rtdlist="$rtdlist gtgs${x}${m}io"
        echo "C*XFIND       OCEAN SFC LATENT SUBLIM HEAT UP HFLI (W/M2)  (${days} ${year_rtdiag_start}-${yearm1})" >> ic.xfind
        echo "C*XSAVE       OCEAN SFC LATENT SUBLIM HEAT UP HFLI (W/M2)  (${days} ${year_rtdiag_start}-${year})
NEWNAM     HFLI" >> ic.xsave
        fi

# ------------------------------------ monthly BWGI

        x="BWGI" # freshwater flux P-E over sea ice
        z="FARE" # fractional areas
        if [ -s gs${x}${m} -a -s gs${z}${m} ] ; then

        # spread fluxes over ocean portion of a grid cell
        mlt gs${x}${m} gs${z}io${m} gs${x}${m}i

        timavg gs${x}${m}i tgs${x}${m}i
#                                      ocean mean
        globavg tgs${x}${m}i _ gtgs${x}${m}io ocean_frac
#                                      convert kg/m2/s or mm/s to mm/day by multiplying with 60x60x24=86400
        echo "C*XLIN        86400.       0.0" | ccc xlin gtgs${x}${m}io gtgs${x}${m}io.1 ; mv gtgs${x}${m}io.1 gtgs${x}${m}io

        rtdlist="$rtdlist gtgs${x}${m}io"
        echo "C*XFIND       NET SFC WATER FLUX SEAICE BWGI (MM/DAY)      (${days} ${year_rtdiag_start}-${yearm1})" >> ic.xfind
        echo "C*XSAVE       NET SFC WATER FLUX SEAICE BWGI (MM/DAY)      (${days} ${year_rtdiag_start}-${year})
NEWNAM     BWGI" >> ic.xsave
        fi

# ------------------------------------ monthly BWGO

        y="BWGO" # freshwater flux P-E over ocean
        z="FARE" # fractional areas
        if [ -s gs${y}${m} -a -s gs${z}${m} ] ; then

        # spread fluxes over ocean portion of a grid cell
        mlt gs${y}${m} gs${z}wo${m} gs${y}${m}w

        timavg gs${y}${m}w tgs${y}${m}w
#                                      ocean mean
        globavg tgs${y}${m}w _ gtgs${y}${m}wo ocean_frac
#                                      convert kg/m2/s or mm/s to mm/day by multiplying with 60x60x24=86400
        echo "C*XLIN        86400.       0.0" | ccc xlin gtgs${y}${m}wo gtgs${y}${m}wo.1 ; mv gtgs${y}${m}wo.1 gtgs${y}${m}wo

        rtdlist="$rtdlist gtgs${y}${m}wo"
        echo "C*XFIND       NET SFC WATER FLUX OPEN WATER BWGO (MM/DAY)  (${days} ${year_rtdiag_start}-${yearm1})" >> ic.xfind
        echo "C*XSAVE       NET SFC WATER FLUX OPEN WATER BWGO (MM/DAY)  (${days} ${year_rtdiag_start}-${year})
NEWNAM     BWGO" >> ic.xsave
        fi

# ------------------------------------ construct PLA tracers

        if [ "$PLAtracers" = "on" ] ; then

# ------------------------------------ ammonium sulphate burden (BAS=VI15+VI18+VI21)

        x="BAS"
        if [ -s gsVI15${m} -a -s gsVI18${m} -a -s gsVI21${m} ] ; then
          add gsVI15${m}    gsVI18${m} gsVI15p18${m}
          add gsVI15p18${m} gsVI21${m} gs${x}${m}
          release gsVI15p18${m}
        fi

# ------------------------------------ organic matter burden (BOA=VI12+VI13+VI16+VI19)

        x="BOA"
        if [ -s gsVI12${m} -a -s gsVI13${m} -a -s gsVI16${m} -a -s gsVI19${m} ] ; then
          add gsVI12${m}    gsVI13${m} gsVI12p13${m}
          add gsVI16${m}    gsVI19${m} gsVI16p19${m}
          add gsVI12p13${m} gsVI16p19${m} gs${x}${m}
          release gsVI12p13${m} gsVI16p19${m}
        fi

# ------------------------------------ black carbon burden (BBC=VI11+VI14+VI17+VI20)

        x="BBC"
        if [ -s gsVI11${m} -a -s gsVI14${m} -a -s gsVI17${m} -a -s gsVI20${m} ] ; then
          add gsVI11${m}    gsVI14${m} gsVI11p14${m}
          add gsVI17${m}    gsVI20${m} gsVI17p20${m}
          add gsVI11p14${m} gsVI17p20${m} gs${x}${m}
          release gsVI11p14${m} gsVI17p20${m}
        fi

# ------------------------------------ sea salt burden (BSS=VI07+VI08)

        x="BSS"
        if [ -s gsVI07${m} -a -s gsVI08${m} ] ; then
          add gsVI07${m} gsVI08${m} gs${x}${m}
        fi

# ------------------------------------ mineral dust burden (BMD=VI09+VI10)

        x="BMD"
        if [ -s gsVI09${m} -a -s gsVI10${m} ] ; then
          add gsVI09${m} gsVI10${m} gs${x}${m}
        fi

# ------------------------------------ number burden internally mixed aerosol (BNI=VI26+VI27+VI28+VI29+VI30)

        x="BNI"
        if [ -s gsVI26${m} -a -s gsVI27${m} -a -s gsVI28${m} -a -s gsVI29${m} -a -s gsVI30${m} ] ; then
          add gsVI26${m}    gsVI27${m}    gsVI26p27${m}
          add gsVI28${m}    gsVI29${m}    gsVI28p29${m}
          add gsVI26p27${m} gsVI28p29${m} gsVI26p29${m}
          add gsVI26p29${m} gsVI30${m}    gs${x}${m}
          release gsVI26p27${m} gsVI28p29${m} gsVI26p29${m}
        fi

# ------------------------------------ number burden externally mixed aerosol (BNE=VI22+VI23+VI24+VI25)

        x="BNE"
        if [ -s gsVI22${m} -a -s gsVI23${m} -a -s gsVI24${m} -a -s gsVI25${m} ] ; then
          add gsVI22${m}    gsVI23${m} gsVI22p23${m}
          add gsVI24${m}    gsVI25${m} gsVI24p25${m}
          add gsVI22p23${m} gsVI24p25${m} gs${x}${m}
          release gsVI22p23${m} gsVI24p25${m}
        fi
        fi # PLAtracers=on

# ------------------------------------ monthly nudging tendencies

# ------------------------------------ heat flux tendency due to difference in SST over sea water

        x="TBEG"
        if [ -s gs${x}${m} ] ; then
        timavg gs${x}${m} tgs${x}${m}
#                                      ocean
        globavg tgs${x}${m} _ gtgs${x}${m}onh ocean_frac_nh
        globavg tgs${x}${m} _ gtgs${x}${m}osh ocean_frac_sh

        rtdlist="$rtdlist gtgs${x}${m}onh gtgs${x}${m}osh"
        echo "C*XFIND       HEAT FLUX TENDENCY DUE TO SST (WATER/WATER)NH(${days} ${year_rtdiag_start}-${yearm1})
C*XFIND       HEAT FLUX TENDENCY DUE TO SST (WATER/WATER)SH(${days} ${year_rtdiag_start}-${yearm1})" >> ic.xfind
        echo "C*XSAVE       HEAT FLUX TENDENCY DUE TO SST (WATER/WATER)NH(${days} ${year_rtdiag_start}-${year})
NEWNAM     TBEG
C*XSAVE       HEAT FLUX TENDENCY DUE TO SST (WATER/WATER)SH(${days} ${year_rtdiag_start}-${year})
NEWNAM     TBEG" >> ic.xsave
        fi

# ------------------------------------ heat flux tendency due to difference in SST over water in model, ice in obs

        x="TBEI"
        if [ -s gs${x}${m} ] ; then
        timavg gs${x}${m} tgs${x}${m}
#                                      ocean
        globavg tgs${x}${m} _ gtgs${x}${m}onh ocean_frac_nh
        globavg tgs${x}${m} _ gtgs${x}${m}osh ocean_frac_sh

        rtdlist="$rtdlist gtgs${x}${m}onh gtgs${x}${m}osh"
        echo "C*XFIND       HEAT FLUX TENDENCY DUE TO SST (WATER/ICE) NH (${days} ${year_rtdiag_start}-${yearm1})
C*XFIND       HEAT FLUX TENDENCY DUE TO SST (WATER/ICE) SH (${days} ${year_rtdiag_start}-${yearm1})" >> ic.xfind
        echo "C*XSAVE       HEAT FLUX TENDENCY DUE TO SST (WATER/ICE) NH (${days} ${year_rtdiag_start}-${year})
NEWNAM     TBEI
C*XSAVE       HEAT FLUX TENDENCY DUE TO SST (WATER/ICE) SH (${days} ${year_rtdiag_start}-${year})
NEWNAM     TBEI" >> ic.xsave
        fi

# ------------------------------------ heat flux tendency due to difference in SST over ice in model, water in obs

        x="TBES"
        if [ -s gs${x}${m} ] ; then
        timavg gs${x}${m} tgs${x}${m}
#                                      ocean
        globavg tgs${x}${m} _ gtgs${x}${m}onh ocean_frac_nh
        globavg tgs${x}${m} _ gtgs${x}${m}osh ocean_frac_sh

        rtdlist="$rtdlist gtgs${x}${m}onh gtgs${x}${m}osh"
        echo "C*XFIND       HEAT FLUX TENDENCY DUE TO SST (ICE/WATER) NH (${days} ${year_rtdiag_start}-${yearm1})
C*XFIND       HEAT FLUX TENDENCY DUE TO SST (ICE/WATER) SH (${days} ${year_rtdiag_start}-${yearm1})" >> ic.xfind
        echo "C*XSAVE       HEAT FLUX TENDENCY DUE TO SST (ICE/WATER) NH (${days} ${year_rtdiag_start}-${year})
NEWNAM     TBES
C*XSAVE       HEAT FLUX TENDENCY DUE TO SST (ICE/WATER) SH (${days} ${year_rtdiag_start}-${year})
NEWNAM     TBES" >> ic.xsave
        fi

# ------------------------------------ heat flux tendency due to difference in SIC over sea water

        x="SBEG"
        if [ -s gs${x}${m} ] ; then
        timavg gs${x}${m} tgs${x}${m}
#                                      ocean
        globavg tgs${x}${m} _ gtgs${x}${m}onh ocean_frac_nh
        globavg tgs${x}${m} _ gtgs${x}${m}osh ocean_frac_sh

        rtdlist="$rtdlist gtgs${x}${m}onh gtgs${x}${m}osh"
        echo "C*XFIND       HEAT FLUX TENDENCY DUE TO SIC (ICE/ICE) NH   (${days} ${year_rtdiag_start}-${yearm1})
C*XFIND       HEAT FLUX TENDENCY DUE TO SIC (ICE/ICE) SH   (${days} ${year_rtdiag_start}-${yearm1})" >> ic.xfind
        echo "C*XSAVE       HEAT FLUX TENDENCY DUE TO SIC (ICE/ICE) NH   (${days} ${year_rtdiag_start}-${year})
NEWNAM     SBEG
C*XSAVE       HEAT FLUX TENDENCY DUE TO SIC (ICE/ICE) SH   (${days} ${year_rtdiag_start}-${year})
NEWNAM     SBEG" >> ic.xsave
        fi

# ------------------------------------ heat flux tendency due to difference in SIC over water in model, ice in obs

        x="SBEI"
        if [ -s gs${x}${m} ] ; then
        timavg gs${x}${m} tgs${x}${m}
#                                      ocean
        globavg tgs${x}${m} _ gtgs${x}${m}onh ocean_frac_nh
        globavg tgs${x}${m} _ gtgs${x}${m}osh ocean_frac_sh

        rtdlist="$rtdlist gtgs${x}${m}onh gtgs${x}${m}osh"
        echo "C*XFIND       HEAT FLUX TENDENCY DUE TO SIC (WATER/ICE) NH (${days} ${year_rtdiag_start}-${yearm1})
C*XFIND       HEAT FLUX TENDENCY DUE TO SIC (WATER/ICE) SH (${days} ${year_rtdiag_start}-${yearm1})" >> ic.xfind
        echo "C*XSAVE       HEAT FLUX TENDENCY DUE TO SIC (WATER/ICE) NH (${days} ${year_rtdiag_start}-${year})
NEWNAM     SBEI
C*XSAVE       HEAT FLUX TENDENCY DUE TO SIC (WATER/ICE) SH (${days} ${year_rtdiag_start}-${year})
NEWNAM     SBEI" >> ic.xsave
        fi

# ------------------------------------ heat flux tendency due to difference in SIC over ice in model, water in obs

        x="SBES"
        if [ -s gs${x}${m} ] ; then
        timavg gs${x}${m} tgs${x}${m}
#                                      ocean
        globavg tgs${x}${m} _ gtgs${x}${m}onh ocean_frac_nh
        globavg tgs${x}${m} _ gtgs${x}${m}osh ocean_frac_sh

        rtdlist="$rtdlist gtgs${x}${m}onh gtgs${x}${m}osh"
        echo "C*XFIND       HEAT FLUX TENDENCY DUE TO SIC (ICE/WATER) NH (${days} ${year_rtdiag_start}-${yearm1})
C*XFIND       HEAT FLUX TENDENCY DUE TO SIC (ICE/WATER) SH (${days} ${year_rtdiag_start}-${yearm1})" >> ic.xfind
        echo "C*XSAVE       HEAT FLUX TENDENCY DUE TO SIC (ICE/WATER) NH (${days} ${year_rtdiag_start}-${year})
NEWNAM     SBES
C*XSAVE       HEAT FLUX TENDENCY DUE TO SIC (ICE/WATER) SH (${days} ${year_rtdiag_start}-${year})
NEWNAM     SBES" >> ic.xsave
        fi

# ------------------------------------ sea ice concentration tendency

        x="TSNN"
        if [ -s gs${x}${m} ] ; then
        timavg gs${x}${m} tgs${x}${m}
#                                      ocean
        globavg tgs${x}${m} _ gtgs${x}${m}onh ocean_frac_nh
        globavg tgs${x}${m} _ gtgs${x}${m}osh ocean_frac_sh

        rtdlist="$rtdlist gtgs${x}${m}onh gtgs${x}${m}osh"
        echo "C*XFIND       SEAICE CONC TENDENCY DUE TO SICN NH          (${days} ${year_rtdiag_start}-${yearm1})
C*XFIND       SEAICE CONC TENDENCY DUE TO SICN SH          (${days} ${year_rtdiag_start}-${yearm1})" >> ic.xfind
        echo "C*XSAVE       SEAICE CONC TENDENCY DUE TO SICN NH          (${days} ${year_rtdiag_start}-${year})
NEWNAM     TSNN
C*XSAVE       SEAICE CONC TENDENCY DUE TO SICN SH          (${days} ${year_rtdiag_start}-${year})
NEWNAM     TSNN" >> ic.xsave
        fi

# ------------------------------------ fresh water flux tendency due to difference in SSS

        x="TBWG"
        if [ -s gs${x}${m} ] ; then
        timavg gs${x}${m} tgs${x}${m}
#                                      ocean
        globavg tgs${x}${m} _ gtgs${x}${m}o ocean_frac

        rtdlist="$rtdlist gtgs${x}${m}o"
        echo "C*XFIND       FRESHWATER FLUX TENDENCY DUE TO SSS          (${days} ${year_rtdiag_start}-${yearm1})" >> ic.xfind
        echo "C*XSAVE       FRESHWATER FLUX TENDENCY DUE TO SSS          (${days} ${year_rtdiag_start}-${year})
NEWNAM     TBWG" >> ic.xsave
        fi

# ------------------------------------ top soil layer temperature tendency

        x="TTG1"
        if [ -s gs${x}${m} ] ; then
        timavg gs${x}${m} tgs${x}${m}
#                                      land
        globavg tgs${x}${m} _ gtgs${x}${m}l tland_frac

        rtdlist="$rtdlist gtgs${x}${m}l"
        echo "C*XFIND       TG1 TENDENCY                                 (${days} ${year_rtdiag_start}-${yearm1})" >> ic.xfind
        echo "C*XSAVE       TG1 TENDENCY                                 (${days} ${year_rtdiag_start}-${year})
NEWNAM     TTG1" >> ic.xsave
        fi

# ------------------------------------ top soil layer moisture tendency (volumetric fraction)

        x="TWG1"
        if [ -s gs${x}${m} ] ; then
        timavg gs${x}${m} tgs${x}${m}
#                                      land
        globavg tgs${x}${m} _ gtgs${x}${m}l tland_frac

        rtdlist="$rtdlist gtgs${x}${m}l"
        echo "C*XFIND       WG1 TENDENCY                                 (${days} ${year_rtdiag_start}-${yearm1})" >> ic.xfind
        echo "C*XSAVE       WG1 TENDENCY                                 (${days} ${year_rtdiag_start}-${year})
NEWNAM     TWG1" >> ic.xsave
        fi

# ------------------------------------ spectral TEMP
        x="TEMP"
        if [ -s ss${x}${m} ] ; then
        globavg ss${x}${m} _ gss${x}${m}
        $RTEXTBLS/lopgm_r8i8/timavg gss${x}${m} gtss${x}${m}
        rtdlistadd=""
        lev=1
        while [ $lev -le $ilev ] ; do
          lev5=`echo $lev | awk '{printf "%05i", $1}'`
          rtdlistadd="$rtdlistadd gtss${x}${m}l${lev}"
          echo "C*XFIND       TEMP($lev5) SPECTRAL                         (${days} ${year_rtdiag_start}-${yearm1})" >> ic.xfind
          echo "C*XSAVE       TEMP($lev5) SPECTRAL                         (${days} ${year_rtdiag_start}-${year})
NEWNAM     TEMP" >> ic.xsave
          lev=`expr $lev + 1`
        done
        rsplit gtss${x}${m} $rtdlistadd
        rtdlist="$rtdlist $rtdlistadd"
        fi

# ------------------------------------ spectral ES
        x="ES"
        if [ -s ss${x}${m} ] ; then
        globavg ss${x}${m} _ gss${x}${m}
        $RTEXTBLS/lopgm_r8i8/timavg gss${x}${m} gtss${x}${m}
        rtdlistadd=""
        lev=1
        while [ $lev -le $levs ] ; do
          lev5=`echo $lev | awk '{printf "%05i", $1}'`
          rtdlistadd="$rtdlistadd gtss${x}${m}l${lev}"
          echo "C*XFIND       ES($lev5) SPECTRAL                           (${days} ${year_rtdiag_start}-${yearm1})" >> ic.xfind
          echo "C*XSAVE       ES($lev5) SPECTRAL                           (${days} ${year_rtdiag_start}-${year})
NEWNAM     TEMP" >> ic.xsave
          lev=`expr $lev + 1`
        done
        rsplit gtss${x}${m} $rtdlistadd
        rtdlist="$rtdlist $rtdlistadd"
        fi

# ------------------------------------ spectral TEMP tendency
        x="TMPN"
        if [ -s ss${x}${m} ] ; then
        globavg ss${x}${m} _ gss${x}${m}
        $RTEXTBLS/lopgm_r8i8/timavg gss${x}${m} gtss${x}${m}
        rtdlistadd=""
        lev=1
        while [ $lev -le $ilev ] ; do
          lev5=`echo $lev | awk '{printf "%05i", $1}'`
          rtdlistadd="$rtdlistadd gtss${x}${m}l${lev}"
          echo "C*XFIND       TEMP($lev5) SPECTRAL TENDENCY                (${days} ${year_rtdiag_start}-${yearm1})" >> ic.xfind
          echo "C*XSAVE       TEMP($lev5) SPECTRAL TENDENCY                (${days} ${year_rtdiag_start}-${year})
NEWNAM     TMPN" >> ic.xsave
          lev=`expr $lev + 1`
        done
        rsplit gtss${x}${m} $rtdlistadd
        rtdlist="$rtdlist $rtdlistadd"
        fi

# ------------------------------------ spectral ES tendency
        x="ESN"
        if [ -s ss${x}${m} ] ; then
        globavg ss${x}${m} _ gss${x}${m}
        $RTEXTBLS/lopgm_r8i8/timavg gss${x}${m} gtss${x}${m}
        rtdlistadd=""
        lev=1
        while [ $lev -le $levs ] ; do
          lev5=`echo $lev | awk '{printf "%05i", $1}'`
          rtdlistadd="$rtdlistadd gtss${x}${m}l${lev}"
          echo "C*XFIND       ES($lev5) SPECTRAL TENDENCY                  (${days} ${year_rtdiag_start}-${yearm1})" >> ic.xfind
          echo "C*XSAVE       ES($lev5) SPECTRAL TENDENCY                  (${days} ${year_rtdiag_start}-${year})
NEWNAM      ESN" >> ic.xsave
          lev=`expr $lev + 1`
        done
        rsplit gtss${x}${m} $rtdlistadd
        rtdlist="$rtdlist $rtdlistadd"
        fi

# ------------------------------------ spectral TEMP reference
        x="TMPR"
        if [ -s ss${x}${m} ] ; then
        globavg ss${x}${m} _ gss${x}${m}
        $RTEXTBLS/lopgm_r8i8/timavg gss${x}${m} gtss${x}${m}
        rtdlistadd=""
        lev=1
        while [ $lev -le $ilev ] ; do
          lev5=`echo $lev | awk '{printf "%05i", $1}'`
          rtdlistadd="$rtdlistadd gtss${x}${m}l${lev}"
          echo "C*XFIND       TEMP($lev5) SPECTRAL REFERENCE               (${days} ${year_rtdiag_start}-${yearm1})" >> ic.xfind
          echo "C*XSAVE       TEMP($lev5) SPECTRAL REFERENCE               (${days} ${year_rtdiag_start}-${year})
NEWNAM     TMPN" >> ic.xsave
          lev=`expr $lev + 1`
        done
        rsplit gtss${x}${m} $rtdlistadd
        rtdlist="$rtdlist $rtdlistadd"
        fi

# ------------------------------------ spectral ES reference
        x="ESR"
        if [ -s ss${x}${m} ] ; then
        globavg ss${x}${m} _ gss${x}${m}
        $RTEXTBLS/lopgm_r8i8/timavg gss${x}${m} gtss${x}${m}
        rtdlistadd=""
        lev=1
        while [ $lev -le $levs ] ; do
          lev5=`echo $lev | awk '{printf "%05i", $1}'`
          rtdlistadd="$rtdlistadd gtss${x}${m}l${lev}"
          echo "C*XFIND       ES($lev5) SPECTRAL REFERENCE                 (${days} ${year_rtdiag_start}-${yearm1})" >> ic.xfind
          echo "C*XSAVE       ES($lev5) SPECTRAL REFERENCE                 (${days} ${year_rtdiag_start}-${year})
NEWNAM      ESN" >> ic.xsave
          lev=`expr $lev + 1`
        done
        rsplit gtss${x}${m} $rtdlistadd
        rtdlist="$rtdlist $rtdlistadd"
        fi

# ************************************ Physical Atmosphere annual section

        if [ ${m} -eq 12 ] ; then

# ------------------------------------ annual ST

        x="ST"
        timavg gs${x}01 tgs${x} _ _ gs${x}02 gs${x}03 gs${x}04 gs${x}05 gs${x}06 gs${x}07 gs${x}08 gs${x}09 gs${x}10 gs${x}11 gs${x}12
#                                      global
        globavg tgs${x} _ gtgs${x}g
#                                      land
        globavg tgs${x} _ gtgs${x}l tland_frac
#                                      ocean
        globavg tgs${x} _ gtgs${x}o ocean_frac
#                                      land excluding glaciers
        globavg tgs${x} _ gtgs${x}n nogla_frac
#                                      land excluding Antarctica
        globavg tgs${x} _ gtgs${x}c noant_frac
#                                      glaciers
        globavg tgs${x} _ gtgs${x}i glaci_frac
#                                      NH polar cap
        globavg tgs${x} _ gtgs${x}nhp globe_frac_nhp
#                                      SH polar cap
        globavg tgs${x} _ gtgs${x}shp globe_frac_shp

        rtdlist_ann="$rtdlist_ann gtgs${x}g gtgs${x}l gtgs${x}o gtgs${x}n gtgs${x}c gtgs${x}i gtgs${x}nhp gtgs${x}shp"
        echo "C*XFIND       SCREEN TEMPERATURE GLOBAL (K)                (ANN ${year_rtdiag_start}-${yearm1})
C*XFIND       SCREEN TEMPERATURE LAND (K)                  (ANN ${year_rtdiag_start}-${yearm1})
C*XFIND       SCREEN TEMPERATURE OCEAN (K)                 (ANN ${year_rtdiag_start}-${yearm1})
C*XFIND       SCREEN TEMPERATURE LAND EXCL.GLACIERS (K)    (ANN ${year_rtdiag_start}-${yearm1})
C*XFIND       SCREEN TEMPERATURE LAND EXCL.ANTARCTICA (K)  (ANN ${year_rtdiag_start}-${yearm1})
C*XFIND       SCREEN TEMPERATURE GLACIERS (K)              (ANN ${year_rtdiag_start}-${yearm1})
C*XFIND       SCREEN TEMPERATURE NORTH OF 60N (K)          (ANN ${year_rtdiag_start}-${yearm1})
C*XFIND       SCREEN TEMPERATURE SOUTH OF 60S (K)          (ANN ${year_rtdiag_start}-${yearm1})" >> ic.xfind_ann
        echo "C*XSAVE       SCREEN TEMPERATURE GLOBAL (K)                (ANN ${year_rtdiag_start}-${year})
NEWNAM       ST
C*XSAVE       SCREEN TEMPERATURE LAND (K)                  (ANN ${year_rtdiag_start}-${year})
NEWNAM       ST
C*XSAVE       SCREEN TEMPERATURE OCEAN (K)                 (ANN ${year_rtdiag_start}-${year})
NEWNAM       ST
C*XSAVE       SCREEN TEMPERATURE LAND EXCL.GLACIERS (K)    (ANN ${year_rtdiag_start}-${year})
NEWNAM       ST
C*XSAVE       SCREEN TEMPERATURE LAND EXCL.ANTARCTICA (K)  (ANN ${year_rtdiag_start}-${year})
NEWNAM       ST
C*XSAVE       SCREEN TEMPERATURE GLACIERS (K)              (ANN ${year_rtdiag_start}-${year})
NEWNAM       ST
C*XSAVE       SCREEN TEMPERATURE NORTH OF 60N (K)          (ANN ${year_rtdiag_start}-${year})
NEWNAM       ST
C*XSAVE       SCREEN TEMPERATURE SOUTH OF 60S (K)          (ANN ${year_rtdiag_start}-${year})
NEWNAM       ST" >> ic.xsave_ann

# ------------------------------------ annual STMX

        x="STMX"
        timavg gs${x}01 tgs${x} _ _ gs${x}02 gs${x}03 gs${x}04 gs${x}05 gs${x}06 gs${x}07 gs${x}08 gs${x}09 gs${x}10 gs${x}11 gs${x}12
#                                      global
        globavg tgs${x} _ gtgs${x}g
#                                      land
        globavg tgs${x} _ gtgs${x}l tland_frac
#                                      ocean
        globavg tgs${x} _ gtgs${x}o ocean_frac

        rtdlist_ann="$rtdlist_ann gtgs${x}g gtgs${x}l gtgs${x}o"
        echo "C*XFIND       SCREEN TEMPERATURE MAX GLOBAL (K)            (ANN ${year_rtdiag_start}-${yearm1})
C*XFIND       SCREEN TEMPERATURE MAX LAND (K)              (ANN ${year_rtdiag_start}-${yearm1})
C*XFIND       SCREEN TEMPERATURE MAX OCEAN (K)             (ANN ${year_rtdiag_start}-${yearm1})" >> ic.xfind_ann
        echo "C*XSAVE       SCREEN TEMPERATURE MAX GLOBAL (K)            (ANN ${year_rtdiag_start}-${year})
NEWNAM     STMX
C*XSAVE       SCREEN TEMPERATURE MAX LAND (K)              (ANN ${year_rtdiag_start}-${year})
NEWNAM     STMX
C*XSAVE       SCREEN TEMPERATURE MAX OCEAN (K)             (ANN ${year_rtdiag_start}-${year})
NEWNAM     STMX" >> ic.xsave_ann

# ------------------------------------ annual STMX max

        x="STMX"
        timmax gs${x}01 tgs${x}max gs${x}02 gs${x}03 gs${x}04 gs${x}05 gs${x}06 gs${x}07 gs${x}08 gs${x}09 gs${x}10 gs${x}11 gs${x}12
#                                      global
        globavg tgs${x}max _ gtgs${x}maxg
#                                      land
        globavg tgs${x}max _ gtgs${x}maxl tland_frac
#                                      ocean
        globavg tgs${x}max _ gtgs${x}maxo ocean_frac

        rtdlist_ann="$rtdlist_ann gtgs${x}maxg gtgs${x}maxl gtgs${x}maxo"
        echo "C*XFIND       MAX SCREEN TEMPERATURE MAX GLOBAL (K)        (ANN ${year_rtdiag_start}-${yearm1})
C*XFIND       MAX SCREEN TEMPERATURE MAX LAND (K)          (ANN ${year_rtdiag_start}-${yearm1})
C*XFIND       MAX SCREEN TEMPERATURE MAX OCEAN (K)         (ANN ${year_rtdiag_start}-${yearm1})" >> ic.xfind_ann
        echo "C*XSAVE       MAX SCREEN TEMPERATURE MAX GLOBAL (K)        (ANN ${year_rtdiag_start}-${year})
NEWNAM     STMX
C*XSAVE       MAX SCREEN TEMPERATURE MAX LAND (K)          (ANN ${year_rtdiag_start}-${year})
NEWNAM     STMX
C*XSAVE       MAX SCREEN TEMPERATURE MAX OCEAN (K)         (ANN ${year_rtdiag_start}-${year})
NEWNAM     STMX" >> ic.xsave_ann

# ------------------------------------ annual STMN

        x="STMN"
        timavg gs${x}01 tgs${x} _ _ gs${x}02 gs${x}03 gs${x}04 gs${x}05 gs${x}06 gs${x}07 gs${x}08 gs${x}09 gs${x}10 gs${x}11 gs${x}12
#                                      global
        globavg tgs${x} _ gtgs${x}g
#                                      land
        globavg tgs${x} _ gtgs${x}l tland_frac
#                                      ocean
        globavg tgs${x} _ gtgs${x}o ocean_frac

        rtdlist_ann="$rtdlist_ann gtgs${x}g gtgs${x}l gtgs${x}o"
        echo "C*XFIND       SCREEN TEMPERATURE MIN GLOBAL (K)            (ANN ${year_rtdiag_start}-${yearm1})
C*XFIND       SCREEN TEMPERATURE MIN LAND (K)              (ANN ${year_rtdiag_start}-${yearm1})
C*XFIND       SCREEN TEMPERATURE MIN OCEAN (K)             (ANN ${year_rtdiag_start}-${yearm1})" >> ic.xfind_ann
        echo "C*XSAVE       SCREEN TEMPERATURE MIN GLOBAL (K)            (ANN ${year_rtdiag_start}-${year})
NEWNAM     STMN
C*XSAVE       SCREEN TEMPERATURE MIN LAND (K)              (ANN ${year_rtdiag_start}-${year})
NEWNAM     STMN
C*XSAVE       SCREEN TEMPERATURE MIN OCEAN (K)             (ANN ${year_rtdiag_start}-${year})
NEWNAM     STMN" >> ic.xsave_ann

# ------------------------------------ annual STMN min

        x="STMN"
        timmin gs${x}01 tgs${x}min gs${x}02 gs${x}03 gs${x}04 gs${x}05 gs${x}06 gs${x}07 gs${x}08 gs${x}09 gs${x}10 gs${x}11 gs${x}12
#                                      global
        globavg tgs${x}min _ gtgs${x}ming
#                                      land
        globavg tgs${x}min _ gtgs${x}minl tland_frac
#                                      ocean
        globavg tgs${x}min _ gtgs${x}mino ocean_frac

        rtdlist_ann="$rtdlist_ann gtgs${x}ming gtgs${x}minl gtgs${x}mino"
        echo "C*XFIND       MIN SCREEN TEMPERATURE MIN GLOBAL (K)        (ANN ${year_rtdiag_start}-${yearm1})
C*XFIND       MIN SCREEN TEMPERATURE MIN LAND (K)          (ANN ${year_rtdiag_start}-${yearm1})
C*XFIND       MIN SCREEN TEMPERATURE MIN OCEAN (K)         (ANN ${year_rtdiag_start}-${yearm1})" >> ic.xfind_ann
        echo "C*XSAVE       MIN SCREEN TEMPERATURE MIN GLOBAL (K)        (ANN ${year_rtdiag_start}-${year})
NEWNAM     STMN
C*XSAVE       MIN SCREEN TEMPERATURE MIN LAND (K)          (ANN ${year_rtdiag_start}-${year})
NEWNAM     STMN
C*XSAVE       MIN SCREEN TEMPERATURE MIN OCEAN (K)         (ANN ${year_rtdiag_start}-${year})
NEWNAM     STMN" >> ic.xsave_ann

# ------------------------------------ annual SQ

        x="SQ"
        timavg gs${x}01 tgs${x} _ _ gs${x}02 gs${x}03 gs${x}04 gs${x}05 gs${x}06 gs${x}07 gs${x}08 gs${x}09 gs${x}10 gs${x}11 gs${x}12
#                                      global
        globavg tgs${x} _ gtgs${x}g
#                                      land
        globavg tgs${x} _ gtgs${x}l tland_frac
#                                      ocean
        globavg tgs${x} _ gtgs${x}o ocean_frac

        rtdlist_ann="$rtdlist_ann gtgs${x}g gtgs${x}l gtgs${x}o"
        echo "C*XFIND       SCREEN SPECIFIC HUMIDITY GLOBAL              (ANN ${year_rtdiag_start}-${yearm1})
C*XFIND       SCREEN SPECIFIC HUMIDITY LAND                (ANN ${year_rtdiag_start}-${yearm1})
C*XFIND       SCREEN SPECIFIC HUMIDITY OCEAN               (ANN ${year_rtdiag_start}-${yearm1})" >> ic.xfind_ann
        echo "C*XSAVE       SCREEN SPECIFIC HUMIDITY GLOBAL              (ANN ${year_rtdiag_start}-${year})
NEWNAM       SQ
C*XSAVE       SCREEN SPECIFIC HUMIDITY LAND                (ANN ${year_rtdiag_start}-${year})
NEWNAM       SQ
C*XSAVE       SCREEN SPECIFIC HUMIDITY OCEAN               (ANN ${year_rtdiag_start}-${year})
NEWNAM       SQ" >> ic.xsave_ann

# ------------------------------------ annual SRH

        x="SRH"
        timavg gs${x}01 tgs${x} _ _ gs${x}02 gs${x}03 gs${x}04 gs${x}05 gs${x}06 gs${x}07 gs${x}08 gs${x}09 gs${x}10 gs${x}11 gs${x}12
#                                      global
        globavg tgs${x} _ gtgs${x}g
#                                      land
        globavg tgs${x} _ gtgs${x}l tland_frac
#                                      ocean
        globavg tgs${x} _ gtgs${x}o ocean_frac

        rtdlist_ann="$rtdlist_ann gtgs${x}g gtgs${x}l gtgs${x}o"
        echo "C*XFIND       SCREEN RELATIVE HUMIDITY GLOBAL              (ANN ${year_rtdiag_start}-${yearm1})
C*XFIND       SCREEN RELATIVE HUMIDITY LAND                (ANN ${year_rtdiag_start}-${yearm1})
C*XFIND       SCREEN RELATIVE HUMIDITY OCEAN               (ANN ${year_rtdiag_start}-${yearm1})" >> ic.xfind_ann
        echo "C*XSAVE       SCREEN RELATIVE HUMIDITY GLOBAL              (ANN ${year_rtdiag_start}-${year})
NEWNAM      SRH
C*XSAVE       SCREEN RELATIVE HUMIDITY LAND                (ANN ${year_rtdiag_start}-${year})
NEWNAM      SRH
C*XSAVE       SCREEN RELATIVE HUMIDITY OCEAN               (ANN ${year_rtdiag_start}-${year})
NEWNAM      SRH" >> ic.xsave_ann

# ------------------------------------ annual GT

        x="GT"
        timavg gs${x}01 tgs${x} _ _ gs${x}02 gs${x}03 gs${x}04 gs${x}05 gs${x}06 gs${x}07 gs${x}08 gs${x}09 gs${x}10 gs${x}11 gs${x}12
#                                      global
        globavg tgs${x} _ gtgs${x}g
#                                      land
        globavg tgs${x} _ gtgs${x}l tland_frac
#                                      ocean
        globavg tgs${x} _ gtgs${x}o ocean_frac
#                                      lakes
        globavg tgs${x} _ gtgs${x}r rlake_frac
#                                      glaciers
        globavg tgs${x} _ gtgs${x}i glaci_frac

        rtdlist_ann="$rtdlist_ann gtgs${x}g gtgs${x}l gtgs${x}o gtgs${x}r gtgs${x}i"
        echo "C*XFIND       SKIN TEMPERATURE GLOBAL (K)                  (ANN ${year_rtdiag_start}-${yearm1})
C*XFIND       SKIN TEMPERATURE LAND (K)                    (ANN ${year_rtdiag_start}-${yearm1})
C*XFIND       SKIN TEMPERATURE OCEAN (K)                   (ANN ${year_rtdiag_start}-${yearm1})
C*XFIND       SKIN TEMPERATURE LAKES (K)                   (ANN ${year_rtdiag_start}-${yearm1})
C*XFIND       SKIN TEMPERATURE GLACIERS (K)                (ANN ${year_rtdiag_start}-${yearm1})" >> ic.xfind_ann
        echo "C*XSAVE       SKIN TEMPERATURE GLOBAL (K)                  (ANN ${year_rtdiag_start}-${year})
NEWNAM       GT
C*XSAVE       SKIN TEMPERATURE LAND (K)                    (ANN ${year_rtdiag_start}-${year})
NEWNAM       GT
C*XSAVE       SKIN TEMPERATURE OCEAN (K)                   (ANN ${year_rtdiag_start}-${year})
NEWNAM       GT
C*XSAVE       SKIN TEMPERATURE LAKES (K)                   (ANN ${year_rtdiag_start}-${year})
NEWNAM       GT
C*XSAVE       SKIN TEMPERATURE GLACIERS (K)                (ANN ${year_rtdiag_start}-${year})
NEWNAM       GT" >> ic.xsave_ann

# ------------------------------------ annual ocean GTT (land/sea water/sea ice tiles)

        x="GTT"
        timavg ggs${x}l01 tggs${x}l _ _ ggs${x}l02 ggs${x}l03 ggs${x}l04 ggs${x}l05 ggs${x}l06 ggs${x}l07 ggs${x}l08 ggs${x}l09 ggs${x}l10 ggs${x}l11 ggs${x}l12
        timavg ggs${x}w01 tggs${x}w _ _ ggs${x}w02 ggs${x}w03 ggs${x}w04 ggs${x}w05 ggs${x}w06 ggs${x}w07 ggs${x}w08 ggs${x}w09 ggs${x}w10 ggs${x}w11 ggs${x}w12
        timavg ggs${x}i01 tggs${x}i _ _ ggs${x}i02 ggs${x}i03 ggs${x}i04 ggs${x}i05 ggs${x}i06 ggs${x}i07 ggs${x}i08 ggs${x}i09 ggs${x}i10 ggs${x}i11 ggs${x}i12
        timavg ggs${x}o01 tggs${x}o _ _ ggs${x}o02 ggs${x}o03 ggs${x}o04 ggs${x}o05 ggs${x}o06 ggs${x}o07 ggs${x}o08 ggs${x}o09 ggs${x}o10 ggs${x}o11 ggs${x}o12

        rtdlist_ann="$rtdlist_ann tggs${x}l tggs${x}w tggs${x}i tggs${x}o"
        echo "C*XFIND       SKIN TEMPERATURE LAND TILE (K)               (ANN ${year_rtdiag_start}-${yearm1})
C*XFIND       SKIN TEMPERATURE OPEN WATER TILE (K)         (ANN ${year_rtdiag_start}-${yearm1})
C*XFIND       SKIN TEMPERATURE SEA ICE TILE (K)            (ANN ${year_rtdiag_start}-${yearm1})
C*XFIND       SKIN TEMPERATURE OCEAN TILES (K)             (ANN ${year_rtdiag_start}-${yearm1})" >> ic.xfind_ann
        echo "C*XSAVE       SKIN TEMPERATURE LAND TILE (K)               (ANN ${year_rtdiag_start}-${year})
NEWNAM      GTT
C*XSAVE       SKIN TEMPERATURE OPEN WATER TILE (K)         (ANN ${year_rtdiag_start}-${year})
NEWNAM      GTT
C*XSAVE       SKIN TEMPERATURE SEA ICE TILE (K)            (ANN ${year_rtdiag_start}-${year})
NEWNAM      GTT
C*XSAVE       SKIN TEMPERATURE OCEAN TILES (K)             (ANN ${year_rtdiag_start}-${year})
NEWNAM      GTT" >> ic.xsave_ann

# ------------------------------------ annual PCP

        x="PCP"
        timavg gs${x}01 tgs${x} _ _ gs${x}02 gs${x}03 gs${x}04 gs${x}05 gs${x}06 gs${x}07 gs${x}08 gs${x}09 gs${x}10 gs${x}11 gs${x}12
#                                      convert kg/m2/s or mm/s to mm/day by multiplying with 60x60x24=86400
        echo "C*XLIN        86400.       0.0" | ccc xlin tgs${x} tgs${x}.1 ; mv tgs${x}.1 tgs${x}
#                                      global
        globavg tgs${x} _ gtgs${x}g
#                                      land
        globavg tgs${x} _ gtgs${x}l tland_frac
#                                      land only
        globavg tgs${x} _ gtgs${x}ll lland_frac
#                                      ocean
        globavg tgs${x} _ gtgs${x}o ocean_frac
#                                      lakes
        globavg tgs${x} _ gtgs${x}r rlake_frac
#                                      land excluding glaciers
        globavg tgs${x} _ gtgs${x}a nogla_frac
#                                      glaciers
        globavg tgs${x} _ gtgs${x}i glaci_frac
#                                      bedrock
        globavg tgs${x} _ gtgs${x}b bedro_frac
#                                      land excluding Antarctica
        globavg tgs${x} _ gtgs${x}c noant_frac
#                                      NH extratropics
        globavg tgs${x} _ gtgs${x}n globe_frac_nhe
#                                      SH extratropics
        globavg tgs${x} _ gtgs${x}s globe_frac_she
#                                      Tropics
        globavg tgs${x} _ gtgs${x}t globe_frac_tro

        rtdlist_ann="$rtdlist_ann gtgs${x}g gtgs${x}l gtgs${x}ll gtgs${x}o gtgs${x}r gtgs${x}a gtgs${x}i gtgs${x}b gtgs${x}c gtgs${x}n gtgs${x}s gtgs${x}t"
        echo "C*XFIND       PRECIPITATION GLOBAL (MM/DAY)                (ANN ${year_rtdiag_start}-${yearm1})
C*XFIND       PRECIPITATION LAND (MM/DAY)                  (ANN ${year_rtdiag_start}-${yearm1})
C*XFIND       PRECIPITATION LAND ONLY (MM/DAY)             (ANN ${year_rtdiag_start}-${yearm1})
C*XFIND       PRECIPITATION OCEAN (MM/DAY)                 (ANN ${year_rtdiag_start}-${yearm1})
C*XFIND       PRECIPITATION LAKES (MM/DAY)                 (ANN ${year_rtdiag_start}-${yearm1})
C*XFIND       PRECIPITATION LAND EXCL.GLAC.(MM/DAY)        (ANN ${year_rtdiag_start}-${yearm1})
C*XFIND       PRECIPITATION GLACIERS (MM/DAY)              (ANN ${year_rtdiag_start}-${yearm1})
C*XFIND       PRECIPITATION BEDROCK (MM/DAY)               (ANN ${year_rtdiag_start}-${yearm1})
C*XFIND       PRECIPITATION LAND EXCL.ANTARCTICA(MM/DAY)   (ANN ${year_rtdiag_start}-${yearm1})
C*XFIND       PRECIPITATION 30N-90N (MM/DAY)               (ANN ${year_rtdiag_start}-${yearm1})
C*XFIND       PRECIPITATION 30S-90S (MM/DAY)               (ANN ${year_rtdiag_start}-${yearm1})
C*XFIND       PRECIPITATION 30S-30N (MM/DAY)               (ANN ${year_rtdiag_start}-${yearm1})" >> ic.xfind_ann
        echo "C*XSAVE       PRECIPITATION GLOBAL (MM/DAY)                (ANN ${year_rtdiag_start}-${year})
NEWNAM      PCP
C*XSAVE       PRECIPITATION LAND (MM/DAY)                  (ANN ${year_rtdiag_start}-${year})
NEWNAM      PCP
C*XSAVE       PRECIPITATION LAND ONLY (MM/DAY)             (ANN ${year_rtdiag_start}-${year})
NEWNAM      PCP
C*XSAVE       PRECIPITATION OCEAN (MM/DAY)                 (ANN ${year_rtdiag_start}-${year})
NEWNAM      PCP
C*XSAVE       PRECIPITATION LAKES (MM/DAY)                 (ANN ${year_rtdiag_start}-${year})
NEWNAM      PCP
C*XSAVE       PRECIPITATION LAND EXCL.GLAC.(MM/DAY)        (ANN ${year_rtdiag_start}-${year})
NEWNAM      PCP
C*XSAVE       PRECIPITATION GLACIERS (MM/DAY)              (ANN ${year_rtdiag_start}-${year})
NEWNAM      PCP
C*XSAVE       PRECIPITATION BEDROCK (MM/DAY)               (ANN ${year_rtdiag_start}-${year})
NEWNAM      PCP
C*XSAVE       PRECIPITATION LAND EXCL.ANTARCTICA(MM/DAY)   (ANN ${year_rtdiag_start}-${year})
NEWNAM      PCP
C*XSAVE       PRECIPITATION 30N-90N (MM/DAY)               (ANN ${year_rtdiag_start}-${year})
NEWNAM      PCP
C*XSAVE       PRECIPITATION 30S-90S (MM/DAY)               (ANN ${year_rtdiag_start}-${year})
NEWNAM      PCP
C*XSAVE       PRECIPITATION 30S-30N (MM/DAY)               (ANN ${year_rtdiag_start}-${year})
NEWNAM      PCP" >> ic.xsave_ann

# ------------------------------------ annual PCP max

        x="PCP"
        timmax gs${x}01 tgs${x}max gs${x}02 gs${x}03 gs${x}04 gs${x}05 gs${x}06 gs${x}07 gs${x}08 gs${x}09 gs${x}10 gs${x}11 gs${x}12
#                                      convert kg/m2/s or mm/s to mm/day by multiplying with 60x60x24=86400
        echo "C*XLIN        86400.       0.0" | ccc xlin tgs${x}max tgs${x}max.1 ; mv tgs${x}max.1 tgs${x}max
#                                      global
        globavg tgs${x}max _ gtgs${x}maxg
#                                      land
        globavg tgs${x}max _ gtgs${x}maxl tland_frac
#                                      ocean
        globavg tgs${x}max _ gtgs${x}maxo ocean_frac
#                                      NH extratropics
        globavg tgs${x}max _ gtgs${x}maxn globe_frac_nhe
#                                      SH extratropics
        globavg tgs${x}max _ gtgs${x}maxs globe_frac_she
#                                      Tropics
        globavg tgs${x}max _ gtgs${x}maxt globe_frac_tro

        rtdlist_ann="$rtdlist_ann gtgs${x}maxg gtgs${x}maxl gtgs${x}maxo gtgs${x}maxn gtgs${x}maxs gtgs${x}maxt"
        echo "C*XFIND       MAX PRECIPITATION GLOBAL (MM/DAY)            (ANN ${year_rtdiag_start}-${yearm1})
C*XFIND       MAX PRECIPITATION LAND (MM/DAY)              (ANN ${year_rtdiag_start}-${yearm1})
C*XFIND       MAX PRECIPITATION OCEAN (MM/DAY)             (ANN ${year_rtdiag_start}-${yearm1})
C*XFIND       MAX PRECIPITATION 30N-90N (MM/DAY)           (ANN ${year_rtdiag_start}-${yearm1})
C*XFIND       MAX PRECIPITATION 30S-90S (MM/DAY)           (ANN ${year_rtdiag_start}-${yearm1})
C*XFIND       MAX PRECIPITATION 30S-30N (MM/DAY)           (ANN ${year_rtdiag_start}-${yearm1})" >> ic.xfind_ann
        echo "C*XSAVE       MAX PRECIPITATION GLOBAL (MM/DAY)            (ANN ${year_rtdiag_start}-${year})
NEWNAM      PCP
C*XSAVE       MAX PRECIPITATION LAND (MM/DAY)              (ANN ${year_rtdiag_start}-${year})
NEWNAM      PCP
C*XSAVE       MAX PRECIPITATION OCEAN (MM/DAY)             (ANN ${year_rtdiag_start}-${year})
NEWNAM      PCP
C*XSAVE       MAX PRECIPITATION 30N-90N (MM/DAY)           (ANN ${year_rtdiag_start}-${year})
NEWNAM      PCP
C*XSAVE       MAX PRECIPITATION 30S-90S (MM/DAY)           (ANN ${year_rtdiag_start}-${year})
NEWNAM      PCP
C*XSAVE       MAX PRECIPITATION 30S-30N (MM/DAY)           (ANN ${year_rtdiag_start}-${year})
NEWNAM      PCP" >> ic.xsave_ann

# ------------------------------------ annual PCPC

        x="PCPC"
        timavg gs${x}01 tgs${x} _ _ gs${x}02 gs${x}03 gs${x}04 gs${x}05 gs${x}06 gs${x}07 gs${x}08 gs${x}09 gs${x}10 gs${x}11 gs${x}12
#                                      convert kg/m2/s or mm/s to mm/day by multiplying with 60x60x24=86400
        echo "C*XLIN        86400.       0.0" | ccc xlin tgs${x} tgs${x}.1 ; mv tgs${x}.1 tgs${x}
#                                      global
        globavg tgs${x} _ gtgs${x}g
#                                      land
        globavg tgs${x} _ gtgs${x}l tland_frac
#                                      ocean
        globavg tgs${x} _ gtgs${x}o ocean_frac
#                                      NH extratropics
        globavg tgs${x} _ gtgs${x}n globe_frac_nhe
#                                      SH extratropics
        globavg tgs${x} _ gtgs${x}s globe_frac_she
#                                      Tropics
        globavg tgs${x} _ gtgs${x}t globe_frac_tro

        rtdlist_ann="$rtdlist_ann gtgs${x}g gtgs${x}l gtgs${x}o gtgs${x}n gtgs${x}s gtgs${x}t"
        echo "C*XFIND       CONV. PRECIPITATION GLOBAL (MM/DAY)          (ANN ${year_rtdiag_start}-${yearm1})
C*XFIND       CONV. PRECIPITATION LAND (MM/DAY)            (ANN ${year_rtdiag_start}-${yearm1})
C*XFIND       CONV. PRECIPITATION OCEAN (MM/DAY)           (ANN ${year_rtdiag_start}-${yearm1})
C*XFIND       CONV. PRECIPITATION 30N-90N (MM/DAY)         (ANN ${year_rtdiag_start}-${yearm1})
C*XFIND       CONV. PRECIPITATION 30S-90S (MM/DAY)         (ANN ${year_rtdiag_start}-${yearm1})
C*XFIND       CONV. PRECIPITATION 30S-30N (MM/DAY)         (ANN ${year_rtdiag_start}-${yearm1})" >> ic.xfind_ann
        echo "C*XSAVE       CONV. PRECIPITATION GLOBAL (MM/DAY)          (ANN ${year_rtdiag_start}-${year})
NEWNAM     PCPC
C*XSAVE       CONV. PRECIPITATION LAND (MM/DAY)            (ANN ${year_rtdiag_start}-${year})
NEWNAM     PCPC
C*XSAVE       CONV. PRECIPITATION OCEAN (MM/DAY)           (ANN ${year_rtdiag_start}-${year})
NEWNAM     PCPC
C*XSAVE       CONV. PRECIPITATION 30N-90N (MM/DAY)         (ANN ${year_rtdiag_start}-${year})
NEWNAM     PCPC
C*XSAVE       CONV. PRECIPITATION 30S-90S (MM/DAY)         (ANN ${year_rtdiag_start}-${year})
NEWNAM     PCPC
C*XSAVE       CONV. PRECIPITATION 30S-30N (MM/DAY)         (ANN ${year_rtdiag_start}-${year})
NEWNAM     PCPC" >> ic.xsave_ann

# ------------------------------------ annual snowfall PCPN

        x="PCPN"
        timavg gs${x}01 tgs${x} _ _ gs${x}02 gs${x}03 gs${x}04 gs${x}05 gs${x}06 gs${x}07 gs${x}08 gs${x}09 gs${x}10 gs${x}11 gs${x}12
#                                      convert kg/m2/s or mm/s to mm/day by multiplying with 60x60x24=86400
        echo "C*XLIN        86400.       0.0" | ccc xlin tgs${x} tgs${x}.1 ; mv tgs${x}.1 tgs${x}
#                                      global
        globavg tgs${x} _ gtgs${x}g
#                                      land
        globavg tgs${x} _ gtgs${x}l tland_frac
#                                      ocean
        globavg tgs${x} _ gtgs${x}o ocean_frac
#                                      lakes
        globavg tgs${x} _ gtgs${x}r rlake_frac
#                                      land excluding glaciers
        globavg tgs${x} _ gtgs${x}a nogla_frac
#                                      glaciers
        globavg tgs${x} _ gtgs${x}i glaci_frac
#                                      bedrock
        globavg tgs${x} _ gtgs${x}b bedro_frac
#                                      NH extratropics
        globavg tgs${x} _ gtgs${x}n globe_frac_nhe
#                                      SH extratropics
        globavg tgs${x} _ gtgs${x}s globe_frac_she
#                                      Tropics
        globavg tgs${x} _ gtgs${x}t globe_frac_tro

        rtdlist_ann="$rtdlist_ann gtgs${x}g gtgs${x}l gtgs${x}o gtgs${x}r gtgs${x}a gtgs${x}i gtgs${x}b gtgs${x}n gtgs${x}s gtgs${x}t"
        echo "C*XFIND       SNOWFALL RATE GLOBAL (MM/DAY)                (ANN ${year_rtdiag_start}-${yearm1})
C*XFIND       SNOWFALL RATE LAND (MM/DAY)                  (ANN ${year_rtdiag_start}-${yearm1})
C*XFIND       SNOWFALL RATE OCEAN (MM/DAY)                 (ANN ${year_rtdiag_start}-${yearm1})
C*XFIND       SNOWFALL RATE LAKES (MM/DAY)                 (ANN ${year_rtdiag_start}-${yearm1})
C*XFIND       SNOWFALL RATE LAND EXCL.GLAC.(MM/DAY)        (ANN ${year_rtdiag_start}-${yearm1})
C*XFIND       SNOWFALL RATE GLACIERS (MM/DAY)              (ANN ${year_rtdiag_start}-${yearm1})
C*XFIND       SNOWFALL RATE BEDROCK (MM/DAY)               (ANN ${year_rtdiag_start}-${yearm1})
C*XFIND       SNOWFALL RATE 30N-90N (MM/DAY)               (ANN ${year_rtdiag_start}-${yearm1})
C*XFIND       SNOWFALL RATE 30S-90S (MM/DAY)               (ANN ${year_rtdiag_start}-${yearm1})
C*XFIND       SNOWFALL RATE 30S-30N (MM/DAY)               (ANN ${year_rtdiag_start}-${yearm1})" >> ic.xfind_ann
        echo "C*XSAVE       SNOWFALL RATE GLOBAL (MM/DAY)                (ANN ${year_rtdiag_start}-${year})
NEWNAM     PCPN
C*XSAVE       SNOWFALL RATE LAND (MM/DAY)                  (ANN ${year_rtdiag_start}-${year})
NEWNAM     PCPN
C*XSAVE       SNOWFALL RATE OCEAN (MM/DAY)                 (ANN ${year_rtdiag_start}-${year})
NEWNAM     PCPN
C*XSAVE       SNOWFALL RATE LAKES (MM/DAY)                 (ANN ${year_rtdiag_start}-${year})
NEWNAM     PCPN
C*XSAVE       SNOWFALL RATE LAND EXCL.GLAC.(MM/DAY)        (ANN ${year_rtdiag_start}-${year})
NEWNAM     PCPN
C*XSAVE       SNOWFALL RATE GLACIERS (MM/DAY)              (ANN ${year_rtdiag_start}-${year})
NEWNAM     PCPN
C*XSAVE       SNOWFALL RATE BEDROCK (MM/DAY)               (ANN ${year_rtdiag_start}-${year})
NEWNAM     PCPN
C*XSAVE       SNOWFALL RATE 30N-90N (MM/DAY)               (ANN ${year_rtdiag_start}-${year})
NEWNAM     PCPN
C*XSAVE       SNOWFALL RATE 30S-90S (MM/DAY)               (ANN ${year_rtdiag_start}-${year})
NEWNAM     PCPN
C*XSAVE       SNOWFALL RATE 30S-30N (MM/DAY)               (ANN ${year_rtdiag_start}-${year})
NEWNAM     PCPN" >> ic.xsave_ann

# ------------------------------------ annual snowfall PCPN over tiles

        x="PCPN"
#                                      time average
        timavg ggs${x}lg01 tggs${x}lg _ _ ggs${x}lg02 ggs${x}lg03 ggs${x}lg04 ggs${x}lg05 ggs${x}lg06 ggs${x}lg07 ggs${x}lg08 ggs${x}lg09 ggs${x}lg10 ggs${x}lg11 ggs${x}lg12
        timavg ggs${x}wg01 tggs${x}wg _ _ ggs${x}wg02 ggs${x}wg03 ggs${x}wg04 ggs${x}wg05 ggs${x}wg06 ggs${x}wg07 ggs${x}wg08 ggs${x}wg09 ggs${x}wg10 ggs${x}wg11 ggs${x}wg12
        timavg ggs${x}ig01 tggs${x}ig _ _ ggs${x}ig02 ggs${x}ig03 ggs${x}ig04 ggs${x}ig05 ggs${x}ig06 ggs${x}ig07 ggs${x}ig08 ggs${x}ig09 ggs${x}ig10 ggs${x}ig11 ggs${x}ig12
	add tggs${x}wg tggs${x}ig tggs${x}og
	add tggs${x}lg tggs${x}og tggs${x}gg

        rtdlist_ann="$rtdlist_ann tggs${x}lg tggs${x}wg tggs${x}ig tggs${x}og tggs${x}gg"
        echo "C*XFIND       GLOBAL SNOWFALL RATE LAND (KG/M2/S)          (ANN ${year_rtdiag_start}-${yearm1})
C*XFIND       GLOBAL SNOWFALL RATE WATER (KG/M2/S)         (ANN ${year_rtdiag_start}-${yearm1})
C*XFIND       GLOBAL SNOWFALL RATE SEAICE (KG/M2/S)        (ANN ${year_rtdiag_start}-${yearm1})
C*XFIND       GLOBAL SNOWFALL RATE OCEAN (KG/M2/S)         (ANN ${year_rtdiag_start}-${yearm1})
C*XFIND       GLOBAL SNOWFALL RATE GLOBE (KG/M2/S)         (ANN ${year_rtdiag_start}-${yearm1})" >> ic.xfind_ann
        echo "C*XSAVE       GLOBAL SNOWFALL RATE LAND (KG/M2/S)          (ANN ${year_rtdiag_start}-${year})
NEWNAM     PCPN
C*XSAVE       GLOBAL SNOWFALL RATE WATER (KG/M2/S)         (ANN ${year_rtdiag_start}-${year})
NEWNAM     PCPN
C*XSAVE       GLOBAL SNOWFALL RATE SEAICE (KG/M2/S)        (ANN ${year_rtdiag_start}-${year})
NEWNAM     PCPN
C*XSAVE       GLOBAL SNOWFALL RATE OCEAN (KG/M2/S)         (ANN ${year_rtdiag_start}-${year})
NEWNAM     PCPN
C*XSAVE       GLOBAL SNOWFALL RATE GLOBE (KG/M2/S)         (ANN ${year_rtdiag_start}-${year})
NEWNAM     PCPN" >> ic.xsave_ann

# ------------------------------------ annual evaporation QFS

        x="QFS"
        timavg gs${x}01  tgs${x}  _ _ gs${x}02  gs${x}03  gs${x}04  gs${x}05  gs${x}06  gs${x}07  gs${x}08  gs${x}09  gs${x}10  gs${x}11  gs${x}12
        timavg gs${x}L01 tgs${x}L _ _ gs${x}L02 gs${x}L03 gs${x}L04 gs${x}L05 gs${x}L06 gs${x}L07 gs${x}L08 gs${x}L09 gs${x}L10 gs${x}L11 gs${x}L12
        timavg gs${x}O01 tgs${x}O _ _ gs${x}O02 gs${x}O03 gs${x}O04 gs${x}O05 gs${x}O06 gs${x}O07 gs${x}O08 gs${x}O09 gs${x}O10 gs${x}O11 gs${x}O12
#                                      convert kg/m2/s or mm/s to mm/day by multiplying with 60x60x24=86400
        echo "C*XLIN        86400.       0.0" | ccc xlin tgs${x}  tgs${x}.1  ; mv tgs${x}.1  tgs${x}
        echo "C*XLIN        86400.       0.0" | ccc xlin tgs${x}L tgs${x}L.1 ; mv tgs${x}L.1 tgs${x}L
        echo "C*XLIN        86400.       0.0" | ccc xlin tgs${x}O tgs${x}O.1 ; mv tgs${x}O.1 tgs${x}O
#                                      global
        globavg tgs${x} _ gtgs${x}g
#                                      land
        globavg tgs${x}L _ gtgs${x}l tland_frac
#                                      land only
        globavg tgs${x}L _ gtgs${x}ll lland_frac
#                                      ocean
        globavg tgs${x}O _ gtgs${x}o ocean_frac
#                                      lakes
        globavg tgs${x}L _ gtgs${x}r rlake_frac
#                                      land excluding glaciers
        globavg tgs${x}L _ gtgs${x}n nogla_frac
#                                      land excluding glaciers/bedrock
        globavg tgs${x}L _ gtgs${x}d noglb_frac
#                                      glaciers
        globavg tgs${x}L _ gtgs${x}i glaci_frac
#                                      bedrock
        globavg tgs${x}L _ gtgs${x}b bedro_frac

        rtdlist_ann="$rtdlist_ann gtgs${x}g gtgs${x}l gtgs${x}ll gtgs${x}o gtgs${x}r gtgs${x}n gtgs${x}d gtgs${x}i gtgs${x}b"
        echo "C*XFIND       EVAPORATION GLOBAL (MM/DAY)                  (ANN ${year_rtdiag_start}-${yearm1})
C*XFIND       EVAPORATION LAND (MM/DAY)                    (ANN ${year_rtdiag_start}-${yearm1})
C*XFIND       EVAPORATION LAND ONLY (MM/DAY)               (ANN ${year_rtdiag_start}-${yearm1})
C*XFIND       EVAPORATION OCEAN (MM/DAY)                   (ANN ${year_rtdiag_start}-${yearm1})
C*XFIND       EVAPORATION LAKES (MM/DAY)                   (ANN ${year_rtdiag_start}-${yearm1})
C*XFIND       EVAPORATION LAND EXCL.GLAC.(MM/DAY)          (ANN ${year_rtdiag_start}-${yearm1})
C*XFIND       EVAPORATION LAND EXCL.GLAC./BEDR.(MM/DAY)    (ANN ${year_rtdiag_start}-${yearm1})
C*XFIND       EVAPORATION GLACIERS (MM/DAY)                (ANN ${year_rtdiag_start}-${yearm1})
C*XFIND       EVAPORATION BEDROCK (MM/DAY)                 (ANN ${year_rtdiag_start}-${yearm1})" >> ic.xfind_ann
        echo "C*XSAVE       EVAPORATION GLOBAL (MM/DAY)                  (ANN ${year_rtdiag_start}-${year})
NEWNAM      QFS
C*XSAVE       EVAPORATION LAND (MM/DAY)                    (ANN ${year_rtdiag_start}-${year})
NEWNAM      QFS
C*XSAVE       EVAPORATION LAND ONLY (MM/DAY)               (ANN ${year_rtdiag_start}-${year})
NEWNAM      QFS
C*XSAVE       EVAPORATION OCEAN (MM/DAY)                   (ANN ${year_rtdiag_start}-${year})
NEWNAM      QFS
C*XSAVE       EVAPORATION LAKES (MM/DAY)                   (ANN ${year_rtdiag_start}-${year})
NEWNAM      QFS
C*XSAVE       EVAPORATION LAND EXCL.GLAC.(MM/DAY)          (ANN ${year_rtdiag_start}-${year})
NEWNAM      QFS
C*XSAVE       EVAPORATION LAND EXCL.GLAC./BEDR.(MM/DAY)    (ANN ${year_rtdiag_start}-${year})
NEWNAM      QFS
C*XSAVE       EVAPORATION GLACIERS (MM/DAY)                (ANN ${year_rtdiag_start}-${year})
NEWNAM      QFS
C*XSAVE       EVAPORATION BEDROCK (MM/DAY)                 (ANN ${year_rtdiag_start}-${year})
NEWNAM      QFS" >> ic.xsave_ann

# ------------------------------------ annual ROF

        x="ROF"
        timavg gs${x}01 tgs${x} _ _ gs${x}02 gs${x}03 gs${x}04 gs${x}05 gs${x}06 gs${x}07 gs${x}08 gs${x}09 gs${x}10 gs${x}11 gs${x}12
#                                      convert kg/m2/s or mm/s to mm/day by multiplying with 60x60x24=86400
        echo "C*XLIN        86400.       0.0" | ccc xlin tgs${x} tgs${x}.1 ; mv tgs${x}.1 tgs${x}
#                                      land
        globavg tgs${x} _ gtgs${x}l tland_frac
#                                      land only
        globavg tgs${x} _ gtgs${x}ll lland_frac
#                                      land excluding glaciers
        globavg tgs${x} _ gtgs${x}n nogla_frac
#                                      land excluding glaciers/bedrock
        globavg tgs${x} _ gtgs${x}d noglb_frac
#                                      glaciers
        globavg tgs${x} _ gtgs${x}i glaci_frac
#                                      bedrock
        globavg tgs${x} _ gtgs${x}b bedro_frac

        rtdlist_ann="$rtdlist_ann gtgs${x}l gtgs${x}ll gtgs${x}n gtgs${x}d gtgs${x}i gtgs${x}b"
        echo "C*XFIND       RUNOFF LAND (MM/DAY)                         (ANN ${year_rtdiag_start}-${yearm1})
C*XFIND       RUNOFF LAND ONLY (MM/DAY)                    (ANN ${year_rtdiag_start}-${yearm1})
C*XFIND       RUNOFF LAND EXCL.GLAC.(MM/DAY)               (ANN ${year_rtdiag_start}-${yearm1})
C*XFIND       RUNOFF LAND EXCL.GLAC./BEDR.(MM/DAY)         (ANN ${year_rtdiag_start}-${yearm1})
C*XFIND       RUNOFF GLACIERS (MM/DAY)                     (ANN ${year_rtdiag_start}-${yearm1})
C*XFIND       RUNOFF BEDROCK (MM/DAY)                      (ANN ${year_rtdiag_start}-${yearm1})" >> ic.xfind_ann
        echo "C*XSAVE       RUNOFF LAND (MM/DAY)                         (ANN ${year_rtdiag_start}-${year})
NEWNAM      ROF
C*XSAVE       RUNOFF LAND ONLY (MM/DAY)                    (ANN ${year_rtdiag_start}-${year})
NEWNAM      ROF
C*XSAVE       RUNOFF LAND EXCL.GLAC.(MM/DAY)               (ANN ${year_rtdiag_start}-${year})
NEWNAM      ROF
C*XSAVE       RUNOFF LAND EXCL.GLAC./BEDR.(MM/DAY)         (ANN ${year_rtdiag_start}-${year})
NEWNAM      ROF
C*XSAVE       RUNOFF GLACIERS (MM/DAY)                     (ANN ${year_rtdiag_start}-${year})
NEWNAM      ROF
C*XSAVE       RUNOFF BEDROCK (MM/DAY)                      (ANN ${year_rtdiag_start}-${year})
NEWNAM      ROF" >> ic.xsave_ann

# ------------------------------------ annual RIVO

        x="RIVO"
        if [ -s gs${x}01 ] ; then
        timavg gs${x}01 tgs${x} _ _ gs${x}02 gs${x}03 gs${x}04 gs${x}05 gs${x}06 gs${x}07 gs${x}08 gs${x}09 gs${x}10 gs${x}11 gs${x}12
#                                      ocean
        globavg tgs${x} _ gtgs${x}o ocean_frac
#                                      convert kg/m2/s or mm/s to mm/day by multiplying with 60x60x24=86400
        echo "C*XLIN        86400.       0.0" | ccc xlin gtgs${x}o gtgs${x}o.1 ; mv gtgs${x}o.1 gtgs${x}o

        rtdlist_ann="$rtdlist_ann gtgs${x}o"
        echo "C*XFIND       RIVER DISCHARGE (MM/DAY)                     (ANN ${year_rtdiag_start}-${yearm1})" >> ic.xfind_ann
        echo "C*XSAVE       RIVER DISCHARGE (MM/DAY)                     (ANN ${year_rtdiag_start}-${year})
NEWNAM     RIVO" >> ic.xsave_ann
        fi

# ------------------------------------ annual (P-E+R) ocean

        x1="PCP"
        x2="QFS"
        x3="RIVO"
        x="PMER"
        if [ -s gtgs${x1}o -a -s gtgs${x2}o -a -s gtgs${x3}o ] ; then
        sub gtgs${x1}o gtgs${x2}o gtgsPMEo  # P-E over ocean
        add gtgsPMEo   gtgs${x3}o gtgs${x}o # P-E+R over ocean

        rtdlist_ann="$rtdlist_ann gtgs${x}o"
        echo "C*XFIND       OCEAN P-E PLUS RUNOFF (MM/DAY)               (ANN ${year_rtdiag_start}-${yearm1})" >> ic.xfind_ann
        echo "C*XSAVE       OCEAN P-E PLUS RUNOFF (MM/DAY)               (ANN ${year_rtdiag_start}-${year})
NEWNAM     PMER" >> ic.xsave_ann
        fi

# ------------------------------------ ocean annual ice mass (SIC = kg)

        x="SIC"
        timavg gs${x}01 tgs${x} _ _ gs${x}02 gs${x}03 gs${x}04 gs${x}05 gs${x}06 gs${x}07 gs${x}08 gs${x}09 gs${x}10 gs${x}11 gs${x}12
#                                      global
        globavg tgs${x} _ gtavg water_frac
#                                      convert kg/m2 to kg
        gmlt gtavg water_area gtgs${x}g
#                                      ocean
        globavg tgs${x} _ gtavg ocean_frac
#                                      convert kg/m2 to kg
        gmlt gtavg ocean_area gtgs${x}o
#                                      lakes
        globavg tgs${x} _ gtavg rlake_frac
#                                      convert kg/m2 to kg
        gmlt gtavg rlake_area gtgs${x}r

        rtdlist_ann="$rtdlist_ann gtgs${x}g gtgs${x}o gtgs${x}r"
        echo "C*XFIND       TOTAL SEAICE MASS (KG)                       (ANN ${year_rtdiag_start}-${yearm1})
C*XFIND       OCEAN SEAICE MASS (KG)                       (ANN ${year_rtdiag_start}-${yearm1})
C*XFIND       LAKES SEAICE MASS (KG)                       (ANN ${year_rtdiag_start}-${yearm1})" >> ic.xfind_ann
        echo "C*XSAVE       TOTAL SEAICE MASS (KG)                       (ANN ${year_rtdiag_start}-${year})
NEWNAM      SIC
C*XSAVE       OCEAN SEAICE MASS (KG)                       (ANN ${year_rtdiag_start}-${year})
NEWNAM      SIC
C*XSAVE       LAKES SEAICE MASS (KG)                       (ANN ${year_rtdiag_start}-${year})
NEWNAM      SIC" >> ic.xsave_ann

# ------------------------------------ annual soil temperature (TG)

        x="TG"
        timavg gs${x}01 tgs${x} _ _ gs${x}02 gs${x}03 gs${x}04 gs${x}05 gs${x}06 gs${x}07 gs${x}08 gs${x}09 gs${x}10 gs${x}11 gs${x}12

#                                      land
        globavg tgs${x} _ gtgs${x}l tland_frac
#                                      land excluding glaciers
        globavg tgs${x} _ gtgs${x}n nogla_frac

        rtdlist_ann="$rtdlist_ann gtgs${x}l gtgs${x}n"
        echo "C*XFIND       SOIL TEMPERATURE LAND (K)                    (ANN ${year_rtdiag_start}-${yearm1})
C*XFIND       SOIL TEMPERATURE EXCL.GLACIERS (K)           (ANN ${year_rtdiag_start}-${yearm1})" >> ic.xfind_ann
        echo "C*XSAVE       SOIL TEMPERATURE LAND (K)                    (ANN ${year_rtdiag_start}-${year})
NEWNAM       TG
C*XSAVE       SOIL TEMPERATURE EXCL.GLACIERS (K)           (ANN ${year_rtdiag_start}-${year})
NEWNAM       TG" >> ic.xsave_ann

# ------------------------------------ annual canopy temperature (TV)

        x="TV"
        if [ -s gs${x}01 ] ; then
#                                      masked time mean (only where TV>0)
        y="${x}msk"
        timavg gs${x}01 tgs${x} _ _ gs${x}02 gs${x}03 gs${x}04 gs${x}05 gs${x}06 gs${x}07 gs${x}08 gs${x}09 gs${x}10 gs${x}11 gs${x}12
        timavg gs${y}01 tgs${y} _ _ gs${y}02 gs${y}03 gs${y}04 gs${y}05 gs${y}06 gs${y}07 gs${y}08 gs${y}09 gs${y}10 gs${y}11 gs${y}12

#                                      masked spatial mean
        globavg tgs${x} _ gtgs${x}
        globavg tgs${y} _ gtgs${y}
        div gtgs${x} gtgs${y} gtgs${x}.1 ; mv gtgs${x}.1 gtgs${x}

        rtdlist_ann="$rtdlist_ann gtgs${x}"
        echo "C*XFIND       CANOPY TEMPERATURE LAND (K)                  (ANN ${year_rtdiag_start}-${yearm1})" >> ic.xfind_ann
        echo "C*XSAVE       CANOPY TEMPERATURE LAND (K)                  (ANN ${year_rtdiag_start}-${year})
NEWNAM       TV" >> ic.xsave_ann
        fi

# ------------------------------------ annual liquid soil moisture (WGL)

        x="WGL"
        timavg gs${x}01 tgs${x} _ _ gs${x}02 gs${x}03 gs${x}04 gs${x}05 gs${x}06 gs${x}07 gs${x}08 gs${x}09 gs${x}10 gs${x}11 gs${x}12

#                                      land
        globavg tgs${x} _ gtgs${x}l tland_frac
#                                      land only
        globavg tgs${x} _ gtgs${x}ll lland_frac
#                                      land excluding glaciers
        globavg tgs${x} _ gtgs${x}n nogla_frac
#                                      land excluding glaciers/bedrock
        globavg tgs${x} _ gtgs${x}d noglb_frac

        rtdlist_ann="$rtdlist_ann gtgs${x}l gtgs${x}ll gtgs${x}n gtgs${x}d"
        echo "C*XFIND       LIQUID SOIL MOISTURE LAND (KG/M2)            (ANN ${year_rtdiag_start}-${yearm1})
C*XFIND       LIQUID SOIL MOISTURE LAND ONLY (KG/M2)       (ANN ${year_rtdiag_start}-${yearm1})
C*XFIND       LIQUID SOIL MOISTURE EXCL.GLAC.(KG/M2)       (ANN ${year_rtdiag_start}-${yearm1})
C*XFIND       LIQUID SOIL MOISTURE EXCL.GLAC./BEDR.(KG/M2) (ANN ${year_rtdiag_start}-${yearm1})" >> ic.xfind_ann
        echo "C*XSAVE       LIQUID SOIL MOISTURE LAND (KG/M2)            (ANN ${year_rtdiag_start}-${year})
NEWNAM      WGL
C*XSAVE       LIQUID SOIL MOISTURE LAND ONLY (KG/M2)       (ANN ${year_rtdiag_start}-${year})
NEWNAM      WGL
C*XSAVE       LIQUID SOIL MOISTURE EXCL.GLAC.(KG/M2)       (ANN ${year_rtdiag_start}-${year})
NEWNAM      WGL
C*XSAVE       LIQUID SOIL MOISTURE EXCL.GLAC./BEDR.(KG/M2) (ANN ${year_rtdiag_start}-${year})
NEWNAM      WGL" >> ic.xsave_ann

# ------------------------------------ annual frozen soil moisture (WGF)

        x="WGF"
        timavg gs${x}01 tgs${x} _ _ gs${x}02 gs${x}03 gs${x}04 gs${x}05 gs${x}06 gs${x}07 gs${x}08 gs${x}09 gs${x}10 gs${x}11 gs${x}12

#                                      land
        globavg tgs${x} _ gtgs${x}l tland_frac
#                                      land only
        globavg tgs${x} _ gtgs${x}ll lland_frac
#                                      land excluding glaciers
        globavg tgs${x} _ gtgs${x}n nogla_frac
#                                      land excluding glaciers/bedrock
        globavg tgs${x} _ gtgs${x}d noglb_frac

        rtdlist_ann="$rtdlist_ann gtgs${x}l gtgs${x}ll gtgs${x}n gtgs${x}d"
        echo "C*XFIND       FROZEN SOIL MOISTURE LAND (KG/M2)            (ANN ${year_rtdiag_start}-${yearm1})
C*XFIND       FROZEN SOIL MOISTURE LAND ONLY (KG/M2)       (ANN ${year_rtdiag_start}-${yearm1})
C*XFIND       FROZEN SOIL MOISTURE EXCL.GLAC.(KG/M2)       (ANN ${year_rtdiag_start}-${yearm1})
C*XFIND       FROZEN SOIL MOISTURE EXCL.GLAC./BEDR.(KG/M2) (ANN ${year_rtdiag_start}-${yearm1})" >> ic.xfind_ann
        echo "C*XSAVE       FROZEN SOIL MOISTURE LAND (KG/M2)            (ANN ${year_rtdiag_start}-${year})
NEWNAM      WGF
C*XSAVE       FROZEN SOIL MOISTURE LAND ONLY (KG/M2)       (ANN ${year_rtdiag_start}-${year})
NEWNAM      WGF
C*XSAVE       FROZEN SOIL MOISTURE EXCL.GLAC.(KG/M2)       (ANN ${year_rtdiag_start}-${year})
NEWNAM      WGF
C*XSAVE       FROZEN SOIL MOISTURE EXCL.GLAC./BEDR.(KG/M2) (ANN ${year_rtdiag_start}-${year})
NEWNAM      WGF" >> ic.xsave_ann

# ------------------------------------ annual total (liquid + frozen) soil moisture (WGF+WGL)

        x="WGLF"
        add tgsWGL tgsWGF tgs${x}
#                                      land
        globavg tgs${x} _ gtgs${x}l tland_frac
#                                      land only
        globavg tgs${x} _ gtgs${x}ll lland_frac
#                                      land excluding glaciers
        globavg tgs${x} _ gtgs${x}n nogla_frac
#                                      land excluding glaciers/bedrock
        globavg tgs${x} _ gtgs${x}d noglb_frac

        rtdlist_ann="$rtdlist_ann gtgs${x}l gtgs${x}ll gtgs${x}n gtgs${x}d"
        echo "C*XFIND       TOTAL SOIL MOISTURE LAND (KG/M2)             (ANN ${year_rtdiag_start}-${yearm1})
C*XFIND       TOTAL SOIL MOISTURE LAND ONLY (KG/M2)        (ANN ${year_rtdiag_start}-${yearm1})
C*XFIND       TOTAL SOIL MOISTURE EXCL.GLAC.(KG/M2)        (ANN ${year_rtdiag_start}-${yearm1})
C*XFIND       TOTAL SOIL MOISTURE EXCL.GLAC./BEDR.(KG/M2)  (ANN ${year_rtdiag_start}-${yearm1})" >> ic.xfind_ann
        echo "C*XSAVE       TOTAL SOIL MOISTURE LAND (KG/M2)             (ANN ${year_rtdiag_start}-${year})
NEWNAM     WGLF
C*XSAVE       TOTAL SOIL MOISTURE LAND ONLY (KG/M2)        (ANN ${year_rtdiag_start}-${year})
NEWNAM     WGLF
C*XSAVE       TOTAL SOIL MOISTURE EXCL.GLAC.(KG/M2)        (ANN ${year_rtdiag_start}-${year})
NEWNAM     WGLF
C*XSAVE       TOTAL SOIL MOISTURE EXCL.GLAC./BEDR.(KG/M2)  (ANN ${year_rtdiag_start}-${year})
NEWNAM     WGLF" >> ic.xsave_ann

# ------------------------------------ annual liquid veg. moisture (WVL)

        x="WVL"
        timavg gs${x}01 tgs${x} _ _ gs${x}02 gs${x}03 gs${x}04 gs${x}05 gs${x}06 gs${x}07 gs${x}08 gs${x}09 gs${x}10 gs${x}11 gs${x}12

#                                      land
        globavg tgs${x} _ gtgs${x}l tland_frac
#                                      land only
        globavg tgs${x} _ gtgs${x}ll lland_frac
#                                      land excluding glaciers
        globavg tgs${x} _ gtgs${x}n nogla_frac
#                                      land excluding glaciers/bedrock
        globavg tgs${x} _ gtgs${x}d noglb_frac

        rtdlist_ann="$rtdlist_ann gtgs${x}l gtgs${x}ll gtgs${x}n gtgs${x}d"
        echo "C*XFIND       LIQUID VEG. MOISTURE LAND (KG/M2)            (ANN ${year_rtdiag_start}-${yearm1})
C*XFIND       LIQUID VEG. MOISTURE LAND ONLY (KG/M2)       (ANN ${year_rtdiag_start}-${yearm1})
C*XFIND       LIQUID VEG. MOISTURE EXCL.GLAC.(KG/M2)       (ANN ${year_rtdiag_start}-${yearm1})
C*XFIND       LIQUID VEG. MOISTURE EXCL.GLAC./BEDR.(KG/M2) (ANN ${year_rtdiag_start}-${yearm1})" >> ic.xfind_ann
        echo "C*XSAVE       LIQUID VEG. MOISTURE LAND (KG/M2)            (ANN ${year_rtdiag_start}-${year})
NEWNAM      WVL
C*XSAVE       LIQUID VEG. MOISTURE LAND ONLY (KG/M2)       (ANN ${year_rtdiag_start}-${year})
NEWNAM      WVL
C*XSAVE       LIQUID VEG. MOISTURE EXCL.GLAC.(KG/M2)       (ANN ${year_rtdiag_start}-${year})
NEWNAM      WVL
C*XSAVE       LIQUID VEG. MOISTURE EXCL.GLAC./BEDR.(KG/M2) (ANN ${year_rtdiag_start}-${year})
NEWNAM      WVL" >> ic.xsave_ann

# ------------------------------------ annual frozen veg. moisture (WVF)

        x="WVF"
        timavg gs${x}01 tgs${x} _ _ gs${x}02 gs${x}03 gs${x}04 gs${x}05 gs${x}06 gs${x}07 gs${x}08 gs${x}09 gs${x}10 gs${x}11 gs${x}12

#                                      land
        globavg tgs${x} _ gtgs${x}l tland_frac
#                                      land only
        globavg tgs${x} _ gtgs${x}ll lland_frac
#                                      land excluding glaciers
        globavg tgs${x} _ gtgs${x}n nogla_frac
#                                      land excluding glaciers/bedrock
        globavg tgs${x} _ gtgs${x}d noglb_frac

        rtdlist_ann="$rtdlist_ann gtgs${x}l gtgs${x}ll gtgs${x}n gtgs${x}d"
        echo "C*XFIND       FROZEN VEG. MOISTURE LAND (KG/M2)            (ANN ${year_rtdiag_start}-${yearm1})
C*XFIND       FROZEN VEG. MOISTURE LAND ONLY (KG/M2)       (ANN ${year_rtdiag_start}-${yearm1})
C*XFIND       FROZEN VEG. MOISTURE EXCL.GLAC.(KG/M2)       (ANN ${year_rtdiag_start}-${yearm1})
C*XFIND       FROZEN VEG. MOISTURE EXCL.GLAC./BEDR.(KG/M2) (ANN ${year_rtdiag_start}-${yearm1})" >> ic.xfind_ann
        echo "C*XSAVE       FROZEN VEG. MOISTURE LAND (KG/M2)            (ANN ${year_rtdiag_start}-${year})
NEWNAM      WVF
C*XSAVE       FROZEN VEG. MOISTURE LAND ONLY (KG/M2)       (ANN ${year_rtdiag_start}-${year})
NEWNAM      WVF
C*XSAVE       FROZEN VEG. MOISTURE EXCL.GLAC.(KG/M2)       (ANN ${year_rtdiag_start}-${year})
NEWNAM      WVF
C*XSAVE       FROZEN VEG. MOISTURE EXCL.GLAC./BEDR.(KG/M2) (ANN ${year_rtdiag_start}-${year})
NEWNAM      WVF" >> ic.xsave_ann

# ------------------------------------ annual total (liquid + frozen) veg. moisture (WVF+WVL)

        x="WVLF"
        add tgsWVL tgsWVF tgs${x}
#                                      land
        globavg tgs${x} _ gtgs${x}l tland_frac
#                                      land only
        globavg tgs${x} _ gtgs${x}ll lland_frac
#                                      land excluding glaciers
        globavg tgs${x} _ gtgs${x}n nogla_frac
#                                      land excluding glaciers/bedrock
        globavg tgs${x} _ gtgs${x}d noglb_frac

        rtdlist_ann="$rtdlist_ann gtgs${x}l gtgs${x}ll gtgs${x}n gtgs${x}d"
        echo "C*XFIND       TOTAL VEG. MOISTURE LAND (KG/M2)             (ANN ${year_rtdiag_start}-${yearm1})
C*XFIND       TOTAL VEG. MOISTURE LAND ONLY (KG/M2)        (ANN ${year_rtdiag_start}-${yearm1})
C*XFIND       TOTAL VEG. MOISTURE EXCL.GLAC.(KG/M2)        (ANN ${year_rtdiag_start}-${yearm1})
C*XFIND       TOTAL VEG. MOISTURE EXCL.GLAC./BEDR.(KG/M2)  (ANN ${year_rtdiag_start}-${yearm1})" >> ic.xfind_ann
        echo "C*XSAVE       TOTAL VEG. MOISTURE LAND (KG/M2)             (ANN ${year_rtdiag_start}-${year})
NEWNAM     WVLF
C*XSAVE       TOTAL VEG. MOISTURE LAND ONLY (KG/M2)        (ANN ${year_rtdiag_start}-${year})
NEWNAM     WVLF
C*XSAVE       TOTAL VEG. MOISTURE EXCL.GLAC.(KG/M2)        (ANN ${year_rtdiag_start}-${year})
NEWNAM     WVLF
C*XSAVE       TOTAL VEG. MOISTURE EXCL.GLAC./BEDR.(KG/M2)  (ANN ${year_rtdiag_start}-${year})
NEWNAM     WVLF" >> ic.xsave_ann

# ------------------------------------ annual volumetric fraction of WGL

        x="VFSL"
#                                      convert kg/m2 to m by dividing by density (1000 kg/m3)
        echo "C*XLIN         1000.       0.0         1" | ccc xlin tgsWGL tgsWGLd
        div tgsWGLd gsDZG01 tgs${x}
#                                      land
        globavg tgs${x} _ gtgs${x}l tland_frac
#                                      land excluding glaciers
        globavg tgs${x} _ gtgs${x}n nogla_frac

        rtdlist_ann="$rtdlist_ann gtgs${x}l gtgs${x}n"
        echo "C*XFIND       VOLUM.LIQUID SOIL MOIST.FRAC.LAND            (ANN ${year_rtdiag_start}-${yearm1})
C*XFIND       VOLUM.LIQUID SOIL MOIST.FRAC.EXCL.GLAC.      (ANN ${year_rtdiag_start}-${yearm1})" >> ic.xfind_ann
        echo "C*XSAVE       VOLUM.LIQUID SOIL MOIST.FRAC.LAND            (ANN ${year_rtdiag_start}-${year})
NEWNAM     VFSL
C*XSAVE       VOLUM.LIQUID SOIL MOIST.FRAC.EXCL.GLAC.      (ANN ${year_rtdiag_start}-${year})
NEWNAM     VFSL" >> ic.xsave_ann

# ------------------------------------ annual volumetric fraction of WGF

        x="VFSF"
#                                      convert kg/m2 to m by dividing by density (917 kg/m3)
        echo "C*XLIN          917.       0.0         1" | ccc xlin tgsWGF tgsWGFd
        div tgsWGFd gsDZG01 tgs${x}
#                                      land
        globavg tgs${x} _ gtgs${x}l tland_frac
#                                      land excluding glaciers
        globavg tgs${x} _ gtgs${x}n nogla_frac

        rtdlist_ann="$rtdlist_ann gtgs${x}l gtgs${x}n"
        echo "C*XFIND       VOLUM.FROZEN SOIL MOIST.FRAC.LAND            (ANN ${year_rtdiag_start}-${yearm1})
C*XFIND       VOLUM.FROZEN SOIL MOIST.FRAC.EXCL.GLAC.      (ANN ${year_rtdiag_start}-${yearm1})" >> ic.xfind_ann
        echo "C*XSAVE       VOLUM.FROZEN SOIL MOIST.FRAC.LAND            (ANN ${year_rtdiag_start}-${year})
NEWNAM     VFSF
C*XSAVE       VOLUM.FROZEN SOIL MOIST.FRAC.EXCL.GLAC.      (ANN ${year_rtdiag_start}-${year})
NEWNAM     VFSF" >> ic.xsave_ann

# ------------------------------------ annual volumetric fraction of total soil moisture (VFSM)

        x="VFSM"
#                                      total moisture depth
        add tgsWGLd tgsWGFd tgsWGLFd
#                                      multi-level average
        echo "C*BINS       -1" | ccc bins tgsWGLFd tgsWGLFdavg
#                                      divide by averaged soil depth
        gdiv tgsWGLFdavg gsDZGavg tgs${x}

#                                      land
        globavg tgs${x} _ gtgs${x}l tland_frac
#                                      land excluding glaciers
        globavg tgs${x} _ gtgs${x}n nogla_frac

        rtdlist_ann="$rtdlist_ann gtgs${x}l gtgs${x}n"
        echo "C*XFIND       VOLUM.TOTAL SOIL MOIST.FRAC.LAND             (ANN ${year_rtdiag_start}-${yearm1})
C*XFIND       VOLUM.TOTAL SOIL MOIST.FRAC.EXCL.GLAC.       (ANN ${year_rtdiag_start}-${yearm1})" >> ic.xfind_ann
        echo "C*XSAVE       VOLUM.TOTAL SOIL MOIST.FRAC.LAND             (ANN ${year_rtdiag_start}-${year})
NEWNAM     VFSM
C*XSAVE       VOLUM.TOTAL SOIL MOIST.FRAC.EXCL.GLAC.       (ANN ${year_rtdiag_start}-${year})
NEWNAM     VFSM" >> ic.xsave_ann

# ------------------------------------ annual water in snow WSNO

        x="WSNO"
        timavg gs${x}01 tgs${x} _ _ gs${x}02 gs${x}03 gs${x}04 gs${x}05 gs${x}06 gs${x}07 gs${x}08 gs${x}09 gs${x}10 gs${x}11 gs${x}12
#                                      land
        globavg tgs${x} _ gtgs${x}l tland_frac
#                                      land only
        globavg tgs${x} _ gtgs${x}ll lland_frac
#                                      land excluding glaciers
        globavg tgs${x} _ gtgs${x}n nogla_frac
#                                      land excluding glaciers/bedrock
        globavg tgs${x} _ gtgs${x}d noglb_frac

        rtdlist_ann="$rtdlist_ann gtgs${x}l gtgs${x}ll gtgs${x}n gtgs${x}d"
        echo "C*XFIND       LIQ. WATER IN SNOW LAND (KG/M2)              (ANN ${year_rtdiag_start}-${yearm1})
C*XFIND       LIQ. WATER IN SNOW LAND ONLY (KG/M2)         (ANN ${year_rtdiag_start}-${yearm1})
C*XFIND       LIQ. WATER IN SNOW EXCL.GLAC.(KG/M2)         (ANN ${year_rtdiag_start}-${yearm1})
C*XFIND       LIQ. WATER IN SNOW EXCL.GLAC./BEDR.(KG/M2)   (ANN ${year_rtdiag_start}-${yearm1})" >> ic.xfind_ann
        echo "C*XSAVE       LIQ. WATER IN SNOW LAND (KG/M2)              (ANN ${year_rtdiag_start}-${year})
NEWNAM     WSNO
C*XSAVE       LIQ. WATER IN SNOW LAND ONLY (KG/M2)         (ANN ${year_rtdiag_start}-${year})
NEWNAM     WSNO
C*XSAVE       LIQ. WATER IN SNOW EXCL.GLAC.(KG/M2)         (ANN ${year_rtdiag_start}-${year})
NEWNAM     WSNO
C*XSAVE       LIQ. WATER IN SNOW EXCL.GLAC./BEDR.(KG/M2)   (ANN ${year_rtdiag_start}-${year})
NEWNAM     WSNO" >> ic.xsave_ann

# ------------------------------------ annual ponding depth ZPND

        x="ZPND"
        timavg gs${x}01 tgs${x} _ _ gs${x}02 gs${x}03 gs${x}04 gs${x}05 gs${x}06 gs${x}07 gs${x}08 gs${x}09 gs${x}10 gs${x}11 gs${x}12
#                                      land
        globavg tgs${x} _ gtgs${x}l tland_frac
#                                      land only
        globavg tgs${x} _ gtgs${x}ll lland_frac
#                                      land excluding glaciers
        globavg tgs${x} _ gtgs${x}n nogla_frac
#                                      land excluding glaciers/bedrock
        globavg tgs${x} _ gtgs${x}d noglb_frac

        rtdlist_ann="$rtdlist_ann gtgs${x}l gtgs${x}ll gtgs${x}n gtgs${x}d"
        echo "C*XFIND       PONDING DEPTH LAND (M)                       (ANN ${year_rtdiag_start}-${yearm1})
C*XFIND       PONDING DEPTH LAND ONLY (M)                  (ANN ${year_rtdiag_start}-${yearm1})
C*XFIND       PONDING DEPTH EXCL.GLAC.(M)                  (ANN ${year_rtdiag_start}-${yearm1})
C*XFIND       PONDING DEPTH EXCL.GLAC./BEDR.(M)            (ANN ${year_rtdiag_start}-${yearm1})" >> ic.xfind_ann
        echo "C*XSAVE       PONDING DEPTH LAND (M)                       (ANN ${year_rtdiag_start}-${year})
NEWNAM     ZPND
C*XSAVE       PONDING DEPTH LAND ONLY (M)                  (ANN ${year_rtdiag_start}-${year})
NEWNAM     ZPND
C*XSAVE       PONDING DEPTH EXCL.GLAC.(M)                  (ANN ${year_rtdiag_start}-${year})
NEWNAM     ZPND
C*XSAVE       PONDING DEPTH EXCL.GLAC./BEDR.(M)            (ANN ${year_rtdiag_start}-${year})
NEWNAM     ZPND" >> ic.xsave_ann

# ------------------------------------ annual snow mass SNO

        x="SNO"
        timavg gs${x}01 tgs${x} _ _ gs${x}02 gs${x}03 gs${x}04 gs${x}05 gs${x}06 gs${x}07 gs${x}08 gs${x}09 gs${x}10 gs${x}11 gs${x}12
#                                      global
        globavg tgs${x} _ gtavg
#                                      convert kg/m2 to kg
        gmlt gtavg globe_area gtgs${x}g

        rtdlist_ann="$rtdlist_ann gtgs${x}g"
        echo "C*XFIND       SNOW MASS GLOBAL (KG)                        (ANN ${year_rtdiag_start}-${yearm1})" >> ic.xfind_ann
        echo "C*XSAVE       SNOW MASS GLOBAL (KG)                        (ANN ${year_rtdiag_start}-${year})
NEWNAM      SNO" >> ic.xsave_ann

# ------------------------------------ annual snow mass SNO over land

        x="SNOTl"
        timavg gs${x}01 tgs${x} _ _ gs${x}02 gs${x}03 gs${x}04 gs${x}05 gs${x}06 gs${x}07 gs${x}08 gs${x}09 gs${x}10 gs${x}11 gs${x}12

#                                      land
        globavg tgs${x} _ gtavg dland_frac
#                                      convert kg/m2 to kg
        gmlt gtavg dland_area gtgs${x}l

#                                      land only
        globavg tgs${x} _ gtavg lland_frac
#                                      convert kg/m2 to kg
        gmlt gtavg lland_area gtgs${x}ll

#                                      land excluding glaciers
        globavg tgs${x} _ gtavg nogla_frac
#                                      convert kg/m2 to kg
        gmlt gtavg nogla_area gtgs${x}n

#                                      land excluding glaciers/bedrock
        globavg tgs${x} _ gtavg noglb_frac
#                                      convert kg/m2 to kg
        gmlt gtavg nogla_area gtgs${x}d

#                                      glaciers
        globavg tgs${x} _ gtavg glaci_frac
#                                      convert kg/m2 to kg
        gmlt gtavg glaci_area gtgs${x}i

#                                      bedrock
        globavg tgs${x} _ gtavg bedro_frac
#                                      convert kg/m2 to kg
        gmlt gtavg bedro_area gtgs${x}b

        rtdlist_ann="$rtdlist_ann gtgs${x}l gtgs${x}ll gtgs${x}n gtgs${x}d gtgs${x}i gtgs${x}b"
        echo "C*XFIND       SNOW MASS LAND (KG)                          (ANN ${year_rtdiag_start}-${yearm1})
C*XFIND       SNOW MASS LAND ONLY (KG)                     (ANN ${year_rtdiag_start}-${yearm1})
C*XFIND       SNOW MASS LAND EXCL.GLACIERS (KG)            (ANN ${year_rtdiag_start}-${yearm1})
C*XFIND       SNOW MASS LAND EXCL.GLACIERS/BEDR. (KG)      (ANN ${year_rtdiag_start}-${yearm1})
C*XFIND       SNOW MASS GLACIERS (KG)                      (ANN ${year_rtdiag_start}-${yearm1})
C*XFIND       SNOW MASS BEDROCK (KG)                       (ANN ${year_rtdiag_start}-${yearm1})" >> ic.xfind_ann
        echo "C*XSAVE       SNOW MASS LAND (KG)                          (ANN ${year_rtdiag_start}-${year})
NEWNAM     SNOT
C*XSAVE       SNOW MASS LAND ONLY (KG)                     (ANN ${year_rtdiag_start}-${year})
NEWNAM     SNOT
C*XSAVE       SNOW MASS LAND EXCL.GLACIERS (KG)            (ANN ${year_rtdiag_start}-${year})
NEWNAM     SNOT
C*XSAVE       SNOW MASS LAND EXCL.GLACIERS/BEDR. (KG)      (ANN ${year_rtdiag_start}-${year})
NEWNAM     SNOT
C*XSAVE       SNOW MASS GLACIERS (KG)                      (ANN ${year_rtdiag_start}-${year})
NEWNAM     SNOT
C*XSAVE       SNOW MASS BEDROCK (KG)                       (ANN ${year_rtdiag_start}-${year})
NEWNAM     SNOT" >> ic.xsave_ann

# ------------------------------------ annual snow mass SNO over sea ice

        x="SNOT"
#                                      time average
        timavg gs${x}ig01 tgs${x}ig _ _ gs${x}ig02 gs${x}ig03 gs${x}ig04 gs${x}ig05 gs${x}ig06 gs${x}ig07 gs${x}ig08 gs${x}ig09 gs${x}ig10 gs${x}ig11 gs${x}ig12
#                                      global mean
        globavg tgs${x}ig _ gtavg
#                                      convert kg/m2 to kg
        gmlt gtavg globe_area gtgs${x}ig

        rtdlist_ann="$rtdlist_ann gtgs${x}ig"
        echo "C*XFIND       SNOW MASS OCEAN (KG)                         (ANN ${year_rtdiag_start}-${yearm1})" >> ic.xfind_ann
        echo "C*XSAVE       SNOW MASS OCEAN (KG)                         (ANN ${year_rtdiag_start}-${year})
NEWNAM     SNOT" >> ic.xsave_ann

# ------------------------------------ annual snow density (RHON)

        x="RHON"
        if [ -s gs${x}01 ] ; then
#                                      masked time mean (only for RHON>0)
        y="${x}msk"
        timavg gs${x}01 tgs${x} _ _ gs${x}02 gs${x}03 gs${x}04 gs${x}05 gs${x}06 gs${x}07 gs${x}08 gs${x}09 gs${x}10 gs${x}11 gs${x}12
        timavg gs${y}01 tgs${y} _ _ gs${y}02 gs${y}03 gs${y}04 gs${y}05 gs${y}06 gs${y}07 gs${y}08 gs${y}09 gs${y}10 gs${y}11 gs${y}12

#                                      masked spatial mean
        globavg tgs${x} _ gtgs${x}
        globavg tgs${y} _ gtgs${y}
        div gtgs${x} gtgs${y} gtgs${x}.1 ; mv gtgs${x}.1 gtgs${x}

        rtdlist_ann="$rtdlist_ann gtgs${x}"
        echo "C*XFIND       SNOW DENSITY (KG/M3)                         (ANN ${year_rtdiag_start}-${yearm1})" >> ic.xfind_ann
        echo "C*XSAVE       SNOW DENSITY (KG/M3)                         (ANN ${year_rtdiag_start}-${year})
NEWNAM     RHON" >> ic.xsave_ann
        fi

# ------------------------------------ annual effective snow grain radius (REF)

        x="REF"
        if [ -s gs${x}01 ] ; then
#                                      masked time mean (only for REF>0)
        y="${x}msk"
        timavg gs${x}01 tgs${x} _ _ gs${x}02 gs${x}03 gs${x}04 gs${x}05 gs${x}06 gs${x}07 gs${x}08 gs${x}09 gs${x}10 gs${x}11 gs${x}12
        timavg gs${y}01 tgs${y} _ _ gs${y}02 gs${y}03 gs${y}04 gs${y}05 gs${y}06 gs${y}07 gs${y}08 gs${y}09 gs${y}10 gs${y}11 gs${y}12

#                                      masked spatial mean
        globavg tgs${x} _ gtgs${x}
        globavg tgs${y} _ gtgs${y}
        div gtgs${x} gtgs${y} gtgs${x}.1 ; mv gtgs${x}.1 gtgs${x}

        rtdlist_ann="$rtdlist_ann gtgs${x}"
        echo "C*XFIND       EFFECTIVE SNOW GRAIN RADIUS (M)              (ANN ${year_rtdiag_start}-${yearm1})" >> ic.xfind_ann
        echo "C*XSAVE       EFFECTIVE SNOW GRAIN RADIUS (M)              (ANN ${year_rtdiag_start}-${year})
NEWNAM      REF" >> ic.xsave_ann
        fi

# ------------------------------------ annual snow albedo AN

        x="AN"
#                                      time average
        timavg ggs${x}g01 tggs${x}g _ _ ggs${x}g02 ggs${x}g03 ggs${x}g04 ggs${x}g05 ggs${x}g06 ggs${x}g07 ggs${x}g08 ggs${x}g09 ggs${x}g10 ggs${x}g11 ggs${x}g12
        timavg ggs${x}l01 tggs${x}l _ _ ggs${x}l02 ggs${x}l03 ggs${x}l04 ggs${x}l05 ggs${x}l06 ggs${x}l07 ggs${x}l08 ggs${x}l09 ggs${x}l10 ggs${x}l11 ggs${x}l12
        timavg ggs${x}o01 tggs${x}o _ _ ggs${x}o02 ggs${x}o03 ggs${x}o04 ggs${x}o05 ggs${x}o06 ggs${x}o07 ggs${x}o08 ggs${x}o09 ggs${x}o10 ggs${x}o11 ggs${x}o12

        rtdlist_ann="$rtdlist_ann tggs${x}g tggs${x}l tggs${x}o"
        echo "
C*XFIND       SNOW ALBEDO GLOBAL                           (ANN ${year_rtdiag_start}-${yearm1})
C*XFIND       SNOW ALBEDO LAND                             (ANN ${year_rtdiag_start}-${yearm1})
C*XFIND       SNOW ALBEDO OCEAN                            (ANN ${year_rtdiag_start}-${yearm1})" >> ic.xfind_ann
        echo "C*XSAVE       SNOW ALBEDO GLOBAL                           (ANN ${year_rtdiag_start}-${year})
NEWNAM       AN
C*XSAVE       SNOW ALBEDO LAND                             (ANN ${year_rtdiag_start}-${year})
NEWNAM       AN
C*XSAVE       SNOW ALBEDO OCEAN                            (ANN ${year_rtdiag_start}-${year})
NEWNAM       AN" >> ic.xsave_ann

# ------------------------------------ annual global BALT (new definition)

        x="BALT"
        if [ -s gs${x}01 ] ; then
        timavg gs${x}01 tgs${x} _ _ gs${x}02 gs${x}03 gs${x}04 gs${x}05 gs${x}06 gs${x}07 gs${x}08 gs${x}09 gs${x}10 gs${x}11 gs${x}12
#                                      global
        globavg tgs${x} _ gtgs${x}g

        rtdlist_ann="$rtdlist_ann gtgs${x}g"
        echo "C*XFIND       NET TOA ENERGY GLOBAL BALT (W/M2)            (ANN ${year_rtdiag_start}-${yearm1})" >> ic.xfind_ann
        echo "C*XSAVE       NET TOA ENERGY GLOBAL BALT (W/M2)            (ANN ${year_rtdiag_start}-${year})
NEWNAM     BALT" >> ic.xsave_ann
        fi

# ------------------------------------ annual global BALP (AR5 definition of BALT)

        x="BALP"
        if [ -s gs${x}01 ] ; then
        timavg gs${x}01 tgs${x} _ _ gs${x}02 gs${x}03 gs${x}04 gs${x}05 gs${x}06 gs${x}07 gs${x}08 gs${x}09 gs${x}10 gs${x}11 gs${x}12
#                                      global
        globavg tgs${x} _ gtgs${x}g

        rtdlist_ann="$rtdlist_ann gtgs${x}g"
        echo "C*XFIND       NET TOA ENERGY GLOBAL BALP (W/M2)            (ANN ${year_rtdiag_start}-${yearm1})" >> ic.xfind_ann
        echo "C*XSAVE       NET TOA ENERGY GLOBAL BALP (W/M2)            (ANN ${year_rtdiag_start}-${year})
NEWNAM     BALP" >> ic.xsave_ann
        fi

# ------------------------------------ annual global BALX (radiation budget at top of model)

        x="BALX"
        if [ -s gs${x}01 ] ; then
        timavg gs${x}01 tgs${x} _ _ gs${x}02 gs${x}03 gs${x}04 gs${x}05 gs${x}06 gs${x}07 gs${x}08 gs${x}09 gs${x}10 gs${x}11 gs${x}12
#                                      global
        globavg tgs${x} _ gtgs${x}g

        rtdlist_ann="$rtdlist_ann gtgs${x}g"
        echo "C*XFIND       NET TOA ENERGY GLOBAL BALX (W/M2)            (ANN ${year_rtdiag_start}-${yearm1})" >> ic.xfind_ann
        echo "C*XSAVE       NET TOA ENERGY GLOBAL BALX (W/M2)            (ANN ${year_rtdiag_start}-${year})
NEWNAM     BALX" >> ic.xsave_ann
        fi

# ------------------------------------ annual BEG

        x="BEG"
        timavg gs${x}01 tgs${x} _ _ gs${x}02 gs${x}03 gs${x}04 gs${x}05 gs${x}06 gs${x}07 gs${x}08 gs${x}09 gs${x}10 gs${x}11 gs${x}12
#                                      global
        globavg tgs${x} _ gtgs${x}g
#                                      ocean
        globavg tgs${x} _ gtgs${x}o ocean_frac
#                                      land
        globavg tgs${x} _ gtgs${x}l tland_frac
#                                      lakes
        globavg tgs${x} _ gtgs${x}r rlake_frac

        rtdlist_ann="$rtdlist_ann gtgs${x}g gtgs${x}o gtgs${x}l gtgs${x}r"
        echo "C*XFIND       NET SFC ENERGY GLOBAL BEG (W/M2)             (ANN ${year_rtdiag_start}-${yearm1})
C*XFIND       NET SFC ENERGY OCEAN BEG (W/M2)              (ANN ${year_rtdiag_start}-${yearm1})
C*XFIND       NET SFC ENERGY LAND BEG (W/M2)               (ANN ${year_rtdiag_start}-${yearm1})
C*XFIND       NET SFC ENERGY LAKES BEG (W/M2)              (ANN ${year_rtdiag_start}-${yearm1})" >> ic.xfind_ann
        echo "C*XSAVE       NET SFC ENERGY GLOBAL BEG (W/M2)             (ANN ${year_rtdiag_start}-${year})
NEWNAM      BEG
C*XSAVE       NET SFC ENERGY OCEAN BEG (W/M2)              (ANN ${year_rtdiag_start}-${year})
NEWNAM      BEG
C*XSAVE       NET SFC ENERGY LAND BEG (W/M2)               (ANN ${year_rtdiag_start}-${year})
NEWNAM      BEG
C*XSAVE       NET SFC ENERGY LAKES BEG (W/M2)              (ANN ${year_rtdiag_start}-${year})
NEWNAM      BEG" >> ic.xsave_ann

# ------------------------------------ annual BEGI/BEGO

        x="BEGI"
        y="BEGO"
        if [ -s gs${x}01 -a -s gs${y}01 ] ; then
        timavg gs${x}01i tgs${x}i _ _ gs${x}02i gs${x}03i gs${x}04i gs${x}05i gs${x}06i gs${x}07i gs${x}08i gs${x}09i gs${x}10i gs${x}11i gs${x}12i
        timavg gs${y}01w tgs${y}w _ _ gs${y}02w gs${y}03w gs${y}04w gs${y}05w gs${y}06w gs${y}07w gs${y}08w gs${y}09w gs${y}10w gs${y}11w gs${y}12w
#                                      ocean mean
        globavg tgs${x}i _ gtgs${x}io ocean_frac
        globavg tgs${y}w _ gtgs${y}wo ocean_frac

        rtdlist_ann="$rtdlist_ann gtgs${x}io gtgs${y}wo"
        echo "C*XFIND       NET SFC ENERGY SEAICE BEGI (W/M2)            (ANN ${year_rtdiag_start}-${yearm1})
C*XFIND       NET SFC ENERGY OPEN WATER BEGO (W/M2)        (ANN ${year_rtdiag_start}-${yearm1})" >> ic.xfind_ann
        echo "C*XSAVE       NET SFC ENERGY SEAICE BEGI (W/M2)            (ANN ${year_rtdiag_start}-${year})
NEWNAM     BEGI
C*XSAVE       NET SFC ENERGY OPEN WATER BEGO (W/M2)        (ANN ${year_rtdiag_start}-${year})
NEWNAM     BEGO" >> ic.xsave_ann
        fi

# ------------------------------------ annual BEGL

        x="BEGL"
        if [ -s gs${x}01 ] ; then
        timavg gs${x}01 tgs${x} _ _ gs${x}02 gs${x}03 gs${x}04 gs${x}05 gs${x}06 gs${x}07 gs${x}08 gs${x}09 gs${x}10 gs${x}11 gs${x}12
#                                      land mean
        globavg tgs${x} _ gtgs${x}l tland_frac

        rtdlist_ann="$rtdlist_ann gtgs${x}l"
        echo "C*XFIND       NET SFC ENERGY LAND BEGL (W/M2)              (ANN ${year_rtdiag_start}-${yearm1})" >> ic.xfind_ann
        echo "C*XSAVE       NET SFC ENERGY LAND BEGL (W/M2)              (ANN ${year_rtdiag_start}-${year})
NEWNAM     BEGL" >> ic.xsave_ann
        fi

# ------------------------------------ annual FSGI/FSGO

        x="FSGI"
        y="FSGO"
        if [ -s gs${x}01 -a -s gs${y}01 ] ; then
        timavg gs${x}01i tgs${x}i _ _ gs${x}02i gs${x}03i gs${x}04i gs${x}05i gs${x}06i gs${x}07i gs${x}08i gs${x}09i gs${x}10i gs${x}11i gs${x}12i
        timavg gs${y}01w tgs${y}w _ _ gs${y}02w gs${y}03w gs${y}04w gs${y}05w gs${y}06w gs${y}07w gs${y}08w gs${y}09w gs${y}10w gs${y}11w gs${y}12w
#                                      ocean mean
        globavg tgs${x}i _ gtgs${x}io ocean_frac
        globavg tgs${y}w _ gtgs${y}wo ocean_frac

        rtdlist_ann="$rtdlist_ann gtgs${x}io gtgs${y}wo"
        echo "C*XFIND       NET SFC SOLAR FLUX SEAICE FSGI (W/M2)        (ANN ${year_rtdiag_start}-${yearm1})
C*XFIND       NET SFC SOLAR FLUX OPEN WATER FSGO (W/M2)    (ANN ${year_rtdiag_start}-${yearm1})" >> ic.xfind_ann
        echo "C*XSAVE       NET SFC SOLAR FLUX SEAICE FSGI (W/M2)        (ANN ${year_rtdiag_start}-${year})
NEWNAM     FSGI
C*XSAVE       NET SFC SOLAR FLUX OPEN WATER FSGO (W/M2)    (ANN ${year_rtdiag_start}-${year})
NEWNAM     FSGO" >> ic.xsave_ann
        fi

# ------------------------------------ annual PCPO/PCPI

        x="PCP"
        if [ -s gs${x}01 ] ; then
        timavg gs${x}01i tgs${x}i _ _ gs${x}02i gs${x}03i gs${x}04i gs${x}05i gs${x}06i gs${x}07i gs${x}08i gs${x}09i gs${x}10i gs${x}11i gs${x}12i
        timavg gs${x}01w tgs${x}w _ _ gs${x}02w gs${x}03w gs${x}04w gs${x}05w gs${x}06w gs${x}07w gs${x}08w gs${x}09w gs${x}10w gs${x}11w gs${x}12w
#                                      ocean mean
        globavg tgs${x}i _ gtgs${x}io ocean_frac
        globavg tgs${x}w _ gtgs${x}wo ocean_frac
#                                      convert kg/m2/s or mm/s to mm/day by multiplying with 60x60x24=86400
        echo "C*XLIN        86400.       0.0" | ccc xlin gtgs${x}io gtgs${x}io.1 ; mv gtgs${x}io.1 gtgs${x}io
        echo "C*XLIN        86400.       0.0" | ccc xlin gtgs${x}wo gtgs${x}wo.1 ; mv gtgs${x}wo.1 gtgs${x}wo

        rtdlist_ann="$rtdlist_ann gtgs${x}io gtgs${x}wo"
        echo "C*XFIND       PRECIPITATION OVER SEAICE (MM/DAY)           (ANN ${year_rtdiag_start}-${yearm1})
C*XFIND       PRECIPITATION OVER OPEN WATER (MM/DAY)       (ANN ${year_rtdiag_start}-${yearm1})" >> ic.xfind_ann
        echo "C*XSAVE       PRECIPITATION OVER SEAICE (MM/DAY)           (ANN ${year_rtdiag_start}-${year})
NEWNAM      PCP
C*XSAVE       PRECIPITATION OVER OPEN WATER (MM/DAY)       (ANN ${year_rtdiag_start}-${year})
NEWNAM      PCP" >> ic.xsave_ann
        fi

# ------------------------------------ annual QFSO/QFSI

        x="QFS"
        if [ -s gs${x}01i -a  -s gs${x}01o ] ; then
        timavg gs${x}01i tgs${x}i _ _ gs${x}02i gs${x}03i gs${x}04i gs${x}05i gs${x}06i gs${x}07i gs${x}08i gs${x}09i gs${x}10i gs${x}11i gs${x}12i
        timavg gs${x}01w tgs${x}w _ _ gs${x}02w gs${x}03w gs${x}04w gs${x}05w gs${x}06w gs${x}07w gs${x}08w gs${x}09w gs${x}10w gs${x}11w gs${x}12w
#                                      ocean mean
        globavg tgs${x}i _ gtgs${x}io ocean_frac
        globavg tgs${x}w _ gtgs${x}wo ocean_frac
#                                      convert kg/m2/s or mm/s to mm/day by multiplying with 60x60x24=86400
        echo "C*XLIN        86400.       0.0" | ccc xlin gtgs${x}io gtgs${x}io.1 ; mv gtgs${x}io.1 gtgs${x}io
        echo "C*XLIN        86400.       0.0" | ccc xlin gtgs${x}wo gtgs${x}wo.1 ; mv gtgs${x}wo.1 gtgs${x}wo

        rtdlist_ann="$rtdlist_ann gtgs${x}io gtgs${x}wo"
        echo "C*XFIND       EVAPORATION OVER SEAICE (MM/DAY)             (ANN ${year_rtdiag_start}-${yearm1})
C*XFIND       EVAPORATION OVER OPEN WATER (MM/DAY)         (ANN ${year_rtdiag_start}-${yearm1})" >> ic.xfind_ann
        echo "C*XSAVE       EVAPORATION OVER SEAICE (MM/DAY)             (ANN ${year_rtdiag_start}-${year})
NEWNAM      QFS
C*XSAVE       EVAPORATION OVER OPEN WATER (MM/DAY)         (ANN ${year_rtdiag_start}-${year})
NEWNAM      QFS" >> ic.xsave_ann
        fi

# ------------------------------------ annual HFLI

        x="HFLI"
        if [ -s gs${x}01 ] ; then
        timavg gs${x}01i tgs${x}i _ _ gs${x}02i gs${x}03i gs${x}04i gs${x}05i gs${x}06i gs${x}07i gs${x}08i gs${x}09i gs${x}10i gs${x}11i gs${x}12i
#                                      ocean mean
        globavg tgs${x}i _ gtgs${x}io ocean_frac

        rtdlist_ann="$rtdlist_ann gtgs${x}io"
        echo "C*XFIND       OCEAN SFC LATENT SUBLIM HEAT UP HFLI (W/M2)  (ANN ${year_rtdiag_start}-${yearm1})" >> ic.xfind_ann
        echo "C*XSAVE       OCEAN SFC LATENT SUBLIM HEAT UP HFLI (W/M2)  (ANN ${year_rtdiag_start}-${year})
NEWNAM     HFLI" >> ic.xsave_ann
        fi

# ------------------------------------ annual BWGI

        x="BWGI"
        if [ -s gs${x}01 ] ; then
        timavg gs${x}01i tgs${x}i _ _ gs${x}02i gs${x}03i gs${x}04i gs${x}05i gs${x}06i gs${x}07i gs${x}08i gs${x}09i gs${x}10i gs${x}11i gs${x}12i
#                                      ocean mean
        globavg tgs${x}i _ gtgs${x}io ocean_frac
#                                      convert kg/m2/s or mm/s to mm/day by multiplying with 60x60x24=86400
        echo "C*XLIN        86400.       0.0" | ccc xlin gtgs${x}io gtgs${x}io.1 ; mv gtgs${x}io.1 gtgs${x}io

        rtdlist_ann="$rtdlist_ann gtgs${x}io"
        echo "C*XFIND       NET SFC WATER FLUX SEAICE BWGI (MM/DAY)      (ANN ${year_rtdiag_start}-${yearm1})" >> ic.xfind_ann
        echo "C*XSAVE       NET SFC WATER FLUX SEAICE BWGI (MM/DAY)      (ANN ${year_rtdiag_start}-${year})
NEWNAM     BWGI" >> ic.xsave_ann
        fi

# ------------------------------------ annual BWGO

        y="BWGO"
        if [ -s gs${y}01 ] ; then
        timavg gs${y}01w tgs${y}w _ _ gs${y}02w gs${y}03w gs${y}04w gs${y}05w gs${y}06w gs${y}07w gs${y}08w gs${y}09w gs${y}10w gs${y}11w gs${y}12w
#                                      ocean mean
        globavg tgs${y}w _ gtgs${y}wo ocean_frac
#                                      convert kg/m2/s or mm/s to mm/day by multiplying with 60x60x24=86400
        echo "C*XLIN        86400.       0.0" | ccc xlin gtgs${y}wo gtgs${y}wo.1 ; mv gtgs${y}wo.1 gtgs${y}wo

        rtdlist_ann="$rtdlist_ann gtgs${y}wo"
        echo "C*XFIND       NET SFC WATER FLUX OPEN WATER BWGO (MM/DAY)  (ANN ${year_rtdiag_start}-${yearm1})" >> ic.xfind_ann
        echo "C*XSAVE       NET SFC WATER FLUX OPEN WATER BWGO (MM/DAY)  (ANN ${year_rtdiag_start}-${year})
NEWNAM     BWGO" >> ic.xsave_ann
        fi

# ------------------------------------ annual OBEG

        x="OBEG"
        timavg gs${x}01 tgs${x} _ _ gs${x}02 gs${x}03 gs${x}04 gs${x}05 gs${x}06 gs${x}07 gs${x}08 gs${x}09 gs${x}10 gs${x}11 gs${x}12
#                                      global
        globavg tgs${x} _ gtgs${x}g
#                                      ocean
        globavg tgs${x} _ gtgs${x}o ocean_frac
#                                      land
        globavg tgs${x} _ gtgs${x}l tland_frac
#                                      lakes
        globavg tgs${x} _ gtgs${x}r rlake_frac

        rtdlist_ann="$rtdlist_ann gtgs${x}g gtgs${x}o gtgs${x}l gtgs${x}r"
        echo "C*XFIND       NET SFC ENERGY GLOBAL OBEG (W/M2)            (ANN ${year_rtdiag_start}-${yearm1})
C*XFIND       NET SFC ENERGY OCEAN OBEG (W/M2)             (ANN ${year_rtdiag_start}-${yearm1})
C*XFIND       NET SFC ENERGY LAND OBEG (W/M2)              (ANN ${year_rtdiag_start}-${yearm1})
C*XFIND       NET SFC ENERGY LAKES OBEG (W/M2)             (ANN ${year_rtdiag_start}-${yearm1})" >> ic.xfind_ann
        echo "C*XSAVE       NET SFC ENERGY GLOBAL OBEG (W/M2)            (ANN ${year_rtdiag_start}-${year})
NEWNAM     OBEG
C*XSAVE       NET SFC ENERGY OCEAN OBEG (W/M2)             (ANN ${year_rtdiag_start}-${year})
NEWNAM     OBEG
C*XSAVE       NET SFC ENERGY LAND OBEG (W/M2)              (ANN ${year_rtdiag_start}-${year})
NEWNAM     OBEG
C*XSAVE       NET SFC ENERGY LAKES OBEG (W/M2)             (ANN ${year_rtdiag_start}-${year})
NEWNAM     OBEG" >> ic.xsave_ann

# ------------------------------------ annual global OBEX=OBEG+OBEI

        x="OBEX"
        if [ -s gs${x}01 ] ; then
        timavg gs${x}01 tgs${x} _ _ gs${x}02 gs${x}03 gs${x}04 gs${x}05 gs${x}06 gs${x}07 gs${x}08 gs${x}09 gs${x}10 gs${x}11 gs${x}12
#                                      global
        globavg tgs${x} _ gtgs${x}g
#                                      ocean
        globavg tgs${x} _ gtgs${x}o ocean_frac

        rtdlist_ann="$rtdlist_ann gtgs${x}g gtgs${x}o"
        echo "C*XFIND       NET SFC ENERGY GLOBAL OBEX (W/M2)            (ANN ${year_rtdiag_start}-${yearm1})
C*XFIND       NET SFC ENERGY OCEAN OBEX (W/M2)             (ANN ${year_rtdiag_start}-${yearm1})" >> ic.xfind_ann
        echo "C*XSAVE       NET SFC ENERGY GLOBAL OBEX (W/M2)            (ANN ${year_rtdiag_start}-${year})
NEWNAM     OBEX
C*XSAVE       NET SFC ENERGY OCEAN OBEX (W/M2)             (ANN ${year_rtdiag_start}-${year})
NEWNAM     OBEX" >> ic.xsave_ann
        fi

# ------------------------------------ annual global FMI

        x="FMI"
        if [ -s gs${x}01 ] ; then
        timavg gs${x}01 tgs${x} _ _ gs${x}02 gs${x}03 gs${x}04 gs${x}05 gs${x}06 gs${x}07 gs${x}08 gs${x}09 gs${x}10 gs${x}11 gs${x}12
#                                      global
        globavg tgs${x} _ gtgs${x}g

        rtdlist_ann="$rtdlist_ann gtgs${x}g"
        echo "C*XFIND       HEAT TO MELT ICE GLOBAL FMI (W/M2)           (ANN ${year_rtdiag_start}-${yearm1})" >> ic.xfind_ann
        echo "C*XSAVE       HEAT TO MELT ICE GLOBAL FMI (W/M2)           (ANN ${year_rtdiag_start}-${year})
NEWNAM      FMI" >> ic.xsave_ann
        fi

# ------------------------------------ annual global ABEG = FSG + FLG - (HFS + LATA), where LATA=HS*PCPN + HV*(PCP-PCPN),HS=2.835E6;HV=2.501E6

        for x in FSG FLG HFS PCPN PCP ; do
          timavg gs${x}01 tgs${x} _ _ gs${x}02 gs${x}03 gs${x}04 gs${x}05 gs${x}06 gs${x}07 gs${x}08 gs${x}09 gs${x}10 gs${x}11 gs${x}12
          globavg tgs${x} _ gtgs${x}
        done
        # compute LATA
        sub gtgsPCP gtgsPCPN gtgsPCPR
        echo "C*XYLIN      2.835E6   2.501E6        0." | ccc xylin gtgsPCPN gtgsPCPR gtgsLATA
        # compute ABEG
        add gtgsFSG  gtgsFLG  gtgsFSLG
        add gtgsHFS  gtgsLATA gtgsHFSL
        sub gtgsFSLG gtgsHFSL gtgsABEG

        x="ABEG"
        rtdlist_ann="$rtdlist_ann gtgs${x}"
        echo "C*XFIND       NET SFC ENERGY GLOBAL ABEG (W/M2)            (ANN ${year_rtdiag_start}-${yearm1})" >> ic.xfind_ann
        echo "C*XSAVE       NET SFC ENERGY GLOBAL ABEG (W/M2)            (ANN ${year_rtdiag_start}-${year})
NEWNAM     ABEG" >> ic.xsave_ann

# ------------------------------------ annual global RES

        x="RES"
        if [ -s gs${x}01 ] ; then
        timavg gs${x}01 tgs${x} _ _ gs${x}02 gs${x}03 gs${x}04 gs${x}05 gs${x}06 gs${x}07 gs${x}08 gs${x}09 gs${x}10 gs${x}11 gs${x}12
#                                      global
        globavg tgs${x} _ gtgs${x}g

        rtdlist_ann="$rtdlist_ann gtgs${x}g"
        echo "C*XFIND       RESIDUAL HEAT FLUX GLOBAL RES (W/M2)         (ANN ${year_rtdiag_start}-${yearm1})" >> ic.xfind_ann
        echo "C*XSAVE       RESIDUAL HEAT FLUX GLOBAL RES (W/M2)         (ANN ${year_rtdiag_start}-${year})
NEWNAM      RES" >> ic.xsave_ann
        fi

# ------------------------------------ annual BWG

        x="BWG"
        if [ -s gs${x}01 ] ; then
        timavg gs${x}01 tgs${x} _ _ gs${x}02 gs${x}03 gs${x}04 gs${x}05 gs${x}06 gs${x}07 gs${x}08 gs${x}09 gs${x}10 gs${x}11 gs${x}12
#                                      convert kg/m2/s or mm/s to mm/day by multiplying with 60x60x24=86400
        echo "C*XLIN        86400.       0.0" | ccc xlin tgs${x} tgs${x}.1 ; mv tgs${x}.1 tgs${x}
#                                      global
        globavg tgs${x} _ gtgs${x}g
#                                      ocean
        globavg tgs${x} _ gtgs${x}o ocean_frac
#                                      land
        globavg tgs${x} _ gtgs${x}l tland_frac
#                                      land only
        globavg tgs${x} _ gtgs${x}ll lland_frac
#                                      lakes
        globavg tgs${x} _ gtgs${x}r rlake_frac

        rtdlist_ann="$rtdlist_ann gtgs${x}g gtgs${x}o gtgs${x}l gtgs${x}ll gtgs${x}r"
        echo "C*XFIND       GLB FRESHWATER FLUX BWG (MM/DAY)             (ANN ${year_rtdiag_start}-${yearm1})
C*XFIND       OCN FRESHWATER FLUX BWG (MM/DAY)             (ANN ${year_rtdiag_start}-${yearm1})
C*XFIND       LAND FRESHWATER FLUX BWG (MM/DAY)            (ANN ${year_rtdiag_start}-${yearm1})
C*XFIND       LAND ONLY FRESHWATER FLUX BWG (MM/DAY)       (ANN ${year_rtdiag_start}-${yearm1})
C*XFIND       LAKES FRESHWATER FLUX BWG (MM/DAY)           (ANN ${year_rtdiag_start}-${yearm1})" >> ic.xfind_ann
        echo "C*XSAVE       GLB FRESHWATER FLUX BWG (MM/DAY)             (ANN ${year_rtdiag_start}-${year})
NEWNAM      BWG
C*XSAVE       OCN FRESHWATER FLUX BWG (MM/DAY)             (ANN ${year_rtdiag_start}-${year})
NEWNAM      BWG
C*XSAVE       LAND FRESHWATER FLUX BWG (MM/DAY)            (ANN ${year_rtdiag_start}-${year})
NEWNAM      BWG
C*XSAVE       LAND ONLY FRESHWATER FLUX BWG (MM/DAY)       (ANN ${year_rtdiag_start}-${year})
NEWNAM      BWG
C*XSAVE       LAKES FRESHWATER FLUX BWG (MM/DAY)           (ANN ${year_rtdiag_start}-${year})
NEWNAM      BWG" >> ic.xsave_ann
        fi

# ------------------------------------ annual OBWG

        x="OBWG"
        if [ -s gs${x}01 ] ; then
        timavg gs${x}01 tgs${x} _ _ gs${x}02 gs${x}03 gs${x}04 gs${x}05 gs${x}06 gs${x}07 gs${x}08 gs${x}09 gs${x}10 gs${x}11 gs${x}12
#                                      convert kg/m2/s or mm/s to mm/day by multiplying with 60x60x24=86400
        echo "C*XLIN        86400.       0.0" | ccc xlin tgs${x} tgs${x}.1 ; mv tgs${x}.1 tgs${x}
#                                      global
        globavg tgs${x} _ gtgs${x}g
#                                      ocean
        globavg tgs${x} _ gtgs${x}o ocean_frac
#                                      land
        globavg tgs${x} _ gtgs${x}l tland_frac
#                                      land only
        globavg tgs${x} _ gtgs${x}ll lland_frac
#                                      lakes
        globavg tgs${x} _ gtgs${x}r rlake_frac

        rtdlist_ann="$rtdlist_ann gtgs${x}g gtgs${x}o gtgs${x}l gtgs${x}ll gtgs${x}r"
        echo "C*XFIND       GLB FRESHWATER FLUX OBWG (MM/DAY)            (ANN ${year_rtdiag_start}-${yearm1})
C*XFIND       OCN FRESHWATER FLUX OBWG (MM/DAY)            (ANN ${year_rtdiag_start}-${yearm1})
C*XFIND       LAND FRESHWATER FLUX OBWG (MM/DAY)           (ANN ${year_rtdiag_start}-${yearm1})
C*XFIND       LAND ONLY FRESHWATER FLUX OBWG (MM/DAY)      (ANN ${year_rtdiag_start}-${yearm1})
C*XFIND       LAKES FRESHWATER FLUX OBWG (MM/DAY)          (ANN ${year_rtdiag_start}-${yearm1})" >> ic.xfind_ann
        echo "C*XSAVE       GLB FRESHWATER FLUX OBWG (MM/DAY)            (ANN ${year_rtdiag_start}-${year})
NEWNAM     OBWG
C*XSAVE       OCN FRESHWATER FLUX OBWG (MM/DAY)            (ANN ${year_rtdiag_start}-${year})
NEWNAM     OBWG
C*XSAVE       LAND FRESHWATER FLUX OBWG (MM/DAY)           (ANN ${year_rtdiag_start}-${year})
NEWNAM     OBWG
C*XSAVE       LAND ONLY FRESHWATER FLUX OBWG (MM/DAY)      (ANN ${year_rtdiag_start}-${year})
NEWNAM     OBWG
C*XSAVE       LAKES FRESHWATER FLUX OBWG (MM/DAY)          (ANN ${year_rtdiag_start}-${year})
NEWNAM     OBWG" >> ic.xsave_ann
        fi

# ------------------------------------ annual FSO

        x="FSO"
        if [ -s gs${x}01 ] ; then
        timavg gs${x}01 tgs${x} _ _ gs${x}02 gs${x}03 gs${x}04 gs${x}05 gs${x}06 gs${x}07 gs${x}08 gs${x}09 gs${x}10 gs${x}11 gs${x}12
#                                      global
        globavg tgs${x} _ gtgs${x}g

        rtdlist_ann="$rtdlist_ann gtgs${x}g"
        echo "C*XFIND       TOA INC S/W GLOBAL FSO (W/M2)                (ANN ${year_rtdiag_start}-${yearm1})" >> ic.xfind_ann
        echo "C*XSAVE       TOA INC S/W GLOBAL FSO (W/M2)                (ANN ${year_rtdiag_start}-${year})
NEWNAM      FSO" >> ic.xsave_ann
        fi

# ------------------------------------ annual FSLO

        x="FSLO"
        if [ -s gs${x}01 ] ; then
        timavg gs${x}01 tgs${x} _ _ gs${x}02 gs${x}03 gs${x}04 gs${x}05 gs${x}06 gs${x}07 gs${x}08 gs${x}09 gs${x}10 gs${x}11 gs${x}12
#                                      global
        globavg tgs${x} _ gtgs${x}g

        rtdlist_ann="$rtdlist_ann gtgs${x}g"
        echo "C*XFIND       TOA SOLAR/LW OVERLAP GLOBAL FSLO (W/M2)      (ANN ${year_rtdiag_start}-${yearm1})" >> ic.xfind_ann
        echo "C*XSAVE       TOA SOLAR/LW OVERLAP GLOBAL FSLO (W/M2)      (ANN ${year_rtdiag_start}-${year})
NEWNAM     FSLO" >> ic.xsave_ann
        fi

# ------------------------------------ annual FSR

        x="FSR"
        if [ -s gs${x}01 ] ; then
        timavg gs${x}01 tgs${x} _ _ gs${x}02 gs${x}03 gs${x}04 gs${x}05 gs${x}06 gs${x}07 gs${x}08 gs${x}09 gs${x}10 gs${x}11 gs${x}12
#                                      global
        globavg tgs${x} _ gtgs${x}g

        rtdlist_ann="$rtdlist_ann gtgs${x}g"
        echo "C*XFIND       TOA REFL S/W GLOBAL FSR (W/M2)               (ANN ${year_rtdiag_start}-${yearm1})" >> ic.xfind_ann
        echo "C*XSAVE       TOA REFL S/W GLOBAL FSR (W/M2)               (ANN ${year_rtdiag_start}-${year})
NEWNAM      FSR" >> ic.xsave_ann
        fi

# ------------------------------------ annual FSAG

        x="FSAG"
        if [ -s gs${x}01 ] ; then
        timavg gs${x}01 tgs${x} _ _ gs${x}02 gs${x}03 gs${x}04 gs${x}05 gs${x}06 gs${x}07 gs${x}08 gs${x}09 gs${x}10 gs${x}11 gs${x}12
#                                      global
        globavg tgs${x} _ gtgs${x}g

        rtdlist_ann="$rtdlist_ann gtgs${x}g"
        echo "C*XFIND       TOA NET S/W GLOBAL FSAG (W/M2)               (ANN ${year_rtdiag_start}-${yearm1})" >> ic.xfind_ann
        echo "C*XSAVE       TOA NET S/W GLOBAL FSAG (W/M2)               (ANN ${year_rtdiag_start}-${year})
NEWNAM     FSAG" >> ic.xsave_ann
        fi

# ------------------------------------ annual FLAG

        x="FLAG"
        if [ -s gs${x}01 ] ; then
        timavg gs${x}01 tgs${x} _ _ gs${x}02 gs${x}03 gs${x}04 gs${x}05 gs${x}06 gs${x}07 gs${x}08 gs${x}09 gs${x}10 gs${x}11 gs${x}12
#                                      global
        globavg tgs${x} _ gtgs${x}g

        rtdlist_ann="$rtdlist_ann gtgs${x}g"
        echo "C*XFIND       TOA NET L/W GLOBAL FLAG (W/M2)               (ANN ${year_rtdiag_start}-${yearm1})" >> ic.xfind_ann
        echo "C*XSAVE       TOA NET L/W GLOBAL FLAG (W/M2)               (ANN ${year_rtdiag_start}-${year})
NEWNAM     FLAG" >> ic.xsave_ann
        fi

# ------------------------------------ annual OLR

        x="OLR"
        if [ -s gs${x}01 ] ; then
        timavg gs${x}01 tgs${x} _ _ gs${x}02 gs${x}03 gs${x}04 gs${x}05 gs${x}06 gs${x}07 gs${x}08 gs${x}09 gs${x}10 gs${x}11 gs${x}12
#                                      global
        globavg tgs${x} _ gtgs${x}g

        rtdlist_ann="$rtdlist_ann gtgs${x}g"
        echo "C*XFIND       GLOBAL OUTGOING L/W OLR (W/M2)               (ANN ${year_rtdiag_start}-${yearm1})" >> ic.xfind_ann
        echo "C*XSAVE       GLOBAL OUTGOING L/W OLR (W/M2)               (ANN ${year_rtdiag_start}-${year})
NEWNAM      OLR" >> ic.xsave_ann
        fi

# ------------------------------------ annual FSS

        x="FSS"
        if [ -s gs${x}01 ] ; then
        timavg gs${x}01 tgs${x} _ _ gs${x}02 gs${x}03 gs${x}04 gs${x}05 gs${x}06 gs${x}07 gs${x}08 gs${x}09 gs${x}10 gs${x}11 gs${x}12
#                                      global
        globavg tgs${x} _ gtgs${x}g
#                                      ocean
        globavg tgs${x} _ gtgs${x}o ocean_frac
#                                      land
        globavg tgs${x} _ gtgs${x}l tland_frac
#                                      lakes
        globavg tgs${x} _ gtgs${x}r rlake_frac

        rtdlist_ann="$rtdlist_ann gtgs${x}g gtgs${x}o gtgs${x}l gtgs${x}r"
        echo "C*XFIND       GLB SFC INCIDENT S/W FSS (W/M2)              (ANN ${year_rtdiag_start}-${yearm1})
C*XFIND       OCN SFC INCIDENT S/W FSS (W/M2)              (ANN ${year_rtdiag_start}-${yearm1})
C*XFIND       LAND SFC INCIDENT S/W FSS (W/M2)             (ANN ${year_rtdiag_start}-${yearm1})
C*XFIND       LAKES SFC INCIDENT S/W FSS (W/M2)            (ANN ${year_rtdiag_start}-${yearm1})" >> ic.xfind_ann
        echo "C*XSAVE       GLB SFC INCIDENT S/W FSS (W/M2)              (ANN ${year_rtdiag_start}-${year})
NEWNAM      FSS
C*XSAVE       OCN SFC INCIDENT S/W FSS (W/M2)              (ANN ${year_rtdiag_start}-${year})
NEWNAM      FSS
C*XSAVE       LAND SFC INCIDENT S/W FSS (W/M2)             (ANN ${year_rtdiag_start}-${year})
NEWNAM      FSS
C*XSAVE       LAKES SFC INCIDENT S/W FSS (W/M2)            (ANN ${year_rtdiag_start}-${year})
NEWNAM      FSS" >> ic.xsave_ann
        fi

# ------------------------------------ annual FSSC

        x="FSSC"
        if [ -s gs${x}01 ] ; then
        timavg gs${x}01 tgs${x} _ _ gs${x}02 gs${x}03 gs${x}04 gs${x}05 gs${x}06 gs${x}07 gs${x}08 gs${x}09 gs${x}10 gs${x}11 gs${x}12
#                                      global
        globavg tgs${x} _ gtgs${x}g
#                                      ocean
        globavg tgs${x} _ gtgs${x}o ocean_frac
#                                      land
        globavg tgs${x} _ gtgs${x}l tland_frac
#                                      lakes
        globavg tgs${x} _ gtgs${x}r rlake_frac

        rtdlist_ann="$rtdlist_ann gtgs${x}g gtgs${x}o gtgs${x}l gtgs${x}r"
        echo "C*XFIND       GLB SFC INC S/W CLEAR SKY FSSC (W/M2)        (ANN ${year_rtdiag_start}-${yearm1})
C*XFIND       OCN SFC INC S/W CLEAR SKY FSSC (W/M2)        (ANN ${year_rtdiag_start}-${yearm1})
C*XFIND       LAND SFC INC S/W CLEAR SKY FSSC (W/M2)       (ANN ${year_rtdiag_start}-${yearm1})
C*XFIND       LAKES SFC INC S/W CLEAR SKY FSSC (W/M2)      (ANN ${year_rtdiag_start}-${yearm1})" >> ic.xfind_ann
        echo "C*XSAVE       GLB SFC INC S/W CLEAR SKY FSSC (W/M2)        (ANN ${year_rtdiag_start}-${year})
NEWNAM     FSSC
C*XSAVE       OCN SFC INC S/W CLEAR SKY FSSC (W/M2)        (ANN ${year_rtdiag_start}-${year})
NEWNAM     FSSC
C*XSAVE       LAND SFC INC S/W CLEAR SKY FSSC (W/M2)       (ANN ${year_rtdiag_start}-${year})
NEWNAM     FSSC
C*XSAVE       LAKES SFC INC S/W CLEAR SKY FSSC (W/M2)      (ANN ${year_rtdiag_start}-${year})
NEWNAM     FSSC" >> ic.xsave_ann
        fi

# ------------------------------------ annual FSG

        x="FSG"
        if [ -s gs${x}01 ] ; then
        timavg gs${x}01 tgs${x} _ _ gs${x}02 gs${x}03 gs${x}04 gs${x}05 gs${x}06 gs${x}07 gs${x}08 gs${x}09 gs${x}10 gs${x}11 gs${x}12
#                                      global
        globavg tgs${x} _ gtgs${x}g
#                                      ocean
        globavg tgs${x} _ gtgs${x}o ocean_frac
#                                      land
        globavg tgs${x} _ gtgs${x}l tland_frac
#                                      lakes
        globavg tgs${x} _ gtgs${x}r rlake_frac

        rtdlist_ann="$rtdlist_ann gtgs${x}g gtgs${x}o gtgs${x}l gtgs${x}r"
        echo "C*XFIND       GLB SFC NET S/W FSG (W/M2)                   (ANN ${year_rtdiag_start}-${yearm1})
C*XFIND       OCN SFC NET S/W FSG (W/M2)                   (ANN ${year_rtdiag_start}-${yearm1})
C*XFIND       LAND SFC NET S/W FSG (W/M2)                  (ANN ${year_rtdiag_start}-${yearm1})
C*XFIND       LAKES SFC NET S/W FSG (W/M2)                 (ANN ${year_rtdiag_start}-${yearm1})" >> ic.xfind_ann
        echo "C*XSAVE       GLB SFC NET S/W FSG (W/M2)                   (ANN ${year_rtdiag_start}-${year})
NEWNAM      FSG
C*XSAVE       OCN SFC NET S/W FSG (W/M2)                   (ANN ${year_rtdiag_start}-${year})
NEWNAM      FSG
C*XSAVE       LAND SFC NET S/W FSG (W/M2)                  (ANN ${year_rtdiag_start}-${year})
NEWNAM      FSG
C*XSAVE       LAKES SFC NET S/W FSG (W/M2)                 (ANN ${year_rtdiag_start}-${year})
NEWNAM      FSG" >> ic.xsave_ann
        fi

# ------------------------------------ annual FSGC

        x="FSGC"
        if [ -s gs${x}01 ] ; then
        timavg gs${x}01 tgs${x} _ _ gs${x}02 gs${x}03 gs${x}04 gs${x}05 gs${x}06 gs${x}07 gs${x}08 gs${x}09 gs${x}10 gs${x}11 gs${x}12
#                                      global
        globavg tgs${x} _ gtgs${x}g
#                                      ocean
        globavg tgs${x} _ gtgs${x}o ocean_frac
#                                      land
        globavg tgs${x} _ gtgs${x}l tland_frac
#                                      lakes
        globavg tgs${x} _ gtgs${x}r rlake_frac

        rtdlist_ann="$rtdlist_ann gtgs${x}g gtgs${x}o gtgs${x}l gtgs${x}r"
        echo "C*XFIND       GLB SFC NET S/W CLEAR SKY FSGC (W/M2)        (ANN ${year_rtdiag_start}-${yearm1})
C*XFIND       OCN SFC NET S/W CLEAR SKY FSGC (W/M2)        (ANN ${year_rtdiag_start}-${yearm1})
C*XFIND       LAND SFC NET S/W CLEAR SKY FSGC (W/M2)       (ANN ${year_rtdiag_start}-${yearm1})
C*XFIND       LAKES SFC NET S/W CLEAR SKY FSGC (W/M2)      (ANN ${year_rtdiag_start}-${yearm1})" >> ic.xfind_ann
        echo "C*XSAVE       GLB SFC NET S/W CLEAR SKY FSGC (W/M2)        (ANN ${year_rtdiag_start}-${year})
NEWNAM     FSGC
C*XSAVE       OCN SFC NET S/W CLEAR SKY FSGC (W/M2)        (ANN ${year_rtdiag_start}-${year})
NEWNAM     FSGC
C*XSAVE       LAND SFC NET S/W CLEAR SKY FSGC (W/M2)       (ANN ${year_rtdiag_start}-${year})
NEWNAM     FSGC
C*XSAVE       LAKES SFC NET S/W CLEAR SKY FSGC (W/M2)      (ANN ${year_rtdiag_start}-${year})
NEWNAM     FSGC" >> ic.xsave_ann
        fi

# ------------------------------------ annual FDL

        x="FDL"
        if [ -s gs${x}01 ] ; then
        timavg gs${x}01 tgs${x} _ _ gs${x}02 gs${x}03 gs${x}04 gs${x}05 gs${x}06 gs${x}07 gs${x}08 gs${x}09 gs${x}10 gs${x}11 gs${x}12
#                                      global
        globavg tgs${x} _ gtgs${x}g
#                                      ocean
        globavg tgs${x} _ gtgs${x}o ocean_frac
#                                      land
        globavg tgs${x} _ gtgs${x}l tland_frac
#                                      lakes
        globavg tgs${x} _ gtgs${x}r rlake_frac

        rtdlist_ann="$rtdlist_ann gtgs${x}g gtgs${x}o gtgs${x}l gtgs${x}r"
        echo "C*XFIND       GLB SFC INCIDENT L/W FDL (W/M2)              (ANN ${year_rtdiag_start}-${yearm1})
C*XFIND       OCN SFC INCIDENT L/W FDL (W/M2)              (ANN ${year_rtdiag_start}-${yearm1})
C*XFIND       LAND SFC INCIDENT L/W FDL (W/M2)             (ANN ${year_rtdiag_start}-${yearm1})
C*XFIND       LAKES SFC INCIDENT L/W FDL (W/M2)            (ANN ${year_rtdiag_start}-${yearm1})" >> ic.xfind_ann
        echo "C*XSAVE       GLB SFC INCIDENT L/W FDL (W/M2)              (ANN ${year_rtdiag_start}-${year})
NEWNAM      FDL
C*XSAVE       OCN SFC INCIDENT L/W FDL (W/M2)              (ANN ${year_rtdiag_start}-${year})
NEWNAM      FDL
C*XSAVE       LAND SFC INCIDENT L/W FDL (W/M2)             (ANN ${year_rtdiag_start}-${year})
NEWNAM      FDL
C*XSAVE       LAKES SFC INCIDENT L/W FDL (W/M2)            (ANN ${year_rtdiag_start}-${year})
NEWNAM      FDL" >> ic.xsave_ann
        fi

# ------------------------------------ annual FLG

        x="FLG"
        if [ -s gs${x}01 ] ; then
        timavg gs${x}01 tgs${x} _ _ gs${x}02 gs${x}03 gs${x}04 gs${x}05 gs${x}06 gs${x}07 gs${x}08 gs${x}09 gs${x}10 gs${x}11 gs${x}12
#                                      global
        globavg tgs${x} _ gtgs${x}g
#                                      ocean
        globavg tgs${x} _ gtgs${x}o ocean_frac
#                                      land
        globavg tgs${x} _ gtgs${x}l tland_frac
#                                      lakes
        globavg tgs${x} _ gtgs${x}r rlake_frac

        rtdlist_ann="$rtdlist_ann gtgs${x}g gtgs${x}o gtgs${x}l gtgs${x}r"
        echo "C*XFIND       GLB SFC NET L/W FLG (W/M2)                   (ANN ${year_rtdiag_start}-${yearm1})
C*XFIND       OCN SFC NET L/W FLG (W/M2)                   (ANN ${year_rtdiag_start}-${yearm1})
C*XFIND       LAND SFC NET L/W FLG (W/M2)                  (ANN ${year_rtdiag_start}-${yearm1})
C*XFIND       LAKES SFC NET L/W FLG (W/M2)                 (ANN ${year_rtdiag_start}-${yearm1})" >> ic.xfind_ann
        echo "C*XSAVE       GLB SFC NET L/W FLG (W/M2)                   (ANN ${year_rtdiag_start}-${year})
NEWNAM      FLG
C*XSAVE       OCN SFC NET L/W FLG (W/M2)                   (ANN ${year_rtdiag_start}-${year})
NEWNAM      FLG
C*XSAVE       LAND SFC NET L/W FLG (W/M2)                  (ANN ${year_rtdiag_start}-${year})
NEWNAM      FLG
C*XSAVE       LAKES SFC NET L/W FLG (W/M2)                 (ANN ${year_rtdiag_start}-${year})
NEWNAM      FLG" >> ic.xsave_ann
        fi

# ------------------------------------ annual FLGC

        x="FLGC"
        if [ -s gs${x}01 ] ; then
        timavg gs${x}01 tgs${x} _ _ gs${x}02 gs${x}03 gs${x}04 gs${x}05 gs${x}06 gs${x}07 gs${x}08 gs${x}09 gs${x}10 gs${x}11 gs${x}12
#                                      global
        globavg tgs${x} _ gtgs${x}g
#                                      ocean
        globavg tgs${x} _ gtgs${x}o ocean_frac
#                                      land
        globavg tgs${x} _ gtgs${x}l tland_frac
#                                      lakes
        globavg tgs${x} _ gtgs${x}r rlake_frac

        rtdlist_ann="$rtdlist_ann gtgs${x}g gtgs${x}o gtgs${x}l gtgs${x}r"
        echo "C*XFIND       GLB SFC NET L/W CLEAR SKY FLGC (W/M2)        (ANN ${year_rtdiag_start}-${yearm1})
C*XFIND       OCN SFC NET L/W CLEAR SKY FLGC (W/M2)        (ANN ${year_rtdiag_start}-${yearm1})
C*XFIND       LAND SFC NET L/W CLEAR SKY FLGC (W/M2)       (ANN ${year_rtdiag_start}-${yearm1})
C*XFIND       LAKES SFC NET L/W CLEAR SKY FLGC (W/M2)      (ANN ${year_rtdiag_start}-${yearm1})" >> ic.xfind_ann
        echo "C*XSAVE       GLB SFC NET L/W CLEAR SKY FLGC (W/M2)        (ANN ${year_rtdiag_start}-${year})
NEWNAM     FLGC
C*XSAVE       OCN SFC NET L/W CLEAR SKY FLGC (W/M2)        (ANN ${year_rtdiag_start}-${year})
NEWNAM     FLGC
C*XSAVE       LAND SFC NET L/W CLEAR SKY FLGC (W/M2)       (ANN ${year_rtdiag_start}-${year})
NEWNAM     FLGC
C*XSAVE       LAKES SFC NET L/W CLEAR SKY FLGC (W/M2)      (ANN ${year_rtdiag_start}-${year})
NEWNAM     FLGC" >> ic.xsave_ann
        fi

# ------------------------------------ annual HFS

        x="HFS"
        if [ -s gs${x}01 ] ; then
        timavg gs${x}01 tgs${x} _ _ gs${x}02 gs${x}03 gs${x}04 gs${x}05 gs${x}06 gs${x}07 gs${x}08 gs${x}09 gs${x}10 gs${x}11 gs${x}12
#                                      global
        globavg tgs${x} _ gtgs${x}g
#                                      ocean
        globavg tgs${x} _ gtgs${x}o ocean_frac
#                                      land
        globavg tgs${x} _ gtgs${x}l tland_frac
#                                      lakes
        globavg tgs${x} _ gtgs${x}r rlake_frac

        rtdlist_ann="$rtdlist_ann gtgs${x}g gtgs${x}o gtgs${x}l gtgs${x}r"
        echo "C*XFIND       GLB SFC SENSIBLE HEAT UP HFS (W/M2)          (ANN ${year_rtdiag_start}-${yearm1})
C*XFIND       OCN SFC SENSIBLE HEAT UP HFS (W/M2)          (ANN ${year_rtdiag_start}-${yearm1})
C*XFIND       LAND SFC SENSIBLE HEAT UP HFS (W/M2)         (ANN ${year_rtdiag_start}-${yearm1})
C*XFIND       LAKES SFC SENSIBLE HEAT UP HFS (W/M2)        (ANN ${year_rtdiag_start}-${yearm1})" >> ic.xfind_ann
        echo "C*XSAVE       GLB SFC SENSIBLE HEAT UP HFS (W/M2)          (ANN ${year_rtdiag_start}-${year})
NEWNAM      HFS
C*XSAVE       OCN SFC SENSIBLE HEAT UP HFS (W/M2)          (ANN ${year_rtdiag_start}-${year})
NEWNAM      HFS
C*XSAVE       LAND SFC SENSIBLE HEAT UP HFS (W/M2)         (ANN ${year_rtdiag_start}-${year})
NEWNAM      HFS
C*XSAVE       LAKES SFC SENSIBLE HEAT UP HFS (W/M2)        (ANN ${year_rtdiag_start}-${year})
NEWNAM      HFS" >> ic.xsave_ann
        fi

# ------------------------------------ annual HFL

        x="HFL"
        if [ -s gs${x}01 ] ; then
        timavg gs${x}01 tgs${x} _ _ gs${x}02 gs${x}03 gs${x}04 gs${x}05 gs${x}06 gs${x}07 gs${x}08 gs${x}09 gs${x}10 gs${x}11 gs${x}12
#                                      global
        globavg tgs${x} _ gtgs${x}g
#                                      ocean
        globavg tgs${x} _ gtgs${x}o ocean_frac
#                                      land
        globavg tgs${x} _ gtgs${x}l tland_frac
#                                      lakes
        globavg tgs${x} _ gtgs${x}r rlake_frac

        rtdlist_ann="$rtdlist_ann gtgs${x}g gtgs${x}o gtgs${x}l gtgs${x}r"
        echo "C*XFIND       GLB SFC LATENT HEAT UP HFL (W/M2)            (ANN ${year_rtdiag_start}-${yearm1})
C*XFIND       OCN SFC LATENT HEAT UP HFL (W/M2)            (ANN ${year_rtdiag_start}-${yearm1})
C*XFIND       LAND SFC LATENT HEAT UP HFL (W/M2)           (ANN ${year_rtdiag_start}-${yearm1})
C*XFIND       LAKES SFC LATENT HEAT UP HFL (W/M2)          (ANN ${year_rtdiag_start}-${yearm1})" >> ic.xfind_ann
        echo "C*XSAVE       GLB SFC LATENT HEAT UP HFL (W/M2)            (ANN ${year_rtdiag_start}-${year})
NEWNAM      HFL
C*XSAVE       OCN SFC LATENT HEAT UP HFL (W/M2)            (ANN ${year_rtdiag_start}-${year})
NEWNAM      HFL
C*XSAVE       LAND SFC LATENT HEAT UP HFL (W/M2)           (ANN ${year_rtdiag_start}-${year})
NEWNAM      HFL
C*XSAVE       LAKES SFC LATENT HEAT UP HFL (W/M2)          (ANN ${year_rtdiag_start}-${year})
NEWNAM      HFL" >> ic.xsave_ann
        fi

# ------------------------------------ annual global FSD (surface diffuse downward shortwave flux)

        x="FSD"
        if [ -s gs${x}01 ] ; then
        timavg gs${x}01 tgs${x} _ _ gs${x}02 gs${x}03 gs${x}04 gs${x}05 gs${x}06 gs${x}07 gs${x}08 gs${x}09 gs${x}10 gs${x}11 gs${x}12
#                                      global
        globavg tgs${x} _ gtgs${x}g

        rtdlist_ann="$rtdlist_ann gtgs${x}g"
        echo "C*XFIND       GLB SFC DIFFUSE S/W FLUX FSD (W/M2)          (ANN ${year_rtdiag_start}-${yearm1})" >> ic.xfind_ann
        echo "C*XSAVE       GLB SFC DIFFUSE S/W FLUX FSD (W/M2)          (ANN ${year_rtdiag_start}-${year})
NEWNAM      FSD" >> ic.xsave_ann
        fi

# ------------------------------------ annual global FSDC

        x="FSDC"
        if [ -s gs${x}01 ] ; then
        timavg gs${x}01 tgs${x} _ _ gs${x}02 gs${x}03 gs${x}04 gs${x}05 gs${x}06 gs${x}07 gs${x}08 gs${x}09 gs${x}10 gs${x}11 gs${x}12
#                                      global
        globavg tgs${x} _ gtgs${x}g

        rtdlist_ann="$rtdlist_ann gtgs${x}g"
        echo "C*XFIND       GLB SFC DIFF.S/W C.SKY FLUX FSDC (W/M2)      (ANN ${year_rtdiag_start}-${yearm1})" >> ic.xfind_ann
        echo "C*XSAVE       GLB SFC DIFF.S/W C.SKY FLUX FSDC (W/M2)      (ANN ${year_rtdiag_start}-${year})
NEWNAM     FSDC" >> ic.xsave_ann
        fi

# ------------------------------------ annual CLDT

        x="CLDT"
        if [ -s gs${x}01 ] ; then
        timavg gs${x}01 tgs${x} _ _ gs${x}02 gs${x}03 gs${x}04 gs${x}05 gs${x}06 gs${x}07 gs${x}08 gs${x}09 gs${x}10 gs${x}11 gs${x}12
#                                      global
        globavg tgs${x} _ gtgs${x}g
#                                      land
        globavg tgs${x} _ gtgs${x}l tland_frac
#                                      ocean
        globavg tgs${x} _ gtgs${x}o ocean_frac

        rtdlist_ann="$rtdlist_ann gtgs${x}g gtgs${x}l gtgs${x}o"
        echo "C*XFIND       GLOBAL TOTAL CLOUD FRACTION CLDT             (ANN ${year_rtdiag_start}-${yearm1})
C*XFIND       LAND TOTAL CLOUD FRACTION CLDT               (ANN ${year_rtdiag_start}-${yearm1})
C*XFIND       OCEAN TOTAL CLOUD FRACTION CLDT              (ANN ${year_rtdiag_start}-${yearm1})" >> ic.xfind_ann
        echo "C*XSAVE       GLOBAL TOTAL CLOUD FRACTION CLDT             (ANN ${year_rtdiag_start}-${year})
NEWNAM     CLDT
C*XSAVE       LAND TOTAL CLOUD FRACTION CLDT               (ANN ${year_rtdiag_start}-${year})
NEWNAM     CLDT
C*XSAVE       OCEAN TOTAL CLOUD FRACTION CLDT              (ANN ${year_rtdiag_start}-${year})
NEWNAM     CLDT" >> ic.xsave_ann
        fi

# ------------------------------------ annual CLDO

        x="CLDO"
        if [ -s gs${x}01 ] ; then
        timavg gs${x}01 tgs${x} _ _ gs${x}02 gs${x}03 gs${x}04 gs${x}05 gs${x}06 gs${x}07 gs${x}08 gs${x}09 gs${x}10 gs${x}11 gs${x}12
#                                      global
        globavg tgs${x} _ gtgs${x}g
#                                      land
        globavg tgs${x} _ gtgs${x}l tland_frac
#                                      ocean
        globavg tgs${x} _ gtgs${x}o ocean_frac

        rtdlist_ann="$rtdlist_ann gtgs${x}g gtgs${x}l gtgs${x}o"
        echo "C*XFIND       GLOBAL TOTAL CLOUD FRACTION CLDO             (ANN ${year_rtdiag_start}-${yearm1})
C*XFIND       LAND TOTAL CLOUD FRACTION CLDO               (ANN ${year_rtdiag_start}-${yearm1})
C*XFIND       OCEAN TOTAL CLOUD FRACTION CLDO              (ANN ${year_rtdiag_start}-${yearm1})" >> ic.xfind_ann
        echo "C*XSAVE       GLOBAL TOTAL CLOUD FRACTION CLDO             (ANN ${year_rtdiag_start}-${year})
NEWNAM     CLDO
C*XSAVE       LAND TOTAL CLOUD FRACTION CLDO               (ANN ${year_rtdiag_start}-${year})
NEWNAM     CLDO
C*XSAVE       OCEAN TOTAL CLOUD FRACTION CLDO              (ANN ${year_rtdiag_start}-${year})
NEWNAM     CLDO" >> ic.xsave_ann
        fi

# ------------------------------------ annual global PWAT

        x="PWAT"
        if [ -s gs${x}01 ] ; then
        timavg gs${x}01 tgs${x} _ _ gs${x}02 gs${x}03 gs${x}04 gs${x}05 gs${x}06 gs${x}07 gs${x}08 gs${x}09 gs${x}10 gs${x}11 gs${x}12
#                                      global
        globavg tgs${x} _ gtgs${x}g

        rtdlist_ann="$rtdlist_ann gtgs${x}g"
        echo "C*XFIND       GLOBAL PRECIPITABLE WATER PWAT (KG/M2)       (ANN ${year_rtdiag_start}-${yearm1})" >> ic.xfind_ann
        echo "C*XSAVE       GLOBAL PRECIPITABLE WATER PWAT (KG/M2)       (ANN ${year_rtdiag_start}-${year})
NEWNAM     PWAT" >> ic.xsave_ann
        fi

# ------------------------------------ annual global surface CO2 - this is a scalar value in runs with specified CO2 concentrations

        x="CO2"
        if [ -s gs${x}01 ] ; then
        timavg gs${x}01 gtavg _ _ gs${x}02 gs${x}03 gs${x}04 gs${x}05 gs${x}06 gs${x}07 gs${x}08 gs${x}09 gs${x}10 gs${x}11 gs${x}12
#                                      convert to ppm
        echo "C*XLIN        1.E+06" | ccc xlin gtavg gtgs${x}

        rtdlist_ann="$rtdlist_ann gtgs${x}"
        echo "C*XFIND       SURFACE CO2 CONCENTRATION (PPMV)             (ANN ${year_rtdiag_start}-${yearm1})" >> ic.xfind_ann
        echo "C*XSAVE       SURFACE CO2 CONCENTRATION (PPMV)             (ANN ${year_rtdiag_start}-${year})
NEWNAM      CO2" >> ic.xsave_ann
        fi

# ------------------------------------ annual global surface CH4 - this is a scalar value

        x="CH4"
        if [ -s gs${x}01 ] ; then
        timavg gs${x}01 gtavg _ _ gs${x}02 gs${x}03 gs${x}04 gs${x}05 gs${x}06 gs${x}07 gs${x}08 gs${x}09 gs${x}10 gs${x}11 gs${x}12
#                                      convert to ppb
        echo "C*XLIN        1.E+09" | ccc xlin gtavg gtgs${x}

        rtdlist_ann="$rtdlist_ann gtgs${x}"
        echo "C*XFIND       SURFACE CH4 CONCENTRATION (PPB)              (ANN ${year_rtdiag_start}-${yearm1})" >> ic.xfind_ann
        echo "C*XSAVE       SURFACE CH4 CONCENTRATION (PPB)              (ANN ${year_rtdiag_start}-${year})
NEWNAM      CH4" >> ic.xsave_ann
        fi

# ------------------------------------ annual global surface N2O - this is a scalar value

        x="N2O"
        if [ -s gs${x}01 ] ; then
        timavg gs${x}01 gtavg _ _ gs${x}02 gs${x}03 gs${x}04 gs${x}05 gs${x}06 gs${x}07 gs${x}08 gs${x}09 gs${x}10 gs${x}11 gs${x}12
#                                      convert to ppb
        echo "C*XLIN        1.E+09" | ccc xlin gtavg gtgs${x}

        rtdlist_ann="$rtdlist_ann gtgs${x}"
        echo "C*XFIND       SURFACE N2O CONCENTRATION (PPB)              (ANN ${year_rtdiag_start}-${yearm1})" >> ic.xfind_ann
        echo "C*XSAVE       SURFACE N2O CONCENTRATION (PPB)              (ANN ${year_rtdiag_start}-${year})
NEWNAM      N2O" >> ic.xsave_ann
        fi

# ------------------------------------ annual global burdens of non-CO2 tracers

        i2=1
        while [ $i2 -le $ntrac ] ; do
#                                      determine tracer name
        i2=`echo $i2 | awk '{printf "%02i", $1}'` # 2-digit tracer number
        eval trac=\${it$i2}
        x="VI${i2}"
        if [ "$trac" != "CO2" -a -s gs${x}01 ] ; then
        timavg gs${x}01 tgs${x} _ _ gs${x}02 gs${x}03 gs${x}04 gs${x}05 gs${x}06 gs${x}07 gs${x}08 gs${x}09 gs${x}10 gs${x}11 gs${x}12
#                                      global
        globavg tgs${x} _ gtavg
#                                      convert Kg m-2 to Gg = 1e6 kg
        gmlt gtavg globe_area_km2 gtgs${x}g

        rtdlist_ann="$rtdlist_ann gtgs${x}g"
        if [ "$trac" = "SO2" -o "$trac" = "SO4" -o "$trac" = "DMS" -o "$trac" = "HPO" ] ; then
          echo "C*XFIND       $trac BURDEN GLOBAL (1E6KG-S)                  (ANN ${year_rtdiag_start}-${yearm1})" >> ic.xfind_ann
          echo "C*XSAVE       $trac BURDEN GLOBAL (1E6KG-S)                  (ANN ${year_rtdiag_start}-${year})
NEWNAM     VI${i2}" >> ic.xsave_ann
        else
          echo "C*XFIND       $trac BURDEN GLOBAL (1E6KG)                    (ANN ${year_rtdiag_start}-${yearm1})" >> ic.xfind_ann
          echo "C*XSAVE       $trac BURDEN GLOBAL (1E6KG)                    (ANN ${year_rtdiag_start}-${year})
NEWNAM     VI${i2}" >> ic.xsave_ann
        fi
        fi
        i2=`expr $i2 + 1`
        done # $ntrac

# ------------------------------------ PLA tracers

        if [ "$PLAtracers" = "on" ] ; then

# ------------------------------------ annual global ammonium sulphate burden (BAS=VI15+VI18+VI21)

        x="BAS"
        if [ -s gs${x}01 ] ; then
        timavg gs${x}01 tgs${x} _ _ gs${x}02 gs${x}03 gs${x}04 gs${x}05 gs${x}06 gs${x}07 gs${x}08 gs${x}09 gs${x}10 gs${x}11 gs${x}12
#                                      global
        globavg tgs${x} _ gtavg
#                                      convert Kg m-2 to Gg = 1e6 kg
        gmlt gtavg globe_area_km2 gtgs${x}g

        rtdlist_ann="$rtdlist_ann gtgs${x}g"
        echo "C*XFIND       AMMONIUM SULPHATE BURDEN (1E6KG)             (ANN ${year_rtdiag_start}-${yearm1})" >> ic.xfind_ann
        echo "C*XSAVE       AMMONIUM SULPHATE BURDEN (1E6KG)             (ANN ${year_rtdiag_start}-${year})
NEWNAM      BAS" >> ic.xsave_ann
        fi

# ------------------------------------ organic matter burden (BOA=VI12+VI13+VI16+VI19)

        x="BOA"
        if [ -s gs${x}01 ] ; then
        timavg gs${x}01 tgs${x} _ _ gs${x}02 gs${x}03 gs${x}04 gs${x}05 gs${x}06 gs${x}07 gs${x}08 gs${x}09 gs${x}10 gs${x}11 gs${x}12
#                                      global
        globavg tgs${x} _ gtavg
#                                      convert Kg m-2 to Gg = 1e6 kg
        gmlt gtavg globe_area_km2 gtgs${x}g

        rtdlist_ann="$rtdlist_ann gtgs${x}g"
        echo "C*XFIND       ORGANIC MATTER BURDEN (1E6KG)                (ANN ${year_rtdiag_start}-${yearm1})" >> ic.xfind_ann
        echo "C*XSAVE       ORGANIC MATTER BURDEN (1E6KG)                (ANN ${year_rtdiag_start}-${year})
NEWNAM      BOA" >> ic.xsave_ann
        fi

# ------------------------------------ black carbon burden (BBC=VI11+VI14+VI17+VI20)

        x="BBC"
        if [ -s gs${x}01 ] ; then
        timavg gs${x}01 tgs${x} _ _ gs${x}02 gs${x}03 gs${x}04 gs${x}05 gs${x}06 gs${x}07 gs${x}08 gs${x}09 gs${x}10 gs${x}11 gs${x}12
#                                      global
        globavg tgs${x} _ gtavg
#                                      convert Kg m-2 to Gg = 1e6 kg
        gmlt gtavg globe_area_km2 gtgs${x}g

        rtdlist_ann="$rtdlist_ann gtgs${x}g"
        echo "C*XFIND       BLACK CARBON BURDEN (1E6KG)                  (ANN ${year_rtdiag_start}-${yearm1})" >> ic.xfind_ann
        echo "C*XSAVE       BLACK CARBON BURDEN (1E6KG)                  (ANN ${year_rtdiag_start}-${year})
NEWNAM      BBC" >> ic.xsave_ann
        fi

# ------------------------------------ sea salt burden (BSS=VI07+VI08)

        x="BSS"
        if [ -s gs${x}01 ] ; then
        timavg gs${x}01 tgs${x} _ _ gs${x}02 gs${x}03 gs${x}04 gs${x}05 gs${x}06 gs${x}07 gs${x}08 gs${x}09 gs${x}10 gs${x}11 gs${x}12
#                                      global
        globavg tgs${x} _ gtavg
#                                      convert Kg m-2 to Gg = 1e6 kg
        gmlt gtavg globe_area_km2 gtgs${x}g

        rtdlist_ann="$rtdlist_ann gtgs${x}g"
        echo "C*XFIND       SEA SALT BURDEN (1E6KG)                      (ANN ${year_rtdiag_start}-${yearm1})" >> ic.xfind_ann
        echo "C*XSAVE       SEA SALT BURDEN (1E6KG)                      (ANN ${year_rtdiag_start}-${year})
NEWNAM      BSS" >> ic.xsave_ann
        fi

# ------------------------------------ mineral dust burden (BMD=VI09+VI10)

        x="BMD"
        if [ -s gs${x}01 ] ; then
        timavg gs${x}01 tgs${x} _ _ gs${x}02 gs${x}03 gs${x}04 gs${x}05 gs${x}06 gs${x}07 gs${x}08 gs${x}09 gs${x}10 gs${x}11 gs${x}12
#                                      global
        globavg tgs${x} _ gtavg
#                                      convert Kg m-2 to Gg = 1e6 kg
        gmlt gtavg globe_area_km2 gtgs${x}g

        rtdlist_ann="$rtdlist_ann gtgs${x}g"
        echo "C*XFIND       MINERAL DUST BURDEN (1E6KG)                  (ANN ${year_rtdiag_start}-${yearm1})" >> ic.xfind_ann
        echo "C*XSAVE       MINERAL DUST BURDEN (1E6KG)                  (ANN ${year_rtdiag_start}-${year})
NEWNAM      BMD" >> ic.xsave_ann
        fi

# ------------------------------------ number burden internally mixed aerosol (BNI=VI21+VI22+VI23+VI24)

        x="BNI"
        if [ -s gs${x}01 ] ; then
        timavg gs${x}01 tgs${x} _ _ gs${x}02 gs${x}03 gs${x}04 gs${x}05 gs${x}06 gs${x}07 gs${x}08 gs${x}09 gs${x}10 gs${x}11 gs${x}12
#                                      global
        globavg tgs${x} _ gtavg
#                                      convert Kg m-2 to Gg = 1e6 kg
        gmlt gtavg globe_area_km2 gtgs${x}g

        rtdlist_ann="$rtdlist_ann gtgs${x}g"
        echo "C*XFIND       NUMBER BURDEN INTERN.MIXED AEROS.(1E6)       (ANN ${year_rtdiag_start}-${yearm1})" >> ic.xfind_ann
        echo "C*XSAVE       NUMBER BURDEN INTERN.MIXED AEROS.(1E6)       (ANN ${year_rtdiag_start}-${year})
NEWNAM      BNI" >> ic.xsave_ann
        fi

# ------------------------------------ number burden externally mixed aerosol (BNE=VI16+VI17+VI18+VI19)

        x="BNE"
        if [ -s gs${x}01 ] ; then
        timavg gs${x}01 tgs${x} _ _ gs${x}02 gs${x}03 gs${x}04 gs${x}05 gs${x}06 gs${x}07 gs${x}08 gs${x}09 gs${x}10 gs${x}11 gs${x}12
#                                      global
        globavg tgs${x} _ gtavg
#                                      convert Kg m-2 to Gg = 1e6 kg
        gmlt gtavg globe_area_km2 gtgs${x}g

        rtdlist_ann="$rtdlist_ann gtgs${x}g"
        echo "C*XFIND       NUMBER BURDEN EXTERN.MIXED AEROS.(1E6)       (ANN ${year_rtdiag_start}-${yearm1})" >> ic.xfind_ann
        echo "C*XSAVE       NUMBER BURDEN EXTERN.MIXED AEROS.(1E6)       (ANN ${year_rtdiag_start}-${year})
NEWNAM      BNE" >> ic.xsave_ann
        fi

        fi # PLAtracers=on

# ------------------------------------ annual nudging tendencies

# ------------------------------------ heat flux tendency due to difference in SST over sea water

        x="TBEG"
        if [ -s gs${x}01 ] ; then
        timavg gs${x}01 tgs${x} _ _ gs${x}02 gs${x}03 gs${x}04 gs${x}05 gs${x}06 gs${x}07 gs${x}08 gs${x}09 gs${x}10 gs${x}11 gs${x}12
#                                      ocean NH/SH
        globavg tgs${x} _ gtgs${x}onh ocean_frac_nh
        globavg tgs${x} _ gtgs${x}osh ocean_frac_sh

        rtdlist_ann="$rtdlist_ann gtgs${x}onh gtgs${x}osh"
        echo "C*XFIND       HEAT FLUX TENDENCY DUE TO SST (WATER/WATER)NH(ANN ${year_rtdiag_start}-${yearm1})
C*XFIND       HEAT FLUX TENDENCY DUE TO SST (WATER/WATER)SH(ANN ${year_rtdiag_start}-${yearm1})" >> ic.xfind_ann
        echo "C*XSAVE       HEAT FLUX TENDENCY DUE TO SST (WATER/WATER)NH(ANN ${year_rtdiag_start}-${year})
NEWNAM     TBEG
C*XSAVE       HEAT FLUX TENDENCY DUE TO SST (WATER/WATER)SH(ANN ${year_rtdiag_start}-${year})
NEWNAM     TBEG" >> ic.xsave_ann
        fi

# ------------------------------------ heat flux tendency due to difference in SST over water in model, ice in obs

        x="TBEI"
        if [ -s gs${x}01 ] ; then
        timavg gs${x}01 tgs${x} _ _ gs${x}02 gs${x}03 gs${x}04 gs${x}05 gs${x}06 gs${x}07 gs${x}08 gs${x}09 gs${x}10 gs${x}11 gs${x}12
#                                      ocean NH/SH
        globavg tgs${x} _ gtgs${x}onh ocean_frac_nh
        globavg tgs${x} _ gtgs${x}osh ocean_frac_sh

        rtdlist_ann="$rtdlist_ann gtgs${x}onh gtgs${x}osh"
        echo "C*XFIND       HEAT FLUX TENDENCY DUE TO SST (WATER/ICE) NH (ANN ${year_rtdiag_start}-${yearm1})
C*XFIND       HEAT FLUX TENDENCY DUE TO SST (WATER/ICE) SH (ANN ${year_rtdiag_start}-${yearm1})" >> ic.xfind_ann
        echo "C*XSAVE       HEAT FLUX TENDENCY DUE TO SST (WATER/ICE) NH (ANN ${year_rtdiag_start}-${year})
NEWNAM     TBEI
C*XSAVE       HEAT FLUX TENDENCY DUE TO SST (WATER/ICE) SH (ANN ${year_rtdiag_start}-${year})
NEWNAM     TBEI" >> ic.xsave_ann
        fi

# ------------------------------------ heat flux tendency due to difference in SST over ice in model, water in obs

        x="TBES"
        if [ -s gs${x}01 ] ; then
        timavg gs${x}01 tgs${x} _ _ gs${x}02 gs${x}03 gs${x}04 gs${x}05 gs${x}06 gs${x}07 gs${x}08 gs${x}09 gs${x}10 gs${x}11 gs${x}12
#                                      ocean NH/SH
        globavg tgs${x} _ gtgs${x}onh ocean_frac_nh
        globavg tgs${x} _ gtgs${x}osh ocean_frac_sh

        rtdlist_ann="$rtdlist_ann gtgs${x}onh gtgs${x}osh"
        echo "C*XFIND       HEAT FLUX TENDENCY DUE TO SST (ICE/WATER) NH (ANN ${year_rtdiag_start}-${yearm1})
C*XFIND       HEAT FLUX TENDENCY DUE TO SST (ICE/WATER) SH (ANN ${year_rtdiag_start}-${yearm1})" >> ic.xfind_ann
        echo "C*XSAVE       HEAT FLUX TENDENCY DUE TO SST (ICE/WATER) NH (ANN ${year_rtdiag_start}-${year})
NEWNAM     TBES
C*XSAVE       HEAT FLUX TENDENCY DUE TO SST (ICE/WATER) SH (ANN ${year_rtdiag_start}-${year})
NEWNAM     TBES" >> ic.xsave_ann
        fi

# ------------------------------------ heat flux tendency due to difference in SIC over sea water

        x="SBEG"
        if [ -s gs${x}01 ] ; then
        timavg gs${x}01 tgs${x} _ _ gs${x}02 gs${x}03 gs${x}04 gs${x}05 gs${x}06 gs${x}07 gs${x}08 gs${x}09 gs${x}10 gs${x}11 gs${x}12
#                                      ocean NH/SH
        globavg tgs${x} _ gtgs${x}onh ocean_frac_nh
        globavg tgs${x} _ gtgs${x}osh ocean_frac_sh

        rtdlist_ann="$rtdlist_ann gtgs${x}onh gtgs${x}osh"
        echo "C*XFIND       HEAT FLUX TENDENCY DUE TO SIC (ICE/ICE) NH   (ANN ${year_rtdiag_start}-${yearm1})
C*XFIND       HEAT FLUX TENDENCY DUE TO SIC (ICE/ICE) SH   (ANN ${year_rtdiag_start}-${yearm1})" >> ic.xfind_ann
        echo "C*XSAVE       HEAT FLUX TENDENCY DUE TO SIC (ICE/ICE) NH   (ANN ${year_rtdiag_start}-${year})
NEWNAM     SBEG
C*XSAVE       HEAT FLUX TENDENCY DUE TO SIC (ICE/ICE) SH   (ANN ${year_rtdiag_start}-${year})
NEWNAM     SBEG" >> ic.xsave_ann
        fi

# ------------------------------------ heat flux tendency due to difference in SIC over water in model, ice in obs

        x="SBEI"
        if [ -s gs${x}01 ] ; then
        timavg gs${x}01 tgs${x} _ _ gs${x}02 gs${x}03 gs${x}04 gs${x}05 gs${x}06 gs${x}07 gs${x}08 gs${x}09 gs${x}10 gs${x}11 gs${x}12
#                                      ocean NH/SH
        globavg tgs${x} _ gtgs${x}onh ocean_frac_nh
        globavg tgs${x} _ gtgs${x}osh ocean_frac_sh

        rtdlist_ann="$rtdlist_ann gtgs${x}onh gtgs${x}osh"
        echo "C*XFIND       HEAT FLUX TENDENCY DUE TO SIC (WATER/ICE) NH (ANN ${year_rtdiag_start}-${yearm1})
C*XFIND       HEAT FLUX TENDENCY DUE TO SIC (WATER/ICE) SH (ANN ${year_rtdiag_start}-${yearm1})" >> ic.xfind_ann
        echo "C*XSAVE       HEAT FLUX TENDENCY DUE TO SIC (WATER/ICE) NH (ANN ${year_rtdiag_start}-${year})
NEWNAM     SBEI
C*XSAVE       HEAT FLUX TENDENCY DUE TO SIC (WATER/ICE) SH (ANN ${year_rtdiag_start}-${year})
NEWNAM     SBEI" >> ic.xsave_ann
        fi

# ------------------------------------ heat flux tendency due to difference in SIC over ice in model, water in obs

        x="SBES"
        if [ -s gs${x}01 ] ; then
        timavg gs${x}01 tgs${x} _ _ gs${x}02 gs${x}03 gs${x}04 gs${x}05 gs${x}06 gs${x}07 gs${x}08 gs${x}09 gs${x}10 gs${x}11 gs${x}12
#                                      ocean NH/SH
        globavg tgs${x} _ gtgs${x}onh ocean_frac_nh
        globavg tgs${x} _ gtgs${x}osh ocean_frac_sh

        rtdlist_ann="$rtdlist_ann gtgs${x}onh gtgs${x}osh"
        echo "C*XFIND       HEAT FLUX TENDENCY DUE TO SIC (ICE/WATER) NH (ANN ${year_rtdiag_start}-${yearm1})
C*XFIND       HEAT FLUX TENDENCY DUE TO SIC (ICE/WATER) SH (ANN ${year_rtdiag_start}-${yearm1})" >> ic.xfind_ann
        echo "C*XSAVE       HEAT FLUX TENDENCY DUE TO SIC (ICE/WATER) NH (ANN ${year_rtdiag_start}-${year})
NEWNAM     SBES
C*XSAVE       HEAT FLUX TENDENCY DUE TO SIC (ICE/WATER) SH (ANN ${year_rtdiag_start}-${year})
NEWNAM     SBES" >> ic.xsave_ann
        fi

# ------------------------------------ sea ice concentration tendency

        x="TSNN"
        if [ -s gs${x}01 ] ; then
        timavg gs${x}01 tgs${x} _ _ gs${x}02 gs${x}03 gs${x}04 gs${x}05 gs${x}06 gs${x}07 gs${x}08 gs${x}09 gs${x}10 gs${x}11 gs${x}12
#                                      ocean NH/SH
        globavg tgs${x} _ gtgs${x}onh ocean_frac_nh
        globavg tgs${x} _ gtgs${x}osh ocean_frac_sh

        rtdlist_ann="$rtdlist_ann gtgs${x}onh gtgs${x}osh"
        echo "C*XFIND       SEAICE CONC TENDENCY DUE TO SICN NH          (ANN ${year_rtdiag_start}-${yearm1})
C*XFIND       SEAICE CONC TENDENCY DUE TO SICN SH          (ANN ${year_rtdiag_start}-${yearm1})" >> ic.xfind_ann
        echo "C*XSAVE       SEAICE CONC TENDENCY DUE TO SICN NH          (ANN ${year_rtdiag_start}-${year})
NEWNAM     TSNN
C*XSAVE       SEAICE CONC TENDENCY DUE TO SICN SH          (ANN ${year_rtdiag_start}-${year})
NEWNAM     TSNN" >> ic.xsave_ann
        fi

# ------------------------------------ fresh water flux tendency due to difference in SSS

        x="TBWG"
        if [ -s gs${x}01 ] ; then
        timavg gs${x}01 tgs${x} _ _ gs${x}02 gs${x}03 gs${x}04 gs${x}05 gs${x}06 gs${x}07 gs${x}08 gs${x}09 gs${x}10 gs${x}11 gs${x}12
#                                      ocean
        globavg tgs${x} _ gtgs${x}o ocean_frac

        rtdlist_ann="$rtdlist_ann gtgs${x}o"
        echo "C*XFIND       FRESHWATER FLUX TENDENCY DUE TO SSS          (ANN ${year_rtdiag_start}-${yearm1})" >> ic.xfind_ann
        echo "C*XSAVE       FRESHWATER FLUX TENDENCY DUE TO SSS          (ANN ${year_rtdiag_start}-${year})
NEWNAM     TBWG" >> ic.xsave_ann
        fi

# ------------------------------------ top soil layer temperature tendency

        x="TTG1"
        if [ -s gs${x}01 ] ; then
        timavg gs${x}01 tgs${x} _ _ gs${x}02 gs${x}03 gs${x}04 gs${x}05 gs${x}06 gs${x}07 gs${x}08 gs${x}09 gs${x}10 gs${x}11 gs${x}12
#                                      land
        globavg tgs${x} _ gtgs${x}l tland_frac

        rtdlist_ann="$rtdlist_ann gtgs${x}l"
        echo "C*XFIND       TG1 TENDENCY                                 (ANN ${year_rtdiag_start}-${yearm1})" >> ic.xfind_ann
        echo "C*XSAVE       TG1 TENDENCY                                 (ANN ${year_rtdiag_start}-${year})
NEWNAM     TTG1" >> ic.xsave_ann
        fi

# ------------------------------------ top soil layer moisture tendency (volumetric fraction)

        x="TWG1"
        if [ -s gs${x}01 ] ; then
        timavg gs${x}01 tgs${x} _ _ gs${x}02 gs${x}03 gs${x}04 gs${x}05 gs${x}06 gs${x}07 gs${x}08 gs${x}09 gs${x}10 gs${x}11 gs${x}12
#                                      land
        globavg tgs${x} _ gtgs${x}l tland_frac

        rtdlist_ann="$rtdlist_ann gtgs${x}l"
        echo "C*XFIND       WG1 TENDENCY                                 (ANN ${year_rtdiag_start}-${yearm1})" >> ic.xfind_ann
        echo "C*XSAVE       WG1 TENDENCY                                 (ANN ${year_rtdiag_start}-${year})
NEWNAM     TWG1" >> ic.xsave_ann
        fi

# ------------------------------------ spectral TEMP
        x="TEMP"
        if [ -s gss${x}01 ] ; then
        $RTEXTBLS/lopgm_r8i8/timavg gss${x}01 gtss${x} _ _ gss${x}02 gss${x}03 gss${x}04 gss${x}05 gss${x}06 gss${x}07 gss${x}08 gss${x}09 gss${x}10 gss${x}11 gss${x}12
        rtdlistadd=""
        lev=1
        while [ $lev -le $ilev ] ; do
          lev5=`echo $lev | awk '{printf "%05i", $1}'`
          rtdlistadd="$rtdlistadd gtss${x}l${lev}"
          echo "C*XFIND       TEMP($lev5) SPECTRAL                         (ANN ${year_rtdiag_start}-${yearm1})" >> ic.xfind_ann
          echo "C*XSAVE       TEMP($lev5) SPECTRAL                         (ANN ${year_rtdiag_start}-${year})
NEWNAM     TEMP" >> ic.xsave_ann
          lev=`expr $lev + 1`
        done
        rsplit gtss${x} $rtdlistadd
        rtdlist_ann="$rtdlist_ann $rtdlistadd"
        fi

# ------------------------------------ spectral ES
        x="ES"
        if [ -s ss${x}01 ] ; then
        $RTEXTBLS/lopgm_r8i8/timavg gss${x}01 gtss${x} _ _ gss${x}02 gss${x}03 gss${x}04 gss${x}05 gss${x}06 gss${x}07 gss${x}08 gss${x}09 gss${x}10 gss${x}11 gss${x}12
        rtdlistadd=""
        lev=1
        while [ $lev -le $levs ] ; do
          lev5=`echo $lev | awk '{printf "%05i", $1}'`
          rtdlistadd="$rtdlistadd gtss${x}l${lev}"
          echo "C*XFIND       ES($lev5) SPECTRAL                           (ANN ${year_rtdiag_start}-${yearm1})" >> ic.xfind_ann
          echo "C*XSAVE       ES($lev5) SPECTRAL                           (ANN ${year_rtdiag_start}-${year})
NEWNAM       ES" >> ic.xsave_ann
          lev=`expr $lev + 1`
        done
        rsplit gtss${x} $rtdlistadd
        rtdlist_ann="$rtdlist_ann $rtdlistadd"
        fi

# ------------------------------------ spectral TEMP tendency
        x="TMPN"
        if [ -s gss${x}01 ] ; then
        $RTEXTBLS/lopgm_r8i8/timavg gss${x}01 gtss${x} _ _ gss${x}02 gss${x}03 gss${x}04 gss${x}05 gss${x}06 gss${x}07 gss${x}08 gss${x}09 gss${x}10 gss${x}11 gss${x}12
        rtdlistadd=""
        lev=1
        while [ $lev -le $ilev ] ; do
          lev5=`echo $lev | awk '{printf "%05i", $1}'`
          rtdlistadd="$rtdlistadd gtss${x}l${lev}"
          echo "C*XFIND       TEMP($lev5) SPECTRAL TENDENCY                (ANN ${year_rtdiag_start}-${yearm1})" >> ic.xfind_ann
          echo "C*XSAVE       TEMP($lev5) SPECTRAL TENDENCY                (ANN ${year_rtdiag_start}-${year})
NEWNAM     TEMP" >> ic.xsave_ann
          lev=`expr $lev + 1`
        done
        rsplit gtss${x} $rtdlistadd
        rtdlist_ann="$rtdlist_ann $rtdlistadd"
        fi

# ------------------------------------ spectral ES tendency
        x="ESN"
        if [ -s ss${x}01 ] ; then
        $RTEXTBLS/lopgm_r8i8/timavg gss${x}01 gtss${x} _ _ gss${x}02 gss${x}03 gss${x}04 gss${x}05 gss${x}06 gss${x}07 gss${x}08 gss${x}09 gss${x}10 gss${x}11 gss${x}12
        rtdlistadd=""
        lev=1
        while [ $lev -le $levs ] ; do
          lev5=`echo $lev | awk '{printf "%05i", $1}'`
          rtdlistadd="$rtdlistadd gtss${x}l${lev}"
          echo "C*XFIND       ES($lev5) SPECTRAL TENDENCY                  (ANN ${year_rtdiag_start}-${yearm1})" >> ic.xfind_ann
          echo "C*XSAVE       ES($lev5) SPECTRAL TENDENCY                  (ANN ${year_rtdiag_start}-${year})
NEWNAM       ES" >> ic.xsave_ann
          lev=`expr $lev + 1`
        done
        rsplit gtss${x} $rtdlistadd
        rtdlist_ann="$rtdlist_ann $rtdlistadd"
        fi

# ------------------------------------ spectral TEMP reference
        x="TMPR"
        if [ -s gss${x}01 ] ; then
        $RTEXTBLS/lopgm_r8i8/timavg gss${x}01 gtss${x} _ _ gss${x}02 gss${x}03 gss${x}04 gss${x}05 gss${x}06 gss${x}07 gss${x}08 gss${x}09 gss${x}10 gss${x}11 gss${x}12
        rtdlistadd=""
        lev=1
        while [ $lev -le $ilev ] ; do
          lev5=`echo $lev | awk '{printf "%05i", $1}'`
          rtdlistadd="$rtdlistadd gtss${x}l${lev}"
          echo "C*XFIND       TEMP($lev5) SPECTRAL REFERENCE               (ANN ${year_rtdiag_start}-${yearm1})" >> ic.xfind_ann
          echo "C*XSAVE       TEMP($lev5) SPECTRAL REFERENCE               (ANN ${year_rtdiag_start}-${year})
NEWNAM     TEMP" >> ic.xsave_ann
          lev=`expr $lev + 1`
        done
        rsplit gtss${x} $rtdlistadd
        rtdlist_ann="$rtdlist_ann $rtdlistadd"
        fi

# ------------------------------------ spectral ES reference
        x="ESR"
        if [ -s ss${x}01 ] ; then
        $RTEXTBLS/lopgm_r8i8/timavg gss${x}01 gtss${x} _ _ gss${x}02 gss${x}03 gss${x}04 gss${x}05 gss${x}06 gss${x}07 gss${x}08 gss${x}09 gss${x}10 gss${x}11 gss${x}12
        rtdlistadd=""
        lev=1
        while [ $lev -le $levs ] ; do
          lev5=`echo $lev | awk '{printf "%05i", $1}'`
          rtdlistadd="$rtdlistadd gtss${x}l${lev}"
          echo "C*XFIND       ES($lev5) SPECTRAL REFERENCE                 (ANN ${year_rtdiag_start}-${yearm1})" >> ic.xfind_ann
          echo "C*XSAVE       ES($lev5) SPECTRAL REFERENCE                 (ANN ${year_rtdiag_start}-${year})
NEWNAM       ES" >> ic.xsave_ann
          lev=`expr $lev + 1`
        done
        rsplit gtss${x} $rtdlistadd
        rtdlist_ann="$rtdlist_ann $rtdlistadd"
        fi

        fi # $m=12
      fi # PhysA=on

# #################################### Atmospheric Carbon section

      if [ "$CarbA" = "on" ] ; then

# ************************************ Atmospheric Carbon annual section

        if [ ${m} -eq 01 ] ; then

# ------------------------------------ global CO2 beginning of year

        if [ -n "$iCO2" ] ; then
        x="VI${iCO2}"
#                                      CO2 burden at the beginning of a year
        echo "C* RCOPY           1         1" | ccc rcopy gs${x}${m} gs${x}${m}.day1
        globavg gs${x}${m}.day1 _ gtavg
#                                      convert BURDEN from Kg CO2/m2 to Pg C
#                                      510099699 km2 x 10^6(km2->m2) x (12.011/44.011) / 10^12 = 139.210822
        echo "C*XLIN    139.210822" | ccc xlin gtavg gtgs${x}day1

        rtdlist_ann="$rtdlist_ann gtgs${x}day1"
        echo "C*XFIND       ATMOSPHERIC CO2 BURDEN JAN 1 (PG C)          (ANN ${year_rtdiag_start}-${yearm1})" >> ic.xfind_ann
        echo "C*XSAVE       ATMOSPHERIC CO2 BURDEN JAN 1 (PG C)          (ANN ${year_rtdiag_start}-${year})
NEWNAM     VI${iCO2}" >> ic.xsave_ann
        fi
        fi # ${m} -eq 1

# ------------------------------------ monthly global surface CO2 MMR

        x="XL${iCO2}"
        timavg gs${x}${m} mgs${x}${m}
        globavg mgs${x}${m} _ gmgs${x}${m}
#                                      convert  surface CO2 MMR to PPMV
#                                      RMCO2  =  CO2_PPM  * 1.5188126 (in AGCM set_mmr.f)
#                                      CO2_PPM = RMCO2 / 1.5188126 x 10^6 = 658409.076
        echo "C*XLIN    658409.076" | ccc xlin gmgs${x}${m} gtgs${x}${m}

        rtdlist="$rtdlist gtgs${x}${m}"
        echo "C*XFIND       SURFACE CO2 CONCENTRATION (PPMV)             (${days} ${year_rtdiag_start}-${yearm1})" >> ic.xfind
        echo "C*XSAVE       SURFACE CO2 CONCENTRATION (PPMV)             (${days} ${year_rtdiag_start}-${year})
NEWNAM     XL${iCO2}" >> ic.xsave

        if [ ${m} -eq 12 ] ; then

# ------------------------------------ global CO2 burden end of year (Dec 31)

        if [ -n "$iCO2" ] ; then
        x="VI${iCO2}"
#                                      CO2 burden at the end of a year
        echo "C* RCOPY          31        31" | ccc rcopy gs${x}${m} gs${x}${m}.last
        globavg gs${x}${m}.last _ gtavg
#                                      convert BURDEN from Kg CO2/m2 to Pg C
#                                      510099699 km2 x 10^6(km2->m2) x (12.011/44.011) / 10^12 = 139.210822
        echo "C*XLIN    139.210822" | ccc xlin gtavg gtgs${x}

        rtdlist_ann="$rtdlist_ann gtgs${x}"
        echo "C*XFIND       ATMOSPHERIC CO2 BURDEN DEC31 (PG C)          (ANN ${year_rtdiag_start}-${yearm1})" >> ic.xfind_ann
        echo "C*XSAVE       ATMOSPHERIC CO2 BURDEN DEC31 (PG C)          (ANN ${year_rtdiag_start}-${year})
NEWNAM     VI${iCO2}" >> ic.xsave_ann

# ------------------------------------ annual global CO2 surface flux

        x="XF${iCO2}"
        timavg gs${x}01 tgs${x} _ _ gs${x}02 gs${x}03 gs${x}04 gs${x}05 gs${x}06 gs${x}07 gs${x}08 gs${x}09 gs${x}10 gs${x}11 gs${x}12
        globavg tgs${x} _ gtavg
#                                      convert CO2 FLUX from Kg CO2 m-2 sec-1 to Pg C/year
#                                      x 365 days x 86400 sec x 510099699 km2 x 10^6(km2->m2) x (12.011/44.011) / 10^12 = 4390152482
        echo "C*XLIN    4390152482" | ccc xlin gtavg gtgs${x}

        rtdlist_ann="$rtdlist_ann gtgs${x}"
        echo "C*XFIND       CO2 FLUX ENTERING ATMOS.(PG C/YR)            (ANN ${year_rtdiag_start}-${yearm1})" >> ic.xfind_ann
        echo "C*XSAVE       CO2 FLUX ENTERING ATMOS.(PG C/YR)            (ANN ${year_rtdiag_start}-${year})
NEWNAM     XF${iCO2}" >> ic.xsave_ann

# ------------------------------------ annual global surface CO2 MMR->PPMV

        x="XL${iCO2}"
        timavg gs${x}01 tgs${x} _ _ gs${x}02 gs${x}03 gs${x}04 gs${x}05 gs${x}06 gs${x}07 gs${x}08 gs${x}09 gs${x}10 gs${x}11 gs${x}12
        globavg tgs${x} _ gtavg
#                                      convert  surface CO2 MMR to PPMV
#                                      RMCO2  =  CO2_PPM  * 1.5188126 (in AGCM set_mmr.f)
#                                      CO2_PPM = RMCO2 / 1.5188126 x 10^6 = 658409.076
        echo "C*XLIN    658409.076" | ccc xlin gtavg gtgs${x}

        rtdlist_ann="$rtdlist_ann gtgs${x}"
        echo "C*XFIND       SURFACE CO2 CONCENTRATION (PPMV)             (ANN ${year_rtdiag_start}-${yearm1})" >> ic.xfind_ann
        echo "C*XSAVE       SURFACE CO2 CONCENTRATION (PPMV)             (ANN ${year_rtdiag_start}-${year})
NEWNAM     XL${iCO2}" >> ic.xsave_ann

# ------------------------------------ annual max/min global surface CO2 MMR (for monthly means)

        x="XL${iCO2}"
        timmax gmgs${x}01 gtmax gmgs${x}02 gmgs${x}03 gmgs${x}04 gmgs${x}05 gmgs${x}06 gmgs${x}07 gmgs${x}08 gmgs${x}09 gmgs${x}10 gmgs${x}11 gmgs${x}12
        timmin gmgs${x}01 gtmin gmgs${x}02 gmgs${x}03 gmgs${x}04 gmgs${x}05 gmgs${x}06 gmgs${x}07 gmgs${x}08 gmgs${x}09 gmgs${x}10 gmgs${x}11 gmgs${x}12
#                                      convert  surface CO2 MMR to PPMV
#                                      RMCO2  =  CO2_PPM  * 1.5188126 (in AGCM set_mmr.f)
#                                      CO2_PPM = RMCO2 / 1.5188126 x 10^6 = 658409.076
        echo "C*XLIN    658409.076" | ccc xlin gtmax gtgs${x}max
        echo "C*XLIN    658409.076" | ccc xlin gtmin gtgs${x}min

        rtdlist_ann="$rtdlist_ann gtgs${x}max gtgs${x}min"
        echo "C*XFIND       SURFACE CO2 CONCENTRATION (PPMV)             (MAX ${year_rtdiag_start}-${yearm1})
C*XFIND       SURFACE CO2 CONCENTRATION (PPMV)             (MIN ${year_rtdiag_start}-${yearm1})" >> ic.xfind_ann
        echo "C*XSAVE       SURFACE CO2 CONCENTRATION (PPMV)             (MAX ${year_rtdiag_start}-${year})
NEWNAM     XL${iCO2}
C*XSAVE       SURFACE CO2 CONCENTRATION (PPMV)             (MIN ${year_rtdiag_start}-${year})
NEWNAM     XL${iCO2}" >> ic.xsave_ann

        fi # $iCO2

# ------------------------------------ annual global ECO2

        x="ECO2"
#                                      find annual mean and global average
        timavg cm${x}01 tavg${x} _ _ cm${x}02 cm${x}03 cm${x}04 cm${x}05 cm${x}06 cm${x}07 cm${x}08 cm${x}09 cm${x}10 cm${x}11 cm${x}12
        globavg tavg${x} _ gtavg
#                                      convert ECO2 from Kg CO2 m-2 sec-1 to Pg C/year
#                                      x 365 days x 86400 sec x 510099699 km2 x 10^6(km2->m2) x (12.011/44.011) / 10^12 = 4390152482.
        echo "C*XLIN    4390152482" | ccc xlin gtavg gtcm$x

        rtdlist_ann="$rtdlist_ann gtcm$x"
        echo "C*XFIND       ANTHROPOGENIC EMISSIONS (PG C/YR)            (ANN ${year_rtdiag_start}-${yearm1})" >> ic.xfind_ann
        echo "C*XSAVE       ANTHROPOGENIC EMISSIONS (PG C/YR)            (ANN ${year_rtdiag_start}-${year})
NEWNAM     ECO2" >> ic.xsave_ann
        fi # $m=12
      fi # CarbA=on

# #################################### Physical Ocean section

      if [ "$PhysO" = "on" ] ; then

# ------------------------------------ monthly global upper layer TEMP

        x="TEMP"
        echo "  RCOPY            1         1" | ccc rcopy gz${x}${m} gz${x}1${m}
        globavw gz${x}1${m} bda1    _ _ ggz${x}1${m}
        globavw gz${x}1${m} bdatro1 _ _ ggz${x}tro1${m}
        globavw gz${x}1${m} bdanhe1 _ _ ggz${x}nhe1${m}
        globavw gz${x}1${m} bdashe1 _ _ ggz${x}she1${m}

        rtdlist="$rtdlist ggz${x}1${m} ggz${x}tro1${m} ggz${x}nhe1${m} ggz${x}she1${m}"
        echo "C*XFIND       OCEAN SFC TEMPERATURE (K)                    (${days} ${year_rtdiag_start}-${yearm1})
C*XFIND       OCEAN SFC TEMPERATURE 30S-30N (K)            (${days} ${year_rtdiag_start}-${yearm1})
C*XFIND       OCEAN SFC TEMPERATURE 30N-90N (K)            (${days} ${year_rtdiag_start}-${yearm1})
C*XFIND       OCEAN SFC TEMPERATURE 30S-90S (K)            (${days} ${year_rtdiag_start}-${yearm1})" >> ic.xfind
        echo "C*XSAVE       OCEAN SFC TEMPERATURE (K)                    (${days} ${year_rtdiag_start}-${year})
NEWNAM     TEMP
C*XSAVE       OCEAN SFC TEMPERATURE 30S-30N (K)            (${days} ${year_rtdiag_start}-${year})
NEWNAM     TEMP
C*XSAVE       OCEAN SFC TEMPERATURE 30N-90N (K)            (${days} ${year_rtdiag_start}-${year})
NEWNAM     TEMP
C*XSAVE       OCEAN SFC TEMPERATURE 30S-90S (K)            (${days} ${year_rtdiag_start}-${year})
NEWNAM     TEMP" >> ic.xsave

# ------------------------------------ NINO indices
        globavw gz${x}1${m} bda_nino3  _ _ ggz${x}nino3${m}
        globavw gz${x}1${m} bda_nino4  _ _ ggz${x}nino4${m}
        globavw gz${x}1${m} bda_nino34 _ _ ggz${x}nino34${m}
        globavw gz${x}1${m} bda_nino12 _ _ ggz${x}nino12${m}

        rtdlist="$rtdlist ggz${x}nino3${m} ggz${x}nino4${m} ggz${x}nino34${m} ggz${x}nino12${m}"
        echo "C*XFIND       OCEAN SFC TEMPERATURE NINO3 (K)              (${days} ${year_rtdiag_start}-${yearm1})
C*XFIND       OCEAN SFC TEMPERATURE NINO4 (K)              (${days} ${year_rtdiag_start}-${yearm1})
C*XFIND       OCEAN SFC TEMPERATURE NINO34 (K)             (${days} ${year_rtdiag_start}-${yearm1})
C*XFIND       OCEAN SFC TEMPERATURE NINO12 (K)             (${days} ${year_rtdiag_start}-${yearm1})" >> ic.xfind
        echo "C*XSAVE       OCEAN SFC TEMPERATURE NINO3 (K)              (${days} ${year_rtdiag_start}-${year})
NEWNAM     TEMP
C*XSAVE       OCEAN SFC TEMPERATURE NINO4 (K)              (${days} ${year_rtdiag_start}-${year})
NEWNAM     TEMP
C*XSAVE       OCEAN SFC TEMPERATURE NINO34 (K)             (${days} ${year_rtdiag_start}-${year})
NEWNAM     TEMP
C*XSAVE       OCEAN SFC TEMPERATURE NINO12 (K)             (${days} ${year_rtdiag_start}-${year})
NEWNAM     TEMP" >> ic.xsave

# ------------------------------------ monthly global upper layer SALT

        x="SALT"
        echo "  RCOPY            1         1" | ccc rcopy gz${x}${m} gz${x}1${m}
        globavw gz${x}1${m} bda1 _ _ ggz${x}1${m}
#                                      convert to psu
        echo "C*XLIN         1000." | ccc xlin ggz${x}1${m} ggz${x}1${m}.1 ; mv ggz${x}1${m}.1 ggz${x}1${m}

        rtdlist="$rtdlist ggz${x}1${m}"
        echo "C*XFIND       OCEAN SFC SALINITY (PSU)                     (${days} ${year_rtdiag_start}-${yearm1})" >> ic.xfind
        echo "C*XSAVE       OCEAN SFC SALINITY (PSU)                     (${days} ${year_rtdiag_start}-${year})
NEWNAM     SALT" >> ic.xsave

# ************************************ Physical Ocean annual section

        if [ ${m} -eq 12 ] ; then

# ------------------------------------ annual global top layer TEMP

        x="TEMP"
        avemongztoann gz${x}101 gz${x}102 gz${x}103 gz${x}104 gz${x}105 gz${x}106 gz${x}107 gz${x}108 gz${x}109 gz${x}110 gz${x}111 gz${x}112 gzann
        globavw gzann bda1    _ _ gtgz${x}1
        globavw gzann bdatro1 _ _ gtgz${x}tro1
        globavw gzann bdanhe1 _ _ gtgz${x}nhe1
        globavw gzann bdashe1 _ _ gtgz${x}she1

        rtdlist_ann="$rtdlist_ann gtgz${x}1 gtgz${x}tro1 gtgz${x}nhe1 gtgz${x}she1"
        echo "C*XFIND       OCEAN SFC TEMPERATURE (K)                    (ANN ${year_rtdiag_start}-${yearm1})
C*XFIND       OCEAN SFC TEMPERATURE 30S-30N (K)            (ANN ${year_rtdiag_start}-${yearm1})
C*XFIND       OCEAN SFC TEMPERATURE 30N-90N (K)            (ANN ${year_rtdiag_start}-${yearm1})
C*XFIND       OCEAN SFC TEMPERATURE 30S-90S (K)            (ANN ${year_rtdiag_start}-${yearm1})" >> ic.xfind_ann
        echo "C*XSAVE       OCEAN SFC TEMPERATURE (K)                    (ANN ${year_rtdiag_start}-${year})
NEWNAM     TEMP
C*XSAVE       OCEAN SFC TEMPERATURE 30S-30N (K)            (ANN ${year_rtdiag_start}-${year})
NEWNAM     TEMP
C*XSAVE       OCEAN SFC TEMPERATURE 30N-90N (K)            (ANN ${year_rtdiag_start}-${year})
NEWNAM     TEMP
C*XSAVE       OCEAN SFC TEMPERATURE 30S-90S (K)            (ANN ${year_rtdiag_start}-${year})
NEWNAM     TEMP" >> ic.xsave_ann

# ------------------------------------ NINO indices

        globavw gzann bda_nino3  _ _ ggz${x}nino3
        globavw gzann bda_nino4  _ _ ggz${x}nino4
        globavw gzann bda_nino34 _ _ ggz${x}nino34
        globavw gzann bda_nino12 _ _ ggz${x}nino12

        rtdlist_ann="$rtdlist_ann ggz${x}nino3 ggz${x}nino4 ggz${x}nino34 ggz${x}nino12"
        echo "C*XFIND       OCEAN SFC TEMPERATURE NINO3 (K)              (ANN ${year_rtdiag_start}-${yearm1})
C*XFIND       OCEAN SFC TEMPERATURE NINO4 (K)              (ANN ${year_rtdiag_start}-${yearm1})
C*XFIND       OCEAN SFC TEMPERATURE NINO34 (K)             (ANN ${year_rtdiag_start}-${yearm1})
C*XFIND       OCEAN SFC TEMPERATURE NINO12 (K)             (ANN ${year_rtdiag_start}-${yearm1})" >> ic.xfind_ann
        echo "C*XSAVE       OCEAN SFC TEMPERATURE NINO3 (K)              (ANN ${year_rtdiag_start}-${year})
NEWNAM     TEMP
C*XSAVE       OCEAN SFC TEMPERATURE NINO4 (K)              (ANN ${year_rtdiag_start}-${year})
NEWNAM     TEMP
C*XSAVE       OCEAN SFC TEMPERATURE NINO34 (K)             (ANN ${year_rtdiag_start}-${year})
NEWNAM     TEMP
C*XSAVE       OCEAN SFC TEMPERATURE NINO12 (K)             (ANN ${year_rtdiag_start}-${year})
NEWNAM     TEMP" >> ic.xsave_ann

# ------------------------------------ annual vertically integrated mean temperature

        x="TEMP"
        avemongztoann gz${x}01 gz${x}02 gz${x}03 gz${x}04 gz${x}05 gz${x}06 gz${x}07 gz${x}08 gz${x}09 gz${x}10 gz${x}11 gz${x}12 gzann
#                                      vertical integral of beta*T
        mlt gzann betao gzb
        vzint gzb dz gzvi
#                                      vertically integrated global mean temperature
        globavw gzvi da1 _ _ ggzvi
        div ggzvi gbetaovi gtgz${x}vi

        rtdlist_ann="$rtdlist_ann gtgz${x}vi"
        echo "C*XFIND       OCEAN TEMPERATURE GLOBAL (K)                 (ANN ${year_rtdiag_start}-${yearm1})" >> ic.xfind_ann
        echo "C*XSAVE       OCEAN TEMPERATURE GLOBAL (K)                 (ANN ${year_rtdiag_start}-${year})
NEWNAM     TEMP" >> ic.xsave_ann

# ------------------------------------ annual upper ocean vertically integrated mean temperature

        x="TEMP"
        for depth in $upper_ocean_depths ; do
        depth10=${depth}0
        depth4=`echo $depth | awk '{printf "%4i", $1}'`
        echo "C*  VZINTV         0${depth10}" | ccc vzintv gzb dz gzvi${depth}
#                                      vertically integrated global mean ocean upper layers temperature
        globavw gzvi${depth} da1 _ _ ggzvi${depth}
        div ggzvi${depth} gbetaovi${depth} gtgz${x}vi${depth}

        rtdlist_ann="$rtdlist_ann gtgz${x}vi${depth}"
        echo "C*XFIND       UPPER ${depth4}M OCEAN TEMPERATURE (K)            (ANN ${year_rtdiag_start}-${yearm1})" >> ic.xfind_ann
        echo "C*XSAVE       UPPER ${depth4}M OCEAN TEMPERATURE (K)            (ANN ${year_rtdiag_start}-${year})
NEWNAM     TEMP" >> ic.xsave_ann
        done

# ------------------------------------ annual global mean ocean temperature at various depths (every 5th level)

        x="TEMP"
        lev=1
        while [ $lev -le $gznlev ] ; do
        lev5=`echo $lev | awk '{printf "%05i", $1}'`
        echo "C*RCOPY        $lev5     $lev5" | ccc rcopy gzann gzann$lev
        echo "C*RCOPY        $lev5     $lev5" | ccc rcopy betao betao$lev
        mlt betao$lev da bda$lev
        rm betao$lev
#                                      global mean ocean temperature
        globavw gzann$lev bda$lev _ _ gtgz${x}l${lev}
        rm gzann$lev

        LEV5=`ggstat bda$lev | grep GRID | cut -c34-38 | awk '{printf "%5i", $0/10}'`
        rtdlist_ann="$rtdlist_ann gtgz${x}l${lev}"
        echo "C*XFIND       OCEAN TEMPERATURE${LEV5}M (K)                  (ANN ${year_rtdiag_start}-${yearm1})" >> ic.xfind_ann
        echo "C*XSAVE       OCEAN TEMPERATURE${LEV5}M (K)                  (ANN ${year_rtdiag_start}-${year})
NEWNAM     TEMP" >> ic.xsave_ann
        if [ $lev -eq 1 ] ; then
          lev=`expr $lev + $gzlevd - 1`
        else
          lev=`expr $lev + $gzlevd`
        fi
        done

# ------------------------------------ annual global top layer SALT

        x="SALT"
        avemongztoann gz${x}101 gz${x}102 gz${x}103 gz${x}104 gz${x}105 gz${x}106 gz${x}107 gz${x}108 gz${x}109 gz${x}110 gz${x}111 gz${x}112 gzann
        globavw gzann bda1 _ _ gtgz${x}1
#                                      convert to psu
        echo "C*XLIN         1000." | ccc xlin gtgz${x}1 gtgz${x}1.1 ; mv gtgz${x}1.1 gtgz${x}1

        rtdlist_ann="$rtdlist_ann gtgz${x}1"
        echo "C*XFIND       OCEAN SFC SALINITY (PSU)                     (ANN ${year_rtdiag_start}-${yearm1})" >> ic.xfind_ann
        echo "C*XSAVE       OCEAN SFC SALINITY (PSU)                     (ANN ${year_rtdiag_start}-${year})
NEWNAM     SALT" >> ic.xsave_ann

# ------------------------------------ annual vertically integrated mean salinity

        x="SALT"
        avemongztoann gz${x}01 gz${x}02 gz${x}03 gz${x}04 gz${x}05 gz${x}06 gz${x}07 gz${x}08 gz${x}09 gz${x}10 gz${x}11 gz${x}12 gzann
#                                      vertical integral of beta*S
        mlt gzann betao gzb
        vzint gzb dz gzvi
#                                      vertically integrated global mean salinity
        globavw gzvi da1 _ _ ggzvi
        div ggzvi gbetaovi gtgz${x}vi
#                                      convert to psu
        echo "C*XLIN         1000.      0.E0 SALT" | ccc xlin gtgz${x}vi gtgz${x}vi.1 ; mv gtgz${x}vi.1 gtgz${x}vi

        rtdlist_ann="$rtdlist_ann gtgz${x}vi"
        echo "C*XFIND       OCEAN SALINITY GLOBAL (PSU)                  (ANN ${year_rtdiag_start}-${yearm1})" >> ic.xfind_ann
        echo "C*XSAVE       OCEAN SALINITY GLOBAL (PSU)                  (ANN ${year_rtdiag_start}-${year})
NEWNAM     SALT" >> ic.xsave_ann

# ------------------------------------ annual upper ocean vertically integrated mean salinity

        x="SALT"
        for depth in $upper_ocean_depths ; do
        depth10=${depth}0
        depth4=`echo $depth | awk '{printf "%4i", $1}'`
        echo "C*  VZINTV         0${depth10}" | ccc vzintv gzb dz gzvi${depth}
#                                      vertically integrated global mean salinity upper layers
        globavw gzvi${depth} da1 _ _ ggzvi${depth}
        div ggzvi${depth} gbetaovi${depth} gtgz${x}vi${depth}
#                                      convert to psu
        echo "C*XLIN         1000.      0.E0 SALT" | ccc xlin gtgz${x}vi${depth} gtgz${x}vi${depth}.1 ; mv gtgz${x}vi${depth}.1 gtgz${x}vi${depth}

        rtdlist_ann="$rtdlist_ann gtgz${x}vi${depth}"
        echo "C*XFIND       UPPER ${depth4}M OCEAN SALINITY (PSU)             (ANN ${year_rtdiag_start}-${yearm1})" >> ic.xfind_ann
        echo "C*XSAVE       UPPER ${depth4}M OCEAN SALINITY (PSU)             (ANN ${year_rtdiag_start}-${year})
NEWNAM     SALT" >> ic.xsave_ann
        done

# ------------------------------------ annual global mean salinity at various depths

        x="SALT"
        lev=1
        while [ $lev -le $gznlev ] ; do
        lev5=`echo $lev | awk '{printf "%05i", $1}'`
        echo "C*RCOPY        $lev5     $lev5" | ccc rcopy gzann gzann$lev
#                                      global mean salinity
        globavw gzann$lev bda$lev _ _ gtgz${x}l${lev}
        rm gzann$lev
#                                      convert to psu
        echo "C*XLIN         1000." | ccc xlin gtgz${x}l${lev} gtgz${x}l${lev}.1 ; mv gtgz${x}l${lev}.1 gtgz${x}l${lev}

        LEV5=`ggstat bda$lev | grep GRID | cut -c34-38 | awk '{printf "%5i", $0/10}'`
        rtdlist_ann="$rtdlist_ann gtgz${x}l${lev}"
        echo "C*XFIND       OCEAN SALINITY${LEV5}M (PSU)                   (ANN ${year_rtdiag_start}-${yearm1})" >> ic.xfind_ann
        echo "C*XSAVE       OCEAN SALINITY${LEV5}M (PSU)                   (ANN ${year_rtdiag_start}-${year})
NEWNAM     SALT" >> ic.xsave_ann
        if [ $lev -eq 1 ] ; then
          lev=`expr $lev + $gzlevd - 1`
        else
          lev=`expr $lev + $gzlevd`
        fi
        done

# ------------------------------------ annual global ocean OBWA (unit: kg/m2/day or mm/day)
#                                      averaged over ocean grid points

        x="OBWA"
        timavg cm${x}01 tavg${x} _ _ cm${x}02 cm${x}03 cm${x}04 cm${x}05 cm${x}06 cm${x}07 cm${x}08 cm${x}09 cm${x}10 cm${x}11 cm${x}12
        globavw tavg${x} bda0 _ _ gcm${x}
#                                      convert kg/m2/s or mm/s to mm/day by multiplying with 60x60x24=86400
        echo "C*XLIN        86400.       0.0" | ccc xlin gcm${x} gcm${x}.1 ; mv gcm${x}.1 gcm${x}
#
        rtdlist_ann="$rtdlist_ann gcm${x}"
        echo "C*XFIND       FRESHWATER INTO OCEAN (MM/DAY)               (ANN ${year_rtdiag_start}-${yearm1})" >> ic.xfind_ann
        echo "C*XSAVE       FRESHWATER INTO OCEAN (MM/DAY)               (ANN ${year_rtdiag_start}-${year})
NEWNAM     OBWA" >> ic.xsave_ann

# ------------------------------------ annual global ocean OBET

        x="OBET"
        if [ -s cm${x}01 ] ; then
        timavg cm${x}01 tavg${x} _ _ cm${x}02 cm${x}03 cm${x}04 cm${x}05 cm${x}06 cm${x}07 cm${x}08 cm${x}09 cm${x}10 cm${x}11 cm${x}12
        globavw tavg${x} bda0 _ _ gcm${x}

        rtdlist_ann="$rtdlist_ann gcm${x}"
        echo "C*XFIND       NET SFC ENERGY OCEAN OBET (W/M2)             (ANN ${year_rtdiag_start}-${yearm1})" >> ic.xfind_ann
        echo "C*XSAVE       NET SFC ENERGY OCEAN OBET (W/M2)             (ANN ${year_rtdiag_start}-${year})
NEWNAM     OBET" >> ic.xsave_ann
        fi

# ------------------------------------ annual global ocean OBEI

        x="OBEI"
        if [ -s cm${x}01 ] ; then
        timavg cm${x}01 tavg${x} _ _ cm${x}02 cm${x}03 cm${x}04 cm${x}05 cm${x}06 cm${x}07 cm${x}08 cm${x}09 cm${x}10 cm${x}11 cm${x}12
        globavw tavg${x} bda0 _ _ gcm${x}

        rtdlist_ann="$rtdlist_ann gcm${x}"
        echo "C*XFIND       NET SFC ENERGY OCEAN OBEI (W/M2)             (ANN ${year_rtdiag_start}-${yearm1})" >> ic.xfind_ann
        echo "C*XSAVE       NET SFC ENERGY OCEAN OBEI (W/M2)             (ANN ${year_rtdiag_start}-${year})
NEWNAM     OBEI" >> ic.xsave_ann
        fi

# ------------------------------------ annual global ocean OBEX=OBET+OBEI

        x="OBEX"
        if [ -s cm${x}01 ] ; then
        timavg cm${x}01 tavg${x} _ _ cm${x}02 cm${x}03 cm${x}04 cm${x}05 cm${x}06 cm${x}07 cm${x}08 cm${x}09 cm${x}10 cm${x}11 cm${x}12
        globavw tavg${x} bda0 _ _ gcm${x}

        rtdlist_ann="$rtdlist_ann gcm${x}"
        echo "C*XFIND       NET SFC ENERGY OCEAN OBEX CM (W/M2)          (ANN ${year_rtdiag_start}-${yearm1})" >> ic.xfind_ann
        echo "C*XSAVE       NET SFC ENERGY OCEAN OBEX CM (W/M2)          (ANN ${year_rtdiag_start}-${year})
NEWNAM     OBEX" >> ic.xsave_ann
        fi

# ------------------------------------ Streamfunction calculation

        for x in "PSIB" "V" "VISO" ; do
          avemongztoann gz${x}01 gz${x}02 gz${x}03 gz${x}04 gz${x}05 gz${x}06 gz${x}07 gz${x}08 gz${x}09 gz${x}10 gz${x}11 gz${x}12 gz${x}ann
        done
        joinup gzann gz_data_description gzPSIBann gzVann gzVISOann
        otransp gzann gzstat psiouta psioutg

# MXAO: north atlantic maximum streamfunction psi
# PSDP: Drake Passage transport (in Sverdrups)
# PSIN: Indonesian passage transport (in Sverdrups)
# PSBS: Bering Strait transport (in Sverdrups)
# PSGH: Cape of Good Hope transport (in Sverdrups)

        gzvars_sf="MXAO PSDP PSIN PSBS PSGH"
        GZVARS_SF=`fmtselname $gzvars_sf`
        echo "C*SELECT   STEP         0 999999999    1         099999 NAME$GZVARS_SF" | ccc select gzstat $gzvars_sf

        for x in $gzvars_sf ; do
          mv $x gz${x}
          rtdlist_ann="$rtdlist_ann gz$x"
        done
        echo "C*XFIND       MAX MERIDIONAL PSI NATL (SV)                 (ANN ${year_rtdiag_start}-${yearm1})
C*XFIND       DRAKE PASSAGE TRANSPORT (SV)                 (ANN ${year_rtdiag_start}-${yearm1})
C*XFIND       INDONESIAN PASSAGE TRANSPORT(SV)             (ANN ${year_rtdiag_start}-${yearm1})
C*XFIND       BERING STRAIT TRANSPORT (SV)                 (ANN ${year_rtdiag_start}-${yearm1})
C*XFIND       CAPE OF GOOD HOPE TRANSPORT (SV)             (ANN ${year_rtdiag_start}-${yearm1})" >> ic.xfind_ann
        echo "C*XSAVE       MAX MERIDIONAL PSI NATL (SV)                 (ANN ${year_rtdiag_start}-${year})
NEWNAM     MXAO
C*XSAVE       DRAKE PASSAGE TRANSPORT (SV)                 (ANN ${year_rtdiag_start}-${year})
NEWNAM     PSDP
C*XSAVE       INDONESIAN PASSAGE TRANSPORT(SV)             (ANN ${year_rtdiag_start}-${year})
NEWNAM     PSIN
C*XSAVE       BERING STRAIT TRANSPORT (SV)                 (ANN ${year_rtdiag_start}-${year})
NEWNAM     PSBS
C*XSAVE       CAPE OF GOOD HOPE TRANSPORT (SV)             (ANN ${year_rtdiag_start}-${year})
NEWNAM     PSGH" >> ic.xsave_ann

# ------------------------------------ North Atlantic overturning circulation indices

#                                      colocate meridional velocity
        colocat gzVann gz_data_description betao vvelcol

#                                      determine Atlantic overturning streamfunction
        gmlt betao atlreg abetao
        echo "  MERPSI      1   -65.0     -35.0" | ccc merpsi vvelcol abetao dx dz lat psio psiplt

        gmlt psio zlat25to26 psio25to26
        gmlt psio zlat47to48 psio47to48

        echo "C*RMAXN   $gznlev" | ccc rmaxn psio       maxpsio
        echo "C*RMAXN   $gznlev" | ccc rmaxn psio25to26 maxpsio25to26
        echo "C*RMAXN   $gznlev" | ccc rmaxn psio47to48 maxpsio47to48
        ggatim maxpsio       gzMAXV   input=ic.ggatim
        ggatim maxpsio25to26 gzMAXV26 input=ic.ggatim
        ggatim maxpsio47to48 gzMAXV48 input=ic.ggatim
        echo "               1.E-6" | ccc xlin gzMAXV   gzMAXVsv
        echo "               1.E-6" | ccc xlin gzMAXV26 gzMAXV26sv
        echo "               1.E-6" | ccc xlin gzMAXV48 gzMAXV48sv

        rtdlist_ann="$rtdlist_ann gzMAXVsv gzMAXV26sv gzMAXV48sv"
        echo "C*XFIND       MAX MERIDIONAL V PSI NATL (SV)               (ANN ${year_rtdiag_start}-${yearm1})
C*XFIND       MAX MERIDIONAL V PSI NATL 26N(SV)            (ANN ${year_rtdiag_start}-${yearm1})
C*XFIND       MAX MERIDIONAL V PSI NATL 48N(SV)            (ANN ${year_rtdiag_start}-${yearm1})" >> ic.xfind_ann
        echo "C*XSAVE       MAX MERIDIONAL V PSI NATL (SV)               (ANN ${year_rtdiag_start}-${year})
NEWNAM     MXAV
C*XSAVE       MAX MERIDIONAL V PSI NATL 26N(SV)            (ANN ${year_rtdiag_start}-${year})
NEWNAM     AV26
C*XSAVE       MAX MERIDIONAL V PSI NATL 48N(SV)            (ANN ${year_rtdiag_start}-${year})
NEWNAM     AV48" >> ic.xsave_ann
        fi # $m=12
      fi # PhysO=on

# #################################### Ocean Carbon section

      if [ "$CarbO" = "on" ] ; then

# ------------------------------------ monthly SWMX

        x="SWMX"
        if [ -s cm${x}${m} ] ; then
        timavg cm${x}${m} tavg${x}
#                                      global
        globavg tavg${x} _ gtcm${x}${m}g
#                                      ocean
        globavg tavg${x} _ gtcm${x}${m}o ocean_frac

        rtdlist="$rtdlist gtcm${x}${m}g gtcm${x}${m}o"
        echo "C*XFIND       SURFACE WIND SPEED GLOBAL (M/S)              (${days} ${year_rtdiag_start}-${yearm1})
C*XFIND       SURFACE WIND SPEED OCEAN (M/S)               (${days} ${year_rtdiag_start}-${yearm1})" >> ic.xfind
        echo "C*XSAVE       SURFACE WIND SPEED GLOBAL (M/S)              (${days} ${year_rtdiag_start}-${year})
NEWNAM     SWMX
C*XSAVE       SURFACE WIND SPEED OCEAN (M/S)               (${days} ${year_rtdiag_start}-${year})
NEWNAM     SWMX" >> ic.xsave
        fi

# ------------------------------------ monthly OFSG

        x="OFSG"
        if [ -s cm${x}${m} ] ; then
        timavg cm${x}${m} tavg${x}
#                                      ocean
        globavg tavg${x} _ gtcm${x}${m}o ocean_frac

        rtdlist="$rtdlist gtcm${x}${m}o"
        echo "C*XFIND       INSOLATION OCEAN (W/M2)                      (${days} ${year_rtdiag_start}-${yearm1})" >> ic.xfind
        echo "C*XSAVE       INSOLATION OCEAN (W/M2)                      (${days} ${year_rtdiag_start}-${year})
NEWNAM     OFSG" >> ic.xsave
        fi

# ------------------------------------ monthly global PMSL

        x="PMSL"
        if [ -s cm${x}${m} ] ; then
        timavg cm${x}${m} tavg${x}
#                                      global
        globavg tavg${x} _ gtcm${x}${m}g
#                                      ocean
        globavg tavg${x} _ gtcm${x}${m}o ocean_frac

        rtdlist="$rtdlist gtcm${x}${m}g gtcm${x}${m}o"
        echo "C*XFIND       SEA-LEVEL PRESSURE GLOBAL (MB)               (${days} ${year_rtdiag_start}-${yearm1})
C*XFIND       SEA-LEVEL PRESSURE OCEAN (MB)                (${days} ${year_rtdiag_start}-${yearm1})" >> ic.xfind
        echo "C*XSAVE       SEA-LEVEL PRESSURE GLOBAL (MB)               (${days} ${year_rtdiag_start}-${year})
NEWNAM     PMSL
C*XSAVE       SEA-LEVEL PRESSURE OCEAN (MB)                (${days} ${year_rtdiag_start}-${year})
NEWNAM     PMSL" >> ic.xsave
        fi

# ************************************ Ocean Carbon annual section

        if [ ${m} -eq 12 ] ; then

# ------------------------------------ annual SWMX

        x="SWMX"
        if [ -s cm${x}01 ] ; then
        timavg cm${x}01 tavg${x} _ _ cm${x}02 cm${x}03 cm${x}04 cm${x}05 cm${x}06 cm${x}07 cm${x}08 cm${x}09 cm${x}10 cm${x}11 cm${x}12
#                                      global
        globavg tavg${x} _ gtcm${x}g
#                                      ocean
        globavg tavg${x} _ gtcm${x}o ocean_frac

        rtdlist_ann="$rtdlist_ann gtcm${x}g gtcm${x}o"
        echo "C*XFIND       SURFACE WIND SPEED GLOBAL (M/S)              (ANN ${year_rtdiag_start}-${yearm1})
C*XFIND       SURFACE WIND SPEED OCEAN (M/S)               (ANN ${year_rtdiag_start}-${yearm1})" >> ic.xfind_ann
        echo "C*XSAVE       SURFACE WIND SPEED GLOBAL (M/S)              (ANN ${year_rtdiag_start}-${year})
NEWNAM     SWMX
C*XSAVE       SURFACE WIND SPEED OCEAN (M/S)               (ANN ${year_rtdiag_start}-${year})
NEWNAM     SWMX" >> ic.xsave_ann
        fi

# ------------------------------------ annual OFSG

        x="OFSG"
        if [ -s cm${x}01 ] ; then
        timavg cm${x}01 tavg${x} _ _ cm${x}02 cm${x}03 cm${x}04 cm${x}05 cm${x}06 cm${x}07 cm${x}08 cm${x}09 cm${x}10 cm${x}11 cm${x}12
#                                      ocean
        globavg tavg${x} _ gtcm${x}o ocean_frac

        rtdlist_ann="$rtdlist_ann gtcm${x}o"
        echo "C*XFIND       INSOLATION OCEAN (W/M2)                      (ANN ${year_rtdiag_start}-${yearm1})" >> ic.xfind_ann
        echo "C*XSAVE       INSOLATION OCEAN (W/M2)                      (ANN ${year_rtdiag_start}-${year})
NEWNAM     OFSG" >> ic.xsave_ann
        fi

# ------------------------------------ annual PMSL

        x="PMSL"
        if [ -s cm${x}01 ] ; then
        timavg cm${x}01 tavg${x} _ _ cm${x}02 cm${x}03 cm${x}04 cm${x}05 cm${x}06 cm${x}07 cm${x}08 cm${x}09 cm${x}10 cm${x}11 cm${x}12
#                                      global
        globavg tavg${x} _ gtcm${x}g
#                                      ocean
        globavg tavg${x} _ gtcm${x}o ocean_frac

        rtdlist_ann="$rtdlist_ann gtcm${x}g gtcm${x}o"
        echo "C*XFIND       SEA-LEVEL PRESSURE GLOBAL (MB)               (ANN ${year_rtdiag_start}-${yearm1})
C*XFIND       SEA-LEVEL PRESSURE OCEAN (MB)                (ANN ${year_rtdiag_start}-${yearm1})" >> ic.xfind_ann
        echo "C*XSAVE       SEA-LEVEL PRESSURE GLOBAL (MB)               (ANN ${year_rtdiag_start}-${year})
NEWNAM     PMSL
C*XSAVE       SEA-LEVEL PRESSURE OCEAN (MB)                (ANN ${year_rtdiag_start}-${year})
NEWNAM     PMSL" >> ic.xsave_ann
        fi

# ------------------------------------ annual global FCOO

        x="FCOO"
        if [ -s cm${x}01 ] ; then
#                                      find annual mean and global integral
        timavg cm${x}01 tavg${x} _ _ cm${x}02 cm${x}03 cm${x}04 cm${x}05 cm${x}06 cm${x}07 cm${x}08 cm${x}09 cm${x}10 cm${x}11 cm${x}12
        globavw tavg${x} bda0 _ _ _ gtint
#                                      convert mol C sec-1 to Pg C/year
#                                      x 365 day * 86400 sec x 12.011 / 10^15 = 3.7878E-7
#                                      x -1 to make it +ve downward
        echo "C*XLIN    -3.7878E-7" | ccc xlin gtint gtcm$x

        rtdlist_ann="$rtdlist_ann gtcm$x"
        echo "C*XFIND       OCEAN-ATMOS CO2 FLUX (PG C/YR)               (ANN ${year_rtdiag_start}-${yearm1})" >> ic.xfind_ann
        echo "C*XSAVE       OCEAN-ATMOS CO2 FLUX (PG C/YR)               (ANN ${year_rtdiag_start}-${year})
NEWNAM     FCOO" >> ic.xsave_ann
        fi

# ------------------------------------ annual global OPCO

        x="OPCO"
        if [ -s gz${x}01 ] ; then
#                                      find annual mean and global average
        avemongztoann gz${x}01 gz${x}02 gz${x}03 gz${x}04 gz${x}05 gz${x}06 gz${x}07 gz${x}08 gz${x}09 gz${x}10 gz${x}11 gz${x}12 gzann
        globavw gzann bda0 _ _ gtgz$x

        rtdlist_ann="$rtdlist_ann gtgz$x"
        echo "C*XFIND       SURFACE OCEAN PCO2 (PPMV)                    (ANN ${year_rtdiag_start}-${yearm1})" >> ic.xfind_ann
        echo "C*XSAVE       SURFACE OCEAN PCO2 (PPMV)                    (ANN ${year_rtdiag_start}-${year})
NEWNAM     OPCO" >> ic.xsave_ann
        fi

# ------------------------------------ annual global ODPC

        x="ODPC"
        if [ -s gz${x}01 ] ; then
#                                      find annual mean and global average
        avemongztoann gz${x}01 gz${x}02 gz${x}03 gz${x}04 gz${x}05 gz${x}06 gz${x}07 gz${x}08 gz${x}09 gz${x}10 gz${x}11 gz${x}12 gzann
        globavw gzann bda0 _ _ gtgz$x

        rtdlist_ann="$rtdlist_ann gtgz$x"
        echo "C*XFIND       OCEAN-ATMOS DELTA-PCO2 (PPMV)                (ANN ${year_rtdiag_start}-${yearm1})" >> ic.xfind_ann
        echo "C*XSAVE       OCEAN-ATMOS DELTA-PCO2 (PPMV)                (ANN ${year_rtdiag_start}-${year})
NEWNAM     ODPC" >> ic.xsave_ann
        fi

# ------------------------------------ annual global OVIC

        x="OVIC"
        if [ -s gz${x}01 ] ; then
#                                      find annual mean and global average
        avemongztoann gz${x}01 gz${x}02 gz${x}03 gz${x}04 gz${x}05 gz${x}06 gz${x}07 gz${x}08 gz${x}09 gz${x}10 gz${x}11 gz${x}12 gzann
        globavw gzann bda0 _ _ _ gtint
#                                      convert mol C sec-1 to Pg C/year
#                                      x 365 day * 86400 sec x 12.011 / 10^15 = 3.7878E-07
#                                      x -1 to make it +ve downward
        echo "C*XLIN    -3.7878E-7" | ccc xlin gtint gtgz$x

        rtdlist_ann="$rtdlist_ann gtgz$x"
        echo "C*XFIND       DIC VIRTUAL FLUX (PG C/YR)                   (ANN ${year_rtdiag_start}-${yearm1})" >> ic.xfind_ann
        echo "C*XSAVE       DIC VIRTUAL FLUX (PG C/YR)                   (ANN ${year_rtdiag_start}-${year})
NEWNAM     OVIC" >> ic.xsave_ann
        fi

# ------------------------------------ annual global OFNP

        x="OFNP"
        if [ -s gz${x}01 ] ; then
#                                      find annual mean and global average
        avemongztoann gz${x}01 gz${x}02 gz${x}03 gz${x}04 gz${x}05 gz${x}06 gz${x}07 gz${x}08 gz${x}09 gz${x}10 gz${x}11 gz${x}12 gzann
        globavw gzann bda0 _ _ _ gtint
#                                      convert mol N yr-1 to Pg C/year
#                                      6.625 (C/N ratio in CMOC) x 12.011 / 10^15 = 7.9573E-14
        echo "C*XLIN    7.9573E-14" | ccc xlin gtint gtgz$x

        rtdlist_ann="$rtdlist_ann gtgz$x"
        echo "C*XFIND       PRIMARY PRODUCTION (PG C/YR)                 (ANN ${year_rtdiag_start}-${yearm1})" >> ic.xfind_ann
        echo "C*XSAVE       PRIMARY PRODUCTION (PG C/YR)                 (ANN ${year_rtdiag_start}-${year})
NEWNAM     OFNP" >> ic.xsave_ann
        fi

# ------------------------------------ annual global OFXO

        x="OFXO"
        if [ -s gz${x}01 ] ; then
#                                      find annual mean and global average
        avemongztoann gz${x}01 gz${x}02 gz${x}03 gz${x}04 gz${x}05 gz${x}06 gz${x}07 gz${x}08 gz${x}09 gz${x}10 gz${x}11 gz${x}12 gzann
        globavw gzann bda0 _ _ _ gtint
#                                      convert mol N yr-1 to Pg C/year
#                                      6.625 (C/N ratio in CMOC) x 12.011 / 10^15 = 7.9573E-14
        echo "C*XLIN    7.9573E-14" | ccc xlin gtint gtgz$x

        rtdlist_ann="$rtdlist_ann gtgz$x"
        echo "C*XFIND       EXPORT FLUX ORGANIC (PG C/YR)                (ANN ${year_rtdiag_start}-${yearm1})" >> ic.xfind_ann
        echo "C*XSAVE       EXPORT FLUX ORGANIC (PG C/YR)                (ANN ${year_rtdiag_start}-${year})
NEWNAM     OFXO" >> ic.xsave_ann
        fi

# ------------------------------------ annual global OFXI

        x="OFXI"
        if [ -s gz${x}01 ] ; then
#                                      find annual mean and global average
        avemongztoann gz${x}01 gz${x}02 gz${x}03 gz${x}04 gz${x}05 gz${x}06 gz${x}07 gz${x}08 gz${x}09 gz${x}10 gz${x}11 gz${x}12 gzann
        globavw gzann bda0 _ _ _ gtint
#                                      convert mol C yr-1 to Pg C/year
#                                      12.011 / 10^15 = 12.E-15
        echo "C*XLIN    12.011E-15" | ccc xlin gtint gtgz$x

        rtdlist_ann="$rtdlist_ann gtgz$x"
        echo "C*XFIND       EXPORT FLUX INORGANIC (PG C/YR)              (ANN ${year_rtdiag_start}-${yearm1})" >> ic.xfind_ann
        echo "C*XSAVE       EXPORT FLUX INORGANIC (PG C/YR)              (ANN ${year_rtdiag_start}-${year})
NEWNAM     OFXI" >> ic.xsave_ann
        fi

# ------------------------------------ annual global OFNF

        x="OFNF"
        if [ -s gz${x}01 ] ; then
#                                      find annual mean and global average
        avemongztoann gz${x}01 gz${x}02 gz${x}03 gz${x}04 gz${x}05 gz${x}06 gz${x}07 gz${x}08 gz${x}09 gz${x}10 gz${x}11 gz${x}12 gzann
        globavw gzann bda0 _ _ _ gtint
#                                      convert mol N yr-1 to Tg N/year
#                                      14.0065 / 10^12 = 14.0065E-12
        echo "C*XLIN    14.007E-12" | ccc xlin gtint gtgz$x

        rtdlist_ann="$rtdlist_ann gtgz$x"
        echo "C*XFIND       N2 FIXATION (TG N/YR)                        (ANN ${year_rtdiag_start}-${yearm1})" >> ic.xfind_ann
        echo "C*XSAVE       N2 FIXATION (TG N/YR)                        (ANN ${year_rtdiag_start}-${year})
NEWNAM     OFNF" >> ic.xsave_ann
        fi

# ------------------------------------ annual global OFSO

        x="OFSO"
        if [ -s gz${x}01 ] ; then
#                                      find annual mean and global average
        avemongztoann gz${x}01 gz${x}02 gz${x}03 gz${x}04 gz${x}05 gz${x}06 gz${x}07 gz${x}08 gz${x}09 gz${x}10 gz${x}11 gz${x}12 gzann
        globavw gzann bda0 _ _ _ gtint
#                                      convert mol N yr-1 to Pg C/year
#                                      6.625 (C/N ratio in CMOC) x 12.011 / 10^15 = 7.9573E-14
        echo "C*XLIN    7.9573E-14" | ccc xlin gtint gtgz$x

        rtdlist_ann="$rtdlist_ann gtgz$x"
        echo "C*XFIND       ORGANIC FLUX TO SEDS (PG C/YR)               (ANN ${year_rtdiag_start}-${yearm1})" >> ic.xfind_ann
        echo "C*XSAVE       ORGANIC FLUX TO SEDS (PG C/YR)               (ANN ${year_rtdiag_start}-${year})
NEWNAM     OFSO" >> ic.xsave_ann
        fi

# ------------------------------------ annual global OFSI

        x="OFSI"
        if [ -s gz${x}01 ] ; then
#                                      find annual mean and global average
        avemongztoann gz${x}01 gz${x}02 gz${x}03 gz${x}04 gz${x}05 gz${x}06 gz${x}07 gz${x}08 gz${x}09 gz${x}10 gz${x}11 gz${x}12 gzann
        globavw gzann bda0 _ _ _ gtint
#                                      convert mol C yr-1 to Pg C/year
#                                      12.011 / 10^15 = 12.011E-15
        echo "C*XLIN    12.011E-15" | ccc xlin gtint gtgz$x

        rtdlist_ann="$rtdlist_ann gtgz$x"
        echo "C*XFIND       INORGANIC FLUX TO SEDS (PG C/YR)             (ANN ${year_rtdiag_start}-${yearm1})" >> ic.xfind_ann
        echo "C*XSAVE       INORGANIC FLUX TO SEDS (PG C/YR)             (ANN ${year_rtdiag_start}-${year})
NEWNAM     OFSI" >> ic.xsave_ann
        fi

# ------------------------------------ annual global average OPH

        x="OPH"
        if [ -s gz${x}01 ] ; then
#                                      find annual mean and global average
        avemongztoann gz${x}01 gz${x}02 gz${x}03 gz${x}04 gz${x}05 gz${x}06 gz${x}07 gz${x}08 gz${x}09 gz${x}10 gz${x}11 gz${x}12 gzann
        globavw gzann bda0 _ _ gtgz$x

        rtdlist_ann="$rtdlist_ann gtgz$x"
        echo "C*XFIND       SURFACE OCEAN PH                             (ANN ${year_rtdiag_start}-${yearm1})" >> ic.xfind_ann
        echo "C*XSAVE       SURFACE OCEAN PH                             (ANN ${year_rtdiag_start}-${year})
NEWNAM      OPH" >> ic.xsave_ann
        fi

# ------------------------------------ annual global ONIT at various levels

        x="ONIT"
        if [ ! -s gz${x}01 ] ; then
#                                      try an older name
          x="BNI"
        fi
        if [ -s gz${x}01 ] ; then
#                                      find annual mean and global average
        avemongztoann gz${x}01 gz${x}02 gz${x}03 gz${x}04 gz${x}05 gz${x}06 gz${x}07 gz${x}08 gz${x}09 gz${x}10 gz${x}11 gz${x}12 gzann
#                                      vertical integral of beta*X
        mlt gzann betao gzb
        vzint gzb dz gzvi
#                                      global vertical integral
        globavw gzvi da1 _ _ _ gtgz${x}vi

        rtdlist_ann="$rtdlist_ann gtgz${x}vi"
        echo "C*XFIND       NITRATE GLOBAL (MOL)                         (ANN ${year_rtdiag_start}-${yearm1})" >> ic.xfind_ann
        echo "C*XSAVE       NITRATE GLOBAL (MOL)                         (ANN ${year_rtdiag_start}-${year})
NEWNAM     ONIT" >> ic.xsave_ann
#                                      various levels
        lev=1
        while [ $lev -le $gznlev ] ; do
        lev5=`echo $lev | awk '{printf "%05i", $1}'`
        echo "C*RCOPY        $lev5     $lev5" | ccc rcopy gzann gzann$lev
#                                      global mean
        globavw gzann$lev bda$lev _ _ gtgz${x}l${lev}
        rm gzann$lev
#                                      convert MOL/M3 to MMOL/M3
        echo "C*XLIN         1000." | ccc xlin gtgz${x}l${lev} gtgz${x}l${lev}.1 ; mv gtgz${x}l${lev}.1 gtgz${x}l${lev}

        LEV5=`ggstat bda$lev | grep GRID | cut -c34-38 | awk '{printf "%5i", $0/10}'`
        rtdlist_ann="$rtdlist_ann gtgz${x}l${lev}"
        echo "C*XFIND       NITRATE${LEV5}M (MMOL/M3)                      (ANN ${year_rtdiag_start}-${yearm1})" >> ic.xfind_ann
        echo "C*XSAVE       NITRATE${LEV5}M (MMOL/M3)                      (ANN ${year_rtdiag_start}-${year})
NEWNAM     ONIT" >> ic.xsave_ann
        if [ $lev -eq 1 ] ; then
          lev=`expr $lev + $gzlevd - 1`
        else
          lev=`expr $lev + $gzlevd`
        fi
        done
        fi

# ------------------------------------ annual global ODIC at various levels

        x="ODIC"
        if [ ! -s gz${x}01 ] ; then
#                                      try an older name
          x="DIC"
        fi
        if [ -s gz${x}01 ] ; then
#                                      find annual mean and global average
        avemongztoann gz${x}01 gz${x}02 gz${x}03 gz${x}04 gz${x}05 gz${x}06 gz${x}07 gz${x}08 gz${x}09 gz${x}10 gz${x}11 gz${x}12 gzann
#                                      vertical integral of beta*X
        mlt gzann betao gzb
        vzint gzb dz gzvi
#                                      global vertical integral
        globavw gzvi da1 _ _ _ gtgz${x}vi

        rtdlist_ann="$rtdlist_ann gtgz${x}vi"
        echo "C*XFIND       DISS.INORGANIC CARBON GLOBAL (MOL)           (ANN ${year_rtdiag_start}-${yearm1})" >> ic.xfind_ann
        echo "C*XSAVE       DISS.INORGANIC CARBON GLOBAL (MOL)           (ANN ${year_rtdiag_start}-${year})
NEWNAM     ODIC" >> ic.xsave_ann
#                                      various levels
        lev=1
        while [ $lev -le $gznlev ] ; do
        lev5=`echo $lev | awk '{printf "%05i", $1}'`
        echo "C*RCOPY        $lev5     $lev5" | ccc rcopy gzann gzann$lev
#                                      global mean
        globavw gzann$lev bda$lev _ _ gtgz${x}l${lev}
        rm gzann$lev
#                                      convert MOL/M3 to MMOL/M3
        echo "C*XLIN         1000." | ccc xlin gtgz${x}l${lev} gtgz${x}l${lev}.1 ; mv gtgz${x}l${lev}.1 gtgz${x}l${lev}

        LEV5=`ggstat bda$lev | grep GRID | cut -c34-38 | awk '{printf "%5i", $0/10}'`
        rtdlist_ann="$rtdlist_ann gtgz${x}l${lev}"
        echo "C*XFIND       DISS.INORGANIC CARBON${LEV5}M (MMOL/M3)        (ANN ${year_rtdiag_start}-${yearm1})" >> ic.xfind_ann
        echo "C*XSAVE       DISS.INORGANIC CARBON${LEV5}M (MMOL/M3)        (ANN ${year_rtdiag_start}-${year})
NEWNAM     ODIC" >> ic.xsave_ann
        if [ $lev -eq 1 ] ; then
          lev=`expr $lev + $gzlevd - 1`
        else
          lev=`expr $lev + $gzlevd`
        fi
        done
        fi

# ------------------------------------ annual global OALK at various levels

        x="OALK"
        if [ ! -s gz${x}01 ] ; then
#                                      try an older name
          x="ALK"
        fi
        if [ -s gz${x}01 ] ; then
#                                      find annual mean and global average
        avemongztoann gz${x}01 gz${x}02 gz${x}03 gz${x}04 gz${x}05 gz${x}06 gz${x}07 gz${x}08 gz${x}09 gz${x}10 gz${x}11 gz${x}12 gzann
#                                      vertical integral of beta*X
        mlt gzann betao gzb
        vzint gzb dz gzvi
#                                      global vertical integral
        globavw gzvi da1 _ _ _ gtgz${x}vi

        rtdlist_ann="$rtdlist_ann gtgz${x}vi"
        echo "C*XFIND       ALKALINITY GLOBAL (MOL)                      (ANN ${year_rtdiag_start}-${yearm1})" >> ic.xfind_ann
        echo "C*XSAVE       ALKALINITY GLOBAL (MOL)                      (ANN ${year_rtdiag_start}-${year})
NEWNAM     OALK" >> ic.xsave_ann
#                                      various levels
        lev=1
        while [ $lev -le $gznlev ] ; do
        lev5=`echo $lev | awk '{printf "%05i", $1}'`
        echo "C*RCOPY        $lev5     $lev5" | ccc rcopy gzann gzann$lev
#                                      global mean
        globavw gzann$lev bda$lev _ _ gtgz${x}l${lev}
        rm gzann$lev
#                                      convert MOL/M3 to MMOL/M3
        echo "C*XLIN         1000." | ccc xlin gtgz${x}l${lev} gtgz${x}l${lev}.1 ; mv gtgz${x}l${lev}.1 gtgz${x}l${lev}

        LEV5=`ggstat bda$lev | grep GRID | cut -c34-38 | awk '{printf "%5i", $0/10}'`
        rtdlist_ann="$rtdlist_ann gtgz${x}l${lev}"
        echo "C*XFIND       ALKALINITY${LEV5}M (MMOL/M3)                   (ANN ${year_rtdiag_start}-${yearm1})" >> ic.xfind_ann
        echo "C*XSAVE       ALKALINITY${LEV5}M (MMOL/M3)                   (ANN ${year_rtdiag_start}-${year})
NEWNAM     OALK" >> ic.xsave_ann
        if [ $lev -eq 1 ] ; then
          lev=`expr $lev + $gzlevd - 1`
        else
          lev=`expr $lev + $gzlevd`
        fi
        done
        fi

# ------------------------------------ annual global OPHY at various levels

        x="OPHY"
        if [ -s gz${x}01 ] ; then
#                                      find annual mean and global average
        avemongztoann gz${x}01 gz${x}02 gz${x}03 gz${x}04 gz${x}05 gz${x}06 gz${x}07 gz${x}08 gz${x}09 gz${x}10 gz${x}11 gz${x}12 gzann
#                                      vertical integral of beta*X
        mlt gzann betao gzb
        vzint gzb dz gzvi
#                                      global vertical integral
        globavw gzvi da1 _ _ _ gtgz${x}vi

        rtdlist_ann="$rtdlist_ann gtgz${x}vi"
        echo "C*XFIND       OPHY CARBON GLOBAL (MOL)                     (ANN ${year_rtdiag_start}-${yearm1})" >> ic.xfind_ann
        echo "C*XSAVE       OPHY CARBON GLOBAL (MOL)                     (ANN ${year_rtdiag_start}-${year})
NEWNAM     OPHY" >> ic.xsave_ann
#                                      various levels
        lev=1
        while [ $lev -le $gznlev ] ; do
        lev5=`echo $lev | awk '{printf "%05i", $1}'`
        echo "C*RCOPY        $lev5     $lev5" | ccc rcopy gzann gzann$lev || true
        if [ ! -s gzann$lev ] ; then
          break
        fi
#                                      global mean
        globavw gzann$lev bda$lev _ _ gtgz${x}l${lev}
        rm gzann$lev
#                                      convert MOL/M3 to MMOL/M3
        echo "C*XLIN         1000." | ccc xlin gtgz${x}l${lev} gtgz${x}l${lev}.1 ; mv gtgz${x}l${lev}.1 gtgz${x}l${lev}

        LEV5=`ggstat bda$lev | grep GRID | cut -c34-38 | awk '{printf "%5i", $0/10}'`
        rtdlist_ann="$rtdlist_ann gtgz${x}l${lev}"
        echo "C*XFIND       OPHY CARBON${LEV5}M (MMOL/M3)                  (ANN ${year_rtdiag_start}-${yearm1})" >> ic.xfind_ann
        echo "C*XSAVE       OPHY CARBON${LEV5}M (MMOL/M3)                  (ANN ${year_rtdiag_start}-${year})
NEWNAM     OPHY" >> ic.xsave_ann
        if [ $lev -eq 1 ] ; then
          lev=`expr $lev + $gzlevd - 1`
        else
          lev=`expr $lev + $gzlevd`
        fi
        done
        fi

# ------------------------------------ annual global OZOO at various levels

        x="OZOO"
        if [ -s gz${x}01 ] ; then
#                                      find annual mean and global average
        avemongztoann gz${x}01 gz${x}02 gz${x}03 gz${x}04 gz${x}05 gz${x}06 gz${x}07 gz${x}08 gz${x}09 gz${x}10 gz${x}11 gz${x}12 gzann
#                                      vertical integral of beta*X
        mlt gzann betao gzb
        vzint gzb dz gzvi
#                                      global vertical integral
        globavw gzvi da1 _ _ _ gtgz${x}vi

        rtdlist_ann="$rtdlist_ann gtgz${x}vi"
        echo "C*XFIND       OZOO CARBON GLOBAL (MOL)                     (ANN ${year_rtdiag_start}-${yearm1})" >> ic.xfind_ann
        echo "C*XSAVE       OZOO CARBON GLOBAL (MOL)                     (ANN ${year_rtdiag_start}-${year})
NEWNAM     OZOO" >> ic.xsave_ann
#                                      various levels
        lev=1
        while [ $lev -le $gznlev ] ; do
        lev5=`echo $lev | awk '{printf "%05i", $1}'`
        echo "C*RCOPY        $lev5     $lev5" | ccc rcopy gzann gzann$lev || true
        if [ ! -s gzann$lev ] ; then
          break
        fi
#                                      global mean
        globavw gzann$lev bda$lev _ _ gtgz${x}l${lev}
        rm gzann$lev
#                                      convert MOL/M3 to MMOL/M3
        echo "C*XLIN         1000." | ccc xlin gtgz${x}l${lev} gtgz${x}l${lev}.1 ; mv gtgz${x}l${lev}.1 gtgz${x}l${lev}

        LEV5=`ggstat bda$lev | grep GRID | cut -c34-38 | awk '{printf "%5i", $0/10}'`
        rtdlist_ann="$rtdlist_ann gtgz${x}l${lev}"
        echo "C*XFIND       OZOO CARBON${LEV5}M (MMOL/M3)                  (ANN ${year_rtdiag_start}-${yearm1})" >> ic.xfind_ann
        echo "C*XSAVE       OZOO CARBON${LEV5}M (MMOL/M3)                  (ANN ${year_rtdiag_start}-${year})
NEWNAM     OZOO" >> ic.xsave_ann
        if [ $lev -eq 1 ] ; then
          lev=`expr $lev + $gzlevd - 1`
        else
          lev=`expr $lev + $gzlevd`
        fi
        done
        fi

# ------------------------------------ annual global ODET at various levels

        x="ODET"
        if [ -s gz${x}01 ] ; then
#                                      find annual mean and global average
        avemongztoann gz${x}01 gz${x}02 gz${x}03 gz${x}04 gz${x}05 gz${x}06 gz${x}07 gz${x}08 gz${x}09 gz${x}10 gz${x}11 gz${x}12 gzann
#                                      vertical integral of beta*X
        mlt gzann betao gzb
        vzint gzb dz gzvi
#                                      global vertical integral
        globavw gzvi da1 _ _ _ gtgz${x}vi

        rtdlist_ann="$rtdlist_ann gtgz${x}vi"
        echo "C*XFIND       ODET CARBON GLOBAL (MOL)                     (ANN ${year_rtdiag_start}-${yearm1})" >> ic.xfind_ann
        echo "C*XSAVE       ODET CARBON GLOBAL (MOL)                     (ANN ${year_rtdiag_start}-${year})
NEWNAM     ODET" >> ic.xsave_ann
#                                      various levels
        lev=1
        while [ $lev -le $gznlev ] ; do
        lev5=`echo $lev | awk '{printf "%05i", $1}'`
        echo "C*RCOPY        $lev5     $lev5" | ccc rcopy gzann gzann$lev
#                                      global mean
        globavw gzann$lev bda$lev _ _ gtgz${x}l${lev}
        rm gzann$lev
#                                      convert MOL/M3 to MMOL/M3
        echo "C*XLIN         1000." | ccc xlin gtgz${x}l${lev} gtgz${x}l${lev}.1 ; mv gtgz${x}l${lev}.1 gtgz${x}l${lev}

        LEV5=`ggstat bda$lev | grep GRID | cut -c34-38 | awk '{printf "%5i", $0/10}'`
        rtdlist_ann="$rtdlist_ann gtgz${x}l${lev}"
        echo "C*XFIND       ODET CARBON${LEV5}M (MMOL/M3)                  (ANN ${year_rtdiag_start}-${yearm1})" >> ic.xfind_ann
        echo "C*XSAVE       ODET CARBON${LEV5}M (MMOL/M3)                  (ANN ${year_rtdiag_start}-${year})
NEWNAM     ODET" >> ic.xsave_ann
        if [ $lev -eq 1 ] ; then
          lev=`expr $lev + $gzlevd - 1`
        else
          lev=`expr $lev + $gzlevd`
        fi
        done
        fi
        fi # $m=12
      fi # CarbO=on

# #################################### Land Carbon section

      if [ "$CarbL" = "on" ] ; then

# ************************************ Land Carbon monthly section

# ------------------------------------ monthly averaged global wetland area calculated from wetland fraction

        x="WFRA"
        if [ -s cm${x}${m} ] ; then
        timavg cm${x}${m} tavg
        globavg tavg _ gtavg

#                                      convert to km2
        gmlt gtavg globe_area_km2 gtcm${x}${m}

        rtdlist="$rtdlist gtcm${x}${m}"
        echo "C*XFIND       DYNAMIC WETLAND AREA (KM2)                   (${days} ${year_rtdiag_start}-${yearm1})" >> ic.xfind
        echo "C*XSAVE       DYNAMIC WETLAND AREA (KM2)                   (${days} ${year_rtdiag_start}-${year})
NEWNAM     WFRA" >> ic.xsave
        fi

# ------------------------------------ monthly averaged global wetland emissions 1 - CH4H

        x="CH4H"
        if [ -s cm${x}${m} ] ; then
        timavg cm${x}${m} tavg
        globavg tavg _ gtcm${x}${m}

#                                      convert u-mol CO2-C m-2 sec-1 to Tg CH4/month
#                                      1.0377504 x $lmon x $globe_area_km2 x 10^6 x (16/12) / 10^12
#                                      1.0377504= 12.011 x 86400 / 10^6 converts u-mol CO2-C m-2 sec-1 to g C/m2.day
#                                      $lmon is month length (defined at the beginning of the month loop)
#                                      $globe_area_km2 is global area in km^2 (defined previously)
#                                      16/12 converts C to CH4

        factor_ch4_mon=`echo $lmon ${globe_area_km2} | awk '{printf "%10.4f",12.011*86400*$1*$2*(16/12)/10^12}'`
        echo "C*XLIN    ${factor_ch4_mon}" | ccc xlin gtcm${x}${m} ggtcm${x}${m}

        rtdlist="$rtdlist ggtcm${x}${m}"
        echo "C*XFIND       HETRES WETLND EMIS SPEC WTLND AREA(TG CH4/M) (${days} ${year_rtdiag_start}-${yearm1})" >> ic.xfind
        echo "C*XSAVE       HETRES WETLND EMIS SPEC WTLND AREA(TG CH4/M) (${days} ${year_rtdiag_start}-${year})
NEWNAM     CH4H" >> ic.xsave
        fi

# ------------------------------------ monthly averaged global wetland emissions 2 - CH4N

        x="CH4N"
        if [ -s cm${x}${m} ] ; then
        timavg cm${x}${m} tavg
        globavg tavg _ gtcm${x}${m}

#                                      convert u-mol CO2-C m-2 sec-1 to Tg CH4/month
        echo "C*XLIN    ${factor_ch4_mon}" | ccc xlin gtcm${x}${m} ggtcm${x}${m}

        rtdlist="$rtdlist ggtcm${x}${m}"
        echo "C*XFIND       NPP WETLAND EMIS SPEC WETLND AREA (TG CH4/M) (${days} ${year_rtdiag_start}-${yearm1})" >> ic.xfind
        echo "C*XSAVE       NPP WETLAND EMIS SPEC WETLND AREA (TG CH4/M) (${days} ${year_rtdiag_start}-${year})
NEWNAM     CH4N" >> ic.xsave
        fi

# ------------------------------------ monthly averaged dynamic global wetland emissions 1 - CW1D

        x="CW1D"
        if [ -s cm${x}${m} ] ; then
        timavg cm${x}${m} tavg
        globavg tavg _ gtcm${x}${m}

#                                      convert u-mol CO2-C m-2 sec-1 to Tg C/month
        echo "C*XLIN    ${factor_ch4_mon}" | ccc xlin gtcm${x}${m} ggtcm${x}${m}

        rtdlist="$rtdlist ggtcm${x}${m}"
        echo "C*XFIND       HETRES WETLND EMIS DYN WTLND AREA (TG CH4/M) (${days} ${year_rtdiag_start}-${yearm1})" >> ic.xfind
        echo "C*XSAVE       HETRES WETLND EMIS DYN WTLND AREA (TG CH4/M) (${days} ${year_rtdiag_start}-${year})
NEWNAM     CW1D" >> ic.xsave
        fi

# ------------------------------------ monthly averaged dynamic global wetland emissions 2 - CW2D

        x="CW2D"
        if [ -s cm${x}${m} ] ; then
        timavg cm${x}${m} tavg
        globavg tavg _ gtcm${x}${m}

#                                      convert u-mol CO2-C m-2 sec-1 to Tg C/month
        echo "C*XLIN    ${factor_ch4_mon}" | ccc xlin gtcm${x}${m} ggtcm${x}${m}

        rtdlist="$rtdlist ggtcm${x}${m}"
        echo "C*XFIND       NPP WETLAND EMIS DYN WETLAND AREA (TG CH4/M) (${days} ${year_rtdiag_start}-${yearm1})" >> ic.xfind
        echo "C*XSAVE       NPP WETLAND EMIS DYN WETLAND AREA (TG CH4/M) (${days} ${year_rtdiag_start}-${year})
NEWNAM     CW2D" >> ic.xsave
        fi

# ------------------------------------ monthly averaged leaf area index CLAI

        x="CLAI"
        if [ -s cm${x}${m} ] ; then
        timavg cm${x}${m} tavg
        globavg tavg _ gtcm${x}${m}    tland_frac
        globavg tavg _ gtcm${x}nhe${m} tland_frac_nhe
        globavg tavg _ gtcm${x}she${m} tland_frac_she
        globavg tavg _ gtcm${x}tro${m} tland_frac_tro

        rtdlist="$rtdlist gtcm${x}${m} gtcm${x}nhe${m} gtcm${x}she${m} gtcm${x}tro${m}"
        echo "C*XFIND       LEAF AREA INDEX                              (${days} ${year_rtdiag_start}-${yearm1})
C*XFIND       LEAF AREA INDEX NORTH OF 30N                 (${days} ${year_rtdiag_start}-${yearm1})
C*XFIND       LEAF AREA INDEX SOUTH OF 30S                 (${days} ${year_rtdiag_start}-${yearm1})
C*XFIND       LEAF AREA INDEX 30S-30N                      (${days} ${year_rtdiag_start}-${yearm1})" >> ic.xfind
        echo "C*XSAVE       LEAF AREA INDEX                              (${days} ${year_rtdiag_start}-${year})
NEWNAM     CLAI
C*XSAVE       LEAF AREA INDEX NORTH OF 30N                 (${days} ${year_rtdiag_start}-${year})
NEWNAM     CLAI
C*XSAVE       LEAF AREA INDEX SOUTH OF 30S                 (${days} ${year_rtdiag_start}-${year})
NEWNAM     CLAI
C*XSAVE       LEAF AREA INDEX 30S-30N                      (${days} ${year_rtdiag_start}-${year})
NEWNAM     CLAI" >> ic.xsave
        fi

# ************************************ Land Carbon annual section

        if [ ${m} -eq 12 ] ; then

# ------------------------------------ CVEG year end vegetation

        x="CVEG"
        echo "C*RCOPY           31        31" | ccc rcopy cm${x}${m} tavg${x}
        globavg tavg${x} _ gtavg
#                                      convert Kg to Pg C.
#                                      x area of Earth 510099699km2 x 10^6(km2->m2) / 10^12=510.099699
        factor_kg2pgc=`echo ${globe_area_km2} | awk '{printf "%10.6f",$1/10^6}'`
        echo "C*XLIN    ${factor_kg2pgc}" | ccc xlin gtavg gtcm$x

        rtdlist_ann="$rtdlist_ann gtcm$x"
        echo "C*XFIND       VEGETATION BIOMASS (PG C)                    (ANN ${year_rtdiag_start}-${yearm1})" >> ic.xfind_ann
        echo "C*XSAVE       VEGETATION BIOMASS (PG C)                    (ANN ${year_rtdiag_start}-${year})
NEWNAM     CVEG" >> ic.xsave_ann

# ------------------------------------ CDEB year end litter

        x="CDEB"
        echo "C*RCOPY           31        31" | ccc rcopy cm${x}${m} tavg${x}
        globavg tavg${x} _ gtavg
#                                      convert Kg to Pg C.
#                                      x area of Earth 510099699km2 x 10^6 (km2->m2) / 10^12
        echo "C*XLIN    ${factor_kg2pgc}" | ccc xlin gtavg gtcm$x

        rtdlist_ann="$rtdlist_ann gtcm$x"
        echo "C*XFIND       LITTER MASS (PG C)                           (ANN ${year_rtdiag_start}-${yearm1})" >> ic.xfind_ann
        echo "C*XSAVE       LITTER MASS (PG C)                           (ANN ${year_rtdiag_start}-${year})
NEWNAM     CDEB" >> ic.xsave_ann

# ------------------------------------ CHUM year end soil C

        x="CHUM"
        echo "C*RCOPY           31        31" | ccc rcopy cm${x}${m} tavg${x}
        globavg tavg${x} _ gtavg
#                                      convert Kg to Pg C.
#                                      x area of Earth 510099699km2 x 10^6 (km2->m2) / 10^12
        echo "C*XLIN    ${factor_kg2pgc}" | ccc xlin gtavg gtcm$x

        rtdlist_ann="$rtdlist_ann gtcm$x"
        echo "C*XFIND       SOIL CARBON MASS (PG C)                      (ANN ${year_rtdiag_start}-${yearm1})" >> ic.xfind_ann
        echo "C*XSAVE       SOIL CARBON MASS (PG C)                      (ANN ${year_rtdiag_start}-${year})
NEWNAM     CHUM" >> ic.xsave_ann

# ------------------------------------ annual global CBRN

        x="CBRN"
#                                      find annual mean and global average
        timavg cm${x}01 tavg${x} _ _ cm${x}02 cm${x}03 cm${x}04 cm${x}05 cm${x}06 cm${x}07 cm${x}08 cm${x}09 cm${x}10 cm${x}11 cm${x}12
#       treat CBRN differently. I need to find global burned area.
#       multiply average annual area by 365 to get annual area burned
        echo "C*XLIN         365.0" | ccc xlin tavg${x} annual_burn_area

#       get area of grid cells
        if [ -z "$nlat" -o -z "$lonsl" ] ; then
          echo "Error: **** CarbL: nlat or lonsl are undefined. ****"
          exit 1
        fi
        echo "C*MKWGHT  $nlat    1$lonsl    1$nlat  1 0    0" | ccc mkwght grid_cell_area_fraction

        div annual_burn_area grid_cell_area_fraction fractional_area_burned
        globavg fractional_area_burned _ gtcm$x

        rtdlist_ann="$rtdlist_ann gtcm$x"
        echo "C*XFIND       GLOBAL BURNED AREA (KM2/YEAR)                (ANN ${year_rtdiag_start}-${yearm1})" >> ic.xfind_ann
        echo "C*XSAVE       GLOBAL BURNED AREA (KM2/YEAR)                (ANN ${year_rtdiag_start}-${year})
NEWNAM     CBRN" >> ic.xsave_ann

# ------------------------------------ annual global CFNP

        x="CFNP"
#                                      find annual mean and global average
        timavg cm${x}01 tavg${x} _ _ cm${x}02 cm${x}03 cm${x}04 cm${x}05 cm${x}06 cm${x}07 cm${x}08 cm${x}09 cm${x}10 cm${x}11 cm${x}12
        globavg tavg${x} _ gtavg
#                                      convert u-mol CO2-C m-2 sec-1 to Pg C/year
#                                      x 12.011 / 10^6 to convert u-mol CO2-C to g C
#                                      x 365 x 86400  for 1/sec to 1/year,
#                                      x 510099699km2 x 10^6 (km2->m2) for global total in g C
#                                    / 10^15 to convert g to Pg C = 193.215001
        echo "C*XLIN    193.215001" | ccc xlin gtavg gtcm$x

        rtdlist_ann="$rtdlist_ann gtcm$x"
        echo "C*XFIND       NET PRIMARY PRODUCTIVITY (PG C/YR)           (ANN ${year_rtdiag_start}-${yearm1})" >> ic.xfind_ann
        echo "C*XSAVE       NET PRIMARY PRODUCTIVITY (PG C/YR)           (ANN ${year_rtdiag_start}-${year})
NEWNAM     CFNP" >> ic.xsave_ann

# ------------------------------------ annual global CFNE

        x="CFNE"
#                                      find annual mean and global average
        timavg cm${x}01 tavg${x} _ _ cm${x}02 cm${x}03 cm${x}04 cm${x}05 cm${x}06 cm${x}07 cm${x}08 cm${x}09 cm${x}10 cm${x}11 cm${x}12
        globavg tavg${x} _ gtavg
#                                      convert u-mol CO2-C m-2 sec-1 to Pg C/year
        echo "C*XLIN    193.215001" | ccc xlin gtavg gtcm$x

        rtdlist_ann="$rtdlist_ann gtcm$x"
        echo "C*XFIND       NET ECOSYSTEM PRODUCTIVITY (PG C/YR)         (ANN ${year_rtdiag_start}-${yearm1})" >> ic.xfind_ann
        echo "C*XSAVE       NET ECOSYSTEM PRODUCTIVITY (PG C/YR)         (ANN ${year_rtdiag_start}-${year})
NEWNAM     CFNE" >> ic.xsave_ann

# ------------------------------------ annual global CFRV

        x="CFRV"
#                                      find annual mean and global average
        timavg cm${x}01 tavg${x} _ _ cm${x}02 cm${x}03 cm${x}04 cm${x}05 cm${x}06 cm${x}07 cm${x}08 cm${x}09 cm${x}10 cm${x}11 cm${x}12
        globavg tavg${x} _ gtavg
#                                      convert u-mol CO2-C m-2 sec-1 to Pg C/year
        echo "C*XLIN    193.215001" | ccc xlin gtavg gtcm$x

        rtdlist_ann="$rtdlist_ann gtcm$x"
        echo "C*XFIND       AUTOTROPHIC RESPIRATION (PG C/YR)            (ANN ${year_rtdiag_start}-${yearm1})" >> ic.xfind_ann
        echo "C*XSAVE       AUTOTROPHIC RESPIRATION (PG C/YR)            (ANN ${year_rtdiag_start}-${year})
NEWNAM     CFRV" >> ic.xsave_ann

# ------------------------------------ annual global CFGP

        x="CFGP"
#                                      find annual mean and global average
        timavg cm${x}01 tavg${x} _ _ cm${x}02 cm${x}03 cm${x}04 cm${x}05 cm${x}06 cm${x}07 cm${x}08 cm${x}09 cm${x}10 cm${x}11 cm${x}12
        globavg tavg${x} _ gtavg
#                                      convert u-mol CO2-C m-2 sec-1 to Pg C/year
        echo "C*XLIN    193.215001" | ccc xlin gtavg gtcm$x

        rtdlist_ann="$rtdlist_ann gtcm$x"
        echo "C*XFIND       GROSS PRIMARY PRODUCTIVITY (PG C/YR)         (ANN ${year_rtdiag_start}-${yearm1})" >> ic.xfind_ann
        echo "C*XSAVE       GROSS PRIMARY PRODUCTIVITY (PG C/YR)         (ANN ${year_rtdiag_start}-${year})
NEWNAM     CFGP" >> ic.xsave_ann

# ------------------------------------ annual global CFNB

        x="CFNB"
#                                      find annual mean and global average
        timavg cm${x}01 tavg${x} _ _ cm${x}02 cm${x}03 cm${x}04 cm${x}05 cm${x}06 cm${x}07 cm${x}08 cm${x}09 cm${x}10 cm${x}11 cm${x}12
        globavg tavg${x} _ gtavg
#                                      convert u-mol CO2-C m-2 sec-1 to Pg C/year
        echo "C*XLIN    193.215001" | ccc xlin gtavg gtcm$x

        rtdlist_ann="$rtdlist_ann gtcm$x"
        echo "C*XFIND       NET BIOME PRODUCTIVITY (PG C/YR)             (ANN ${year_rtdiag_start}-${yearm1})" >> ic.xfind_ann
        echo "C*XSAVE       NET BIOME PRODUCTIVITY (PG C/YR)             (ANN ${year_rtdiag_start}-${year})
NEWNAM     CFNB" >> ic.xsave_ann

# ------------------------------------ annual global CFLV

        x="CFLV"
#                                      find annual mean and global average
        timavg cm${x}01 tavg${x} _ _ cm${x}02 cm${x}03 cm${x}04 cm${x}05 cm${x}06 cm${x}07 cm${x}08 cm${x}09 cm${x}10 cm${x}11 cm${x}12
        globavg tavg${x} _ gtavg
#                                      convert u-mol CO2-C m-2 sec-1 to Pg C/year
        echo "C*XLIN    193.215001" | ccc xlin gtavg gtcm$x

        rtdlist_ann="$rtdlist_ann gtcm$x"
        echo "C*XFIND       LUC VEG. COMBUST. EMISSION (PG C/YR)         (ANN ${year_rtdiag_start}-${yearm1})" >> ic.xfind_ann
        echo "C*XSAVE       LUC VEG. COMBUST. EMISSION (PG C/YR)         (ANN ${year_rtdiag_start}-${year})
NEWNAM     CFLV" >> ic.xsave_ann

# ------------------------------------ annual global CFLD

        x="CFLD"
#                                      find annual mean and global average
        timavg cm${x}01 tavg${x} _ _ cm${x}02 cm${x}03 cm${x}04 cm${x}05 cm${x}06 cm${x}07 cm${x}08 cm${x}09 cm${x}10 cm${x}11 cm${x}12
        globavg tavg${x} _ gtavg
#                                      convert u-mol CO2-C m-2 sec-1 to Pg C/year
        echo "C*XLIN    193.215001" | ccc xlin gtavg gtcm$x

        rtdlist_ann="$rtdlist_ann gtcm$x"
        echo "C*XFIND       LUC LITTER INPUTS (PG C/YR)                  (ANN ${year_rtdiag_start}-${yearm1})" >> ic.xfind_ann
        echo "C*XSAVE       LUC LITTER INPUTS (PG C/YR)                  (ANN ${year_rtdiag_start}-${year})
NEWNAM     CFLD" >> ic.xsave_ann

# ------------------------------------ annual global CFLH

        x="CFLH"
#                                      find annual mean and global average
        timavg cm${x}01 tavg${x} _ _ cm${x}02 cm${x}03 cm${x}04 cm${x}05 cm${x}06 cm${x}07 cm${x}08 cm${x}09 cm${x}10 cm${x}11 cm${x}12
        globavg tavg${x} _ gtavg
#                                      convert u-mol CO2-C m-2 sec-1 to Pg C/year
        echo "C*XLIN    193.215001" | ccc xlin gtavg gtcm$x

        rtdlist_ann="$rtdlist_ann gtcm$x"
        echo "C*XFIND       LUC SOIL CARBON INPUTS (PG C/YR)             (ANN ${year_rtdiag_start}-${yearm1})" >> ic.xfind_ann
        echo "C*XSAVE       LUC SOIL CARBON INPUTS (PG C/YR)             (ANN ${year_rtdiag_start}-${year})
NEWNAM     CFLH" >> ic.xsave_ann

# ------------------------------------ annual global CFRH

        x="CFRH"
#                                      find annual mean and global average
        timavg cm${x}01 tavg${x} _ _ cm${x}02 cm${x}03 cm${x}04 cm${x}05 cm${x}06 cm${x}07 cm${x}08 cm${x}09 cm${x}10 cm${x}11 cm${x}12
        globavg tavg${x} _ gtavg
#                                      convert u-mol CO2-C m-2 sec-1 to Pg C/year
        echo "C*XLIN    193.215001" | ccc xlin gtavg gtcm$x

        rtdlist_ann="$rtdlist_ann gtcm$x"
        echo "C*XFIND       SOIL CARBON RESPIRATION (PG C/YR)            (ANN ${year_rtdiag_start}-${yearm1})" >> ic.xfind_ann
        echo "C*XSAVE       SOIL CARBON RESPIRATION (PG C/YR)            (ANN ${year_rtdiag_start}-${year})
NEWNAM     CFRH" >> ic.xsave_ann

# ------------------------------------ annual global CFHT

        x="CFHT"
#                                      find annual mean and global average
        timavg cm${x}01 tavg${x} _ _ cm${x}02 cm${x}03 cm${x}04 cm${x}05 cm${x}06 cm${x}07 cm${x}08 cm${x}09 cm${x}10 cm${x}11 cm${x}12
        globavg tavg${x} _ gtavg
#                                      convert u-mol CO2-C m-2 sec-1 to Pg C/year
        echo "C*XLIN    193.215001" | ccc xlin gtavg gtcm$x

        rtdlist_ann="$rtdlist_ann gtcm$x"
        echo "C*XFIND       LITTER TO SOIL C TRANSFER (PG C/YR)          (ANN ${year_rtdiag_start}-${yearm1})" >> ic.xfind_ann
        echo "C*XSAVE       LITTER TO SOIL C TRANSFER (PG C/YR)          (ANN ${year_rtdiag_start}-${year})
NEWNAM     CFHT" >> ic.xsave_ann

# ------------------------------------ annual global CFLF

        x="CFLF"
#                                      find annual mean and global average
        timavg cm${x}01 tavg${x} _ _ cm${x}02 cm${x}03 cm${x}04 cm${x}05 cm${x}06 cm${x}07 cm${x}08 cm${x}09 cm${x}10 cm${x}11 cm${x}12
        globavg tavg${x} _ gtavg
#                                      convert u-mol CO2-C m-2 sec-1 to Pg C/year
        echo "C*XLIN    193.215001" | ccc xlin gtavg gtcm$x

        rtdlist_ann="$rtdlist_ann gtcm$x"
        echo "C*XFIND       LITTER FALL (PG C/YR)                        (ANN ${year_rtdiag_start}-${yearm1})" >> ic.xfind_ann
        echo "C*XSAVE       LITTER FALL (PG C/YR)                        (ANN ${year_rtdiag_start}-${year})
NEWNAM     CFLF" >> ic.xsave_ann

# ------------------------------------ annual global CFRD

        x="CFRD"
#                                      find annual mean and global average
        timavg cm${x}01 tavg${x} _ _ cm${x}02 cm${x}03 cm${x}04 cm${x}05 cm${x}06 cm${x}07 cm${x}08 cm${x}09 cm${x}10 cm${x}11 cm${x}12
        globavg tavg${x} _ gtavg
#                                      convert u-mol CO2-C m-2 sec-1 to Pg C/year
        echo "C*XLIN    193.215001" | ccc xlin gtavg gtcm$x

        rtdlist_ann="$rtdlist_ann gtcm$x"
        echo "C*XFIND       LITTER RESPIRATION (PG C/YR)                 (ANN ${year_rtdiag_start}-${yearm1})" >> ic.xfind_ann
        echo "C*XSAVE       LITTER RESPIRATION (PG C/YR)                 (ANN ${year_rtdiag_start}-${year})
NEWNAM     CFRD" >> ic.xsave_ann

# ------------------------------------ annual global CFFD

        x="CFFD"
#                                      find annual mean and global average
        timavg cm${x}01 tavg${x} _ _ cm${x}02 cm${x}03 cm${x}04 cm${x}05 cm${x}06 cm${x}07 cm${x}08 cm${x}09 cm${x}10 cm${x}11 cm${x}12
        globavg tavg${x} _ gtavg
#                                      convert u-mol CO2-C m-2 sec-1 to Pg C/year
        echo "C*XLIN    193.215001" | ccc xlin gtavg gtcm$x

        rtdlist_ann="$rtdlist_ann gtcm$x"
        echo "C*XFIND       FIRE EMISSION FROM LITTER (PG C/YR)          (ANN ${year_rtdiag_start}-${yearm1})" >> ic.xfind_ann
        echo "C*XSAVE       FIRE EMISSION FROM LITTER (PG C/YR)          (ANN ${year_rtdiag_start}-${year})
NEWNAM     CFFD" >> ic.xsave_ann

# ------------------------------------ annual global CFFV

        x="CFFV"
#                                      find annual mean and global average
        timavg cm${x}01 tavg${x} _ _ cm${x}02 cm${x}03 cm${x}04 cm${x}05 cm${x}06 cm${x}07 cm${x}08 cm${x}09 cm${x}10 cm${x}11 cm${x}12
        globavg tavg${x} _ gtavg
#                                      convert u-mol CO2-C m-2 sec-1 to Pg C/year
        echo "C*XLIN    193.215001" | ccc xlin gtavg gtcm$x

        rtdlist_ann="$rtdlist_ann gtcm$x"
        echo "C*XFIND       FIRE EMISSION FROM VEGETATION (PG C/YR)      (ANN ${year_rtdiag_start}-${yearm1})" >> ic.xfind_ann
        echo "C*XSAVE       FIRE EMISSION FROM VEGETATION (PG C/YR)      (ANN ${year_rtdiag_start}-${year})
NEWNAM     CFFV" >> ic.xsave_ann

# ------------------------------------ annual averaged global wetland area calculated from wetland fraction

        x="WFRA"
        if [ -s cm${x}01 ] ; then
#                                      find annual mean and global average
        timavg cm${x}01 tavg _ _ cm${x}02 cm${x}03 cm${x}04 cm${x}05 cm${x}06 cm${x}07 cm${x}08 cm${x}09 cm${x}10 cm${x}11 cm${x}12
        globavg tavg _ gtavg

#                                      convert to km2
        gmlt gtavg globe_area_km2 gtcm$x

        rtdlist_ann="$rtdlist_ann gtcm$x"
        echo "C*XFIND       DYNAMIC WETLAND AREA (KM2)                   (ANN ${year_rtdiag_start}-${yearm1})" >> ic.xfind_ann
        echo "C*XSAVE       DYNAMIC WETLAND AREA (KM2)                   (ANN ${year_rtdiag_start}-${year})
NEWNAM     WFRA" >> ic.xsave_ann
        fi

# ------------------------------------ annual global CH4H

        x="CH4H"
        if [ -s cm${x}01 ] ; then
#                                      find annual mean and global average
        timavg cm${x}01 tavg _ _ cm${x}02 cm${x}03 cm${x}04 cm${x}05 cm${x}06 cm${x}07 cm${x}08 cm${x}09 cm${x}10 cm${x}11 cm${x}12
        globavg tavg _ gtavg
#                                      convert u-mol CO2-C m-2 sec-1 to Tg CH4/year
#                                      1.0377504 x 365 x $globe_area_km2 x 10^6 x (16/12) / 10^12 = 257620.001
#                                      1.0377504= 12.011 x 86400 / 10^6 converts u-mol CO2-C m-2 sec-1 to g C/m2.day
#                                      16/12 converts C to CH4

        factor_ch4_ann=`echo 365 ${globe_area_km2} | awk '{printf "%10.3f",12.011*86400*$1*$2*(16/12)/10^12}'`
        echo "C*XLIN    ${factor_ch4_ann}" | ccc xlin gtavg gtcm$x

        rtdlist_ann="$rtdlist_ann gtcm$x"
        echo "C*XFIND       HETRES WETLND EMIS SPEC WTLND AREA(TG CH4/Y) (ANN ${year_rtdiag_start}-${yearm1})" >> ic.xfind_ann
        echo "C*XSAVE       HETRES WETLND EMIS SPEC WTLND AREA(TG CH4/Y) (ANN ${year_rtdiag_start}-${year})
NEWNAM     CH4H" >> ic.xsave_ann
        fi

# ------------------------------------ annual global CH4N

        x="CH4N"
        if [ -s cm${x}01 ] ; then
#                                      find annual mean and global average
        timavg cm${x}01 tavg _ _ cm${x}02 cm${x}03 cm${x}04 cm${x}05 cm${x}06 cm${x}07 cm${x}08 cm${x}09 cm${x}10 cm${x}11 cm${x}12
        globavg tavg _ gtavg
#                                      convert u-mol CO2-C m-2 sec-1 to Tg CH4/year
        echo "C*XLIN    ${factor_ch4_ann}" | ccc xlin gtavg gtcm$x

        rtdlist_ann="$rtdlist_ann gtcm$x"
        echo "C*XFIND       NPP WETLND EMIS SPEC WETLAND AREA (TG CH4/Y) (ANN ${year_rtdiag_start}-${yearm1})" >> ic.xfind_ann
        echo "C*XSAVE       NPP WETLND EMIS SPEC WETLAND AREA (TG CH4/Y) (ANN ${year_rtdiag_start}-${year})
NEWNAM     CH4N" >> ic.xsave_ann
        fi

# ------------------------------------ annual global CW1D

        x="CW1D"
        if [ -s cm${x}01 ] ; then
#                                      find annual mean and global average
        timavg cm${x}01 tavg _ _ cm${x}02 cm${x}03 cm${x}04 cm${x}05 cm${x}06 cm${x}07 cm${x}08 cm${x}09 cm${x}10 cm${x}11 cm${x}12
        globavg tavg _ gtavg
#                                      convert u-mol CO2-C m-2 sec-1 to Tg CH4/year
        echo "C*XLIN    ${factor_ch4_ann}" | ccc xlin gtavg gtcm$x

        rtdlist_ann="$rtdlist_ann gtcm$x"
        echo "C*XFIND       HETRES WETLND EMIS DYN WTLND AREA (TG CH4/Y) (ANN ${year_rtdiag_start}-${yearm1})" >> ic.xfind_ann
        echo "C*XSAVE       HETRES WETLND EMIS DYN WTLND AREA (TG CH4/Y) (ANN ${year_rtdiag_start}-${year})
NEWNAM     CW1D" >> ic.xsave_ann
        fi

# ------------------------------------ annual global CW2D

        x="CW2D"
        if [ -s cm${x}01 ] ; then
#                                      find annual mean and global average
        timavg cm${x}01 tavg _ _ cm${x}02 cm${x}03 cm${x}04 cm${x}05 cm${x}06 cm${x}07 cm${x}08 cm${x}09 cm${x}10 cm${x}11 cm${x}12
        globavg tavg _ gtavg
#                                      convert u-mol CO2-C m-2 sec-1 to Tg CH4/year
        echo "C*XLIN    ${factor_ch4_ann}" | ccc xlin gtavg gtcm$x

        rtdlist_ann="$rtdlist_ann gtcm$x"
        echo "C*XFIND       NPP WETLND EMIS DYN WETLAND AREA (TG CH4/Y)  (ANN ${year_rtdiag_start}-${yearm1})" >> ic.xfind_ann
        echo "C*XSAVE       NPP WETLND EMIS DYN WETLAND AREA (TG CH4/Y)  (ANN ${year_rtdiag_start}-${year})
NEWNAM     CW2D" >> ic.xsave_ann
        fi

# ------------------------------------ annual global CLAI

        x="CLAI"
        if [ -s cm${x}01 ] ; then
#                                      find annual mean and global average
        timavg cm${x}01 tavg _ _ cm${x}02 cm${x}03 cm${x}04 cm${x}05 cm${x}06 cm${x}07 cm${x}08 cm${x}09 cm${x}10 cm${x}11 cm${x}12
        globavg tavg _ gtcm${x}    tland_frac
        globavg tavg _ gtcm${x}nhe tland_frac_nhe
        globavg tavg _ gtcm${x}she tland_frac_she
        globavg tavg _ gtcm${x}tro tland_frac_tro

        rtdlist_ann="$rtdlist_ann gtcm$x gtcm${x}nhe gtcm${x}she gtcm${x}tro"
        echo "C*XFIND       LEAF AREA INDEX                              (ANN ${year_rtdiag_start}-${yearm1})
C*XFIND       LEAF AREA INDEX NORTH OF 30N                 (ANN ${year_rtdiag_start}-${yearm1})
C*XFIND       LEAF AREA INDEX SOUTH OF 30S                 (ANN ${year_rtdiag_start}-${yearm1})
C*XFIND       LEAF AREA INDEX 30S-30N                      (ANN ${year_rtdiag_start}-${yearm1})" >> ic.xfind_ann
        echo "C*XSAVE       LEAF AREA INDEX                              (ANN ${year_rtdiag_start}-${year})
NEWNAM     CLAI
C*XSAVE       LEAF AREA INDEX NORTH OF 30N                 (ANN ${year_rtdiag_start}-${year})
NEWNAM     CLAI
C*XSAVE       LEAF AREA INDEX SOUTH OF 30S                 (ANN ${year_rtdiag_start}-${year})
NEWNAM     CLAI
C*XSAVE       LEAF AREA INDEX 30S-30N                      (ANN ${year_rtdiag_start}-${year})
NEWNAM     CLAI" >> ic.xsave_ann
        fi

        fi # m=12
      fi # CarbL=on

# ------------------------------------ add the number of seconds since January 1, 1970 (CUT)

      if [ ${m} -eq 12 ] ; then
      x="SECS"
      datesec=`date +%s`
      echo " TIME      1001 SECS         0         1         1    100101        -1
$datesec.E00" > datesec.txt
      chabin datesec.txt gtgs$x
      rtdlist_ann="$rtdlist_ann gtgs$x"
      echo "C*XFIND       DATE IN SECONDS SINCE 1970                   (ANN ${year_rtdiag_start}-${yearm1})" >> ic.xfind_ann
      echo "C*XSAVE       DATE IN SECONDS SINCE 1970                   (ANN ${year_rtdiag_start}-${year})
NEWNAM     SECS" >> ic.xsave_ann
      fi # $m=12

# ------------------------------------ add ocean/land mask area fractions

      if [ ${m} -eq 12 ] ; then
      rtdlist_ann="$rtdlist_ann globe_area dland_area tland_area lland_area tland_area_nh tland_area_sh tland_area_nhe tland_area_she tland_area_tro ocean_area ocean_area_nh ocean_area_sh rlake_area ulake_area water_area nogla_area noglb_area glaci_area bedro_area dland_favg tland_favg lland_favg tland_favg_nh tland_favg_sh tland_favg_nhe tland_favg_she tland_favg_tro ocean_favg ocean_favg_nh ocean_favg_sh rlake_favg ulake_favg water_favg nogla_favg noglb_favg glaci_favg bedro_favg"
      echo "C*XFIND       GLOBAL AREA (M2)                             (ANN ${year_rtdiag_start}-${yearm1})
C*XFIND       DRY LAND AREA (M2)                           (ANN ${year_rtdiag_start}-${yearm1})
C*XFIND       LAND AREA (M2)                               (ANN ${year_rtdiag_start}-${yearm1})
C*XFIND       LAND AREA ONLY (M2)                          (ANN ${year_rtdiag_start}-${yearm1})
C*XFIND       LAND AREA NH (M2)                            (ANN ${year_rtdiag_start}-${yearm1})
C*XFIND       LAND AREA SH (M2)                            (ANN ${year_rtdiag_start}-${yearm1})
C*XFIND       LAND AREA NHE (M2)                           (ANN ${year_rtdiag_start}-${yearm1})
C*XFIND       LAND AREA SHE (M2)                           (ANN ${year_rtdiag_start}-${yearm1})
C*XFIND       LAND AREA TRO (M2)                           (ANN ${year_rtdiag_start}-${yearm1})
C*XFIND       OCEAN AREA (M2)                              (ANN ${year_rtdiag_start}-${yearm1})
C*XFIND       OCEAN AREA NH (M2)                           (ANN ${year_rtdiag_start}-${yearm1})
C*XFIND       OCEAN AREA SH (M2)                           (ANN ${year_rtdiag_start}-${yearm1})
C*XFIND       RESOLVED LAKES AREA (M2)                     (ANN ${year_rtdiag_start}-${yearm1})
C*XFIND       PARAMETERIZED LAKES AREA (M2)                (ANN ${year_rtdiag_start}-${yearm1})
C*XFIND       WATER AREA (M2)                              (ANN ${year_rtdiag_start}-${yearm1})
C*XFIND       LAND W/O GLACIERS AREA (M2)                  (ANN ${year_rtdiag_start}-${yearm1})
C*XFIND       LAND W/O GLACIERS/BEDROCK AREA (M2)          (ANN ${year_rtdiag_start}-${yearm1})
C*XFIND       GLACIERS AREA (M2)                           (ANN ${year_rtdiag_start}-${yearm1})
C*XFIND       BEDROCK AREA (M2)                            (ANN ${year_rtdiag_start}-${yearm1})
C*XFIND       DRY LAND AREA FRACTION                       (ANN ${year_rtdiag_start}-${yearm1})
C*XFIND       LAND AREA FRACTION                           (ANN ${year_rtdiag_start}-${yearm1})
C*XFIND       LAND ONLY AREA FRACTION                      (ANN ${year_rtdiag_start}-${yearm1})
C*XFIND       LAND AREA FRACTION NH                        (ANN ${year_rtdiag_start}-${yearm1})
C*XFIND       LAND AREA FRACTION SH                        (ANN ${year_rtdiag_start}-${yearm1})
C*XFIND       LAND AREA FRACTION NHE                       (ANN ${year_rtdiag_start}-${yearm1})
C*XFIND       LAND AREA FRACTION SHE                       (ANN ${year_rtdiag_start}-${yearm1})
C*XFIND       LAND AREA FRACTION TRO                       (ANN ${year_rtdiag_start}-${yearm1})
C*XFIND       OCEAN AREA FRACTION                          (ANN ${year_rtdiag_start}-${yearm1})
C*XFIND       OCEAN AREA FRACTION NH                       (ANN ${year_rtdiag_start}-${yearm1})
C*XFIND       OCEAN AREA FRACTION SH                       (ANN ${year_rtdiag_start}-${yearm1})
C*XFIND       RESOLVED LAKES AREA FRACTION                 (ANN ${year_rtdiag_start}-${yearm1})
C*XFIND       PARAMETERIZED LAKES AREA FRACTION            (ANN ${year_rtdiag_start}-${yearm1})
C*XFIND       WATER AREA FRACTION                          (ANN ${year_rtdiag_start}-${yearm1})
C*XFIND       LAND W/O GLACIERS AREA FRACTION              (ANN ${year_rtdiag_start}-${yearm1})
C*XFIND       LAND W/O GLACIERS/BEDROCK AREA FRACTION      (ANN ${year_rtdiag_start}-${yearm1})
C*XFIND       GLACIERS AREA FRACTION                       (ANN ${year_rtdiag_start}-${yearm1})
C*XFIND       BEDROCK AREA FRACTION                        (ANN ${year_rtdiag_start}-${yearm1})" >> ic.xfind_ann
      echo "C*XSAVE       GLOBAL AREA (M2)                             (ANN ${year_rtdiag_start}-${year})
NEWNAM     AREA
C*XSAVE       DRY LAND AREA (M2)                           (ANN ${year_rtdiag_start}-${year})
NEWNAM     AREA
C*XSAVE       LAND AREA (M2)                               (ANN ${year_rtdiag_start}-${year})
NEWNAM     AREA
C*XSAVE       LAND ONLY AREA (M2)                          (ANN ${year_rtdiag_start}-${year})
NEWNAM     AREA
C*XSAVE       LAND AREA NH (M2)                            (ANN ${year_rtdiag_start}-${year})
NEWNAM     AREA
C*XSAVE       LAND AREA SH (M2)                            (ANN ${year_rtdiag_start}-${year})
NEWNAM     AREA
C*XSAVE       LAND AREA NHE (M2)                           (ANN ${year_rtdiag_start}-${year})
NEWNAM     AREA
C*XSAVE       LAND AREA SHE (M2)                           (ANN ${year_rtdiag_start}-${year})
NEWNAM     AREA
C*XSAVE       LAND AREA TRO (M2)                           (ANN ${year_rtdiag_start}-${year})
NEWNAM     AREA
C*XSAVE       OCEAN AREA (M2)                              (ANN ${year_rtdiag_start}-${year})
NEWNAM     AREA
C*XSAVE       OCEAN AREA NH (M2)                           (ANN ${year_rtdiag_start}-${year})
NEWNAM     AREA
C*XSAVE       OCEAN AREA SH (M2)                           (ANN ${year_rtdiag_start}-${year})
NEWNAM     AREA
C*XSAVE       RESOLVED LAKES AREA (M2)                     (ANN ${year_rtdiag_start}-${year})
NEWNAM     AREA
C*XSAVE       PARAMETERIZED LAKES AREA (M2)                (ANN ${year_rtdiag_start}-${year})
NEWNAM     AREA
C*XSAVE       WATER AREA (M2)                              (ANN ${year_rtdiag_start}-${year})
NEWNAM     AREA
C*XSAVE       LAND W/O GLACIERS AREA (M2)                  (ANN ${year_rtdiag_start}-${year})
NEWNAM     AREA
C*XSAVE       LAND W/O GLACIERS/BEDROCK AREA (M2)          (ANN ${year_rtdiag_start}-${year})
NEWNAM     AREA
C*XSAVE       GLACIERS AREA (M2)                           (ANN ${year_rtdiag_start}-${year})
NEWNAM     AREA
C*XSAVE       BEDROCK AREA (M2)                            (ANN ${year_rtdiag_start}-${year})
NEWNAM     AREA
C*XSAVE       DRY LAND AREA FRACTION                       (ANN ${year_rtdiag_start}-${year})
NEWNAM     FRAC
C*XSAVE       LAND AREA FRACTION                           (ANN ${year_rtdiag_start}-${year})
NEWNAM     FRAC
C*XSAVE       LAND ONLY AREA FRACTION                      (ANN ${year_rtdiag_start}-${year})
NEWNAM     FRAC
C*XSAVE       LAND AREA FRACTION NH                        (ANN ${year_rtdiag_start}-${year})
NEWNAM     FRAC
C*XSAVE       LAND AREA FRACTION SH                        (ANN ${year_rtdiag_start}-${year})
NEWNAM     FRAC
C*XSAVE       LAND AREA FRACTION NHE                       (ANN ${year_rtdiag_start}-${year})
NEWNAM     FRAC
C*XSAVE       LAND AREA FRACTION SHE                       (ANN ${year_rtdiag_start}-${year})
NEWNAM     FRAC
C*XSAVE       LAND AREA FRACTION TRO                       (ANN ${year_rtdiag_start}-${year})
NEWNAM     FRAC
C*XSAVE       OCEAN AREA FRACTION                          (ANN ${year_rtdiag_start}-${year})
NEWNAM     FRAC
C*XSAVE       OCEAN AREA FRACTION NH                       (ANN ${year_rtdiag_start}-${year})
NEWNAM     FRAC
C*XSAVE       OCEAN AREA FRACTION SH                       (ANN ${year_rtdiag_start}-${year})
NEWNAM     FRAC
C*XSAVE       RESOLVED LAKES AREA FRACTION                 (ANN ${year_rtdiag_start}-${year})
NEWNAM     FRAC
C*XSAVE       PARAMETERIZED LAKES AREA FRACTION            (ANN ${year_rtdiag_start}-${year})
NEWNAM     FRAC
C*XSAVE       WATER AREA FRACTION                          (ANN ${year_rtdiag_start}-${year})
NEWNAM     FRAC
C*XSAVE       LAND W/O GLACIERS AREA FRACTION              (ANN ${year_rtdiag_start}-${year})
NEWNAM     FRAC
C*XSAVE       LAND W/O GLACIERS/BEDROCK AREA FRACTION      (ANN ${year_rtdiag_start}-${year})
NEWNAM     FRAC
C*XSAVE       GLACIERS AREA FRACTION                       (ANN ${year_rtdiag_start}-${year})
NEWNAM     FRAC
C*XSAVE       BEDROCK AREA FRACTION                        (ANN ${year_rtdiag_start}-${year})
NEWNAM     FRAC" >> ic.xsave_ann

      if [ "$PhysO" = "on" ] ; then
      rtdlist_ann="$rtdlist_ann gbetaovi gbetaovol"
      echo "C*XFIND       OCEAN MEAN DEPTH (M)                         (ANN ${year_rtdiag_start}-${yearm1})
C*XFIND       OCEAN VOLUME (M3)                            (ANN ${year_rtdiag_start}-${yearm1})" >> ic.xfind_ann
      echo "C*XSAVE       OCEAN MEAN DEPTH (M)                         (ANN ${year_rtdiag_start}-${year})
NEWNAM     ODPT
C*XSAVE       OCEAN VOLUME (M3)                            (ANN ${year_rtdiag_start}-${year})
NEWNAM     OVOL" >> ic.xsave_ann
      fi # PhysO = on

      fi # $m=12

# ------------------------------------ xsave monthly rtd statistics (split into batches of no more that $nvarmax variables)

      rtdlist=`echo $rtdlist | sed -e 's/^ *//' -e 's/  */ /g' -e 's/ *$/ /'`
      rtdlist0="$rtdlist"
      ln -sf ic.xsave ic.xsave0
      while [ -n "$rtdlist0" ] ; do
        rtdlist1=`echo "$rtdlist0" | cut -f1-$nvarmax -d' '`
        head -$nvarmax2 ic.xsave0 > ic.xsave1
        echo $rtdlist1
        head -$nvarmax2 ic.xsave1 | grep -v NEWNAM
        xsave newrtd $rtdlist1 newrtd1 input=ic.xsave1
        mv newrtd1 newrtd
        rtdlist0=`echo "$rtdlist0" | cut -f$nvarmaxp1- -d' '`
        tail -n +$nvarmax2p1 ic.xsave0 > ic.xsave1 ; mv ic.xsave1 ic.xsave0
      done
    done # month loop

# ------------------------------------ xsave annual rtd statistics (split into batches of no more that $nvarmax variables)

    rtdlist_ann=`echo "$rtdlist_ann" | sed -e 's/^ *//' -e 's/  */ /g' -e 's/ *$/ /'`
    rtdlist0="$rtdlist_ann"
    ln -sf ic.xsave_ann ic.xsave0
    while [ -n "$rtdlist0" ] ; do
      rtdlist1=`echo "$rtdlist0" | cut -f1-$nvarmax -d' '`
      head -$nvarmax2 ic.xsave0 > ic.xsave1
      echo $rtdlist1
      head -$nvarmax2 ic.xsave1 | grep -v NEWNAM
      xsave newrtd $rtdlist1 newrtd1 input=ic.xsave1
      mv newrtd1 newrtd
      rtdlist0=`echo "$rtdlist0" | cut -f$nvarmaxp1- -d' '`
      tail -n +$nvarmax2p1 ic.xsave0 > ic.xsave1 ; mv ic.xsave1 ic.xsave0
    done

#                                      relabel GRID to TIME
    echo "C*RELABL  +GRID
C*RELABL  +TIME      1001                                       100101    1" | ccc relabl newrtd newrtd1
    mv newrtd1 newrtd

# ------------------------------------ append current year to old rtd file

    if [ $year -gt ${year_rtdiag_start} ] ; then
      joinrtd oldrtd newrtd newrtd2 ; mv newrtd2 newrtd
    fi

# ************************************ save new runtime diagnostics.

    save newrtd $rtdfile

# ************************************ delete older rtd files
    if [ "$keep_old_rtdiag" != "on" ] ; then
      release old
      access  old $rtdfileo na nocp
      delete  old na || true
    fi

# ************************************ make copy to common location

    rtd_dest_dir=${rtd_dest_dir:="$RUNPATH_ROOT/../rtdfiles"} # default location for rtd files
    rm -f        $rtd_dest_dir/$rtdfile
    cp -p newrtd $rtd_dest_dir/$rtdfile
