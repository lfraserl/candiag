#deck cosp3
jobname=cosp3 ; time=$stime ; memory=$memory1
. comjcl.cdk

cat > Execute_Script <<'end_of_script'

#
# jcl - Feb. 16, 2012
#   added code to handle variable CHMK and fields for rain and snow when 
#   the radar is turned on.  Also added script to handle the old and new 
#   way to output ISCCP p-tau fields.
#
# jcl - Nov. 22, 2011 - Previous cosp2.dk.
#   added code to handle MSTZ which have more than 100 levels which is 
#   problematic for statsav
#
# jcl - Sept 19, 2011
#   added code to handle CERES fields along an orbital swath
# 
# jcl - May 16/11 
#   added code to handle MODIS output
#
# jcl - May 11/11
#   changed code to reflect new way to save some multi-level fields 
#   and MISR output.
#
# jcl - Mar 09/11
#   added code to properly weight, i.e., by cloud fraction.
#
# jcl - Feb 03/11
#   added code to handle effective radius and droplet number 
#   concentration for liquid clouds
#
#                 cosp3                jcl - Apr 30/12 - jcl
#   ---------------------------------- module to compute monthly mean fields 
#                                      related to output created by COSP
#                                      simulator.
#                                      The output from COSP is rather flexible
#                                      so deck has been set up to check for 
#                                      every possible field from COSP and if 
#                                      they are present generate diagnostics 
#                                      for them.

.   spfiles.cdk
.   ggfiles.cdk

# ----------------------------------------------------- Possible input fields
# ISCCP related fields
#
# ICCA -> Unadjusted total cloud albedo
# ICCF -> Total cloud fraction
# ICCT -> Unadjusted total cloud optical thickness (linear average)
# ICCP -> Unadjusted total cloud top pressure (cloud fraction weighted)
# SUNL -> Sunlit gridbox indicator
# ICTP -> Cloud frequency in ISCCP cloud top pressure/optical thickness bins
#
# CALIPSO related fields
#
# CLHC,CLHM -> High cloud fraction from lidar and occurrence count
# CLMC,CLMM -> Middle cloud fraction from lidar and occurrence count
# CLLC,CLLM -> Low cloud fraction from lidar and occurrence count
# CLTC,CLTM -> Total cloud fraction from lidar and occurrence count
# CLCP,CLCM -> Cloud fraction profile from lidar and occurrence count
# CCCP,CCCM -> Cloud fraction detected by lidar but not radar
# SR*       -> Backscattering ratio/height histogram
#
# PARASOL related fields
#
# PL01,ML01 -> PARASOL relfectance at 0 degree off nadir
# PL02,ML02 -> PARASOL relfectance at 15 degree off nadir
# PL03,ML03 -> PARASOL relfectance at 30 degree off nadir
# PL04,ML04 -> PARASOL relfectance at 45 degree off nadir
# PL05,ML05 -> PARASOL relfectance at 60 degree off nadir
#
# CloudSat related fields
#
# ZE*        -> Radar reflectivity/height histogram
# CCTC, CCTM -> Total cloud fraction using lidar and radar
# CHMK       -> Mask of land topography (needed when interpolating data to height grid)
# SMIX, SREF -> Snow mixing ratio and snow effective radius
# RMIX, RREF -> Rain mixing ratio and rain effective radius
#
# CERES-like fields
#
# CCCF -> Cloud fraction (liquid + ice)
# CCNT -> Cloud occurence (liquid + ice)
# CCTP -> Cloud top pressure 
# CCCT -> Cloud visible optical thickness (linear average)
# CCLT -> Cloud visible optical thickness (log average)
# CCFL -> Liquid cloud fraction
# CCFI -> Ice cloud fraction
# CLWP -> Cloud liquid water path in each pressure range
# CIWP -> Cloud ice water path in each pressure range
# CNLC -> Occurence of liquid cloud in each pressure range
# CNIC -> Occurence of ice cloud in each pressure range
# CREL -> Cloud top effective radius for liquid clouds
# CCDD -> Cloud droplet number concentrations for liquid clouds
# CCFM -> Liquid cloud fraction for microphysical parameters
# CNLC -> Occurence of liquid cloud microphysical parameters in each pressure range
#
# CERES-like cloud fields along orbit
#
# CS01 -> Cloud fraction (liquid + ice)
# CS02 -> Cloud occurence (liquid + ice)
# CS03 -> Cloud top pressure 
# CS04 -> Cloud visible optical thickness (linear average)
# CS05 -> Cloud visible optical thickness (log average)
# CS06 -> Liquid cloud fraction
# CS07 -> Ice cloud fraction
# CS08 -> Cloud liquid water path in each pressure range
# CS09 -> Cloud ice water path in each pressure range
# CS10 -> Occurence of liquid cloud in each pressure range
# CS11 -> Occurence of ice cloud in each pressure range
# CS12 -> Cloud top effective radius for liquid clouds
# CS13 -> Cloud droplet number concentrations for liquid clouds
# CS14 -> Liquid cloud fraction for microphysical parameters
# CS15 -> Occurence of liquid cloud microphysical parameters in each pressure range
#
# CERES radiative fluxes by cloud type along orbit
#
# ORBA -> Sum of occurrences of orbit in gridboxes (total orbit)
# ORBS -> Sum of occurrences of orbit in gridboxes (sunlit orbit)
# PWTC -> Precipitable water
# GTC  -> Surface temperature
# GCC  -> Surface type
# CSZC -> Cosine of solar zenith angle
# CDSW -> Upward solar flux at TOA for each cloud type
# CULW -> Upward thermal flux at TOA for each cloud type
# CFSO -> Downward solar flux at the TOA (for each cloud type for convience)
# CLRF -> Occurrence of clear sky
#
# MISR related fields
#
# MSTC -> Total cloud fraction from MISR
# MSMZ -> Cloud-mean cloud top height
# MSDT -> Occurence of model layers in particular MISR height ranges
# MSTZ -> Cloud top height versus optical thickness 
#
# MODIS related fields
#
# MDTC -> Total cloud fraction
# MDWC -> Water cloud fraction
# MDIC -> Ice cloud fraction
# MDHC -> High cloud fraction
# MDMC -> Middle cloud fraction
# MDLC -> Low cloud fraction
# MDTT -> Linear average cloud optical thickness (all clouds)
# MDWT -> Linear average cloud optical thickness (water clouds)
# MDIT -> Linear average cloud optical thickness (ice clouds)
# MDGT -> Log average cloud optical thickness (all clouds)
# MDGW -> Log average cloud optical thickness (water clouds)
# MDGI -> Log average cloud optical thickness (ice clouds)
# MDRE -> Liquid particle effective radius
# MDRI -> Ice particle effective radius
# MDPT -> Total cloud top pressure
# MDWP -> Liquid water path
# MDIP -> Ice water path
# MDTP -> Cloud top pressure versus cloud optical thickness histogram
#
# ----------------------------------------------------- Possible output fields
# ISCCP related fields
#
# ICCA -> Unadjusted total cloud albedo
# ICCF -> Total cloud fraction
# ICCT -> Unadjusted total cloud optical thickness (linear average)
# ICCP -> Unadjusted total cloud top pressure (cloud fraction weighted)
# SUNL -> Sunlit gridbox indicator
# ICTP -> Cloud frequency in ISCCP cloud top pressure/optical thickness bins
#
# CALIPSO related fields
#
# CLHC,CLHM -> High cloud fraction from lidar and occurrence count
# CLMC,CLMM -> Middle cloud fraction from lidar and occurrence count
# CLLC,CLLM -> Low cloud fraction from lidar and occurrence count
# CLTC,CLTM -> Total cloud fraction from lidar and occurrence count
# CLCP,CLCM -> Cloud fraction profile from lidar and occurrence count
# CCCP,CCCM -> Cloud fraction detected by lidar but not radar
# SR*       -> Backscattering ratio/height histogram
#
# PARASOL related fields
#
# PL01,ML01 -> PARASOL relfectance at 0 degree off nadir
# PL02,ML02 -> PARASOL relfectance at 15 degree off nadir
# PL03,ML03 -> PARASOL relfectance at 30 degree off nadir
# PL04,ML04 -> PARASOL relfectance at 45 degree off nadir
# PL05,ML05 -> PARASOL relfectance at 60 degree off nadir
#
# CloudSat related fields
#
# ZE*  -> Radar reflectivity/height histogram
# CHMK -> Mask of land topography (needed when interpolating data to height grid)
#
# CERES-like fields
#
# CCCF -> Cloud fraction (liquid + ice)
# CCNT -> Cloud occurence (liquid + ice)
# CCTP -> Cloud top pressure 
# CCCT -> Cloud visible optical thickness (linear average)
# CCLT -> Cloud visible optical thickness (log average)
# CCFL -> Liquid cloud fraction
# CCFI -> Ice cloud fraction
# CLWP -> Cloud liquid water path in each pressure range
# CIWP -> Cloud ice water path in each pressure range
# CNLC -> Occurence of liquid cloud in each pressure range
# CNIC -> Occurence of ice cloud in each pressure range
# CREL -> Cloud top effective radius for liquid clouds
# CCDD -> Cloud droplet number concentrations for liquid clouds
#
# CERES-like cloud fields along orbit
#
# CS01 -> Cloud fraction (liquid + ice)
# CS02 -> Cloud occurence (liquid + ice)
# CS03 -> Cloud top pressure 
# CS04 -> Cloud visible optical thickness (linear average)
# CS05 -> Cloud visible optical thickness (log average)
# CS06 -> Liquid cloud fraction
# CS07 -> Ice cloud fraction
# CS08 -> Cloud liquid water path in each pressure range
# CS09 -> Cloud ice water path in each pressure range
# CS10 -> Occurence of liquid cloud in each pressure range
# CS11 -> Occurence of ice cloud in each pressure range
# CS12 -> Cloud top effective radius for liquid clouds
# CS13 -> Cloud droplet number concentrations for liquid clouds
# CS14 -> Liquid cloud fraction for microphysical parameters
# CS15 -> Occurence of liquid cloud microphysical parameters in each pressure range
#
# CERES radiative fluxes by cloud type along orbit
#
# ORBA -> Sum of occurrences of orbit in gridboxes (total orbit)
# ORBS -> Sum of occurrences of orbit in gridboxes (sunlit orbit)
# PWTC -> Precipitable water
# GTC  -> Surface temperature
# GCC  -> Surface type
# CSZC -> Cosine of solar zenith angle
# CDSW -> Upward solar flux at TOA for each cloud type
# CULW -> Upward thermal flux at TOA for each cloud type
# CFSO -> Downward solar flux at the TOA (for each cloud type for convience)
# CLRF -> Occurrence of clear sky
#
# MISR related fields
#
# MSTC -> Total cloud fraction from MISR
# MSMZ -> Cloud-mean cloud top height
# MSDT -> Occurence of model layers in particular MISR height ranges
# MSTZ -> Cloud top height versus optical thickness 
#      -> This is saved as MST1 and MST2 to avoid problems with statsav
#
# MODIS related fields
#
# MDTC -> Total cloud fraction
# MDWC -> Water cloud fraction
# MDIC -> Ice cloud fraction
# MDHC -> High cloud fraction
# MDMC -> Middle cloud fraction
# MDLC -> Low cloud fraction
# MDTT -> Linear average cloud optical thickness (all clouds)
# MDWT -> Linear average cloud optical thickness (water clouds)
# MDIT -> Linear average cloud optical thickness (ice clouds)
# MDGT -> Log average cloud optical thickness (all clouds)
# MDGW -> Log average cloud optical thickness (water clouds)
# MDGI -> Log average cloud optical thickness (ice clouds)
# MDRE -> Liquid particle effective radius
# MDRI -> Ice particle effective radius
# MDPT -> Total cloud top pressure
# MDWP -> Liquid water path
# MDIP -> Ice water path
# MDTP -> Cloud top pressure versus cloud optical thickness histogram
#
# ---------------------------------- Notes
# There are two rather important issues to take note of for this deck
#
# 1. It only processes variables that exist in the "gs" file.  Basically, the error
#    checking is turned off, we attempt to read in the variable.
#    Therefore if the variable is not present,
#    OR the read does not work properly, the script continues onward without the variable.
#    Therefore, if you expected a particular field in the "gp" file and it 
#    is not there check first if it is present in the "gs" file and the correct
#    options were set in the GCM submission job.
#
# 2. Most of the variables need to be weighted to get the proper monthly, seasonal, etc.
#    means.  Since the pooling deck have trouble with missing values the next step is
#    to average all of the weighted mean denominators and numerators.  Potentially adds
#    a lot of extra output to the diagnostic files but is necessary to get the proper 
#    means at averaging times greater than a month.
# 
# 3. When averaging the profile data, and when it is interpolated onto the special height grid, 
#    you will need to account for the points that wind up below the height of the land.
#
# Manipulations needed to get the proper weighted means using the fields from the diagnostic
# output
#
# ICCA = ICCA/ICCF (cloud fraction weighted)
# ICCT = ICCT/ICCF (cloud fraction weighted)
# ICCP = ICCP/ICCF (cloud fraction weighted)
# ICCF = ICCF/SUNL (weighed by "observation" occurrence, 
#                   i.e., solar zenith angle greater than 0.2)
# ICTP = ICTP/SUNL  (weighed by "observation" occurrence, 
#                   i.e., solar zenith angle greater than 0.2)
#
# CLHC = CLHC/CLHM (weighted by "valid observation", i.e., backscatter above threshold)
# CLMC = CLMC/CLMM (weighted by "valid observation", i.e., backscatter above threshold)
# CLLC = CLLC/CLLM (weighted by "valid observation", i.e., backscatter above threshold)
# CLTC = CLTC/CLTM (weighted by "valid observation", i.e., backscatter above threshold)
# CLCP = CLCP/CLCM (weighted by "valid observation", i.e., backscatter above threshold)
# CCCP = CCCP/CCCM (weighted by "valid observation", i.e., backscatter above threshold)
#
# PL01 = PL01/ML01 (weighted by valid surface type, i.e., not over land)
# PL02 = PL02/ML02 (weighted by valid surface type, i.e., not over land)
# PL03 = PL03/ML03 (weighted by valid surface type, i.e., not over land)
# PL04 = PL04/ML04 (weighted by valid surface type, i.e., not over land)
# PL05 = PL05/ML05 (weighted by valid surface type, i.e., not over land)
#
# The topography mask (CHMK) will be needed when averaging the profiles over regions, 
# i.e., zonally, when the profile has been interpolated onto the heights above mean
# sea level.
#
# For the CERES-like fields we assume that things are properly weighted, i.e., they use
# cloud fraction weighting, as needed.
#
# CCCF = CCCF/CCNT
# CCFL = CCFL/CNLC
# CCFI = CCFI/CNIC
# CCTP = CCTP/CCCF
# CCCT = CCCT/CCCF
# CCLT = CCLT/CCCF
# CLWP = CLWP/CCFL
# CIWP = CIWP/CCFI
# CREL = CREL/CCFM
# CCDD = CCDD/CCFM
# CCFM = CCFM/CNLM
#
# For MISR we need
#
# MSTC = MSTC/SUNL
# MSMZ = MSMZ/MSTC
# MSTZ = (Join up MST1 and MST2) and then MSTZ/SUNL
# MSDT = MSDT/SUNL
#
# For MODIS we need
#
# MDTC = MDTC/SUNL
# MDWC = MDWC/SUNL
# MDIC = MDIC/SUNL
# MDHC = MDHC/SUNL
# MDMC = MDMC/SUNL
# MDLC = MDLC/SUNL
# MDTT = MDTT/MDTC
# MDWT = MDWT/MDWC
# MDIT = MDIT/MDIC
# MDGT = MDGT/MDTC
# MDGW = MDGW/MDWC
# MDGI = MDGI/MDIC
# MDRE = MDRE/MDWC
# MDRI = MDRI/MDIC
# MDPT = MDPT/MDTC
# MDWP = MDWP/MDWC
# MDIP = MDIP/MDIC
# MDTP = MDTP/SUNL
#
# Variables to hold fields for output
      vars=""

# ----------------------------------- Select all of the ISCCP related fields at once
    varnames="icca icct iccf iccp ictp sunl"

    V=`fmtselname ${varnames}`
    echo "C*SELECT   STEP         0 999999999    1     -900199999 NAME${V}" | ccc select npakgg ${varnames} || true

    for ivar in ${varnames} ; do
      if [ -s $ivar ] ; then
        vars="$vars $ivar"
      fi
    done


# I initially saved the histograms over cloud top pressure and optical thickness into separate 
# variables.  Try to select these here to support old implementations, i.e., that for CMIP5.
# If they are not present then this section will do nothing.

    varnames="it01 it02 it03 it04 it05 it06 it07 it08 it09 it10 it11 it12 it13 it14"
    varnames="$varnames it15 it16 it17 it18 it19 it20 it21 it22 it23 it24 it25 it26 it27 it28"
    varnames="$varnames it29 it30 it31 it32 it33 it34 it35 it36 it37 it38 it39 it40 it41 it42"
    varnames="$varnames it43 it44 it45 it46 it47 it48 it49"

    V=`fmtselname ${varnames}`
    echo "C*SELECT   STEP         0 999999999    1     -900199999 NAME${V}" | ccc select npakgg ${varnames} || true

    for ivar in ${varnames} ; do
      if [ -s $ivar ] ; then
        vars="$vars $ivar"
      fi
    done



# ----------------------------------- Select CALIPSO and CloudSat cloud fraction fields at once

    varnames="clhc clhm clmc clmm cllc cllm cltc cltm cctm clcp clcm cccp cccm"

    V=`fmtselname ${varnames}`
    echo "C*SELECT   STEP         0 999999999    1     -900199999 NAME${V}" | ccc select npakgg ${varnames} || true

    for ivar in ${varnames} ; do
      if [ -s $ivar ] ; then
        vars="$vars $ivar"
      fi
    done


# ----------------------------------- Backscatter/height histogram for CALIPSO
    varnames="sr01 sr02 sr03 sr04 sr05 sr06 sr07 sr08 sr09 sr10 sr11 sr12 sr13 sr14 sr15"

    V=`fmtselname ${varnames}`
    echo "C*SELECT   STEP         0 999999999    1     -900199999 NAME${V}" | ccc select npakgg ${varnames} || true

    for ivar in ${varnames} ; do
      if [ -s $ivar ] ; then
        vars="$vars $ivar"
      fi
    done


# ----------------------------------- Reflectances for PARASOL
    varnames="pl01 pl02 pl03 pl04 pl05 ml01 ml02 ml03 ml04 ml05"

    V=`fmtselname ${varnames}`
    echo "C*SELECT   STEP         0 999999999    1     -900199999 NAME${V}" | ccc select npakgg ${varnames} || true

    for ivar in ${varnames} ; do
      if [ -s $ivar ] ; then
        vars="$vars $ivar"
      fi
    done


# ----------------------------------- Radar reflectivity/height histogram for CloudSat
    varnames="ze01 ze02 ze03 ze04 ze05 ze06 ze07 ze08 ze09 ze10 ze11 ze12 ze13 ze14 ze15"

    V=`fmtselname ${varnames}`
    echo "C*SELECT   STEP         0 999999999    1     -900199999 NAME${V}" | ccc select npakgg ${varnames} || true

    for ivar in ${varnames} ; do
      if [ -s $ivar ] ; then
        vars="$vars $ivar"
      fi
    done


# ----------------------------------- Select all of the CERES related fields at once

    varnames="cccf cctp ccct cclt ccnt clwp ciwp ccfl ccfi cnlc cnic crel ccdd ccfm cnlm"

    V=`fmtselname ${varnames}`
    echo "C*SELECT   STEP         0 999999999    1     -900199999 NAME${V}" | ccc select npakgg ${varnames} || true

    for ivar in ${varnames} ; do
      if [ -s $ivar ] ; then
        vars="$vars $ivar"
      fi
    done

  
# ----------------------------------- Select all of the CERES related fields along the swath at once

    varnames="cs01 cs02 cs03 cs04 cs05 cs06 cs07 cs08 cs09 cs10 cs11 cs12 cs13 cs14 cs15"
    varnames="$varnames orba orbs pwtc gtc gcc cszc cdsw culw cfso clrf"

    V=`fmtselname ${varnames}`
    echo "C*SELECT   STEP         0 999999999    1     -900199999 NAME${V}" | ccc select npakgg ${varnames} || true

    for ivar in ${varnames} ; do
      if [ -s $ivar ] ; then
        vars="$vars $ivar"
      fi
    done


# ----------------------------------- Select all of the MISR related fields at once
    varnames="mstc msmz msdt"

    V=`fmtselname ${varnames}`
    echo "C*SELECT   STEP         0 999999999    1     -900199999 NAME${V}" | ccc select npakgg ${varnames} || true

    for ivar in ${varnames} ; do
      if [ -s $ivar ] ; then
        vars="$vars $ivar"
      fi
    done

# Special case for MSTZ
    echo "C*SELECT   STEP         0 999999999    1     -9001   99 NAME MSTZ" | ccc select npakgg mst1
    echo "C*SELECT   STEP         0 999999999    1       10099999 NAME MSTZ" | ccc select npakgg mst2

    for ivar in "mst1" "mst2" ; do
      if [ -s $ivar ] ; then
        vars="$vars $ivar"
      fi
    done


# ----------------------------------- Select all of the MODIS related fields at once
    varnames="mdtc mdwc mdic mdhc mdmc mdlc mdtt mdwt mdit mdgt mdgw mdgi"
    varnames="$varnames mdre mdri mdpt mdwp mdip mdtp"

    V=`fmtselname ${varnames}`
    echo "C*SELECT   STEP         0 999999999    1     -900199999 NAME${V}" | ccc select npakgg ${varnames} || true

    for ivar in ${varnames} ; do
      if [ -s $ivar ] ; then
        vars="$vars $ivar"
      fi
    done


# ----------------------------------- Select misc fields used by COSP but not computed in COSP
    varnames="chmk smix sref rmix rref"

    V=`fmtselname ${varnames}`
    echo "C*SELECT   STEP         0 999999999    1     -900199999 NAME${V}" | ccc select npakgg ${varnames} || true

    for ivar in ${varnames} ; do
      if [ -s $ivar ] ; then
        vars="$vars $ivar"
      fi
    done


    echo $vars

# Only try to save the variables if there is something to save, i.e., $vars is not empty

      if [ -n "$vars" ]; then
# ----------------------------------- Compute the statistics and save the fields to a file

        statsav ${vars} new_gp new_xp off
        rm ${vars}

#  ---------------------------------- save results.

        access oldgp ${flabel}gp na
        xjoin oldgp new_gp newgp
        save newgp ${flabel}gp na
        delete oldgp na
        
#    if [ "$rcm" != "on" ] ; then
#      access oldxp ${flabel}xp
#      xjoin oldxp new_xp newxp
#      save newxp ${flabel}xp
#      delete oldxp
#    fi
      fi
end_of_script

cat > Input_Cards <<end_of_data
+   .    :    .    :    .    :    .    :    .    :    .    :    .    :    .    :
         DAYS      = $days
         RUN       = $run
         FLABEL    = $flabel
         T1        = $t1
         T2        = $t2
         T3        =      $t3
0
0 COSP3 ------------------------------------------------------------------ COSP3
end_of_data

. endjcl.cdk
