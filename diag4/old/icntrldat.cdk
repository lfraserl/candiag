#comdeck icntrldat
# Adjust for "gcm6u" case by setting if necessary "ilev/lonsl/nlat/itraca" to 
# "slv/lon/lat/itrac" value, respectively.
# Hard-code "ihyd" reference to 2, ktr to '    2',substitute "lmt" for "lrt".
# Increase support up to 200 levels.
#                 icntrldat            FM,ML - Dec 09/15 - FM ---------icntrldat

#  ----------------------------------- In the case of running "gcm6u", adjust
#                                      for the change in parameter names used
#                                      to setup the input files further below.

if [ "$inphys" = inphys6u -o "$inphys" = inphys6 ] ; then
 echo " icntrldat - Identified gcm6u run; check if any of ilev,lonsl,nlat,itraca needs to be set"
 if [ ! -n "$ilev" -a -n "$slv" ] ; then
  ilev="$slv"
  echo " slv(=$slv) is used to set ilev"
 fi
 if [ ! -n "$lonsl" -a -n "$lon" ] ; then
  lonsl="$lon"
  echo " lon(=$lon) is used to set lonsl"
 fi
 if [ ! -n "$nlat" -a -n "$lat" ] ; then
  nlat="$lat"
  echo " lat(=$lat) is used to set nlat"
 fi
 if [ ! -n "$itraca" -a -n "$itrac" ] ; then
  itraca="$itrac"
  echo " itrac(=$itrac) is used to set itraca"
 fi
fi

#  ----------------------------------- Generate "icntrl.dat" file used in the
#                                      initialization.

# Abort if any of "lonsl/nlat/ilev" is not defined:

if [ -z "$lonsl" -a -z "$nlat" -a -z "$ilev" ] ; then
 
   echo "\n Sorry: lonsl, nlat and ilev variables must be defined !" 
   echo "\n Sorry: lonsl, nlat and ilev variables must be defined !" >> haltit
   exit 1

elif [ -z "$lonsl" -a -z "$nlat" ] ; then
 
   echo "\n Sorry: lonsl and nlat variables must be defined !" 
   echo "\n Sorry: lonsl and nlat variables must be defined !" >> haltit
   exit 1

elif [ -z "$lonsl" ] ; then

   echo "\n Sorry: lonsl variable must be defined !" 
   echo "\n Sorry: lonsl variable must be defined !" >> haltit
   exit 1

elif [ -z "$nlat" ] ; then

   echo "\n Sorry: nlat variable must be defined !" 
   echo "\n Sorry: nlat variable must be defined !" >> haltit
   exit 1

elif [ -z "$ilev" ] ; then

   echo "\n Sorry: ilev variable must be defined !" 
   echo "\n Sorry: ilev variable must be defined !" >> haltit
   exit 1

fi
  
Ilev=`echo $ilev | sed -n -e '/^ *[0-9]*$/p'`

if [ -z "$Ilev" ] ; then
    echo "\n Sorry: ilev variable (=$ilev) must have an appropriate integer value!"
    echo "\n Sorry: ilev variable (=$ilev) must have an appropriate integer value!" >> haltit
    exit 2
fi

# Ensure "ilev" <=200 (the current maximum).

if [ $Ilev -gt 200 ] ; then

   echo "\n Sorry: ilev variable (=$ilev) must be < 200 (current maximum) !"
   echo "\n Sorry: ilev variable (=$ilev) must be < 200 (current maximum) !" >> haltit
   exit 3

fi

(cat <<end_of_data ) > icntrl.dat
icntrl    $ilev$lonsl$nlat$lmt$lmt    2$iday
$lay$gmt$coord$plid$moist    2$sref$spow
end_of_data

# Figure out how many lines are needed for the level
# values (minimum of 5 for upward compatability).

Nlines=`expr \( $Ilev + 9 \) / 10 ` 
Nlines=${Nlines:=5}

if [ $Nlines -lt 5 ] ; then
   Nlines=5
fi

# Ensure all not needed variables are set to blanks:

if [ $Ilev -le 200 ] ; then
  Icntr=200
  while [ $Icntr -gt $Ilev ] 
   do
     if [ $Icntr -lt 10 ] ; then
       eval "g0${Icntr}='     ' ; h0${Icntr}='     '"
     else
       eval "g${Icntr}='     ' ; h${Icntr}='     '"
     fi
     Icntr=`expr $Icntr - 1`
   done
fi

# Append the level values to "icntrl.dat"
# based on the appropriate parameter values 
# specified.

# "g-levels":

(cat  << end_of_data ) | head -$Nlines >> icntrl.dat
$g01$g02$g03$g04$g05$g06$g07$g08$g09$g10
$g11$g12$g13$g14$g15$g16$g17$g18$g19$g20
$g21$g22$g23$g24$g25$g26$g27$g28$g29$g30
$g31$g32$g33$g34$g35$g36$g37$g38$g39$g40
$g41$g42$g43$g44$g45$g46$g47$g48$g49$g50
$g51$g52$g53$g54$g55$g56$g57$g58$g59$g60
$g61$g62$g63$g64$g65$g66$g67$g68$g69$g70
$g71$g72$g73$g74$g75$g76$g77$g78$g79$g80
$g81$g82$g83$g84$g85$g86$g87$g88$g89$g90
$g91$g92$g93$g94$g95$g96$g97$g98$g99$g100
$g101$g102$g103$g104$g105$g106$g107$g108$g109$g110
$g111$g112$g113$g114$g115$g116$g117$g118$g119$g120
$g121$g122$g123$g124$g125$g126$g127$g128$g129$g130
$g131$g132$g133$g134$g135$g136$g137$g138$g139$g140
$g141$g142$g143$g144$g145$g146$g147$g148$g149$g150
$g151$g152$g153$g154$g155$g156$g157$g158$g159$g160
$g161$g162$g163$g164$g165$g166$g167$g168$g169$g170
$g171$g172$g173$g174$g175$g176$g177$g178$g179$g180
$g181$g182$g183$g184$g185$g186$g187$g188$g189$g190
$g191$g192$g193$g194$g195$g196$g197$g198$g199$g200
end_of_data

# "h-levels":

(cat  << end_of_data ) | head -$Nlines >> icntrl.dat
$h01$h02$h03$h04$h05$h06$h07$h08$h09$h10
$h11$h12$h13$h14$h15$h16$h17$h18$h19$h20
$h21$h22$h23$h24$h25$h26$h27$h28$h29$h30
$h31$h32$h33$h34$h35$h36$h37$h38$h39$h40
$h41$h42$h43$h44$h45$h46$h47$h48$h49$h50
$h51$h52$h53$h54$h55$h56$h57$h58$h59$h60
$h61$h62$h63$h64$h65$h66$h67$h68$h69$h70
$h71$h72$h73$h74$h75$h76$h77$h78$h79$h80
$h81$h82$h83$h84$h85$h86$h87$h88$h89$h90
$h91$h92$h93$h94$h95$h96$h97$h98$h99$h100
$h101$h102$h103$h104$h105$h106$h107$h108$h109$h110
$h111$h112$h113$h114$h115$h116$h117$h118$h119$h120
$h121$h122$h123$h124$h125$h126$h127$h128$h129$h130
$h131$h132$h133$h134$h135$h136$h137$h138$h139$h140
$h141$h142$h143$h144$h145$h146$h147$h148$h149$h150
$h151$h152$h153$h154$h155$h156$h157$h158$h159$h160
$h161$h162$h163$h164$h165$h166$h167$h168$h169$h170
$h171$h172$h173$h174$h175$h176$h177$h178$h179$h180
$h181$h182$h183$h184$h185$h186$h187$h188$h189$h190
$h191$h192$h193$h194$h195$h196$h197$h198$h199$h200
end_of_data

# Reverse the setting of just the extra "g/h" variables beyond MAX(Ilev,50)...

Icntr=200 ; Ilevmb=$Ilev ; [ $Ilevmb -lt 50 ] && Ilevmb=50 || :
while [ $Icntr -gt $Ilevmb ]
 do
   if [ $Icntr -lt 10 ] ; then
     eval "unset g0${Icntr} h0${Icntr}"
   else
     eval "unset g${Icntr} h${Icntr}"
   fi
   Icntr=`expr $Icntr - 1`
 done

unset Nlines Ilev Icntr Ilevmb
