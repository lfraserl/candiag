#deck plotsfc2
jobname=plsf2 ; time=$stime ; memory=$memory1
. comjcl.cdk

cat > Execute_Script <<end_of_script

#   modify xfinds on lva  lt"2  and lq"2 to give warning exits if missing.
#                   plotsfc2            gjb. jul 25/90 - ec.
#   ----------------------------------  lowest model level plots.
#                                       note  the mean amplitude of the wind
#                                        lva  may be either at the lowest
#                                       model level or at screen level
#                                        see note in sscreen .
# 
#
    access gpfile ${flabel}gp
    access gbfile ${flabel}gb na
    joinup ggfile gpfile gbfile 
    rm gpfile gbfile
# 
    libncar plunit=$plunit 
.   plotid.cdk
# 
    xfind ggfile tu 
    xfind ggfile tv 
    xfind ggfile tva 
    xfind ggfile tt 
    xfind ggfile ttp2 
    xfind ggfile tq 
    xfind ggfile tqp2 
    rm ggfile
#   ----------------------------------- calculate the amplitude of the mean
#                                       wind.
    square tu tu2 
    square tv tv2 
    add tu2 tv2 amp2 
    sqroot amp2 amp 
    rm tu2 tv2 amp2
# 
#   ----------------------------------- generate plots.
    ggplot amp tu tv 
    ggplot tt 
    ggplot tq 
    rm amp
    if [ "$pooled" != on ] ; then
      sqroot ttp2 stdt 
      sqroot tqp2 stdq 
      ggplot tva 
      ggplot stdt 
      ggplot stdq 
      rm stdt stdq
    fi
# 
#   ----------------------------------  zonally average the fields.
    zonavg tu ztu 
    zonavg tv ztv 
    zonavg tt ztt 
    zonavg tq ztq 
    rm tu tv tt tq
    if [ "$pooled" != on ] ; then
      zonavg tva ztva 
      zonavg ttp2 zttp2 
      zonavg tqp2 ztqp2 
      sqroot zttp2 stdzt 
      sqroot ztqp2 stdzq 
      rm tva ttp2 tqp2 zttp2 ztqp2
    fi
# 
#   ----------------------------------  now xplots.
    xplot ztu 
    xplot ztv 
    xplot ztt 
    xplot ztq 
    if [ "$pooled" != on ] ; then
      xplot ztva 
      xplot stdzt 
      xplot stdzq 
    fi
.   plotid.cdk
.   plot.cdk

end_of_script

cat > Input_Cards <<end_of_data

+   .    :    .    :    .    :    .    :    .    :    .    :    .    :    .    :
         DAYS      = $days
         RUN       = $run
         FLABEL    = $flabel
0 PLOTSFC2 ------------------------------------------------------------ PLOTSFC2
. headfram.cdk
+   .    :    .    :    .    :    .    :    .    :    .    :    .    :    .    :
XFIND         LU
+   .    :    .    :    .    :    .    :    .    :    .    :    .    :    .    :
XFIND         LV
+   .    :    .    :    .    :    .    :    .    :    .    :    .    :    .    :
XFIND     1   LVA
+   .    :    .    :    .    :    .    :    .    :    .    :    .    :    .    :
XFIND         LT
+   .    :    .    :    .    :    .    :    .    :    .    :    .    :    .    :
XFIND     1   LT"2
+   .    :    .    :    .    :    .    :    .    :    .    :    .    :    .    :
XFIND         LQ
+   .    :    .    :    .    :    .    :    .    :    .    :    .    :    .    :
XFIND     1   LQ"2
+   .    :    .    :    .    :    .    :    .    :    .    :    .    :    .    :
 GGPLOT.          -1 NEXT   -1    0     1.0E0        0.     5.0E1     5.0E02${b}8
RUN $run. DAYS $days. AMPLITUDE OF MEAN WIND AT LOW LEV.  UNITS M/SEC.
                  1.        0.       10.$ncx$ncy
                      VECPLOT OF (U,V).                     UNITS M/SEC.
+   .    :    .    :    .    :    .    :    .    :    .    :    .    :    .    :
 GGPLOT.           0 NEXT    1    0     1.0E0    -1.0E2     5.0E1     5.0E00${b}8
  RUN $run.  DAYS $days.   LOWEST LEVEL AIR TEMP.  UNITS  DEG C.
+   .    :    .    :    .    :    .    :    .    :    .    :    .    :    .    :
 GGPLOT.           0 NEXT    1    0     1.0E3     5.0E0     5.0E1     5.0E00${b}8
  RUN $run.  DAYS $days.   LOWEST LEVEL Q.  UNITS GM/KG.
if [ "$pooled" != on ] ; then
+   .    :    .    :    .    :    .    :    .    :    .    :    .    :    .    :
 GGPLOT.           0 NEXT    1    0     1.0E0        0.     5.0E1     5.0E00${b}8
RUN $run. DAYS $days. AVERAGE AMPLITUDE OF WIND AT LOW LEV.  UNITS M/SEC.
+   .    :    .    :    .    :    .    :    .    :    .    :    .    :    .    :
 GGPLOT.           0 NEXT    1    0        1.        0.     5.0E1     2.0E00${b}8
  RUN $run.  DAYS $days.  LOWEST LEV AIR TEMP STD. DEV.  UNITS  DEGC.
+   .    :    .    :    .    :    .    :    .    :    .    :    .    :    .    :
 GGPLOT.           0 NEXT    1    0     1.0E3        0.     5.0E1     2.0E00${b}8
  RUN $run.  DAYS $days.   LOWEST LEVEL Q STD. DEV.      UNITS  GM/KG.
fi
+   .    :    .    :    .    :    .    :    .    :    .    :    .    :    .    :
XPLOT   LU         0 NEXT    1      -15.       15.    1      -15.
   RUN $run.  DAYS $days.  LOWEST LEVEL ZONAL WIND.  UNITS M/SEC.
+   .    :    .    :    .    :    .    :    .    :    .    :    .    :    .    :
XPLOT   LV         0 NEXT    1      -15.       15.    1      -15.
   RUN $run.  DAYS $days.  LOWEST LEVEL MERIDIONAL WIND.  UNITS M/SEC.
+   .    :    .    :    .    :    .    :    .    :    .    :    .    :    .    :
XPLOT   LT         0 NEXT    1      -75.       75.    1      -75.
   RUN $run.  DAYS $days.  LOWEST LEVEL AIR TEMPERATURE.  UNITS DEG C.
+   .    :    .    :    .    :    .    :    .    :    .    :    .    :    .    :
XPLOT   LQ         0 NEXT    1        0.    2.5E-2    1        0.
   RUN $run.  DAYS $days.  LOWEST LEVEL SPECIFIC HUMIDITY.  UNITS GM/GM.
if [ "$pooled" != on ] ; then
+   .    :    .    :    .    :    .    :    .    :    .    :    .    :    .    :
XPLOT   VA         0 NEXT    1        0.       30.    1        0.
   RUN $run.  DAYS $days.  AVERAGE AMP. OF WIND AT LOW LEV.  UNITS M/SEC.
+   .    :    .    :    .    :    .    :    .    :    .    :    .    :    .    :
XPLOT LT"2         0 NEXT    1        0.       50.    1        0.
   RUN $run.  DAYS $days.  LOWEST LEVEL AIR TEMP STD. DEV.  UNITS DEGC.
+   .    :    .    :    .    :    .    :    .    :    .    :    .    :    .    :
XPLOT LQ"2         0 NEXT    1        0.    5.0E-3    1        0.
   RUN $run.  DAYS $days.  LOWEST LEVEL Q STD. DEV.    UNITS GM/GM.
fi
. tailfram.cdk

end_of_data

. endjcl.cdk


