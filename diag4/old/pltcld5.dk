#deck pltcld5
jobname=pltcld5 ; time=$dtime ; memory=$memory1
. comjcl.cdk

cat > Execute_Script <<end_of_script

#                   pltcld5             ml - Apr 20/99.
#                                       plot model cloud statistics
#                                       for gcm12.
# 
#
#   if the "shade" option is on, then:
#     default shading is light background and dark clouds with shading card
#                6    0   48   51   56   64    3
#     if dark background and white clouds are desired  change shading card to
#                6    3   64   56   51   48    0
#
#   ----------------------------------- cloud information plots.
    libncar plunit=$plunit 
.   plotid.cdk
    access gg ${flabel}gp
    access zx ${flabel}xp
#   ----------------------------------- relative humidity cross-section.
    xfind zx rhr 
    zxplot rhr 
    rm rhr
#   ----------------------------------- cloudiness cross-section.
    xfind zx scld 
    zxplot scld 
#   ----------------------------------- optical depth cross-section.
    xfind zx tacr
    zxplot tacr
#   ----------------------------------- log10 water content cross-section.
    xfind gg clw
    loge clw loge_clw
    xlin loge_clw log10_clw
    zonavg log10_clw zlog10_clw
    zxplot zlog10_clw
    rm clw loge_clw log10_clw zlog10_clw
#   ----------------------------------- log10 ice content cross-section.
    xfind gg cic
    loge cic loge_cic
    xlin loge_cic log10_cic
    zonavg log10_cic zlog10_cic
    zxplot zlog10_cic
    rm cic loge_cic log10_cic zlog10_cic
#   ----------------------------------- land and ocean separately.
#                                       ocean =1  non-ocean =0.
    xfind gg gc 
    fmask gc mi 
    fmask gc ml 
    add ml mi mil 
    xlin mil mo 
    zonavg mo zmo 
    zonavg mil zmil 
    fpow zmil izmil 
    globavg mo gmo 
    rm mi ml gc mil zmil
#   ----------------------------------- total cloudiness.
    xfind gg tcld 
    zonavg tcld ztcld 
    mlt tcld mo tclda 
    rzonavg tclda mo ztcldo 
    gmlt ztcldo zmo a1 
    sub ztcld a1 b1 
    gmlt b1 izmil ztcldl 
    rm a1 b1
    div tclda gmo tcldo 
    rm tclda
# 
    globavg tcld  output=dummy 
    globavg tcldo output=dummy 
    txtplot input=dummy 
    rm tcldo
# 
    ggplot tcld 
    joinup zcldj ztcldo ztcldl ztcld 
    xmplot zcldj 
    rm zcldj ztcldo ztcldl ztcld
#   ----------------------------------- total liquid water path.
    xfind gg lwct 
    zonavg lwct zlwct 
    mlt lwct mo lwcta 
    rzonavg lwcta mo zlwcto 
    gmlt zlwcto zmo a1 
    sub zlwct a1 b1 
    gmlt b1 izmil zlwctl 
    rm a1 b1
    div lwcta gmo lwcto 
    rm lwcta
# 
    rm dummy
    globavg lwct  output=dummy 
    globavg lwcto output=dummy 
    txtplot input=dummy 
    rm lwcto
# 
    ggplot lwct 
    joinup zlwcj zlwcto zlwctl zlwct 
    xmplot zlwcj 
    rm zlwcj zlwcto zlwctl zlwct
#   ----------------------------------- total ice path.
    xfind gg cict 
    zonavg cict zcict 
    mlt cict mo cicta 
    rzonavg cicta mo zcicto 
    gmlt zcicto zmo a1 
    sub zcict a1 b1 
    gmlt b1 izmil zcictl 
    rm a1 b1
    div cicta gmo cicto 
    rm cicta
# 
    rm dummy
    globavg cict  output=dummy 
    globavg cicto output=dummy 
    txtplot input=dummy 
    rm cicto
# 
    ggplot cict 
    joinup zcicj zcicto zcictl zcict 
    xmplot zcicj 
    rm zcicj zcicto zcictl zcict
#   ------------------------------------ level plots.
    if [ "$plotlev" = on ] ; then
      xfind gg cld 
      ggplot cld 
    if [ "$xtracld" = on ] ; then
      xfind gg tac 
      div tac cld ltac 
      ggplot ltac 
      rm cld tac ltac
    fi
    fi
.   plotid.cdk
.   plot.cdk

end_of_script

cat > Input_Cards <<end_of_data

+   .    :    .    :    .    :    .    :    .    :    .    :    .    :    .    :
         DAYS      = $days
         RUN       = $run
         FLABEL    = $flabel
0 PLTCLD4 ----------------------------------------------------- PLTCLD4
. headfram.cdk
+   .    :    .    :    .    :    .    :    .    :    .    :    .    :    .    :
  XFIND.      (RHC)R
+   .    :    .    :    .    :    .    :    .    :    .    :    .    :    .    :
ZXPLOT.    NEXT    0    0   -1     1.0E2     -200.      100.     1.0E1    0$kin
  RUN $run.  DAYS $days.  MODEL RELATIVE HUMIDITY (RH)R.  UNITS PERCENTAGE.
                 90.      -90.     $m01     1000.    0
+   .    :    .    :    .    :    .    :    .    :    .    :    .    :    .    :
  XFIND.      (SCLD)R
+   .    :    .    :    .    :    .    :    .    :    .    :    .    :    .    :
ZXPLOT.    NEXT    0    0   -1        1.        0.        1.     1.E-1    0$kin
  RUN $run.  DAYS $days.  SIGMA CLOUDINESS (SCLD)R.
                 90.      -90.     $m01     1000.    0
+   .    :    .    :    .    :    .    :    .    :    .    :    .    :    .    :
  XFIND.      (TACN)R
+   .    :    .    :    .    :    .    :    .    :    .    :    .    :    .    :
ZXPLOT.    NEXT    0    0   -1        1.        0.      100.      2.E0    0$kin
  RUN $run.  DAYS $days.  CLOUD OPTICAL DEPTH (TACN)R.
                 90.      -90.     $m01     1000.    0
+   .    :    .    :    .    :    .    :    .    :    .    :    .    :    .    :
  XFIND.      CLW
+   .    :    .    :    .    :    .    :    .    :    .    :    .    :    .    :
  XLIN.       0.4343
+   .    :    .    :    .    :    .    :    .    :    .    :    .    :    .    :
ZXPLOT.    NEXT    0    0   -1        1.      -10.       10.   0.33333    0$kin
  RUN $run.  DAYS $days.  LOG10(CLOUDWATERCONTENT) 
                 90.      -90.     $m01     1000.    0
+   .    :    .    :    .    :    .    :    .    :    .    :    .    :    .    :
  XFIND.      CIC
+   .    :    .    :    .    :    .    :    .    :    .    :    .    :    .    :
  XLIN.       0.4343
+   .    :    .    :    .    :    .    :    .    :    .    :    .    :    .    :
ZXPLOT.    NEXT    0    0   -1        1.      -10.       10.   0.33333    0$kin
  RUN $run.  DAYS $days.  LOG10(CLOUDICECONTENT) 
                 90.      -90.     $m01     1000.    0
+   .    :    .    :    .    :    .    :    .    :    .    :    .    :    .    :
  XFIND.      GC
+   .    :    .    :    .    :    .    :    .    :    .    :    .    :    .    :
  FMASK.             NEXT   GT       0.5
+   .    :    .    :    .    :    .    :    .    :    .    :    .    :    .    :
  FMASK.             NEXT   LT      -0.5
+   .    :    .    :    .    :    .    :    .    :    .    :    .    :    .    :
  XLIN.          -1.        1.
+   .    :    .    :    .    :    .    :    .    :    .    :    .    :    .    :
  FPOW.          -1.
+   .    :    .    :    .    :    .    :    .    :    .    :    .    :    .    :
  XFIND.      CLDT
+   .    :    .    :    .    :    .    :    .    :    .    :    .    :    .    :
GGPLOT            -1 CLDT   -1    0        1.        0.        1.     2.E-10${b}8
    RUN $run.  DAYS $days.    TOTAL CLOUDINESS.
+   .    :    .    :    .    :    .    :    .    :    .    :    .    :    .    :
XMPLOT.            0 NEXT    1        0.        1.    1    3
   RUN $run.  DAYS $days.  OCEAN,LAND/ICE,TOTAL CLOUDINESS.
+   .    :    .    :    .    :    .    :    .    :    .    :    .    :    .    :
  XFIND.      CLWT
+   .    :    .    :    .    :    .    :    .    :    .    :    .    :    .    :
GGPLOT            -1 CLWT   -1    0     1000.        0.      700.       50.0${b}8
    RUN $run.  DAYS $days.    TOTAL LIQUID WATER PATH.  UNITS G/M2.
+   .    :    .    :    .    :    .    :    .    :    .    :    .    :    .    :
XMPLOT.            0 NEXT    1        0.       0.2    1    3
   RUN $run. DAYS $days. OCEAN,LAND/ICE,TOTAL LIQUID WATER PATH. UNITS KG/M2.
+   .    :    .    :    .    :    .    :    .    :    .    :    .    :    .    :
  XFIND.      CICT
+   .    :    .    :    .    :    .    :    .    :    .    :    .    :    .    :
GGPLOT            -1 CICT   -1    0     1000.        0.      700.       50.0${b}8
    RUN $run.  DAYS $days.    TOTAL ICE PATH.  UNITS G/M2.
+   .    :    .    :    .    :    .    :    .    :    .    :    .    :    .    :
XMPLOT.            0 NEXT    1        0.       0.2    1    3
   RUN $run. DAYS $days. OCEAN,LAND/ICE,TOTAL ICE PATH. UNITS KG/M2.
if [ "$plotlev" = on ] ; then
+   .    :    .    :    .    :    .    :    .    :    .    :    .    :    .    :
  XFIND.      SCLD
if [ "$shade" != on ] ; then
+   .    :    .    :    .    :    .    :    .    :    .    :    .    :    .    :
GGPLOT            -1 SCLD   -1    0        1.        0.        1.     1.E-10${b}8
    RUN $run.  DAYS $days.    CLOUDINESS IN LAYER-01.
GGPLOT            -1 SCLD   -1    0        1.        0.        1.     1.E-10${b}8
    RUN $run.  DAYS $days.    CLOUDINESS IN LAYER-02.
GGPLOT            -1 SCLD   -1    0        1.        0.        1.     1.E-10${b}8
    RUN $run.  DAYS $days.    CLOUDINESS IN LAYER-03.
GGPLOT            -1 SCLD   -1    0        1.        0.        1.     1.E-10${b}8
    RUN $run.  DAYS $days.    CLOUDINESS IN LAYER-04.
GGPLOT            -1 SCLD   -1    0        1.        0.        1.     1.E-10${b}8
    RUN $run.  DAYS $days.    CLOUDINESS IN LAYER-05.
GGPLOT            -1 SCLD   -1    0        1.        0.        1.     1.E-10${b}8
    RUN $run.  DAYS $days.    CLOUDINESS IN LAYER-06.
GGPLOT            -1 SCLD   -1    0        1.        0.        1.     1.E-10${b}8
    RUN $run.  DAYS $days.    CLOUDINESS IN LAYER-07.
GGPLOT            -1 SCLD   -1    0        1.        0.        1.     1.E-10${b}8
    RUN $run.  DAYS $days.    CLOUDINESS IN LAYER-08.
GGPLOT            -1 SCLD   -1    0        1.        0.        1.     1.E-10${b}8
    RUN $run.  DAYS $days.    CLOUDINESS IN LAYER-09.
GGPLOT            -1 SCLD   -1    0        1.        0.        1.     1.E-10${b}8
    RUN $run.  DAYS $days.    CLOUDINESS IN LAYER-10.
GGPLOT            -1 SCLD   -1    0        1.        0.        1.     1.E-10${b}8
    RUN $run.  DAYS $days.    CLOUDINESS IN LAYER-11.
GGPLOT            -1 SCLD   -1    0        1.        0.        1.     1.E-10${b}8
    RUN $run.  DAYS $days.    CLOUDINESS IN LAYER-12.
GGPLOT            -1 SCLD   -1    0        1.        0.        1.     1.E-10${b}8
    RUN $run.  DAYS $days.    CLOUDINESS IN LAYER-13.
GGPLOT            -1 SCLD   -1    0        1.        0.        1.     1.E-10${b}8
    RUN $run.  DAYS $days.    CLOUDINESS IN LAYER-14.
GGPLOT            -1 SCLD   -1    0        1.        0.        1.     1.E-10${b}8
    RUN $run.  DAYS $days.    CLOUDINESS IN LAYER-15.
GGPLOT            -1 SCLD   -1    0        1.        0.        1.     1.E-10${b}8
    RUN $run.  DAYS $days.    CLOUDINESS IN LAYER-16.
GGPLOT            -1 SCLD   -1    0        1.        0.        1.     1.E-10${b}8
    RUN $run.  DAYS $days.    CLOUDINESS IN LAYER-17.
GGPLOT            -1 SCLD   -1    0        1.        0.        1.     1.E-10${b}8
    RUN $run.  DAYS $days.    CLOUDINESS IN LAYER-18.
GGPLOT            -1 SCLD   -1    0        1.        0.        1.     1.E-10${b}8
    RUN $run.  DAYS $days.    CLOUDINESS IN LAYER-19.
GGPLOT            -1 SCLD   -1    0        1.        0.        1.     1.E-10${b}8
    RUN $run.  DAYS $days.    CLOUDINESS IN LAYER-20.
GGPLOT            -1 SCLD   -1    0        1.        0.        1.     1.E-10${b}8
    RUN $run.  DAYS $days.    CLOUDINESS IN LAYER-21.
GGPLOT            -1 SCLD   -1    0        1.        0.        1.     1.E-10${b}8
    RUN $run.  DAYS $days.    CLOUDINESS IN LAYER-22.
GGPLOT            -1 SCLD   -1    0        1.        0.        1.     1.E-10${b}8
    RUN $run.  DAYS $days.    CLOUDINESS IN LAYER-23.
GGPLOT            -1 SCLD   -1    0        1.        0.        1.     1.E-10${b}8
    RUN $run.  DAYS $days.    CLOUDINESS IN LAYER-24.
GGPLOT            -1 SCLD   -1    0        1.        0.        1.     1.E-10${b}8
    RUN $run.  DAYS $days.    CLOUDINESS IN LAYER-25.
GGPLOT            -1 SCLD   -1    0        1.        0.        1.     1.E-10${b}8
    RUN $run.  DAYS $days.    CLOUDINESS IN LAYER-26.
GGPLOT            -1 SCLD   -1    0        1.        0.        1.     1.E-10${b}8
    RUN $run.  DAYS $days.    CLOUDINESS IN LAYER-27.
GGPLOT            -1 SCLD   -1    0        1.        0.        1.     1.E-10${b}8
    RUN $run.  DAYS $days.    CLOUDINESS IN LAYER-28.
GGPLOT            -1 SCLD   -1    0        1.        0.        1.     1.E-10${b}8
    RUN $run.  DAYS $days.    CLOUDINESS IN LAYER-29.
GGPLOT            -1 SCLD   -1    0        1.        0.        1.     1.E-10${b}8
    RUN $run.  DAYS $days.    CLOUDINESS IN LAYER-30.
GGPLOT            -1 SCLD   -1    0        1.        0.        1.     1.E-10${b}8
    RUN $run.  DAYS $days.    CLOUDINESS IN LAYER-31.
GGPLOT            -1 SCLD   -1    0        1.        0.        1.     1.E-10${b}8
    RUN $run.  DAYS $days.    CLOUDINESS IN LAYER-32.
GGPLOT            -1 SCLD   -1    0        1.        0.        1.     1.E-10${b}8
    RUN $run.  DAYS $days.    CLOUDINESS IN LAYER-33.
GGPLOT            -1 SCLD   -1    0        1.        0.        1.     1.E-10${b}8
    RUN $run.  DAYS $days.    CLOUDINESS IN LAYER-34.
GGPLOT            -1 SCLD   -1    0        1.        0.        1.     1.E-10${b}8
    RUN $run.  DAYS $days.    CLOUDINESS IN LAYER-35.
GGPLOT            -1 SCLD   -1    0        1.        0.        1.     1.E-10${b}8
    RUN $run.  DAYS $days.    CLOUDINESS IN LAYER-36.
GGPLOT            -1 SCLD   -1    0        1.        0.        1.     1.E-10${b}8
    RUN $run.  DAYS $days.    CLOUDINESS IN LAYER-37.
GGPLOT            -1 SCLD   -1    0        1.        0.        1.     1.E-10${b}8
    RUN $run.  DAYS $days.    CLOUDINESS IN LAYER-38.
GGPLOT            -1 SCLD   -1    0        1.        0.        1.     1.E-10${b}8
    RUN $run.  DAYS $days.    CLOUDINESS IN LAYER-39.
GGPLOT            -1 SCLD   -1    0        1.        0.        1.     1.E-10${b}8
    RUN $run.  DAYS $days.    CLOUDINESS IN LAYER-40.
fi
if [ "$shade" = on ] ; then
+   .    :    .    :    .    :    .    :    .    :    .    :    .    :    .    :
GGPLOT            -1 SCLD   -14   0        1.        0.        1.     1.E-10${b}8
    RUN $run.  DAYS $days.    CLOUDINESS IN LAYER-01.
              6    0   48   51   56   64    3
                  .1        .3        .5        .7        .9       1.0
GGPLOT            -1 SCLD   -14   0        1.        0.        1.     1.E-10${b}8
    RUN $run.  DAYS $days.    CLOUDINESS IN LAYER-02.
              6    0   48   51   56   64    3
                  .1        .3        .5        .7        .9       1.0
GGPLOT            -1 SCLD   -14   0        1.        0.        1.     1.E-10${b}8
    RUN $run.  DAYS $days.    CLOUDINESS IN LAYER-03.
              6    0   48   51   56   64    3
                  .1        .3        .5        .7        .9       1.0
GGPLOT            -1 SCLD   -14   0        1.        0.        1.     1.E-10${b}8
    RUN $run.  DAYS $days.    CLOUDINESS IN LAYER-04.
              6    0   48   51   56   64    3
                  .1        .3        .5        .7        .9       1.0
GGPLOT            -1 SCLD   -14   0        1.        0.        1.     1.E-10${b}8
    RUN $run.  DAYS $days.    CLOUDINESS IN LAYER-05.
              6    0   48   51   56   64    3
                  .1        .3        .5        .7        .9       1.0
GGPLOT            -1 SCLD   -14   0        1.        0.        1.     1.E-10${b}8
    RUN $run.  DAYS $days.    CLOUDINESS IN LAYER-06.
              6    0   48   51   56   64    3
                  .1        .3        .5        .7        .9       1.0
GGPLOT            -1 SCLD   -14   0        1.        0.        1.     1.E-10${b}8
    RUN $run.  DAYS $days.    CLOUDINESS IN LAYER-07.
              6    0   48   51   56   64    3
                  .1        .3        .5        .7        .9       1.0
GGPLOT            -1 SCLD   -14   0        1.        0.        1.     1.E-10${b}8
    RUN $run.  DAYS $days.    CLOUDINESS IN LAYER-08.
              6    0   48   51   56   64    3
                  .1        .3        .5        .7        .9       1.0
GGPLOT            -1 SCLD   -14   0        1.        0.        1.     1.E-10${b}8
    RUN $run.  DAYS $days.    CLOUDINESS IN LAYER-09.
              6    0   48   51   56   64    3
                  .1        .3        .5        .7        .9       1.0
GGPLOT            -1 SCLD   -14   0        1.        0.        1.     1.E-10${b}8
    RUN $run.  DAYS $days.    CLOUDINESS IN LAYER-10.
              6    0   48   51   56   64    3
                  .1        .3        .5        .7        .9       1.0
GGPLOT            -1 SCLD   -14   0        1.        0.        1.     1.E-10${b}8
    RUN $run.  DAYS $days.    CLOUDINESS IN LAYER-11.
              6    0   48   51   56   64    3
                  .1        .3        .5        .7        .9       1.0
GGPLOT            -1 SCLD   -14   0        1.        0.        1.     1.E-10${b}8
    RUN $run.  DAYS $days.    CLOUDINESS IN LAYER-12.
              6    0   48   51   56   64    3
                  .1        .3        .5        .7        .9       1.0
GGPLOT            -1 SCLD   -14   0        1.        0.        1.     1.E-10${b}8
    RUN $run.  DAYS $days.    CLOUDINESS IN LAYER-13.
              6    0   48   51   56   64    3
                  .1        .3        .5        .7        .9       1.0
GGPLOT            -1 SCLD   -14   0        1.        0.        1.     1.E-10${b}8
    RUN $run.  DAYS $days.    CLOUDINESS IN LAYER-14.
              6    0   48   51   56   64    3
                  .1        .3        .5        .7        .9       1.0
GGPLOT            -1 SCLD   -14   0        1.        0.        1.     1.E-10${b}8
    RUN $run.  DAYS $days.    CLOUDINESS IN LAYER-15.
              6    0   48   51   56   64    3
                  .1        .3        .5        .7        .9       1.0
GGPLOT            -1 SCLD   -14   0        1.        0.        1.     1.E-10${b}8
    RUN $run.  DAYS $days.    CLOUDINESS IN LAYER-16.
              6    0   48   51   56   64    3
                  .1        .3        .5        .7        .9       1.0
GGPLOT            -1 SCLD   -14   0        1.        0.        1.     1.E-10${b}8
    RUN $run.  DAYS $days.    CLOUDINESS IN LAYER-17.
              6    0   48   51   56   64    3
                  .1        .3        .5        .7        .9       1.0
GGPLOT            -1 SCLD   -14   0        1.        0.        1.     1.E-10${b}8
    RUN $run.  DAYS $days.    CLOUDINESS IN LAYER-18.
              6    0   48   51   56   64    3
                  .1        .3        .5        .7        .9       1.0
GGPLOT            -1 SCLD   -14   0        1.        0.        1.     1.E-10${b}8
    RUN $run.  DAYS $days.    CLOUDINESS IN LAYER-19.
              6    0   48   51   56   64    3
                  .1        .3        .5        .7        .9       1.0
GGPLOT            -1 SCLD   -14   0        1.        0.        1.     1.E-10${b}8
    RUN $run.  DAYS $days.    CLOUDINESS IN LAYER-20.
              6    0   48   51   56   64    3
                  .1        .3        .5        .7        .9       1.0
GGPLOT            -1 SCLD   -14   0        1.        0.        1.     1.E-10${b}8
    RUN $run.  DAYS $days.    CLOUDINESS IN LAYER-21.
              6    0   48   51   56   64    3
                  .1        .3        .5        .7        .9       1.0
GGPLOT            -1 SCLD   -14   0        1.        0.        1.     1.E-10${b}8
    RUN $run.  DAYS $days.    CLOUDINESS IN LAYER-22.
              6    0   48   51   56   64    3
                  .1        .3        .5        .7        .9       1.0
GGPLOT            -1 SCLD   -14   0        1.        0.        1.     1.E-10${b}8
    RUN $run.  DAYS $days.    CLOUDINESS IN LAYER-23.
              6    0   48   51   56   64    3
                  .1        .3        .5        .7        .9       1.0
GGPLOT            -1 SCLD   -14   0        1.        0.        1.     1.E-10${b}8
    RUN $run.  DAYS $days.    CLOUDINESS IN LAYER-24.
              6    0   48   51   56   64    3
                  .1        .3        .5        .7        .9       1.0
GGPLOT            -1 SCLD   -14   0        1.        0.        1.     1.E-10${b}8
    RUN $run.  DAYS $days.    CLOUDINESS IN LAYER-25.
              6    0   48   51   56   64    3
                  .1        .3        .5        .7        .9       1.0
GGPLOT            -1 SCLD   -14   0        1.        0.        1.     1.E-10${b}8
    RUN $run.  DAYS $days.    CLOUDINESS IN LAYER-26.
              6    0   48   51   56   64    3
                  .1        .3        .5        .7        .9       1.0
GGPLOT            -1 SCLD   -14   0        1.        0.        1.     1.E-10${b}8
    RUN $run.  DAYS $days.    CLOUDINESS IN LAYER-27.
              6    0   48   51   56   64    3
                  .1        .3        .5        .7        .9       1.0
GGPLOT            -1 SCLD   -14   0        1.        0.        1.     1.E-10${b}8
    RUN $run.  DAYS $days.    CLOUDINESS IN LAYER-28.
              6    0   48   51   56   64    3
                  .1        .3        .5        .7        .9       1.0
GGPLOT            -1 SCLD   -14   0        1.        0.        1.     1.E-10${b}8
    RUN $run.  DAYS $days.    CLOUDINESS IN LAYER-29.
              6    0   48   51   56   64    3
                  .1        .3        .5        .7        .9       1.0
GGPLOT            -1 SCLD   -14   0        1.        0.        1.     1.E-10${b}8
    RUN $run.  DAYS $days.    CLOUDINESS IN LAYER-30.
              6    0   48   51   56   64    3
                  .1        .3        .5        .7        .9       1.0
GGPLOT            -1 SCLD   -14   0        1.        0.        1.     1.E-10${b}8
    RUN $run.  DAYS $days.    CLOUDINESS IN LAYER-31.
              6    0   48   51   56   64    3
                  .1        .3        .5        .7        .9       1.0
GGPLOT            -1 SCLD   -14   0        1.        0.        1.     1.E-10${b}8
    RUN $run.  DAYS $days.    CLOUDINESS IN LAYER-32.
              6    0   48   51   56   64    3
                  .1        .3        .5        .7        .9       1.0
GGPLOT            -1 SCLD   -14   0        1.        0.        1.     1.E-10${b}8
    RUN $run.  DAYS $days.    CLOUDINESS IN LAYER-33.
              6    0   48   51   56   64    3
                  .1        .3        .5        .7        .9       1.0
GGPLOT            -1 SCLD   -14   0        1.        0.        1.     1.E-10${b}8
    RUN $run.  DAYS $days.    CLOUDINESS IN LAYER-34.
              6    0   48   51   56   64    3
                  .1        .3        .5        .7        .9       1.0
GGPLOT            -1 SCLD   -14   0        1.        0.        1.     1.E-10${b}8
    RUN $run.  DAYS $days.    CLOUDINESS IN LAYER-35.
              6    0   48   51   56   64    3
                  .1        .3        .5        .7        .9       1.0
GGPLOT            -1 SCLD   -14   0        1.        0.        1.     1.E-10${b}8
    RUN $run.  DAYS $days.    CLOUDINESS IN LAYER-36.
              6    0   48   51   56   64    3
                  .1        .3        .5        .7        .9       1.0
GGPLOT            -1 SCLD   -14   0        1.        0.        1.     1.E-10${b}8
    RUN $run.  DAYS $days.    CLOUDINESS IN LAYER-37.
              6    0   48   51   56   64    3
                  .1        .3        .5        .7        .9       1.0
GGPLOT            -1 SCLD   -14   0        1.        0.        1.     1.E-10${b}8
    RUN $run.  DAYS $days.    CLOUDINESS IN LAYER-38.
              6    0   48   51   56   64    3
                  .1        .3        .5        .7        .9       1.0
GGPLOT            -1 SCLD   -14   0        1.        0.        1.     1.E-10${b}8
    RUN $run.  DAYS $days.    CLOUDINESS IN LAYER-39.
              6    0   48   51   56   64    3
                  .1        .3        .5        .7        .9       1.0
GGPLOT            -1 SCLD   -14   0        1.        0.        1.     1.E-10${b}8
    RUN $run.  DAYS $days.    CLOUDINESS IN LAYER-40.
              6    0   48   51   56   64    3
                  .1        .3        .5        .7        .9       1.0
fi
if [ "$xtracld" = on ] ; then
+   .    :    .    :    .    :    .    :    .    :    .    :    .    :    .    :
  XFIND.      TAC
+   .    :    .    :    .    :    .    :    .    :    .    :    .    :    .    :
GGPLOT            -1   -1   -1    0        1.        0.        1.     1.E-10${b}8
    RUN $run.  DAYS $days.    CLOUD TAU IN LAYER-01.
GGPLOT            -1   -1   -1    0        1.        0.        1.     1.E-10${b}8
    RUN $run.  DAYS $days.    CLOUD TAU IN LAYER-02.
GGPLOT            -1   -1   -1    0        1.        0.        1.     1.E-10${b}8
    RUN $run.  DAYS $days.    CLOUD TAU IN LAYER-03.
GGPLOT            -1   -1   -1    0        1.        0.        1.     1.E-10${b}8
    RUN $run.  DAYS $days.    CLOUD TAU IN LAYER-04.
GGPLOT            -1   -1   -1    0        1.        0.        1.     1.E-10${b}8
    RUN $run.  DAYS $days.    CLOUD TAU IN LAYER-05.
GGPLOT            -1   -1   -1    0        1.        0.        1.     1.E-10${b}8
    RUN $run.  DAYS $days.    CLOUD TAU IN LAYER-06.
GGPLOT            -1   -1   -1    0        1.        0.        1.     1.E-10${b}8
    RUN $run.  DAYS $days.    CLOUD TAU IN LAYER-07.
GGPLOT            -1   -1   -1    0        1.        0.        1.     1.E-10${b}8
    RUN $run.  DAYS $days.    CLOUD TAU IN LAYER-08.
GGPLOT            -1   -1   -1    0        1.        0.        1.     1.E-10${b}8
    RUN $run.  DAYS $days.    CLOUD TAU IN LAYER-09.
GGPLOT            -1   -1   -1    0        1.        0.        1.     1.E-10${b}8
    RUN $run.  DAYS $days.    CLOUD TAU IN LAYER-10.
GGPLOT            -1   -1   -1    0        1.        0.        1.     1.E-10${b}8
    RUN $run.  DAYS $days.    CLOUD TAU IN LAYER-11.
GGPLOT            -1   -1   -1    0        1.        0.        1.     1.E-10${b}8
    RUN $run.  DAYS $days.    CLOUD TAU IN LAYER-12.
GGPLOT            -1   -1   -1    0        1.        0.        1.     1.E-10${b}8
    RUN $run.  DAYS $days.    CLOUD TAU IN LAYER-13.
GGPLOT            -1   -1   -1    0        1.        0.        1.     1.E-10${b}8
    RUN $run.  DAYS $days.    CLOUD TAU IN LAYER-14.
GGPLOT            -1   -1   -1    0        1.        0.        1.     1.E-10${b}8
    RUN $run.  DAYS $days.    CLOUD TAU IN LAYER-15.
GGPLOT            -1   -1   -1    0        1.        0.        1.     1.E-10${b}8
    RUN $run.  DAYS $days.    CLOUD TAU IN LAYER-16.
GGPLOT            -1   -1   -1    0        1.        0.        1.     1.E-10${b}8
    RUN $run.  DAYS $days.    CLOUD TAU IN LAYER-17.
GGPLOT            -1   -1   -1    0        1.        0.        1.     1.E-10${b}8
    RUN $run.  DAYS $days.    CLOUD TAU IN LAYER-18.
GGPLOT            -1   -1   -1    0        1.        0.        1.     1.E-10${b}8
    RUN $run.  DAYS $days.    CLOUD TAU IN LAYER-19.
GGPLOT            -1   -1   -1    0        1.        0.        1.     1.E-10${b}8
    RUN $run.  DAYS $days.    CLOUD TAU IN LAYER-20.
GGPLOT            -1   -1   -1    0        1.        0.        1.     1.E-10${b}8
    RUN $run.  DAYS $days.    CLOUD TAU IN LAYER-21.
GGPLOT            -1   -1   -1    0        1.        0.        1.     1.E-10${b}8
    RUN $run.  DAYS $days.    CLOUD TAU IN LAYER-22.
GGPLOT            -1   -1   -1    0        1.        0.        1.     1.E-10${b}8
    RUN $run.  DAYS $days.    CLOUD TAU IN LAYER-23.
GGPLOT            -1   -1   -1    0        1.        0.        1.     1.E-10${b}8
    RUN $run.  DAYS $days.    CLOUD TAU IN LAYER-24.
GGPLOT            -1   -1   -1    0        1.        0.        1.     1.E-10${b}8
    RUN $run.  DAYS $days.    CLOUD TAU IN LAYER-25.
GGPLOT            -1   -1   -1    0        1.        0.        1.     1.E-10${b}8
    RUN $run.  DAYS $days.    CLOUD TAU IN LAYER-26.
GGPLOT            -1   -1   -1    0        1.        0.        1.     1.E-10${b}8
    RUN $run.  DAYS $days.    CLOUD TAU IN LAYER-27.
GGPLOT            -1   -1   -1    0        1.        0.        1.     1.E-10${b}8
    RUN $run.  DAYS $days.    CLOUD TAU IN LAYER-28.
GGPLOT            -1   -1   -1    0        1.        0.        1.     1.E-10${b}8
    RUN $run.  DAYS $days.    CLOUD TAU IN LAYER-29.
GGPLOT            -1   -1   -1    0        1.        0.        1.     1.E-10${b}8
    RUN $run.  DAYS $days.    CLOUD TAU IN LAYER-30.
GGPLOT            -1   -1   -1    0        1.        0.        1.     1.E-10${b}8
    RUN $run.  DAYS $days.    CLOUD TAU IN LAYER-31.
GGPLOT            -1   -1   -1    0        1.        0.        1.     1.E-10${b}8
    RUN $run.  DAYS $days.    CLOUD TAU IN LAYER-32.
GGPLOT            -1   -1   -1    0        1.        0.        1.     1.E-10${b}8
    RUN $run.  DAYS $days.    CLOUD TAU IN LAYER-33.
GGPLOT            -1   -1   -1    0        1.        0.        1.     1.E-10${b}8
    RUN $run.  DAYS $days.    CLOUD TAU IN LAYER-34.
GGPLOT            -1   -1   -1    0        1.        0.        1.     1.E-10${b}8
    RUN $run.  DAYS $days.    CLOUD TAU IN LAYER-35.
GGPLOT            -1   -1   -1    0        1.        0.        1.     1.E-10${b}8
    RUN $run.  DAYS $days.    CLOUD TAU IN LAYER-36.
GGPLOT            -1   -1   -1    0        1.        0.        1.     1.E-10${b}8
    RUN $run.  DAYS $days.    CLOUD TAU IN LAYER-37.
GGPLOT            -1   -1   -1    0        1.        0.        1.     1.E-10${b}8
    RUN $run.  DAYS $days.    CLOUD TAU IN LAYER-38.
GGPLOT            -1   -1   -1    0        1.        0.        1.     1.E-10${b}8
    RUN $run.  DAYS $days.    CLOUD TAU IN LAYER-39.
GGPLOT            -1   -1   -1    0        1.        0.        1.     1.E-10${b}8
    RUN $run.  DAYS $days.    CLOUD TAU IN LAYER-40.
fi
fi
. tailfram.cdk

end_of_data

. endjcl.cdk
