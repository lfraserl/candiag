#deck strtst
jobname=strat ; time=$stime ; memory=$memory1
. comjcl.cdk

cat > Execute_Script <<end_of_script

#                   strtst              m lazare. dec 10/84 - ml.
#   ----------------------------------- computes the standard t-test for
#                                       significance of the deviation of a
#                                       statistic from its pooled control
#                                       state  and plots out the result.
#                                       the percent area of significance on the
#                                       t-test plots are also calculated.
#                                       this test is done for statistics at
#                                       specified pressure levels in the
#                                       stratosphere.
#                                       model1 and letter x refer to pooled
#                                       control run  while model2 and letter y
#                                       refer to the experimental run.
#
    access filex ${model1}gp
    access filey ${model2}gp
    libncar plunit=$plunit 
.   plotid.cdk
#   ----------------------------------- make initial null standard deviation file
    xfind filey del 
    xlin del null 
    rm del xlin
#   ----------------------------------- find appropriate critical value of t.
    critt tc 
    xlin null tcrit input=tc 
    rm xlin
    xlin null nx 
    xlin null ny 
    xlin nx nx1 
    xlin ny ny1 
    add nx ny b1 
    mlt nx ny b2 
    div b1 b2 b3 
    sqroot b3 fact 
    add nx1 ny1 dof 
    rm tc nx ny b1 b2 b3
#   ----------------------------------- zonal wind at 10  50 and 100 mbs.
.   ttstgp.cdk
    newnam num du 
    square du du2 
    ggplot tratio 
    rm x y num tratio
#   ----------------------------------- meridional wind at 10  50 and 100 mbs.
#                                       plot the vector wind difference.
.   ttstgp.cdk
    newnam num dv 
    square dv dv2 
    add du2 dv2 dav2 
    sqroot dav2 dav 
    newnam dav amp 
    rm dav2 du2 dv2 dav
    ggplot amp du dv 
    rm amp du dv
    ggplot tratio 
    rm x y num tratio
#   ----------------------------------- temperature at 10  50 and 100 mbs.
.   ttstgp.cdk
    ggplot num 
    ggplot tratio 
    rm nx1 ny1 dof fact tcrit filex filey
    rm x y num tratio
    if [ "$splot" = on ] ; then
      ggplot stotal 
      rm stotal
    fi
.   plotid.cdk
.   plot.cdk

end_of_script

cat > Input_Cards <<end_of_data

+   .    :    .    :    .    :    .    :    .    :    .    :    .    :    .    :
         DAYS      = $days 
         RUN       = $run
         MODEL1    = $model1 
         MODEL2    = $model2 
0 STRTST   ---------------------------------------------------- STRTST
. headfram.cdk
+   .    :    .    :    .    :    .    :    .    :    .    :    .    :    .    :
XFIND.        $d 
+   .    :    .    :    .    :    .    :    .    :    .    :    .    :    .    :
XLIN.           0.E0      0.E0
+   .    :    .    :    .    :    .    :    .    :    .    :    .    :    .    :
CRITT.       $alpha     $xyr     $yyr
+   .    :    .    :    .    :    .    :    .    :    .    :    .    :    .    :
XLIN.           0.E0     $xyr
+   .    :    .    :    .    :    .    :    .    :    .    :    .    :    .    :
XLIN.           0.E0     $yyr
+   .    :    .    :    .    :    .    :    .    :    .    :    .    :    .    :
XLIN.           1.E0     -1.E0
+   .    :    .    :    .    :    .    :    .    :    .    :    .    :    .    :
XLIN.           1.E0     -1.E0
+   .    :    .    :    .    :    .    :    .    :    .    :    .    :    .    :
XFIND.        U 
+   .    :    .    :    .    :    .    :    .    :    .    :    .    :    .    :
XFIND.        S(U)
+   .    :    .    :    .    :    .    :    .    :    .    :    .    :    .    :
XFIND.        U 
+   .    :    .    :    .    :    .    :    .    :    .    :    .    :    .    :
XFIND.        S(U)
+   .    :    .    :    .    :    .    :    .    :    .    :    .    :    .    :
NEWNAM.     NUM 
+   .    :    .    :    .    :    .    :    .    :    .    :    .    :    .    :
NEWNAM.      SU 
+   .    :    .    :    .    :    .    :    .    :    .    :    .    :    .    :
NEWNAM.    TRAT 
+   .    :    .    :    .    :    .    :    .    :    .    :    .    :    .    :
  FMASK.          -1   -1   GE        1.
+   .    :    .    :    .    :    .    :    .    :    .    :    .    :    .    :
  XLIN.         100.
+   .    :    .    :    .    :    .    :    .    :    .    :    .    :    .    :
NEWNAM.      DU 
+   .    :    .    :    .    :    .    :    .    :    .    :    .    :    .    :
GGPLOT.           -1 TRAT   10$map      1.E0      0.E0  1025.E-2      1.E00-1+8
RUN $run. DAYS $days. RATIO OF T/TCRIT FOR U AT  10 MBS.
GGPLOT.           -1 TRAT   50$map      1.E0      0.E0  1025.E-2      1.E00-1+8
RUN $run. DAYS $days. RATIO OF T/TCRIT FOR U AT  50 MBS.
GGPLOT.           -1 TRAT  100$map      1.E0      0.E0  1025.E-2      1.E00-1+8
RUN $run. DAYS $days. RATIO OF T/TCRIT FOR U AT 100 MBS.
+   .    :    .    :    .    :    .    :    .    :    .    :    .    :    .    :
XFIND.        V 
+   .    :    .    :    .    :    .    :    .    :    .    :    .    :    .    :
XFIND.        S(V)
+   .    :    .    :    .    :    .    :    .    :    .    :    .    :    .    :
XFIND.        V 
+   .    :    .    :    .    :    .    :    .    :    .    :    .    :    .    :
XFIND.        S(V)
+   .    :    .    :    .    :    .    :    .    :    .    :    .    :    .    :
NEWNAM.     NUM 
+   .    :    .    :    .    :    .    :    .    :    .    :    .    :    .    :
NEWNAM.      SV 
+   .    :    .    :    .    :    .    :    .    :    .    :    .    :    .    :
NEWNAM.    TRAT 
+   .    :    .    :    .    :    .    :    .    :    .    :    .    :    .    :
  FMASK.          -1   -1   GE        1.
+   .    :    .    :    .    :    .    :    .    :    .    :    .    :    .    :
  XLIN.         100.
+   .    :    .    :    .    :    .    :    .    :    .    :    .    :    .    :
NEWNAM.      DV 
+   .    :    .    :    .    :    .    :    .    :    .    :    .    :    .    :
NEWNAM.     AMP 
+   .    :    .    :    .    :    .    :    .    :    .    :    .    :    .    :
GGPLOT.           -1  AMP   10$map        1.        0.       30.        4.2${b}8
RUN $run. DAYS $days. (EXPMT-CONTROL) FOR MEAN WIND AT  10 MBS. UNITS M/SEC.
                  1.        0.       20.$ncx$ncy
                        VECPLOT OF (U,V). 
GGPLOT.           -1  AMP   50$map        1.        0.       30.        2.2${b}8
RUN $run. DAYS $days. (EXPMT-CONTROL) FOR MEAN WIND AT  50 MBS. UNITS M/SEC.
                  1.        0.       10.$ncx$ncy
                        VECPLOT OF (U,V). 
GGPLOT.           -1  AMP  100$map        1.        0.       30.        2.2${b}8
RUN $run. DAYS $days. (EXPMT-CONTROL) FOR MEAN WIND AT 100 MBS. UNITS M/SEC.
                  1.        0.       10.$ncx$ncy
                        VECPLOT OF (U,V). 
+   .    :    .    :    .    :    .    :    .    :    .    :    .    :    .    :
GGPLOT.           -1 TRAT   10$map      1.E0      0.E0  1025.E-2      1.E00-1+8
RUN $run. DAYS $days. RATIO OF T/TCRIT FOR V AT  10 MBS.
GGPLOT.           -1 TRAT   50$map      1.E0      0.E0  1025.E-2      1.E00-1+8
RUN $run. DAYS $days. RATIO OF T/TCRIT FOR V AT  50 MBS.
GGPLOT.           -1 TRAT  100$map      1.E0      0.E0  1025.E-2      1.E00-1+8
RUN $run. DAYS $days. RATIO OF T/TCRIT FOR V AT 100 MBS.
+   .    :    .    :    .    :    .    :    .    :    .    :    .    :    .    :
XFIND.        T 
+   .    :    .    :    .    :    .    :    .    :    .    :    .    :    .    :
XFIND.        S(T)
+   .    :    .    :    .    :    .    :    .    :    .    :    .    :    .    :
XFIND.        T 
+   .    :    .    :    .    :    .    :    .    :    .    :    .    :    .    :
XFIND.        S(T)
+   .    :    .    :    .    :    .    :    .    :    .    :    .    :    .    :
NEWNAM.     NUM 
+   .    :    .    :    .    :    .    :    .    :    .    :    .    :    .    :
NEWNAM.      ST 
+   .    :    .    :    .    :    .    :    .    :    .    :    .    :    .    :
NEWNAM.    TRAT 
+   .    :    .    :    .    :    .    :    .    :    .    :    .    :    .    :
  FMASK.          -1   -1   GE        1.
+   .    :    .    :    .    :    .    :    .    :    .    :    .    :    .    :
  XLIN.         100.
+   .    :    .    :    .    :    .    :    .    :    .    :    .    :    .    :
GGPLOT.           -1  NUM   10$map      1.E0    -40.E0     40.E0      2.E00${b}8
RUN $run. DAYS $days. (EXPMT-CONTROL) FOR T AT  10 MBS. UNITS DEGK. 
GGPLOT.           -1  NUM   50$map      1.E0    -40.E0     40.E0      2.E00${b}8
RUN $run. DAYS $days. (EXPMT-CONTROL) FOR T AT  50 MBS. UNITS DEGK. 
GGPLOT.           -1  NUM  100$map      1.E0    -40.E0     40.E0      2.E00${b}8
RUN $run. DAYS $days. (EXPMT-CONTROL) FOR T AT 100 MBS. UNITS DEGK. 
+   .    :    .    :    .    :    .    :    .    :    .    :    .    :    .    :
GGPLOT.           -1 TRAT   10$map      1.E0      0.E0  1025.E-2      1.E00-1+8
RUN $run. DAYS $days. RATIO OF T/TCRIT FOR T AT  10 MBS.
GGPLOT.           -1 TRAT   50$map      1.E0      0.E0  1025.E-2      1.E00-1+8
RUN $run. DAYS $days. RATIO OF T/TCRIT FOR T AT  50 MBS.
GGPLOT.           -1 TRAT  100$map      1.E0      0.E0  1025.E-2      1.E00-1+8
RUN $run. DAYS $days. RATIO OF T/TCRIT FOR T AT 100 MBS.
if [ "$splot" = on ] ; then
+   .    :    .    :    .    :    .    :    .    :    .    :    .    :    .    :
GGPLOT.           -1   SU   10$map      1.E0      0.E0     40.E0      2.E00${b}8
RUN $run. DAYS $days. COMBINED ST. DEV. OF U AT  10 MBS. UNITS M/SEC. 
GGPLOT.           -1   SU   50$map      1.E0      0.E0     10.E0      1.E00${b}8
RUN $run. DAYS $days. COMBINED ST. DEV. OF U AT  50 MBS. UNITS M/SEC. 
GGPLOT.           -1   SU  100$map      1.E0      0.E0     10.E0      1.E00${b}8
RUN $run. DAYS $days. COMBINED ST. DEV. OF U AT 100 MBS. UNITS M/SEC. 
GGPLOT.           -1   SV   10$map      1.E0      0.E0     40.E0      2.E00${b}8
RUN $run. DAYS $days. COMBINED ST. DEV. OF V AT  10 MBS. UNITS M/SEC. 
GGPLOT.           -1   SV   50$map      1.E0      0.E0     10.E0     5.E-10${b}8
RUN $run. DAYS $days. COMBINED ST. DEV. OF V AT  50 MBS. UNITS M/SEC. 
GGPLOT.           -1   SV  100$map      1.E0      0.E0     10.E0     5.E-10${b}8
RUN $run. DAYS $days. COMBINED ST. DEV. OF V AT 100 MBS. UNITS M/SEC. 
GGPLOT.           -1   ST   10$map      1.E0      0.E0     20.E0      1.E00${b}8
RUN $run. DAYS $days. COMBINED ST. DEV. OF T AT  10 MBS. UNITS DEGK.
GGPLOT.           -1   ST   50$map      1.E0      0.E0     20.E0      1.E00${b}8
RUN $run. DAYS $days. COMBINED ST. DEV. OF T AT  50 MBS. UNITS DEGK.
GGPLOT.           -1   ST  100$map      1.E0      0.E0     20.E0      1.E00${b}8
RUN $run. DAYS $days. COMBINED ST. DEV. OF T AT 100 MBS. UNITS DEGK.
fi
. tailfram.cdk

end_of_data

. endjcl.cdk


