#!/bin/sh
#
# This script is only included in the merged_diag_script grouping when
# gas-phase chemistry is run in MAM mode. BW, June 2023
#
# Amendments to original: replace $pmaxl with 1000
#                         source the file of switches
#                         replace use of gsapl with gsaspl_lnsp, line 55
#
# DAP: Sep 28, 2022 - conversion to straight shell script
# ------------------------------------------------------------------------------

# Source the list of on/off switches
source ${CANESM_SRC_ROOT}/CanDIAG/diag4/gaschem_diag_switches.sh

# SK: access del and gslnsp
    access del    ${flabel}_gptbeta
    access gslnsp ${flabel}_gslnsp na
    if [ ! -s gslnsp ] ; then
      if [ "$datatype" = "gridsig" ] ; then
        echo "SELECT    STEPS $t1 $t2 $t3 LEVS    1    1 NAME LNSP" |
           ccc select npakgg gslnsp
      else
        echo "SELECT    STEPS $t1 $t2 $t3 LEVS    1    1 NAME LNSP" |
           ccc select npaksp sslnsp
        echo "COFAGG.   $lon$lat" | ccc cofagg sslnsp gslnsp
        rm sslnsp
      fi
    fi
#
    if [ "$datatype" = "specsig" ] ; then

#  -------  gsapl input card
      echo "GSAPL.    $plv       0.0       0.0$coord$plid
$p01$p02$p03$p04$p05$p06$p07$p08$p09$p10$p11$p12$p13$p14$p15$p16
$p17$p18$p19$p20$p21$p22$p23$p24$p25$p26$p27$p28$p29$p30$p31$p32
$p33$p34$p35$p36$p37$p38$p39$p40$p41$p42$p43$p44$p45$p46$p47$p48
$p49$p50$p51$p52$p53$p54$p55$p56$p57$p58$p59$p60$p61$p62$p63$p64
$p65$p66$p67$p68$p69$p70$p71$p72$p73$p74$p75$p76$p77$p78$p79$p80
$p81$p82$p83$p84$p85$p86$p87$p88$p89$p90$p91$p92$p93$p94$p95$p96
$p97$p98$p99$p100" > .gsapl_input_card


#   ---------------------------------- select variables
.     ggfiles.cdk

      gsvars="FTOX FTOY FTNE FTNW FTNY TDOX TDOY TDNX TDNY RHOG"
      GSVARS=`fmtselname $gsvars`

echo "SELECT.   STEPS $t1 $t2 $s3 LEVS-9100 1000 NAME$GSVARS" | ccc select npakgg $gsvars
      add FTNE FTNW FTNX

# do for each variable
      for v in FTOX FTOY FTNE FTNW FTNX FTNY TDOX TDOY TDNX TDNY RHOG ; do
	gsapl_lnsp ${v} gslnsp ${v}_P input=.gsapl_input_card
        timavg ${v}_P  ${v}_PT
        zonavg ${v}_PT ${v}_PTZ
#        rm ${v}_P
      done

#   ---------------------------------- save gpfile
      echo "XSAVE.        FTOX_PT
NEWNAM.    FTOX
XSAVE.        FTOY_PT
NEWNAM.    FTOY
XSAVE.        FTNE_PT
NEWNAM.    FTNE
XSAVE.        FTNW_PT
NEWNAM.    FTNW
XSAVE.        FTNX_PT
NEWNAM.    FTNX
XSAVE.        FTNY_PT
NEWNAM.    FTNY
XSAVE.        TDOX_PT
NEWNAM.    TDOX
XSAVE.        TDOY_PT
NEWNAM.    TDOY
XSAVE.        TDNX_PT
NEWNAM.    TDNX
XSAVE.        TDNY_PT
NEWNAM.    TDNY
XSAVE.        RHOG_PT
NEWNAM.    RHOG" > .xsave_input_card
      xsave new_gp FTOX_PT FTOY_PT FTNE_PT FTNW_PT \
                   FTNX_PT FTNY_PT TDOX_PT TDOY_PT TDNX_PT TDNY_PT \
                   RHOG_PT new_gp. input=.xsave_input_card
      mv new_gp. new_gp

      release oldgp
      access  oldgp ${flabel}gp
      xjoin   oldgp new_gp newgp
      save    newgp ${flabel}gp
      delete oldgp

#   ---------------------------------- save xpfile
      echo "XSAVE.        (FTOX_PTZ)R
NEWNAM.    FTOX
XSAVE.        (FTOY_PTZ)R
NEWNAM.    FTOY
XSAVE.        (FTNE_PTZ)R
NEWNAM.    FTNE
XSAVE.        (FTNW_PTZ)R
NEWNAM.    FTNW
XSAVE.        (FTNX_PTZ)R
NEWNAM.    FTNX
XSAVE.        (FTNY_PTZ)R
NEWNAM.    FTNY
XSAVE.        (TDOX_PTZ)R
NEWNAM.    TDOX
XSAVE.        (TDOY_PTZ)R
NEWNAM.    TDOY
XSAVE.        (TDNX_PTZ)R
NEWNAM.    TDNX
XSAVE.        (TDNY_PTZ)R
NEWNAM.    TDNY
XSAVE.        (RHOG_PTZ)R
NEWNAM.    RHOG" > .xsave_input_card
      xsave new_xp FTOX_PTZ FTOY_PTZ FTNE_PTZ FTNW_PTZ \
                   FTNX_PTZ FTNY_PTZ TDOX_PTZ TDOY_PTZ TDNX_PTZ \
                   TDNY_PTZ RHOG_PTZ new_xp. input=.xsave_input_card
      mv new_xp. new_xp

      release oldxp
      access  oldxp ${flabel}xp
      xjoin   oldxp new_xp newxp
      save newxp ${flabel}xp
      delete oldxp
#
#   ----- save instantaneous data
      if [ "$iesave" = "on" ] ; then
#
# --- not including option with selstep here (ieslct=on)
#
        if [ "$ieslct" = "off" ] ; then
          echo "XSAVE.        INSTANTANEOUS FTOX
NEWNAM.    FTOX
XSAVE.        INSTANTANEOUS FTOY
NEWNAM.    FTOY
XSAVE.        INSTANTANEOUS FTNE
NEWNAM.    FTNE
XSAVE.        INSTANTANEOUS FTNW
NEWNAM.    FTNW
XSAVE.        INSTANTANEOUS FTNX
NEWNAM.    FTNX
XSAVE.        INSTANTANEOUS FTNY
NEWNAM.    FTNY
XSAVE.        INSTANTANEOUS TDOX
NEWNAM.    TDOX
XSAVE.        INSTANTANEOUS TDOY
NEWNAM.    TDOY
XSAVE.        INSTANTANEOUS TDNX
NEWNAM.    TDNX
XSAVE.        INSTANTANEOUS TDNY
NEWNAM.    TDNY
XSAVE.        INSTANTANEOUS RHOG
NEWNAM.    RHOG" > .xsave_input_card
          xsave new_id FTOX FTOY FTNE FTNW FTNX FTNY TDOX \
                       TDOY TDNX TDNY RHOG new_id. input=.xsave_input_card
          mv new_id. new_id

          release oldid
          access oldid ${flabel}id      na
          xjoin oldid new_id newid
          save newid ${flabel}id
          delete oldid                  na
        fi
      fi

    fi # specsig endif
