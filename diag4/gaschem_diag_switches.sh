#!/bin/sh

# This file holds a list of switches required by some of the diagnostics
# scripts for gas-phase chemistry. Maybe one day it will grow up and become
# a namelist and live under /config, but for now it is here, and will be
# sourced by the scripts that use the switches.
#
# The settings are taken from Dave's sub_diags.scr file.
#
# BWinter, July 14 2023
# -----------------------------------------------------------------------------

# Save instantaneous daily data
dailysv=on

# Save day/night zonal-time mean data to [c]xp file
dnsave=off

# Pressure deck: save pressure on model levels
gepsave=on

# Density deck: save number density on model levels
gerhosave=on

# Save monthly-average data on eta-levels to ge file
gesave=on

# Save instantaneous eta-level data to ie file
iesave=off

# Flag to save ie fields only at 00Z on 1st, 11th and 21st of each month
ieslct=off

# Save instantaneous data on constant pressure levels to ip file
ipsave=off

# Tailored saving of temperature tendencies in _td file
# That breaks shit so lets not
#tprhsc_chem=on
tprhsc_chem=off

# Is chemistry calculated on all model levels? This switch is used 'fill in'
# chemical fields like ozone from advected fields when chemistry is not
# calculated on all levels
trchem=on

# Save zonal-time mean data to xp file
ztsave=on
