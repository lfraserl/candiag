#!/bin/bash
#   
#  
#                  betdata             FM - Sep 06/17 ------------------ betdata
#   ---------------------------------- Transfers back-end data.
#   ---------------------------------- Files can be transfered from one of the
#                                      back-end XC40 into sitestore and saved 
#                                      on the PPP front-end in the same hall.
#                                      Note: This deck is intended to be run
#                                            in xfer batch queue on the XC40 
#                                            I/O node.
#                                            Default RUNPATH saving subdirectory 
#                                            on PPP can be altered via 
#                                            DESTSSPATH environment variable.
#

#   ---------------------------------- Allow deletion of transfered files

    if [ "$delete_files" = on ] ; then
       delete_files='yes'
    else
       unset delete_files
    fi
    
#   ---------------------------------- setup for data transfer from target
#                                      platform, create local temporary 
#                                      subdirectory and define needed 
#                                      functions.

    invoker_deck='betdata'
    Cwd=$(pwd)
    \cp $(which tfrstup) tfrstup
    chmod u+w tfrstup
    PATH=$(clnstrng ".:${Cwd}:${PATH}" ':')
    export PATH
    hash -r
    set +e
    source tfrstup

#   ---------------------------------- obtain the specified files ...

    loopline='putfile pdn=\${file$counter}'
    source looper.cdk                                                                

#   ---------------------------------- Copy back-end files into target
#                                      sitestore subdirectory.

    be2fecp

#   ---------------------------------- Save files in sitestore subdirectory
#                                      on PPP front-end.

    fesave
 
#   ---------------------------------- Cleanup sitestore subdirectory and if
#                                      requested delete the back-end files.

    becleanup
