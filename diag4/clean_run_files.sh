#!/bin/bash
# Clean up script to get rid of necessary files from the post processing machines
#
#   This script relies on multiple functions in CanESM_shell_functions.sh, and a few other
#       tools within CCCma_tools (i.e. make_file_name_list/data_file_list)

#~~~~~~~~~~~~~~~
# history files
#~~~~~~~~~~~~~~~
if (( with_delhistFE == 1 )); then
    # build list of files to delete

    # -- determine inputs to make_file_name_list
    export suffix_list=${mdeleteHIST_suffix_list}
    export prefix_list=${uxxx}
    export runid
    make_file_list_args="--nomulti_list"
    if [[ $mdeleteHIST_leave_last_mon == "on" ]]; then
        # add a month offset so the last files are left
        make_file_list_args="${make_file_list_args} --mon_offset=-1"
    fi
    start_arg=$(printf "%04d:%02d" $job_start_year $job_start_month)
    stop_arg=$(printf "%04d:%02d" $job_stop_year $job_stop_month)
    tmp_file_list=tmp_hist_file_list

    # -- create list and add filenames to a variable
    make_file_name_list $make_file_list_args --start=$start_arg --stop=$stop_arg $tmp_file_list ||
            bail "Failed to make $tmp_file_list!"
    filelist=$(get_file_entries $tmp_file_list)

    # delete files
    delete_database_files $filelist
fi

#~~~~~~~~~~~~~~~
# restart files
#~~~~~~~~~~~~~~~
if (( with_delrsFE == 1 )); then
    # build list of files to delete
    export suffix_list=${mdeleteRS_suffix_list}
    export prefix_list=${uxxx}
    export runid
    make_file_list_args="--nomulti_list"
    if [[ $mdeleteRS_leave_last_mon == "on" ]]; then
        # add a month offset so the last files are left
        make_file_list_args="${make_file_list_args} --mon_offset=-1"
    fi
    start_arg=$(printf "%04d:%02d" $job_start_year $job_start_month)
    stop_arg=$(printf "%04d:%02d" $job_stop_year $job_stop_month)
    tmp_file_list=tmp_rs_file_list

    # -- create list and add filenames to a variable
    make_file_name_list $make_file_list_args --start=$start_arg --stop=$stop_arg $tmp_file_list ||
            bail "Failed to make $tmp_file_list!"
    filelist=$(get_file_entries $tmp_file_list)

    # delete files
    delete_database_files $filelist
fi

#~~~~~~~~~~~~~~~~~~
# diagnostic files
#~~~~~~~~~~~~~~~~~~
if (( with_deldiag == 1 )); then
    # build list of files to delete
    export suffix_list=${mdeleteDIAG_suffix_list}
    export prefix_list=${mdeleteDIAG_prefix_list}
    export runid
    make_file_list_args="--nomulti_list"
    if [[ $mdeleteDIAG_leave_last_mon == "on" ]]; then
        # add a month offset so the last files are left
        make_file_list_args="${make_file_list_args} --mon_offset=-1"
    fi
    start_arg=$(printf "%04d:%02d" $job_start_year $job_start_month)
    stop_arg=$(printf "%04d:%02d" $job_stop_year $job_stop_month)
    tmp_file_list=tmp_rs_file_list

    # -- create list and add filenames to a variable
    make_file_name_list $make_file_list_args --start=$start_arg --stop=$stop_arg $tmp_file_list ||
            bail "Failed to make $tmp_file_list!"
    filelist=$(get_file_entries $tmp_file_list)

    # delete files
    delete_database_files $filelist
fi

#~~~~~~~~~~~~~~~~~~~
# time series files
#~~~~~~~~~~~~~~~~~~~
if (( with_del_tser == 1 )); then
    # build list of files to delete
    prefix=${tser_uxxx:-sc}
    data_dir=$RUNPATH
    start_arg=$(printf "%04d:%02d" $job_start_year $job_start_month)
    stop_arg=$(printf "%04d:%02d" $job_stop_year $job_stop_month)
    data_file_lists $runid $data_dir --start=$start_arg --stop=$stop_arg ||
            bail "Failed to make data file lists!"

    # delete files
    delete_database_files -f ${prefix}_time_series_files
fi

#~~~~~~~~~~~~~~
# netcdf files
#~~~~~~~~~~~~~~
if (( with_del_nc == 1 )); then
    # get list of files we want to delete
    netcdf_dir="${RUNPATH}/nc_output"
    filelist=$( get_list_of_cmorized_netcdf_files -c -f data_directory=${netcdf_dir} chunk_start_date=${job_start_date} chunk_stop_date=${job_stop_date} )

    # remove entries from list if user wants to keep some files on disk
    if is_defined $keep_vartab_pairs; then
        # create regex pattern to identify files to keep
        for vartab in $keep_vartab_pairs; do
            file_prefix=${vartab/:/_}
            PATTERN="${PATTERN} \S*${file_prefix}\S*" # '\S' is non-white space characters
        done
        PATTERN=$(echo $PATTERN | sed 's/^ *//') # remove leading whitespace
        PATTERN=${PATTERN//\* \\/\*\|\\}         # add 'OR' syntax instead of spaces

        # determine what files to keep and remove them from the file list
        files_to_keep=$( echo $filelist | grep -Po "$PATTERN" )
        for fl in $files_to_keep; do
            filelist=$(echo $filelist | sed "s#${fl}##" )
        done
    fi

    # delete files then clean empty directories
    rm -f $filelist
    find $netcdf_dir -type d -empty -delete
fi
