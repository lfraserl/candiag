#!/bin/sh
#
# This script is only included in the merged_diag_script grouping when
# gas-phase chemistry is run in MAM mode. BW, June 2023
#
# Amendments to the original: source the file of switches
#  DESCRIPTION
#
#  Revised version of standard DENSITY deck. Compute density and
#  pressure at eta/sigma surface. SK: inlcude numberdensity.dk
# 
#  PREREQUISITES
#
#  PARMSUB PARAMETERS
#
#    -- flabel, pmin, pmax, t1, t2, t3, t4.
#
#  CHANGELOG
#
#    2023-01-09: Converted to shell script
#    2019-11-01: Combined density_chem_diag (calculation of number
#                 density on model levels) and diag_chem_nd (monthly
#                 averaging and saving into diagnostic files)
#    2012-10-25: Removed redundent saving of _gerho - assuming _gerho
#                 was created with correct frequency (D. Plummer)
#    2007-11-19: Added proper description header (A.Jonsson)
#    2007-11-15: Added subversion date ('Date') and revision ('Rev')
#                keyword substitutions to all decks (A.Jonsson)
#    2007-11-13: Added 'cat Input_cards' at the end of most decks
#                (A.Jonsson)
#    2007-11-??: $chem flag added (A.Jonsson)
#    2007-11-??: only save gerho; all code after gerho save is commented
#                out (A.Jonsson)
#    2007-02-??: Use lower packing density (A.Jonsson)
# -----------------------------------------------------------------------------

# Source the list of on/off switches
source ${CANESM_SRC_ROOT}/CanDIAG/diag4/gaschem_diag_switches.sh

#
#  ----  access model history files
#
.   spfiles.cdk
#
#  ---- get the surface pressure field
#
    access gslnsp ${flabel}_gslnsp na
    if [ ! -s gslnsp ] ; then
      if [ "$datatype" = "gridsig" ] ; then
.       ggfiles.cdk
        echo "SELECT    STEPS $t1 $t2 $t3 LEVS    1    1 NAME LNSP" |
           ccc select npakgg gslnsp
        rm npakgg
      else
        echo "SELECT    STEPS $t1 $t2 $t3 LEVS    1    1 NAME LNSP" |
           ccc select npaksp sslnsp
        echo "COFAGG    $lon$lat" | ccc cofagg sslnsp gslnsp
      fi
    fi
#
# SK: use rho precomputed in pressure_density.dk
    access gerho ${flabel}_gerho
#
# SK: included here from numberdensity.dk
#
# (B) CALCULATE NUMBER DENSITY FROM AIR DENSITY
#
#     nd_air    = rho * Na/Ma
#     nd_tracer = vmr_tracer * nd_air
#   
#     nd  - number density [molecules/m3]
#     rho - mass density [kg/m3]
#     Na  - Avogadro constant [molecules/mol]
#     Ma  - molar mass [kg/mol]
#
#     Avogadro constant & air molecular mass
    echo "XLIN......        0.  6.022E23" | ccc xlin gerho Na
    echo "XLIN......        0.  28.97E-3" | ccc xlin gerho Ma
#     Combined constants
    div Na  Ma  NaMa
#     Air number density
    mlt gerho NaMa nd1
#     Units converted from [molecules/m3] to [molecules/cm3]
    echo "XLIN......      1E-6        0.   ND" | ccc xlin nd1 nd_ge
    rm gerho Na Ma NaMa nd1
#
# (C) INTERPOLATE ONTO CONSTANT PRESSURE SURFACES
#
    echo "GSAPL.    $plv       0.0       0.0$coord$plid
$p01$p02$p03$p04$p05$p06$p07$p08$p09$p10$p11$p12$p13$p14$p15$p16
$p17$p18$p19$p20$p21$p22$p23$p24$p25$p26$p27$p28$p29$p30$p31$p32
$p33$p34$p35$p36$p37$p38$p39$p40$p41$p42$p43$p44$p45$p46$p47$p48
$p49$p50$p51$p52$p53$p54$p55$p56$p57$p58$p59$p60$p61$p62$p63$p64
$p65$p66$p67$p68$p69$p70$p71$p72$p73$p74$p75$p76$p77$p78$p79$p80
$p81$p82$p83$p84$p85$p86$p87$p88$p89$p90$p91$p92$p93$p94$p95$p96
$p97$p98$p99$p100" | ccc gsapl nd_ge gslnsp nd_gp
#
# (D) CREATE MONTHLY AND ZONAL AVERAGES
#
    timavg nd_gp nd_ptav
#
    if [ "$gesave" = on ] ; then
      timavg nd_ge nd_etav
    fi
#
    if [ "$ztsave" = on ] ; then
      zonavg nd_ptav nd_ztav
    fi
#
# (E) SAVE INSTANTANEOUS FIELDS
#
#     ip - instantaneous data on constant pressure surfaces
#
    if [ "$ipsave" = on ] ; then
      access oldip ${flabel}ip      na
      echo "XSAVE.        NUMBER DENSITY - 1 OVER CM3
NEWNAM." | ccc xsave oldip nd_gp newip
      save   newip ${flabel}ip
      delete oldip                  na
    fi
#
# (F) SAVE AVERAGED FIELDS
#
#     xp - time and zonal averaged data on pressure levels
#     gp - time averaged data on pressure levels
#     ge - time averaged data on model eta surfaces
#
    if [ "$ztsave" = on ] ; then
      access oldxp ${flabel}xp      na
      echo "XSAVE.        NUMBER DENSITY - 1 OVER CM3
NEWNAM." | ccc xsave oldxp nd_ztav newxp
      save   newxp ${flabel}xp
      delete oldxp                  na
    fi
#
    access oldgp ${flabel}gp      na
    echo "XSAVE.        NUMBER DENSITY - 1 OVER CM3
NEWNAM." | ccc xsave oldgp nd_ptav newgp
    save   newgp ${flabel}gp
    delete oldgp                  na
    rm nd_ptav newgp
#
    if [ "$gesave" = "on" ] ; then
      access oldge ${flabel}ge      na
      echo "XSAVE.        NUMBER DENSITY - 1 OVER CM3
NEWNAM." | ccc xsave oldge nd_etav newge
      save   newge ${flabel}ge
      delete oldge                  na
      rm nd_etav newge
    fi
#
    save nd_ge ${flabel}_gend
    rm nd_gp nd_ge
