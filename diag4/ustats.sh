#!/bin/sh
#   sk: re-write for new timavg/xsave, phase out xtrans.
#
#                  ustats              gjb,dl. jan 24/01 - sk.
#   ---------------------------------- calculate u statistics
#                                      and covariances with v and w.
#

      access u ${flabel}_gpu
      access v ${flabel}_gpv

      if [ "$datatype" = "specsig" ]; then
	echo "DIFF          U $t1 $t2 $delt" | ccc dif u dudt
      else
	echo "DIFF          U $t1 $t2 $delt       1.0" | ccc dif u dudt
      fi
      timavg u  tu up  tup2
      timavg v  tv vp
      timcov up vp tupvp

echo "XSAVE.        U
NEWNAM.
XSAVE.        DU/DT
NEWNAM.      U.
XSAVE.        U\"U\"
NEWNAM.    U\"U\"
XSAVE.        U\"V\"
NEWNAM.    U\"V\"" > ic.xsave
      vars="tu dudt tup2 tupvp"

      if [ "$wxstats" = on ]; then
#   ---------------------------------- do calculations for w
         access w  ${flabel}_gpw
         timavg w  tw wp
         timcov up wp tupwp
echo "XSAVE.        U\"W\"
NEWNAM.    U\"W\"" >> ic.xsave
	 vars="$vars tupwp"
      fi
#   ---------------------------------- xsave
      xsave new_gp $vars new.
      mv new. new_gp

#   ---------------------------------- save results.
      release oldgp
      access  oldgp ${flabel}gp
      xjoin   oldgp new_gp newgp
      save    newgp ${flabel}gp
      delete  oldgp
