#!/bin/sh
#
# This script is only included in the merged_diag_script grouping when
# gas-phase chemistry is run in MAM mode. BW, June 2023
#
# Amendments to original: hard-code dy1, dy2, dy3
#                         hard-code deltmin and yearoff
#                         source the file of switches
#
#  DESCRIPTION (original name: diag_chem_Chlorine_961c.sh)
# 
#  Create monthly averages and monthly zonal averages for chlorine
#  species: Cl, ClO, OClO, Cl2O2, Cl2, HOCl, HCl, ClONO2, CFCl3
#  (CFC-11), CF2Cl2 (CFC-12), CCl4, CH3CCl3, CHF2Cl (HCFC-22) and
#  CH3Cl and the advected family C9 (Cl + ClO + OClO + 2*(Cl2O2 + Cl2)
#  + HOCl + ClONO2); and diagnosed families: ClOx (Cl + ClO + OClO +
#  2*(Cl2O2 + Cl2) + HOCl + BrCl), Cly (Cl + ClO + OClO + 2*(Cl2O2 +
#  Cl2) + HOCl + HCl + ClONO2 + BrCl) and Clz (Cly + 3*CFCl3 +
#  2*CF2Cl2 + 4*CCl4 + 3*CH3CCl3 + CHF2Cl + CH3Cl).
#
#  Note that Cly = C9 + HCl + BrCl; and ClOx = Cly - HCl - ClONO2.
#
#  Note that BrCl is included in ClOx, Cly and Clz but is not
#  diagnosed separately (see the Bromine deck).
#
#  Zonal averages include daytime and nighttime masked values for Cl
#  and ClO.
#
#  PARMSUB PARAMETERS
#
#    -- chem, coord, days, dnsave, flabel, lat, lon, memory1, model1,
#       npg, plid, plv, plv2, pmax, pmaxc, pmin, run, stime, t1, t2,
#       t3, t4, trchem, ztsave.
#    -- p01-p100 (pressure level set)
#
#  PREREQUISITES
#
#    -- GS and SS history files
#    -- DAYMASK deck (GPDAY data file, unaveraged)
#
#  CHANGELOG
#
#    2023-01-20: Converted to shell script and removed the logic to support
#                 running chemistry on a subset of model levels (D. Plummer)
#    2015-04-01: Modified to reflect ClONO2 is now an advected species
#    2014-06-06: Monthly-average output on model surfaces for CCMI and 
#                 harmonization of save options iesave, ipsave, gesave
#                 (D. Plummer)
#    2013-06-10: Added iesave option (Y. Jiao)
#    2011-11-29: Modified xsave labels to be friendlier when converted to
#                timeseries filenames by spltdiag (D. Plummer)
#    2007-07-01: Added saving of instantaneous (un-averaged) Cly to ie
#                file (A.Jonsson)
#    2008-06-30: Changed output names for diagnostic families: NY to
#                NOY; CY to CLY; BY to BRY; to avoid confusion with
#                new simulated advected families (NY, CY, BY). For
#                consistency CZ was changed to CLZ and BZ to BRZ
#                (A.Jonsson)
#    2008-06-27: Added four new CFCs (CCL4, CH3CCL3, CHF2Cl and
#                CH3CL); Revised super labels for CFC-11 and CFC-12 to
#                CFCL3 and CF2CL2, respectively; Upgraded Clz for new
#                CFCs; Modified super label for Clz to spell out
#                components; Shifted HCl from gs to ss file and
#                removed HCl from C9 family (A.Jonsson)
#    2008-06-25: Modified to handle case with chemistry calculated on
#                all model levels (trchem=on); Added diagnostics for
#                ClOx family; Created new total chlorine estimate
#                including BrCl (Cly = C9 + BrCl); Changed order of
#                species to output files to better reflect grouping
#                within families and to separate between chemistry and
#                full domain species; Changed super labels for
#                families (C9, ClOx, Cly) to show all components
#                (A.Jonsson)
#    2007-12-17: Moved call to access gpday to within $dnsave if-statement
#                (A.Jonsson)
#    2007-11-23: Capitalized all super labels (affecting H20(g),
#                H2O(s), HNO3(g), HNO3(s) and labels containing Br,
#                Cl, x, y or z) (A.Jonsson)
#    2007-11-18: Bug fix: Added $dnsave if-statement for daymask
#                select input card (A.Jonsson)
#    2007-11-15: Added subversion date ('Date') and revision ('Rev')
#                keyword substitutions to all decks (A.Jonsson)
#    2007-11-15: Removed BrCl from chlorine decks (A.Jonsson)
#    2007-11-15: Added parmsub parameter $dnsave to control day/night
#                time zonal averaging (A.Jonsson)
#    2007-11-13: Revised $ztsave if-statement to only encompass xp
#                file data (A.Jonsson)
#    2007-11-13: Added 'cat Input_cards' at the end of most decks
#                (A.Jonsson)
#    2007-11-12: Bug fix: Switched upper limit from $pmin to $p01
#                in input card for daymask selection (A.Jonsson)
#    2007-11-05: Added Chlorine deck (A.Jonsson)
#    2007-11-??: Based on the Clz deck (A.Jonsson)
# -----------------------------------------------------------------------------

# Source the list of on/off switches
source ${CANESM_SRC_ROOT}/CanDIAG/diag4/gaschem_diag_switches.sh

# Define variables in lieu of reading them off a jobfile
dy1="000000000"; dy2="999999999"; dy3="0192";
deltmin=7.500
yearoff=1950
#
#  ----  access model history files
#
.     spfiles.cdk
.     ggfiles.cdk
#
#  ----  get the surface pressure field
#
    access gslnsp ${flabel}_gslnsp na
    if [ ! -s gslnsp ] ; then
      if [ "$datatype" = "gridsig" ] ; then
        echo "SELECT    STEPS $t1 $t2 $t3 LEVS    1    1 NAME LNSP" |
           ccc select npakgg gslnsp
      else
        echo "SELECT    STEPS $t1 $t2 $t3 LEVS    1    1 NAME LNSP" |
           ccc select npaksp sslnsp
        echo "COFAGG.   $lon$lat" | ccc cofagg sslnsp gslnsp
        rm sslnsp
      fi
    fi
#
#  ----  select out required fields from model history
#
    echo "SELECT    STEPS $t1 $t2 $t3 LEVS-9001 1000 NAME   C1   C3   C2   C5
SELECT       OC   C6   BC" | ccc select npakgg Cl_ge ClO_ge Cl2_ge Cl2O2_ge \
                                     OClO_ge HOCl_ge BrCl_ge
#
    echo "SELECT    STEPS $t1 $t2 $t3 LEVS-9001 1000 NAME   C9   C7   C4   F1
SELECT       F2   F3   F4   F5   F6" | ccc select npaksp C9_se HCl_se ClONO2_se \
                            CFC11_se CFC12_se CCl4_se CH3CCl3_se HCFC22_se CH3Cl_se
#
    echo "COFAGG.   $lon$lat    0$npg" > .cofagg_input_card
    cofagg C9_se      C9_ge input=.cofagg_input_card
    cofagg HCl_se     HCl_ge input=.cofagg_input_card
    cofagg ClONO2_se  ClONO2_ge input=.cofagg_input_card
    cofagg CFC11_se   CFC11_ge input=.cofagg_input_card
    cofagg CFC12_se   CFC12_ge input=.cofagg_input_card
    cofagg CCl4_se    CCl4_ge input=.cofagg_input_card
    cofagg CH3CCl3_se CH3CCl3_ge input=.cofagg_input_card
    cofagg HCFC22_se  HCFC22_ge input=.cofagg_input_card
    cofagg CH3Cl_se   CH3Cl_ge input=.cofagg_input_card
#
    rm *_se npakgg npaksp
#
#  ----  create new variables
#
# --- Create total inorganic chlorine family (Cly = C9 + ClONO2 + HCl + BrCl)
#
#     Note that C9, ClONO2 and HCl are defined on the full domain, while
#     BrCl is defined on the chemistry sub-domain.
#
    add C9_ge HCl_ge aw
    add aw    ClONO2_ge bw
    add bw BrCl_ge cw
    echo "NEWNAM.     CLY" | ccc newnam cw Cly_ge
#
    rm aw bw cw
#
# --- Create ClOx family (Cl + ClO + OClO + 2*(Cl2O2 + Cl2) + HOCl +
#     BrCl)
    echo "XLIN......       2.0       0.0" | ccc xlin Cl2_ge   2Cl2_ge
    echo "XLIN......       2.0       0.0" | ccc xlin Cl2O2_ge 2Cl2O2_ge
    add    Cl_ge  ClO_ge    aw
    add    aw     OClO_ge   bw
    add    bw     2Cl2_ge   cw
    add    cw     2Cl2O2_ge dw
    add    dw     HOCl_ge   ew
    add    ew     BrCl_ge   fw
    echo "NEWNAM.    CLOX" | ccc newnam fw ClOx_ge
#
    rm 2Cl2_ge 2Cl2O2_ge aw bw cw dw ew fw
#
# --- Create total chlorine family (Clz)
#
    echo "XLIN......       3.0       0.0" | ccc xlin CFC11_ge   3CFC11_ge
    echo "XLIN......       2.0       0.0" | ccc xlin CFC12_ge   2CFC12_ge
    echo "XLIN......       4.0       0.0" | ccc xlin CCl4_ge    4CCl4_ge
    echo "XLIN......       3.0       0.0" | ccc xlin CH3CCl3_ge 3CH3CCl3_ge
    add  3CFC11_ge  2CFC12_ge   aw
    add  aw         4CCl4_ge    bw
    add  bw         3CH3CCl3_ge cw
    add  cw         HCFC22_ge   dw
    add  dw         CH3Cl_ge    ew
    add  ew         Cly_ge      Clz_ge
#
    rm 3CFC11_ge 2CFC12_ge 4CCl4_ge 3CH3CCl3_ge
    rm aw bw cw dw ew
#
#  ----  interpolate onto constant pressure surfaces
#
    echo "GSAPL.    $plv       0.0       0.0$coord$plid
$p01$p02$p03$p04$p05$p06$p07$p08$p09$p10$p11$p12$p13$p14$p15$p16
$p17$p18$p19$p20$p21$p22$p23$p24$p25$p26$p27$p28$p29$p30$p31$p32
$p33$p34$p35$p36$p37$p38$p39$p40$p41$p42$p43$p44$p45$p46$p47$p48
$p49$p50$p51$p52$p53$p54$p55$p56$p57$p58$p59$p60$p61$p62$p63$p64
$p65$p66$p67$p68$p69$p70$p71$p72$p73$p74$p75$p76$p77$p78$p79$p80
$p81$p82$p83$p84$p85$p86$p87$p88$p89$p90$p91$p92$p93$p94$p95$p96
$p97$p98$p99$p100" > .gsapl_input_card
#
    gsapl Cl_ge      gslnsp Cl_gp input=.gsapl_input_card
    gsapl ClO_ge     gslnsp ClO_gp input=.gsapl_input_card
    gsapl Cl2_ge     gslnsp Cl2_gp input=.gsapl_input_card
    gsapl Cl2O2_ge   gslnsp Cl2O2_gp input=.gsapl_input_card
    gsapl OClO_ge    gslnsp OClO_gp input=.gsapl_input_card
    gsapl HOCl_ge    gslnsp HOCl_gp input=.gsapl_input_card
    gsapl ClOx_ge    gslnsp ClOx_gp input=.gsapl_input_card
    gsapl ClONO2_ge  gslnsp ClONO2_gp input=.gsapl_input_card
    gsapl C9_ge      gslnsp C9_gp input=.gsapl_input_card
    gsapl HCl_ge     gslnsp HCl_gp input=.gsapl_input_card
    gsapl Cly_ge     gslnsp Cly_gp input=.gsapl_input_card
    gsapl CFC11_ge   gslnsp CFC11_gp input=.gsapl_input_card
    gsapl CFC12_ge   gslnsp CFC12_gp input=.gsapl_input_card
    gsapl CCl4_ge    gslnsp CCl4_gp input=.gsapl_input_card
    gsapl CH3CCl3_ge gslnsp CH3CCl3_gp input=.gsapl_input_card
    gsapl HCFC22_ge  gslnsp HCFC22_gp input=.gsapl_input_card
    gsapl CH3Cl_ge   gslnsp CH3Cl_gp input=.gsapl_input_card
    gsapl Clz_ge     gslnsp Clz_gp input=.gsapl_input_card
#
#  ---- create monthly and zonal averages
#
# -- monthly averages
    timavg Cl_gp      Cl_tav
    timavg ClO_gp     ClO_tav
    timavg Cl2_gp     Cl2_tav
    timavg Cl2O2_gp   Cl2O2_tav
    timavg OClO_gp    OClO_tav
    timavg HOCl_gp    HOCl_tav
    timavg ClOx_gp    ClOx_tav
    timavg ClONO2_gp  ClONO2_tav
    timavg C9_gp      C9_tav
    timavg HCl_gp     HCl_tav
    timavg Cly_gp     Cly_tav
    timavg CFC11_gp   CFC11_tav
    timavg CFC12_gp   CFC12_tav
    timavg CCl4_gp    CCl4_tav
    timavg CH3CCl3_gp CH3CCl3_tav
    timavg HCFC22_gp  HCFC22_tav
    timavg CH3Cl_gp   CH3Cl_tav
    timavg Clz_gp     Clz_tav
#
    if [ "$gesave" = on ] ; then
      timavg Cl_ge      Cl_etav
      timavg ClO_ge     ClO_etav
      timavg Cl2_ge     Cl2_etav
      timavg Cl2O2_ge   Cl2O2_etav
      timavg OClO_ge    OClO_etav
      timavg HOCl_ge    HOCl_etav
      timavg ClOx_ge    ClOx_etav
      timavg ClONO2_ge  ClONO2_etav
      timavg C9_ge      C9_etav
      timavg HCl_ge     HCl_etav
      timavg Cly_ge     Cly_etav
      timavg CFC11_ge   CFC11_etav
      timavg CFC12_ge   CFC12_etav
      timavg CCl4_ge    CCl4_etav
      timavg CH3CCl3_ge CH3CCl3_etav
      timavg HCFC22_ge  HCFC22_etav
      timavg CH3Cl_ge   CH3Cl_etav
      timavg Clz_ge     Clz_etav
    fi
#
# -- monthly zonal averages
    if [ "$ztsave" = on ] ; then
      zonavg Cl_tav      Cl_ztav
      zonavg ClO_tav     ClO_ztav
      zonavg Cl2_tav     Cl2_ztav
      zonavg Cl2O2_tav   Cl2O2_ztav
      zonavg OClO_tav    OClO_ztav
      zonavg HOCl_tav    HOCl_ztav
      zonavg ClOx_tav    ClOx_ztav
      zonavg ClONO2_tav  ClONO2_ztav
      zonavg C9_tav      C9_ztav
      zonavg HCl_tav     HCl_ztav
      zonavg Cly_tav     Cly_ztav
      zonavg CFC11_tav   CFC11_ztav
      zonavg CFC12_tav   CFC12_ztav
      zonavg CCl4_tav    CCl4_ztav
      zonavg CH3CCl3_tav CH3CCl3_ztav
      zonavg HCFC22_tav  HCFC22_ztav
      zonavg CH3Cl_tav   CH3Cl_ztav
      zonavg Clz_tav     Clz_ztav
    fi
#
# -- apply day/night mask to a subset of species on the chemistry
#    sub-domain and calculate day-time and night-time zonal averages
    if [ "$dnsave" = on ] ; then
      access day_gp ${flabel}_gpday
      echo "SELECT    STEPS $t1 $t2 $t3 LEVS-9001 1000 NAME DMSK" |
          ccc     select   day_gp day
      rzonavg3 Cl_gp  day Cl_z  Cl_d  Cl_n
      rzonavg3 ClO_gp day ClO_z ClO_d ClO_n
#
      timavgmsk Cl_d Cl_dztav
      timavgmsk Cl_n Cl_nztav
#
      timavgmsk ClO_d ClO_dztav
      timavgmsk ClO_n ClO_nztav
#
      rm day* *_z *_d *_n
    fi
#
#  ---- save instantaneous fields
#
#     ie - instantaneous data on model levels
#     ip - instantaneous data on constant pressure surfaces
#     ix - instanteneous zonal average cross-sections
#
    if [ "$iesave" = "on" ] ; then
      echo "XSAVE.        CL
NEWNAM.
XSAVE.        CL2
NEWNAM.
XSAVE.        CLO
NEWNAM.
XSAVE.        CL2O2
NEWNAM.
XSAVE.        OCLO
NEWNAM.
XSAVE.        HOCL
NEWNAM.
XSAVE.        CLONO2
NEWNAM.
XSAVE.        HCL
NEWNAM.
XSAVE.        CLY - CL CLO OCLO 2XCL2O2 2XCL2 HOCL HCL CLONO2 BRCL
NEWNAM.
XSAVE.        CFCL3
NEWNAM.
XSAVE.        CF2CL2
NEWNAM.
XSAVE.        CCL4
NEWNAM.
XSAVE.        CH3CCL3
NEWNAM.
XSAVE.        CHF2CL
NEWNAM.
XSAVE.        CH3CL
NEWNAM." > .xsave_input_card

      if [ "$ieslct" = "on" ] ; then
        echo "C*TSTEP   $deltmin     ${yearoff}010100          MODEL     YYYYMMDDHH" > ic.tstep_model
        selstep      Cl_ge      Cl_ie input=ic.tstep_model
        selstep     Cl2_ge     Cl2_ie input=ic.tstep_model
        selstep     ClO_ge     ClO_ie input=ic.tstep_model
        selstep   Cl2O2_ge   Cl2O2_ie input=ic.tstep_model
        selstep    OClO_ge    OClO_ie input=ic.tstep_model
        selstep    HOCl_ge    HOCl_ie input=ic.tstep_model
        selstep  ClONO2_ge  ClONO2_ie input=ic.tstep_model
        selstep     HCl_ge     HCl_ie input=ic.tstep_model
        selstep     Cly_ge     Cly_ie input=ic.tstep_model
        selstep   CFC11_ge   CFC11_ie input=ic.tstep_model
        selstep   CFC12_ge   CFC12_ie input=ic.tstep_model
        selstep    CCl4_ge    CCl4_ie input=ic.tstep_model
        selstep CH3CCl3_ge CH3CCl3_ie input=ic.tstep_model
        selstep  HCFC22_ge  HCFC22_ie input=ic.tstep_model
        selstep   CH3Cl_ge   CH3Cl_ie input=ic.tstep_model
#
        release oldie
        access oldie ${flabel}ie      na
        xsave oldie Cl_ie Cl2_ie ClO_ie Cl2O2_ie OClO_ie HOCl_ie ClONO2_ie \
                      HCl_ie Cly_ie CFC11_ie CFC12_ie CCl4_ie CH3CCl3_ie \
                      HCFC22_ie CH3Cl_ie newie input=.xsave_input_card
        save newie ${flabel}ie
        delete oldie                  na
        rm *_ie
      fi
#
      if [ "$ieslct" = "off" ] ; then
        release oldie
        access oldie ${flabel}ie      na
        xsave oldie Cl_ge Cl2_ge ClO_ge Cl2O2_ge OClO_ge HOCl_ge ClONO2_ge \
                      HCl_ge Cly_ge CFC11_ge CFC12_ge CCl4_ge CH3CCl3_ge \
                      HCFC22_ge CH3Cl_ge newie input=.xsave_input_card
        save newie ${flabel}ie
        delete oldie                  na
      fi
      rm .xsave_input_card
    fi
#
    if [ "$ipsave" = on ] ; then
      release oldip
      access oldip ${flabel}ip      na
      echo "XSAVE.        CL
NEWNAM.
XSAVE.        CLO
NEWNAM.
XSAVE.        CL2O2
NEWNAM.
XSAVE.        OCLO
NEWNAM.
XSAVE.        HOCL
NEWNAM.
XSAVE.        CLONO2
NEWNAM.
XSAVE.        HCL
NEWNAM.
XSAVE.        CLY - CL CLO OCLO 2XCL2O2 2XCL2 HOCL HCL CLONO2 BRCL
NEWNAM.
XSAVE.        CFCL3
NEWNAM.
XSAVE.        CF2CL2
NEWNAM.
XSAVE.        CCL4
NEWNAM.
XSAVE.        CH3CCL3
NEWNAM.
XSAVE.        CHF2CL
NEWNAM.
XSAVE.        CH3CL
NEWNAM." | ccc xsave oldip Cl_gp ClO_gp Cl2O2_gp OClO_gp HOCl_gp ClONO2_gp \
                   HCl_gp Cly_gp CFC11_gp CFC12_gp CCl4_gp CH3CCl3_gp \
                   HCFC22_gp CH3Cl_gp newip
      save newip ${flabel}ip
      delete oldip                   na
    fi
#
    if [ "$dailysv" = on ] ; then
      echo "SELECT.   STEPS $dy1 $dy2 $dy3 LEVS-9001 1000 NAME   C3" |
         ccc select ClO_gp ClO_dly_gp
      zonavg ClO_dly_gp ClO_ixp
#
      release oldix
      access oldix ${flabel}ix      na
      echo "XSAVE.        INSTANTANEOUS CLO
NEWNAM." | ccc xsave oldix ClO_ixp newix
      save newix ${flabel}ix
      delete oldix                  na
    fi
#
    rm *_gp
    rm *_ge
#
#  ----  save time averaged fields
#
#     xp - time and zonal averaged data on pressure levels
#     gp - time averaged data on pressure levels
#     ge - time averaged data on model levels
#
    if [ "$ztsave" = on ] ; then
      echo "XSAVE.        CL
NEWNAM.
XSAVE.        CLO
NEWNAM.
XSAVE.        CL2
NEWNAM.
XSAVE.        CL2O2
NEWNAM.
XSAVE.        OCLO
NEWNAM.
XSAVE.        HOCL
NEWNAM.
XSAVE.        CLOX - CL CLO OCLO 2XCL2O2 2XCL2 HOCL BRCL
NEWNAM.
XSAVE.        CLONO2
NEWNAM.
XSAVE.        C9 - CL CLO OCLO 2XCL2O2 2XCL2 HOCL
NEWNAM.
XSAVE.        HCL
NEWNAM.
XSAVE.        CLY - CL CLO OCLO 2XCL2O2 2XCL2 HOCL HCL CLONO2 BRCL
NEWNAM.
XSAVE.        CFCL3
NEWNAM.
XSAVE.        CF2CL2
NEWNAM.
XSAVE.        CCL4
NEWNAM.
XSAVE.        CH3CCL3
NEWNAM.
XSAVE.        CHF2CL
NEWNAM.
XSAVE.        CH3CL
NEWNAM.
XSAVE.        CL_TOT - CLY 3XCFCL3 2XCF2CL2 4XCCL4 3XCH3CCL3 CHF2CL CH3CL
NEWNAM.     CLZ" | ccc xsave oldxp Cl_ztav ClO_ztav Cl2_ztav Cl2O2_ztav OClO_ztav \
                        HOCl_ztav ClOx_ztav ClONO2_ztav C9_ztav HCl_ztav Cly_ztav \
                        CFC11_ztav CFC12_ztav CCl4_ztav CH3CCl3_ztav HCFC22_ztav \
                        CH3Cl_ztav Clz_ztav newxp
    fi
#
    if [ "$dnsave" = on ] ; then
      echo "XSAVE.        CL DAY
NEWNAM.
XSAVE.        CLO DAY
NEWNAM.
XSAVE.        CL NIGHT
NEWNAM.
XSAVE.        CLO NIGHT
NEWNAM." | ccc xsave newxp Cl_dztav ClO_dztav Cl_nztav ClO_nztav newxp1
      mv newxp1 newxp
    fi
    release old_xp
    access old_xp ${flabel}xp     na
    xjoin  old_xp newxp new_xp
    save   new_xp ${flabel}xp
    delete old_xp                 na
#
    access oldgp ${flabel}gp      na
    echo "XSAVE.        CL
NEWNAM.
XSAVE.        CLO
NEWNAM.
XSAVE.        CL2
NEWNAM.
XSAVE.        CL2O2
NEWNAM.
XSAVE.        OCLO
NEWNAM.
XSAVE.        HOCL
NEWNAM.
XSAVE.        CLOX - CL CLO OCLO 2XCL2O2 2XCL2 HOCL BRCL
NEWNAM.
XSAVE.        CLONO2
NEWNAM.
XSAVE.        C9 - CL CLO OCLO 2XCL2O2 2XCL2 HOCL
NEWNAM.
XSAVE.        HCL
NEWNAM.
XSAVE.        CLY - CL CLO OCLO 2XCL2O2 2XCL2 HOCL HCL CLONO2 BRCL
NEWNAM.
XSAVE.        CFCL3
NEWNAM.
XSAVE.        CF2CL2
NEWNAM.
XSAVE.        CCL4
NEWNAM.
XSAVE.        CH3CCL3
NEWNAM.
XSAVE.        CHF2CL
NEWNAM.
XSAVE.        CH3CL
NEWNAM.
XSAVE.        CL_TOT - CLY 3XCFCL3 2XCF2CL2 4XCCL4 3XCH3CCL3 CHF2CL CH3CL
NEWNAM.     CLZ" | ccc xsave oldgp Cl_tav ClO_tav Cl2_tav Cl2O2_tav OClO_tav \
                        HOCl_tav ClOx_tav ClONO2_tav C9_tav HCl_tav Cly_tav \
                        CFC11_tav CFC12_tav CCl4_tav CH3CCl3_tav HCFC22_tav \
                        CH3Cl_tav Clz_tav newgp
    save newgp ${flabel}gp
    delete oldgp                  na
    rm new* *_tav
#
    if [ "$gesave" = on ] ; then      
      access oldge ${flabel}ge     na
      echo "XSAVE.        CL
NEWNAM.
XSAVE.        CLO
NEWNAM.
XSAVE.        CL2
NEWNAM.
XSAVE.        CL2O2
NEWNAM.
XSAVE.        OCLO
NEWNAM.
XSAVE.        HOCL
NEWNAM.
XSAVE.        CLOX - CL CLO OCLO 2XCL2O2 2XCL2 HOCL BRCL
NEWNAM.
XSAVE.        CLONO2
NEWNAM.
XSAVE.        HCL
NEWNAM.
XSAVE.        CLY - CL CLO OCLO 2XCL2O2 2XCL2 HOCL HCL CLONO2 BRCL
NEWNAM.
XSAVE.        CFCL3
NEWNAM.
XSAVE.        CF2CL2
NEWNAM.
XSAVE.        CCL4
NEWNAM.
XSAVE.        CH3CCL3
NEWNAM.
XSAVE.        CHF2CL
NEWNAM.
XSAVE.        CH3CL
NEWNAM.
XSAVE.        CL_TOT - CLY 3XCFCL3 2XCF2CL2 4XCCL4 3XCH3CCL3 CHF2CL CH3CL
NEWNAM.     CLZ" | ccc xsave oldge Cl_etav ClO_etav Cl2_etav Cl2O2_etav OClO_etav \
                        HOCl_etav ClOx_etav ClONO2_etav HCl_etav Cly_etav \
                        CFC11_etav CFC12_etav CCl4_etav CH3CCl3_etav HCFC22_etav \
                        CH3Cl_etav Clz_etav newge
      save newge ${flabel}ge
      delete oldge                 na
      rm newge *_etav
    fi
