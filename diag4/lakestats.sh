#!/bin/sh
#                                       ml - dec 06/17. ML. Only CSLM.
#                  lakestats            ml - nov 20/16 ----------------- lakestats
#
#   ---------------------------------- compute statistics on inland lake fields 
#                                      in gs file.
#
#  Common Sampled Input/Output variables:
#
#  LDMX : mixed-layer depth (m)
#  LRIM : resolved   lake ice mass (Kg/m2)
#  LRIN : resolved   lake ice fraction
#  LUIM : unresolved lake ice mass (Kg/m2)
#  LUIN : unresolved lake ice fraction
#  LZIC : ice depth (m)
#
#C if defined (cslm)
#
#  Prognostic (sampled) CSLM Input/Output variables:
#
#  DELU : current jump across bottom of mixed layer [m s-1]
#  DTMP : temperature jump across bottom of mixed layer [K]
#  EXPW : volume expansivity of water [K-1]
#  GRED : reduced gravity across bottom of mixed layer [m s-2]
#  RHOM : mean density of mixed layer [kg m-3]
#  T0LK : lake surface temperature [K]
#  TKEL : mean mixed layer TKE per unit mass [m2 s-2]
#  TLAK : lake level temperatures (1,NLAKMAX) (deg K)
#
#  Accumulated CSLM variables:
#
#  FLGL : aggregate net l/w flux down (W/m2)
#  FNL  : snow fraction (dimensionless)
#  FSGL : aggregate net s/w flux down (W/m2)
#  HFCL : aggregate internal energy change associated with conduction
#         or phase change (W/m2) 
#  HFLL : aggregate latent surface flux up (W/m2)
#  HFSL : aggregate sensible surface flux up (W/m2)
#  HMFL : aggregate energy associated with phase change (W/m2)
#  PIL  : aggregate incident precipitation (Kg/m2-s)
#  QFL  : aggregate water vapour flux from ground (Kg/m2-s)
#C endif

#  ---------------------------------- access gs files
 
.   ggfiles.cdk

#  ---------------------------------- Select variables
#                                     (the number of variables for one select call 
#                                      must not exceed 86)

# First, common variables:
#
    gsvars="LDMX LZIC LRIM LRIN LUIM LUIN"
    GSVARS=`fmtselname $gsvars`
    echo "C*SELECT  STEPS $t1 $t2 $t3 LEVS-9100 1000 NAME$GSVARS" | ccc select npakgg $gsvars

#  ----------------------------------- compute and save statistics.

    statsav LDMX LZIC LRIM LRIN LUIM LUIN \
            new_gp new_xp $stat2nd

#    if [ "$cslm" = 'on' ] ; then

    gsvars="DELU DTMP EXPW GRED RHOM T0LK TKEL \
            FLGL  FNL FSGL HFCL HFLL HFSL HMFL  PIL  QFL"
    GSVARS=`fmtselname $gsvars`
    echo "C*SELECT  STEPS $t1 $t2 $t3 LEVS-9100 1000 NAME$GSVARS" | ccc select npakgg $gsvars

    # attempt to select TLAK
    echo "C*SELECT  STEPS $t1 $t2 $t3 LEVS-9100 1000 NAME$GSVARS" | ccc select npakgg TLAK || true
    if [ -s TLAK ] ; then
      # TLAK is found
      gsvars="$gsvars TLAK"
    fi

#  ----------------------------------- compute and save statistics.

    statsav $gsvars new_gp new_xp $stat2nd

#    fi

#   ---------------------------------- save results.

      access oldgp ${flabel}gp
      xjoin oldgp new_gp newgp
      save newgp ${flabel}gp
      delete oldgp

      if [ "$rcm" != "on" ] ; then
        access oldxp ${flabel}xp
        xjoin oldxp new_xp newxp
        save newxp ${flabel}xp
        delete oldxp
      fi
