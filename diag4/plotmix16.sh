#!/bin/sh
#
#                  plotmix16           ml - Mar 03/15 - ml ----------- plotmix16
# ------------------------------------ Use FLND to get land mask
#                                      and SICN to get ice mask,
#                                      since GC no longer exists
#                                      in new model version.
#
#                                      Previous version plotmix15:
#                                      - superlabels adjusted for
#                                        new Ceres_2.6r dataset.
#                                      - based on plotmixj but
#                                        revise superlabels in
#                                        xfind's to work with
#                                        new CERES dataset.
#                                      - handle both cases with/
#                                        without CLDO.
#                                      - uses new UWISC liquid water path
#                                        dataset.
#                                      Previous version plotmixj:
#                                      - based on plotmixi, but adds
#                                        CLDO.
#                                      Previous version plotmixi:
#                                      - revise superlabels in
#                                        xfind's to work with
#                                        corrected CERES dataset.
#                                      Previous version plotmixh:
#                                      - GPCP added.
#                                      - ECMWF PMSL added.
#                                      - SSM/I Liquid water path added.
#                                      Previous version plotmixg:
#                                      - use new Ceres GEO (including PWAT).
#
# -------------------------------------------------------------------------

#
# --- Define ccg function to add global means to ggplot plots
#
ccg(){
  if [ "$datatype" = "gridsig" ] ; then
     avg=$(echo "C* GLOBAVL${shiftdy}         1"|ccc globavl $2 |grep "SURFACE MEAN IS"|head -1|awk '{printf "%10.3f\n",$11}')
  else
     globavg $2 _ gavg_$2
     avg=$(ggstat gavg_$2 | grep GRID | head -1 | cut -c73-85 | awk '{printf "%g", $1}' | tr -d ' ')
     rm gavg_$2
  fi
  echo "GGPLOT            $idx NEXT    $ILVL   0        1.   $FLO$HI$FINC${b}8" >ic.gplotf
  echo "$run $days $title $avg                                                   " >>ic.gplotf
  if [ "$colourplots" = on ] ; then
     cat ic.gplotf0 >>ic.gplotf
  fi
  ggplot $2 input=ic.gplotf
}


# --- Define a function to print out global average
printglb(){
  if [ "$datatype" = "gridsig" ] ; then
     glbline1=$(echo "C* GLOBAVL${shiftdy}         1"|ccc globavl $1 |head -1 |sed 's/,input=.ccc_cards//'| awk '{printf "%-30s",$2}')
     glbline2=$(echo "C* GLOBAVL${shiftdy}         1"|ccc globavl $1 |grep "SURFACE MEAN IS"|head -1| cut -c45-73)
  else
     glbline1=$(globavg $1 | head -1 | awk '{printf "%-30s",$2}')
     glbline2=$(globavg $1 | grep SURFACE | head -1 | cut -c45-73)
  fi
  echo "$glbline1$glbline2"
  echo "$glbline1$glbline2" >> $2
}


#   ---------------------------------- standard plots.
#
headframe=$(cat $(which headfram.cdk) |sed '1,2d')
echo "$headframe" > Input_Cards
echo "         DAYS      = $days     ">> Input_Cards
echo "         RUN       = $runid    ">> Input_Cards
echo "         FLABEL    = $flabel   ">> Input_Cards
echo "         OBSFILE   = $obsfile  ">> Input_Cards
echo "         OBSDAY    = $obsday   ">> Input_Cards

#    libncar plunit=$plunit
.   plotid.cdk
rm Input_Cards
#
#   Access model files (gp & xp)
#
    release gp xp era gpcp cmap ceres ceresm cerescld warrencld isccp uwisc uwiscm ssmi ssmim snow

    access gp ${flabel}gp
    if [ "$stat2nd" != "off" ] ; then
    access xp ${flabel}xp
    fi
#
#   Access various observation files (need to be merged into one file!)
#
    access era       ${erafile}gp    # ERA climatology file
    access gpcp      $obspcp_gpcp    # GPCP (precipitation) data
    access cmap      $obspcp_cmap    # CMAP (precipition) data (Xie&Arkin)
    access ceres     $obsceres       # Ceres climatology file
    access ceresm    $obsceres_msk   # Ceres climatology mask file
    access cerescld  $ceres_cld      # Ceres climatology cloud
    access warrencld $warren_cld     # Warren cloudiness
    access isccp     $obsisccp       # ISCCP (clouds) data
    access uwisc     $obsuwisc       # UWISC cloud liquid water path
    access uwiscm    $obsuwisc_msk   # UWISC mask
    access ssmi      $obsssmi        # SSMI data (mainly for liquid water)
    access ssmim     $obsssmi_msk    # SSMI mask
    access snow      $obssnow        # SNOW mass

#   ---------------------------------- print out datasets.
    rm -f dummy
    echo "DATATSETS"              >> dummy
    echo "MODEL=$flabel"          >> dummy
    echo "REANAL=$erafile"        >> dummy
    echo "GPCP=$obspcp_gpcp"      >> dummy
    echo "CMAP=$obspcp_cmap"      >> dummy
    echo "CERES=$obsceres"        >> dummy
    echo "CERES_CLD=$ceres_cld"   >> dummy
    echo "WARREN_CLD=$warren_cld" >> dummy
    echo "ISCCP=$obsisccp"        >> dummy
    echo "UWISC=$obsuwisc"        >> dummy
    echo "SSMI=$obsssmi"          >> dummy
    echo "SNOW=$obssnow"          >> dummy
    txtplot input=dummy
    rm dummy

#   ---------------------------------- interpolate to the model resolution

    nlon=$(echo $resol | cut -f1 -d'_' | $AWK '{printf "%5i", $1}')
    nlat=$(echo $resol | cut -f2 -d'_' | $AWK '{printf "%5i", $1}')

# select subset of ERA fields to accelerate interpolation
    echo 'XFIND       1 PMSL
XFIND       1 ST
XFIND       1 GT
XFIND       1 DSNO
XFIND       1 SRAD
XFIND       1 LRAD
XFIND       1 SH
XFIND       1 LH' | ccc xfind era subset
    mv subset era

    if [ "$datatype" = "gridsig" ] ; then
      #ryj: for interpolating obs to GU grid
      shiftdy=$(echo $nlat |awk '{printf "%10.5f", 90.0/$1}')
      mlat=`echo $resol | cut -f2 -d'_' | $AWK '{printf "%5i", -$1}'`
# interpolate ERA on unshifted LAT/LON grid to the target resolution (area-weighted)
      printf "LLAGG     $nlon$mlat    5                                       1\nC* LLAGG  ${shiftdy}        0.    0" | ccc llagg era era.1 ; mv era.1 era

# interpolate 5x5 degree datasets on shifted LAT/LON grid to the target resolution (area-weighted)
      for v in gpcp cmap isccp; do
      printf "LLAGG     $nlon$mlat   -5    0     1.E38      1.25      1.25    1\nC* LLAGG  ${shiftdy}        0.    0" | ccc llagg $v $v.1 ; mv $v.1 $v
      done

# interpolate 1x1 degree datasets on shifted LAT/LON grid to the target resolution (area-weighted)
      for v in ceres ceresm cerescld uwisc uwiscm ssmi ssmim snow ; do
      printf "LLAGG     $nlon$mlat   -5    0     1.E38       0.5       0.5    1\nC* LLAGG  ${shiftdy}        0.    0" | ccc llagg $v $v.1 ; mv $v.1 $v
      done

# interpolate Warren clouds 5x5 degree dataset on unshifted LAT/LON grid to the target resolution (area-weighted)
      for v in warrencld; do
      printf "LLAGG     $nlon$mlat    5                                       1\nC* LLAGG  ${shiftdy}        0.    0" | ccc llagg $v $v.1 ; mv $v.1 $v
      done

    else
# interpolate ERA on unshifted LAT/LON grid to the target resolution (area-weighted)
      echo "LLAGG     $nlon$nlat    5                                       1" | ccc llagg era era.1 ; mv era.1 era

# interpolate 5x5 degree datasets on shifted LAT/LON grid to the target resolution (area-weighted)
      for v in gpcp cmap isccp; do
      echo "LLAGG     $nlon$nlat   -5    0     1.E38      1.25      1.25    1" | ccc llagg $v $v.1 ; mv $v.1 $v
      done

# interpolate 1x1 degree datasets on shifted LAT/LON grid to the target resolution (area-weighted)
      for v in ceres ceresm cerescld uwisc uwiscm ssmi ssmim snow ; do
      echo "LLAGG     $nlon$nlat   -5    0     1.E38       0.5       0.5    1" | ccc llagg $v $v.1 ; mv $v.1 $v
      done

# interpolate Warren clouds 5x5 degree dataset on unshifted LAT/LON grid to the target resolution (area-weighted)
      for v in warrencld; do
      echo "LLAGG     $nlon$nlat    5                                       1" | ccc llagg $v $v.1 ; mv $v.1 $v
      done
    fi
#   ---------------------------------- basic surface plots.
#
#
#                                      land and ocean separate masks.
#                                      use flnd>0.5 for land and
#                                      sicn>0.15 for ice.
    echo "XFIND         DATA DESCRIPTION"|ccc xfind gp data_description
    echo "SELECT.                 0 999999999    1         0    1      FLND"|ccc select data_description flnd
    echo "XFIND         SICN "|ccc xfind gp sicn
    echo "FMASK                NEXT   GT       0.5  "|ccc fmask flnd ml
    echo "FMASK                NEXT   GT      0.15  "|ccc fmask sicn mi
    add ml mi mil
    echo "XLIN             -1.        1." |ccc xlin mil mo
    zonavg mo zmo
    zonavg mil zmil
    echo "  FPOW.          -1." |ccc fpow zmil izmil
    globavg mo gmo
    globavg mil gmil
    rm zmil mil ml mi
    rm sicn data_description
#
#   ---------------------------------- pmsl.
#
#                                      model data.
    echo "XFIND         PMSL" |ccc xfind gp tpmsl
    title="MODEL PMSL [MB]"
    echo "             11  189  187  186  182  180  140  150                              " >ic.gplotf0
    echo "                 152  155  157  159                                             " >>ic.gplotf0
    echo "                985.      990.      995.     1000.     1005.     1010.     1015." >>ic.gplotf0
    echo "               1020.     1025.     1030.                                        " >>ic.gplotf0
    idx=$(printf "%2d" 0) ; ILVL=$(printf "%2d" 13)
    FLO=$(printf "%7.1f" 900.)
    HI=$(printf "%10.1f" 1100.)
    FINC=$(printf "%11.1f" 5.0)
ccg ggplot tpmsl
    zonavg tpmsl ztpmsl
#                                      observed data.
    echo "XFIND         PMSL" |ccc xfind era tpmslo
    title="ERA PMSL [MB]"
ccg ggplot tpmslo
    zonavg tpmslo ztpmslo
#                                      plot zonal avg
    joinup plts ztpmsl ztpmslo
    echo "XMPLT PS           0 NEXT    1      950.     1050.    1    2                   8" >ic.plts
    echo "$run $days MODEL,ERA(R) PS [MB]                                                " >>ic.plts
    echo "              2   99  100                                                      " >>ic.plts
    xmplot plts input=ic.plts
    rm tpmsl tpmslo ztpmsl ztpmslo plts
#
#   ---------------------------------- pcp
#
#                                      model data.
    echo "XFIND         PCP" |ccc xfind gp tpcp1
#   ---------------------------------- convert to mm/day.
    echo "XLIN          86400.        0." |ccc xlin tpcp1 tpcp
    echo "              7  140  180  185  187  189  153  159                              " >ic.gplotf0
    echo "                  2.        4.        6.        8.       12.       20.       24." >>ic.gplotf0
    idx=$(printf "%2d" 0) ; ILVL=$(printf "%2d" 13)
    FLO=$(printf "%7.1f" 0.)
    HI=$(printf "%10.1f" 50.)
    FINC=$(printf "%11.1f" 2.0)
    title="TOTAL MODEL PRECIP.[MM/DAY]"
ccg ggplot tpcp
    printglb tpcp dummy
    zonavg tpcp ztpcp
    rm tpcp1
#                                      observed data - gpcp.
    echo "XFIND         PCP" |ccc xfind gpcp tgpcp
    echo "FMASK                NEXT   LE     1.E30" |ccc fmask tgpcp mask
    rzonavg tgpcp mask ztgpcp
#                                      observed data - cmap (former xie & arkin).
    echo "XFIND         PCP1" |ccc xfind cmap tcmap
    echo "FMASK                NEXT   LE     1.E30" |ccc fmask tcmap mask
    rzonavg tcmap mask ztcmap
#                                      plot out observed data.
    title="GPCP PRECIP.[MM/DAY]"
ccg ggplot tgpcp
    printglb tgpcp dummy
#
    title="CMAP PRECIP.[MM/DAY]"
ccg ggplot tcmap
    printglb tcmap dummy

    joinup plts ztpcp ztgpcp ztcmap
    echo "XMPLOT PCP         0 NEXT    1        0.       10.    1    3                   8" >ic.plts
    echo "$run $days MODEL,GPCP(R),CMAP(B) PRECIP.[MM/DAY]                               " >>ic.plts
    echo "              3   99  100  120                                                 " >>ic.plts
    xmplot plts input=ic.plts
    rm gpcp cmap tgpcp tcmap ztgpcp ztcmap ztpcp plts mask
#
#   ---------------------------------- screen temperature.
    echo "XFIND         ST  " |ccc xfind gp tst
    zonavg tst ztst
    printglb tst dummy
    rm tst
#                                      observed screen temperature
    echo "XFIND         ST  " |ccc xfind era tostx
    echo "XLIN              1.   -273.16" |ccc xlin tostx tost
    zonavg tost ztost
    printglb tost dummy
    rm tost tostx
#                                      plot out screen temps.
    joinup plts ztst ztost
    echo "XMPLOT  ST         0 NEXT    1      -50.       50.    1    2                   8" >ic.plts
    echo "$run $days MODEL,ERA(R) SCREEN TEMP.                                           " >>ic.plts
    echo "              2   99  100                                                      " >>ic.plts
    xmplot plts input=ic.plts
    rm ztst ztost plts
#
#   ---------------------------------- skin temperature.

    echo "XFIND         GT  " |ccc xfind gp tgt
    zonavg tgt ztgt
    printglb tgt dummy
    rm tgt
#                                      observed skin temperature
    echo "XFIND         GT  " |ccc xfind era togtx
    echo "XLIN              1.   -273.16" |ccc xlin togtx togt
    zonavg togt ztogt
    printglb togt dummy
    rm togt togtx
#                                      plot out skin temps.
    joinup plts ztgt ztogt
    echo "XMPLOT  GT         0 NEXT    1      -50.       50.    1    2                   8" >ic.plts
    echo "$run $days MODEL,ERA(R) SKIN TEMP.                                             " >>ic.plts
    echo "              2   99  100                                                      " >>ic.plts
    xmplot plts input=ic.plts
    rm ztgt ztogt plts
#
#   ---------------------------------- snow mass

    echo "XFIND  MOD    SNO" |ccc xfind gp tsno
    idx=$(printf "%2d" 0) ; ILVL=$(printf "%2d" 13)
    FLO=$(printf "%7.1f" 0.)
    HI=$(printf "%10.1f" 500.)
    FINC=$(printf "%11.1f" -9.0)
    title="SNOW MASS [KG/M2]"
    echo "              8  140  180  185  187  189  153  157                              " >ic.gplotf0
    echo "                 159                                                            " >>ic.gplotf0
    echo "                 10.       20.       50.      100.      150.      300.      500." >>ic.gplotf0
    echo "             999999.                                                            " >>ic.gplotf0
    echo "                 10.       20.       50.      100.      150.      300.      500." >>ic.gplotf0
    echo "               1000.                                                            " >>ic.gplotf0
ccg ggplot tsno
    printglb tsno dummy
#                                      Blended obs snow mass
    echo "XFIND  OBS    SNO" |ccc xfind snow tosno
#                                      replace missing with 0.
    echo "C* FMSKPLT        -1 NEXT   LT0.9999E+38    1        0.    1" | ccc fmskplt tosno tosno0 ; mv tosno0 tosno
    title="SNOW MASS OBS. [KG/M2]"
ccg ggplot tosno
    printglb tosno dummy
#                                      ERA snow mass
    echo "XFIND  ERA    DSNO" |ccc xfind era tedsno
    echo "XLIN            1.E3        0." |ccc xlin tedsno tesno
    title="SNOW MASS ERA [KG/M2]"
ccg ggplot tesno
    printglb tesno dummy
#                                      plot out zonal snow masses over land
    mlt tsno  flnd tsnol
    mlt tosno flnd tosnol
    mlt tesno flnd tesnol
    zonavg tsnol  ztsno
    zonavg tosnol ztosno
    zonavg tesnol ztesno
    joinup plts ztsno ztosno ztesno
    echo "XMPLOT SNO         0 NEXT    1        0.      300.    1    3                   8" >ic.plts
    echo "$run $days MODEL,OBS.(R),ERA(B) SNO MASS                                       " >>ic.plts
    echo "              3   99  100  120                                                 " >>ic.plts
    xmplot plts input=ic.plts
    rm tsno tsnol ztsno tosno tosnol ztosno tesno tesnol ztesno plts
#
#   ---------------------------------- cloud and energy budget plots.
#
#                                      relative humidity.
    echo "XFIND         RHC " |ccc xfind gp rh
    zonavg rh zrh
    echo "ZXPLOT     NEXT    0    0   -1     1.0E2        0.      100.     1.0E1    0$kin" >ic.plts
    echo "$run $days REL. HUMIDITY (RHC) [%]                                             " >>ic.plts
    echo "                 90.      -90.     $m01     1000.    0                         " >>ic.plts
    zxplot zrh input=ic.plts
    rm rh zrh

#                                      cloudiness.
    echo "XFIND         SCLD" |ccc xfind gp cld
    zonavg cld zcld
    echo "ZXPLOT     NEXT    0    0   -1        1.        0.        1.     1.E-1    0$kin" >ic.plts
    echo "$run $days CLOUDINESS SCLD                                                     " >>ic.plts
    echo "                 90.      -90.     $m01     1000.    0                         " >>ic.plts
    zxplot zcld input=ic.plts
    rm cld zcld

#                                      total overlapped cloudiness.
    echo "XFIND         CLDT" |ccc xfind gp ttcld
    zonavg ttcld zttcld
    idx=$(printf "%2d" -1) ; ILVL=$(printf "%2d" 13)
    FLO=$(printf "%7.1f" 0.)
    HI=$(printf "%10.1f" 1.)
    FINC=$(printf "%11.1f" 2.E-10)
    title="CLDT"
    echo "              5  128  124  117  105  102                                        " >ic.gplotf0
    echo "                  .2        .4        .6        .8        1.                    " >>ic.gplotf0
ccg ggplot ttcld
    printglb ttcld dummy
#                                      total overlapped cloudiness
#                                      with optical depth cutoff.
#
    if [ "$nocldo" = off ] ; then
    echo "XFIND         CLDO" |ccc xfind gp tcldo
    zonavg tcldo ztcldo
    title="CLDO (WITH OPTICAL DEPTH CUTOFF)"
ccg ggplot tcldo
    printglb tcldo dummy
    fi
#                                      observed cloudiness (Ceres)
    echo "XFIND         Cloud Area Fraction, Day and Night (unitless)" |ccc xfind cerescld c3
    echo "XLIN            0.01        0." |ccc xlin c3 ttocld_ceres
    rm c3
    printglb ttocld_ceres dummy
    zonavg ttocld_ceres zttocld_ceres
    title="CERES TOTAL CLOUDINESS"
ccg ggplot ttocld_ceres
#                                       observed cloudiness (ISCCP/D2)
    echo "XFIND         Mean cloud amount (CA)" |ccc xfind isccp ttocld_isccpx
    echo "XLIN            0.01        0." |ccc xlin ttocld_isccpx ttocld_isccp
    rm ttocld_isccpx
    printglb ttocld_isccp dummy
    zonavg ttocld_isccp zttocld_isccp
    title="ISCCP TOTAL CLOUDINESS"
ccg ggplot ttocld_isccp
#
    if [ "$nocldo" = off ] ; then
    joinup plts ztcldo zttocld_ceres zttocld_isccp
    echo "XMPLOT CLD         0 NEXT    1        0.        1.    1    3                   8" >ic.plts
    echo "$run $days MODEL,CERES(R),ISCCP(B) CLOUDINESS WITH TAU CUTOFF                  " >>ic.plts
    echo "              3   99  100  120                                                 " >>ic.plts
    xmplot plts input=ic.plts
    rm tcldo ztcldo ttocld_ceres ttocld_isccp zttocld_ceres zttocld_isccp plts
    fi
#                                       observed cloudiness (Warren)
    echo "XFIND         CLD*${obsday}*WARREN (FRACTION)" |ccc xfind warrencld ttocld_warren
    printglb ttocld_warren dummy
    zonavg ttocld_warren zttocld_warren
    title="WARREN TOTAL CLOUDINESS"
ccg ggplot ttocld_warren
    joinup plts zttcld zttocld_warren

    echo "XMPLOT CLD         0 NEXT    1        0.        1.    1    2                   8" >ic.plts
    echo "$run $days MODEL,WARREN(R) CLOUDINESS                                          " >>ic.plts
    echo "              2   99  100                                                      " >>ic.plts
    xmplot plts input=ic.plts
    rm ttcld zttcld ttocld_warren zttocld_warren plts

#                                       liquid water path over oceans.
#
#                                       - modelled cloud water path
#
    echo "XFIND         CLWT" |ccc xfind gp tmlwp
    echo "XLIN           1000.        0." |ccc xlin tmlwp tmlwpx ; mv tmlwpx tmlwp
    idx=$(printf "%2d" 0) ; ILVL=$(printf "%2d" 13)
    FLO=$(printf "%7.1f" 0.)
    HI=$(printf "%10.1f" 450.)
    FINC=$(printf "%11.1f" 30.0)
    echo "             12  122  120  118  114  108  106  105                              " >ic.gplotf0
    echo "                 104  103  102  101  100                                        " >>ic.gplotf0
    echo "                 30.       60.       90.      120.      150.      180.      210." >>ic.gplotf0
    echo "                240.      270.      300.      330.                              " >>ic.gplotf0
    title="MODEL LIQUID WATER PATH [G/M2]"
ccg ggplot tmlwp
#                                       - UWISC obs.
    echo "XFIND         Total Cloud Liquid Water Path (g/m2)" |ccc xfind uwisc  tolwp
    echo "XFIND         Total Cloud Liquid Water Path (g/m2)" |ccc xfind uwiscm tolwpm
    echo "FMASK             -1 NEXT   GE       0.5" |ccc fmask tolwpm mask
    zonavg mask zmask

    echo "FMSKPLT           -1 NEXT   GE       0.5                   1" |ccc fmskplt tolwp tolwpx mask
    idx=$(printf "%2d" 0) ; ILVL=$(printf "%2d" 16)
    title="UWISC LIQUID WATER PATH [G/M2]"
ccg ggplot tolwpx
#                                       - model masked
    rzonavg tmlwp mask rztmlwp
    echo "FMSKPLT           -1 NEXT   GE       0.5                   1" |ccc fmskplt tmlwp tmlwpx mask
    globavg tmlwpx model_lwp
    printglb model_lwp dummy

    rzonavg tolwp mask rztolwp
    globavg tolwpx uwisc_lwp
    printglb uwisc_lwp dummy
    joinup plts rztmlwp rztolwp
    echo "C* FMSKPLT        -1 NEXT   GT       0.0                        1" | ccc fmskplt plts pltsm zmask
    echo "XMPLOT             0 NEXT    1        0.      200.    1    2                   8" >ic.plts
    echo "$run $days MODEL,UWISC(R) LWP(OCEAN)[G/M2]                                     " >>ic.plts
    echo "              2   99  100                                                      " >>ic.plts
    xmplot pltsm input=ic.plts

#                                       - SSM/I obs.
    echo 'XFIND         Cloud Liquid Water Path (1000*mm)
XFIND         Cloud Fraction (0-1)' | ccc xfind ssmi  tolwpincld  tocfr
    echo 'XFIND         Cloud Liquid Water Path (1000*mm)
XFIND         Cloud Fraction (0-1)' | ccc xfind ssmim tolwpincldm tocfrm
    mlt tolwpincld  tocfr tolwp
    echo "FMASK                NEXT   GE       0.5" |ccc fmask tolwpincldm mask1
    echo "FMASK                NEXT   GE       0.5" |ccc fmask  tocfrm     mask2
    mlt mask1 mask2 mask
    zonavg mask zmask
    rm tolwpincld tocfr tolwpincldm tocfrm mask1 mask2

    echo "FMSKPLT           -1 NEXT   GE       0.5                   1" |ccc fmskplt tolwp tolwpx mask
    title="SSM/I LIQUID WATER PATH [G/M2]"
ccg ggplot tolwpx
#                                       - model masked
    rzonavg tmlwp mask rztmlwp
    echo "FMSKPLT           -1 NEXT   GT     0.5E0                   1" |ccc fmskplt tmlwp tmlwpx mask
    globavg tmlwpx model_lwp
    printglb model_lwp dummy
#
    rzonavg tolwp mask rztolwp
    globavg tolwpx ssmi_lwp
    printglb ssmi_lwp dummy

    joinup plts rztmlwp rztolwp
    echo "C* FMSKPLT        -1 NEXT   GT       0.0                        1" | ccc fmskplt plts pltsm zmask
    echo "XMPLOT             0 NEXT    1        0.      200.    1    2                   8" >ic.plts
    echo "$run $days MODEL,SSM/I(R) LWP(OCEAN)[G/M2]                                     " >>ic.plts
    echo "              2   99  100                                                      " >>ic.plts
    xmplot pltsm input=ic.plts
    rm model_lwp ssmi_lwp uwisc_lwp tmlwp tmlwpx tolwp tolwpm tolwpx rztmlwp rztolwp mask zmask plts pltsm

    txtplot input=dummy
    rm dummy

#
#                                       get obs cloud forcings.
#
    echo 'XFIND         SW cloud-radiative effect at TOA  (W/m2)
XFIND         LW cloud-radiative effect at TOA  (W/m2)' | ccc xfind ceres  ocfst  ocflt
    echo 'XFIND         SW cloud-radiative effect at TOA  (W/m2)
XFIND         LW cloud-radiative effect at TOA  (W/m2)' | ccc xfind ceresm ocfstm ocfltm
    echo "FMASK                NEXT   GE       0.5" |ccc fmask ocfstm masks
    echo "FMASK                NEXT   GE       0.5" |ccc fmask ocfltm maskl
#
#					 short wave cloud forcing at the top
#
    echo "XFIND         CFST" |ccc xfind gp mcfst

    rzonavg mcfst masks zmcfst
    rzonavg ocfst masks zocfst
    joinup plts zmcfst zocfst
    echo "XMPLOTCFST         0 NEXT    1     -160.        0.    1    2                   8" >ic.plts
    echo "$run $days MODEL,OBS(R) SW CLD FORCING (TOP)[W/M2]                             " >>ic.plts
    echo "              2   99  100                                                      " >>ic.plts
    xmplot plts input=ic.plts
    rm ocfstm zmcfst zocfst plts

    echo "FMSKPLT           -1 NEXT   GT     0.5E0                   1" |ccc fmskplt mcfst mcfstx masks
    echo "FMSKPLT           -1 NEXT   GT     0.5E0                   1" |ccc fmskplt ocfst ocfstx masks
    printglb mcfstx dummy
    printglb ocfstx dummy
    rm mcfst ocfst mcfstx ocfstx masks
#
#					 long  wave cloud forcing at the top
#
    echo "XFIND         CFLT" |ccc xfind gp mcflt

    rzonavg mcflt maskl zmcflt
    rzonavg ocflt maskl zocflt
    joinup plts zmcflt zocflt
    echo "XMPLOTCFLT         0 NEXT    1        0.       60.    1    2                   8" >ic.plts
    echo "$run $days MODEL,OBS(R) LW CLD FORCING (TOP)[W/M2]                             " >>ic.plts
    echo "              2   99  100                                                      " >>ic.plts
    xmplot plts input=ic.plts
    rm ocfltm zmcflt zocflt plts

    echo "FMSKPLT           -1 NEXT   GT     0.5E0                   1" |ccc fmskplt mcflt mcfltx maskl
    echo "FMSKPLT           -1 NEXT   GT     0.5E0                   1" |ccc fmskplt ocflt ocfltx maskl
    printglb mcfltx dummy
    printglb ocfltx dummy
    rm mcflt ocflt mcfltx ocfltx
#                                      output global average
    txtplot input=dummy
    rm dummy

#                                      all-sky planetary albedo.
#
#                                      model data.
    echo "XFIND         LPA" |ccc xfind gp tmlpa
#                                      observed data.
    echo "XFIND         Total-sky TOA Albedo SSTF/SOLF (dimensionless)" |ccc xfind ceres  tolpa
    echo "XFIND         Total-sky TOA Albedo SSTF/SOLF (dimensionless)" |ccc xfind ceresm tolpam
    echo "FMASK                NEXT   GE       0.5" |ccc fmask tolpam mask
#
    zonavg mask zmask
    rzonavg tmlpa mask ztmlpa
    rzonavg tolpa mask ztolpa
    joinup plts ztmlpa ztolpa
    echo "C* FMSKPLT        -1 NEXT   GT       0.0                        1" | ccc fmskplt plts pltsm zmask
    echo "XMPLOT LPA         0 NEXT    1        0.        1.    1    2                   8" >ic.plts
    echo "$run $days LOCAL PLANETARY ALBEDO                                              " >>ic.plts
    echo "              2   99  100                                                      " >>ic.plts
    xmplot pltsm input=ic.plts
    rm tmlpa tolpa tolpam ztmlpa ztolpa plts pltsm mask zmask
#
#
#                                      clear sky planetary albedo.
#
#                                      model data.
    echo "XFIND         LPAC" |ccc xfind gp tmlpac
#                                      observed data.
    echo "XFIND         Clear-sky TOA Albedo SATF/SOLF (dimensionless)" |ccc xfind ceres  tolpac
    echo "XFIND         Clear-sky TOA Albedo SATF/SOLF (dimensionless)" |ccc xfind ceresm tolpacm
    echo "FMASK                NEXT   GE       0.5" |ccc fmask tolpacm mask
#
    zonavg mask zmask
    rzonavg tmlpac mask ztmlpac
    rzonavg tolpac mask ztolpac
    joinup plts ztmlpac ztolpac
    echo "C* FMSKPLT        -1 NEXT   GT        0.0                       1" | ccc fmskplt plts pltsm zmask
    echo "XMPLOTLPAC         0 NEXT    1        0.        1.    1    2                   8" >ic.plts
    echo "$run $days CLEAR-SKY PLANETARY ALBEDO                                          " >>ic.plts
    echo "              2   99  100                                                      " >>ic.plts
    xmplot pltsm input=ic.plts
    rm tmlpac tolpac tolpacm ztmlpac ztolpac plts pltsm mask zmask
#
#                                      reflected all-sky solar flux at toa.
#
    echo "XFIND         FSR" |ccc xfind gp tmfsr
#                                      observed data.
    echo "XFIND         Total-sky TOA Outgoing SW Flux  (W/m2)" |ccc xfind ceres  tofsr
    echo "XFIND         Total-sky TOA Outgoing SW Flux  (W/m2)" |ccc xfind ceresm tofsrm
    echo "FMASK                NEXT   GE       0.5" |ccc fmask tofsrm mask
#
    rzonavg tmfsr mask ztmfsr
    rzonavg tofsr mask ztofsr
    joinup plts ztmfsr ztofsr
    echo "XMPLOT FST         0 NEXT    1        0.      500.    1    2                   8" >ic.plts
    echo "$run $days ALL-SKY REFL. SOLAR FLUX AT TOA (FST)[W/M2]                         " >>ic.plts
    echo "              2   99  100                                                      " >>ic.plts
    xmplot plts input=ic.plts
    rm tmfsr tofsr tofsrm ztmfsr ztofsr plts mask
#
#                                      reflected clear-sky solar flux at toa.
#
    echo "XFIND         FSRC" |ccc xfind gp tmfsrc
#                                      observed data.
    echo "XFIND         Clear-sky TOA Outgoing SW Flux  (W/m2)" |ccc xfind ceres  tofsrc
    echo "XFIND         Clear-sky TOA Outgoing SW Flux  (W/m2)" |ccc xfind ceresm tofsrcm
    echo "FMASK                NEXT   GE       0.5" |ccc fmask tofsrcm mask
#
    rzonavg tmfsrc mask ztmfsrc
    rzonavg tofsrc mask ztofsrc
    joinup plts ztmfsrc ztofsrc
    echo "XMPLOTFSTC         0 NEXT    1        0.      500.    1    2                   8" >ic.plts
    echo "$run $days CLEAR-SKY REFL. SOLAR FLUX AT TOA[W/M2]                             " >>ic.plts
    echo "              2   99  100                                                      " >>ic.plts
    xmplot plts input=ic.plts
    rm tmfsrc tofsrc tofsrcm ztmfsrc ztofsrc plts mask
#
#                                      outgoing longwave radiation at toa.
#
    echo "XFIND         OLR" |ccc xfind gp  tmolr
#                                      observed data.
    echo "XFIND         Total-sky TOA Outgoing LW Flux  (W/m2)" |ccc xfind ceres  toolr
    echo "XFIND         Total-sky TOA Outgoing LW Flux  (W/m2)" |ccc xfind ceresm toolrm
    echo "FMASK                NEXT   GE       0.5" |ccc fmask toolrm mask
#
    rzonavg tmolr mask ztmolr
    rzonavg toolr mask ztoolr
    joinup plts ztmolr ztoolr
    echo "XMPLOT OLR         0 NEXT    1        0.      500.    1    2                   8" >ic.plts
    echo "$run $days ALL-SKY OLR [W/M2]                                                  " >>ic.plts
    echo "              2   99  100                                                      " >>ic.plts
    xmplot plts input=ic.plts
    rm tmolr toolr toolrm ztmolr ztoolr plts mask
#
#                                      clear-sky outgoing longwave radiation at toa.
#
    echo "XFIND         OLRC" |ccc xfind gp  tmolrc
#                                      observed data.
    echo "XFIND         Clear-sky TOA Outgoing LW Flux  (W/m2)" |ccc xfind ceres  toolrc
    echo "XFIND         Clear-sky TOA Outgoing LW Flux  (W/m2)" |ccc xfind ceresm toolrcm
    echo "FMASK                NEXT   GE       0.5" |ccc fmask toolrcm mask
#
    rzonavg tmolrc mask ztmolrc
    rzonavg toolrc mask ztoolrc
    joinup plts ztmolrc ztoolrc
    echo "XMPLOTOLRC         0 NEXT    1        0.      500.    1    2                   8" >ic.plts
    echo "$run $days CLEAR-SKY OLR [W/M2]                                                " >>ic.plts
    echo "              2   99  100                                                      " >>ic.plts
    xmplot plts input=ic.plts
    rm tmolrc toolrc toolrcm ztmolrc ztoolrc plts mask

#   ---------------------------------  surface energy fluxes.
#                                      sign - positive downward.

    echo "XFIND         HFS" |ccc xfind gp hfx
    echo "XFIND         QFS" |ccc xfind gp qfx
    echo "XFIND         BALG" |ccc xfind gp balg

    echo "XLIN        -2.501E6" |ccc xlin qfx lf
    echo "XLIN            -1.0" |ccc xlin hfx hf

    zonavg hf zhf
    zonavg lf zlf
    zonavg balg zbalg

#                                      total fluxes.
    add balg hf aa
    add aa lf net
    zonavg net znet
    joinup zflux znet zbalg zlf zhf
    rm aa znet zhf zlf zbalg hfx

#                                      ocean fluxes.
    mlt hf mo hfa
    mlt lf mo lfa
    mlt balg mo balga
    add balga hfa aa
    add aa lfa neta
    rm aa

    rzonavg hfa mo zhfo
    rzonavg lfa mo zlfo
    rzonavg balga mo zbalgo
    rzonavg neta  mo zneto
    joinup zfluxo zneto zbalgo zlfo zhfo
    rm zneto zhfo zlfo zbalgo

#                                      land/ice fluxes.
    gmlt zfluxo zmo fluxa
    sub zflux fluxa fluxb
    gmlt fluxb izmil zfluxl
    rm fluxa fluxb izmil zmo

#                                      global averages total and ocean.
    div neta gmo neto
    div balga gmo balgo
    div lfa gmo lfo
    div hfa gmo hfo
    rm neta balga lfa hfa gmo

#                                      balance at top and bottom.

#                                      model data for balance at top.
    echo "XFIND         BALT" |ccc xfind gp balt
#                                      observed data for balance at top.
    echo "XFIND         Total-sky TOA Net Flux  (W/m2)" |ccc xfind ceres  obalt
    echo "XFIND         Total-sky TOA Net Flux  (W/m2)" |ccc xfind ceresm obaltm
    echo "FMASK                NEXT   GE       0.5" |ccc fmask obaltm mask
    mlt balt  mask mbalt
    mlt obalt mask mobalt
    rzonavg mbalt  mask zbalt
    rzonavg mobalt mask zobalt
    joinup plts zbalt zobalt
    echo "XMPLOTBALT         0 NEXT    1     -200.      200.    1    2                   8" >ic.plts
    echo "$run $days MODEL AND OBS BALANCE TOP (BALT) [W/M2]                             " >>ic.plts
    echo "              2   99  100                                                      " >>ic.plts
    xmplot plts input=ic.plts
#                                      model and obs for balt where obs available.
    globavg mask gmask
    div mbalt  gmask baltm
    div mobalt gmask obaltm
    rm ceres ceresm zbalt zobalt plts mask mobalt gmask
#
#                                      surface fluxes.
    echo 'XFIND         BEG
XFIND         PCP' | ccc xfind gp beg pcp
    sub pcp qfx pme
    echo "XLIN         2.501E6" |ccc xlin pme lpme
    sub balt beg nete

#
    rm qfx pcp pme

    printglb nete   dummy
    printglb beg    dummy
    printglb balt   dummy
    printglb baltm  dummy
    printglb obaltm dummy
    printglb lpme   dummy

    txtplot input=dummy
    rm nete balt obalt baltm obaltm lpme dummy

#                                      global surface fluxes.
    printglb net    dummy
    printglb balg   dummy
    printglb lf     dummy
    printglb hf     dummy

#                                      ocean surface fluxes.
    printglb neto   dummy
    printglb balgo  dummy
    printglb lfo    dummy
    printglb hfo    dummy
    txtplot input=dummy
    rm neto balgo lfo hfo dummy

#   ---------------------------------- plot out fluxes.

    echo "XMPLOT             0 NEXT    1     -250.      250.         4                   8" >ic.plts
    echo "$run $days NET,RAD-R,LAT-B,SEN-G FLX[W/M2]                                     " >>ic.plts
    echo "              4   99  100  120  112                                            " >>ic.plts
    xmplot zflux input=ic.plts
    echo "XMPLOT             0 NEXT    1     -250.      250.         4                   8" >ic.plts
    echo "$run $days NET,RAD-R,LAT-B,SEN-G FLX(OCN)[W/M2]                                " >>ic.plts
    echo "              4   99  100  120  112                                            " >>ic.plts
    xmplot zfluxo input=ic.plts
    echo "XMPLOT             0 NEXT    1     -250.      250.         4                   8" >ic.plts
    echo "$run $days NET,RAD-R,LAT-B,SEN-G FLX(LND/ICE)[W/M2]                            " >>ic.plts
    echo "              4   99  100  120  112                                            " >>ic.plts
    xmplot zfluxl input=ic.plts

    idx=$(printf "%2d" 0) ; ILVL=$(printf "%2d" 13)
    FLO=$(printf "%7.1f" -500.)
    HI=$(printf "%10.1f" 500.)
    FINC=$(printf "%11.1f" 25.0)
    echo "              7  181  140  150  152  154  156  158                          " >ic.gplotf0
    echo "                -25.       25.       75.      125.      175.      225.      " >>ic.gplotf0
    title="SURFACE RADIATIVE FLUX [W/M2]"
ccg ggplot balg
    echo "              7  188  186  184  182  180  140  150                          " >ic.gplotf0
    echo "               -225.     -175.     -125.      -75.      -25.       25.      " >>ic.gplotf0
    title="SURFACE LATENT HEAT FLUX [W/M2]"
ccg ggplot lf
    echo "              7  188  186  184  182  140  150  153                          " >ic.gplotf0
    echo "               -100.      -75.      -50.      -25.       25.       50.      " >>ic.gplotf0
    title="SURFACE SENSIBLE HEAT FLUX [W/M2]"
ccg ggplot hf

#   ---------------------------------- plot net energy flux
#                                      at ground.
    echo 'XFIND         SRAD
XFIND         LRAD
XFIND         SH
XFIND         LH' | ccc xfind era xfsg xflg xhfs xhfl
    add xfsg xflg xbalg
    add xhfs xhfl xhfsl
    add xbalg xhfsl begox
    printf "  RELABL.  GRID\n                           BEG    1" | ccc relabl begox bego
    idx=$(printf "%2d" 0) ; ILVL=$(printf "%2d" 13)
    FLO=$(printf "%7.1f" -900.)
    HI=$(printf "%10.1f" 900.)
    FINC=$(printf "%11.1f" 25.0)
    title="SURFACE ENERGY FLUX [W/M2]"
    echo "              7  186  183  180  140  150  153  156                              " >ic.gplotf0
    echo "               -125.      -75.      -25.       25.       75.      125.          " >>ic.gplotf0
ccg ggplot beg
    title="ERA SURFACE ENERGY FLUX [W/M2]"
ccg ggplot bego
    mlt beg  mo mbeg
    mlt bego mo mbego
    rzonavg mbeg  mo zmbeg
    rzonavg mbego mo zmbego
    joinup zplts zmbeg zmbego
    echo "XMPLOT BEG         0 NEXT    1     -250.      250.    1    2                   8" >ic.plts
    echo "$run $days MODEL,OBS(R) NET HEAT FLUX [W/M2]                                   " >>ic.plts
    echo "              2   99  100                                                      " >>ic.plts
    xmplot zplts input=ic.plts
    rm zmbeg zmbego mo mbeg mbego bego
    rm xhfl xhfs xhfsl xbalg xflg xfsg
    rm begox zplts hf lf balg zflux zfluxo zfluxl
#
#    ---------------------------------- various means.
    echo "XFIND         $d
XFIND         U
XFIND         T
XFIND         V" | ccc xfind gp del u t v
    ggstat del
    ggstat u
    ggstat t
    ggstat v

    zonavg del zdel
    rzonavg u del ur
    rzonavg v del vr
    rzonavg t del tr
    echo "XLIN              1.   -273.16" |ccc xlin tr tc
    ggstat ur
    ggstat vr
    ggstat tc

    mlt vr zdel dv
    zxpsi dv psi

    echo "ZXPLOT        U    0    0$lxp        1.     -150.      150.        5.$kax$kin" >ic.plts
    echo "$run $days ZONAL VELOCITY (U)R [M/SEC]                                       " >>ic.plts
    zxplot ur input=ic.plts
    echo "ZXPLOT     NEXT    0    0$lxp        1.     -140.      100.       10.$kax$kin" >ic.plts
    echo "$run $days TEMPERATURE (T)R[DEG C]                                           " >>ic.plts
    zxplot tc input=ic.plts
    echo "ZXPLOT      PSI    0    0$lxp    1.E-10    -9.0E1     9.0E1     1.0E0    0$kin" >ic.plts
    echo "$run $days MASS STREAM FUNCTION PSI [1E10KG/SEC]                              " >>ic.plts
    zxplot psi input=ic.plts
    rm zdel ur vr tr tc dv psi

#                                      variances.
    if [ "$stat2nd" != "off" ] ; then
    echo "XFIND         (U*U*)R
XFIND         (U'U')R
XFIND         (V*V*)R
XFIND         (V'V')R" | ccc xfind xp us2r up2r vs2r vp2r

    sqroot us2r sdus
    sqroot up2r sdup
    echo "ZXPLOT     U*U*    0    0$lxp        1.        0.      500.        2.$kax$kin" >ic.plts
    echo "$run $days STANDING EDDY SD  ((U*U*)R.)**0.5) [M/SEC]                        " >>ic.plts
    zxplot sdus input=ic.plts
    echo "ZXPLOT     U'U'    0    0$lxp        1.        0.      500.        2.$kax$kin" >ic.plts
    echo "$run $days TRANSIENT EDDY SD  ((U+U+)R.)**0.5) [M/SEC]                       " >>ic.plts
    zxplot sdup input=ic.plts
    rm sdus sdup

    sqroot vs2r sdvs
    sqroot vp2r sdvp
    echo "ZXPLOT     V*V*    0    0$lxp        1.        0.      500.        2.$kax$kin" >ic.plts
    echo "$run $days STANDING EDDY SD ((V*V*)R)**0.5) [M/SEC]                          " >>ic.plts
    zxplot sdvs input=ic.plts
    echo "ZXPLOT     V'V'    0    0$lxp        1.        0.      500.        2.$kax$kin" >ic.plts
    echo "$run $days TRANSIENT EDDY SD ((V+V+)R)**0.5) [M/SEC]                         " >>ic.plts
    zxplot sdvp input=ic.plts
    rm sdvs sdvp

#                                      kinetic energy.
    add us2r up2r a
    add vs2r vp2r b
    add a b eke2
    echo "XLIN           0.5E0" |ccc xlin eke2 eke
    echo "ZXPLOT     NEXT    0    0$lxp    1.0E-1        5.      500.       5.0$kax$kin" >ic.plts
    echo "$run $days EDDY KINETIC ENERGY (KE)R [10*(M2/SEC2)]                          " >>ic.plts
    zxplot eke input=ic.plts
    rm a b eke2 eke us2r up2r vs2r vp2r

#                                      uv transport.
    echo "XFIND         (U*V*)R
XFIND         (U'V')R" | ccc xfind xp usvsr upvpr
    echo "ZXPLOT     U*V*    0    0$lxp        1.     -300.      300.       5.0$kax$kin" >ic.plts
    echo "$run $days STANDING EDDY TRANSPORT (U*V*)R [M2/SEC2]                         " >>ic.plts
    zxplot usvsr input=ic.plts
    echo "ZXPLOT     U'V'    0    0$lxp        1.     -300.      300.        5.$kax$kin" >ic.plts
    echo "$run $days TRANSIENT EDDY TRANSPORT (U+V+)R [M2/SEC2]                        " >>ic.plts
    zxplot upvpr input=ic.plts
    rm usvsr upvpr

#                                      tv transport.
    echo "XFIND         (T*V*)R
XFIND         (T"V")R" | ccc xfind xp tsvsr tpvpr
    echo "ZXPLOT     T*V*    0    0$lxp        1.     -300.      300.       5.0$kax$kin" >ic.plts
    echo "$run $days STANDING EDDY TRANSPORT (T*V*)R [DEG*M/SEC]                       " >>ic.plts
    zxplot tsvsr input=ic.plts
    echo "ZXPLOT     T'V'    0    0$lxp        1.     -300.      300.        5.$kax$kin" >ic.plts
    echo "$run $days TRANSIENT EDDY TRANSPORT (T+V+)R [DEG*M/SEC]                      " >>ic.plts
    zxplot tpvpr input=ic.plts
    rm tsvsr tpvpr xp
    fi

tailfram=$(cat $(which tailfram.cdk) |sed '1,2d')
echo "$tailfram" > Input_Cards

.   plotid.cdk
.   plot.cdk

rm Input_Cards
