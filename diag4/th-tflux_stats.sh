#!/bin/sh
#
# This script is only included in the merged_diag_script grouping when
# gas-phase chemistry is run in MAM mode. BW, June 2023
#
# Amendments to the original: - source the file of switches
#                             - replace "access press ${flabel}_gep" with  
#                                          "access gspres ${flabel}_gep"
#                               to be consistent with pqtphirho
#                             - replace other occurrences of press with gspres
#
# -----------------------------------------------------------------------------
# TH-VT_STATS
#
# Calculates theta statistics and covariances with u, v, and w.
#
# --- DAP Oct. 14, 2022 : merged in calculation of eddy heat flux (V'T')
# --- DAP Sept. 28, 2022: converted to shell script
# --- DAP December 2005 : modified to remove geqtz.dk from string
#                         necessary calculations included here
# ------------------------------------------------------------------------------

# Source the list of on/off switches
source ${CANESM_SRC_ROOT}/CanDIAG/diag4/gaschem_diag_switches.sh

# Spectral sigma case
if [ "$datatype" = "specsig" ] ; then
  . spfiles.cdk

  # Get the surface pressure field
  access gslnsp ${flabel}_gslnsp na
  if [ ! -s gslnsp ] ; then
    echo "SELECT    STEPS $t1 $t2 $t3 LEVS    1    1 NAME LNSP" | ccc select npaksp sslnsp
    echo "COFAGG.   $lon$lat" | ccc cofagg sslnsp gslnsp
    rm sslnsp
  fi
fi

# Pressure and temperature on model layers - calculated in pqtphirho.sh
access gstemp ${flabel}_get
#access press  ${flabel}_gep
access gspres ${flabel}_gep

# Pre-calculated temperature and meridional wind on pressure levels
access gpt ${flabel}_gpt
access gpv ${flabel}_gpv

# Beta for rzonavg calculation
access beta1 ${flabel}_gpbeta

# Convert temp to potential temperature
    
# Create p0 file
#echo "XLIN              0.   101325." | ccc xlin press p0
echo "XLIN              0.   101325." | ccc xlin gspres p0

# Divide p0 by p; raise to power R/Cp
#div  p0 press pratio
div  p0 gspres pratio
echo "FPOW R/CP      0.286" | ccc fpow pratio a

# Evaluate potential temperature from T.
mlt gstemp a getheta

echo "  GSAPL.  $plv       0.0       0.0$coord$plid
$p01$p02$p03$p04$p05$p06$p07$p08$p09$p10$p11$p12$p13$p14$p15$p16
$p17$p18$p19$p20$p21$p22$p23$p24$p25$p26$p27$p28$p29$p30$p31$p32
$p33$p34$p35$p36$p37$p38$p39$p40$p41$p42$p43$p44$p45$p46$p47$p48
$p49$p50$p51$p52$p53$p54$p55$p56$p57$p58$p59$p60$p61$p62$p63$p64
$p65$p66$p67$p68$p69$p70$p71$p72$p73$p74$p75$p76$p77$p78$p79$p80
$p81$p82$p83$p84$p85$p86$p87$p88$p89$p90$p91$p92$p93$p94$p95$p96
$p97$p98$p99$p100" | ccc gsapl getheta gslnsp gptheta

timavg gptheta theta_tav
zonavg theta_tav theta_ztav

# Eddy heat flux calculations

#  x* = xdsh = zonal deviation of x field.
ccc rzonavg gpt beta1 rzx xdsh

#  v* = vdsh = zonal deviation of v field.
ccc rzonavg gpv beta1 rzv vdsh

#  v*x*   = vdxd
ccc mlt vdsh xdsh vdxd

# <v*x*>  = rzvdxd = eddy flux (total)
ccc rzonavg vdxd beta1  rzvdxd

# [<v*x*>] = rtzvdxd = time averaged total meridional eddy flux. 
ccc timavg rzvdxd rtzvdxd

# Save fields

if [ "$ztsave" = on ] ; then
  access oldxp ${flabel}xp
  echo "XSAVE.        POT. TEMP.
NEWNAM.    POTT
XSAVE...      [<V*TEMP*>]R
NEWNAM.    V!T!" | ccc xsave oldxp theta_ztav rtzvdxd newxp
  save newxp ${flabel}xp
  delete oldxp
  rm newxp
fi

if [ "$iesave" = "on" -o "$ipsave" = "on" ] ; then
  access oldip ${flabel}ip  na
  echo "XSAVE...      INSTANTANEOUS [<V*TEMP*>]R
NEWNAM.    V!T!" | xsave oldip rzvdxd newip
  save newip ${flabel}ip
  delete oldip              na
  rm newip
fi

rm theta_ztav rzx xdsh rzv vdsh vdxd rzvdxd rtzvdxd

access oldgp ${flabel}gp
echo "XSAVE.        POT. TEMP.
NEWNAM.    POTT" | ccc xsave oldgp  theta_tav  newgp
save newgp ${flabel}gp
delete oldgp
rm newgp

if [ "$gpthetafile" = on ] ; then
  save gptheta ${flabel}_gptheta
fi

# *******************************************************************
# Calculate eddy flux of potential temperature if requested

if [ "$vxstats" = on ] ; then

  # rename field as TEMP
  echo "NEWNAM.    TEMP" | ccc newnam gptheta x

  # <a> = zonal average, a* = zonal deviation from average.
  # [a] = time average,  a" = time deviation from average.

  #  x* = xdsh = zonal deviation of x field.
  rzonavg x beta1 rzx xdsh

  #  v* = vdsh = zonal deviation of v field.
  rzonavg gpv beta1 rzv vdsh

  #  v*x*   = vdxd
  mlt vdsh xdsh vdxd

  # <v*x*>  = rzvdxd = eddy flux (total)
  rzonavg vdxd beta1  rzvdxd 

  # [<v*x*>] = rtzvdxd = time averaged total meridional eddy flux.
  timavg rzvdxd rtzvdxd

  rm rzx xdsh rzv vdsh vdxd

  # Output <v*x*> r

  if [ "$ztsave" = on ] ; then

    # Saves time average [<v*x*>]
    access oldxp ${flabel}xp
    # ------------------------------ (CARD)
    echo "XSAVE...      [<V*TH*>]R
NEWNAM.    V.TH" | ccc  xsave oldxp rtzvdxd newxp
    save newxp ${flabel}xp
    delete oldxp
    rm newxp
    fi
  rm rzvdxd rtzvdxd
#   ..........................end of ............................ fluxes.
fi

#rm getheta gstemp a pratio p0 press
rm getheta gstemp a pratio p0 gspres
rm gslnsp gptheta beta1
