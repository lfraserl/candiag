#!/bin/sh
#
# This script is only included in the merged_diag_script grouping when
# gas-phase chemistry is run in MAM mode. BW, June 2023
#
# Amendments to the original: hard-code the station list
#
#  DESCRIPTION
# 
#  Pulls out instantaneous values of a field at a pre-defined set of
#  station locations from the 3-D model concentration fields
#
#  PARMSUB PARAMETERS
#
#    -- stime, memory1, model1, flabel, lat, lon, pmax, pmin, 
#       t1, t2, t3, t4, co_cmdl_stlist, delt
#
#  PREREQUISITES
#
#    -- SS history files
#
#  CHANGELOG
#
#    2023-01-29: Converted to shell script (D. Plummer)
#    2008-05-28: Version one modified from earlier stand-alone scripts
# -----------------------------------------------------------------------------
#
# -- access the list of station locations
co_cmdl_stlist="/home/rdp001/cmam/diags/stations/cmdl_co_stations_2009"
ln -s ${co_cmdl_stlist} stations1
#
# -- access model history files
. spfiles.cdk
#
#  ----  select the required model fields
#
echo "SELECT    STEPS $t1 $t2    1 LEVS-9001 1000 NAME   CO" | ccc select npaksp CO_se
echo "COFAGG    $lon$lat    0    0" | ccc cofagg CO_se CO_ge
rm npaksp CO_se
#
#  ---- pull data at selected stations
#    ---- the first flag on the PULLSTN card specifies that
#         only the second file passed to pull_sfc_stns is
#         to be used - the first one specified is ignored
#
echo "PULLSTN   2 $delt" | ccc pull_sfc_stns_v2 dummy CO_ge stations1 txtout1
#
access oldgp co_cmdl_st_${runid}_${year} na
if [ -e oldgp ] ; then
  cat oldgp  txtout1 > temp1
else
  mv txtout1 temp1
fi
save temp1 co_cmdl_st_${runid}_${year}
delete oldgp                             na
rm -f CO_ge stations1 txtout1 temp1
