#!/bin/sh

#
# Run loop for 12 months
#
    release all
    touch all
    for m in $months ; do
      if [ "$m" -ge "$mon_start" ] ; then
        year0=`expr $year + 0 | $AWK '{printf "%4d", $0}'`;
        access  gz mf_${modelh}_${year0}_m${m}_gz
      else
        year0=`expr $year + 1 | $AWK '{printf "%4d", $0}'`;
        access  gz mf_${modelh}_${year0}_m${m}_gz
      fi

      echo "XFIND         OCEAN TEMPERATURE
XFIND         OCEAN SALINITY" | ccc xfind gz temp salt
      release gz
#
# Calculate mix layer depth
#
      echo "            0.8" | ccc mixldep temp salt mxld
      rm salt temp
#
#   ---------------------------------- adjust labels and save results
#
      yeara=`expr ${year0} + ${year_offset} | $AWK '{printf "%4d", $0}'`
      echo "RELABL
RELABL             $yeara$m" | ccc relabl mxld zz

      cat zz >> all
      rm  zz mxld
    done
#
# ------------------------------------save mixed layer depths in Meter
#
    echo "XSAVE         OCEAN MIXED LAYER DEPTH (M)
XSAVE" | ccc xsave old_dat all new_dat

# save
    access  old  ${atmos_file} na
    xappend old new_dat newa
    save    newa ${atmos_file}
    delete  old  na
    release newa all new_dat old_dat
