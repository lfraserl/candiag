#!/bin/sh
# 
# This script is only included in the merged_diag_script grouping when
# gas-phase chemistry is run in MAM mode. BW, June 2023
#
# Amendments to the original: source the file of switches
#
#     Compute pressure at eta/sigma surface.
#     SK: compute also dp/de and rho
#  PARMSUB's used - run, model1, flabel, stime, memory1, days
#                   t1, t2, t3, lon, lat, coord, plid, pmin, pmax,
#
#  CHANGELOG
#
#    2022-10-17: Combined calculation of pressure and density from
#                pressure_density.dk with calculation of q, t, gz on 
#                eta levels originally done in geqtz2
#    2022-09-26: Converted to script (D. Plummer)
#    2019-10-30: Removing iesave of pressure because of redundancy 
#    2015-04-20: Minor modification since temperature is absolutely
#                 required
#    2013-06-10: Add iesave option (Y. Jiao)
# ------------------------------------------------------------------------------

# Source the list of on/off switches
source ${CANESM_SRC_ROOT}/CanDIAG/diag4/gaschem_diag_switches.sh

if [ "$datatype" = "specsig" ] ; then
.     spfiles.cdk
      access GsCheck ${model1}gs nocp na
  if [ -s "./GsCheck" ] ; then
        release GsCheck
.       ggfiles.cdk
  else
        release GsCheck
  fi
  npakgg="off"
  if [ -s npakgg ] ; then
        npakgg="on"
  fi

#   ----- get log of surface pressure and surface geopotential
  echo "SELECT.   STEPS $t1 $t2 $t3 LEVS-9001 1000 NAME PHIS LNSP" |
         ccc select npaksp ssphis sslnsp
  echo "COFAGG.   $lon$lat    0    1" | ccc cofagg ssphis gsphis
  echo "COFAGG.   $lon$lat    0    1" | ccc cofagg sslnsp gslnsp

#   ----- get temperature
  if [ "$gcmtsav" = on ] ; then
        echo "SELECT.         $t1 $t2 $t3 LEVS-9001 1000 NAME TEMP" |
           ccc select npaksp sstemp
  else
        echo "SELECT.         $t1 $t2 $t3 LEVS-9001 1000 NAME  PHI" |
           ccc select npaksp ssphi
        ctemps ssphis ssphi sstemp
  fi
  echo "COFAGG    $lon$lat    0$npg" | ccc cofagg sstemp gstemp
#
#   ----- calculate pressure at eta levels
  echo "GSAPRES   $coord$plid" | ccc gsapres gstemp gslnsp gspres0 gsdpde0
  echo "NEWNAM     P(E)" | ccc newnam gspres0 gspres
  echo "NEWNAM     DPDE" | ccc newnam gsdpde0 gsdpde
#
#   ----- calculate density at eta levels
  div gspres gstemp ga
  echo "XLIN              0.    287.04" | ccc xlin gspres gascon
  div ga gascon rho0
  echo "NEWNAM....  RHO" | ccc newnam rho0 gerho

  echo "GSAPL     $plv       0.0       0.0$coord$plid
$p01$p02$p03$p04$p05$p06$p07$p08$p09$p10$p11$p12$p13$p14$p15$p16
$p17$p18$p19$p20$p21$p22$p23$p24$p25$p26$p27$p28$p29$p30$p31$p32
$p33$p34$p35$p36$p37$p38$p39$p40$p41$p42$p43$p44$p45$p46$p47$p48
$p49$p50$p51$p52$p53$p54$p55$p56$p57$p58$p59$p60$p61$p62$p63$p64
$p65$p66$p67$p68$p69$p70$p71$p72$p73$p74$p75$p76$p77$p78$p79$p80
$p81$p82$p83$p84$p85$p86$p87$p88$p89$p90$p91$p92$p93$p94$p95$p96
$p97$p98$p99$p100" | ccc gsapl gerho gslnsp gprho

  timavg gprho gprho_tav   # time average
#
#   ----- calculate humidity and geopotential height on model levels
  if [ "$npakgg" = "on" -a \( "$moist" = " SL3D" -o "$moist" = " SLQB" \) ] ; then
        echo "SELECT.   STEPS $t1 $t2 $t3 LEVS-9001 1000 NAME   ES" |
           ccc select npakgg gses
  else
        echo "SELECT.   STEPS $t1 $t2 $t3 LEVS-9001 1000 NAME   ES" |
           ccc select npaksp sses
        echo "COFAGG.   $lon$lat    0$npg" | ccc cofagg sses gses
  fi
  echo "GSHUMH.   $coord$moist$plid$sref$spow" |
         ccc gshumh gses gstemp gslnsp gsshum gsrhum
#
  if [ "$gcmtsav" = on ] ; then
        echo "XLIN.      174.52032    287.04 RGAS" | ccc xlin gsshum gsrgasm
        echo "TAPHI.    $coord$lay$plid" |
           ccc taphi gstemp gsphis gslnsp gsrgasm gsphi
  else
        echo "COFAGG.   $lon$lat    0$npg" | ccc cofagg ssphi gsphi
  fi
#
#   ----- saving
#
  echo "XSAVE.        AIR DENSITY - KG OVER M3
NEWNAM.... DENT" > .xsave_input_card

#  ----- save instantaneous data on pressure surfaces
  if [ "$ipsave" = "on" ] ; then
        access oldip ${flabel}ip       na
        xsave oldip gprho newip input=.xsave_input_card
        save newip ${flabel}ip
        delete oldip
  fi

#  ----- save time averaged rho 
  access oldgp ${flabel}gp       na
  xsave  oldgp gprho_tav newgp input=.xsave_input_card
  save   newgp ${flabel}gp      
  delete oldgp                   na
#
#  ----- save temporary file of air density
  if [ "$gerhosave" = "on" ] ; then
        save gerho ${flabel}_gerho
  fi
#
#  ----- save temporary files of P(E) and dP/dE
  if [ "$gepsave" = "on" ] ; then
        save gspres ${flabel}_gep
        save gsdpde ${flabel}_gedpde
  fi
#
#  ----- save temporary files of Q, GZ and T for use by
#         subsequent decks
  save gstemp ${flabel}_get
  save gsshum ${flabel}_geshum
  save gsphi  ${flabel}_gez
fi
