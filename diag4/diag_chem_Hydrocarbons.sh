#!/bin/sh
#
# This script is only included in the merged_diag_script grouping when
# gas-phase chemistry is run in MAM mode. BW, June 2023
#
# Amendments to the original: hard-code deltmin and yearoff
#                             source the file of switches
#
#  DESCRIPTION
# 
#  Create monthly averages and monthly zonal averages for methane
#  (CH4) and methane oxidation products (CH3O2, CH3OOH, CH2O and CO).
#
#  PARMSUB PARAMETERS
#
#    -- chem, coord, days, flabel, lat, lon, memory1, model1, npg,
#       plid, plv, plv2, pmax, pmaxc, pmin, run, stime, t1, t2, t3,
#       t4, trchem, ztsave.
#    -- p01-p100 (pressure level set)
#
#  PREREQUISITES
#
#    -- GS and SS history files
#
#  CHANGELOG
#
#    2023-01-16: Converted to shell script, and removed treatment of
#                 chemical fields on a reduced vertical domain (D. Plummer)
#    2014-06-06: Monthly-average output on model surfaces for CCMI and 
#                 harmonization of save options iesave, ipsave, gesave
#                 (D. Plummer)
#    2013-06-10: Add iesave option (Y. Jiao)
#    2008-04-24: Modifications to look for CH3OOH and CH2O in SS file
#                for tropospheric job (D. Plummer)
#    2007-11-15: Added subversion date ('Date') and revision ('Rev')
#                keyword substitutions to all decks (A.Jonsson)
#    2007-11-13: Revised $ztsave if-statement to only encompass xp
#                file data (A.Jonsson)
#    2007-11-13: Added 'cat Input_cards' at the end of most decks
#                (A.Jonsson)
#    2007-11-??: Derived from the CH4ox deck (A.Jonsson)
# -----------------------------------------------------------------------------

# Source the list of on/off switches
source ${CANESM_SRC_ROOT}/CanDIAG/diag4/gaschem_diag_switches.sh

# Define variables in lieu of reading them off a jobfile
deltmin=7.500
yearoff=1950
#
#  ----  access model history files
#
.   ggfiles.cdk
.   spfiles.cdk
#
#  ----  get the surface pressure field
#
    access gslnsp ${flabel}_gslnsp na
    if [ ! -s gslnsp ] ; then
      if [ "$datatype" = "gridsig" ] ; then
        echo "SELECT    STEPS $t1 $t2 $t3 LEVS    1    1 NAME LNSP" |
           ccc select npakgg gslnsp
      else
        echo "SELECT    STEPS $t1 $t2 $t3 LEVS    1    1 NAME LNSP" |
           ccc select npaksp sslnsp
        echo "COFAGG    $lon$lat" | ccc cofagg sslnsp gslnsp
        rm sslnsp
      fi
    fi
#
#  ----  select required fields from model history files
#
    echo "SELECT    STEPS $t1 $t2 $t3 LEVS-9001 1000 NAME   M2" | ccc select npakgg CH3O2_ge
    if [ "$datatype" = "gridsig" ] ; then
      echo "SELECT    STEPS $t1 $t2 $t3 LEVS-9001 1000 NAME   MM   CO" | ccc select npakgg CH4_ge CO_ge
      echo "SELECT    STEPS $t1 $t2 $t3 LEVS-9001 1000 NAME   MH   HA" | ccc select npakgg CH3OOH_ge CH2O_ge
    else
      echo "SELECT    STEPS $t1 $t2 $t3 LEVS-9001 1000 NAME   MM   CO" | ccc select npaksp CH4_se CO_se
      echo "SELECT    STEPS $t1 $t2 $t3 LEVS-9001 1000 NAME   MH   HA" | ccc select npaksp CH3OOH_se CH2O_se
      echo "COFAGGxxx $lon$lat    0$npg" | ccc cofagg CH4_se CH4_ge
      echo "COFAGGxxx $lon$lat    0$npg" | ccc cofagg CO_se  CO_ge
      echo "COFAGGxxx $lon$lat    0$npg" | ccc cofagg CH3OOH_se  CH3OOH_ge
      echo "COFAGGxxx $lon$lat    0$npg" | ccc cofagg CH2O_se  CH2O_ge
      rm CH4_se CO_se CH3OOH_se CH2O_se
    fi
#
      rm npaksp npakgg
#
#  ----  interpolate onto constant pressure surfaces
      #
    echo "GSAPL.    $plv       0.0       0.0$coord$plid
$p01$p02$p03$p04$p05$p06$p07$p08$p09$p10$p11$p12$p13$p14$p15$p16
$p17$p18$p19$p20$p21$p22$p23$p24$p25$p26$p27$p28$p29$p30$p31$p32
$p33$p34$p35$p36$p37$p38$p39$p40$p41$p42$p43$p44$p45$p46$p47$p48
$p49$p50$p51$p52$p53$p54$p55$p56$p57$p58$p59$p60$p61$p62$p63$p64
$p65$p66$p67$p68$p69$p70$p71$p72$p73$p74$p75$p76$p77$p78$p79$p80
$p81$p82$p83$p84$p85$p86$p87$p88$p89$p90$p91$p92$p93$p94$p95$p96
$p97$p98$p99$p100" > .gsapl_input_card

    gsapl CH3O2_ge  gslnsp CH3O2_gp input=.gsapl_input_card
    gsapl CH3OOH_ge gslnsp CH3OOH_gp input=.gsapl_input_card
    gsapl CH2O_ge   gslnsp CH2O_gp input=.gsapl_input_card
    gsapl CO_ge     gslnsp CO_gp input=.gsapl_input_card
    gsapl CH4_ge    gslnsp CH4_gp input=.gsapl_input_card
#
#  ----  create monthly and zonal averages
#
    timavg CH3O2_gp  CH3O2_tav
    timavg CH3OOH_gp CH3OOH_tav
    timavg CH2O_gp   CH2O_tav
    timavg CO_gp     CO_tav
    timavg CH4_gp    CH4_tav
#
    if [ "$gesave" = "on" ] ; then
      timavg CH3O2_ge  CH3O2_etav
      timavg CH3OOH_ge CH3OOH_etav
      timavg CH2O_ge   CH2O_etav
      timavg CO_ge     CO_etav
      timavg CH4_ge    CH4_etav
    fi
#
    if [ "$ztsave" = on ] ; then
      zonavg CH3O2_tav  CH3O2_ztav
      zonavg CH3OOH_tav CH3OOH_ztav
      zonavg CH2O_tav   CH2O_ztav
      zonavg CO_tav     CO_ztav
      zonavg CH4_tav    CH4_ztav
    fi
#
#  ----  save instantaneous fields
#
#     ie - instantaneous data on model levels
#     ip - instantaneous data on constant pressure levels
#
    if [ "$iesave" = "on" ] ; then
      if [ "$ieslct" = "on" ] ; then
        echo "C*TSTEP   $deltmin     ${yearoff}010100          MODEL     YYYYMMDDHH" > ic.tstep_model
        selstep  CH3O2_ge  CH3O2_ie input=ic.tstep_model
        selstep CH3OOH_ge CH3OOH_ie input=ic.tstep_model
        selstep   CH2O_ge   CH2O_ie input=ic.tstep_model
        selstep     CO_ge     CO_ie input=ic.tstep_model
        selstep    CH4_ge    CH4_ie input=ic.tstep_model
#
        release oldie
        access oldie ${flabel}ie       na
        echo "XSAVE.        CH3O2
NEWNAM.
XSAVE.        CH3OOH
NEWNAM.
XSAVE.        CH2O
NEWNAM.
XSAVE.        CO
NEWNAM.
XSAVE.        CH4
NEWNAM." | ccc xsave oldie CH3O2_ie CH3OOH_ie CH2O_ie CO_ie CH4_ie newie
        save newie ${flabel}ie
        delete oldie                   na
        rm *_ie
      fi
#
      if [ "$ieslct" = "off" ] ; then
        access oldie ${flabel}ie       na
        echo "XSAVE.        CH3O2
NEWNAM.
XSAVE.        CH3OOH
NEWNAM.
XSAVE.        CH2O
NEWNAM.
XSAVE.        CO
NEWNAM.
XSAVE.        CH4
NEWNAM." | ccc xsave oldie CH3O2_ge CH3OOH_ge CH2O_ge CO_ge CH4_ge newie
        save newie ${flabel}ie
        delete oldie                   na
      fi
    fi
#
    if [ "$ipsave" = "on" ] ; then
      release oldip
      access oldip ${flabel}ip       na
      echo "XSAVE.        CH3O2
NEWNAM.
XSAVE.        CH3OOH
NEWNAM.
XSAVE.        CH2O
NEWNAM.
XSAVE.        CO
NEWNAM.
XSAVE.        CH4
NEWNAM." | ccc xsave oldip CH3O2_gp CH3OOH_gp CH2O_gp CO_gp CH4_gp newip
      save   newip ${flabel}ip
      delete oldip                   na
    fi
#
    rm *_gp
    rm *_ge
#
#  ---- save averaged fields
#
#     xp - time and zonal averaged data on pressure levels
#     gp - time averaged data on pressure levels
#     ge - time averaged data on model eta levels
#
    if [ "$ztsave" = on ] ; then
      release oldxp
      access oldxp ${flabel}xp      na
      echo "XSAVE.        CH3O2
NEWNAM.
XSAVE.        CH3OOH
NEWNAM.
XSAVE.        CH2O
NEWNAM.
XSAVE.        CO
NEWNAM.
XSAVE.        CH4
NEWNAM." | ccc xsave oldxp CH3O2_ztav CH3OOH_ztav CH2O_ztav CO_ztav CH4_ztav newxp
      save newxp ${flabel}xp
      delete oldxp                  na
    fi
#
    access oldgp ${flabel}gp      na
    echo "XSAVE.        CH3O2
NEWNAM.
XSAVE.        CH3OOH
NEWNAM.
XSAVE.        CH2O
NEWNAM.
XSAVE.        CO
NEWNAM.
XSAVE.        CH4
NEWNAM." | ccc xsave oldgp CH3O2_tav CH3OOH_tav CH2O_tav CO_tav CH4_tav newgp
    save   newgp ${flabel}gp
    delete oldgp                  na
#
    if [ "$gesave" = on ] ; then
      access oldge ${flabel}ge      na
      echo "XSAVE.        CH3O2
NEWNAM.
XSAVE.        CH3OOH
NEWNAM.
XSAVE.        CH2O
NEWNAM.
XSAVE.        CO
NEWNAM.
XSAVE.        CH4
NEWNAM." | ccc xsave oldge CH3O2_etav CH3OOH_etav CH2O_etav CO_etav CH4_etav newge
      save   newge ${flabel}ge
      delete oldge                  na
      rm *_etav
    fi
