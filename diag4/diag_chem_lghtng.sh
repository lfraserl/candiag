#!/bin/sh
#
# This script is only included in the merged_diag_script grouping when
# gas-phase chemistry is run in MAM mode. BW, June 2023
# => Careful this does not conflict with eventual similar scripts by Cyndi
#
#  DESCRIPTION
# 
#  Create monthly averages of the lightning diagnostic fields
#  saved by the model
#   ELCG = cloud-to-ground flash rate
#   ELCC = cloud-to-cloud flash rate
#   ELNI = vertically integrated lightning NOx emission
#
#  PARMSUB PARAMETERS
#
#    -- chem, flabel, memory1, model1, pmax, pmin, stime, r1, 
#       r2, r3
#
#  PREREQUISITES
#
#    -- GS history files
#
#  CHANGELOG
#
#    2023-01-29: Converted to shell script (D. Plummer)
#    2013-06-26: Using r1, r2, r3(==1) for time intervals on select
#                 to catch all outputs
#    2011-11-30: Modified xsave labels to be friendlier when converted to
#                timeseries filenames by spltdiag (D. Plummer)
#    2008-05-31: Initial version (D. Plummer)
#

#
#  ----  access model history files
#
.     ggfiles.cdk
#
#  ----  select required fields
#
    echo "SELECT    STEPS $r1 $r2 $r3 LEVS    1    1 NAME ELNI ELCG ELCC" |
       ccc select npakgg nxem c2g c2c
    rm npakgg
#
#  ----  create monthly average fields
#
    timavg nxem tmp1_tav
    timavg c2g tmp2_tav
    timavg c2c tmp3_tav
#
# -- convert vertically integrated lightning NOx emission from
#    (mixing ratio x mb/sec) to moles/m^2/sec
    echo "XLIN.....    352.008      0.00" | ccc xlin tmp1_tav nxem_tav
#
# -- convert flash frequency from cm^-2 to m^-2
    echo "XLIN.....    1.0E+04      0.00" | ccc xlin tmp2_tav c2g_tav
    echo "XLIN.....    1.0E+04      0.00" | ccc xlin tmp3_tav c2c_tav
#
    rm c2g c2c nxem tmp1_tav tmp2_tav tmp3_tav
#
#  ----  save time averaged fields
#     gp - time averaged data on lat/lon
#
    release oldgp
    access oldgp ${flabel}gp      na
    echo "XSAVE.        LIGHTNING NOX EMISSION - MOLES OVER M2 OVER SEC
NEWNAM.    ELNI
XSAVE.        CLD-TO-GRND LIGHTNING FREQUENCY - FLASHES OVER M2 OVER SEC
NEWNAM.    ELCG
XSAVE.        CLD-TO-CLD LIGHTNING FREQUENCY - FLASHES OVER M2 OVER SEC
NEWNAM.    ELCC" | ccc xsave oldgp nxem_tav c2g_tav c2c_tav newgp
    save newgp ${flabel}gp
    delete oldgp                  na
    rm *_tav
