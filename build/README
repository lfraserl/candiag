The CanDIAG compilation system now relies on a single Makefile, which relies on a platform dependent 
make template, which is defined by the COMPILER_TEMPLATE=/path/to/make/template argument. To find
the appropriate make template, navigate to the PLATFORM link in this directory.

While the flags/compiler settings come from the make template, the main Makefile is capable of building 
three different targets:
    - 'default_bins'    -> This is the default goal, which is built within the production system if 
                           CanDIAG version has source code changes from the reference library. To 
                           add targets to this group, add source code to the lspgm directory (excluding
                           the plots directory) and add the binary name to the proper DEFAULT_DIAG* list
    - 'all_diag_bins'   -> This goal will build binaries for all source code files contained in the 
                           lspgm directory (excluding the plots directory). To add targets to this group
                           simply add the source file underneath lspgm (again, excluding the plots directory)
    - 'plot_pgms'       -> This goal builds legacy plotting programs, with 32bit precision, for the codes underneath
                           lspgm/plots directory. Targets should only be added if absolutely necessary, but if 
                           so, simply add the source files underneath lspgm/plots.
                            - NOTE: that this target must be built on a platform that has the NCAR graphics library
                                    installed and the proper link flags defined with LDFLAGS_CANDIAG_PLT, within
                                    the associated make template

Note that this Makefile also builds a 32bit and 64bit library of routines from the lssub directory. If you wish to 
add subroutines, simply add the source code to this directory.

Additionally, when compiling as part of the production system, a specific compilation environment is used. To 
be consistent, source the associated compilation_environment file within the PLATFORM directory.
