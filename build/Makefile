# Makefile for CanDIAG executables/binaries
# compiler template
COMPILER_TEMPLATE 	?=

# warn if no compiler template was given
ifndef COMPILER_TEMPLATE
$(warning For all targets but 'clean' COMPILER_TEMPLATE must be given to the make call)
else
# bring in compiler flags
include $(COMPILER_TEMPLATE)
endif

# gather flags into 32bit and 64bit groupings
FFLAGS_32BIT = $(FFLAGS_PIC) $(FFLAGS_FLOAT_CONTROL) $(FFLAGS_OPT) $(FFLAGS_CANDIAG)
FFLAGS_64BIT = $(FFLAGS_PIC) $(FFLAGS_FLOAT_CONTROL) $(FFLAGS_OPT) $(FFLAGS_64BIT_REAL) $(FFLAGS_64BIT_INT) $(FFLAGS_CANDIAG)

# define specific CPP directives
CPPDEFS =
CPPDEFS_32BIT = $(CPPDEFS) -D_PAR_NWORDIO=2
CPPDEFS_64BIT = $(CPPDEFS) -D_PAR_NWORDIO=1

# define pertinent directories
LIB64_DIR	= lib64
LIB32_DIR	= lib32
BIN_DIR		= bin
BUILD_DIR	= .

#~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~
# Create Rules/Src/Obj lists for standard CanDIAG suite of programs
#~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~
# define plotting program specific settings
PLT_LIB_DIR = $(LIB32_DIR)
PLT_CPPDEFS = $(CPPDEFS_32BIT)
PLT_FFLAGS  = $(FFLAGS_32BIT)

# define directories to search for source subroutine source files
VPATH = $(shell find ../lssub -type d)

# find all subroutine files in ../lssub
LIB_SOURCE = $(shell find ../lssub -type f -name '*.f' -o -name '*.c')

# create object file lists for the libraries
TMP_LIB_OBJECTS		= $(notdir $(patsubst %.c, %.o, $(patsubst %.f, %.o, $(LIB_SOURCE))))
LIB_64BIT_OBJECTS	= $(addprefix $(LIB64_DIR)/, $(TMP_LIB_OBJECTS))
LIB_32BIT_OBJECTS	= $(addprefix $(LIB32_DIR)/, $(TMP_LIB_OBJECTS))

# find all source program files in ../lspgm
DIAG_BIN_SOURCE = $(shell find ../lspgm/ -path ../lspgm/plots -prune -false -o -type f -name '*.f' -o -name '*.f90')

# find plotting program files in ../lspgm
PLOT_BIN_SOURCE  = $(shell find ../lspgm/plots -type f -name '*.f')
PLOT_BIN_TARGETS = $(addprefix $(BIN_DIR)/, $(notdir $(basename $(PLOT_BIN_SOURCE))))

# define target/source lists for ALL 64bit programs
#	NOTE: generally, we don't build all the targets and this is included for legacy support
ALL_DIAG_64BIT_BIN_SOURCE  = $(DIAG_BIN_SOURCE)
ALL_DIAG_64BIT_BIN_TARGETS = $(addprefix $(BIN_DIR)/, $(notdir $(basename $(ALL_DIAG_64BIT_BIN_SOURCE))))

# define sublist of 64bit targets to allow for smaller/shorter compilation by default
DEFAULT_DIAG_64BIT_PGMS = add beta betao bin2txt bins binsml chabin cofagg cwinds delhat div epflux_cmip6 expone fmask fmskplt frall gadd gdiv ggacof ggstat globavg globavl globavw gmlt gpxstat gsapl gsapl_lnsp gsaplt gsapres gshumh gsmslph gstracx gsub gwtqd joinrtd joinup joinpio mag2d mkwght mlt mstrfcn newnam rcopy recmax recmin relabl rmax rmerge rmergeml rmin murge rsplit rzonavg select repack sellev separmc spltall spltslb sqroot square statsav sub taphi timavg timmax timmin tstep txt2bin window xfind xjoin xlin xsave xylin zonavg lpcrev2 icntrld initgs9 initgs9r initss9 initss9r icntrlc boxl2hr initg14 pakgcm9 rstime splinv pakrs unpakrs perturb pakrs2 paktnd perturb_winds gcmrcpy

# List the diagnostics programs used only when gas-phase chemistry is active in
# MAM mode. Most of these are specific to gas-phase chemistry and a few are
# simply older routines no longer used by the regular CanESM diagnostics.
GAS_CHEM_DIAG_PGMS = initgsx_t10_2000 pakgcm9_gaschem dnmask gridcalc_eta llafrb pull_sfc_stns_v2 pull_sondes_v2 rzonavg3 sf6-to-age timavgmsk vsinth_trop fpow pgen ggdlat ggdlon dxdp ggtrig gsathl gsomgah vsinth loge
GAS_CHEM_DIAG_F90 = epflux4gaschem

# Attach the names of the gas-phase chemistry programs to the 64-bit list
DEFAULT_DIAG_64BIT_PGMS += $(GAS_CHEM_DIAG_PGMS) 

# Stick .f on the end of all the names of F77 routines
TMP_DEFAULT_DIAG_64BIT_BIN_SOURCE	= $(addsuffix .f, $(DEFAULT_DIAG_64BIT_PGMS))
# Stick .f90 on the end of the names that are f90 routines
TMP_GAS_CHEM_F90	= $(addsuffix .f90, $(GAS_CHEM_DIAG_F90))
# Glom those two lists together and proceed as before
TMP_DEFAULT_DIAG_64BIT_BIN_SOURCE += $(TMP_GAS_CHEM_F90)

DEFAULT_DIAG_64BIT_BIN_SOURCE		= $(filter $(addprefix %/, $(TMP_DEFAULT_DIAG_64BIT_BIN_SOURCE)), $(DIAG_BIN_SOURCE))
DEFAULT_DIAG_64BIT_BIN_TARGETS		= $(addprefix $(BIN_DIR)/, $(notdir $(basename $(DEFAULT_DIAG_64BIT_BIN_SOURCE))))

# define sublist 32bit target list
#	NOTE: we only need a subset of these, and we append "_float1" to differentiate between the 64bit counter parts
DEFAULT_DIAG_32BIT_PGMS = unpakrs rcopy newnam murge pakrs pakgcm9 perturb select repack separmc rstime joinup pakgcm8 pakrs2 joinpio sub paktnd perturb_winds ggstat gcmrcpy lpcrev2 icntrld initgs9 initss9 boxl2hr initg14

# Attach the names of the gas-phase chemistry programs to the 32-bit list
DEFAULT_DIAG_32BIT_PGMS += $(GAS_CHEM_DIAG_PGMS)

# Stick .f on the end of all the names of F77 routines
TMP_DEFAULT_DIAG_32BIT_BIN_SOURCE	= $(addsuffix .f, $(DEFAULT_DIAG_32BIT_PGMS))
# Glom the list of f90 routines (one, so far) onto the end of this list too
TMP_DEFAULT_DIAG_32BIT_BIN_SOURCE += $(TMP_GAS_CHEM_F90)

DEFAULT_DIAG_32BIT_BIN_SOURCE		= $(filter $(addprefix %/, $(TMP_DEFAULT_DIAG_32BIT_BIN_SOURCE)), $(DIAG_BIN_SOURCE))
DEFAULT_DIAG_32BIT_BIN_TARGETS		= $(addprefix $(BIN_DIR)/, $(addsuffix _float1, $(notdir $(basename $(DEFAULT_DIAG_32BIT_BIN_SOURCE)))))

# General Object Rules - we rely on VPATH for the source files to be found
# 	64 bit objects
$(LIB64_DIR)/%.o : $(notdir %.f)
	$(FC) $(FFLAGS_64BIT) -c $< -o $@
$(LIB64_DIR)/%.o : $(notdir %.f90)
	$(FC) $(FFLAGS_64BIT) -c $< -o $@
$(LIB64_DIR)/%.o : $(notdir %.F)
	$(FC) $(FFLAGS_64BIT) -c $< -o $@
$(LIB64_DIR)/%.o : $(notdir %.F90)
	$(FC) $(FFLAGS_64BIT) -c $< -o $@
$(LIB64_DIR)/%.o : $(notdir %.c)
	$(CC) $(CFLAGS) -c $< -o $@

# 	32 bit objects
$(LIB32_DIR)/%.o : $(notdir %.f)
	$(FC) $(FFLAGS_32BIT) -c $< -o $@
$(LIB32_DIR)/%.o : $(notdir %.f90)
	$(FC) $(FFLAGS_32BIT) -c $< -o $@
$(LIB32_DIR)/%.o : $(notdir %.F)
	$(FC) $(FFLAGS_32BIT) -c $< -o $@
$(LIB32_DIR)/%.o : $(notdir %.F90)
	$(FC) $(FFLAGS_32BIT) -c $< -o $@
$(LIB32_DIR)/%.o : $(notdir %.c)
	$(CC) $(CFLAGS) -c $< -o $@

# create function to create rules diagnostic binaries (not ploting pgms)
#	arg1 : target name
#	arg2 : program source file
#	arg3 : compilation flags
#	arg4 : lib directory
#
define DIAG_BIN_RULE
$(BIN_DIR)/$(1): $(2) $(4)/libcandiag.a $(4)/diag_sizes.o
	$(FC) $(3) -o $$@ $$^ $(FFLAGS_MOD) $(4)
endef

# create function to create rules for plotting programs
#	arg1 : target name
#	arg2 : program source file
#
# Note that these programs are always 32bit
#
define PLOT_BIN_RULE
$(BIN_DIR)/$(1): $(2) $(PLT_LIB_DIR)/libcandiag.a $(PLT_LIB_DIR)/diag_sizes.o
	$(FC) $(FFLAGS_32BIT) -o $$@ $$^ $(FFLAGS_MOD) $(PLT_LIB_DIR) $(LDFLAGS_CANDIAG_PLT)
endef

#-- Define 64 bit CanDIAG Suite Targets
# subroutine library
$(LIB64_DIR)/libcandiag.a: $(LIB_64BIT_OBJECTS)
	ar rc $@ $^

# sizes module
$(LIB64_DIR)/diag_sizes.o: ../diag_sizes.f90
	$(FC) $(FFLAGS_64BIT) $(CPPDEFS_64BIT) $(FFLAGS_CPP) -c $< -o $@ $(FFLAGS_MOD) $(LIB64_DIR)

# build rules for ALL non-plotting 64bit targets
$(foreach bin_source,$(ALL_DIAG_64BIT_BIN_SOURCE),$(eval $(call DIAG_BIN_RULE,$(notdir $(basename $(bin_source))),$(bin_source),$(FFLAGS_64BIT),$(LIB64_DIR))))

#--- Define 32 bit CanDIAG Suite Targets
# subroutine library
$(LIB32_DIR)/libcandiag.a: $(LIB_32BIT_OBJECTS)
	ar rc $@ $^

# sizes module
$(LIB32_DIR)/diag_sizes.o: ../diag_sizes.f90
	$(FC) $(FFLAGS_32BIT) $(CPPDEFS_32BIT) $(FFLAGS_CPP) -c $< -o $@ $(FFLAGS_MOD) $(LIB32_DIR)

# here we only create rules for the default non-plotting targets, but it could easily be expanded
$(foreach bin_source,$(DEFAULT_DIAG_32BIT_BIN_SOURCE),$(eval $(call DIAG_BIN_RULE,$(notdir $(basename $(bin_source)))_float1,$(bin_source),$(FFLAGS_32BIT),$(LIB32_DIR))))

# create rules for plotting programs, which are always in 32bit mode
$(foreach bin_source,$(PLOT_BIN_SOURCE),$(eval $(call PLOT_BIN_RULE,$(notdir $(basename $(bin_source))),$(bin_source))))

#~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~
# This section relates to the CanDIAG inline Minimal Working example code
#~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~
MWE_DIR=../mwe

$(LIB32_DIR)/utilities.o: $(MWE_DIR)/utilities.f90
	$(FC)  -c $< -o $@ $(FFLAGS_32BIT) $(FFLAGS_MOD) $(LIB32_DIR) $(INCLUDEFLAGS_USR) $(LDFLAGS_USR) $(LDFLAGS_NETCDF)

$(LIB32_DIR)/ccc_data.o: $(MWE_DIR)/ccc_data.f90 $(LIB32_DIR)/utilities.o
	$(FC)  -c $< -o $@ $(FFLAGS_32BIT) $(FFLAGS_MOD) $(LIB32_DIR) $(INCLUDEFLAGS_USR) $(LDFLAGS_USR) $(LDFLAGS_NETCDF)

$(LIB32_DIR)/rebuild.o: $(MWE_DIR)/rebuild.f90 $(LIB32_DIR)/ccc_data.o
	$(FC)  -c $< -o $@ $(FFLAGS_32BIT) $(FFLAGS_MOD) $(LIB32_DIR) $(INCLUDEFLAGS_USR) $(LDFLAGS_USR) $(LDFLAGS_NETCDF)

$(LIB32_DIR)/stats.o: $(MWE_DIR)/stats.f90 $(LIB32_DIR)/ccc_data.o
	$(FC)  -c $< -o $@ $(FFLAGS_32BIT) $(FFLAGS_MOD) $(LIB32_DIR) $(INCLUDEFLAGS_USR) $(LDFLAGS_USR) $(LDFLAGS_NETCDF)

$(LIB32_DIR)/transforms.o: $(MWE_DIR)/transforms.f90 $(LIB32_DIR)/ccc_data.o
	$(FC)  -c $< -o $@ $(FFLAGS_32BIT) $(FFLAGS_MOD) $(LIB32_DIR) $(INCLUDEFLAGS_USR) $(LDFLAGS_USR) $(LDFLAGS_NETCDF)

$(LIB32_DIR)/operations.o: $(MWE_DIR)/operations.f90 $(LIB32_DIR)/ccc_data.o $(LIB32_DIR)/transforms.o
	$(FC)  -c $< -o $@ $(FFLAGS_32BIT) $(FFLAGS_MOD) $(LIB32_DIR) $(INCLUDEFLAGS_USR) $(LDFLAGS_USR) $(LDFLAGS_NETCDF)

$(LIB32_DIR)/candiag_mwe.o: $(MWE_DIR)/candiag_mwe.f90 $(LIB32_DIR)/utilities.o $(LIB32_DIR)/ccc_data.o $(LIB32_DIR)/rebuild.o $(LIB32_DIR)/stats.o $(LIB32_DIR)/transforms.o $(LIB32_DIR)/operations.o
	$(FC)  -c $< -o $@ $(FFLAGS_32BIT) $(FFLAGS_MOD) $(LIB32_DIR) $(INCLUDEFLAGS_USR) $(LDFLAGS_USR) $(LDFLAGS_NETCDF)

MWE_OBJECTS=$(LIB32_DIR)/diag_sizes.o $(LIB32_DIR)/utilities.o $(LIB32_DIR)/ccc_data.o $(LIB32_DIR)/rebuild.o $(LIB32_DIR)/stats.o $(LIB32_DIR)/transforms.o $(LIB32_DIR)/operations.o $(LIB32_DIR)/candiag_mwe.o

candiag_mwe.exe: $(MWE_OBJECTS)
	$(FC) -o $(BIN_DIR)/$@ $(MWE_OBJECTS) $(LIB32_DIR)/libcandiag.a $(FFLAGS_32BIT) $(FFLAGS_MOD) $(LIB32_DIR) $(INCLUDEFLAGS_USR) $(LDFLAGS_USR) $(LDFLAGS_NETCDF)

#~~~~~~~~~~~~~~~~~~~
# NCCRIP Compilation
#~~~~~~~~~~~~~~~~~~~
NCCRIP_DIR=../nccrip
NCCRIP_SOURCE=$(filter-out $(NCCRIP_DIR)/nccrip_main.F90, $(wildcard $(NCCRIP_DIR)/*.f $(NCCRIP_DIR)/*.F $(NCCRIP_DIR)/*.f90 $(NCCRIP_DIR)/*.F90))

# Define rules for nccrip programs within a macro/function/"canned sequence" to allow for 32-bit and 64-bit compilation
# 	arg1 : directory containing library functions
# 	arg2 : flag grouping for the desired precision
# 	arg3 : name of the final nccrip executable
define NCCRIP_COMPILATION_RULES
LIB_NCCRIP_DIR=$(1)
FFLAGS_PRECIS_NCCRIP=$(2)
NCCRIP_EXEC_NAME=$(3)

# determine precision specific object files and group flags
NCCRIP_OBJECTS=$$(subst $$(NCCRIP_DIR), $$(LIB_NCCRIP_DIR), $$(patsubst %.f, %.o, $$(patsubst %.F, %.o, $$(patsubst %.f90, %.o, $$(patsubst %.F90, %.o, $$(NCCRIP_SOURCE))))))
NCCRIP_FFLAGS=$$(FFLAGS_PRECIS_NCCRIP) $$(FFLAGS_MOD) $$(LIB_NCCRIP_DIR) -I./$$(LIB_NCCRIP_DIR) $$(INCLUDEFLAGS_USR)

# GERNERIC NCCRIP RULES (with no explicit dependencies)
$$(LIB_NCCRIP_DIR)/%.o: $$(NCCRIP_DIR)/%.f
	$$(FC) $$(NCCRIP_FFLAGS) -c $$< -o $$@
$$(LIB_NCCRIP_DIR)/%.o: $$(NCCRIP_DIR)/%.f90
	$$(FC) $$(NCCRIP_FFLAGS) -c $$< -o $$@
$$(LIB_NCCRIP_DIR)/%.o: $$(NCCRIP_DIR)/%.F
	$$(FC) $$(NCCRIP_FFLAGS) -c $$< -o $$@
$$(LIB_NCCRIP_DIR)/%.o: $$(NCCRIP_DIR)/%.F90
	$$(FC) $$(NCCRIP_FFLAGS) -c $$< -o $$@

# RULES WITH EXPLICIT DEPENDENCIES
$$(LIB_NCCRIP_DIR)/var_meta_data.o: $$(NCCRIP_DIR)/var_meta_data.F90 $$(LIB_NCCRIP_DIR)/nccrip_params.o $$(LIB_NCCRIP_DIR)/strings.o
	$$(FC) $$(NCCRIP_FFLAGS) -c $$< -o $$@
$$(LIB_NCCRIP_DIR)/derived_vars.o: $$(NCCRIP_DIR)/derived_vars.F90 $$(LIB_NCCRIP_DIR)/nccrip_params.o $$(LIB_NCCRIP_DIR)/var_meta_data.o
	$$(FC) $$(NCCRIP_FFLAGS) -c $$< -o $$@
$$(LIB_NCCRIP_DIR)/ccc_io.o: $$(NCCRIP_DIR)/ccc_io.F90 $$(LIB_NCCRIP_DIR)/iocom.o
	$$(FC) $$(NCCRIP_FFLAGS) -c $$< -o $$@
$$(LIB_NCCRIP_DIR)/file_subs.o: $$(NCCRIP_DIR)/file_subs.F90 $$(LIB_NCCRIP_DIR)/nccrip_params.o $$(LIB_NCCRIP_DIR)/strings.o
	$$(FC) $$(NCCRIP_FFLAGS) -c $$< -o $$@
$$(LIB_NCCRIP_DIR)/time_subs.o: $$(NCCRIP_DIR)/time_subs.F90 $$(LIB_NCCRIP_DIR)/nccrip_params.o $$(LIB_NCCRIP_DIR)/strings.o
	$$(FC) $$(NCCRIP_FFLAGS) -c $$< -o $$@
$$(LIB_NCCRIP_DIR)/crec_subs.o: $$(NCCRIP_DIR)/crec_subs.F90 $$(LIB_NCCRIP_DIR)/time_subs.o
	$$(FC) $$(NCCRIP_FFLAGS) -c $$< -o $$@
$$(LIB_NCCRIP_DIR)/ccc_subs.o: $$(NCCRIP_DIR)/ccc_subs.F90 $$(LIB_NCCRIP_DIR)/nccrip_params.o $$(LIB_NCCRIP_DIR)/iocom.o $$(LIB_NCCRIP_DIR)/time_subs.o $$(LIB_NCCRIP_DIR)/strings.o $$(LIB_NCCRIP_DIR)/crec_data.o
	$$(FC) $$(NCCRIP_FFLAGS) -c $$< -o $$@
$$(LIB_NCCRIP_DIR)/grid_info.o: $$(NCCRIP_DIR)/grid_info.F90 $$(LIB_NCCRIP_DIR)/nccrip_params.o $$(LIB_NCCRIP_DIR)/crec_subs.o $$(LIB_NCCRIP_DIR)/ccc_subs.o $$(LIB_NCCRIP_DIR)/strings.o
	$$(FC) $$(NCCRIP_FFLAGS) -c $$< -o $$@
$$(LIB_NCCRIP_DIR)/nc_subs.o: $$(NCCRIP_DIR)/nc_subs.F90 $$(LIB_NCCRIP_DIR)/nccrip_params.o $$(LIB_NCCRIP_DIR)/crec_subs.o $$(LIB_NCCRIP_DIR)/strings.o $$(LIB_NCCRIP_DIR)/var_meta_data.o $$(LIB_NCCRIP_DIR)/ccc_io.o $$(LIB_NCCRIP_DIR)/time_subs.o $$(LIB_NCCRIP_DIR)/grid_info.o
	$$(FC) $$(NCCRIP_FFLAGS) -c $$< -o $$@
$$(LIB_NCCRIP_DIR)/crec_data.o: $$(NCCRIP_DIR)/crec_data.F90 $$(LIB_NCCRIP_DIR)/nccrip_params.o $$(LIB_NCCRIP_DIR)/iocom.o $$(LIB_NCCRIP_DIR)/strings.o $$(LIB_NCCRIP_DIR)/time_subs.o $$(LIB_NCCRIP_DIR)/crec_subs.o $$(LIB_NCCRIP_DIR)/ccc_io.o
	$$(FC) $$(NCCRIP_FFLAGS) -c $$< -o $$@
$$(LIB_NCCRIP_DIR)/proc_file.o: $$(NCCRIP_DIR)/proc_file.F90 $$(LIB_NCCRIP_DIR)/nccrip_params.o $$(LIB_NCCRIP_DIR)/crec_data.o $$(LIB_NCCRIP_DIR)/crec_subs.o $$(LIB_NCCRIP_DIR)/iocom.o $$(LIB_NCCRIP_DIR)/time_subs.o $$(LIB_NCCRIP_DIR)/ccc_io.o $$(LIB_NCCRIP_DIR)/ccc_subs.o $$(LIB_NCCRIP_DIR)/nc_subs.o $$(LIB_NCCRIP_DIR)/file_subs.o $$(LIB_NCCRIP_DIR)/strings.o $$(LIB_NCCRIP_DIR)/derived_vars.o
	$$(FC) $$(NCCRIP_FFLAGS) -c $$< -o $$@
$$(LIB_NCCRIP_DIR)/ccc_time_series.o: $$(NCCRIP_DIR)/ccc_time_series.F90 $$(LIB_NCCRIP_DIR)/nccrip_params.o $$(LIB_NCCRIP_DIR)/file_subs.o $$(LIB_NCCRIP_DIR)/ccc_io.o $$(LIB_NCCRIP_DIR)/ccc_subs.o $$(LIB_NCCRIP_DIR)/time_subs.o $$(LIB_NCCRIP_DIR)/strings.o $$(LIB_NCCRIP_DIR)/nc_subs.o $$(LIB_NCCRIP_DIR)/crec_subs.o $$(LIB_NCCRIP_DIR)/crec_data.o $$(LIB_NCCRIP_DIR)/iocom.o
	$$(FC) $$(NCCRIP_FFLAGS) -c $$< -o $$@
$$(LIB_NCCRIP_DIR)/nccrip_cmdline.o: $$(NCCRIP_DIR)/nccrip_cmdline.F90 $$(LIB_NCCRIP_DIR)/nccrip_params.o $$(LIB_NCCRIP_DIR)/iocom.o $$(LIB_NCCRIP_DIR)/var_meta_data.o $$(LIB_NCCRIP_DIR)/derived_vars.o $$(LIB_NCCRIP_DIR)/strings.o $$(LIB_NCCRIP_DIR)/time_subs.o $$(LIB_NCCRIP_DIR)/file_subs.o $$(LIB_NCCRIP_DIR)/ccc_io.o $$(LIB_NCCRIP_DIR)/ccc_subs.o $$(LIB_NCCRIP_DIR)/grid_info.o
	$$(FC) $$(NCCRIP_FFLAGS) -c $$< -o $$@

# NCCRIP EXEC
$$(NCCRIP_EXEC_NAME): $$(NCCRIP_OBJECTS) $$(LIB_NCCRIP_DIR)/libcandiag.a $$(NCCRIP_DIR)/nccrip_main.F90
	$$(FC) $$(NCCRIP_FFLAGS) -o $$(BIN_DIR)/$$@ $$^ $$(LDFLAGS_USR) $$(LDFLAGS_NETCDF)
endef

# create 64bit and 32bit nccrip
$(eval $(call NCCRIP_COMPILATION_RULES,$(LIB64_DIR),$(FFLAGS_64BIT),nccrip))
$(eval $(call NCCRIP_COMPILATION_RULES,$(LIB32_DIR),$(FFLAGS_32BIT),nccrip_float1))

#~~~~~~~~~~~~~~~~~~~~~~~~~
# Define Target Groupings
#~~~~~~~~~~~~~~~~~~~~~~~~~
.PHONY: all_diag_bins default_bins folders clean plot_pgms dummy
.DEFAULT_GOAL := default_bins

# create folders
folders:
	mkdir -p $(BIN_DIR)
	mkdir -p $(LIB64_DIR)
	mkdir -p $(LIB32_DIR)

# build the plotting programs
plot_pgms: folders $(PLOT_BIN_TARGETS)

# ALL binaries (excluding plotting programs)
all_diag_bins: folders $(ALL_DIAG_64BIT_BIN_TARGETS) $(DEFAULT_DIAG_32BIT_BIN_TARGETS) candiag_mwe.exe nccrip nccrip_float1

# DEFAULT binaries only
default_bins: folders $(DEFAULT_DIAG_64BIT_BIN_TARGETS) $(DEFAULT_DIAG_32BIT_BIN_TARGETS) candiag_mwe.exe nccrip nccrip_float1

clean:
	rm -rf lib* bin

dummy:
	@echo $(NCCRIP_OBJECTS)
